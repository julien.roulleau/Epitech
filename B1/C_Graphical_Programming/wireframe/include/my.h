/*
**my.hformy_draw_linein/home/infocraft/Job/bswireframe/
**
**MadebyRoulleauJulien
**Login<julien.roulleau@epitech.eu>
**
**StartedonThuNov1717:50:382016RoulleauJulien
**LastupdateThuNov1717:51:242016RoulleauJulien
*/

#ifndef _MY_H_

# define _MY_H_

# include <SFML/Graphics.h>
# include <unistd.h>
# include <stdlib.h>
# include <math.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <stdio.h>

# define SCREEN_WIDTH 1600
# define SCREEN_HEIGHT 800
# define BITS_PER_PIXEL 32
# define IS_NBR(c) (c >= '0' && c <= '9')

typedef struct 		s_list
{
	int		nb;
	int 		root;
	struct s_list	*previous;
	struct s_list	*next;
}			t_list;

typedef struct		s_my_framebuffer
{
	sfUint8		*pixels;
	int		width;
	int		height;
}			t_my_framebuffer;

/* main */
void		init_windows(sfRenderWindow **, sfSprite **,
				sfTexture **, t_my_framebuffer **);
void		play(sfRenderWindow *, sfSprite *,
			sfTexture *, t_my_framebuffer *);
/* list */
int		init_list(t_list **, int);
int		add_node(t_list *, int);

/* wireframe */
int		*parcing(t_list **, char *);
void		map(t_my_framebuffer *, int *);
int		*list_in_tab(t_list *, int);

/* aux */
t_my_framebuffer 	*my_framebuffer_create(int, int);
sfVector2i	pos2d(int, int);
sfVector3f	pos3d(int, int, int);
sfColor		color_put(int, int, int, int);
int		my_getnbr(char *, int);

/* pixel */
void 		my_put_pixel(t_my_framebuffer *,
			int, int, sfColor);

/* my_draw_line */
void		my_draw_line(t_my_framebuffer *,
			sfVector2i, sfVector2i, sfColor);

/* my_parallel_projection */
sfVector2i 	my_parallel_projection(sfVector3f, float);

/* my_isometric_projection */
sfVector2i 	my_isometric_projection(sfVector3f);

#endif		/* !_MY_H_ */
