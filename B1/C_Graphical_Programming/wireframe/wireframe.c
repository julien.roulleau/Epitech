/*
** wireframe.c for wireframe in /home/infocraft/Job/En_Cours/wireframe/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Dec  5 13:50:20 2016 Roulleau Julien
** Last update Sun Dec 11 15:32:38 2016 Roulleau Julien
*/

#include "my.h"

int		*parcing(t_list **list, char *file)
{
	char	buffer[2];
	int 	fd;
	int	tmp;
	int	line;

	if ((fd = open(file, O_RDONLY)) == -1 || ((tmp = 0) && (line = 1)))
		return (0);
	while ((read(fd, buffer, 1)) > 0 &&
		!(buffer[1] = '\0') && !(IS_NBR(buffer[0])));
	init_list(list, my_getnbr(buffer, 0));
	while ((read(fd, buffer, 1)) > 0 && !(buffer[1] = '\0'))
	{
		if (buffer[0] == '\n')
			line++;
		else if (tmp == 1 && IS_NBR(buffer[0]))
			(*list)->previous->nb = (*list)->previous->nb * 10 +
				my_getnbr(buffer, 0);
		else if (IS_NBR(buffer[0]) && (tmp = 1))
			add_node(*list, my_getnbr(buffer, 0));
		else
			tmp = 0;
	}
	close(fd);
	return (list_in_tab(*list, line));
}

int		*list_in_tab(t_list *list, int line)
{
	int	*tab;
	int	count;

	count = 1;
	while ((list = list->next) && !(list->root))
		count++;
	tab = malloc(sizeof(int) * (count + 3));
	count = 2;
	tab[1] = line;
	tab[count] = list->nb;
	while ((list = list->next) && !(list->root))
		tab[++count] = list->nb;
	tab[++count] = -1;
	tab[0] = (count - 1) / tab[1];
	return (tab);
}

void 		map(t_my_framebuffer *framebuffer, int *tab)
{
	int	i;
	int	c;
	int	l;
	int	aw;
	int	ah;
	int	w;
	int	h;

	w = SCREEN_WIDTH / (tab[0] + 2);
	h = SCREEN_HEIGHT / (tab[1] + 2);
	ah = h * 2;
	l = -1;
	i = 1;
	while (++l < tab[1])
	{
		aw = (w * tab[0] - w) / 2;
		c = -1;
		while (++c < tab[0])
		{
			i++;
			my_draw_line(framebuffer,
				my_parallel_projection(pos3d(aw, ah, -tab[i] * 30), 35),
				my_parallel_projection(pos3d(aw + w, ah, -tab[i + 1] * 30), 35),
				color_put(255, 0, 255, 255));
			if (l + 1 != tab[1])
				my_draw_line(framebuffer,
					my_parallel_projection(pos3d(aw, ah, -tab[i] * 30), 35),
					my_parallel_projection(pos3d(aw, ah + h, -tab[i + tab[0]] * 30), 35),
					color_put(255, 0, 255, 255));
			aw += w;
		}
		if (l + 1 != tab[1])
			my_draw_line(framebuffer,
				my_parallel_projection(pos3d(aw, ah, -tab[i + 1] * 30), 35),
				my_parallel_projection(pos3d(aw, ah + h, -tab[i + tab[0] + 1] * 30), 35),
				color_put(255, 0, 255, 255));
		ah += h;
	}
}
