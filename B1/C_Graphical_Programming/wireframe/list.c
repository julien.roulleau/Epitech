/*
** list.c for my_ls in /home/infocraft/Job/En_Cours/PSU_2016_my_ls/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Nov 30 20:08:58 2016 Roulleau Julien
** Last update Tue Dec  6 16:48:43 2016 Roulleau Julien
*/

#include "my.h"

int 		init_list(t_list **list, int nb)
{
	if (!(*list = malloc(sizeof(t_list))))
		return (84);
	(*list)->nb = nb;
	(*list)->next = *list;
	(*list)->previous = *list;
	(*list)->root = 1;
	return (0);
}

int		add_node(t_list *list, int nb)
{
	t_list	*node;

	if (!(node = malloc(sizeof(t_list))))
		return (84);
	node->root = 0;
	node->nb = nb;
	node->previous = list->previous;
	node->next = list;
	list->previous->next = node;
	list->previous = node;
	return (0);
}
