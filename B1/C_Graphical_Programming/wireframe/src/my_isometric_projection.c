/*
** my_isometric_projection.c for my_isometric_projection in /home/infocraft/Job/bswireframe/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Nov 18 21:10:13 2016 Roulleau Julien
** Last update Thu Dec  8 03:04:48 2016 Roulleau Julien
*/

#include "my.h"

sfVector2i 		my_isometric_projection(sfVector3f pos3d)
{
	sfVector2i	vect;
	float		angle;

	angle = 30 * (M_PI / 180);
	vect.x = pos3d.x * cos(angle) - pos3d.y * cos(angle);
	vect.y = pos3d.x * sin(angle) + pos3d.y * sin(angle) - pos3d.z;
	return (vect);
}
