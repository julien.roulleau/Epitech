/*
** my_draw_line.c for my_draw_line in /home/infocraft/Job/bswireframe/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Nov 18 09:58:02 2016 Roulleau Julien
** Last update Sat Dec 10 16:01:05 2016 Roulleau Julien
*/

#include "my.h"

# define ABS(nb) (nb < 0 ? nb * -1 : nb)

void		my_draw_line(t_my_framebuffer *framebuffer,
				sfVector2i from, sfVector2i to, sfColor color)
{
	float 	size;
	float 	x;
	float 	y;
	float 	dx;
	float 	dy;
	float 	i;

	if (ABS(to.x - from.x) > ABS(to.y - from.y))
		size = ABS(to.x - from.x);
	else
		size = ABS(to.y - from.y);
	dx = (to.x - from.x) / size;
	dy = (to.y - from.y) / size;
	x = from.x;
	y = from.y;
	my_put_pixel(framebuffer, x, y, color);
	i = -1;
	while (++i < size)
	{
		x += dx;
		y += dy;
		my_put_pixel(framebuffer, x, y, color);
	}
}
