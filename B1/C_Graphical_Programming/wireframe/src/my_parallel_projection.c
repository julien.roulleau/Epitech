/*
** my_parallel_projection.c for my_parallel_projection in /home/infocraft/Job/bswireframe/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Nov 18 20:08:09 2016 Roulleau Julien
** Last update Thu Dec  8 03:05:02 2016 Roulleau Julien
*/

#include "my.h"

sfVector2i 		my_parallel_projection(sfVector3f pos3d, float angle)
{
	sfVector2i	vect;

	angle = angle * (M_PI / 180);
	vect.x = pos3d.x - pos3d.y * sin(angle);
	vect.y = pos3d.y * cos(angle) - pos3d.z;
	return (vect);
}
