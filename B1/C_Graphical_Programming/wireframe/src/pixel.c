/*
** pixel.c for pixel in /home/infocraft/Job/bswireframe/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Nov 18 20:08:40 2016 Roulleau Julien
** Last update Mon Dec  5 11:36:04 2016 Roulleau Julien
*/

#include "my.h"

void 		my_put_pixel(t_my_framebuffer *framebuffer,
					int x, int y, sfColor color)
{
	if (x < 0 || y < 0)
		return;
	if (x >= framebuffer->width || y >= framebuffer->height)
		return;
	framebuffer->pixels[(framebuffer->width * y + x) * 4] = color.r;
	framebuffer->pixels[(framebuffer->width * y + x) * 4 + 1] = color.g;
	framebuffer->pixels[(framebuffer->width * y + x) * 4 + 2] = color.b;
	framebuffer->pixels[(framebuffer->width * y + x) * 4 + 3] = color.a;
}
