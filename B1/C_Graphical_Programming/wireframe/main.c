/*
** main.c for main in /home/infocraft/Job/bswireframe/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Nov 18 20:09:15 2016 Roulleau Julien
** Last update Sun Dec 11 15:00:17 2016 Roulleau Julien
*/

#include "my.h"

int 				main(int argc, char **argv)
{
	sfRenderWindow		*window;
	sfSprite 		*sprite;
	sfTexture		*texture;
	t_my_framebuffer	*framebuffer;
	t_list			*list;
	int			*tab;

	if (argc < 2)
		return (84);
	tab = parcing(&list, argv[1]);
	init_windows(&window, &sprite, &texture, &framebuffer);
	while (sfRenderWindow_isOpen(window))
	{
		sfRenderWindow_clear(window, sfBlack);
		map(framebuffer, tab);
		play(window, sprite, texture, framebuffer);
	}
	sfRenderWindow_destroy(window);
	return (0);
}

void		init_windows(sfRenderWindow **window, sfSprite **sprite,
			sfTexture **texture, t_my_framebuffer **framebuffer)
{
	sfVideoMode		mode;

	mode.width = SCREEN_WIDTH;
	mode.height = SCREEN_HEIGHT;
	mode.bitsPerPixel = BITS_PER_PIXEL;
	*window = sfRenderWindow_create (mode, "SFMLwindow",
			sfResize | sfClose, NULL);
	*sprite = sfSprite_create();
	*texture = sfTexture_create(SCREEN_WIDTH, SCREEN_HEIGHT);
	sfSprite_setTexture(*sprite, *texture, sfTrue);
	*framebuffer = my_framebuffer_create(SCREEN_WIDTH, SCREEN_HEIGHT);
}

void		play(sfRenderWindow *window, sfSprite *sprite,
			sfTexture *texture, t_my_framebuffer *framebuffer)
{
	sfEvent	 		event;

	while (sfRenderWindow_pollEvent(window, &event))
		if (event.type == sfEvtClosed)
			sfRenderWindow_close(window);
	sfTexture_updateFromPixels(texture, framebuffer->pixels,
			SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0);
	sfRenderWindow_drawSprite(window, sprite, NULL);
	sfRenderWindow_display(window);
}
