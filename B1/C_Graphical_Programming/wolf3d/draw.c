/*
** draw.c for wolf3d in /home/infocraft/Job/En_Cours/wolf3d/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Dec 28 23:25:34 2016 Roulleau Julien
** Last update Tue Jan 10 16:54:15 2017 Roulleau Julien
*/

#include "my.h"

void 		draw_background(t_my_framebuffer *framebuffer)
{
	int 	dir;

	dir = -1;
	while (++dir < SCREEN_WIDTH)
	{
		my_draw_line(framebuffer,
			pos2d(dir, 0),
			pos2d(dir, SCREEN_HEIGHT / 2),
			color_put(0, 0, 150, 255));
		my_draw_line(framebuffer,
			pos2d(dir, SCREEN_HEIGHT / 2),
			pos2d(dir, SCREEN_HEIGHT),
			color_put(0, 150, 0, 255));
	}
}

void 		draw_map(t_my_framebuffer *framebuffer,
			int **map, t_entity entity)
{
	float	tmp;
	float 	dir;
	float	k;
	int	pos;

	tmp = (float) FOV / (float) SCREEN_WIDTH;
	dir = entity.look;
	pos = -1;
	while (++pos < SCREEN_WIDTH)
	{
		k = raycast(entity.player, dir, map, entity.mapsize);
		k *=  cos((dir - entity.look) * M_PI / (float) 180);
		draw_wall(framebuffer, pos, k);
		dir += tmp;
	}
}

void 		draw_wall(t_my_framebuffer *framebuffer, int pos, float k)
{
	if (WALL / k > SCREEN_HEIGHT)
		my_draw_line(framebuffer,
			pos2d(pos, SCREEN_HEIGHT / 2 - (SCREEN_HEIGHT / 2)),
			pos2d(pos, SCREEN_HEIGHT / 2 + (SCREEN_HEIGHT / 2)),
			color_put(150, 100, 0, 255));
	else
		my_draw_line(framebuffer,
			pos2d(pos, SCREEN_HEIGHT / 2 - (WALL / k)),
			pos2d(pos, SCREEN_HEIGHT / 2 + (WALL / k)),
			color_put(150, 100, 0, 255));
}
