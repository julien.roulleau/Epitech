/*
** move_forward.c for wolf3d in /home/infocraft/Job/En_Cours/wolf3d/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun Dec 18 22:30:32 2016 Roulleau Julien
** Last update Wed Jan  4 19:33:46 2017 Roulleau Julien
*/

#include "my.h"

sfVector2f		my_move_forward(sfVector2f pos, float direction,
				float distance, int **map)
{
	sfVector2f	new_pos;
	float		rad;

	new_pos.x = pos.x;
	new_pos.y = pos.y;
	rad = (direction * M_PI) / (float) 180;
	new_pos.x += distance * cos(rad);
	new_pos.y += distance * sin(rad);
	if (map[ROUND(new_pos.y)][ROUND(pos.x)] != 1)
		pos.y = new_pos.y;
	if (map[ROUND(pos.y)][ROUND(new_pos.x)] != 1)
		pos.x = new_pos.x;
	return (pos);
}
