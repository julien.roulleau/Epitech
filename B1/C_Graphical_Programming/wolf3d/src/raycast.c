/*
** raycast.c for wolf3d in /home/infocraft/Job/En_Cours/wolf3d/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Dec 22 10:32:33 2016 Roulleau Julien
** Last update Mon Jan 23 09:28:42 2017 Roulleau Julien
*/

#include "my.h"

float 			raycast(sfVector2f player, float direction,
					int **map, sfVector2i mapsize)
{
	sfVector2f	vector;
	sfVector2f	pos_k;
	float 		rad;
	float 		k;

	k = 0;
	pos_k.x = player.x;
	pos_k.y = player.y;
	rad = direction * (M_PI / 180);
	vector.x = cos(rad);
	vector.y = sin(rad);
	while (pos_k.y >= 0 && pos_k.y + 1 < mapsize.y &&
			pos_k.x >= 0 && pos_k.x + 1 < mapsize.x &&
			map[ROUND(pos_k.y)][ROUND(pos_k.x)] != 1)
	{
		k += 0.01;
		pos_k.x = player.x + (k * vector.x);
		pos_k.y = player.y + (k * vector.y);
	}
	return (k);

}
