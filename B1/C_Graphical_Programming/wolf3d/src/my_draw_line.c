/*
** my_draw_line.c for my_draw_line in /home/infocraft/Job/bswireframe/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Nov 18 09:58:02 2016 Roulleau Julien
** Last update Mon Dec 26 17:07:28 2016 Roulleau Julien
*/

#include "my.h"

void			my_draw_line(t_my_framebuffer *framebuffer,
				sfVector2i from, sfVector2i to, sfColor color)
{
	float 		size;
	sfVector2f	vector;
	sfVector2f	delta;
	int 		i;

	if (ABS(to.x - from.x) > ABS(to.y - from.y))
		size = ABS(to.x - from.x);
	else
		size = ABS(to.y - from.y);
	delta.x = (to.x - from.x) / size;
	delta.y = (to.y - from.y) / size;
	vector.x = from.x;
	vector.y = from.y;
	i = -1;
	while (++i < size)
	{
		my_put_pixel(framebuffer, vector.x, vector.y, color);
		vector.x += delta.x;
		vector.y += delta.y;
	}
}
