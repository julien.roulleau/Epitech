/*
** main.c for wolf3d in /home/infocraft/Job/En_Cours/wolf3d/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Dec 19 10:56:23 2016 Roulleau Julien
** Last update Tue Jan 10 17:40:33 2017 Roulleau Julien
*/

#include "my.h"

int 				main(int argc, char **argv)
{
	sfRenderWindow		*window;
	sfSprite 		*sprite;
	sfTexture		*texture;
	t_my_framebuffer	*framebuffer;
	t_entity		entity;
	int			**map;

	if (argc < 2)
		return (84);
	map = parsing(argv[1]);
	set_pos(&entity, map);
	init_windows(&window, &sprite, &texture, &framebuffer);
	while (sfRenderWindow_isOpen(window))
	{
		sfRenderWindow_clear(window, sfBlack);
		event(window, &entity, map);
		draw_background(framebuffer);
		draw_map(framebuffer, map, entity);
		play(window, sprite, texture, framebuffer);
	}
	sfRenderWindow_destroy(window);
	return (0);
}

void			init_windows(sfRenderWindow **window,
					sfSprite **sprite,
					sfTexture **texture,
					t_my_framebuffer **framebuffer)
{
	sfVideoMode	mode;

	mode.width = SCREEN_WIDTH;
	mode.height = SCREEN_HEIGHT;
	mode.bitsPerPixel = BITS_PER_PIXEL;
	*window = sfRenderWindow_create (mode, "SFMLwindow",
			sfResize | sfClose, NULL);
	*sprite = sfSprite_create();
	*texture = sfTexture_create(SCREEN_WIDTH, SCREEN_HEIGHT);
	sfSprite_setTexture(*sprite, *texture, sfTrue);
	*framebuffer = my_framebuffer_create(SCREEN_WIDTH, SCREEN_HEIGHT);
}

void		play(sfRenderWindow *window, sfSprite *sprite,
			sfTexture *texture, t_my_framebuffer *framebuffer)
{
	sfTexture_updateFromPixels(texture, framebuffer->pixels,
			SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0);
	sfRenderWindow_drawSprite(window, sprite, NULL);
	sfRenderWindow_display(window);
}

void 		event(sfRenderWindow *window, t_entity *entity, int **map)
{
	sfEvent	event;

	while (sfRenderWindow_pollEvent(window, &event))
		if (event.type == sfEvtClosed)
			sfRenderWindow_close(window);
	if (sfKeyboard_isKeyPressed(sfKeyZ) == sfTrue)
		entity->player = my_move_forward(entity->player,
			entity->look + FOV / 2, MOVE_SPEED, map);
	if (sfKeyboard_isKeyPressed(sfKeyS) == sfTrue)
		entity->player = my_move_forward(entity->player,
			entity->look + FOV / 2, -MOVE_SPEED, map);
	if (sfKeyboard_isKeyPressed(sfKeyA) == sfTrue)
		entity->player = my_move_forward(entity->player,
			entity->look + FOV / 2 - 90, MOVE_SPEED, map);
	if (sfKeyboard_isKeyPressed(sfKeyE) == sfTrue)
		entity->player = my_move_forward(entity->player,
			entity->look + FOV / 2 - 90, -MOVE_SPEED, map);
	if (sfKeyboard_isKeyPressed(sfKeyQ) == sfTrue)
		entity->look -= TURN_SPEED;
	if (sfKeyboard_isKeyPressed(sfKeyD) == sfTrue)
		entity->look += TURN_SPEED;
	if (sfKeyboard_isKeyPressed(sfKeyEscape) == sfTrue)
		sfRenderWindow_close(window);
}
