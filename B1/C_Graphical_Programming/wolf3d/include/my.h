/*
** my.h for wolf3d in /home/infocraft/Job/En_Cours/wolf3d/include/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Dec 19 10:39:07 2016 Roulleau Julien
** Last update Tue Jan 10 15:56:01 2017 Roulleau Julien
*/

#ifndef _MY_H_

# define _MY_H_

# include <SFML/Graphics.h>
# include <unistd.h>
# include <stdlib.h>
# include <math.h>
# include <stdio.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>

# define SCREEN_WIDTH 1200
# define SCREEN_HEIGHT 600
# define BITS_PER_PIXEL 32
# define TURN_SPEED 4
# define MOVE_SPEED 0.1
# define FOV 60
# define WALL 150

# define IS_NBR(c) (c >= '0' && c <= '9')

# define ABS(nb) (nb < 0 ? nb * -1 : nb)
#define ROUND(x) ((int) (x + 0.5))

typedef struct		s_my_framebuffer
{
	sfUint8		*pixels;
	int		width;
	int		height;
}			t_my_framebuffer;

typedef struct		s_entity
{
	sfVector2f	player;
	sfVector2i	mapsize;
	int 		look;
}			t_entity;

/* window */
void		init_windows(sfRenderWindow **, sfSprite **,
			sfTexture **, t_my_framebuffer **);
void		play(sfRenderWindow *, sfSprite *,
			sfTexture *, t_my_framebuffer *);
void 		event(sfRenderWindow *, t_entity *, int **);

/* draw */
void 		draw_background(t_my_framebuffer *);
void 		draw_map(t_my_framebuffer *, int **, t_entity);
void 		draw_wall(t_my_framebuffer *, int, float);

/* parsing */
int		**parsing(char *);
void 		set_map(int ***, char *);
void 		make_map(int ***, char *);
void 		set_pos(t_entity *, int **);

/* src */
void 		my_put_pixel(t_my_framebuffer *, int, int, sfColor);
void		my_draw_line(t_my_framebuffer *, sfVector2i,
				sfVector2i, sfColor);
float		raycast(sfVector2f, float, int **, sfVector2i);
sfVector2f	move_forward(sfVector2f, float, float);
sfVector2f	my_move_forward(sfVector2f, float, float, int **);

/* aux */
t_my_framebuffer 	*my_framebuffer_create(int, int);
sfVector2i		pos2d(int, int);
sfColor			color_put(int, int, int, int);
int			my_getnbr(char *, int);

#endif /* _MY_H_ */
