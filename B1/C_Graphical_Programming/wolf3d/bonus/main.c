/*
** main.c for wolf3d in /home/infocraft/Job/En_Cours/wolf3d/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Dec 19 10:56:23 2016 Roulleau Julien
** Last update Fri Jan 27 12:37:52 2017 Roulleau Julien
*/

#include "my.h"

void	my_exit(t_texture *texture, sfRenderWindow *window)
{
	sfMusic_destroy(texture->music);
	sfMusic_destroy(texture->laser);
	sfRenderWindow_destroy(window);
}

int 				main(int argc, char **argv)
{
	sfRenderWindow		*window;
	t_texture		*texture;
	t_my_framebuffer	*framebuffer;
	t_entity		entity;
	int			**map;

	if (argc < 2)
		return (84);
	if ((map = parsing(argv[1])) == 0)
		return (84);
	set_pos(&entity, map);
	if (!(texture = malloc(sizeof(t_texture))))
		return (0);
	init_windows(&window, texture, &framebuffer);
	while (sfRenderWindow_isOpen(window))
	{
		sfRenderWindow_clear(window, sfBlack);
		event(window, &entity, *texture, map);
		draw_background(framebuffer);
		draw_map(framebuffer, map, entity);
		play(window, texture, framebuffer);
	}
	my_exit(texture, window);
	return (0);
}

void		play(sfRenderWindow *window, t_texture *texture,
			t_my_framebuffer *framebuffer)
{
	sfTexture_updateFromPixels(texture->texture, framebuffer->pixels,
			SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0);
	sfRenderWindow_drawSprite(window, texture->sprite, NULL);
	sfRenderWindow_drawSprite(window, texture->sp_weapon, NULL);
	sfRenderWindow_display(window);
}

void 		event(sfRenderWindow *window, t_entity *entity,
				t_texture texture, int **map)
{
	sfEvent	event;

	while (sfRenderWindow_pollEvent(window, &event))
	{
		if (event.type == sfEvtClosed)
			sfRenderWindow_close(window);
		if (event.type == sfEvtKeyPressed)
			if (event.key.code == sfKeySpace)
				broken_wall(*entity, texture, map);
	}
	if (map[ROUND(entity->player.y)][ROUND(entity->player.x)] == 4)
		sfRenderWindow_drawText(window, texture.win_text, NULL);
	control(window, entity, map);
}

void 		control(sfRenderWindow *window, t_entity *entity, int **map)
{
	if (sfKeyboard_isKeyPressed(sfKeyZ) == sfTrue)
		entity->player = my_move_forward(entity,
			entity->look + FOV / 2, MOVE_SPEED, map);
	if (sfKeyboard_isKeyPressed(sfKeyS) == sfTrue)
		entity->player = my_move_forward(entity,
			entity->look + FOV / 2, -MOVE_SPEED, map);
	if (sfKeyboard_isKeyPressed(sfKeyA) == sfTrue)
		entity->player = my_move_forward(entity,
			entity->look + FOV / 2 - 90, MOVE_SPEED, map);
	if (sfKeyboard_isKeyPressed(sfKeyE) == sfTrue)
		entity->player = my_move_forward(entity,
			entity->look + FOV / 2 - 90, -MOVE_SPEED, map);
	if (sfKeyboard_isKeyPressed(sfKeyQ) == sfTrue)
		entity->look -= TURN_SPEED;
	if (sfKeyboard_isKeyPressed(sfKeyD) == sfTrue)
		entity->look += TURN_SPEED;
	if (sfKeyboard_isKeyPressed(sfKeyEscape) == sfTrue)
		sfRenderWindow_close(window);
}
