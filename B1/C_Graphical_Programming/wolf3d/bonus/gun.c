/*
** gun.c for wolf3d in /home/infocraft/Job/En_Cours/wolf3d/bonus/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun Jan 15 22:26:30 2017 Roulleau Julien
** Last update Mon Jan 16 12:18:58 2017 Roulleau Julien
*/

#include "my.h"

void		broken_wall(t_entity entity, t_texture texture, int **map)
{
	(void) entity;
	(void) map;
	sfVector2f	vector;
	sfVector2f	pos_k;
	float 		rad;
	float 		k;

	k = 0;
	pos_k.x = entity.player.x;
	pos_k.y = entity.player.y;
	rad = (entity.look + FOV / 2) * (M_PI / 180);
	vector.x = cos(rad);
	vector.y = sin(rad);
	while (pos_k.y >= 0 && pos_k.y + 1 < entity.mapsize.y &&
			pos_k.x >= 0 && pos_k.x + 1 < entity.mapsize.x &&
			map[ROUND(pos_k.y)][ROUND(pos_k.x)] != 1 &&
			map[ROUND(pos_k.y)][ROUND(pos_k.x)] != 3)
	{
		k += 0.01;
		pos_k.x = entity.player.x + (k * vector.x);
		pos_k.y = entity.player.y + (k * vector.y);
	}
	sfMusic_play(texture.laser);
	if (map[ROUND(pos_k.y)][ROUND(pos_k.x)] == 3)
		map[ROUND(pos_k.y)][ROUND(pos_k.x)] = 0;
}
