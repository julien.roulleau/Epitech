/*
** draw.c for wolf3d in /home/infocraft/Job/En_Cours/wolf3d/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Dec 28 23:25:34 2016 Roulleau Julien
** Last update Thu Jan 26 16:19:25 2017 Roulleau Julien
*/

#include "my.h"

void 		draw_background(t_my_framebuffer *framebuffer)
{
	int 	dir;

	dir = -1;
	while (++dir < SCREEN_WIDTH)
	{
		my_draw_line(framebuffer,
			pos2d(dir, 0),
			pos2d(dir, SCREEN_HEIGHT / 2),
			color_put(60, 150, 220, 200));
		my_draw_line(framebuffer,
			pos2d(dir, SCREEN_HEIGHT / 2),
			pos2d(dir, SCREEN_HEIGHT),
			color_put(220, 130, 50, 150));
	}
}

void 		draw_map(t_my_framebuffer *framebuffer,
			int **map, t_entity entity)
{
	float	tmp;
	float 	dir;
	float	k;
	int	pos;

	tmp = (float) FOV / (float) SCREEN_WIDTH;
	dir = entity.look;
	pos = -1;
	while (++pos < SCREEN_WIDTH)
	{
		k = raycast(entity.player, dir, map, entity.mapsize);
		k *=  cos((dir - entity.look) * M_PI / (float) 180);
		draw_wall(framebuffer, pos, k);
		dir += tmp;
	}
	miniminimap(framebuffer, entity, map);
}

void 		draw_wall(t_my_framebuffer *framebuffer, int pos, float k)
{
	if (WALL / k > SCREEN_HEIGHT)
		my_draw_line(framebuffer,
			pos2d(pos, SCREEN_HEIGHT / 2 - (SCREEN_HEIGHT / 2)),
			pos2d(pos, SCREEN_HEIGHT / 2 + (SCREEN_HEIGHT / 2)),
			color_put(115, 130, 115, 200));
	else
		my_draw_line(framebuffer,
			pos2d(pos, SCREEN_HEIGHT / 2 - (WALL / k)),
			pos2d(pos, SCREEN_HEIGHT / 2 + (WALL / k)),
			color_put(115, 130, 115, 200));
}

void 			draw_square(t_my_framebuffer *framebuffer,
				sfVector2i pos, int size, sfColor color)
{
	sfVector2i	tmp;

	tmp = pos;
	while (++tmp.x <= pos.x + size)
		my_draw_line(framebuffer,
			pos2d(tmp.x, pos.y),
			pos2d(tmp.x, pos.y + size),
			color);
}
