/*
** my.h for wolf3d in /home/infocraft/Job/En_Cours/wolf3d/include/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Dec 19 10:39:07 2016 Roulleau Julien
** Last update Fri Jan 27 12:36:54 2017 Roulleau Julien
*/

#ifndef _MY_H_

# define _MY_H_

# include <SFML/Graphics.h>
# include <SFML/Audio.h>
# include <unistd.h>
# include <stdlib.h>
# include <math.h>
# include <stdio.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>

# define SCREEN_WIDTH 1200
# define SCREEN_HEIGHT 600
# define BITS_PER_PIXEL 32
# define TURN_SPEED 3
# define MOVE_SPEED 0.05
# define FOV 60
# define WALL 150

# define IS_NBR(c) (c >= '0' && c <= '9')

# define ABS(nb) (nb < 0 ? nb * -1 : nb)
#define ROUND(x) ((int) (x + 0.5))

typedef struct		s_my_framebuffer
{
	sfUint8		*pixels;
	int		width;
	int		height;
}			t_my_framebuffer;

typedef struct		s_entity
{
	sfVector2f	player;
	sfVector2i	mapsize;
	int 		look;
}			t_entity;

typedef struct		s_texture
{
	sfSprite 	*sprite;
	sfTexture	*texture;
	sfMusic 	*music;
	sfMusic 	*laser;
	sfFont		*font;
	sfText		*win_text;
	sfSprite 	*sp_weapon;
	sfTexture	*te_weapon;
}			t_texture;

/* window */
void		play(sfRenderWindow *, t_texture *, t_my_framebuffer *);
void 		event(sfRenderWindow *, t_entity *, t_texture, int **);
void 		control(sfRenderWindow *, t_entity *, int **);

/* init */
void		init_windows(sfRenderWindow **, t_texture *,
				t_my_framebuffer **);

/* draw */
void 		draw_background(t_my_framebuffer *);
void 		draw_map(t_my_framebuffer *, int **, t_entity);
void 		draw_wall(t_my_framebuffer *, int, float);

/* parsing */
int		**parsing(char *);
void 		set_map(int ***, char *);
void 		make_map(int ***, char *);
void 		set_pos(t_entity *, int **);
void		draw_square(t_my_framebuffer *, sfVector2i, int, sfColor);

/* gun */
void		broken_wall(t_entity, t_texture texture, int **);

/* minimap */
void		minimap(t_my_framebuffer *, t_entity, int **);
void		miniminimap(t_my_framebuffer *, t_entity, int **);
void		draw_miniwall(t_my_framebuffer *, sfVector2i,
				int **);
/* src */
void 		my_put_pixel(t_my_framebuffer *, int, int, sfColor);
void		my_draw_line(t_my_framebuffer *, sfVector2i,
				sfVector2i, sfColor);
float		raycast(sfVector2f, float, int **, sfVector2i);
sfVector2f	move_forward(sfVector2f, float, float);
sfVector2f	my_move_forward(t_entity *, float, float, int **);

/* aux */
t_my_framebuffer 	*my_framebuffer_create(int, int);
sfVector2i		pos2d(int, int);
sfColor			color_put(int, int, int, int);
int			my_getnbr(char *, int);

/* test */
int		test_map(char *);

#endif /* _MY_H_ */
