/*
** minimap.c for wolf3d in /home/infocraft/Job/En_Cours/wolf3d/bonus/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Jan 16 14:07:55 2017 Roulleau Julien
** Last update Mon Jan 16 17:42:05 2017 Roulleau Julien
*/

#include "my.h"

void			minimap(t_my_framebuffer *framebuffer,
					t_entity entity, int **map)
{
	sfVector2i	cord;
	sfVector2i	put;

	put.y = 0;
	cord.y = -1;
	draw_square(framebuffer, pos2d(10, 10),
		entity.mapsize.x > entity.mapsize.y ? entity.mapsize.x * 10 :
			entity.mapsize.y * 10, color_put(0, 0, 0, 100));
	while (map[++cord.y] && (cord.x = -1) && !(put.x = 0) && (put.y += 10))
		while (map[cord.y][++cord.x] != -1 && (put.x += 10))
		{
			draw_miniwall(framebuffer, cord, map);
			draw_square(framebuffer,
				pos2d(ROUND(entity.player.x) * 10 + 10,
					ROUND(entity.player.y) * 10 + 10),
				10, color_put(0, 0, 255, 100));
		}
}

void			miniminimap(t_my_framebuffer *framebuffer,
					t_entity entity, int **map)
{
	sfVector2i	cord;

	cord.y = ROUND(entity.player.x);
	cord.x = ROUND(entity.player.y);
	draw_square(framebuffer, pos2d(10, 10),
		entity.mapsize.x > entity.mapsize.y ? entity.mapsize.x * 10 :
			entity.mapsize.y * 10, color_put(0, 0, 0, 100));
	draw_miniwall(framebuffer, pos2d(cord.y - 1, cord.x - 1), map);
	draw_miniwall(framebuffer, pos2d(cord.y - 1, cord.x), map);
	draw_miniwall(framebuffer, pos2d(cord.y - 1, cord.x + 1), map);
	draw_miniwall(framebuffer, pos2d(cord.y, cord.x - 1), map);
	draw_square(framebuffer,
		pos2d(cord.y * 10 + 10,
			cord.x * 10 + 10),
		10, color_put(0, 0, 255, 100));
	draw_miniwall(framebuffer, pos2d(cord.y, cord.x + 1), map);
	draw_miniwall(framebuffer, pos2d(cord.y + 1, cord.x - 1), map);
	draw_miniwall(framebuffer, pos2d(cord.y + 1, cord.x), map);
	draw_miniwall(framebuffer, pos2d(cord.y + 1, cord.x + 1), map);
}

void		draw_miniwall(t_my_framebuffer *framebuffer, sfVector2i cord,
				int **map)
{
	if (map[cord.y][cord.x] == 1)
		draw_square(framebuffer,
			pos2d(cord.x * 10 + 10, cord.y * 10 + 10), 10,
			color_put(255, 0, 0, 100));
	if (map[cord.y][cord.x] == 3)
		draw_square(framebuffer,
			pos2d(cord.x * 10 + 10, cord.y * 10 + 10), 10,
			color_put(255, 255, 255, 100));
	if (map[cord.y][cord.x] == 4)
		draw_square(framebuffer,
			pos2d(cord.x * 10 + 10, cord.y * 10 + 10), 10,
			color_put(0, 255, 0, 100));
}
