/*
** move_forward.c for wolf3d in /home/infocraft/Job/En_Cours/wolf3d/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun Dec 18 22:30:32 2016 Roulleau Julien
** Last update Mon Dec 19 14:05:32 2016 Roulleau Julien
*/

#include "my.h"

sfVector2f		move_forward(sfVector2f pos,
					float direction, float distance)
{
	float		rad;

	rad = (direction * M_PI) / (float) 180;
	pos.x += distance * cos(rad);
	pos.y += distance * sin(rad);
	return (pos);
}
