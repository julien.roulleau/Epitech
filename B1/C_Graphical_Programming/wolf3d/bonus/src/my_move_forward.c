/*
** move_forward.c for wolf3d in /home/infocraft/Job/En_Cours/wolf3d/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun Dec 18 22:30:32 2016 Roulleau Julien
** Last update Fri Jan 27 12:41:18 2017 Roulleau Julien
*/

#include "my.h"

sfVector2f		my_move_forward(t_entity *entity, float direction,
				float distance, int **map)
{
	sfVector2f	pos;
	sfVector2f	mapsize;
	sfVector2f	new_pos;
	float		rad;

	mapsize.x = entity->mapsize.x - 1.5;
	mapsize.y = entity->mapsize.y - 1.5;
	pos.x = entity->player.x;
	pos.y = entity->player.y;
	new_pos.x = pos.x;
	new_pos.y = pos.y;
	rad = (direction * M_PI) / (float) 180;
	new_pos.x += distance * cos(rad);
	new_pos.y += distance * sin(rad);
	if (new_pos.y > 0.5 && pos.x > 0.5 && new_pos.y < mapsize.y &&
		map[ROUND(new_pos.y)][ROUND(pos.x)] != 1 &&
		map[ROUND(new_pos.y)][ROUND(pos.x)] != 3)
		pos.y = new_pos.y;
	if (pos.y > 0.5 && new_pos.x > 0.5 && new_pos.x < mapsize.x &&
		map[ROUND(pos.y)][ROUND(new_pos.x)] != 1 &&
		map[ROUND(pos.y)][ROUND(new_pos.x)] != 3)
		pos.x = new_pos.x;
	return (pos);
}
