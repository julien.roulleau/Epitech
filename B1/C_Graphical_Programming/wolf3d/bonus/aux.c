/*
** aux.c for wireframe in /home/infocraft/Job/En_Cours/wireframe/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Dec  5 11:04:24 2016 Roulleau Julien
** Last update Mon Jan 16 12:43:24 2017 Roulleau Julien
*/

#include "my.h"

t_my_framebuffer* 		my_framebuffer_create(int width, int height)
{
	t_my_framebuffer	*framebuffer;
	int			 p;

	p = -1;
	framebuffer = malloc(sizeof(t_my_framebuffer));
	framebuffer->pixels = malloc(sizeof(char) * 4 * width * height);
	framebuffer->width = width;
	framebuffer->height = height;
	while (++p < width * height * 4)
			framebuffer->pixels[p] = 0;
	return (framebuffer);
}

sfVector2i		pos2d(int x, int y)
{
	sfVector2i	vect;

	vect.x = x;
	vect.y = y;
	return (vect);
}

sfColor			color_put(int r, int g, int b, int a)
{
	sfColor 	color;

	color.r = r;
	color.g = g;
	color.b = b;
	color.a = a;
	return (color);

}

int	my_getnbr(char *str, int nb)
{
	if (*str == '+')
		return (my_getnbr(++str, nb));
	if (*str == '-')
		return (-my_getnbr(++str, nb));
	if (*str == '\0')
		return (nb);
	nb = (nb * 10) + (*str - '0');
	return (my_getnbr(++str, nb));
}
