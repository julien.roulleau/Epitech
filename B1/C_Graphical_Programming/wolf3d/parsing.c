/*
** parsing.c for wolf3d in /home/infocraft/Job/En_Cours/wolf3d/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Dec 22 14:03:15 2016 Roulleau Julien
** Last update Mon Jan 16 18:09:07 2017 Roulleau Julien
*/

#include "my.h"

int		**parsing(char *file)
{
	int	**map;
	char	*buffer;
	int	len;
	int 	fd;

	if (!(buffer = malloc(sizeof(char) * (1000))))
		return (0);
	if ((fd = open(file, O_RDONLY)) == -1)
			return (0);
	len = read(fd, buffer, 999);
	buffer[len] = 0;
	set_map(&map, buffer);
	make_map(&map, buffer);
	return (map);
}

void 		set_map(int ***map, char *buffer)
{
	int	count;
	int	line;
	int 	y;
	int 	i;

	i = -1;
	line = 0;
	while (buffer[++i])
		if (buffer[i] == '\n')
			line ++;
	if (!((*map) = malloc(sizeof(int *) * (line + 1))))
		return ;
	(*map)[line] = 0;
	i = y = count = -1;
	while (buffer[++i])
	{
		count++;
		if (buffer[i] == '\n')
		{
			if (!((*map)[++y] = malloc(sizeof(int) * (count + 1))))
				return ;
			(*map)[y][count] = 0;
			count = -1;
		}
	}
}

void 		make_map(int ***map, char *buffer)
{
	int	i;
	int	y;
	int	x;

	i = -1;
	x = -1;
	y = 0;
	while (buffer[++i])
	{
		if (IS_NBR(buffer[i]))
			(*map)[y][++x] = buffer[i] - '0';
		else if (buffer[i] == '\n')
		{
			(*map)[y][++x] = -1;
			y++;
			x = -1;
		}
	}
}

void 		set_pos(t_entity *entity, int **map)
{
	int 	y;
	int 	x;

	y = -1;
	entity->mapsize.x  = 0;
	entity->look = 0;
	while (map[++y])
	{
		x = -1;
		while (map[y][++x] != -1)
		{
			if (map[y][x] == 2)
			{
				entity->player.y = y + 0.5;
				entity->player.x = x + 0.5;
			}
		}
	if (entity->mapsize.x < x)
		entity->mapsize.x = x;
	}
entity->mapsize.y = y;
}
