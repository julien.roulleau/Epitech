/*
** env.c for 42sh in /home/infocraft/Job/En_Cours/PSU_2016_minishell1/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Jan 16 20:47:35 2017 Roulleau Julien
** Last update Fri Jan 20 12:45:19 2017 Roulleau Julien
*/

#include "my.h"

char		**my_env(char **param, char **env)
{
	int 	i;
	int 	n;

	i = -1;
	n = 1;
	if (param[1])
	{
		if (my_strcmp(param[1], "-i") && param[2])
			{
				while (param[++n])
					param[++i] = param[n];
				param[++i] = 0;
				env[0] = 0;
				my_exec(param, env);
			}
		else if (!my_strcmp(param[1], "-i"))
			my_putstr("Invalid flag\n");
		return (env);
	}
	while (env[++i])
	{
		my_putstr(env[i]);
		my_putstr("\n");
	}
	return (env);
}

static char	**addmalloc_env(char **env, char *name, char *value)
{
	char	**new_env;
	int	i;

	if (!(new_env = malloc(sizeof(char *) * (nb_param(env) + 3))))
		return (0);
	i = -1;
	while (env[++i])
		new_env[i] = env[i];
	new_env[i] = merge_str(merge_str(name, "="), value);
	new_env[++i] = 0;
	return (new_env);
}

char		**my_setenv(char **param, char **env)
{
	int	pos;
	int	nb;

	if (!(nb = nb_param(param)))
		my_env(param, env);
	else if (!(pos = find_name(env, param[1])))
	{
		if (nb == 1);
		else
			env[pos] = merge_str(
					merge_str(param[1], "="), param[2]);
	}
	else
	{
		if (nb == 1)
			env = addmalloc_env(env, param[1], "\0");
		else
			env = addmalloc_env(env, param[1], param[2]);
	}
	return (env);
}

static char	**lessmalloc_env(char **env, int pos)
{
	char	**new_env;
	int	i;
	int	n;

	n = -1;
	if (!(new_env = malloc(sizeof(char *) * (nb_param(env) + 1))))
		return (0);
	i = -1;
	while (env[++i])
		if (i != pos)
			new_env[++n] = env[i];
	new_env[++n] = 0;
	return (new_env);
}

char		**my_unsetenv(char **param, char **env)
{
	int	pos;

	if (nb_param(param) == 0)
		my_putstr("unsetenv: Too few arguments.\n");
	else if ((pos = find_name(env, param[1])) != -1)
		env = lessmalloc_env(env, pos);
	return (env);
}
