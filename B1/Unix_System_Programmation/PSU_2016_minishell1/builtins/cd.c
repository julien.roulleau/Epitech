/*
** cd.c for 42sh in /home/infocraft/Job/En_Cours/PSU_2016_minishell1/builtins/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Jan 18 18:48:57 2017 Roulleau Julien
** Last update Fri Jan 20 12:37:38 2017 Roulleau Julien
*/

#include "my.h"

#define NOT(fold) (my_putstr(fold), my_putstr(": No shuch file or directory\n"))
#define NOT_D(fold) (my_putstr(fold), my_putstr(": Not a directory\n"))
#define ROOT() (param[1][0] == '/' && !chdir(param[1]))
#define CURRENT() (!chdir(merge_str(merge_str(buffer, "/"), param[1])))

static char 	**set_pwd(char **env, char *buffer)
{
	char	**send;

	if (!(send = malloc(sizeof(char *) * (4))))
		return (0);
	send[0] = send[2] = buffer;
	send[1] = "PWD";
	send[3] = 0;
	env = my_setenv(send, env);
	free(send);
	return (env);
}

char			**my_cd(char **param, char **env)
{
	static char 	*last = 0;
	char		*buffer;
	int		home;
	int		pos;

	if (!(buffer = malloc (sizeof(char) * (1000))))
		return (0);
	buffer = getcwd(buffer, 999);
	if ((pos = find_name(env, "PWD")) == -1 && (env = set_pwd(env, buffer)))
		pos = find_name(env, "PWD");
	if (nb_param(param) > 0)
	{
		if (my_strcmp(param[1], "-") && last && !chdir(last + 4))
			env[pos] = my_strdup(last);
		else if (ROOT() && (last = my_strdup(env[pos])))
			env[pos] = merge_str("PWD=", getcwd(buffer, 4000));
		else if (CURRENT() && (last = my_strdup(env[pos])))
			env[pos] = merge_str("PWD=", getcwd(buffer, 4000));
		else (access(param[1], F_OK) ? NOT(param[1]) : NOT_D(param[1]));

	}
	else if ((home = find_name(env, "HOME")) != -1)
		if (!chdir(env[home] + 5) && (last = my_strdup(env[pos])))
			env[pos] = merge_str("PWD=", env[home] + 5);
	return (free(buffer), env);
}
