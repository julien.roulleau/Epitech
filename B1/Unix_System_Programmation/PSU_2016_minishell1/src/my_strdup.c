/*
** my_strdup.c for src in /home/infocraft/Job/Lib/my_printf/src/str/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun Jan  8 23:15:19 2017 Roulleau Julien
** Last update Wed Jan 18 00:25:15 2017 Roulleau Julien
*/

#include <stdlib.h>

int		my_strlen(char *);

char		*my_strdup(char *src)
{
	char	*str;
	int	i;

	str = malloc(sizeof(char) * (my_strlen(src) + 1));
	i = -1;
	while (src[++i])
		str[i] = src[i];
	str[i] = 0;
	return (str);
}
