/*
** aux.c for 42sh in /home/infocraft/Job/En_Cours/PSU_2016_minishell1/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Jan 16 22:18:02 2017 Roulleau Julien
** Last update Wed Jan 18 23:49:33 2017 Roulleau Julien
*/

#include "my.h"

int		find_name(char **env, char *name)
{
	int	i;

	i = -1;
	while (env[++i])
		if (my_strcmp(name, name_to_str(env[i])))
			return (i);
	return (-1);
}

char		*name_to_str(char *str)
{
	char 	*new;
	int	i;

	i = -1;
	while (str[++i] && str[i] != '=');
	new = malloc(sizeof(char) * (i + 1));
	new[i] = 0;
	i = -1;
	while (str[++i] && str[i] != '=')
		new[i] = str[i];
	return (new);
}

int		nb_param(char **param)
{
	int	i;

	i = -1;
	while (param[++i]);
	return (i - 1);
}
