/*
** my_exec.c for 42sh in /home/infocraft/Job/En_Cours/PSU_2016_my_exec/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Jan  5 11:04:42 2017 Roulleau Julien
** Last update Sun Jan 22 23:40:54 2017 Roulleau Julien
*/

#include "my.h"

static char 	*set_param(char **param, char **env)
{
	char	**path;
	int 	i;

	i = 0;
	if (param[0][0] == '.' && param[0][1] == '/')
	{
		if (access(param[0], F_OK))
			return (0);
	}
	else
	{
		path = str_to_wordtab(env[find_name(env, "PATH")] + 5, ':');
		while (path[++i] &&
			access(merge_str(merge_str(path[i], "/"),
						param[0]), F_OK) &&
			access(merge_str(merge_str(path[i], "/"),
						param[0]), X_OK));
		if (!path[i])
			return (0);
		param[0] = merge_str(merge_str(path[i], "/"), param[0]);
	}
	return (param[0]);
}

int		my_exec(char **param, char **env)
{
	char	*tmp;
	int	status;
	int	wait;
	int	pid;

	if (env)
	{
		if (!(tmp = set_param(param, env)))
			return (-1);
		param[0] = tmp;
	}
	pid = fork();
	if (pid == 0 && execve(param[0], param, env) == -1)
		exit(-1);
	else if (pid > 0)
	{
		wait = waitpid(pid, &status, WUNTRACED | WCONTINUED);
		if (wait == -1)
			return (84);
		print_status(status);
	}
	else
		return (84);
	return (0);
}

void 		print_status(int status)
{
	if (WIFEXITED(status) && WEXITSTATUS(status) == 0);
	else if (WIFSIGNALED(status) && WTERMSIG(status) == 11)
		my_putstr("Segmentation Fault\n");
	else if (WIFCONTINUED(status))
		my_putstr("continued\n");
	else
		my_putstr("?\n");
}
