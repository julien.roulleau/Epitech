/*
** mysh.c for 42sh in /home/infocraft/Job/En_Cours/PSU_2016_minishell1/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Jan 16 18:58:27 2017 Roulleau Julien
** Last update Thu Jan 19 01:01:07 2017 Roulleau Julien
*/

#include "my.h"

t_flag		g_flag[] =
{
	{"cd", &my_cd},
	{"env", &my_env},
	{"setenv", &my_setenv},
	{"unsetenv", &my_unsetenv},
	{NULL, NULL}
};

void		mysh(char **param, char ***env)
{
	int	f;

	f = -1;
	while (g_flag[++f].flag != NULL && !my_strcmp(param[0], g_flag[f].flag));
	if (g_flag[f].flag == NULL)
	{
		if ((my_exec(param, *env)) == -1)
		{
			my_putstr(param[0]);
			my_putstr(": Command not found.\n");
		}
	}
	else
		*env = g_flag[f].flag_pointer(param, *env);
}
