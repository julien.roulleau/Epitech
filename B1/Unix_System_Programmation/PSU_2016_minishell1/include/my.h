/*
** my.h for 42sh in /home/infocraft/Job/En_Cours/PSU_2016_my_exec/include/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Jan  5 11:03:38 2017 Roulleau Julien
** Last update Sun Jan 22 23:33:53 2017 Roulleau Julien
*/

#ifndef MY_H
# define MY_H

# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include <string.h>
# include <sys/types.h>
# include <sys/wait.h>
# include <signal.h>
# include <fcntl.h>
#include <dirent.h>

typedef void 	(*handler_t)(int);
handler_t 	signal (int, handler_t);

typedef struct 		s_flag
{
	char 		*flag;
	char 		**(*flag_pointer)(char **, char **);
}			t_flag;

/* my_exec */
int		my_exec(char **, char **);
void 		print_info(char **);
void 		print_status(int);

/* my_exit */
int		my_exit(char *);

/* mysh */
void		mysh(char **, char ***);

/* builtins */
char		**my_cd(char **, char **);

/* env */
char		**my_env(char **, char **);
char		**my_setenv(char **, char **);
char		**my_unsetenv(char **, char **);

/* aux */
int		find_name(char **, char *);
char		*name_to_str(char *);
int		nb_param(char **);

/* src */
char		**str_to_wordtab(char *, char);
int		my_strcmp(char *, char *);
int		my_putstr(char *);
void 		my_putnbr(int);
char		*get_next_line(const int);
int		my_strlen(char *);
char		*my_strdup(char *);
char		*merge_str(char *, char *);
int		my_getnbr(char *);

#endif /* MY_H */
