/*
** main.c for 42sh in /home/infocraft/Job/En_Cours/PSU_2016_my_exec/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Jan  5 11:01:15 2017 Roulleau Julien
** Last update Sun Jan 22 23:38:05 2017 Roulleau Julien
*/

#include "my.h"

/* TODO: améliorer prompt custom */
/* TODO: retour de execve et les signaux*/
/* TODO: fix parametre avec plein d'espace et de tab*/
/* TODO: setenv, que des variable alpha numérique commençant par une lettre*/

static int	print_prompt()
{
	char	*pwd;

	pwd = malloc(sizeof(char) * (1000));
	pwd = getcwd(pwd, 999);
	my_putstr(pwd);
	my_putstr(" § ");
	return (1);
}

int 		main(int argc, char **argv, char **env)
{
	char	**param;
	char	*run;

	(void) argc;
	(void) argv;
	while (print_prompt() && (run = get_next_line(0)))
	{
		param = str_to_wordtab(run, ' ');
		if (my_strcmp(param[0], ""));
		else if (my_strcmp(param[0], "exit"))
		{
			write(1, "exit\n", 5);
			if (param[1])
				return (my_getnbr(param[1]) % 512);
			return (0);
		}
		else
			mysh(param, &env);
	}
	write(1, "exit\n", 5);
	return (0);
}
