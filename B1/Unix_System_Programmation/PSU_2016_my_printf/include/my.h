/*
** my.h for my_printf in /home/infocraft/Job/PSU_2016_my_printf/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Nov 15 11:27:49 2016 Roulleau Julien
** Last update Sun Nov 20 21:53:39 2016 Roulleau Julien
*/

#ifndef _MY_H_

# define _MY_H_

# include <stdio.h>
# include <stdarg.h>
# include <unistd.h>
# include <stdlib.h>

typedef struct 		s_list
{
	char 		*flag;
	int 		(*flag_pointer)(va_list);
}			t_list;

/*my_printf.c*/
int 	my_printf(const char *, ...);
int 	invalid_flag(char);
int 	print_text(char);

/*aux.c*/
int	my_putstr(char *);
int	my_strlen(char *);
int	my_nbrlen(int);
int	my_put_nbr(int);
int	my_putstr_s(char *);
char	*my_strdup(char *);
int	my_put_nbr_base(int, char *);
int	my_put_nbr_base_unsigned(unsigned int, char *);
int	my_put_nbr_base_pointer(unsigned long int, char *);
int	my_put_nbr_base_binary(int, char *);

/*print_type.c*/
int	my_print_char(va_list);
int	my_print_str(va_list);
int	my_print_percent(va_list);
int	my_print_str_s(va_list);
int	my_print_base_decimal(va_list);
int	my_print_base_hexdown(va_list);
int	my_print_base_hexup(va_list);
int	my_print_base_octal(va_list);
int	my_print_base_binary(va_list);
int	my_print_base_unsigned(va_list);
int	my_print_base_pointer(va_list);

#endif		/* !_MY_H_ */
