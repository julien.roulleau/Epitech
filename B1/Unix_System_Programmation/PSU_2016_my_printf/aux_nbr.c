/*
** aux2.c for my_printf in /home/infocraft/Job/PSU_2016_my_printf/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Nov 16 18:57:30 2016 Roulleau Julien
** Last update Thu Nov 17 11:32:33 2016 Roulleau Julien
*/

#include "my.h"

int		my_put_nbr_base(int nb, char *base)
{
	int	i;
	int	div_max;
	int	len_base;
	int	count;

	count = 0;
	div_max = 1;
	len_base = my_strlen(base) + 1;
	if (nb < 0)
		{
			write(1, "-", 1);
			count++;
			nb *= -1;
		}
	while (nb / div_max > len_base - 1)
		div_max *= len_base;
	while (div_max > 0)
	{
		i = nb / div_max % len_base;
		div_max /= len_base;
		write(1, &base[i], 1);
		count++;
	}
	return (count);
}

int		my_put_nbr_base_unsigned(unsigned int nb, char *base)
{
	int	i;
	int	div_max;
	int	len_base;
	int	count;

	count = 0;
	div_max = 1;
	len_base = my_strlen(base) + 1;
	while (nb / div_max > len_base - 1)
		div_max *= len_base;
	while (div_max > 0)
	{
		i = nb / div_max % len_base;
		div_max /= len_base;
		write(1, &base[i], 1);
		count++;
	}
	return (count);
}

int		my_put_nbr_base_pointer(unsigned long int nb, char *base)
{
	int	i;
	int	div_max;
	int	len_base;
	int	count;

	count = 0;
	div_max = 1;
	len_base = my_strlen(base) + 1;
	while (nb / div_max > len_base - 1)
		div_max *= len_base;
	while (div_max > 0)
	{
		i = nb / div_max % len_base;
		div_max /= len_base;
		write(1, &base[i], 1);
		count++;
	}
	return (count);
}

int		my_nbrlen(int nb)
{
	int 	count;

	count = 0;
	while (nb > 0)
	{
		nb /= 10;
		count++;
	}
	return (count);
}

int		my_put_nbr(int nb)
{
	char	i;
	int	div_max;
	int 	count;

	div_max = 1;
	count = 0;
	while (nb / div_max > 9)
		div_max *= 10;
	while (div_max > 0)
	{
		i = nb / div_max % 10 + 48;
		div_max /= 10;
		write(1, &i, 1);
		count++;
	}
	return (count);
}
