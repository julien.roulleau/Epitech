/*
** print_type.c for my_printf in /home/infocraft/Job/PSU_2016_my_printf/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Nov 16 14:52:29 2016 Roulleau Julien
** Last update Sun Nov 20 21:44:31 2016 Roulleau Julien
*/

#include "my.h"

int		my_print_char(va_list va)
{
	char 	c;

	c = va_arg(va, int);
	write(1, &c, 1);
	return (1);
}

int		my_print_str(va_list va)
{
	char 	*str;
	int	count;

	str = va_arg(va, char *);
	if (str == NULL)
	{
		write(1, "(null)", 6);
		count = 6;
	}
	else
		count = my_putstr(str);
	return (count);
}

int		my_print_percent(va_list va)
{
	write(1, "%", 1);
	return (1);
}

int		my_print_str_s(va_list va)
{
	char 	*str;

	str = va_arg(va, char *);
	if (str == NULL)
		return (0);
	return (my_putstr_s(str));
}
