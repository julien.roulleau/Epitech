/*
** main.c for my_ls in /home/infocraft/Job/En_Cours/PSU_2016_my_ls/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Nov 30 18:48:18 2016 Roulleau Julien
** Last update Fri Dec  9 14:49:02 2016 Roulleau Julien
*/

#include "my.h"

int 		main(int argc, char **argv)
{
	t_flag	*flag;

	if (!(flag = malloc(sizeof(t_flag))))
		exit(84);
	init_flag(flag);
	if (argc == 1)
		my_ls("./", flag);
	else
	{
		flag = set_flag(argv, flag);
		if (argv[last_argv(argv)][0] == '-')
			my_ls("./", flag);
		else
			my_ls(argv[last_argv(argv)], flag);
	}
	return (0);
}

int		last_argv(char **argv)
{
	int	i;

	i = -1;
	while (argv[++i]);
	return (i - 1);
}

void 	init_flag(t_flag *flag)
{
	flag->l = 0;
	flag->r = 0;
	flag->d = 0;
	flag->rr = 0;
	flag->t = 0;
	flag->a = 0;
}
