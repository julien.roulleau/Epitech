/*
** my_ls_flag.c for my_ls in /home/infocraft/Job/En_Cours/PSU_2016_my_ls/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Dec  2 10:20:56 2016 Roulleau Julien
** Last update Sun Dec  4 18:47:35 2016 Roulleau Julien
*/

#include "my.h"

t_flag		*set_flag(char **argv, t_flag *flag)
{
	int 	n;
	int 	i;

	i = 0;
	while (argv[++i + 1])
	{
		n = -1;
		while (argv[i][++n])
		{
			if (argv[i][n] == 'l')
				flag->l = 1;
			else if (argv[i][n] == 'r')
				flag->r = 1;
			else if (argv[i][n] == 'd')
				flag->d = 1;
			else if (argv[i][n] == 'R')
				flag->rr = 1;
			else if (argv[i][n] == 't')
				flag->t = 1;
			else if (argv[i][n] == 'a')
				flag->a = 1;
		}
	}
	return (flag);
}

int		my_ls_no(t_list *list, t_flag *flag)
{
	print_my_ls(flag, list);
	while ((list = list->next) && !(list->root))
		print_my_ls(flag, list);
	return (0);
}

int			my_ls_l(t_list *list)
{
	my_printf("%c", list->type);
	my_printf("%s ", list->permission);
	my_printf("%i ", list->link);
	my_printf("%s ", list->user);
	my_printf("%s ", list->group);
	my_printf("%i ", list->size);
	my_printf("%s ", list->modif);
	return (0);
}

int		my_ls_r(t_flag *flag, t_list *list)
{
	list = list->previous;
	print_my_ls(flag, list);
	while ((list = list->previous) && !(list->root))
		print_my_ls(flag, list);
	if (list->name[0] != '.')
		print_my_ls(flag, list);
	return (0);
}

int		my_ls_d(t_flag *flag, t_list *list)
{
	while ((list = list->next) && !(list->root))
		if (list->name[0] == '.' && list->name[1] == '\0')
		{
			if (flag->l)
				my_ls_l(list);
			my_printf("%s\n", list->name);
		}
	return (0);
}
