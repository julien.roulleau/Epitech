/*
** sort.c for mt_ls in /home/infocraft/Job/En_Cours/PSU_2016_my_ls/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Nov 30 21:42:26 2016 Roulleau Julien
** Last update Sun Dec  4 18:42:14 2016 Roulleau Julien
*/

#include "my.h"
#include <stdio.h>

t_list 		*sort_list(t_list *list)
{
	t_list	*sort;
	t_list	*small;

	small = find_small(list);
	sort = init_list(small->name);
	remove_node(&small);
	while (list != list->next)
	{
		small = find_small(list);
		add_node(sort, small->name);
		remove_node(&small);
	}
	return (sort);
}

t_list		*find_small(t_list *list)
{
	t_list	*small;

	small = list;
	while ((list = list->next) && !(list->root))
		if (my_strcmp(small->name, list->next->name))
			small = list->next;
	return (small);
}

int		my_strcmp(char *first, char *second)
{
	int 	f;
	int 	s;

	f = -1;
	s = -1;
	while (first[++f] && second[++s] && first[f] == second[s]);
	if (first[f] > second[s])
		return (1);
	else if (first[f] < second[s])
		return (-1);
	else
		return (0);
}
