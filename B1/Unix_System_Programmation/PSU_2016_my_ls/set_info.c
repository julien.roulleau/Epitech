/*
** set_info.c for my_ls in /home/infocraft/Job/En_Cours/PSU_2016_my_ls/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun Dec  4 16:01:56 2016 Roulleau Julien
** Last update Sun Dec  4 19:16:04 2016 Roulleau Julien
*/

#include "my.h"

char		**find_dir(t_list *list, char *loc)
{
	char 	**dir;
	int	count;
	int	i;

	count = 0;
	i = 0;
	while ((list = list->next) && !(list->root))
		if (list->type == 'd')
			count++;
	dir = malloc(sizeof(char *) * (count - 1));
	while ((list = list->next) && !(list->root))
		if (list->type == 'd' && my_strcmp(list->name, loc) != 0)
			dir[i++] = list->loc;
	return (dir);
}

void 		set_name(char name[FILENAME_MAX],
			struct dirent *file, char *loc)
{
	int	i;
	int 	n;

	i = -1;
	n = -1;
	while (++i < FILENAME_MAX)
		name[i] = '\000';
	i = -1;
	while (loc[++n])
		name[++i] = loc[n];
	if (loc[n - 1] != '/')
		name[++i] = '/';
	n = -1;
	while (file->d_name[++n])
		name[++i] = file->d_name[n];
}

char		*set_permission(struct stat fstat)
{
	char	*permission;

	permission = malloc(sizeof(char) * 10);
	permission[0] = (fstat.st_mode & S_IRUSR) ? 'r' : '-';
	permission[1] = (fstat.st_mode & S_IWUSR) ? 'w' : '-';
	permission[2] = (fstat.st_mode & S_IXUSR) ? 'x' :
		(fstat.st_mode & S_ISUID) ? 's' : '-';
	permission[3] = (fstat.st_mode & S_IRGRP) ? 'r' : '-';
	permission[4] = (fstat.st_mode & S_IWGRP) ? 'w' : '-';
	permission[5] = (fstat.st_mode & S_IXGRP) ? 'x' :
		(fstat.st_mode & S_ISGID) ? 'S' : '-';
	permission[6] = (fstat.st_mode & S_IROTH) ? 'r' : '-';
	permission[7] = (fstat.st_mode & S_IWOTH) ? 'w' : '-';
	permission[8] = (fstat.st_mode & S_IXOTH) ? 'x' :
		(fstat.st_mode & S_ISVTX) ? 't' : '-';
	permission[9] = '\0';
	return (permission);
}

char		set_type(struct stat fstat)
{
	if (S_ISREG(fstat.st_mode))
		return ('-');
	else if (S_ISDIR(fstat.st_mode))
		return ('d');
	else if (S_ISCHR(fstat.st_mode))
		return ('c');
	else if (S_ISBLK(fstat.st_mode))
		return ('b');
	else if (S_ISFIFO(fstat.st_mode))
		return ('i');
	else if (S_ISLNK(fstat.st_mode))
		return ('l');
	else if (S_ISSOCK(fstat.st_mode))
		return ('s');
	return ('-');
}

char		*set_modif(struct stat fstat)
{
	char	*modif;
	char	*t;
	int	i;

	modif = ctime(&fstat.st_mtime) + 4;
	i = my_strlen(modif);
	t = modif + i - 4;
	if (t[0] == '2' && t[1] == '0' && t[2] == '1' && t[3] == '6')
		modif[i - 8] = '\0';
	else
	{
		modif[i - 13] = t[0];
		modif[i - 12] = t[1];
		modif[i - 11] = t[2];
		modif[i - 10] = t[3];
		modif[i - 9] = '\0';
	}
	return (modif);
}
