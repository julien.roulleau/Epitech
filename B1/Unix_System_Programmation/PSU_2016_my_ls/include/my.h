/*
** my.h for push_swap in /home/infocraft/Job/CPE_2016_Pushswap/include/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Nov 21 17:10:48 2016 Roulleau Julien
** Last update Sun Dec  4 18:29:33 2016 Roulleau Julien
*/

#ifndef _MY_H_

# define _MY_H_

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>
#include <stdio.h>

typedef struct 		s_list
{
	int		root;
	char 		*name;
	struct s_list	*next;
	struct s_list	*previous;
	char		loc[FILENAME_MAX];
	int		block;
	char 		type;
	char		*permission;
	int		link;
	char		*user;
	char		*group;
	int 		size;
	char 		*modif;
}			t_list;

typedef struct		s_flag
{
	int		l;
	int		r;
	int		d;
	int		rr;
	int		t;
	int		a;
}			t_flag;

/* main */
int		last_argv(char **);
void 		init_flag(t_flag *);

/* my_ls */
int		my_ls(char *, t_flag *);
int 		print_my_ls(t_flag *, t_list *);
t_list		*create_list(char *);
int 		put_in_list(t_list *, char *, struct dirent *);
int 		print_total(t_list *, t_flag *);

/* set_info */
void 		set_name(char *, struct dirent *, char *);
char		*set_permission(struct stat);
char 		set_type(struct stat);
char		*set_modif(struct stat);
char		**find_dir(t_list *, char *);

/* my_ls_flag */
t_flag		*set_flag(char **, t_flag *);
int		my_ls_no(t_list *, t_flag *);
int		my_ls_l(t_list *);
int		my_ls_r(t_flag *, t_list *);
int		my_ls_d(t_flag *, t_list *);

/* list */
t_list		*init_list(char *);
int		add_node(t_list *, char *);
int 		remove_node(t_list **);

/* lib */
int		my_strlen(char *);
int		my_printf(char *, ...);

/* sort */
t_list 		*sort_list(t_list *);
t_list		*find_small(t_list *);
int		my_strcmp(char *, char *);

#endif /* _MY_H_ */
