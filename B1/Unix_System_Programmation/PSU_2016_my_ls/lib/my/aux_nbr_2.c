/*
** aux.c for push_swap in /home/infocraft/Job/CPE_2016_Pushswap/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Nov 21 17:08:25 2016 Roulleau Julien
** Last update Thu Nov 24 19:22:00 2016 Roulleau Julien
*/

#include "my.h"

int	my_getnbr(char *str, int nb)
{
	if (*str == '-')
		return (-my_getnbr(++str, nb));
	if (*str == '\0')
		return (nb);
	nb = (nb * 10) + (*str - '0');
	return (my_getnbr(++str, nb));
}
