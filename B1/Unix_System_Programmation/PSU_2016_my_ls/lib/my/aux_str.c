/*
** aux.c for my_printf in /home/infocraft/PSU_2016_my_printf_bootstrap/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Nov 10 18:49:12 2016 Roulleau Julien
** Last update Thu Dec  1 16:49:24 2016 Roulleau Julien
*/

#include "my.h"

int		my_putstr(char *str)
{
	if (*str != '\0' && write(1, &(*str), 1))
		my_putstr(++str);
	return (my_strlen(str) - 4);
}

int		my_putstr_s(char *str)
{
	int 	i;
	int 	count;

	i = -1;
	count = 0;
	while (str[++i])
	{
		if (str[i] < 32)
		{
			write(1, "\\0", 2);
			if (str[i] < 10)
			{
				write(1, "0", 1);
				my_put_nbr(str[i]);
				count ++;
			}
			else
				my_put_nbr(str[i]);
			count++;
		}
		else
			write(1, &str[i], 1);
		count++;
	}
	return (count);
}

int		my_strlen(char *str)
{
	int 	i;

	i = -1;
	while (str[++i]);
	return (i - 1);
}

char		*my_strdup(char *src)
{
	char	*str;
	int	i;

	str = malloc(sizeof(char) * (my_strlen(src) + 1));
	i = -1;
	while (src[++i])
		str[i] = src[i];
	str[i] = 0;
	return (str);
}
