/*
** print_type_base.c for my_printf in /home/infocraft/Job/PSU_2016_my_printf/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Nov 16 20:59:08 2016 Roulleau Julien
** Last update Sun Nov 20 21:45:05 2016 Roulleau Julien
*/

#include "my.h"

int		my_print_base_decimal(va_list va)
{
	int	nb;

	nb = va_arg(va, int);
	nb = my_put_nbr_base(nb, "0123456789");
	return (nb);
}

int		my_print_base_hexdown(va_list va)
{
	int	nb;

	nb = va_arg(va, int);
	nb = my_put_nbr_base_unsigned(nb, "0123456789abcdef");
	return (nb);
}

int		my_print_base_hexup(va_list va)
{
	int	nb;

	nb = va_arg(va, int);
	nb = my_put_nbr_base_unsigned(nb, "0123456789ABCDEF");
	return (nb);
}

int		my_print_base_octal(va_list va)
{
	int	nb;

	nb = va_arg(va, int);
	nb = my_put_nbr_base(nb, "01234567");
	return (nb);
}

int		my_print_base_binary(va_list va)
{
	int	nb;

	nb = va_arg(va, int);
	nb = my_put_nbr_base(nb, "01");
	return (nb);
}
