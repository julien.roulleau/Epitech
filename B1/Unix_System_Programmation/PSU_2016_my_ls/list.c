/*
** list.c for my_ls in /home/infocraft/Job/En_Cours/PSU_2016_my_ls/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Nov 30 20:08:58 2016 Roulleau Julien
** Last update Thu Dec  1 19:28:18 2016 Roulleau Julien
*/

#include "my.h"

t_list		*init_list(char *name)
{
	t_list	*list;

	if (!(list = malloc(sizeof(t_list))))
		return (NULL);
	list->name = name;
	list->next = list;
	list->previous = list;
	list->root = 1;
	return (list);
}

int		add_node(t_list *list, char *name)
{
	t_list	*node;

	if (!(node = malloc(sizeof(t_list))))
		return (84);
	node->root = 0;
	node->name = name;
	node->previous = list->previous;
	node->next = list;
	list->previous->next = node;
	list->previous = node;
	return (0);
}

int 		remove_node(t_list **list)
{
	t_list *tmp;

	// if ((*list) == (*list)->next)
	// {
	// 	free(*list);
	// 	(*list) = NULL;
	// }
	// else
	// {
		tmp = (*list)->next;
		if ((*list)->root)
			(*list)->next->root = 1;
		(*list)->previous->next = (*list)->next;
		(*list)->next->previous = (*list)->previous;
		free(*list);
		(*list) = tmp;
	// }
	return (0);
}
