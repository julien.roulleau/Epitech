/*
** my_ls.c for my_ls in /home/infocraft/Job/En_Cours/PSU_2016_my_ls/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Nov 30 18:52:21 2016 Roulleau Julien
** Last update Sun Dec  4 18:45:12 2016 Roulleau Julien
*/

#include "my.h"

int		my_ls(char *loc, t_flag *flag)
{
	t_list	*list;

	list = create_list(loc);
	if (flag->l && !(flag->d))
		print_total(list, flag);
	if (flag->d)
		my_ls_d(flag, list);
	else if (flag->r)
		my_ls_r(flag, list);
	else
		my_ls_no(list, flag);
	return (0);
}

int 		print_total(t_list *list, t_flag *flag)
{
	int	block;

	block = 0;
	while ((list = list->next) && !(list->root))
		if (flag->a)
			block += list->block;
		else if (list->name[0] != '.')
			block += list->block;
	my_printf("total %d\n", block / 2 + 4);
	return (0);
}

int 		print_my_ls(t_flag *flag, t_list *list)
{
	if (flag->a)
	{
		if (flag->l)
			my_ls_l(list);
		my_printf("%s\n", list->name);
	}
	else if (list->name[0] != '.')
	{
		if (flag->l)
			my_ls_l(list);
		my_printf("%s\n", list->name);
	}
	return (0);
}

t_list			*create_list(char *loc)
{
	DIR 		*dir;
	struct dirent 	*file;
	t_list		*list;

	if (!(dir = opendir(loc)))
		exit(84);
	if (!(file = readdir(dir)))
		exit(84);
	list = init_list(file->d_name);
	put_in_list(list, loc, file);
	while ((file = readdir(dir)))
	{
		add_node(list, file->d_name);
		put_in_list(list->previous, loc, file);
	}
	return (list);
}

int 		put_in_list(t_list *list, char *loc, struct dirent *file)
{
	struct stat 	fstat;
	struct passwd	*user;
	struct group	*group;

	set_name(list->loc, file, loc);
	if (stat(list->loc, &fstat) == -1)
		exit(84);
	list->block = fstat.st_blocks;
	list->type = set_type(fstat);
	list->permission = set_permission(fstat);
	list->link = fstat.st_nlink;
	user = getpwuid(fstat.st_uid);
	list->user = user->pw_name;
	group = getgrgid(fstat.st_gid);
	list->group = group->gr_name;
	list->size = fstat.st_size;
	list->modif = set_modif(fstat);
	return (0);
}
