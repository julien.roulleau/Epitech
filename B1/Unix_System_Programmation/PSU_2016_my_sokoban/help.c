/*
** help.c for my_sokoban in /home/infocraft/Job/En_Cours/PSU_2016_my_sokoban/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Dec 14 11:54:35 2016 Roulleau Julien
** Last update Thu Dec 15 10:11:40 2016 Roulleau Julien
*/

#include "my.h"

void 		print_help()
{
	PRINT("USAGE\n");
	PRINT("\t./my_sokoban map\n");
	PRINT("\n");
	PRINT("DESCRIPTION\n");
	PRINT("\tmap\tfile representing the warehouse map,");
	PRINT(" containing '#' for walls,\n");
	PRINT("\t\t'P' for the player, 'X' for boxes");
	PRINT(" and 'O' for storage locations.\n");
}
