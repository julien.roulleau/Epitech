/*
** list.c for my_sokoban in /home/infocraft/Job/En_Cours/PSU_2016_my_sokoban/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Dec 15 21:22:55 2016 Roulleau Julien
** Last update Thu Dec 15 21:54:31 2016 Roulleau Julien
*/

#include "my.h"

void 		list_area(t_info *info, int y, int x)
{
	t_entity	*area;

	if (!(area = malloc(sizeof(t_entity))))
		exit(84);
	area->y = y;
	area->x = x;
	if (info->area == NULL)
	{
		area->next = NULL;
		info->area = area;
	}
	else
	{
		area->next = info->area;
		info->area = area;
	}
}

void 		list_box(t_info *info, int y, int x)
{
	t_entity	*box;

	if (!(box = malloc(sizeof(t_entity))))
		exit(84);
	box->y = y;
	box->x = x;
	if (info->box == NULL)
	{
		box->next = NULL;
		info->box = box;
	}
	else
	{
		box->next = info->box;
		info->box = box;
	}
}

void 		free_info(t_info *info)
{
	if (info->area != NULL)
		free_entity(info->area);
	free(info);
}

void 		free_entity(t_entity *entity)
{
	if (entity->next != NULL)
		free_entity(entity->next);
	free(entity);
}
