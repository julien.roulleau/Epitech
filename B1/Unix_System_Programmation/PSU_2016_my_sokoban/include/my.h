/*
** my.h for my_popup in /home/infocraft/Job/En_Cours/PSU_2016_my_popup/include/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Dec  5 23:32:45 2016 Roulleau Julien
** Last update Fri Dec 16 18:14:20 2016 Roulleau Julien
*/

#ifndef _MY_H_
# define _MY_H_

# include <curses.h>
# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>

# define STRLEN(str) (sizeof(str) / sizeof(str[0]) - 1)
# define PRINT(str) (write(1, str, STRLEN(str)))

# define DIR(pos, yes, no) (cord == pos ? value * yes : no)
# define POS(pos, yes, no) (info->p[pos] + DIR(pos, yes, no))
# define POS_MAP(yes, no) [POS(0, yes, no)][POS(1, yes, no)]

# define TO_X(add, c) (map[box->y][box->x + add] == c)
# define TO_Y(add, c) (map[box->y + add][box->x] == c)
# define X_AND_X(add) (TO_X(add, 'X') || TO_X(add, '#') || TO_X(add, 'A'))
# define Y_AND_Y(add) (TO_Y(add, 'X') || TO_Y(add, '#') || TO_Y(add, 'A'))
# define AROUND(add_x, add_y) (X_AND_X(add_x) && Y_AND_Y(add_y))

typedef struct		s_entity
{
	int		x;
	int		y;
	struct s_entity	*next;
}			t_entity;

typedef struct		s_info
{
	int		p[2];
	int		line;
	int		colone;
	int		nb_box;
	int		nb_area;
	t_entity	*box;
	t_entity	*area;
}			t_info;

/* aux */
int 		my_strlen(char *, int);

/* list */
void 		list_area(t_info *, int, int);
void 		list_box(t_info *, int, int);
void 		free_info(t_info *);
void 		free_entity(t_entity *);

/* my_sokoban */
void		my_sokoban(char *);
void 		close_prog(char **, t_info *, int);
void 		play(char ***, int *, t_info *, char *);
void 		set_info(t_info *, char **);

/* win_lose */
void 		win(char ***, t_info *);
void 		lose(char **, t_info *);

/* move */
void 		move_player(char ***, int, t_info *);
void 		move_to(char ***, t_info *, int, int);
void 		set_cord_X(t_info *, int, int);

/*map*/
void		make_map(char *, char ***, t_info *);
void 		print_map(char **, t_info *);
void		my_strcpy(char **, char *, t_info *);
void 		max_map(char *, t_info *);
void 		empty_line(char **, t_info *);

/* color */
void 		set_color();
void 		print_color(char *);

/* help */
void 		print_help();

#endif		/* !_MY_H_ */
