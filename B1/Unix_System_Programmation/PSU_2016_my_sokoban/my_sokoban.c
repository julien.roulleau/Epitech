/*
** my_sokoban.c for my_sokoban in /home/infocraft/Job/En_Cours/PSU_2016_my_sokoban/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Dec 14 11:45:43 2016 Roulleau Julien
** Last update Fri Dec 16 14:03:34 2016 Roulleau Julien
*/

#include "my.h"

void		my_sokoban(char *file)
{
	int	key;
	char	**map;
	t_info	*info;

	key = 0;
	if (!(info = malloc(sizeof(t_info))))
		exit(84);
	make_map(file, &map, info);
	initscr();
	start_color();
	set_color();
	raw();
	noecho();
	curs_set(FALSE);
	set_info(info, map);
	keypad(stdscr, true);
	while (key != 27)
		play(&map, &key, info, file);
	close_prog(map, info, 0);
}

void 		close_prog(char **map, t_info *info, int leave)
{
	endwin();
	free_info(info);
	free(map);
	exit(leave);
}

void 		play(char ***map, int *key, t_info *info, char *file)
{
	clear();
	print_map(*map, info);
	*key = getch();
	if (*key == KEY_UP || *key == KEY_DOWN ||
		*key == KEY_RIGHT || *key == KEY_LEFT)
			move_player(map, *key, info);
	else if (*key == ' ')
	{
		make_map(file, map, info);
		set_info(info, *map);
	}
	win(map, info);
	lose(*map, info);
}

void 		set_info(t_info *info, char **map)
{
	int	x;
	int	y;

	y = -1;
	info->box = NULL;
	info->area = NULL;
	info->nb_box = 0;
	info->nb_area = 0;
	while (!(x = 0) && map[++y])
		while (map[y][++x])
		{
			if (map[y][x] == 'X' && (info->nb_box += 1))
				list_box(info, y, x);
			else if (map[y][x] == 'O' && (info->nb_area += 1))
				list_area(info, y, x);
			else if (map[y][x] == 'P')
			{
				info->p[1] = x;
				info->p[0] = y;
			}
		}
	if (info->nb_box != info->nb_area)
		close_prog(map, info, 84);
}
