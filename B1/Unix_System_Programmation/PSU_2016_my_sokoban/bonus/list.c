/*
** list.c for my_sokoban in /home/infocraft/Job/En_Cours/PSU_2016_my_sokoban/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Dec 15 21:22:55 2016 Roulleau Julien
** Last update Sun Dec 18 22:03:42 2016 Roulleau Julien
*/

#include "my.h"

void 		list_area(t_info *info, int y, int x)
{
	t_entity	*area;

	if (!(area = malloc(sizeof(t_entity))))
		exit(84);
	area->y = y;
	area->x = x;
	if (info->area == NULL)
	{
		area->next = NULL;
		info->area = area;
	}
	else
	{
		area->next = info->area;
		info->area = area;
	}
}

void 		list_box(t_info *info, int y, int x)
{
	t_entity	*box;

	if (!(box = malloc(sizeof(t_entity))))
		exit(84);
	box->y = y;
	box->x = x;
	if (info->box == NULL)
	{
		box->next = NULL;
		info->box = box;
	}
	else
	{
		box->next = info->box;
		info->box = box;
	}
}

void 		list_move(t_info *info)
{
	t_move 	*move;

	if (!(move = malloc(sizeof(t_move))))
		exit(84);
	move->p[0] = -1;
	move->p[1] = -1;
	move->box[0] = -1;
	move->box[1] = -1;
	if (info->move == NULL)
	{
		move->next = NULL;
		info->move = move;
	}
	else
	{
		move->next = info->move;
		info->move = move;
	}
}

void 		remove_node_move(t_move **move)
{
	t_move	*tmp;

	tmp = (*move);
	(*move) = (*move)->next;
	free(tmp);
}
