/*
** menu.c for my_sokoban in /home/infocraft/Job/En_Cours/PSU_2016_my_sokoban/bonus/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Dec 16 22:09:37 2016 Roulleau Julien
** Last update Tue Dec 20 17:51:08 2016 Roulleau Julien
*/

#include "my.h"

void 		menu()
{
	t_info	*info;
	char	**level;
	int	key;
	int 	i;

	i = key = 0;
	init_screen(&info, &level);
	while (!(key == ' ' && i == 1))
	{
		clear();
		if (i == 0)
			print_menu(10, 9);
		else if (i == 1)
			print_menu(9, 10);
		key = getch();
		if (key == ' ' && i == 0)
			set_level(&level, info);
		else if (key == KEY_UP && i == 1)
			i--;
		else if (key == KEY_DOWN && i == 0)
			i++;
		else if (key == 'l')
			info->lose = !(info->lose);
	}
	close_prog(level, info, 0);
}

void 		init_screen(t_info **info, char ***level)
{
	(*info) = malloc(sizeof(t_info));
	(*info)->area = NULL;
	(*info)->box = NULL;
	(*info)->move = NULL;
	(*info)->lose = 1;
	*level = NULL;
	initscr();
	start_color();
	set_color();
	raw();
	noecho();
	curs_set(FALSE);
	keypad(stdscr, true);
}

void 		print_menu(int first, int second)
{
	print_title();
	if (LINES < 3 || COLS < 8)
	{
		move(LINES / 2, COLS / 2 - 4);
		attron(COLOR_PAIR(9));
		printw("Bad size");
	}
	else
	{
		attron(COLOR_PAIR(first));
		mvprintw(LINES / 2 - 1, COLS / 2 - 3, "Levels");
		attron(COLOR_PAIR(second));
		mvprintw(LINES / 2 + 1, COLS / 2 - 3, "Close");
	}
}
