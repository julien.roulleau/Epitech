/*
** free.c for my_sokoban in /home/infocraft/Job/En_Cours/PSU_2016_my_sokoban/bonus/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun Dec 18 13:42:01 2016 Roulleau Julien
** Last update Sun Dec 18 13:43:47 2016 Roulleau Julien
*/

#include "my.h"

void 		free_info(t_info *info)
{
	if (info->area != NULL)
		free_entity(info->area);
	if (info->box != NULL)
		free_entity(info->box);
	if (info->move != NULL)
		free_move(info->move);
	free(info);
}

void 		free_move(t_move *move)
{
	if (move->next != NULL)
		free_move(move->next);
	free(move);
}

void 		free_entity(t_entity *entity)
{
	if (entity->next != NULL)
		free_entity(entity->next);
	free(entity);
}
