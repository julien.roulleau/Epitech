/*
** win_lose.c for my_sokoban in /home/infocraft/Job/En_Cours/PSU_2016_my_sokoban/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Dec 16 09:44:45 2016 Roulleau Julien
** Last update Tue Dec 20 17:43:38 2016 Roulleau Julien
*/

#include "my.h"

void 			win(char ***map, t_info *info, int *key)
{
	t_entity	*area;
	int		match;

	match = 0;
	area = info->area;
	while (area != NULL)
	{
		if ((*map)[area->y][area->x] == 'X')
			match++;
		else if ((*map)[area->y][area->x] != 'P')
			(*map)[area->y][area->x] = 'O';
		area = area->next;
	}
	if (match == info->nb_area)
	{
		attron(COLOR_PAIR(9));
		clear();
		mvprintw(LINES / 2 - 1, COLS / 2 - 8, "You Win Press Key");
		mvprintw(LINES / 2 + 1, COLS / 2 - 8,
				"Score : %i\n", info->score);
		getch();
		*key = 27;
	}
	else if (info->lose)
		lose(*map, info, key);
}

void 			lose(char **map, t_info *info, int *key)
{
	t_entity	*box;
	int		match;

	match = 0;
	box = info->box;
	while (box != NULL)
	{
		if (AROUND(1, 1) ||
			AROUND(1, -1) ||
			AROUND(-1, 1) ||
			AROUND(-1, -1))
			match ++;
		box = box->next;
	}
	if (match == info->nb_box)
	{
		attron(COLOR_PAIR(9));
		clear();
		mvprintw(LINES / 2 - 1, COLS / 2 - 9, "You Lose Press Key");
		getch();
		*key = 27;
	}
}
