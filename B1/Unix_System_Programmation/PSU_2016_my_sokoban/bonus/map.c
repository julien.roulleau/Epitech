/*
** (*map).c for my_sokoban in /home/infocraft/Job/En_Cours/PSU_2016_my_sokoban/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Dec 14 23:12:39 2016 Roulleau Julien
** Last update Mon Dec 19 21:36:24 2016 Roulleau Julien
*/

#include "my.h"

void		make_map(char *file, char ***map, t_info *info)
{
	FILE	*stream;
	char	*line;
	size_t	len;
	ssize_t	read;
	int	i;

	i = -1;
	line = NULL;
	len = 0;
	max_map(file, info);
	if (!(*map = malloc(sizeof(char *) * (info->line + 4))))
		exit(84);
	while (++i < info->line)
		(*map)[i] = 0;
	i = 0;
	empty_line(&(*map)[i], info);
	if ((stream = fopen(file, "r")) == NULL)
		exit(84);
	while ((read = getline(&line, &len, stream)) != -1)
		my_strcpy(&(*map)[++i], line, info);
	empty_line(&(*map)[++i], info);
	(*map)[++i] = 0;
	fclose(stream);
	free(line);
}

void 		print_map(char **map, t_info *info)
{
	int	i;
	int	line;
	int	colone;

	i = -1;
	line = LINES / 2 - info->line / 2 - 2;
	colone = COLS / 2 - info->colone - 1;
	attron(COLOR_PAIR(9));
	mvprintw(line -2, colone , "score : %i\n", info->score);
	if (LINES < info->line + 8 || COLS < info->colone * 2 + 2)
	{
		move(LINES / 2, COLS / 2 - 4);
		attron(COLOR_PAIR(9));
		printw("Bad size");
	}
	else
		while (map[++i])
		{
			move(line++, colone);
			print_color(map[i]);
		}
}

void		my_strcpy(char **mapl, char *line, t_info *info)
{
	int	i;

	i = -1;
	(*mapl) = malloc(sizeof(char) * (info->colone + 3));
	(*mapl)[0] = 'A';
	while (line[++i])
		(*mapl)[i + 1] = line[i];
	i--;
	while (++i < info->colone)
		(*mapl)[i] = ' ';
	(*mapl)[i] = 'A';
	(*mapl)[++i] = '\n';
	(*mapl)[++i] = '\0';
}

void 		max_map(char *file, t_info *info)
{
	FILE	*stream;
	char	*line;
	size_t	len;
	ssize_t	read;
	int	i;

	i = 0;
	line = NULL;
	len = 0;
	info->colone = 0;
	if ((stream = fopen(file, "r")) == NULL)
		exit(84);
	while ((read = getline(&line, &len, stream)) != -1 && (i += 1))
		if (my_strlen(line, 0) > info->colone)
			info->colone = my_strlen(line, 0);
	info->line = i - 1;
	fclose(stream);
	free(line);
}

void 		empty_line(char **map, t_info *info)
{
	int	i;

	i = -1;
	(*map) = malloc(sizeof(char) * (info->colone + 3));
	while (++i <= info->colone)
		(*map)[i] = 'A';
	(*map)[i] = '\n';
	(*map)[++i] = '\0';
}
