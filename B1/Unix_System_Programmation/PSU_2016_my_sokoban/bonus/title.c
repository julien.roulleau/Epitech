/*
** title.c for my_sokoban in /home/infocraft/Job/En_Cours/PSU_2016_my_sokoban/bonus/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Dec 20 12:38:35 2016 Roulleau Julien
** Last update Tue Dec 20 13:00:50 2016 Roulleau Julien
*/

#include "my.h"

void 		print_title()
{
	move(1, COLS / 2 - 60);
	pct("  ####     ##     #    #     ##     ####       ##     #    #");
	move(2, COLS / 2 - 60);
	pct(" #        #  #    #   #     #  #    #   #     #  #    ##   #");
	move(3, COLS / 2 - 60);
	pct("#        #    #   #  #     #    #   #    #   #    #   # #  #");
	move(4, COLS / 2 - 60);
	pct("######   #    #   # #      #    #   #####    ######   # #  #");
	move(6, COLS / 2 - 60);
	pct("     #   #    #   #  #     #    #   #    #   #    #   #  # #");
	move(5, COLS / 2 - 60);
	pct("     #   #    #   ## #     #    #   #    #   #    #   #  # #");
	move(7, COLS / 2 - 60);
	pct("    #     #  #    #   #     #  #    #    #   #    #   #   ##");
	move(8, COLS / 2 - 60);
	pct("####       ##     #    #     ##     #####    #    #   #    #");
}

void 		pct(char *line)
{
	char	c[3];
	int	i;

	c[2] = 0;
	i = -1;
	while (line[++i])
	{
		c[0] = line[i];
		c[1] = line[i];
		if (c[0] == ' ')
			attron(COLOR_PAIR(1));
		else if (c[0] == '#')
			attron(COLOR_PAIR(7));
		printw(c);
	}
}
