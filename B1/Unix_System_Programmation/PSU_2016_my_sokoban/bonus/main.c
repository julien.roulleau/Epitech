/*
** main.c for my_sokoban in /home/infocraft/Job/En_Cours/PSU_2016_my_sokoban/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Dec 14 11:45:52 2016 Roulleau Julien
** Last update Sat Dec 17 01:17:41 2016 Roulleau Julien
*/

#include "my.h"

int		main(int argc, char **argv)
{
	if (argc < 2)
		menu();
	else if (argv[1][0] == '-')
	{
		if (argv[1][1] == 'h')
		{
			print_help();
			return (0);
		}
		else
		{
			write(2, "Invalid argument\n", 17);
			return (84);
		}
	}
	return (0);
}
