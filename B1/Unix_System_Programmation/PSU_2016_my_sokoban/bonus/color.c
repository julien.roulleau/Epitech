/*
** color.c for my_sokoban in /home/infocraft/Job/En_Cours/PSU_2016_my_sokoban/bonus/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Dec 16 13:57:31 2016 Roulleau Julien
** Last update Mon Dec 19 15:14:21 2016 Roulleau Julien
*/

#include "my.h"

void 		set_color()
{
	init_pair(1, COLOR_BLACK, COLOR_BLACK);
	init_pair(2, COLOR_RED, COLOR_RED);
	init_pair(3, COLOR_GREEN, COLOR_GREEN);
	init_pair(4, COLOR_YELLOW, COLOR_YELLOW);
	init_pair(5, COLOR_BLUE, COLOR_BLUE);
	init_pair(6, COLOR_MAGENTA, COLOR_MAGENTA);
	init_pair(7, COLOR_CYAN, COLOR_CYAN);
	init_pair(8, COLOR_WHITE, COLOR_WHITE);
	init_pair(9, COLOR_WHITE, COLOR_BLACK);
	init_pair(10, COLOR_BLACK, COLOR_WHITE);
}

void 		print_color(char *line)
{
	char	c[3];
	int	i;

	c[2] = 0;
	i = -1;
	while (line[++i])
	{
		c[0] = line[i];
		c[1] = line[i];
		if (c[0] == ' ')
			attron(COLOR_PAIR(1));
		else if (c[0] == '#')
			attron(COLOR_PAIR(8));
		else if (c[0] == 'X')
			attron(COLOR_PAIR(4));
		else if (c[0] == 'O')
			attron(COLOR_PAIR(3));
		else if (c[0] == 'P')
			attron(COLOR_PAIR(5));
		else if (c[0] == 'A')
			attron(COLOR_PAIR(1));
		printw(c);
	}
}
