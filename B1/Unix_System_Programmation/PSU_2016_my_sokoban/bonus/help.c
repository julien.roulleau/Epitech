/*
** help.c for my_sokoban in /home/infocraft/Job/En_Cours/PSU_2016_my_sokoban/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Dec 14 11:54:35 2016 Roulleau Julien
** Last update Sat Dec 17 01:21:21 2016 Roulleau Julien
*/

#include "my.h"

void 		print_help()
{
	PRINT("USAGE\n");
	PRINT("\t./my_sokoban\n");
	PRINT("\n");
	PRINT("DESCRIPTION\n");
	PRINT("\tmenu\tuse up and down arrow to move and space to select.\n");
	PRINT("\n");
	PRINT("\tmap\tfile representing the warehouse map,");
	PRINT(" containing white box for walls,\n");
	PRINT("\t\tblue box for the player, yellow box for boxes");
	PRINT(" and green box for storage locations.\n");
}
