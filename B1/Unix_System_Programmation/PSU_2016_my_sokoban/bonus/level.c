/*
** level.c for my_sokoban in /home/infocraft/Job/En_Cours/PSU_2016_my_sokoban/bonus/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Dec 16 22:52:07 2016 Roulleau Julien
** Last update Tue Dec 20 17:46:35 2016 Roulleau Julien
*/

#include "my.h"

void 		set_level(char ***level, t_info *info)
{
	char	*map;
	int	key;
	int	i;

	key = 0;
	i = 0;
	make_level("map/level_list", level, info);
	while (key != 27)
	{
		print_level(level, i, info);
		print_title();
		key = getch();
		if (key == ' ')
		{
			str_and_str(&map, "map/", (*level)[i]);
			my_sokoban(map, info->lose);
			key = 27;
		}
		else if (key == KEY_UP && i > 0)
			i--;
		else if (key == KEY_DOWN && i < info->line)
			i++;
	}
}

void		make_level(char *file, char ***level, t_info *info)
{
	FILE	*stream;
	char	*line;
	size_t	len;
	ssize_t	read;
	int	i;

	i = -1;
	line = NULL;
	len = 0;
	max_map(file, info);
	if (!(*level = malloc(sizeof(char *) * (info->line + 2))))
		exit(84);
	while (++i < info->line)
		(*level)[i] = 0;
	i = -1;
	if ((stream = fopen(file, "r")) == NULL)
		exit(84);
	while ((read = getline(&line, &len, stream)) != -1)
		my_strcpy_level(&(*level)[++i], line, info);
	(*level)[++i] = 0;
	fclose(stream);
	free(line);
}

void		my_strcpy_level(char **mapl, char *line, t_info *info)
{
	int	i;

	i = -1;
	(*mapl) = malloc(sizeof(char) * (info->colone + 1));
	while (line[++i])
		(*mapl)[i] = line[i];
	(*mapl)[i] = '\0';
}

void 		print_level(char ***level, int i, t_info *info)
{
	clear();
	if (LINES < 25 || COLS < 16)
	{
		attron(COLOR_PAIR(9));
		mvprintw(LINES / 2, COLS / 2 - 4, "Bad size");
	}
	else
	{
		attron(COLOR_PAIR(9));
		if (i >= 3)
			mvprintw(LINES / 2, COLS / 2 - 3, (*level)[i - 3]);
		if (i >= 2)
			mvprintw(LINES / 2 + 2, COLS / 2 - 3, (*level)[i - 2]);
		if (i >= 1)
			mvprintw(LINES / 2 + 4, COLS / 2 - 3, (*level)[i - 1]);
		attron(COLOR_PAIR(10));
		mvprintw(LINES / 2 + 6, COLS / 2 - 3, (*level)[i]);
		attron(COLOR_PAIR(9));
		if (i <= info->line - 1)
			mvprintw(LINES / 2 + 8, COLS / 2 - 3, (*level)[i + 1]);
		if (i <= info->line - 2)
			mvprintw(LINES / 2 + 10, COLS / 2 - 3, (*level)[i + 2]);
		if (i <= info->line - 3)
			mvprintw(LINES / 2 + 12, COLS / 2 - 3, (*level)[i + 3]);
	}
}

void 		str_and_str(char **dest, char *str, char *and)
{
	int 	d;
	int 	i;
	int	n;

	d = -1;
	i = my_strlen(str, 0);
	n = my_strlen(and, 0);
	*dest = malloc(sizeof(char) * (i + n + 1));
	i = -1;
	n = -1;
	while (str[++i])
		(*dest)[++d] = str[i];
	while (and[++n])
		(*dest)[++d] = and[n];
	(*dest)[d] = '\0';
}
