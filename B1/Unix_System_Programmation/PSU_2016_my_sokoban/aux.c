/*
** aux.c for my_sokoban in /home/infocraft/Job/En_Cours/PSU_2016_my_sokoban/bonus/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Dec 16 17:50:03 2016 Roulleau Julien
** Last update Fri Dec 16 17:56:43 2016 Roulleau Julien
*/

#include "my.h"

int 		my_strlen(char *str, int size)
{
	if (*str == '\0')
		return (size);
	size = my_strlen(++str, ++size);
	return (size);
}
