/*
** move.c for my_sokoban in /home/infocraft/Job/En_Cours/PSU_2016_my_sokoban/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Dec 15 11:07:13 2016 Roulleau Julien
** Last update Fri Dec 16 17:30:15 2016 Roulleau Julien
*/

#include "my.h"

void 		move_player(char ***map, int key, t_info *info)
{
	if (key == KEY_UP &&
		(*map)[info->p[0] - 1][info->p[1]] != '#' &&
		(*map)[info->p[0] - 1][info->p[1]] != 'A')
		move_to(map, info, 0, -1);
	else if (key == KEY_DOWN &&
		(*map)[info->p[0] + 1][info->p[1]] != '#' &&
		(*map)[info->p[0] + 1][info->p[1]] != 'A')
		move_to(map, info, 0, 1);
	else if (key == KEY_LEFT &&
		(*map)[info->p[0]][info->p[1] - 1] != '#' &&
		(*map)[info->p[0]][info->p[1] - 1] != 'A')
		move_to(map, info, 1, -1);
	else if (key == KEY_RIGHT &&
		(*map)[info->p[0]][info->p[1] + 1] != '#' &&
		(*map)[info->p[0]][info->p[1] + 1] != 'A')
		move_to(map, info, 1, 1);
}

void 		move_to(char ***map, t_info *info, int cord, int value)
{
	if ((*map)POS_MAP(1, 0) == 'X' &&
		(*map)POS_MAP(2, 0) != '#' &&
		(*map)POS_MAP(2, 0) != 'X' &&
		(*map)POS_MAP(2, 0) != 'A')
		{
			(*map)[info->p[0]][info->p[1]] = ' ';
			info->p[cord] += value;
			(*map)[info->p[0]][info->p[1]] = 'P';
			(*map)POS_MAP(1, 0) = 'X';
			set_cord_X(info, cord, value);
		}
	else if ((*map)POS_MAP(1, 0) != 'X')
	{
		(*map)[info->p[0]][info->p[1]] = ' ';
		info->p[cord] += value;
		(*map)[info->p[0]][info->p[1]] = 'P';
	}
}

void 			set_cord_X(t_info *info, int cord, int value)
{
	t_entity	*box;

	box = info->box;
	while (box->y != info->p[0] || box->x != info->p[1])
		box = box->next;
	box->y = POS(0, 1, 0);
	box->x = POS(1, 1, 0);
}
