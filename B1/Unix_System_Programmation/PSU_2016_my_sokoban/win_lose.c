/*
** win_lose.c for my_sokoban in /home/infocraft/Job/En_Cours/PSU_2016_my_sokoban/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Dec 16 09:44:45 2016 Roulleau Julien
** Last update Fri Dec 16 12:49:14 2016 Roulleau Julien
*/

#include "my.h"

void 			win(char ***map, t_info *info)
{
	t_entity	*area;
	int		match;

	match = 0;
	area = info->area;
	while (area != NULL)
	{
		if ((*map)[area->y][area->x] == 'X')
			match++;
		else if ((*map)[area->y][area->x] != 'P')
			(*map)[area->y][area->x] = 'O';
		area = area->next;
	}
	if (match == info->nb_area)
		close_prog(*map, info, 0);
}

void 			lose(char **map, t_info *info)
{
	t_entity	*box;
	int		match;

	match = 0;
	box = info->box;
	while (box != NULL)
	{
		if (AROUND(1, 1) ||
			AROUND(1, -1) ||
			AROUND(-1, 1) ||
			AROUND(-1, -1))
			match ++;
		box = box->next;
	}
	if (match == info->nb_box)
		close_prog(map, info, 1);
}
