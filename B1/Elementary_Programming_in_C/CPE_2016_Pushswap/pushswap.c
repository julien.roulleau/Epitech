/*
** pushswap.c for pushswap in /home/infocraft/Job/CPE_2016_Pushswap/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Nov 23 11:29:01 2016 Roulleau Julien
** Last update Thu Nov 24 21:17:54 2016 Roulleau Julien
*/

#include "my.h"

int		my_push_swap(t_list **list1, t_list **list2)
{
	while (*list1 != NULL)
	{
		my_sort(list1, list2);
	}
	while (*list2 != NULL)
	{
		if ((*list2)->next == *list2)
			write(1, "pa\n", 3);
		else
			write(1, "pa ", 3);
		my_p(list2, list1);
	}
	return (0);
}

void 		my_sort(t_list **list1, t_list **list2)
{
	int	pos_small;

	pos_small = find_small(*list1);
	if (pos_small * 2 < count_list(*list1))
	{
		while (pos_small > 0)
		{
			my_r(list1);
			write(1, "ra ", 3);
			pos_small--;
		}
	}
	else
	{
		pos_small = count_list(*list1) - pos_small + 1;
		while (pos_small > 0)
		{
			write(1, "rra ", 4);
			my_rr(list1);
			pos_small--;
		}
	}
	write(1, "pb ", 3);
	my_p(list1, list2);
}

int		find_small(t_list *list)
{
	int	first;
	int	count;
	int	pos;
	int	small;

	pos = 0;
	count = 0;
	first = list->nb;
	small = list->nb;
	list = list->next;
	while (first != list->nb)
	{
		count++;
		if (small > list->nb)
		{
			small = list->nb;
			pos = count;
		}
		list = list->next;
	}
	return (pos);
}

int		count_list(t_list *list)
{
	int 	first;
	int	count;

	count = 0;
	first = list->nb;
	list = list->next;
	while (first != list->nb)
	{
		list = list->next;
		count++;
	}
	return (count);
}
