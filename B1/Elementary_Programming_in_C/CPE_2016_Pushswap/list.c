/*
** list.c for pushswap in /home/infocraft/Job/CPE_2016_Pushswap/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Nov 22 13:48:05 2016 Roulleau Julien
** Last update Thu Nov 24 19:22:32 2016 Roulleau Julien
*/

#include <my.h>

t_list		*create_list()
{
	t_list	*list;

	if (!(list = malloc(sizeof(t_list))))
		return (NULL);
	list->previous = list;
	list->next = list;
	return (list);
}

void 		put_list(t_list *list, char **argv)
{
	int	i;

	i = 0;
	list->nb = my_getnbr(argv[0], 0);
	while (argv[++i])
	{
		create_node_after(list, my_getnbr(argv[i], 0));
		list = list->next;
	}
}

int		create_node_before(t_list *list, int nb)
{
	t_list	*node;

	if (!(node = malloc(sizeof(t_list))))
		return (84);
	node->nb = nb;
	node->previous = list->previous;
	node->next = list;
	list->previous->next = node;
	list->previous = node;
	return (0);
}

int		create_node_after(t_list *list, int nb)
{
	t_list	*node;

	if (!(node = malloc(sizeof(t_list))))
		return (84);
	node->nb = nb;
	node->previous = list;
	node->next = list->next;
	list->next->previous = node;
	list->next = node;
	return (0);
}

int 		remove_node(t_list **list)
{
	t_list *tmp;

	tmp = (*list)->next;
	(*list)->previous->next = (*list)->next;
	(*list)->next->previous = (*list)->previous;
	free(*list);
	(*list) = tmp;
	return (0);
}
