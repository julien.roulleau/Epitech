/*
** my.h for push_swap in /home/infocraft/Job/CPE_2016_Pushswap/include/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Nov 21 17:10:48 2016 Roulleau Julien
** Last update Thu Nov 24 19:21:55 2016 Roulleau Julien
*/

#ifndef _MY_H_

# define _MY_H_

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct 		s_list
{
	int		nb;
	struct s_list	*previous;
	struct s_list	*next;
}			t_list;

/*main*/
int 		test_error(int, char **);

/*list*/
t_list		*create_list();
int		create_node_before(t_list *, int);
int		create_node_after(t_list *, int);
int		remove_node(t_list **);
void 		put_list(t_list *, char **);

/*aux*/
int		my_getnbr(char *, int);

/*basic_func*/
void		my_s(t_list **);
void		my_p(t_list **, t_list **);
void		my_r(t_list **);
void		my_rr(t_list **);

/*pushswap*/
int		my_push_swap(t_list **, t_list **);
void 		my_sort(t_list **, t_list **);
int		find_small(t_list *);
int		count_list(t_list *);

#endif /* _MY_H_ */
