/*
** main.c for pushswap in /home/infocraft/Job/CPE_2016_Pushswap/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Nov 21 16:48:23 2016 Roulleau Julien
** Last update Sat Nov 26 22:35:51 2016 Roulleau Julien
*/

#include "my.h"

int 		main(int argc, char **argv)
{
	t_list 	*list1;
	t_list 	*list2;
	int	temp;
	int	need;

	need = 0;
	if (test_error(argc - 1, argv + 1) != 0)
		return (84);
	list1 = create_list();
	list2 = NULL;
	put_list(list1, argv + 1);
	temp = list1->nb;
	while (list1->next->nb != temp)
	{
		if (list1->nb > list1->next->nb)
			need = 1;
		list1 = list1->next;
	}
	list1 = list1->next;
	if (need)
		my_push_swap(&list1, &list2);
	else
		write(1, "\n", 1);
	return (0);
}

int 		test_error(int argc, char **argv)
{
	int 	i;
	int 	n;

	i = -1;
	n = -1;
	if (argc < 1)
		return (84);
	while (argv[++i])
		{
			n = -1;
			while (argv[i][++n])
			if (!(argv[i][n] >= '0' && argv[i][n] <= '9') && argv[i][n] != '-')
				return (84);
			}
	(void) argv;
	return (0);
}
