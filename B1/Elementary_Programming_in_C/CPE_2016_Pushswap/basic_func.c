/*
** basic_func.c for pushswap in /home/infocraft/Job/CPE_2016_Pushswap/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Nov 21 16:01:24 2016 Roulleau Julien
** Last update Wed Nov 23 21:41:01 2016 Roulleau Julien
*/

#include "my.h"

void		my_s(t_list **list)
{
	int	temp;

	temp = (*list)->nb;
	(*list)->nb = (*list)->next->nb;
	(*list)->next->nb = temp;
}

void		my_p(t_list **list1, t_list **list2)
{
	if (*list2 == NULL)
			(*list2) = create_list();
	if ((*list2)->nb == 0)
		(*list2)->nb = (*list1)->nb;
	else
	{
		create_node_before(*list2, (*list1)->nb);
		my_rr(list2);
	}
	if ((*list1) == (*list1)->next)
	{
		remove_node(&*list1);
		(*list1) = NULL;
	}
	else
		remove_node(&*list1);
}

void		my_r(t_list **list)
{
	(*list) = (*list)->next;
}

void		my_rr(t_list **list)
{
	(*list) = (*list)->previous;
}
