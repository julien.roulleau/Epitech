/*
** parcing.c for bsq in /home/infocraft/Job/En_Cours/CPE_2016_BSQ/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Dec  7 00:44:14 2016 Roulleau Julien
** Last update Wed Jan  4 11:13:47 2017 Roulleau Julien
*/

#include "my.h"

int			*set_map(char *file, t_info *info)
{
	struct stat	fstat;
	char		*buffer;
	int		*map;
	int 		fd;
	int		len;

	if (stat(file, &fstat) == -1)
		exit(84);
	if (!(buffer = malloc(sizeof(char) * fstat.st_size)))
		return (0);
	if ((fd = open(file, O_RDONLY)) == -1)
		exit(84);
	while ((read(fd, buffer, 1)) > 0 && buffer[0] != '\n')
		buffer[0] = '\0';
 	if ((len = read(fd, buffer, fstat.st_size)) < 0)
		exit(84);
	close(fd);
	buffer[len] = '\0';
	map = malloc(sizeof(int) * (len + 2));
	info->l = 0;
	map = map_maker(buffer, map, info);
	info->t = len - info->l;
	info->c = info->t / info->l;
	free(buffer);
	return (map);
}

int		*map_maker(char *buffer, int *map, t_info *info)
{
	int	i;
	int	b;
	int	dots;

	dots = 0;
	b = -1;
	i = -1;
	while (buffer[++b])
	{
		if (buffer[b] == '.')
			dots++;
		else
			while (dots > 0)
			{
				map[++i] = dots;
				dots--;
			}
		if (buffer[b] == 'o')
			map[++i] = 0;
		else if (buffer[b] == '\n')
			info->l++;
	}
	map[++i] = -1;
	map[++i] = -1;
	return (map);
}
