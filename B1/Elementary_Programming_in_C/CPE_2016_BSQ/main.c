/*
** mainc.c for bsq in /home/infocraft/Job/En_Cours/CPE_2016_BSQ/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Dec  7 00:42:59 2016 Roulleau Julien
** Last update Sun Dec 18 02:21:44 2016 Roulleau Julien
*/

#include "my.h"

int 		main(int argc, char **argv)
{
	char 	*solved_map;
	int 	*map;
	t_info	*info;

	if (argc < 2)
		exit(84);
	if (!(info = malloc(sizeof(t_info))))
		exit(84);
	map = set_map(argv[1], info);
	map = square(map, info);
	if (!(solved_map = malloc(sizeof(char) * (info->l + info->t + 1))))
		exit(84);
	print_map(map, info, solved_map);
	free(map);
	free(info);
	return (0);
}
