/*
** square.c for bsq in /home/infocraft/Job/En_Cours/CPE_2016_BSQ/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sat Dec 17 23:28:21 2016 Roulleau Julien
** Last update Sun Dec 18 00:42:41 2016 Roulleau Julien
*/

#include "my.h"

int		*square(int *map, t_info *info)
{
	int 	i;

	i = 0;
	while (map[i] != -1)
		i++;
	while (i + info->c > info->t && (i--))
		if (map[i] != 0)
			map[i] = 1;
	i--;
	while (i >= 0 && (i--))
	{
		if (map[i + info->c] < map[i])
			map[i] = map[i + info->c] + 1;
		if (map[i + info->c + 1] < map[i])
			map[i] = map[i + info->c + 1] + 1;
	}
	return (map);
}
