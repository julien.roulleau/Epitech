/*
** solved_map.c for bsq in /home/infocraft/Job/En_Cours/CPE_2016_BSQ/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Dec  8 00:43:51 2016 Roulleau Julien
** Last update Sun Dec 18 02:20:33 2016 Roulleau Julien
*/

#include "my.h"

void		print_map(int *map, t_info *info, char *solved_map)
{
	int	p;
	int	c;
	int	len;
	int	i;
	int	n;

	i = -1;
	n = -1;
	c = 0;
	p = find_big_square(map);
	len = map[p];
	while (map[++i] != -1)
	{
		if (IN_SQUARE)
			solved_map[++n] = 'x';
		else if (map[i] > 0)
			solved_map[++n] = '.';
		else if (map[i] == 0)
			solved_map[++n] = 'o';
		if ((i + 1) % info->c == 0 && (c += 1))
			solved_map[++n] = '\n';
	}
	solved_map[++n] = '\0';
	write(1, solved_map, n);
}

int		find_big_square(int *map)
{
	int 	i;
	int	position;
	int	value;

	i = 0;
	value = map[i];
	position = i;
	while (map[++i] != -1)
		if (value < map[i])
			{
				value = map[i];
				position = i;
			}
	return (position);
}
