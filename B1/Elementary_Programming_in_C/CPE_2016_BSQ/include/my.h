/*
**my.hformy_draw_linein/home/infocraft/Job/bswireframe/
**
**MadebyRoulleauJulien
**Login<julien.roulleau@epitech.eu>
**
**StartedonThuNov1717:50:382016RoulleauJulien
**LastupdateThuNov1717:51:242016RoulleauJulien
*/

#ifndef _MY_H_

# define _MY_H_

# include <unistd.h>
# include <stdlib.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <stdio.h>

# define STRLEN(str) (sizeof(str) / sizeof(str[0]) - 1)
# define PRINT(str) (write(1, str, STRLEN(str)))

# define IN_COL (i % info->c >= p % info->c && i % info->c < p % info->c + len)
# define IN_LINE (c >= p / info->c && c < p / info->c + len)
# define IN_SQUARE (IN_COL && IN_LINE)

typedef struct 		s_info
{
	int 		l;
	int 		c;
	int 		t;
}			t_info;

/* init_map */
int		*set_map(char *, t_info *);
int		*map_maker(char *, int *, t_info *);

/* square */
int		*square(int *, t_info *);

/* print_map */
void		print_map(int *, t_info *, char *);
int		find_big_square(int *);

#endif		/* !_MY_H_ */
