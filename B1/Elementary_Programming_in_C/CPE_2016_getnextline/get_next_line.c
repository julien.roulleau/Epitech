/*
** get_next_line.c for get_next_line in /home/infocraft/Job/En_Cours/CPE_2016_getnextline/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Jan  3 23:48:15 2017 Roulleau Julien
** Last update Tue Jan 17 10:22:34 2017 Roulleau Julien
*/

#include <stdlib.h>
#include <unistd.h>

#include "get_next_line.h"

char 		*re_malloc(char *, int, int);

#define CPY() (buf[++i] && buf[i] != '\n' && (ln[++l] = buf[i]))
#define END_LN() ((buf[i] == '\n') && !(ln[l + 1] = '\0'))

char			*get_next_line(const int fd)
{
	static char	buf[READ_SIZE + 1];
	static int	i = -1;
	char		*ln;
	int		len;
	int 		nb;
	int		l;

	if (READ_SIZE < 0 || fd < 0 ||
		!(ln = malloc(sizeof(char) * (READ_SIZE + 1))))
		return (NULL);
	if ((l = -1) && (nb = 1) && i >= 0 && buf[i + 1])
	{
		while (CPY());
		if (END_LN())
			return (ln);
	}
	while ((len = read(fd, buf, READ_SIZE)) != -1 && (i = -1) && (nb++) &&
		!(buf[len] = ln[l + 1] = 0) && (ln = re_malloc(ln, nb, len)))
	{
		while (CPY());
		if (END_LN())
			return (ln);
	}
	free(ln);
	return (0);
}

char 		*re_malloc(char *ln, int nb, int len)
{
	char	*new_line;
	int	i;

	i = -1;
	if (!(new_line = malloc(sizeof(char) *
		(READ_SIZE * (nb - 1) + len + 1))))
		return (0);
	while (ln[++i])
		new_line[i] = ln[i];
	free(ln);
	return (new_line);
}
