/*
** get_next_line.h for get_next_line in /home/infocraft/Job/En_Cours/CPE_2016_getnextline/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Jan  3 23:44:40 2017 Roulleau Julien
** Last update Sun Jan 15 22:16:55 2017 Roulleau Julien
*/

#ifndef READ_SIZE
# define READ_SIZE (10)

char		*get_next_line(const int);

#endif /* !READ_SIZE */
