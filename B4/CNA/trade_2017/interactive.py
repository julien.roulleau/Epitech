import json
from threading import Thread

import requests

import Trade
from algo import run


def display_help(args):
    print("List of command: help / pause / resume / quit / login / reset_token / pull / buy / sell / balance")


def isnumber(number):
    try:
        int(number)
        return True
    except ValueError:
        return False


class Interactive(Thread):
    def __init__(self, api):
        Thread.__init__(self)
        api.start = False
        self.api = api
        self.isStart = True
        self.commands = {
            'help': display_help,
            'pause': self.pause,
            'resume': self.resume,
            'host': self.host,
            'quit': self.quit,
            'login': self.login,
            'reset_token': self.reset_token,
            'pull': self.pull,
            'buy': self.buy,
            'sell': self.sell,
            'balance': self.balance,
        }

    def run(self):
        while self.isStart:
            input_command = input().split()
            command = self.commands.get(input_command[0].lower())
            if command is None:
                print('Invalid Command')
            else:
                if all([*map(lambda x: isnumber(x), input_command[1:])]):
                    try:
                        command([*map(lambda x: int(x), input_command[1:])])
                    except requests.exceptions.HTTPError as e:
                        print("Request fail: {}".format(e))
                    except Trade.Error as e:
                        print(e.msg)
                else:
                    print("Invalid argument: Only number are expected")

    def pause(self, args):
        if len(args) == 0:
            self.api.start = False
        else:
            print("Invalid argument: Usage: pause")

    def resume(self, args):
        if len(args) == 0:
            self.api.start = True
        else:
            print("Invalid argument: Usage: pause")

    def host(self, args):
        if len(args) == 1:
            self.api.host = args[0]
        else:
            print("Invalid argument: Usage: host url")

    def quit(self, args):
        if len(args) == 0:
            self.api.running = False
            self.isStart = False
        else:
            print("Invalid argument: Usage: quit")

    def login(self, args):
        if len(args) == 0:
            self.api.login()
        else:
            print("Invalid argument: Usage: login")

    def reset_token(self, args):
        if len(args) == 0:
            self.api.reset_token()
        else:
            print("Invalid argument: Usage: reset_token")

    def pull(self, args):
        if 1 <= len(args) <= 3:
            print(json.dumps({"type": "pull", "data": self.api.pull(*args)}))
        else:
            print("Invalid argument: Usage: pull marketplace_id [count] [index]")

    def buy(self, args):
        if len(args) == 2:
            print(json.dumps({"type": "buy", "data": self.api.buy(*args)}))
        else:
            print("Invalid argument: Usage: buy marketplace_id quantity")

    def sell(self, args):
        if len(args) == 2:
            print(json.dumps({"type": "sell", "data": self.api.sell(*args)}))
        else:
            print("Invalid argument: Usage: sell marketplace_id quantity")

    def balance(self, args):
        if len(args) == 0:
            print(json.dumps({"type": "balance", "data": self.api.balance()}))
        else:
            print("Invalid argument: Usage: balance")


def interactive():
    api = Trade.Api()
    t = Interactive(api)
    t.start()
    api.run(run, 0)
    t.join()
