const {spawn} = require('child_process')
const Renderer = require('./renderer.js')

function Bot(canvas_name) {
    self = this
    this.bot = spawn('python3', ['-u', '../trade_MPL_Deleter', '-i'])
    this.charts = {}
    canvas_name.forEach(function (elem, i) {
        self.charts[i + 1] = new Renderer(elem)
    })
    this.bot.stdout.on('data', (data) => this.receive(data))
}

Bot.prototype.receive = function (data) {
    const j = JSON.parse(data)
    if (j.type === "pull") {
        this.charts[j.id].mergeData(j.data)
        this.charts[j.id].render()
    } else if (j.type === "balance") {
        document.getElementById("balance").value = j.data.balance
        document.getElementById("balance_1").value = j.data[1]
        document.getElementById("balance_2").value = j.data[2]
        document.getElementById("balance_3").value = j.data[3]
        document.getElementById("balance_4").value = j.data[4]
    } else {
        console.log("stdout: " + data)
    }
}

Bot.prototype.pause = function () {
    this.bot.stdin.write("pause\n")
}

Bot.prototype.resume = function () {
    this.bot.stdin.write("resume\n")
}

Bot.prototype.host = function (host) {
    this.bot.stdin.write("host " + host + "\n")
}

Bot.prototype.login = function () {
    this.bot.stdin.write("login\n")
}

Bot.prototype.reset_token = function () {
    this.bot.stdin.write("reset_token\n")
}

Bot.prototype.pull = function (marketplace_id, count, index) {
    this.bot.stdin.write("pull " + marketplace_id + " " + count + " " + index + "\n")
}

Bot.prototype.buy = function (marketplace_id, quantity) {
    this.bot.stdin.write("buy " + marketplace_id + " " + quantity + "\n")
}

Bot.prototype.sell = function (marketplace_id, quantity) {
    this.bot.stdin.write("sell " + marketplace_id + " " + quantity + "\n")
}

Bot.prototype.balance = function () {
    this.bot.stdin.write("balance\n")
}

module.exports = Bot