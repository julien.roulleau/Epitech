const Chart = require('chart.js')

function Renderer(canvas_name) {
    this.chart = new Chart(document.getElementById(canvas_name), {
        type: 'line',
        data: {},
        options: {
            tooltip: {
                yPadding: 10
            },
            animation: false
        }
    })
    this.data = []
    this.lastData = () => this.data[this.data.length - 1]
    this.maxValue = () => document.getElementById('max_value').value
}

Renderer.prototype.render = function () {
    this.chart.data = {
        labels: this.getLabels(),
        datasets: [{
            label: "values",
            data: this.getValues(),
            fill: false,
            backgroundColor: ['rgba(0, 0, 255, 0.2)'],
            borderColor: ['rgba(0, 0, 255, 0.4)']
        }]
    }
    this.chart.update()
}

Renderer.prototype.getLabels = function () {
    const labels = []
    const start_label = this.lastData().position -
        (this.maxValue() > this.data.length ? this.data.length : this.maxValue())
    for (let i = start_label > 0 ? start_label : 0; i < this.lastData().position; i++) {
        labels.push(i)
    }
    return labels
}

Renderer.prototype.getValues = function () {
    const values = []
    const start_value = this.data.length - this.maxValue()
    for (let i = start_value > 0 ? start_value : 0; i < this.data.length; i++) {
        values.push(this.data[i].value)
    }
    return values
}


Renderer.prototype.mergeData = function (data) {
    if (data instanceof Array)
        for (let elem of data) {
            if (!this.data.find(function (d) {
                    return d.position === elem.position
                }))
                this.data.push(elem)
        }
    else if (!this.data.find(function (d) {
            return d.position === data.position
        }))
        this.data.push(data)
    this.data.sort(function (a, b) {
        return a.position - b.position
    })
}

module.exports = Renderer