#!/usr/bin/env python3
from sys import argv

import requests
import json


def get_klines(symbol):
    payload = {"symbol": symbol, "interval": "1m"}
    while True:
        r = requests.get("https://api.binance.com/api/v1/klines", params=payload)
        r.raise_for_status()
        j = r.json()
        if len(j) == 0:
            break
        else:
            payload["endTime"] = j[0][0] - 1
            yield j


def get(symbol):
    klines = []
    for kline in get_klines(symbol):
        klines = kline + klines
    print(json.dumps(klines))


def display(path):
    with open(path) as file:
        j = json.load(file)
    print(j[1][0], sep="\n")


def reformat(path):
    with open(path) as file:
        j = json.load(file)

    print(json.dumps([round(abs(float(value[1]) + float(value[4])), 8) / 2 for value in j]))


if __name__ == '__main__':
    if len(argv) == 3 and argv[1] == "--get":
        get(argv[2])
    elif len(argv) == 3 and argv[1] == "--reformat":
        reformat(argv[2])
    elif len(argv) == 3 and argv[1] == "--display":
        display(argv[2])
