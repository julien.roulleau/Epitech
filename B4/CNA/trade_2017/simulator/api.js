"use strict"
const MarketPlace = require('./marketplace')
const crypto = require('crypto')

function generateToken() {
    return crypto.randomBytes(40).toString('hex')
}

function Api() {
    this.token = generateToken()
    this.balance = 10000
    this.market = [
        new MarketPlace('crypto', './Values/crypto.txt'),
        new MarketPlace('forex', './Values/forex.txt'),
        new MarketPlace('raw_material', './Values/raw_material.txt'),
        new MarketPlace('stock_exchange', './Values/stock_exchange.txt'),
    ]
}

Api.prototype.login = function (req, res) {
    console.log('Login')
    res.json({token: this.token})
}

Api.prototype.reset_token = function (req, res) {
    console.log('Reset Token')
    this.token = generateToken()
    res.json({token: this.token})
}

Api.prototype.pull = function (req, res) {
    console.log(`Pull: marketplace id: ${req.arg['marketplace_id']}, count: ${req.arg['count']}, index: ${req.arg['index']}`)
    const market = this.market[req.arg['marketplace_id'] - 1]
    if (market) {
        const values = market.getValue(req.arg['count'], req.arg['index'])
        if (values.length === 0)
            res.sendStatus(204)
        else
            res.json(values.length === 1 ? values[0] : values)
    } else {
        res.sendStatus(400)
    }
}

Api.prototype.buy = function (req, res) {
    console.log(`Buy: marketplace id: ${req.arg['marketplace_id']}, quantity: ${req.arg['quantity']}`)
    const market = this.market[req.arg['marketplace_id'] - 1]
    if (market && req.arg['quantity'] && this.balance - market.cost(req.arg['quantity']) >= 0) {
        this.balance -= market.cost(req.arg['quantity'])
        market.balance += req.arg['quantity']
        res.json({
            group: this.token,
            marketplace: req.arg['marketplace_id'],
            action: 'BUY',
            quantity: req.arg['quantity'],
            price: market.getPrice()
        })
    }
    else {
        res.sendStatus(400)
    }
}

Api.prototype.sell = function (req, res) {
    console.log(`Sell: marketplace id: ${req.arg['marketplace_id']}, quantity: ${req.arg['quantity']}`)
    const market = this.market[req.arg['marketplace_id'] - 1]
    if (market && req.arg['quantity'] && market.balance - req.arg['quantity'] >= 0) {
        this.balance += market.gain(req.arg['quantity'])
        market.balance -= req.arg['quantity']
        res.json({
            group: this.token,
            marketplace: req.arg['marketplace_id'],
            action: 'SELL',
            quantity: req.arg['quantity'],
            price: market.getPrice()
        })
    }
    else {
        res.sendStatus(400)
    }
}

Api.prototype.getBalance = function (req, res) {
    const value = {
        balance: this.balance,
        '1': this.market[0].balance,
        '2': this.market[1].balance,
        '3': this.market[2].balance,
        '4': this.market[3].balance,
    }
    console.log('Balance', value)
    res.json(value)
}

Api.prototype.end = function(req, res) {
    const self = this
    this.market.forEach(function (m) {
        self.balance += m.gain(m.balance)
        m.balance = 0
    })
    this.getBalance(req, res)
}

module.exports = Api