"use strict"
const express = require('express')
const app = express()
const Api = require('./api')
const api = new Api()

app.param('token', function (req, res, next) {
    if (req.params.token !== api.token) {
        res.sendStatus(400)
        return
    }
    next()
})

app.param('arg', function (req, res, next) {
    req.arg = {}
    for (let elem of req.params.arg.split('&')) {
        const part = elem.split('=')
        if (part.length !== 2) {
            res.sendStatus(400)
            return
        }
        const value = parseInt(part[1])
        if (isNaN(part[1]) || value <= 0) {
            res.sendStatus(400)
            return
        }
        req.arg[part[0]] = value
    }
    next()
})

app.get('/', (req, res) => res.send('{}'))
app.get('/login', (req, res) => api.login(req, res))
app.get('/reset_token', (req, res) => api.reset_token(req, res))
app.get('/:token/balance', (req, res) => api.getBalance(req, res))
app.get('/:token/pull/:arg', (req, res) => api.pull(req, res))
app.get('/:token/buy/:arg', (req, res) => api.buy(req, res))
app.get('/:token/sell/:arg', (req, res) => api.sell(req, res))
app.get('/:token/end', (req, res) => api.end(req, res))

app.listen(3000, () => console.log('Example app listening on port 3000!'))