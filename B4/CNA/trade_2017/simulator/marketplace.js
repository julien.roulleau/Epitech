"use strict"
const fs = require('fs')

function MarketPlace(name, path) {
    this.name = name
    this.position = 1
    this.values = fs.readFileSync(path, 'utf8').split("\n")
    this.balance = 0
}

MarketPlace.prototype.getContextValue = function (pos, index) {
    const values = []
    for (let value of this.values.slice(pos, index)) {
        values.push({
            marketplace: this.name,
            position: pos + 1,
            value: parseFloat(value)
        })
        pos++
    }
    return values
}

MarketPlace.prototype.getValue = function (count, index) {
    count = typeof count !== 'undefined' ? count : 1
    this.position++
    if (index) {
        if (index > this.position)
            index = this.position
        return this.getContextValue(
            index - count > 0 ? index - count : 0,
            index)
    }
    else {
        return this.getContextValue(
            this.position - count > 0 ? this.position - count : 0,
            this.position)
    }
}

MarketPlace.prototype.getPrice = function () {
    return parseFloat(this.values[(this.position < this.values.length ? this.position : this.values.length - 1)])
}

MarketPlace.prototype.cost = function (quantity) {
    const price = this.getPrice() * quantity
    return Math.abs(price + price * 0.0015)
}

MarketPlace.prototype.gain = function (quantity) {
    const price = this.getPrice() * quantity
    return Math.abs(price - price * 0.0015)
}

module.exports = MarketPlace
