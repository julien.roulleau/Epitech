#!/usr/bin/env python3
import sys
import json
from functools import reduce

def get_point(K, D):
    if len(K) < 5:
        return 0

    oldI = len(K) - 2
    while oldI >= 0 and K[oldI] == D[oldI]:
        oldI -= 1
    if K[-1] < D[-1] and K[oldI] > D[oldI] and (K[oldI] <= 25 or K[oldI] >= 65):
        return 1
    if K[-1] > D[-1] and K[oldI] < D[oldI] and (K[oldI] <= 25 or K[oldI] >= 65):
        return 2
    return 0


def stoc_osc(dv, K, D, diff=1):
    for i in range(diff):
        L = min(dv[len(dv) - 20:len(dv) - diff])
        H = max(dv[len(dv) - 20:len(dv) - diff])
        K.append(100 * (dv[len(dv) - diff] - L) / (H - L))
        if len(K) > 1:
            D.append(reduce(lambda x, y: x + y, K[len(K) - 3 if len(K) >= 3 else 0:len(K)]) / 3)
        else:
            D.append(0.0)
    return get_point(K, D)
