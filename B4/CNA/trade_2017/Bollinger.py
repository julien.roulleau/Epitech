import functools
import math


class Bollinger:
    def __init__(self, values, coef=0.):
        self.values = values
        self.coef = coef
        self.moving_average = self.ma()
        self.standard_deviation = self.sd()
        self.upper_bands = self.ub()
        self.lower_bands = self.lb()

    def ma(self):
        return functools.reduce(lambda x, y: x + y, self.values) / len(self.values)

    def sd(self):
        return math.sqrt(1 / len(self.values) *
                         functools.reduce(lambda x, y: x + y,
                                          map(lambda x: math.pow(x - self.moving_average, 2), self.values)))

    def ub(self):
        return self.moving_average + (self.standard_deviation * self.coef)

    def lb(self):
        return self.moving_average - (self.standard_deviation * self.coef)

