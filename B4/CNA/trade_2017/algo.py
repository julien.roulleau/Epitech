import json

from Bollinger import Bollinger


def is_float(value):
    try:
        float(value)
        return True
    except:
        return False


def run(api, i, *args, **kwargs):
    data = api.pull(i, 20)
    if data is None:
        return False
    if isinstance(data, dict):
        return True
    values = [x["value"] for x in data if is_float(x["value"])]
    if len(values) < 20:
        return True
    balance = api.balance()
    bollinger = Bollinger(values, 2)
    r = (values[-1] - bollinger.lower_bands) / (bollinger.upper_bands - bollinger.lower_bands)
    if (bollinger.lower_bands > values[-1] or 0 < r < 0.1)\
            and balance["balance"] > values[-1] * 5:
        try:
            api.buy(i, 1)
        except:
            return False
    if (bollinger.upper_bands < values[-1] or r > 0.9) and balance[str(i)] > 0:
        try:
            api.sell(i, balance[str(i)])
        except:
            return False
