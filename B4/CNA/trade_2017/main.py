#!/usr/bin/env python3
import requests
import sys
import Trade

from interactive import interactive
from algo import run


def display_help():
    print("Usage: {} [option]".format(sys.argv[0]))
    print()
    print("\t-i\t--interactive\t\tUse interactive mode")


options = {
    '-h': display_help,
    '--help': display_help,
    '-i': interactive,
    '--interactive': interactive,
}


def main(args):
    if len(args) > 1:
        option = options.get(args[1])
        if option is None:
            print("Invalid option", file=sys.stderr)
            exit(84)
        else:
            option()
    else:
        api = Trade.Api()
        api.run(run, 0)


if __name__ == '__main__':
    try:
        main(sys.argv)
    except requests.exceptions.HTTPError as e:
        print("Request fail: {}".format(e), file=sys.stderr)
        exit(84)
    except Trade.Error as e:
        print(e.msg, file=sys.stderr)
    else:
        exit(0)
