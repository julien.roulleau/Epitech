import json
from sys import stderr
from time import sleep

import requests


class Error(Exception):
    def __init__(self, msg):
        self.msg = msg


def format_params(args):
    return '&'.join(['{}={}'.format(key, value) for key, value in args.items()])


class Api:
    def __init__(self):
        self.host = "http://localhost:3000"
        self.token = ""
        self.login()
        self.running = True
        self.start = True

    def run(self, process, wait=1, *args, **kwargs):
        market_place_id = [i for i in range(1, 5)]
        while self.running:
            if self.start:
                if len(market_place_id) == 0:
                    self.end()
                for i in market_place_id:
                    if process(self, i, *args, **kwargs) is False:
                        balance = self.balance()
                        market_place_id.remove(i)
                        b = balance[str(i)]
                        if b > 0:
                            self.sell(i, b)
                balance = self.balance()
                print(json.dumps({"type": "balance", "data": balance}))
            sleep(wait)

    def login(self):
        r = requests.get("{}/login".format(self.host))
        r.raise_for_status()
        j = r.json()
        self.token = j.get('token')
        return self.token

    def reset_token(self):
        r = requests.get("{}/reset_token".format(self.host))
        r.raise_for_status()
        j = r.json()
        self.token = j.get('token')
        return self.token

    def pull(self, marketplace_id, count=1, index=-1):
        payload = {"marketplace_id": marketplace_id, "count": count}
        if index != -1:
            payload["index"] = index
        r = requests.get("{}/{}/pull/{}".format(self.host, self.token, format_params(payload)))
        r.raise_for_status()
        if r.status_code == 204:
            return None
        data = r.json()
        print(json.dumps({"id": marketplace_id, "type": "pull", "data": data}))
        return data

    def buy(self, marketplace_id, quantity):
        r = requests.get("{}/{}/buy/{}".format(self.host, self.token, format_params(
            {"marketplace_id": marketplace_id, "quantity": quantity})))
        r.raise_for_status()
        return r.json()

    def sell(self, marketplace_id, quantity):
        r = requests.get("{}/{}/sell/{}".format(self.host, self.token, format_params(
            {"marketplace_id": marketplace_id, "quantity": quantity})))
        r.raise_for_status()
        return r.json()

    def balance(self):
        r = requests.get("{}/{}/balance".format(self.host, self.token))
        r.raise_for_status()
        return r.json()

    def end(self):
        r = requests.get("{}/{}/end".format(self.host, self.token))
        r.raise_for_status()
        print(json.dumps({"type": "balance", "data": r.json()}))
        print("end", file=stderr)
        exit(0)
