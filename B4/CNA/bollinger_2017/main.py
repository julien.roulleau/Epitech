#!/usr/bin/env python3
import sys
import json
import functools
import math


class Bollinger:
    def __init__(self, values, coef=0.):
        self.values = values
        self.coef = coef
        self.moving_average = self.ma()
        self.standard_deviation = self.sd()
        self.upper_bands = self.ub()
        self.lower_bands = self.lb()

    def ma(self):
        return functools.reduce(lambda x, y: x + y, self.values) / len(self.values)

    def sd(self):
        return math.sqrt(1 / len(self.values) *
                         functools.reduce(lambda x, y: x + y,
                                          map(lambda x: math.pow(x - self.moving_average, 2), self.values)))

    def ub(self):
        return self.moving_average + (self.standard_deviation * self.coef)

    def lb(self):
        return self.moving_average - (self.standard_deviation * self.coef)


def main():
    if len(sys.argv) < 2:
        print("Invalid number of arguments")
        exit(84)
    if sys.argv[1] == "--all":
        around, sd_coef, path = int(sys.argv[2]), float(sys.argv[3]), sys.argv[4]
        print(json.dumps(compute_all(around, sd_coef, get_values(path))))
    elif len(sys.argv) == 2 and sys.argv[1] == "-h":
        help()
    else:
        if len(sys.argv) != 5:
            print("Invalid number of arguments")
            exit(84)
        if not sys.argv[1].isdigit() or not isfloat(sys.argv[2]) or not sys.argv[4].isdigit():
            print("Invalid arguments")
            exit(84)
        period, sd_coef, path, index = int(sys.argv[1]), float(sys.argv[2]), sys.argv[3], int(sys.argv[4])
        display(index, period, sd_coef, compute(period, sd_coef, index, get_values(path)))


def get_values(path):
    try:
        with open(path) as file:
            return [float(x) for x in file if isfloat(x)]
    except IOError:
        print("File not found")
        exit(84)


def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False


def compute_all(around, sd_coef, values):
    values_size = len(values)
    data = {'labels': [x for x in range(values_size)],
            'values': values,
            'moving_average': [],
            'standard_deviation': [],
            'upper_bands': [],
            'lower_bands': []}
    for i in range(values_size):
        start = i - around if i - around >= 0 else 0
        bollinger = Bollinger(values[start: i + 1 if i + 1 < values_size else i], sd_coef)
        data['moving_average'].append(bollinger.moving_average)
        data['standard_deviation'].append(bollinger.standard_deviation)
        data['upper_bands'].append(bollinger.upper_bands)
        data['lower_bands'].append(bollinger.lower_bands)
    return data


def compute(period, sd_coef, index, values):
    if not 0 <= index - period + 1 <= len(values) \
            or not 0 <= index + 1 <= len(values):
        print("Invalid index or period")
        exit(84)
    return Bollinger(values[index - period + 1:index + 1], sd_coef)


def display(index, period, sd_coef, bollinger):
    print("""INPUT
Index: {}
Period: {}
SD_coef: {:0.2f}

OUTPUT""".format(index, period, sd_coef))
    print("MA: {:0.2f}".format(bollinger.moving_average))
    print("SD: {:0.2f}".format(bollinger.standard_deviation))
    print("B+: {:0.2f}".format(bollinger.upper_bands))
    print("B-: {:0.2f}".format(bollinger.lower_bands))


def help():
    print("""Bollinger Bands

USAGE
    ./bollinger [-h] period standard_dev indexes_file index_number
    period          number of indexes for the moving average
    standard_dev    standard deviation coefficient to apply
    indexes_file    file containing daily indexes
    index_number    index number to compute moving average and Bollinger bands

OPTIONS
    -h              print the usage and quit.""")


main()
