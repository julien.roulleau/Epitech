#!/usr/bin/env python3
import sys
import json
from functools import reduce


def main():
    if len(sys.argv) != 3:
        print("Invalid number of arguments")
        exit(84)
    filename, period = sys.argv[1], int(sys.argv[2])
    dv = get_values(filename)
    if period != 0:
        dv = dv[(len(dv) - period):]
    return stoc_osc(dv)


def get_values(path):
    try:
        with open(path) as file:
            return [float(x) for x in file if isfloat(x)]
    except IOError:
        print("File not found")
        exit(84)


def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False


def get_point(K, D, dv):
    up = []
    down = []
    for i in range(len(K))[1:]:
        if K[i] < D[i] and K[i - 1] > D[i - 1] and (K[i] <= 20 or K[i] >= 80):
            down.append({"x": i, "y": dv[i]})
        if K[i] > D[i] and K[i - 1] < D[i - 1] and (K[i] <= 20 or K[i] >= 80):
            up.append({"x": i, "y": dv[i]})
    return {"down": down, "up": up}


def stoc_osc(dv):
    """
    Stochastic Oscillator
    :param dv: Data Value
    :type dv: DataFrame
    :return: Json
    """
    K = []
    D = []
    for i in range(len(dv))[1:]:
        RK = 14 if 14 < i else i
        RD = 3 if 3 < i else i
        L = min(dv[i - RK:i + 1])
        H = max(dv[i - RK:i + 1])
        K.append(100 * (dv[i] - L) / (H - L))
        if len(K) > 1:
            D.append(reduce(lambda x, y: x + y, K[len(K) - RD if len(K) >= RD else 0:len(K)]) / RD)
        else:
            D.append(0.0)
    print(json.dumps({
        "labels": (list(range(len(dv)))),
        "values": dv,
        "k": K,
        "d": D,
        "points": get_point(K, D, dv)
    }))
    return


exit(main())
