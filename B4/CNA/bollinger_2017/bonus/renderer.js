// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const Chart = require('chart.js')
const {exec} = require('child_process')

module.exports.bollinger = (canvas_name) => {
    return {
        chart: new Chart(document.getElementById(canvas_name), {
            type: 'line',
            data: {},
            options: {
                tooltip: {
                    yPadding: 10
                }
            }
        }),

        render(range, coef, file) {
            exec(`../main.py --all ${range} ${coef} ${file}`, (err, stdout, stderr) => {
                if (err) {
                    console.error(err)
                    console.error(stderr)
                    return
                }
                const obj = JSON.parse(stdout)

                exec(`./stoc_osc.py ${file} 0`, (err, stdout, stderr) => {
                    if (err) {
                        console.error(err)
                        console.error(stderr)
                        return
                    }
                    const points = JSON.parse(stdout).points

                    console.log(points)
                    this.chart.data = {
                        labels: obj.labels,
                        datasets: [{
                            label: "values",
                            data: obj.values,
                            fill: false,
                            backgroundColor: ['rgba(0, 0, 255, 0.2)'],
                            borderColor: ['rgba(0, 0, 255, 0.4)']
                        }, {
                            label: "Moving average",
                            data: obj.moving_average,
                            fill: '-1',
                            backgroundColor: ['rgba(0, 255, 0, 0.2)'],
                            borderColor: ['rgba(0, 255, 0, 0.4)']
                        }, {
                            label: "Upper bands",
                            data: obj.upper_bands,
                            fill: "end",
                            backgroundColor: ['rgba(255, 0, 0, 0.2)'],
                            borderColor: ['rgba(255, 0, 0, 0.4)']
                        }, {
                            label: "Lower bands",
                            data: obj.lower_bands,
                            fill: "start",
                            backgroundColor: ['rgba(255, 0, 0, 0.2)'],
                            borderColor: ['rgba(255, 0, 0, 0.4)']
                        }, {
                            label: "up",
                            data: points.up,
                            fill: false,
                            showLine: 0,
                            pointRadius: 10,
                            backgroundColor: ['rgba(0, 255, 0, 1)'],
                            borderColor: ['rgba(0, 255, 0, 1)']
                        }, {
                            label: "down",
                            data: points.down,
                            fill: false,
                            showLine: 0,
                            pointRadius: 10,
                            backgroundColor: ['rgba(255, 0, 0, 1)'],
                            borderColor: ['rgba(255, 0, 0, 1)']
                        }]
                    }
                    this.chart.update()
                })
            })
        }
    }
}

module.exports.stoc_osc = (canvas_name) => {
    return {
        chart: new Chart(document.getElementById(canvas_name), {
            type: 'line',
            data: {},
            options: {
                tooltip: {
                    yPadding: 10
                }
            }
        }),

        render(file) {
            exec(`./stoc_osc.py ${file} 0`, (err, stdout, stderr) => {
                if (err) {
                    console.error(err)
                    console.error(stderr)
                    return
                }
                const obj = JSON.parse(stdout)
                this.chart.data = {
                    labels: obj.labels,
                    datasets: [{
                        label: "Stochastic Oscillator : K",
                        data: obj.k,
                        fill: false,
                        backgroundColor: ['rgba(0, 255, 0, 0.2)'],
                        borderColor: ['rgba(0, 255, 0, 0.4)']
                    }, {
                        label: "Stochastic Oscillator : D",
                        data: obj.d,
                        fill: false,
                        backgroundColor: ['rgba(255, 0, 0, 0.2)'],
                        borderColor: ['rgba(255, 0, 0, 0.4)']
                    }]
                }
                this.chart.update()
            })
        }
    }
}