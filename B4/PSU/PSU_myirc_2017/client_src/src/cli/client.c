/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** client.c
*/

#include <arpa/inet.h>
#include <stdlib.h>
#include "sender/sender.h"
#include "receiver/receiver.h"
#include "client.h"

bool run(client_t *client)
{
	fd_set fds;

	while (client->running) {
		FD_ZERO(&fds);
		if (client->sock != -1)
			FD_SET(client->sock, &fds);
		FD_SET(0, &fds);
		if (select((client->sock != -1 ? client->sock : 0) + 1,
			&fds, NULL, NULL, NULL) == -1)
			return (false);
		if (FD_ISSET(0, &fds) && !sender(client))
			return (false);
		if (client->sock != -1)
			if (FD_ISSET(client->sock, &fds) && !receiver(client))
				return (false);
	}
	return (true);
}