/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** client.h
*/

#ifndef MYIRC_CLIEN_CLIENT_H
#define MYIRC_CLIEN_CLIENT_H

#include <stdbool.h>
#include "list.h"

typedef struct {
	bool running;
	int sock;
	bool reg;
	char *old_nick;
	char *nick;
	char *name;
	char *real_name;
	char *chan;
	char *serv;
	int port;
	list_t *channels;
} client_t;

bool run(client_t *client);

#endif //MYIRC_CLIENT_H
