/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** main.c
*/

#include <zconf.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "connect/connect.h"
#include "cli/client.h"

static bool set_client(client_t *client)
{
	client->running = true;
	client->sock = -1;
	client->reg = false;
	client->nick = NULL;
	client->name = NULL;
	client->real_name = NULL;
	client->old_nick = NULL;
	client->serv = NULL;
	client->port = -1;
	client->channels = create_list(LINKED);
	client->chan = NULL;
	return (true);
}

static void free_client(client_t *clt)
{
	if (clt->nick) {
		free(clt->nick);
		free(clt->name);
		free(clt->real_name);
	}
	if (clt->old_nick)
		free(clt->old_nick);
	if (clt->serv)
		free(clt->serv);
	if (clt->channels->first) {
		FOREACH(node_t, clt->channels->first) {
			free(item->data);
			item->data = NULL;
		}
	}
	free_list(clt->channels);
}

static int printt_help(void)
{
	printf("USAGE: ./client\n\n"
		"- Command list and functionality:\n"
		"/server $host[:$port]\t- connects to a server\n"
		"/nick $nickname\t\t- define / modifie de user's nickname\n"
		"/list $string\t\t- lists the server’s available channels\n"
		"/join $channel\t\t- joins a server's channel\n"
		"/part $channel\t\t- leave a channel\n"
		"/move $channel\t\t- change the current channel\n"
		"$message\t\t- send a message to all the users that are "
			"connected to the current channel\n"
		"/users\t\t\t- list the nickname of the users that are "
			"connected to the server\n"
		"/names\t\t\t- list the nickname of the users that are"
			" connected to the channel\n"
		"/msg $nick $msg\t\t- send a message to a specific user\n"
		"/help\t\t\t- display help\n"
		"/info\t\t\t- print client informations\n\n"
		"To be identifie on a server, both '/server' ans '/nick' "
			"have to be done, whatever the order.\n");
	return (0);
}


int main(int ac, char **av)
{
	client_t client;

	if (ac == 2 && !strcmp(av[1], "-help"))
		return (printt_help());
	else if (ac != 1)
		return (84);
	set_client(&client);
	if (!run(&client))
		return (84);
	if (client.sock != -1)
		close(client.sock);
	free_client(&client);
	return (0);
}