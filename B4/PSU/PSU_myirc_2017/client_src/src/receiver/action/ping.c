/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** pong.c
*/

#include "action.h"

bool ping(client_t *clt, char *prefix, char *opt)
{
	(void)prefix;
	(void)opt;
	dprintf(clt->sock, "PONG :%s\r\n", clt->serv);
	return (true);
}