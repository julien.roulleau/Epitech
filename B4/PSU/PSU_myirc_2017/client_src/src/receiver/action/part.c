/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** part.c
*/

#include <string.h>
#include "action.h"

bool part(client_t *clt, char *prefix, char *opt)
{
	char *pref = prefix ? strchr(prefix, '!') : NULL;

	(void)clt;
	if (pref)
		pref[0] = 0;
	if (opt && opt[0] == ':')
		opt++;
	if (prefix && prefix[0] == ':')
		prefix++;
	printf("%s leave the channel %s\n",
		prefix ? prefix : "", opt ? opt : "");
	return (true);
}
