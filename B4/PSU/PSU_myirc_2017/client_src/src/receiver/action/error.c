/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** error.c
*/

#include "action.h"

bool error(client_t *clt, char *prefix, char *opt)
{
	(void)prefix;
	clt->running = false;
	printf("Error : %s\n", opt[0] == ':' ? opt + 1 : opt);
	return (true);
}
