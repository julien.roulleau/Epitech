/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** num_433_432_431.c
*/

#include <stdlib.h>
#include <string.h>
#include "action.h"

bool num_433_432_431(client_t *clt, char *prefix, char *option)
{
	(void)prefix;
	free(clt->nick);
	clt->nick = strdup(clt->old_nick);
	printf("Error: %s\n", option);
	return (true);
}
