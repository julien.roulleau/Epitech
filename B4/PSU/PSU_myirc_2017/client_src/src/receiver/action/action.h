/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** action.h
*/

#ifndef MYIRC_GET_H
#define MYIRC_GET_H

#include <stdbool.h>
#include <stdio.h>
#include "cli/client.h"

typedef struct simple_react_s {
	struct simple_react_s *next;
	char *code;
	char *react;
} simple_react_t;

typedef struct receive_func_list_s {
	char *flag;
	bool (*func)(client_t *client, char *prefix, char *option);
	struct receive_func_list_s *next;
} receive_func_list_t;

bool join(client_t *client, char *prefix, char *option);
bool privmsg(client_t *client, char *prefix, char *option);
bool part(client_t *clt, char *prefix, char *opt);
bool ping(client_t *clt, char *prefix, char *opt);
bool quit(client_t *clt, char *prefix, char *opt);
bool error(client_t *clt, char *prefix, char *opt);

bool num_001(client_t *client, char *prefix, char *option);
bool num_433_432_431(client_t *clt, char *prefix, char *option);
bool num_simple_print(client_t *client, char *prefix, char *option);

#endif //MYIRC_GET_H
