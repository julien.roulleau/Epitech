/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** quit.c
*/

#include <string.h>
#include "action.h"

bool quit(client_t *clt, char *prefix, char *opt)
{
	char *pref = prefix ? strchr(prefix, '!') : NULL;

	(void)clt;
	if (pref)
		pref[0] = 0;
	if (opt && opt[0] == ':')
		opt++;
	if (prefix && prefix[0] == ':')
		prefix++;
	printf("%s quit(%s)\n", prefix ? prefix : "", opt ? opt : "");
	return (true);
}