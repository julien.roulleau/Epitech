/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** privmsg.c
*/

#include <string.h>
#include "action.h"

bool privmsg(client_t *client, char *prefix, char *option)
{
	char *from = prefix + 1;
	char *dest = option;
	char *msg = strchr(option, ':') + 1;

	(void)client;
	for (int i = 0; from[i]; i++)
		if (from[i] == '!')
			from[i] = 0;
	for (int i = 0; dest[i]; i++)
		if (dest[i] == ' ') {
			dest[i] = 0;
			break;
		}
	printf("%s->%s >%s\n", from, dest, msg);
	return (true);
}
