/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** num_001.c
*/

#include <string.h>
#include "action.h"

bool num_001(client_t *client, char *prefix, char *option)
{
	(void)prefix;
	client->reg = true;
	printf("%s\n", strchr(option, ':') ? strchr(option, ':') + 1 : option);
	return (true);
}