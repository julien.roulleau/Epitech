/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** join.c
*/

#include <string.h>
#include "action.h"

static bool add_chan(client_t *clt, char *chan, char *prefix)
{
	char *new = strdup(chan);

	if (!new)
		exit(84);
	push_back(clt->channels, new);
	if (clt->channels->size == 1)
		clt->chan = new;
	if (clt->channels->size == 0)
		exit(84);
	printf("%s join the channel : %s\n", prefix + 1, new);
	return (true);
}

bool join(client_t *clt, char *prefix, char *opt)
{
	if (!opt)
		return (true);
	opt = opt[0] == ':' ? opt + 1: opt;
	add_chan(clt, opt, prefix);
	(void) prefix;
	return (true);
}