/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** receiver.h
*/

#ifndef MYIRC_RECEIVER_H
#define MYIRC_RECEIVER_H

#include <stdbool.h>
#include "action/action.h"

bool receiver(client_t *client);

#endif //MYIRC_RECEIVER_H
