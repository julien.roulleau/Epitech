/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** receiver.c
*/

#include <string.h>
#include <stdio.h>
#include <zconf.h>
#include "utils.h"
#include "list.h"
#include "receiver.h"

static simple_react_t NUM_REACT[] = {
	{.next = NULL, .code = "000", .react = "ERROR"}
};

static receive_func_list_t CMD_LIST[] = {
	{.next = &CMD_LIST[1], .flag = "JOIN", .func = &join},
	{.next = &CMD_LIST[2], .flag = "001", .func = &num_001},
	{.next = &CMD_LIST[3], .flag = "431", .func = &num_433_432_431},
	{.next = &CMD_LIST[4], .flag = "432", .func = &num_433_432_431},
	{.next = &CMD_LIST[5], .flag = "433", .func = &num_433_432_431},
	{.next = &CMD_LIST[6], .flag = "332", .func = &num_simple_print},
	{.next = &CMD_LIST[7], .flag = "353", .func = &num_simple_print},
	{.next = &CMD_LIST[8], .flag = "401", .func = &num_simple_print},
	{.next = &CMD_LIST[9], .flag = "402", .func = &num_simple_print},
	{.next = &CMD_LIST[10], .flag = "403", .func = &num_simple_print},
	{.next = &CMD_LIST[11], .flag = "404", .func = &num_simple_print},
	{.next = &CMD_LIST[12], .flag = "405", .func = &num_simple_print},
	{.next = &CMD_LIST[13], .flag = "465", .func = &num_simple_print},
	{.next = &CMD_LIST[14], .flag = "466", .func = &num_simple_print},
	{.next = &CMD_LIST[15], .flag = "467", .func = &num_simple_print},
	{.next = &CMD_LIST[16], .flag = "471", .func = &num_simple_print},
	{.next = &CMD_LIST[17], .flag = "472", .func = &num_simple_print},
	{.next = &CMD_LIST[18], .flag = "473", .func = &num_simple_print},
	{.next = &CMD_LIST[19], .flag = "474", .func = &num_simple_print},
	{.next = &CMD_LIST[20], .flag = "475", .func = &num_simple_print},
	{.next = &CMD_LIST[21], .flag = "476", .func = &num_simple_print},
	{.next = &CMD_LIST[22], .flag = "477", .func = &num_simple_print},
	{.next = &CMD_LIST[23], .flag = "478", .func = &num_simple_print},
	{.next = &CMD_LIST[24], .flag = "481", .func = &num_simple_print},
	{.next = &CMD_LIST[25], .flag = "482", .func = &num_simple_print},
	{.next = &CMD_LIST[26], .flag = "461", .func = &num_simple_print},
	{.next = &CMD_LIST[27], .flag = "462", .func = &num_simple_print},
	{.next = &CMD_LIST[28], .flag = "451", .func = &num_simple_print},
	{.next = &CMD_LIST[29], .flag = "446", .func = &num_simple_print},
	{.next = &CMD_LIST[30], .flag = "463", .func = &num_simple_print},
	{.next = &CMD_LIST[31], .flag = "464", .func = &num_simple_print},
	{.next = &CMD_LIST[32], .flag = "436", .func = &num_simple_print},
	{.next = &CMD_LIST[33], .flag = "437", .func = &num_simple_print},
	{.next = &CMD_LIST[34], .flag = "441", .func = &num_simple_print},
	{.next = &CMD_LIST[35], .flag = "442", .func = &num_simple_print},
	{.next = &CMD_LIST[36], .flag = "443", .func = &num_simple_print},
	{.next = &CMD_LIST[37], .flag = "444", .func = &num_simple_print},
	{.next = &CMD_LIST[38], .flag = "PART", .func = &part},
	{.next = &CMD_LIST[39], .flag = "PING", .func = &ping},
	{.next = &CMD_LIST[40], .flag = "ERROR", .func = &error},
	{.next = &CMD_LIST[41], .flag = "QUIT", .func = &quit},
	{.next = &CMD_LIST[42], .flag = "322", .func = &num_simple_print},
	{.next = &CMD_LIST[43], .flag = "393", .func = &num_simple_print},
	{.next = &CMD_LIST[44], .flag = "395", .func = &num_simple_print},
	{.next = NULL, .flag = "PRIVMSG", .func = &privmsg},
};

static char *cut_buffer(char *buffer, char sep)
{
	char *tmp;

	if (!buffer)
		return (NULL);
	tmp = index(buffer, sep);
	if (tmp) {
		*tmp = 0;
		tmp++;
		return (tmp);
	} else {
		return (NULL);
	}
}

static bool interpreter(client_t *client, char *buffer)
{
	char *prefix = buffer;
	char *cmd = buffer;
	char *option;

	if (buffer[0] == ':')
		cmd = cut_buffer(prefix, ' ');
	else
		prefix = NULL;
	option = cut_buffer(cmd, ' ');
	if (!cmd)
		return (false);
	FOREACH(receive_func_list_t, CMD_LIST)
		if (!strcasecmp(item->flag, cmd))
			return (item->func(client, prefix, option));
	FOREACH(simple_react_t, NUM_REACT)
		if (!strcasecmp(item->code, cmd))
			printf("%s\n", item->react);
	return (true);
}

bool receiver(client_t *client)
{
	char *buffer = gnl(client->sock);

	if (!buffer) {
		if (close(client->sock) == -1)
			return (false);
		client->sock = -1;
		return (true);
	}
	if (buffer[strlen(buffer) - 1] == '\r')
		buffer[strlen(buffer) - 1] = 0;
	if (!interpreter(client, buffer))
		return (false);
	free(buffer);
	return (true);
}
