/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** socket.c
*/

#include <arpa/inet.h>
#include <stdio.h>
#include "connect.h"

int connect_to_server(char *ip, int port)
{
	int sock = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in sin;

	if (sock == -1 || port < 0) {
		perror("socket");
		return (-1);
	}
	sin.sin_addr.s_addr = inet_addr(ip);
	sin.sin_family = AF_INET;
	sin.sin_port = htons((uint16_t) port);
	if (connect(sock, (struct sockaddr *) &sin, sizeof(sin)) == -1) {
		perror("connect");
		return (-1);
	}
	return (sock);
}
