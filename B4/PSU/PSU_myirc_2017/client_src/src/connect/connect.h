/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** socket.h
*/

#ifndef MYIRC_SOCKET_H
#define MYIRC_SOCKET_H

int connect_to_server(char *ip, int port);

#endif //MYIRC_SOCKET_H
