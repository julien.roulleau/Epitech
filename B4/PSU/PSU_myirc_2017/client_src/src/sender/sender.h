/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** sender.h
*/

#ifndef MYIRC_SENDER_H
#define MYIRC_SENDER_H

#include <stdbool.h>
#include "action/action.h"

bool sender(client_t *client);

#endif //MYIRC_SENDER_H
