/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** sender.c
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "list.h"
#include "sender.h"

static send_func_list_t CMD_LIST[] = {
	{.next = &CMD_LIST[1], .flag = "/accept_file",
		.func = &cmd_accept_file},
	{.next = &CMD_LIST[2], .flag = "/join", .func = &cmd_join},
	{.next = &CMD_LIST[3], .flag = "/list", .func = &cmd_list},
	{.next = &CMD_LIST[4], .flag = "/msg", .func = &cmd_msg},
	{.next = &CMD_LIST[5], .flag = "/names", .func = &cmd_names},
	{.next = &CMD_LIST[6], .flag = "/nick", .func = &cmd_nick},
	{.next = &CMD_LIST[7], .flag = "/part", .func = &cmd_part},
	{.next = &CMD_LIST[8], .flag = "/server", .func = &cmd_server},
	{.next = &CMD_LIST[9], .flag = "/move", .func = &cmd_move},
	{.next = &CMD_LIST[10], .flag = "/quit", .func = &cmd_quit},
	{.next = &CMD_LIST[11], .flag = "/info", .func = &cmd_info},
	{.next = &CMD_LIST[12], .flag = "/help", .func = &cmd_help},
	{.next = NULL, .flag = "/users", .func = &cmd_users},
};

static char *clear_buffer(char *buffer)
{
	while (buffer[0] && buffer[0] == ' ')
		buffer++;
	for (size_t i = strlen(buffer) - 1; i > 0 && buffer[i] == ' '; i--)
		buffer[i] = 0;
	if (buffer[strlen(buffer) - 1] == '\n')
		buffer[strlen(buffer) - 1] = 0;
	return (buffer);
}

static bool interpreter(client_t *client, char *buffer)
{
	char *buff_cpy = strdup(buffer);
	char *cmd = buff_cpy;
	char *option = index(buff_cpy, ' ');

	if (!buff_cpy)
		return (false);
	if (!option)
		option = "";
	else if (strlen(option)) {
		*option = 0;
		option++;
	}
	FOREACH(send_func_list_t, CMD_LIST) {
		if (!strcasecmp(item->flag, cmd)) {
			return (item->func(client, option));
		}
	}
	free(buff_cpy);
	return (cmd_default(client, buffer));
}

bool sender(client_t *client)
{
	char *buffer = NULL;
	char *buf = NULL;
	size_t len = 0;

	if (getline(&buffer, &len, stdin) == -1)
		return (perror("getline"), false);
	buf = clear_buffer(buffer);
	if (!interpreter(client, buf))
		return (false);
	free(buffer);
	buffer = NULL;
	return (true);
}
