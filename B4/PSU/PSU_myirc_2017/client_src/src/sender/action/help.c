/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** help.c
*/

#include "action.h"

bool cmd_help(client_t *clt, char *option)
{
	(void)option;
	printf("HELP: Command list and functionality:\n"
	       "/server $host[:$port]\t- connects to a server\n"
	       "/nick $nickname\t\t- define / modifie de user's nickname\n"
	       "/list $string\t\t- lists the server’s available channels\n"
	       "/join $channel\t\t- joins a server's channel\n"
	       "/part $channel\t\t- leave a channel\n"
	       "/move $channel\t\t- change the current channel\n"
	       "$message\t\t- send a message to all the users that are "
	       "connected to the current channel\n"
	       "/users\t\t\t- list the nickname of the users that are "
	       "connected to the server\n"
	       "/names\t\t\t- list the nickname of the users that are"
	       " connected to the channel\n"
	       "/msg $nick $msg\t\t- send a message to a specific user\n"
	       "/help\t\t\t- display help\n"
	       "/info\t\t\t- print client informations\n\n"
	       "To be identifie on a server, both '/server' ans '/nick' "
	       "have to be done, whatever the order.\n");
	return (clt->running);
}
