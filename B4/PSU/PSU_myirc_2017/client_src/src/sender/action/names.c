/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** names.c
*/

#include "action.h"

static bool ret_msg(char *msg, bool ret)
{
	printf("%s\n", msg);
	return (ret);
}

bool cmd_names(client_t *clt, char *opt)
{
	if (clt->sock == -1)
		return (ret_msg("You have to be connected to a server.", true));
	if (opt)
		dprintf(clt->sock, "NAMES %s\r\n", opt);
	else
		dprintf(clt->sock, "NAMES\r\n");
	return (true);
}
