/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** msg.c
*/

#include <string.h>
#include "action.h"

static bool ret_msg(char *msg, bool ret)
{
	printf("%s\n", msg);
	return (ret);
}

bool cmd_msg(client_t *clt, char *option)
{
	char *msg = strchr(option, ' ');

	if (clt->sock == -1)
		return (ret_msg("You have to be connected to a server.", true));
	if (!msg)
		return (ret_msg("No message to send.", true));
	msg[0] = 0;
	msg++;
	dprintf(clt->sock, "PRIVMSG %s :%s\r\n", option, msg);
	printf("%s->%s >%s\n", clt->nick, option, msg);
	return (true);
}
