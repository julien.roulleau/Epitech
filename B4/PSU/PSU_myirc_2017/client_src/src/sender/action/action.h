/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** cmd.h
*/

#ifndef MYIRC_CMD_H
#define MYIRC_CMD_H

#include <stdbool.h>
#include <stdio.h>
#include "cli/client.h"

#define CHAR_IS_NICK(c) ((c <= '9' && c >= '0') || (c <= 'z' && c >= 'a') ||\
(c <= 'Z' && c >= 'A') || c == '-' || c == '[' || c == ']' ||\
c == '\\' || c == '^' || c == '{' || c == '}' || c == ' ')

typedef struct send_func_list_s {
	char *flag;
	bool (*func)(client_t *client, char *option);
	struct send_func_list_s *next;
} send_func_list_t;

bool cmd_accept_file(client_t *client, char *option);
bool cmd_join(client_t *client, char *option);
bool cmd_list(client_t *client, char *option);
bool cmd_msg(client_t *client, char *option);
bool cmd_names(client_t *client, char *option);
bool cmd_nick(client_t *client, char *option);
bool cmd_part(client_t *client, char *option);
bool cmd_server(client_t *client, char *option);
bool cmd_users(client_t *client, char *option);
bool cmd_default(client_t *client, char *option);
bool cmd_move(client_t *clt, char *opt);
bool cmd_quit(client_t *clt, char *opt);
bool cmd_info(client_t *clt, char *option);
bool cmd_help(client_t *clt, char *option);
#endif //MYIRC_CMD_H
