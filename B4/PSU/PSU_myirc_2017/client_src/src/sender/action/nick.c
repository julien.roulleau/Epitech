/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** nick.c
*/

#include <string.h>
#include <stdlib.h>
#include "action.h"

static bool nick_is_valid(char *nick)
{
	for (int i = 0; nick[i]; i++)
		if (!CHAR_IS_NICK(nick[i]))
			return (false);
	return (true);
}

static bool ret_msg(char *msg, bool ret)
{
	printf("%s\n", msg);
	return (ret);
}

static void first_nick(client_t *clt, char *opt)
{
	clt->nick = strdup(opt);
	clt->name = strdup(opt);
	clt->real_name = strdup(opt);
	if (!clt->nick || !clt->name || !clt->real_name)
		exit(84);
}

bool cmd_nick(client_t *clt, char *opt)
{
	if (!opt)
		return (ret_msg("USAGE: '/server $host[:$port]'", true));
	if (!nick_is_valid(opt))
		return (ret_msg("Bad nickname.", true));
	if (!clt->nick)
		first_nick(clt, opt);
	else if (!strcmp(clt->nick, opt))
		return (ret_msg("Same nick.", true));
	if (clt->old_nick)
		free(clt->old_nick);
	clt->old_nick = clt->nick;
	clt->nick = strdup(opt);
	if (clt->sock != -1)
		dprintf(clt->sock, "NICK %s\r\n", clt->nick);
	if (clt->sock != -1 && !clt->reg) {
		dprintf(clt->sock, "USER %s unknow unknow :%s\r\n",
			clt->name, clt->real_name);
		clt->reg = true;
	}
	return (true);
}
