/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** default.c
*/

#include "action.h"

static bool ret_msg(char *msg, bool ret)
{
	printf("%s\n", msg);
	return (ret);
}

bool cmd_default(client_t *clt, char *option)
{
	if (!option[0])
		return (true);
	if (option[0] == '/')
		return (ret_msg("Unknow command", true));
	if (clt->sock == -1)
		return (ret_msg("You have to be connected to a server.", true));
	if (!clt->chan)
		return (ret_msg("You are not on any channel.", true));
	dprintf(clt->sock, "PRIVMSG %s :%s\r\n", clt->chan, option);
	printf("%s->%s >%s\n", clt->nick, clt->chan, option);
	return (true);
}
