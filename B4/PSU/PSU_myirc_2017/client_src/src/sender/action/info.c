/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** info.c
*/

#include "action.h"

bool cmd_info(client_t *clt, char *option)
{
	(void)option;
	printf("--- Client Informations ---\n");
	printf("Nick :\t\t[%s]\n", clt->nick ? clt->nick : " ");
	printf("Name :\t\t[%s]\n", clt->name ? clt->name : " ");
	if (clt->port != -1 && clt->serv)
		printf("Connected to :\t[%s:%i]\n", clt->serv, clt->port);
	else
		printf("You aren't connected to a server.\n");
	if (clt->channels->size) {
		printf("\nYours channels :\n");
		FOREACH(node_t, clt->channels->first) {
			if (item->data == clt->chan)
				printf("> %s\n", (char*)item->data);
			else
				printf("- %s\n", (char*)item->data);
		}
	} else
		printf("You aren't connected on any channel.\n");
	return (true);
}