/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** move.c
*/

#include <string.h>
#include "action.h"

static bool ret_msg(char *msg, bool ret)
{
	printf("%s\n", msg);
	return (ret);
}

bool cmd_move(client_t *clt, char *opt)
{
	if (clt->sock == -1)
		return (ret_msg("You have to be connected to a server.", true));
	if (!opt)
		return (ret_msg("USAGE: /move $channel", true));
	if (clt->channels->first) {
		FOREACH(node_t, clt->channels->first) {
			if (!strcmp(opt, item->data)) {
				clt->chan = item->data;
				printf("The messages will now be "
					"sent on the channel %s.\n", clt->chan);
				return (true);
			}
		}
	}
	printf("The channel %s doesn't exist or you aren't connected on it\n",
		opt);
	return (true);
}
