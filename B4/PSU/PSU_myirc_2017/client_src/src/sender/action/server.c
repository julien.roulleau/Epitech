/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** server.c
*/

#include <string.h>
#include <stdlib.h>
#include "action.h"
#include "connect/connect.h"

static bool ret_msg(char *msg, bool ret)
{
	printf("%s\n", msg);
	return (ret);
}

bool cmd_server(client_t *clt, char *opt)
{
	char *port = opt ? strchr(opt, ':') : NULL;

	if (!opt)
		return (ret_msg("USAGE: /server $host[:$port]", true));
	if (clt->sock != -1)
		return (ret_msg("You are already connect to a server.", true));
	if (port)
		port[0] = 0;
	clt->sock = connect_to_server(opt, (port ? atoi(port + 1) : 6667));
	clt->port = port ? atoi(port + 1) : 6667;
	if (clt->sock == -1)
		return (ret_msg("Connection fail: Try again !", true));
	if (clt->nick) {
		dprintf(clt->sock, "NICK %s\r\n", clt->nick);
		dprintf(clt->sock, "USER %s unknow unknow :%s\r\n",
			clt->name, clt->real_name);
	}
	clt->serv = strdup(opt);
	return (ret_msg("Connection success!", true));
}
