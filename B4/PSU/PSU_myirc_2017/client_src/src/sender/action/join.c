/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** join.c
*/

#include <string.h>
#include "action.h"

static bool chan_is_valid(char *name)
{
	if (name[0] != '&' && name[0] != '#')
		return (false);
	if (strchr(name, ' '))
		return (false);
	return (true);
}

static bool ret_msg(char *msg, bool ret)
{
	printf("%s\n", msg);
	return (ret);
}

bool cmd_join(client_t *clt, char *opt)
{
	if (clt->sock == -1)
		return (ret_msg("You have to be connected to a server.", true));
	if (!opt)
		return (ret_msg("USAGE: /join $channel", true));
	if (!chan_is_valid(opt))
		return (ret_msg("Bad channel name.", true));
	dprintf(clt->sock, "JOIN %s\r\n", opt);
	return (true);
}
