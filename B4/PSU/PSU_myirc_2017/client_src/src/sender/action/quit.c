/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** quit.c
*/

#include "action.h"

bool cmd_quit(client_t *clt, char *opt)
{
	if (clt->sock != -1 && opt)
		dprintf(clt->sock, "QUIT :%s\r\n", opt);
	else if (clt->sock != -1)
		dprintf(clt->sock, "QUIT\r\n");
	clt->running = false;
	return (true);
}
