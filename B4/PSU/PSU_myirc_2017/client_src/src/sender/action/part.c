/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** part.c
*/

#include <string.h>
#include <cli/client.h>
#include "action.h"

static bool ret_msg(char *msg, bool ret)
{
	printf("%s\n", msg);
	return (ret);
}

static bool leave_chan(client_t *clt, char *opt)
{
	FOREACH(node_t, clt->channels->first) {
		if (!strcmp(item->data, opt)) {
			if (item->data == clt->chan)
				clt->chan = NULL;
			free(item->data);
			free_node(clt->channels, item);
			break;
		}
	}
	if (!clt->chan && clt->channels->size)
		clt->chan = clt->channels->first->data;
	return (true);
}

bool cmd_part(client_t *clt, char *opt)
{
	char *msg = opt ? strchr(opt, ' ') : NULL;

	if (clt->sock == -1)
		return (ret_msg("You have to be connected to a server.", true));
	if (!opt)
		return (ret_msg("USAGE: /part $channel", true));
	if (msg) {
		msg[0] = 0;
		msg += 1;
	}
	leave_chan(clt, opt);
	if (msg)
		dprintf(clt->sock, "PART %s :%s\r\n", opt, msg);
	else
		dprintf(clt->sock, "PART %s\r\n", opt);
	return (true);
}