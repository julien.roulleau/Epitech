/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** receive.cpp
*/

#include <unistd.h>
#include "utils.h"

bool data(int dest, int src)
{
	char *buff[4048];
	ssize_t size;

	do {
		size = read(src, &buff, sizeof(buff));
		if (size == 0)
			break;
		if (write(dest, buff, (size_t) size) == -1)
			return (false);
	} while (true);
	return (true);
}