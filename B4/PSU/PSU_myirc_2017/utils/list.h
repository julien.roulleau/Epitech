/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** list.h
*/

#ifndef MYIRC_LIST_H
#define MYIRC_LIST_H

#include <stdlib.h>
#include <stdbool.h>

typedef enum {
	LINKED, CIRCULAR
} type_list_t;

typedef struct s_node {
	void *data;
	struct s_node *previous;
	struct s_node *next;
} node_t;

typedef struct s_list {
	node_t *first;
	node_t *last;
	type_list_t type;
	int size;
} list_t;

#define FOREACH(type, list) for (type *item = list; item; item = item->next)

list_t *create_list(type_list_t);
bool free_node(list_t *, node_t *);
bool clean_list(list_t *list);
bool free_list(list_t *);

bool push_front(list_t *, void *);
bool push_back(list_t *, void *);

void free_tab(void **tab, int size);
void **tab_list(list_t *list);

bool exec_list(list_t *list, bool(*func)(void *));

#endif //MYIRC_LIST_H
