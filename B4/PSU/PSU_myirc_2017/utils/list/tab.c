/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** tab.c
*/

#include "list.h"

void free_tab(void **tab, int size)
{
	int i = -1;

	while (++i < size)
		free(tab[i]);
	free(tab);
}

void **tab_list(list_t *list)
{
	node_t *node;
	void **tab;
	int i = -1;

	if (!list || !list->first)
		return (NULL);
	tab = malloc(sizeof(void *) * (list->size + 1));
	if (!tab)
		return (NULL);
	node = list->first;
	while (++i < list->size) {
		tab[i] = node->data;
		if (!node->next) {
			free_tab(tab, i);
			return (NULL);
		}
		node = node->next;
	}
	tab[list->size] = 0;
	return (tab);
}
