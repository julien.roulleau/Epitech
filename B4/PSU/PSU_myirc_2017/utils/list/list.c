/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** list.c
*/

#include "list.h"

list_t *create_list(type_list_t type)
{
	list_t *list = malloc(sizeof(list_t));

	if (!list)
		return (NULL);
	list->first = NULL;
	list->last = NULL;
	list->type = type;
	list->size = 0;
	return (list);
}

bool free_node(list_t *list, node_t *node)
{
	if (!list || !node)
		return (false);
	if (node == list->first)
		list->first = node->next ? node->next : 0;
	if (node == list->last)
		list->last = node->previous ? node->previous : 0;
	if (node->previous)
		node->previous->next = node->next ? node->next : 0;
	if (node->next)
		node->next->previous = node->previous ? node->previous : 0;
	free(node);
	list->size -= 1;
	return (true);
}

bool clean_list(list_t *list)
{
	int i;

	if (!list)
		return (false);
	i = -1;
	while (i++ < list->size)
		if (!free_node(list, list->first))
			return (false);
	return (true);
}

bool free_list(list_t *list)
{
	if (!list || !clean_list(list))
		return (false);
	free(list);
	return (true);
}
