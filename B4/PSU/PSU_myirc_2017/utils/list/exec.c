/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** exec.c
*/

#include "list.h"

bool exec_list(list_t *list, bool(*func)(void *))
{
	node_t *node;
	int i = 0;

	if (!list || list->first)
		return (NULL);
	node = list->first;
	do {
		if (!func(node->data))
			return (false);
		node = node->next;
		i++;
	} while (i < list->size);
	return (true);
}