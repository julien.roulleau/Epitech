/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** push.c
*/

#include "list.h"

bool push_front(list_t *list, void *data)
{
	node_t *node = malloc(sizeof(node_t));

	if (!list || !node)
		return (false);
	node->data = data;
	node->next = list->first;
	if (list->type == CIRCULAR) {
		node->previous = list->last;
		if (list->last)
			list->last->next = node;
	} else
		node->previous = 0;
	if (list->first)
		list->first->previous = node;
	list->first = node;
	if (list->size == 0)
		list->last = node;
	list->size++;
	return (true);
}

bool push_back(list_t *list, void *data)
{
	node_t *node = malloc(sizeof(node_t));

	if (!list || !node)
		return (false);
	node->data = data;
	node->previous = list->last;
	if (list->type == CIRCULAR) {
		node->next = list->first;
		if (list->first)
			list->first->previous = node;
	} else
		node->next = 0;
	if (list->last)
		list->last->next = node;
	list->last = node;
	if (list->size == 0)
		list->first = node;
	list->size++;
	return (true);
}
