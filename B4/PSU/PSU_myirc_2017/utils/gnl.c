/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** gnl.c
*/

#include <stdlib.h>
#include <unistd.h>

char *gnl(const int fd)
{
	char *line = calloc(1, sizeof(char));
	char buff;
	size_t i = 0;

	while (read(fd, &buff, 1) > 0) {
		if (buff == '\n')
			return (line);
		line = realloc(line, i + 2);
		line[i] = buff;
		line[++i] = 0;
	}
	return (*line ? line : 0);
}
