/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** free_strtab.c
*/

#include <stdlib.h>

void free_strtab(char **tab)
{
	for (int i = 0; tab[i]; i++)
		free(tab[i]);
	free(tab);
}
