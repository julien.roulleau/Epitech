/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** utils.h
*/

#ifndef MYIRC_UTILS_H
#define MYIRC_UTILS_H

#include <stdbool.h>

char *gnl(int fd);
bool data(int dest, int src);
char **strtab(char*, char);
void free_strtab(char**);
int len_strtab(char**);

#endif //MYIRC_UTILS_H
