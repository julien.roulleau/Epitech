/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** strtab.c
*/

#include <stdlib.h>
#include <string.h>

static int word_len(char *str, char sep)
{
	int i = 0;

	while (str[i] && str[i] != sep)
		i++;
	return (i);
}

static int get_nb_word(char *str, char sep)
{
	int nb = 0;
	int prev = 0;

	if (!str)
		return (0);
	for (int i = 0; str[i]; i++) {
		if (str[i] != sep && prev == 0) {
			nb++;
			prev = 1;
		}
		if (str[i] == sep)
			prev = 0;
	}
	return (nb);
}

static int fill_word(char **line, char *str, char sep)
{
	char *n_line = NULL;
	int i;

	n_line = malloc(sizeof(char) * (word_len(str, sep) + 1));
	if (!n_line)
		return (0);
	for (i = 0; str[i] && str[i] != sep; i++)
		n_line[i] = str[i];
	n_line[i] = 0;
	*line = n_line;
	return (i - 1);
}

char **strtab(char *str, char sep)
{
	int size = get_nb_word(str, sep);
	char **tab = malloc(sizeof(char*) * (size + 1));
	int new_word = 1;
	int j = -1;

	if (!size && tab != NULL)
		free(tab);
	if (!size || !tab)
		return (NULL);
	for (int i = 0; str[i]; i++) {
		if (str[i] != sep && new_word) {
			j++;
			i += fill_word(&tab[j], str + i, sep);
			new_word = 0;
		}
		if (str[i] == sep)
			new_word = 1;
	}
	tab[size] = NULL;
	return (tab);
}
