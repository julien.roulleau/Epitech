/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** len_strtab.c
*/

int len_strtab(char **tab)
{
	int i = 0;

	while (tab[i])
		i++;
	return (i);
}