###############################
#                             #
#   MY_IRC : Epitech project  #
#                             #
###############################

This project consists of creating an IRC client and server.


 SERVER:
~~~~~~~~~~~~
# USAGE: ./server port



 CLIENT:
~~~~~~~~~~~~
# USAGE: ./client

# Command list and functionality:
/server $host[:$port]           - connects to a server
/nick $nickname                 - define / modifie de user's nickname
/list $string                   - lists the server’s available channels
/join $channel                  - joins a server's channel
/part $channel                  - leave a channel
/move $channel                  - change the current channel
$message                        - send a message to all the users that are connected to the current channel
/users                          - list the nickname of the users that are connected to the server
/names                          - list the nickname of the users that are connected to the channel
/msg $nick $msg                 - send a message to a specific user
/info                           - display client informations
/help                           - display help

To be identifie on a server, both '/server' ans '/nick' have to be done, whatever the order.