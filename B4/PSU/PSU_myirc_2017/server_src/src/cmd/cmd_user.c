/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** cmd_user.c
*/

#include "socket/socket.h"
#include <zconf.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "cmd.h"

static char *get_host(client_t *client)
{
	socklen_t len;
	struct sockaddr_storage addr;
	char ipstr[INET6_ADDRSTRLEN];

	len = sizeof(addr);
	getpeername(client->fd, (struct sockaddr*)&addr, &len);
	if (addr.ss_family == AF_INET) {
		struct sockaddr_in *s = (struct sockaddr_in *)&addr;
		inet_ntop(AF_INET, &s->sin_addr, ipstr, sizeof(ipstr));
	}
	return (strdup(ipstr));
}

static bool success_reply(client_t *client)
{
	if (!client->log && client->nick && client->name) {
		client->log = true;
		return (msg(client->fd, 001, SERVER_ADDR, "001", client->nick,
			client->nick, client->name, client->hote));
	}
	return (true);
}

bool cmd_user(server_t *server, client_t *client, char *cmd)
{
	char **cmd_tab = strtab(cmd, CMD_SEP);
	char *name;

	(void)server;
	if (!cmd_tab)
		return (false);
	if (len_strtab(cmd_tab) < 4)
		return (msg(client->fd, 461, SERVER_ADDR, 461, "USER"));
	if (client->name != NULL)
		return (msg(client->fd, 462, SERVER_ADDR, 462));
	client->name = strdup(cmd_tab[0]);
	client->hote = get_host(client);
	name = strchr(cmd, ':');
	if (!name)
		return (msg(client->fd, 461, SERVER_ADDR, 461, "USER"));
	client->real_name = strdup(name + 1);
	free_strtab(cmd_tab);
	return (success_reply(client));
}
