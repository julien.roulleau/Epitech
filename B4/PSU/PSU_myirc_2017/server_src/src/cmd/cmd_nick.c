/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** cmd_nick.c
*/

#include <server/server.h>
#include "cmd.h"

static bool nick_is_valid(char *nick)
{
	for (int i = 0; nick[i]; i++)
		if (!CHAR_IS_NICK(nick[i]))
			return (false);
	return (true);
}

static bool success_reply(client_t *client)
{
	if (!client->log && client->nick && client->name) {
		client->log = true;
		return (msg(client->fd, 001, SERVER_ADDR, "001", client->nick,
			client->nick, client->name, client->hote));
	}
	return (true);
}

bool cmd_nick(server_t *server, client_t *client, char *cmd)
{
	char **cmd_tab = strtab(cmd, CMD_SEP);

	if (!cmd_tab)
		return (false);
	if (!cmd_tab[0])
		return (msg(client->fd, 431, SERVER_ADDR, 431));
	if (!nick_is_valid(cmd_tab[0]))
		return (msg(client->fd, 432, SERVER_ADDR, 432, cmd_tab[0]));
	if (find_client_by_name(server->clients, cmd_tab[0]))
		return (msg(client->fd, 433, SERVER_ADDR, 433, cmd_tab[0]));
	if (client->nick)
		free(client->nick);
	client->nick = strdup(cmd_tab[0]);
	free_strtab(cmd_tab);
	return (success_reply(client));
}
