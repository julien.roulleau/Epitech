/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** cmd_join.c
*/

#include <server/server.h>
#include <stdarg.h>
#include "cmd.h"

static bool chan_name_is_valid(char *name)
{
	if (!name)
		return (false);
	if (name[0] != '&' && name[0] != '#')
		return (false);
	return (true);
}

static bool add_client_to_chan(channel_t *chan, client_t *client, char *pass)
{
	client_t *clt = NULL;

	if ((!pass && chan->pass) ||
		(chan->pass && pass && strcmp(pass, chan->pass)))
		return (msg(client->fd, 475, SERVER_ADDR, 475, chan->name));
	push_back(chan->clients, client);
	push_back(client->channels, chan);
	FOREACH(node_t, chan->clients->first) {
		clt = item->data;
		dprintf(clt->fd, ":%s!%s@%s JOIN :%s\r\n",
			client->nick, client->name, client->hote, chan->name);
	}
	msg(client->fd, 332, SERVER_ADDR, 332, chan->name);
	return (true);
}

static bool create_channel(client_t *client, list_t *channels,
				char *name, char *pass)
{
	channel_t *chan = NULL;

	FOREACH(node_t, channels->first) {
		if (!strcmp(name, ((channel_t*)(item->data))->name))
			return (true);
	}
	chan = malloc(sizeof(channel_t));
	if (!chan)
		return (false);
	chan->name = strdup(name);
	chan->pass = (pass != NULL) ? strdup(pass) : NULL;
	chan->clients = create_list(LINKED);
	push_back(chan->clients, client);
	push_back(client->channels, chan);
	push_back(channels, chan);
	dprintf(client->fd, ":%s!%s@%s JOIN :%s\r\n", client->nick,
		client->name, client->hote, name);
	msg(client->fd, 332, SERVER_ADDR, 332, name);
	return (true);
}

static bool join_channels(server_t *server, client_t *client,
		char **chan_name, char **t_pass)
{
	char *pass = NULL;
	channel_t *chan = NULL;

	for (int i = 0; chan_name[i]; i++) {
		pass = (t_pass && i < len_strtab(t_pass)) ? t_pass[i] : NULL;
		if (!chan_name_is_valid(chan_name[i])) {
			msg(client->fd, 403, SERVER_ADDR, 403, chan_name[i]);
			continue;
		}
		chan = find_channel(server->channels, chan_name[i]);
		if (!chan) {
			if (!create_channel(client, server->channels,
					chan_name[i], pass))
				return (false);
		} else
			add_client_to_chan(chan, client, pass);
	}
	return (true);
}

bool cmd_join(server_t *server, client_t *client, char *cmd)
{
	char **cmd_tab = strtab(cmd, CMD_SEP);
	char **chan_name = cmd_tab ? strtab(cmd_tab[0], ',') : NULL;
	char **chan_pass = NULL;

	if (!cmd_tab || !chan_name)
		return (false);
	if (len_strtab(cmd_tab) < 1)
		return (msg(client->fd, 461, SERVER_ADDR, 461, "JOIN"));
	if (len_strtab(cmd_tab) >= 2)
		if (!(chan_pass = strtab(cmd_tab[1], ',')))
			return (false);
	if (join_channels(server, client, chan_name, chan_pass))
		cmd_names(server, client, cmd_tab[0]);
	free_strtab(cmd_tab);
	if (chan_pass)
		free_strtab(chan_pass);
	free_strtab(chan_name);
	return (true);
}
