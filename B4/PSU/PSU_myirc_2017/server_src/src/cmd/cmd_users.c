/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** cmd_users.c
*/

#include "cmd.h"

bool cmd_users(server_t *srv, client_t *clt, char *cmd)
{
	(void)srv;
	(void)cmd;
	dprintf(clt->fd, ":%s 446 :USERS has been disabled\r\n", SERVER_ADDR);
	return (true);
}

