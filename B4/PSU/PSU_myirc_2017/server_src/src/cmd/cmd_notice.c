/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** cmd_notice.c
*/

#include "cmd.h"

static char *create_msg(client_t *client, char *cmd, char *dest)
{
	char *notice = strchr(cmd, ':');
	char prefix[2048];
	char *message;

	if (!notice)
		return (NULL);
	if (client->name && client->hote)
		sprintf(prefix, ":%s!%s@%s NOTICE %s ", client->nick,
			client->name, client->hote, dest);
	else if (client->name)
		sprintf(prefix, ":%s!%s NOTICE %s ", client->nick,
			client->name, dest);
	else
		sprintf(prefix, ":%s NOTICE %s ", client->nick, dest);
	message = malloc(sizeof(char) * (strlen(prefix) + strlen(notice) + 9));
	if (!message)
		return (NULL);
	sprintf(message, "%s%s", prefix, notice);
	return (message);
}

static bool call_send_client(char *cmd, client_t *client,
			server_t *server, char *target_name)
{
	char *message;
	client_t *target = find_client_by_name(server->clients, target_name);

	if (!target)
		return (msg(client->fd, 401, SERVER_ADDR, 401, target_name));
	message = create_msg(client, cmd, target_name);
	if (!message)
		return (msg(client->fd, 412, SERVER_ADDR, 412));
	send_client(target, message);
	return (true);
}

static bool call_send_chan(server_t *serv, char *cmd,
			client_t *client, char *chan_name)
{
	channel_t *chan = find_channel(client->channels, chan_name);
	char *message;

	if (!find_channel(serv->channels, chan_name))
		return (msg(client->fd, 401, SERVER_ADDR, 401, chan_name));
	if (!chan)
		return (msg(client->fd, 404, SERVER_ADDR, 404, chan_name));
	message = create_msg(client, cmd, chan_name);
	if (!message)
		return (msg(client->fd, 412, SERVER_ADDR, 412));
	send_channel(chan, message, client);
	free(message);
	return (true);
}

bool cmd_notice(server_t *server, client_t *client, char *cmd)
{
	char **cmd_tab = strtab(cmd, CMD_SEP);
	char **dest_tab = cmd_tab ? strtab(cmd_tab[0], ',') : NULL;

	if (!cmd_tab || !dest_tab)
		return (msg(client->fd, 411, SERVER_ADDR, 411, "NOTICE"));
	if (cmd_tab[0][0] == ':')
		return (msg(client->fd, 411, SERVER_ADDR, 411, "NOTICE"));
	if (len_strtab(cmd_tab) < 2 && cmd_tab[1][0] != ':')
		return (msg(client->fd, 412, SERVER_ADDR, 412));
	for (int i = 0; dest_tab[i]; i++) {
		if (dest_tab[i][0] == '&' || dest_tab[i][0] == '#')
			call_send_chan(server, cmd, client, dest_tab[i]);
		else
			call_send_client(cmd, client, server, dest_tab[i]);
	}
	free_strtab(cmd_tab);
	return (true);
}
