/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** cmd_ping_pong.c
*/

#include "cmd.h"

bool cmd_ping(server_t *serv, client_t *clt, char *cmd)
{
	(void)serv;
	if (cmd) {
		cmd = cmd[0] == ':' ? cmd + 1 : cmd;
		dprintf(clt->fd, ":%s %s %s :%s", SERVER_ADDR, "PONG",
			SERVER_ADDR, cmd);
	} else
		msg(clt->fd, 461, SERVER_ADDR, 461, "PING");
	return (true);
}
