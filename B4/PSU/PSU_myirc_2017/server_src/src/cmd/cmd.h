/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** cmd.h
*/

#ifndef MYIRC_CMD_H
#define MYIRC_CMD_H

#include <string.h>
#include <stdio.h>
#include "server/server.h"
#include "client/client.h"
#include "msg/msg.h"
#include "utils.h"

#define SERVER_ADDR "127.0.0.1"
#define CMD_SEP ' '
#define CHAR_IS_NICK(c) ((c <= '9' && c >= '0') || (c <= 'z' && c >= 'a') ||\
(c <= 'Z' && c >= 'A') || c == '-' || c == '[' || c == ']' ||\
c == '\\' || c == '^' || c == '{' || c == '}')

typedef struct func_list_s {
	char *flag;
	bool (*func)(server_t *server, client_t *client, char *cmd);
	struct func_list_s *next;
} func_list_t;

bool cmd_privmsg(server_t*, client_t*, char*);
bool cmd_notice(server_t*, client_t*, char*);
bool cmd_names(server_t*, client_t*, char*);
bool cmd_users(server_t*, client_t*, char*);
bool cmd_join(server_t*, client_t*, char*);
bool cmd_nick(server_t*, client_t*, char*);
bool cmd_list(server_t*, client_t*, char*);
bool cmd_part(server_t*, client_t*, char*);
bool cmd_pass(server_t*, client_t*, char*);
bool cmd_user(server_t*, client_t*, char*);
bool cmd_quit(server_t*, client_t*, char*);
bool cmd_ping(server_t*, client_t*, char*);

#endif //MYIRC_CMD_H
