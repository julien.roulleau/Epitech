/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** cmd_quit.c
*/

#include "interpreter/interpreter.h"
#include "cmd.h"

bool cmd_quit(server_t *server, client_t *client, char *cmd)
{
	(void)cmd;
	if (cmd)
		dprintf(client->fd, "ERROR :deconnection %s\r\n", cmd);
	else
		dprintf(client->fd, "ERROR :deconnection\r\n");
	close_connection(client);
	delete_client(server->clients, server->channels, client);
	return (true);
}
