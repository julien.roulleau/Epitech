/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** cmd_names.c
*/

#include <server/server.h>
#include "cmd.h"

bool names_chan(channel_t *chan, client_t *client)
{
	char *client_list = malloc(sizeof(char));

	if (!chan || !client_list)
		return (false);
	client_list[0] = 0;
	FOREACH(node_t, chan->clients->first) {
		client_list = realloc(client_list, strlen(client_list) +
			strlen(((client_t*)(item->data))->nick) + 2);
		if (!client_list)
			return (false);
		client_list = strcat(client_list, " ");
		client_list = strcat(client_list,
			((client_t*)(item->data))->nick);
	}
	msg(client->fd, 353, SERVER_ADDR, 353, client->nick, "=",
		chan->name, client_list + 1);
	free(client_list);
	return (true);
}

static void end_of_names(server_t *srv, client_t *clt, char *name)
{
	char *last = NULL;
	char **chan_tab = NULL;

	if (name) {
		chan_tab = strtab(name, ',');
		for (int i = 0; i < len_strtab(chan_tab); i++)
			if (find_channel(srv->channels, chan_tab[i]), clt)
				last = chan_tab[i];
		msg(clt->fd, 366, SERVER_ADDR, 366, clt->nick, last);
		free_strtab(chan_tab);
	} else if (srv->channels->size) {
		FOREACH(node_t, srv->channels->first) {
			last = ((channel_t*)(item->data))->name;
		}
		msg(clt->fd, 366, SERVER_ADDR, 366, clt->nick, last);
	}
}

bool cmd_names(server_t *srv, client_t *clt, char *cmd)
{
	char **cmd_tab = strtab(cmd, CMD_SEP);
	char **chan_tab = NULL;

	if (cmd_tab && cmd_tab[0]) {
		dprintf(2, "%s\n", cmd_tab[0]);
		chan_tab = strtab(cmd_tab[0], ',');
		for (int i = 0; i < len_strtab(chan_tab); i++)
			if (names_chan(find_channel(srv->channels,
						chan_tab[i]), clt))
		free_strtab(chan_tab);
	} else if (srv->channels->size) {
		FOREACH(node_t, srv->channels->first)
			names_chan(item->data, clt);
	}
	end_of_names(srv, clt, (cmd_tab ? cmd_tab[0] : NULL));
	if (cmd_tab)
		free_strtab(cmd_tab);
	return (true);
}
