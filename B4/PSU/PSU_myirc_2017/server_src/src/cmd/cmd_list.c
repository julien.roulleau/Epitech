/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** cmd_list.c
*/

#include "cmd.h"

static bool list_chan(channel_t *chan, client_t *clt)
{
	return (msg(clt->fd, 322, SERVER_ADDR, 322, clt->nick,
			chan->name, chan->clients->size));
}

bool cmd_list(server_t *srv, client_t *clt, char *cmd)
{
	char **cmd_tab = strtab(cmd, CMD_SEP);
	char **chan_tab = NULL;

	if (cmd_tab) {
		chan_tab = strtab(cmd_tab[0], ',');
		for (int i = 0; i < len_strtab(chan_tab); i++)
			if (list_chan(find_channel(srv->channels,
						chan_tab[i]), clt))
				free_strtab(chan_tab);
	} else if (srv->channels->size) {
		FOREACH(node_t, srv->channels->first)
			list_chan(item->data, clt);
	}
	msg(clt->fd, 323, SERVER_ADDR, 323, clt->nick);
	if (cmd_tab)
		free_strtab(cmd_tab);
	return (true);
}
