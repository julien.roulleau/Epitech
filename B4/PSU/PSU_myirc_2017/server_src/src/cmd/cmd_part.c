/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** cmd_part.c
*/

#include <server/server.h>
#include "cmd.h"

static bool msg_part(client_t *client, char *quit_msg, channel_t *chan)
{
	client_t *clt = NULL;

	FOREACH(node_t, chan->clients->first) {
		clt = item->data;
		dprintf(clt->fd, ":%s!%s@%s PART %s :%s\r\n", client->nick,
			client->name, client->hote, chan->name,
			(quit_msg ? quit_msg : ""));
	}
	return (true);
}

static bool quit_channels(server_t *server, client_t *client,
				char **chan_name, char *quit_msg)
{
	channel_t *chan = NULL;

	for (int i = 0; chan_name[i]; i++) {
		chan = find_channel(server->channels, chan_name[i]);
		if (!chan) {
			msg(client->fd, 403, SERVER_ADDR, 403, chan_name[i]);
			continue;
		}
		if (!find_client_by_name(chan->clients, client->name)) {
			msg(client->fd, 442, SERVER_ADDR, 442, chan_name[i]);
			continue;
		}
		msg_part(client, quit_msg, chan);
		leave_channel(server->channels, chan, client);
	}
	return (true);
}

bool cmd_part(server_t *server, client_t *client, char *cmd)
{
	char **cmd_tab = strtab(cmd, CMD_SEP);
	char **chan_name = NULL;

	if (!cmd_tab)
		return (false);
	if (len_strtab(cmd_tab) < 1)
		return (msg(client->fd, 461, SERVER_ADDR, 461, "PART"));
	chan_name = strtab(cmd_tab[0], ',');
	if (!chan_name || !quit_channels(server, client, chan_name, cmd_tab[1]))
		return (false);
	free_strtab(cmd_tab);
	free_strtab(chan_name);
	return (true);
}
