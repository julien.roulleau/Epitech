/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** client.h
*/

#ifndef MYIRC_CLIENT_H
#define MYIRC_CLIENT_H

#include "list.h"

typedef struct {
	int fd;
	char *nick;
	char *name;
	char *real_name;
	char *hote;
	char *mdp;
	bool log;
	list_t *channels;
} client_t;

bool send_client(client_t *client, char *buffer);
node_t *find_client(list_t *clients, int fd);
client_t *find_client_by_name(list_t*, char*);
bool delete_client(list_t*, list_t*, client_t*);

#endif //MYIRC_CLIENT_H
