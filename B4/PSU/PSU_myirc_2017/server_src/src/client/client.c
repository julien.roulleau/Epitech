/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** client.c
*/

#include <stdio.h>
#include <string.h>
#include "client.h"
#include "channel/channel.h"

bool send_client(client_t *client, char *buffer)
{
	if (!client || !buffer)
		return (false);
	dprintf(client->fd, "%s\r\n", buffer);
	return (true);
}

node_t *find_client(list_t *clients, int fd)
{
	client_t *tmp;

	if (!clients || !clients->first)
		return (NULL);
	FOREACH(node_t, clients->first) {
		tmp = item->data;
		if (tmp->fd == fd)
			return (item);
	}
	return (NULL);
}

client_t *find_client_by_name(list_t *clients, char *name)
{
	if (!clients || !clients->first)
		return (NULL);
	FOREACH(node_t, clients->first) {
		if (!(((client_t*)(item->data))->nick))
			continue;
		if (!strcasecmp(((client_t*)(item->data))->nick, name))
			return ((client_t*)(item->data));
	}
	return (NULL);
}

bool delete_client(list_t *clients, list_t *chans, client_t *client)
{
	FOREACH(node_t, client->channels->first)
		leave_channel(chans, item->data, client);
	if (client->nick)
		free(client->nick);
	if (client->hote)
		free(client->hote);
	if (client->name)
		free(client->name);
	if (client->real_name)
		free(client->real_name);
	if (client->mdp)
		free(client->mdp);
	if (!free_node(clients, find_client(clients, client->fd)))
		return (false);
	clean_list(client->channels);
	free(client);
	return (true);
}
