/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** interpreter.c
*/

#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <zconf.h>
#include <server/server.h>
#include "server/server.h"
#include "utils.h"
#include "cmd/cmd.h"
#include "interpreter.h"

func_list_t CMD_LIST[] = {
	{.next = &CMD_LIST[1], .flag = "join", .func = &cmd_join},
	{.next = &CMD_LIST[2], .flag = "nick", .func = &cmd_nick},
	{.next = &CMD_LIST[3], .flag = "list", .func = &cmd_list},
	{.next = &CMD_LIST[4], .flag = "privmsg", .func = &cmd_privmsg},
	{.next = &CMD_LIST[5], .flag = "names", .func = &cmd_names},
	{.next = &CMD_LIST[6], .flag = "part", .func = &cmd_part},
	{.next = &CMD_LIST[7], .flag = "pass", .func = &cmd_pass},
	{.next = &CMD_LIST[8], .flag = "users", .func = &cmd_users},
	{.next = &CMD_LIST[9], .flag = "quit", .func = &cmd_quit},
	{.next = &CMD_LIST[10], .flag = "notice", .func = &cmd_notice},
	{.next = &CMD_LIST[11], .flag = "ping", .func = &cmd_ping},
	{.next = NULL, .flag="user", .func = &cmd_user},
};

static char *cut_buffer(char *buffer, char sep)
{
	char *tmp;

	if (!buffer)
		return (NULL);
	tmp = index(buffer, sep);
	if (tmp) {
		*tmp = 0;
		tmp++;
		return (tmp);
	} else {
		return (NULL);
	}
}

static bool exec_cmd(server_t *server, client_t *client, char *buffer)
{
	char *prefix = buffer;
	char *cmd = buffer;
	char *option;

	if (buffer[0] == ':')
		cmd = cut_buffer(prefix, ' ');
	option = cut_buffer(cmd, ' ');
	if (!cmd)
		return (false);
	FOREACH(func_list_t, CMD_LIST) {
		if (!strcasecmp(item->flag, cmd)) {
			if (!client->log && !(!strcasecmp(cmd, "user") ||
					!strcasecmp(cmd, "nick") ||
					!strcasecmp(cmd, "quit") ||
					!strcasecmp(cmd, "pass")))
				return (msg(client->fd, 451, SERVER_ADDR, 451));
			return (item->func(server, client, option));
		}
	}
	return (cmd[0] ? msg(client->fd, 421, SERVER_ADDR, 421, cmd) : true);
}

bool close_connection(client_t *client)
{
	if (close(client->fd) == -1)
		return (false);
	return (true);
}

bool interpreter(server_t *server, client_t *client)
{
	char *buffer = NULL;

	buffer = gnl(client->fd);
	if (!buffer) {
		close_connection(client);
		delete_client(server->clients, server->channels, client);
		return (true);
	}
	if (buffer[strlen(buffer) - 1] == '\r')
		buffer[strlen(buffer) - 1] = 0;
	dprintf(2, "[in]\t%s\n", buffer);
	if (!exec_cmd(server, client, buffer))
		return (false);
	free(buffer);
	return (true);
}
