/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** interpreter.h
*/

#ifndef MYIRC_INTERPRETER_H
#define MYIRC_INTERPRETER_H

#include "server/server.h"

bool interpreter(server_t *server, client_t *client);
bool close_connection(client_t *client);

#endif //MYIRC_INTERPRETER_H
