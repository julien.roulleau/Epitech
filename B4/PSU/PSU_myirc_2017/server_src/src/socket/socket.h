/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** socket.h
*/

#ifndef MYIRC_SOCKET_H
#define MYIRC_SOCKET_H

int create_connection(int port);
int wait_connection(int sock);

#endif //MYIRC_SOCKET_H
