/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** msg.c
*/

#include <stdio.h>
#include <zconf.h>
#include "msg.h"

msg_list_t MSG_LIST[] = {
	{.next = &MSG_LIST[1], .flag = 421, .name = "ERR_UNKNOWNCOMMAND",
		.msg = ":%s %i %s :Unknown command"},
	{.next = &MSG_LIST[2], .flag = 431, .name = "ERR_NONICKNAMEGIVEN",
		.msg = ":%s %i :No nickname given"},
	{.next = &MSG_LIST[3], .flag = 432, .name = "ERR_ERRONEUSNICKNAME",
		.msg = ":%s %i %s :Erroneus nickname"},
	{.next = &MSG_LIST[4], .flag = 433, .name = "ERR_NICKNAMEINUSE",
		.msg = ":%s %i %s :Nickname is already in use"},
	{.next = &MSG_LIST[5], .flag = 461, .name = "ERR_NEEDMOREPARAMS",
		.msg = ":%s %i %s :Not enough parameters"},
	{.next = &MSG_LIST[6], .flag = 462, .name = "ERR_ALREADYREGISTRED",
		.msg = ":%s %i :You may not reregister"},
	{.next = &MSG_LIST[7], .flag = 403, .name = "ERR_NOSUCHCHANNEL",
		.msg = ":%s %i %s :No such channel"},
	{.next = &MSG_LIST[8], .flag = 332, .name = "RPL_TOPIC",
		.msg = ":%s %i %s :No subject"},
	{.next = &MSG_LIST[9], .flag = 442, .name = "ERR_NOTONCHANNEL",
		.msg = ":%s %i %s :You're not on that channel"},
	{.next = &MSG_LIST[10], .flag = 475, .name = "ERR_BADCHANNELKEY",
		.msg = ":%s %i %s :Cannot join channel (+k)"},
	{.next = &MSG_LIST[11], .flag = 001, .name = "RPL_WELCOME",
		.msg = ":%s %s %s :Welcome to the Internet Relay Network "
			"%s!%s@%s"},
	{.next = &MSG_LIST[12], .flag = 002, .name = "RPL_YOURHOST",
		.msg = ":%s %i %s "},
	{.next = &MSG_LIST[13], .flag = 003, .name = "RPL_CREATED",
		.msg = ":%s %i %s "},
	{.next = &MSG_LIST[14], .flag = 412, .name = "ERR_NOTEXTTOSEND",
		.msg = ":%s %i :No text to send"},
	{.next = &MSG_LIST[15], .flag = 411, .name = "ERR_NORECIPIENT",
		.msg = ":%s %i :No recipient given (%s)"},
	{.next = &MSG_LIST[16], .flag = 404, .name = "ERR_CANNOTSENDTOCHAN",
		.msg = ":%s %i %s :Cannot send to channel"},
	{.next = &MSG_LIST[17], .flag = 401, .name = "ERR_NOSUCHNICK",
		.msg = ":%s %i %s :No such nick/channel"},
	{.next = &MSG_LIST[18], .flag = 451, .name = "ERR_NOTREGISTERED",
		.msg = ":%s %i :You have not registered"},
	{.next = &MSG_LIST[19], .flag = 321, .name = "RPL_LISTSTART",
		.msg = ":%s %i Channel :Users Name"},
	{.next = &MSG_LIST[20], .flag = 322, .name = "RPL_LIST",
		.msg = ":%s %i %s %s %i:"},
	{.next = &MSG_LIST[21], .flag = 323, .name = "RPL_LISTEND",
		.msg = ":%s %i %s :End of LIST"},
	{.next = &MSG_LIST[22], .flag = 353, .name = "RPL_NAMREPLY",
		.msg = ":%s %i %s %s %s :%s"},
	{.next = &MSG_LIST[23], .flag = 366, .name = "RPL_ENDOFNAMES",
		.msg = ":%s %i %s %s :End of NAMES list"},
	{.next = NULL, .flag = 004, .name = "RPL_MYINFO",
		.msg = ":%s %i %s "}
};

bool msg(int fd, int code, ...)
{
	va_list va;
	char buffer[2048];

	for (msg_list_t *item = MSG_LIST; item; item = item->next) {
		if (item->flag == code) {
			va_start(va, code);
			vsprintf(buffer, item->msg, va);
			va_end(va);
			dprintf(fd, "%s\r\n", buffer);
			dprintf(2, "[out]\t %s\n", buffer);
			return (true);
		}
	}
	return (false);
}