/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** msg.h
*/

#ifndef MYIRC_MSG_H
#define MYIRC_MSG_H

#include <stdbool.h>

typedef struct msg_list_s {
	int flag;
	char *name;
	char *msg;
	struct msg_list_s *next;
} msg_list_t;

bool msg(int fd, int code, ...);

#endif //MYIRC_MSG_H
