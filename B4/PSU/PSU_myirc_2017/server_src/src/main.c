/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** main.c
*/

#include <stdlib.h>
#include <zconf.h>
#include "server/server.h"
#include "socket/socket.h"

int main(int ac, char **av)
{
	int sock;

	if (ac != 2)
		return (84);
	sock = create_connection(atoi(av[1]));
	if (sock == -1 || !run(sock))
		return (84);
	close(sock);
	return (0);
}