/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** server.c
*/

#include "client/client.h"
#include "interpreter/interpreter.h"
#include "socket/socket.h"
#include "server.h"

static bool add_client(list_t *clients, int sock)
{
	client_t *new = malloc(sizeof(client_t));
	int csock;

	if (!new)
		return (false);
	csock = wait_connection(sock);
	if (csock == -1)
		return (false);
	new->fd = csock;
	new->channels = create_list(LINKED);
	new->mdp = NULL;
	new->name = NULL;
	new->nick = NULL;
	new->real_name = NULL;
	new->hote = NULL;
	new->log = false;
	return (push_back(clients, new));
}

static bool exec_select(fd_set *fd_read, server_t *server, int sock, int max)
{
	for (int i = 0; i < max; i++) {
		if (FD_ISSET(i, fd_read)) {
			if (i == sock)
				return (add_client(server->clients, sock));
			else
				return (interpreter(
					server,
					find_client(server->clients, i)->data));
		}
	}
	return (true);
}

static int set_fd_client(fd_set *fds, list_t *clients)
{
	int max = 0;
	client_t *tmp;

	FOREACH(node_t, clients->first) {
		tmp = item->data;
		FD_SET(tmp->fd, fds);
		max = max > tmp->fd ? max : tmp->fd;
	}
	return (max);
}

bool run(int sock)
{
	server_t server = {.channels = create_list(LINKED),
		.clients = create_list(LINKED)};
	fd_set fds;
	int max;

	if (!server.channels || !server.clients)
		return (false);
	while (true) {
		FD_ZERO(&fds);
		FD_SET(sock, &fds);
		max = set_fd_client(&fds, server.clients);
		max = max > sock ? max : sock;
		max++;
		if (select(max, &fds, NULL, NULL, NULL) == -1)
			return (false);
		if (!exec_select(&fds, &server, sock, max))
			return (false);
	}
	return (true);
}