/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** server.h
*/

#ifndef MYIRC_SERVER_H
#define MYIRC_SERVER_H

#include "channel/channel.h"

typedef struct {
	list_t *channels;
	list_t *clients;
} server_t;

#define MAX_CONNECTION 1024 * 1 + 0

typedef struct {
	char fd_type[MAX_CONNECTION];
	void (*func[MAX_CONNECTION])(void);
} connection_t;

bool run(int sock);

#endif //MYIRC_SERVER_H
