/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** channel.h
*/

#ifndef MYIRC_CHANNEL_H
#define MYIRC_CHANNEL_H

#include "list.h"
#include "client/client.h"

typedef struct channel_s{
	char *name;
	char *pass;
	list_t *clients;
} channel_t;

bool send_channel(channel_t *channel, char *buffer, client_t *client);
channel_t *find_channel(list_t*, char*);
bool leave_channel(list_t *chans, channel_t *chan, client_t *client);

#endif //MYIRC_CHANNEL_H
