/*
** EPITECH PROJECT, 2018
** myIrc
** File description:
** channel.c
*/

#include <stdio.h>
#include <string.h>
#include "channel.h"

bool send_channel(channel_t *channel, char *buffer, client_t *client)
{
	if (!channel || !channel->clients || !channel->clients->first)
		return (false);
	FOREACH(node_t, channel->clients->first) {
		if (client != item->data)
			if (!send_client(item->data, buffer))
				return (false);
	}
	return (true);
}

channel_t *find_channel(list_t *channels, char *name)
{
	FOREACH(node_t, channels->first) {
		if (!strcmp(name, ((channel_t*)(item->data))->name))
			return (((channel_t*)(item->data)));
	}
	return (NULL);
}

bool leave_channel(list_t *chans, channel_t *chan, client_t *client)
{
	FOREACH(node_t, chan->clients->first)
		if (item->data == (void*)client) {
			free_node(chan->clients, item);
			break;
		}
	FOREACH(node_t, client->channels->first)
		if (item->data == (void*)chan) {
			free_node(client->channels, item);
			break;
		}
	if (!chan->clients->size && chans->size)
		FOREACH(node_t, chans->first)
			if (item->data == (void *) chan) {
				free_node(chan->clients, item);
				return (true);
			}
	return (true);
}
