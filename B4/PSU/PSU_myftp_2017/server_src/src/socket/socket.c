/*q
** EPITECH PROJECT, 2018
** lemipc
** File description:
** socket.c
*/

#include "socket.h"

int create_connection(int port)
{
	int sock = socket(AF_INET, SOCK_STREAM, 0);
	struct sockaddr_in sin;

	if (port < 0)
		return (-1);
	if (sock == -1)
		return (perror("socket"), -1);
	sin.sin_addr.s_addr = htonl(INADDR_ANY);
	sin.sin_family = AF_INET;
	sin.sin_port = htons((uint16_t) port);
	if (bind(sock, (const struct sockaddr *) &sin, sizeof(sin)) == -1)
		return (perror("bind"), close(sock), -1);
	if (listen(sock, 1) == -1)
		return (perror("listen"), close(sock), -1);
	return (sock);
}

int wait_connection(int sock)
{
	struct sockaddr_in csin;
	socklen_t size = sizeof(csin);
	int csock;

	csock = accept(sock, (struct sockaddr *) &csin, &size);
	if (csock == -1)
		return (perror("accept"), -1);
	return (csock);
}