/*
** EPITECH PROJECT, 2018
** lemipc
** File description:
** socket.h
*/


#ifndef LEMIPC_SOCKET_H
#define LEMIPC_SOCKET_H

#include <netinet/in.h>
#include "interpreter/interpreter.h"

int create_connection(int port);
int wait_connection(int sock);

#endif //LEMIPC_SOCKET_H
