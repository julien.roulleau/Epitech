/*
** EPITECH PROJECT, 2018
** my_ftp
** File description:
** interpreter.h
*/


#ifndef MY_FTP_INTERPRETER_H
#define MY_FTP_INTERPRETER_H

#include <zconf.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

typedef struct {
	bool is_logged;
	char *name;
} user_t;

typedef struct {
	bool connected;
	user_t user;
	int csock;
	int dsock;
} session_t;

void *interpreter(int csock);

#endif //MY_FTP_INTERPRETER_H
