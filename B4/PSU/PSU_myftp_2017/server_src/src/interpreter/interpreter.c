/*
** EPITECH PROJECT, 2018
** my_ftp
** File description:
** interpreter.c
*/


#include <string.h>
#include <stdlib.h>

#include "utls.h"
#include "cmd/cmd.h"

func_list_t CMD_LIST[] = {
	{.next = &CMD_LIST[1], .flag = "user", .func = &user},
	{.next = &CMD_LIST[2], .flag = "pass", .func = &pass},
	{.next = &CMD_LIST[3], .flag = "pwd", .func = &pwd},
	{.next = &CMD_LIST[4], .flag = "cwd", .func = &cwd},
	{.next = &CMD_LIST[5], .flag = "cdup", .func = &cdup},
	{.next = &CMD_LIST[6], .flag = "list", .func = &list},
	{.next = &CMD_LIST[7], .flag = "dele", .func = &dele},
	{.next = &CMD_LIST[8], .flag = "retr", .func = &retr},
	{.next = &CMD_LIST[9], .flag = "stor", .func = &stor},
	{.next = &CMD_LIST[10], .flag = "pasv", .func = &pasv},
	{.next = &CMD_LIST[11], .flag = "port", .func = &port},
	{.next = &CMD_LIST[12], .flag = "syst", .func = &syst},
	{.next = &CMD_LIST[13], .flag = "quit", .func = &quit},
	{.next = &CMD_LIST[14], .flag = "help", .func = &help},
	{.next = &CMD_LIST[15], .flag = "type", .func = &type},
	{.next = NULL, .flag = "noop", .func = &noop},
};

void exec_cmd(session_t *session, char *buffer)
{
	char *cmd = buffer;
	char *option = index(buffer, ' ');

	if (!option)
		option = "";
	else if (strlen(option)) {
		*option = 0;
		option++;
	}
	for (func_list_t *item = CMD_LIST; item; item = item->next) {
		if (!strcasecmp(item->flag, cmd)) {
			item->func(session, option);
			return;
		}
	}
	msg(session, 500);
}

void *interpreter(int csock)
{
	char *buffer = NULL;
	session_t session = {
		.connected = true,
		.user = {false, NULL},
		.csock = csock,
		.dsock = -1,
	};

	msg(&session, 220);
	while (session.connected) {
		buffer = gnl(csock);
		if (buffer[strlen(buffer) - 1] == '\r')
			buffer[strlen(buffer) - 1] = 0;
		printf("[in]\t%s\n", buffer);
		exec_cmd(&session, buffer);
		free(buffer);
	}
	return (0);
}
