/*
** EPITECH PROJECT, 2018
** my_ftp
** File description:
** file.c
*/

#include <fcntl.h>
#include <interpreter/interpreter.h>
#include "cmd.h"

void dele(session_t *session, char *option)
{
	if (strlen(option) && unlink(option) == 0)
		msg(session, 250);
	else
		msg(session, 500);
}

static int data(int dest, int src)
{
	char *buff[4048];
	ssize_t size;

	do {
		size = read(src, &buff, sizeof(buff));
		if (size == 0)
			break;
		write(dest, buff, size);
	} while (true);
	return (0);
}

void retr(session_t *session, char *option)
{
	int fd;

	if (session->dsock == -1 || session->user.is_logged == false) {
		msg(session, 530);
		return;
	}
	if (strlen(option) && access(option, F_OK) != -1) {
		msg(session, 150);
		fd = open(option, O_RDONLY);
		if (fd == -1)
			msg(session, 500);
		else
			data(session->dsock, fd);
		close(fd);
		close(session->dsock);
		session->dsock = -1;
		msg(session, 226);
	} else
		msg(session, 500);
}

void stor(session_t *session, char *option)
{
	int fd;

	if (session->dsock == -1 || session->user.is_logged == false) {
		msg(session, 530);
		return;
	}
	if (strlen(option) && access(option, F_OK) == -1) {
		msg(session, 150);
		fd = open(option, O_CREAT | O_WRONLY, 0664);
		if (fd == -1)
			msg(session, 500);
		else
			data(fd, session->dsock);
		close(fd);
		close(session->dsock);
		session->dsock = -1;
		msg(session, 226);
	} else
		msg(session, 500);
}