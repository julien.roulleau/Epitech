/*
** EPITECH PROJECT, 2018
** my_ftp
** File description:
** aux.c
*/

#include "cmd.h"

help_list_t HELP_LIST[] = {
	{.next = &HELP_LIST[1], .flag = "USER", .msg =
	"USER <username> : Specify user for authentication."},
	{.next = &HELP_LIST[2], .flag = "PASS", .msg =
	"PASS <password> : Specify password for authentication."},
	{.next = &HELP_LIST[3], .flag = "CWD", .msg =
	"CWD <pathname> : Change working directory."},
	{.next = &HELP_LIST[4], .flag = "CDUP", .msg =
	"CDUP : Change working directory to parent directory."},
	{.next = &HELP_LIST[5], .flag = "QUIT", .msg =
	"QUIT : Disconnection."},
	{.next = &HELP_LIST[6], .flag = "DELE", .msg =
	"DELE <pathname> : Delete file on the server."},
	{.next = &HELP_LIST[7], .flag = "PWD", .msg =
	"PWD : Print working directory."},
	{.next = &HELP_LIST[8], .flag = "PASV", .msg =
	"PASV : Enable \"passive\" mode for data transfer."},
	{.next = &HELP_LIST[9], .flag = "PORT", .msg =
	"PORT <host-port> : Enable \"active\" mode for data transfer."},
	{.next = &HELP_LIST[10], .flag = "HELP", .msg =
	"HELP [<string>] : List available commands."},
	{.next = &HELP_LIST[11], .flag = "NOOP", .msg =
	"NOOP : Do nothing."},
	{.next = &HELP_LIST[12], .flag = "RETR", .msg =
	"RETR <pathname> : Download file from server to client."},
	{.next = &HELP_LIST[13], .flag = "STOR", .msg =
	"STOR <pathname> : Upload file from client to server."},
	{.next = &HELP_LIST[14], .flag = "LIST", .msg =
	"LIST [<pathname>] : List files in the current working directory."},
	{.next = NULL, .flag = "", .msg =
	"USER, PASS, CWD, CDUP, QUIT, DELE, PWD, PASV, PORT, HELP, NOOP, "
	"RETR, STOR, LIST."}
};

void help(session_t *session, char *option)
{
	for (help_list_t *item = HELP_LIST; item; item = item->next) {
		if (!strcasecmp(item->flag, option)) {
			msg(session, 214, item->msg);
			return;
		}
	}
}

void noop(session_t *session, char *option)
{
	(void) option;
	msg(session, 200);
}

void type(session_t *session, char *option)
{
	(void) option;
	msg(session, 200);
}
