/*
** EPITECH PROJECT, 2018
** my_ftp
** File description:
** connection.c
*/

#include <stdlib.h>
#include <netinet/in.h>
#include <interpreter/interpreter.h>
#include <arpa/inet.h>
#include "cmd.h"
#include "socket/socket.h"

void pasv(session_t *session, char *option)
{
	struct sockaddr_in sin;
	int size = sizeof(sin);
	int sock;
	char *ip;
	int port;

	(void) option;
	if (session->dsock != -1)
		close(session->dsock);
	sock = create_connection(0);
	if (getsockname(sock, (struct sockaddr *) &sin,
			(socklen_t *) &size) == -1) {
		msg(session, 500);
		return;
	}
	port = ntohs(sin.sin_port);
	ip = (char *) &sin.sin_addr.s_addr;
	msg(session, 227, ip[0], ip[1], ip[2], ip[3], port / 256, port % 256);
	session->dsock = wait_connection(sock);
	close(sock);
}

struct sockaddr_in set_sin(char *option)
{
	struct sockaddr_in sin;

	sin.sin_addr.s_addr = 0;
	sin.sin_addr.s_addr += atoi(strsep(&option, ","));
	sin.sin_addr.s_addr <<= 8;
	sin.sin_addr.s_addr += atoi(strsep(&option, ","));
	sin.sin_addr.s_addr <<= 8;
	sin.sin_addr.s_addr += atoi(strsep(&option, ","));
	sin.sin_addr.s_addr <<= 8;
	sin.sin_addr.s_addr += atoi(strsep(&option, ","));
	sin.sin_addr.s_addr = htonl(sin.sin_addr.s_addr);
	sin.sin_family = AF_INET;
	sin.sin_port = 0;
	sin.sin_port += atoi(strsep(&option, ","));
	sin.sin_port <<= 8;
	sin.sin_port += atoi(strsep(&option, ","));
	sin.sin_port = htons(sin.sin_port);
	return (sin);
}

void port(session_t *session, char *option)
{
	struct sockaddr_in sin = set_sin(option);

	if (session->dsock != -1)
		close(session->dsock);
	session->dsock = socket(AF_INET, SOCK_STREAM, 0);
	if (session->dsock == -1) {
		perror("socket");
		msg(session, 500);
		return;
	}
	if (connect(session->dsock, (struct sockaddr *) &sin,
		    sizeof(sin)) == -1) {
		perror("connect");
		msg(session, 500);
		return;
	}
	msg(session, 225);
}

void quit(session_t *session, char *option)
{
	(void) option;
	session->connected = false;
	free(session->user.name);
	msg(session, 221);
}

void syst(session_t *session, char *option)
{
	(void) option;
	msg(session, 215);
}