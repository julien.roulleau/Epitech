/*
** EPITECH PROJECT, 2018
** my_ftp
** File description:
** msg.c
*/

#include <interpreter/interpreter.h>
#include "cmd.h"

msg_list_t MSG_LIST[] = {
	{.next = &MSG_LIST[1], .flag = 120, .msg =
	"Service ready in %i minutes."},
	{.next = &MSG_LIST[2], .flag = 125, .msg =
	"Data connection already open; transfer starting."},
	{.next = &MSG_LIST[3], .flag = 150, .msg =
	"File status okay; about to open data connection."},
	{.next = &MSG_LIST[4], .flag = 200, .msg =
	"Command okay."},
	{.next = &MSG_LIST[5], .flag = 214, .msg =
	"%s"},
	{.next = &MSG_LIST[6], .flag = 215, .msg =
	"UNIX Type: L8."},
	{.next = &MSG_LIST[7], .flag = 220, .msg =
	"Service ready for new user."},
	{.next = &MSG_LIST[8], .flag = 221, .msg =
	"Service closing control connection. Logged out if appropriate."},
	{.next = &MSG_LIST[9], .flag = 225, .msg =
	"Data connection open; no transfer in progress."},
	{.next = &MSG_LIST[10], .flag = 226, .msg =
	"Closing data connection. Requested file action successful"},
	{.next = &MSG_LIST[11], .flag = 227, .msg =
	"Entering Passive Mode (%u,%u,%u,%u,%u,%u)."},
	{.next = &MSG_LIST[12], .flag = 230, .msg =
	"User logged in, proceed."},
	{.next = &MSG_LIST[13], .flag = 250, .msg =
	"Requested file action okay, completed."},
	{.next = &MSG_LIST[14], .flag = 257, .msg =
	"\"%s\"."},
	{.next = &MSG_LIST[15], .flag = 331, .msg =
	"User name okay, need password."},
	{.next = &MSG_LIST[16], .flag = 332, .msg =
	"Need account for login."},
	{.next = &MSG_LIST[17], .flag = 500, .msg =
	"Syntax error, command unrecognized."},
	{.next = &MSG_LIST[18], .flag = 502, .msg =
	"Command not implemented."},
	{.next = NULL, .flag = 530, .msg = "Not logged in."},
};

int msg(session_t *session, int code, ...)
{
	va_list va;
	char *buffer[2048];

	for (msg_list_t *item = MSG_LIST; item; item = item->next) {
		if (item->flag == code) {
			va_start(va, code);
			vsprintf((char *) buffer, item->msg, va);
			va_end(va);
			dprintf(session->csock, "%i %s\r\n", code,
				(char *) buffer);
			printf("[out]\t%i %s\r\n", code,
			       (char *) buffer);
			return (0);
		}
	}
	msg(session, 500);
	return (0);
}