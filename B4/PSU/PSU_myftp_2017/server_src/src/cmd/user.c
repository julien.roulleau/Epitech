/*
** EPITECH PROJECT, 2018
** my_ftp
** File description:
** user.c
*/

#include <interpreter/interpreter.h>
#include "cmd.h"

void user(session_t *session, char *option)
{
	if (!strcmp("Anonymous", option)) {
		session->user.is_logged = true;
		session->user.name = strdup(option);
	}
	msg(session, 331);
}

void pass(session_t *session, char *option)
{
	(void) option;
	if (session->user.is_logged == true && strlen(option) == 0)
		msg(session, 230);
	else
		msg(session, 530);
}