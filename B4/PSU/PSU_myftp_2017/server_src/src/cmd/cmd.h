/*
** EPITECH PROJECT, 2018
** my_ftp
** File description:
** cmd.h
*/


#ifndef MY_FTP_CMD_H
#define MY_FTP_CMD_H

#include <stdio.h>

#include "interpreter/interpreter.h"

typedef struct func_list_s {
	char *flag;
	void (*func)(session_t *, char *);
	struct func_list_s *next;
} func_list_t;

typedef struct msg_list_s {
	int flag;
	char *msg;
	struct msg_list_s *next;
} msg_list_t;

typedef struct help_list_s {
	char *flag;
	char *msg;
	struct help_list_s *next;
} help_list_t;

void user(session_t *session, char *option);
void pass(session_t *session, char *option);

void pwd(session_t *session, char *option);
void cwd(session_t *session, char *option);
void cdup(session_t *session, char *option);
void list(session_t *session, char *option);

void dele(session_t *session, char *option);
void retr(session_t *session, char *option);
void stor(session_t *session, char *option);

void pasv(session_t *session, char *option);
void port(session_t *session, char *option);
void quit(session_t *session, char *option);
void syst(session_t *session, char *option);

void help(session_t *session, char *option);
void noop(session_t *session, char *option);
void type(session_t *session, char *option);

int msg(session_t *session, int code, ...);

#endif //MY_FTP_CMD_H
