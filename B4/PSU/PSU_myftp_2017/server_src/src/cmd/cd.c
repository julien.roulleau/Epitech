/*
** EPITECH PROJECT, 2018
** my_ftp
** File description:
** cd.c
*/

#include <stdlib.h>
#include <interpreter/interpreter.h>
#include <dirent.h>
#include "cmd.h"

void pwd(session_t *session, char *option)
{
	char *path[PATH_MAX];

	(void) option;
	getcwd((char *) path, sizeof(path));
	msg(session, 257, path);
}

void cwd(session_t *session, char *option)
{
	if (strlen(option) && chdir(option) == 0)
		msg(session, 250);
	else
		msg(session, 500);
}

void cdup(session_t *session, char *option)
{
	(void) option;
	if (chdir("..") == 0)
		msg(session, 250);
	else
		msg(session, 500);
}

static void exec_ls(session_t *session)
{
	struct dirent *dirent;
	DIR *dir;

	dir = opendir("./");
	if (dir == NULL) {
		perror("opendir");
		return;
	}
	while ((dirent = readdir(dir))) {
		if (dirent->d_name[0] != '.')
			dprintf(session->dsock, "%s\r\n", dirent->d_name);
	}
	closedir(dir);
}

void list(session_t *session, char *option)
{
	(void) option;
	if (session->dsock == -1 || session->user.is_logged == false) {
		msg(session, 530);
		return;
	}
	msg(session, 150);
	exec_ls(session);
	close(session->dsock);
	session->dsock = -1;
	msg(session, 226);
}
