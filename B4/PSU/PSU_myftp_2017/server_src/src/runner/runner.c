/*
** EPITECH PROJECT, 2018
** lemipc
** File description:
** runner.c
*/

#include <signal.h>
#include <stdlib.h>
#include <wait.h>
#include "runner.h"
#include "socket/socket.h"

int ACTIVE_CLIENT = 0;

static void client_finish(int sig)
{
	int status;
	int pid = wait(&status);

	(void) sig;
	if (pid == -1) {
		perror("wait");
		exit(84);
	}
	printf("Process: %i return %i\n", pid, status);
	ACTIVE_CLIENT--;
}

void child_prossess(int sock, int csock)
{
	close(sock);
	interpreter(csock);
	close(csock);
	exit(0);
}

int run(int sock)
{
	int csock;
	int pid;

	signal(SIGCHLD, client_finish);
	while (1) {
		csock = wait_connection(sock);
		if (csock == -1)
			return (perror("wait_connection"), -1);
		ACTIVE_CLIENT++;
		pid = fork();
		if (pid == -1)
			return (perror("fork"), -1);
		else if (pid == 0) {
			child_prossess(sock, csock);
		} else
			close(csock);
	}
	return (0);
}