/*
** EPITECH PROJECT, 2018
** lemipc
** File description:
** runner.h
*/


#ifndef LEMIPC_RUNNER_H
#define LEMIPC_RUNNER_H

int run(int sock);

#endif //LEMIPC_RUNNER_H
