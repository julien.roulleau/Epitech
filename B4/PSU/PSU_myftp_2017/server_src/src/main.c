/*
** EPITECH PROJECT, 2018
** PSU_myftp_2017
** File description:
** main.c
*/

#include <stdlib.h>

#include "socket/socket.h"
#include "runner/runner.h"

static void display_help(void)
{
	printf("USAGE: ./server port path\n"
	"\tport is the port number on which the server socket listens\n"
	"\tpath is the path to the home directory for the Anonymous user\n");
}

int main(int ac, char **av)
{
	int sock;

	if (ac == 2 && !strcmp(av[1], "-help")) {
		display_help();
		return (0);
	}
	if (ac != 3)
		return (84);
	sock = create_connection(atoi(av[1]));
	if (chdir(av[2]) == -1)
		return (perror("chdir"), 84);
	if (sock == -1 || run(sock) == -1)
		return (84);
	if (close(sock) == -1)
		return (perror("close"), 84);
	return (0);
}
