/*
** EPITECH PROJECT, 2018
** malloc
** File description:
** write_header.c
*/

#include <stdio.h>
#include <elf.h>
#include <string.h>
#include "objdump.h"
#include "flag.h"

static const machine_t MACHINES[] = {
	{EM_NONE,        "None"},
	{EM_M32,         "WE32100"},
	{EM_SPARC,       "Sparc"},
	{EM_386,         "Intel 80386"},
	{EM_68K,         "MC68000"},
	{EM_88K,         "MC88000"},
	{EM_860,         "Intel 80860"},
	{EM_MIPS,        "MIPS R3000"},
	{EM_PARISC,      "HPPA"},
	{EM_SPARC32PLUS, "Sparc v8+"},
	{EM_PPC,         "PowerPC"},
	{EM_PPC64,       "PowerPC64"},
	{EM_S390,        "IBM S/390"},
	{EM_ARM,         "ARM"},
	{EM_SH,          "Renesas / SuperH SH"},
	{EM_SPARCV9,     "Sparc v9"},
	{EM_IA_64,       "Intel IA-64"},
	{EM_X86_64,      "i386:x86-64"},
	{(uint16_t) -1,  ""}
};

int have_section(Elf64_Ehdr const *ehdr, char *section)
{
	Elf64_Shdr *header_start = (Elf64_Shdr *) ((void *) ehdr +
						   ehdr->e_shoff);
	Elf64_Shdr header;
	char *strings = (char *) ((void *) ehdr +
				  header_start[ehdr->e_shstrndx].sh_offset);

	for (int i = 1; i < ehdr->e_shnum; i++) {
		header = header_start[i];
		if (!strcmp(strings + header.sh_name, section))
			return (1);
	}
	return (0);
}

static char const *get_flag_text(int flags)
{
	(void) flags;
	return "";
}

static int get_flag_value(Elf64_Ehdr const *ehdr)
{
	int flags = 0;

	if (have_section(ehdr, ".sht_rel") || have_section(ehdr, ".sht_rela"))
		flags |= HAS_RELOC;
	if (have_section(ehdr, ""))
		flags |= EXEC_P;
	if (have_section(ehdr, ""))
		flags |= HAS_LINENO;
	if (have_section(ehdr, ""))
		flags |= HAS_DEBUG;
	if (have_section(ehdr, ".sht_symtab"))
		flags |= HAS_SYMS;
	if (have_section(ehdr, ""))
		flags |= HAS_LOCALS;
	if (have_section(ehdr, ""))
		flags |= DYNAMIC;
	if (have_section(ehdr, ""))
		flags |= WP_TEXT;
	if (have_section(ehdr, ".sht_hash"))
		flags |= D_PAGED;
	return (flags);
}


static char const *machine_name(uint16_t e_machine)
{
	int i = -1;

	while (MACHINES[++i].key != (uint16_t) -1)
		if (MACHINES[i].key == e_machine)
			return (MACHINES[i].str);
	return ("i386:x86-64");
}

int display_header(elf_t *const elf)
{
	int flags = get_flag_value(elf->ehdr);

	printf("\n%s:     file format %s\n", elf->file_name, "elf64-x86-64");
	printf("architecture: %s, flags 0x%08x:\n",
	       machine_name(elf->ehdr->e_machine),
	       flags);
	printf("%s\n", get_flag_text(flags));
	printf("start address 0x%016lx\n\n", elf->ehdr->e_entry);
	return (1);
}
