/*
** EPITECH PROJECT, 2018
** malloc
** File description:
** display_data.c
*/

#include <stdio.h>
#include <string.h>
#include "objdump.h"

void print_line(int pos, uint8_t const *const data, int const size)
{
	int i = -1;

	printf(" %04x ", pos);
	while (++i < 16) {
		if (i < size)
			printf("%02x", data[i]);
		else
			printf("  ");
		if (!((i + 1) % 4))
			printf(" ");
	}
	printf(" ");
	i = -1;
	while (++i < 16) {
		if (i < size)
			printf("%c", IS_PRINTABLE(data[i]) ? data[i] : '.');
		else
			printf(" ");
	}
	printf("\n");
}

void print_section(Elf64_Shdr const shdr, Elf64_Ehdr const *ehdr)
{
	int j = (int) shdr.sh_offset;
	while ((unsigned) j < shdr.sh_offset +
			      shdr.sh_size) {
		print_line(
			(int) (shdr.sh_addr + j - shdr.sh_offset),
			(unsigned char *) ehdr + j,
			(int) (shdr.sh_offset + shdr.sh_size - j));
		j += 16;
	}
}

int is_skip(Elf64_Shdr const shdr, char const *str_section)
{
	if (((shdr.sh_type == SHT_SYMTAB
	      || shdr.sh_type == SHT_NOBITS
	      || shdr.sh_type == SHT_STRTAB)
	     && strcmp(&str_section[shdr.sh_name], ".dynstr") != 0)
	    || !shdr.sh_size)
		return (1);
	return (0);
}

int display_data(elf_t *const elf)
{
	int i = -1;

	while (++i < elf->ehdr->e_shnum) {
		if ((void *) &elf->shdr[i] > elf->end
		    || (void *) &elf->str_section[elf->shdr[i].sh_name]
		       > elf->end) {
			dprintf(2, "Invalid file\n");
			return (0);
		}
		if (is_skip(elf->shdr[i], elf->str_section))
			continue;
		printf("Contents of section %s:\n",
		       &elf->str_section[elf->shdr[i].sh_name]);
		print_section(elf->shdr[i], elf->ehdr);
	}
	return (1);
}