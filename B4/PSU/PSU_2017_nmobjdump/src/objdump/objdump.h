/*
** EPITECH PROJECT, 2018
** malloc
** File description:
** objdump.h
*/


#ifndef MALLOC_OBJDUMP_H
#define MALLOC_OBJDUMP_H

#include <elf.h>
#include <sys/stat.h>

typedef struct {
	uint16_t const key;
	char const *const str;
} machine_t;

typedef struct {
	Elf64_Ehdr const *ehdr;
	Elf64_Shdr const *shdr;
	void *end;
	char const *str_section;
	char const *file_name;
	struct stat file_stat;
} elf_t;

# define IS_PRINTABLE(x) ((x) >= ' ' && (x) <= '~')

int file_parser(elf_t *elf);
int display_header(elf_t *elf);
int display_data(elf_t *elf);

#endif //MALLOC_OBJDUMP_H
