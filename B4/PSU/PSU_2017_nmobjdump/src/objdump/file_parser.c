/*
** EPITECH PROJECT, 2018
** malloc
** File description:
** file.c
*/

#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdio.h>
#include <string.h>
#include "objdump.h"

static int format_verification(elf_t *const elf)
{
	char magic[] = "\177ELF";

	if ((size_t) elf->file_stat.st_size < sizeof(Elf64_Ehdr)
	    || memcmp(elf->ehdr, magic, sizeof(magic) - 1) != 0) {
		dprintf(2, "File format not recognized\n");
		return (0);
	}
	return (1);
}

static int file_verificator(elf_t *const elf)
{
	elf->shdr = (void *) elf->ehdr + elf->ehdr->e_shoff;
	elf->str_section =
		(char *) ((void *) elf->ehdr +
			  elf->shdr[elf->ehdr->e_shstrndx].sh_offset);
	if ((void *) (elf->shdr) > elf->end
	    || (void *) &elf->shdr[elf->ehdr->e_shstrndx] > elf->end
	    || (void *) (elf->str_section) > elf->end) {
		dprintf(2, "Invalid file\n");
		return (0);
	}
	return (1);
}

static int set_stat(int fd, elf_t *const elf)
{
	if (fstat(fd, &elf->file_stat) == -1) {
		perror("fstat");
		return (0);
	}
	return (1);
}

static int set_map(int fd, elf_t *const elf)
{
	elf->ehdr = mmap(NULL, (size_t) elf->file_stat.st_size, PROT_READ,
			 MAP_PRIVATE, fd, 0);
	if (elf->ehdr == (void *) -1) {
		perror("mmap");
		return (0);
	}
	elf->end = (void *) elf->ehdr + elf->file_stat.st_size;
	return (1);
}

int file_parser(elf_t *const elf)
{
	int fd;

	fd = open(elf->file_name, O_RDONLY);
	if (fd == -1) {
		dprintf(2, "objdump: '%s': No such file\n", elf->file_name);
		return (0);
	}
	if (!set_stat(fd, elf) || !set_map(fd, elf)) {
		close(fd);
		return (0);
	}
	close(fd);
	return (format_verification(elf) && file_verificator(elf));
}
