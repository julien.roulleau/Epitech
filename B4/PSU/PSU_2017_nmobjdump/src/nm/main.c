/*
** EPITECH PROJECT, 2018
** malloc
** File description:
** main.c
*/

#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include "nm.h"

static char **set_arg(int ac, char **av)
{
	int i = 0;
	char **arg = malloc(sizeof(char *) * (ac > 1 ? ac : 2));

	if (!arg)
		return (NULL);
	if (ac == 1) {
		arg[0] = strdup("a.out");
		arg[1] = NULL;
	} else {
		while (av[++i])
			arg[i - 1] = av[i];
		arg[i - 1] = NULL;
	}
	return (arg);
}

int main(int ac, char **av)
{
	elf_t elf;
	char **arg = set_arg(ac, av);
	int i = -1;

	if (!arg)
		return (84);
	while (arg[++i]) {
		elf.file_name = arg[i];
		if (ac > 2)
			printf("\n%s:\n", elf.file_name);
		if (!file_parser(&elf)
		    || !nm(&elf))
			return (84);
	}
	if (ac == 1)
		free(arg[0]);
	free(arg);
	return (0);
}