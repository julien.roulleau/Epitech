/*
** EPITECH PROJECT, 2018
** malloc
** File description:
** symbol.c
*/

#include <stdio.h>
#include "nm.h"

static const type_t TYPE[] = {
	{'B', SHT_NOBITS,   SHF_ALLOC | SHF_WRITE},
	{'D', SHT_PROGBITS, SHF_ALLOC | SHF_WRITE},
	{'R', SHT_PROGBITS, SHF_ALLOC},
	{'R', SHT_PROGBITS, 0},
	{'T', SHT_PROGBITS, SHF_ALLOC | SHF_EXECINSTR},
	{'U', SHT_NULL,     0},
	{0, 0,              0}
};

char get_type(elf_t *const elf, Elf64_Sym const *const sym)
{
	int i = -1;

	while (TYPE[++i].c) {
		if ((void *) &elf->shdr[sym->st_shndx] > elf->end) {
			dprintf(2, "Invalid file\n");
			return (0);
		}
		if (elf->shdr[sym->st_shndx].sh_type == TYPE[i].type
		    && elf->shdr[sym->st_shndx].sh_flags == TYPE[i].flags)
			return (TYPE[i].c);
	}
	return ('?');
}

static const type_t SYM_TYPE[] = {
	{'A', SHN_ABS,    0},
	{'C', SHN_COMMON, 0},
	{'U', SHN_UNDEF,  0},
	{0, 0,            0}
};

char get_type_by_sym(Elf64_Sym const *const sym)
{
	int i = -1;

	while (SYM_TYPE[++i].c)
		if (sym->st_shndx == SYM_TYPE[i].type)
			return (SYM_TYPE[i].c);
	return (0);
}

int set_symbol(elf_t *const elf,
		Elf64_Sym const *const sym,
		symbol_t *const symbol)
{
	char type = get_type_by_sym(sym);

	type = type ? type : get_type(elf, sym);
	if (!type)
		return (0);
	if (ELF64_ST_BIND(sym->st_info) == STB_LOCAL)
		type = (char) (type >= 'A' && type <= 'Z' ?
			       type + 32 : type);
	if (ELF64_ST_BIND(sym->st_info) == STB_WEAK)
		type = 'w';
	symbol->value = (unsigned int) sym->st_value;
	symbol->type = type;
	symbol->name = &elf->str_section[sym->st_name];
	return (1);
}
