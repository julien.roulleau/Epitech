/*
** EPITECH PROJECT, 2018
** malloc
** File description:
** error.c
*/

#include <stdio.h>
#include <string.h>
#include "nm.h"

static int format_verificator(elf_t *const elf)
{
	char magic[] = "\177ELF";

	if ((size_t) elf->file_stat.st_size < sizeof(Elf64_Ehdr)
	    || memcmp(elf->ehdr, magic, sizeof(magic) - 1) != 0) {
		dprintf(2, "File format not recognized\n");
		return (0);
	}
	return (1);
}

static int file_verificator(elf_t *const elf)
{
	if ((void *) (elf->shdr) > elf->end
	    || (void *) &elf->shdr[elf->ehdr->e_shstrndx] > elf->end) {
		dprintf(2, "Invalid file\n");
		return (0);
	}
	return (1);
}


static int set_sym(elf_t *const elf, int i)
{
	elf->sym_start = (Elf64_Sym *) (
		(void *) elf->ehdr + elf->shdr[i].sh_offset);
	elf->sym_end = (Elf64_Sym *) (
		(void *) elf->sym_start + elf->shdr[i].sh_size);
	elf->str_section =
		(char *) elf->ehdr + elf->shdr[elf->shdr[i].sh_link].sh_offset;
	if ((void *) (elf->sym_start) > elf->end
	    || (void *) (elf->sym_end) > elf->end
	    || (void *) (elf->str_section) > elf->end)
		return (0);
	return (1);
}


static int check_shdr(elf_t *const elf)
{
	int i = -1;

	elf->shdr = (Elf64_Shdr *) ((void *) elf->ehdr + elf->ehdr->e_shoff);
	if ((void *) elf->shdr > elf->end) {
		dprintf(2, "Invalid file\n");
		return (0);
	}
	while (++i != elf->ehdr->e_shnum) {

		if ((void *) &(elf->shdr[i]) > elf->end)
			return (fprintf(stderr, "Invalid file\n") && 0);
		if (elf->shdr[i].sh_type == SHT_SYMTAB && !set_sym(elf, i)) {
			dprintf(2, "Invalid file\n");
			return (0);
		}
	}
	if (elf->sym_end < elf->sym_start) {
		dprintf(2, "nm: %s: no symbols\n", elf->file_name);
		return (0);
	}
	return (1);
}

int check(elf_t *const elf)
{
	return (format_verificator(elf)
		&& file_verificator(elf)
		&& check_shdr(elf));
}