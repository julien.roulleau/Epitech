/*
** EPITECH PROJECT, 2018
** malloc
** File description:
** nm.h
*/


#ifndef MALLOC_NM_H
#define MALLOC_NM_H

#include <elf.h>
#include <sys/stat.h>

typedef struct {
	unsigned int value;
	char type;
	char const *name;
} symbol_t;

typedef struct {
	Elf64_Ehdr const *ehdr;
	Elf64_Shdr const *shdr;
	Elf64_Sym const *sym_start;
	Elf64_Sym const *sym_end;
	char const *str_section;
	void *end;
	char const *file_name;
	struct stat file_stat;
} elf_t;

typedef struct {
	char c;
	uint32_t type;
	uint64_t flags;
} type_t;

int file_parser(elf_t *elf);
int nm(elf_t *elf);
int set_symbol(elf_t *elf, Elf64_Sym const *sym, symbol_t *symbol);
void sort_sym(symbol_t *sym_list);
int check(elf_t *elf);

#endif //MALLOC_NM_H
