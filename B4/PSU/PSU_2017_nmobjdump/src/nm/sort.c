/*
** EPITECH PROJECT, 2018
** malloc
** File description:
** sort.c
*/

#include <string.h>
#include "nm.h"

static void swap_ptr(symbol_t *const a, symbol_t *const b)
{
	symbol_t c;

	c = *a;
	*a = *b;
	*b = c;
}

static int cmp_after_(char const *a, char const *b)
{
	while (*a == '_')
		++a;
	while (*b == '_')
		++b;
	return (strcasecmp(a, b));
}

void sort_sym(symbol_t *const
sym_list)
{
	int i;
	int j;

	i = -1;
	while (sym_list[++i].value || sym_list[i].type || sym_list[i].name) {
		j = i - 1;
		while (sym_list[++j].value || sym_list[j].type
		       || sym_list[j].name)
			if (cmp_after_(sym_list[i].name, sym_list[j].name) > 0)
				swap_ptr(&sym_list[i], &sym_list[j]);
	}
}
