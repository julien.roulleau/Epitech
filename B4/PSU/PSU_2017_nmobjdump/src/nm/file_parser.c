/*
** EPITECH PROJECT, 2018
** malloc
** File description:
** file.c
*/

#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdio.h>
#include "nm.h"

static int set_stat(int fd, elf_t *const elf)
{
	if (fstat(fd, &elf->file_stat) == -1) {
		perror("fstat");
		return (0);
	}
	return (1);
}

static int set_map(int fd, elf_t *const elf)
{
	elf->ehdr = mmap(NULL, (size_t) elf->file_stat.st_size, PROT_READ,
			 MAP_PRIVATE, fd, 0);
	if (elf->ehdr == (void *) -1) {
		perror("mmap");
		return (0);
	}
	elf->end = (void *) elf->ehdr + elf->file_stat.st_size;
	return (1);
}

int file_parser(elf_t *const elf)
{
	int fd;

	fd = open(elf->file_name, O_RDONLY);
	if (fd == -1) {
		dprintf(2, "nm: '%s': No such file\n", elf->file_name);
		return (0);
	}
	if (!set_stat(fd, elf) || !set_map(fd, elf)) {
		close(fd);
		return (0);
	}
	close(fd);
	elf->shdr = (void *) elf->ehdr + elf->ehdr->e_shoff;
	return (check(elf));
}
