/*
** EPITECH PROJECT, 2018
** malloc
** File description:
** nm.c
*/

#include <unistd.h>
#include <malloc.h>
#include <elf.h>
#include "nm.h"

static symbol_t *get_sym_list(elf_t *const elf)
{
	symbol_t *sym_list = calloc(
		sizeof(symbol_t) * (elf->sym_end - elf->sym_start),
		elf->sym_end - elf->sym_start);
	Elf64_Sym *tmp = (Elf64_Sym *) elf->sym_start;
	int i = 0;

	if (!sym_list)
		return (NULL);
	while (tmp < elf->sym_end) {
		if (tmp->st_info != STT_FILE && tmp->st_info != STT_SECTION
		    && tmp->st_info != STT_NOTYPE) {
			if ((void *) &elf->str_section[tmp->st_name] > elf->end)
				return (dprintf(2, "Invalid file\n")
					* 0 + NULL);
			if (!set_symbol(elf, tmp, &sym_list[i++]))
				return (NULL);
		}
		++tmp;
	}
	return (sym_list);
}

static void display(symbol_t *sym_list)
{
	int i = -1;

	while (sym_list[++i].value || sym_list[i].type || sym_list[i].name) {
		if (sym_list[i].value)
			printf("%016x %c %s\n", sym_list[i].value,
			       sym_list[i].type, sym_list[i].name);
		else
			printf("%18c %s\n", sym_list[i].type, sym_list[i].name);
	}
}

int nm(elf_t *const elf)
{
	symbol_t *sym_list = get_sym_list(elf);

	if (!sym_list)
		return (0);
	sort_sym(sym_list);
	display(sym_list);
	free(sym_list);
	return (1);
}
