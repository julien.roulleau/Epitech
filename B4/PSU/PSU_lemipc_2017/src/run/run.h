/*
** EPITECH PROJECT, 2018
** lemipc
** File description:
** run.h
*/


#ifndef LEMIPC_RUN_H
#define LEMIPC_RUN_H

#include <stdbool.h>

#include "player/player.h"
#include "mem/mem.h"

#define IN_ARENA(x, y) (arena[(x)][(y)] && arena[(x)][(y)] != player->team_id)

bool run(mem_t *mem, player_t *player);

#endif //LEMIPC_RUN_H
