/*
** EPITECH PROJECT, 2018
** lemipc
** File description:
** run.c
*/

#include <unistd.h>
#include <mem/mem.h>

#include "sem/sem.h"
#include "run.h"

static int count_player_around(char **arena, player_t *player)
{
	int i = 0;
	int x = player->pos.x;
	int y = player->pos.y;

	if (x < MAP_W - 1) {
		if (y < MAP_H - 1)
			i += (IN_ARENA(x + 1, y + 1)) ? 1 : 0;
		i += (IN_ARENA(x + 1, y)) ? 1 : 0;
		if (y > 0)
			i += (IN_ARENA(x + 1, y - 1)) ? 1 : 0;
	}
	i += (IN_ARENA(x, y + 1)) ? 1 : 0;
	i += (IN_ARENA(x, y - 1)) ? 1 : 0;
	if (x > 0) {
		if (y < MAP_H - 1)
			i += (IN_ARENA(x - 1, y + 1)) ? 1 : 0;
		i += (IN_ARENA(x - 1, y)) ? 1 : 0;
		if (y > 0)
			i += (IN_ARENA(x - 1, y - 1)) ? 1 : 0;
	}
	return (i);
}

static bool move_center(char **arena, player_t *player)
{
	int center_x = MAP_W / 2;
	int center_y = MAP_H / 2;
	int x = player->pos.x;
	int y = player->pos.y;

	if (ABS(x - center_x) > ABS(y - center_y))
		x = x - center_x < 0 ? x + 1: x - 1;
	else
		y = y - center_y < 0 ? y + 1: y - 1;
	if (!arena[x][y]) {
		arena[player->pos.x][player->pos.y] = 0;
		arena[x][y] = (char) player->team_id;
		player->pos.x = x;
		player->pos.y = y;
	}
	return (true);
}

static int count_player_not_in_team(mem_t *mem, player_t *player)
{
	int i = 0;

	sem_lock(mem->sem_id);
	for (int x = 0; x < MAP_W; x++)
		for (int y = 0; y < MAP_H; y++)
			if (mem->arena[x][y]
			    && mem->arena[x][y] != player->team_id) {
				i++;
			}
	sem_unlock(mem->sem_id);
	return (i);
}

bool run(mem_t *mem, player_t *player)
{
	while (!count_player_not_in_team(mem, player));
	while (count_player_not_in_team(mem, player)) {
		usleep(500000);
		sem_lock(mem->sem_id);
		if (count_player_around(mem->arena, player) >= 2)
			break;
		move_center(mem->arena, player);
		sem_unlock(mem->sem_id);
	}
	sem_unlock(mem->sem_id);
	return (true);
}