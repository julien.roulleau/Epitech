/*
** EPITECH PROJECT, 2018
** philosophers
** File description:
** main.c
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "run/run.h"
#include "display/display.h"
#include "sig/sig.h"

static void usage()
{
	printf(""
		       "USAGE\n"
		       "\t./lemipc PATH TEAM_NUMBER\n"
		       "\n"
		       "DESCRIPTION\n"
		       "\tPATH\t\tpath you’ll give to ftok\n"
		       "\tTEAM_NUMBER\tteam number of the current "
		       "champion (greater than 0)\n");
}

static bool starter(char *path, int team_id)
{
	mem_t mem;
	player_t player;

	if (!init_mem(&mem, path) || !init_player(&player, &mem, team_id)
		|| !init_sig(&mem, &player, path))
		return (false);
	if (mem.first && pthread_create(&mem.th, NULL, &display, &mem))
		return (false);
	if (!run(&mem, &player))
		return (false);
	destroy_player(&mem, &player);
	if (count_player(&mem) == 0) {
		printf("team %i win\n", player.team_id);
		destroy_mem(&mem, path);
	}
	if (mem.first && pthread_join(mem.th, NULL))
		return (false);
	free(mem.arena);
	return (true);
}

static bool argument_gestionner(int ac, char **av)
{
	int team_id = 0;

	if (ac == 2 && !strcmp(av[1], "--help"))
		usage();
	else if (ac == 3) {
		team_id = atoi(av[2]);
		if (!team_id) {
			printf("Invalid team number\n");
			return (false);
		}
		if (!starter(av[1], team_id))
			return (false);
	} else {
		printf("Invalid arguments\n");
		return (false);
	}
	return (true);
}

int main(int ac, char **av)
{
	if (!argument_gestionner(ac, av))
		return (84);
	else
		return (0);
}