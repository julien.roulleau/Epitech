/*
** EPITECH PROJECT, 2018
** lemipc
** File description:
** sem.c
*/

#include <sys/sem.h>
#include "sem.h"

bool sem_lock(int sem_id)
{
	if (semop(sem_id, &(struct sembuf) {0, -1, 0}, 1) == -1)
		return (false);
	return (true);
}

bool sem_unlock(int sem_id)
{
	if (semop(sem_id, &(struct sembuf) {0, 1, 0}, 1) == -1)
		return (false);
	return (true);
}