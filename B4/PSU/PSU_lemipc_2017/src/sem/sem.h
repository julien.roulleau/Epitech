/*
** EPITECH PROJECT, 2018
** lemipc
** File description:
** sem.h
*/


#ifndef LEMIPC_SEM_H
#define LEMIPC_SEM_H

#include <stdbool.h>

bool sem_lock(int sem);
bool sem_unlock(int sem);

#endif //LEMIPC_SEM_H
