/*
** EPITECH PROJECT, 2018
** lemipc
** File description:
** display.h
*/


#ifndef LEMIPC_DISPLAY_H
#define LEMIPC_DISPLAY_H

void *display(void *data);

#endif //LEMIPC_DISPLAY_H
