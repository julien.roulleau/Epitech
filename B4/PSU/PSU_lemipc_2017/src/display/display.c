/*
** EPITECH PROJECT, 2018
** lemipc
** File description:
** thread.c
*/

#include <stdio.h>
#include <unistd.h>
#include "player/player.h"
#include "sem/sem.h"

static void display_arena(char **arena)
{
	printf("\033[%dA\n", MAP_H + 1);
	for (int x = 0; x < MAP_W; x++) {
		for (int y = 0; y < MAP_H; y++) {
			printf("%02i", arena[x][y]);
			if (y != MAP_W - 1)
				printf(" ");
		}
		printf("\n");
	}
}

void *display(void *data)
{
	mem_t *mem = data;

	printf("\033[?25l");
	printf("\033[%dS", MAP_H);
	while (count_player(mem)) {
		sem_lock(mem->sem_id);
		display_arena(mem->arena);
		sem_unlock(mem->sem_id);
		usleep(500000);
	}
	printf("\033[?25h");
	return (NULL);
}