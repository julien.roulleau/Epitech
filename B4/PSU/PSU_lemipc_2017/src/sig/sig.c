/*
** EPITECH PROJECT, 2018
** lemipc
** File description:
** signal.c
*/

#include <stdlib.h>
#include <signal.h>

#include "player/player.h"
#include "sig.h"

static mem_t *get_mem(mem_t *save)
{
	static mem_t *mem = NULL;

	if (save)
		mem = save;
	return (mem);
}

static player_t *get_player(player_t *save)
{
	static player_t *player = NULL;

	if (save)
		player = save;
	return (player);
}

static char *get_path(char *save)
{
	static char *path = NULL;

	if (save)
		path = save;
	return (path);
}

static void destroy(int sig)
{
	mem_t *mem = get_mem(NULL);
	player_t *player = get_player(NULL);

	(void) sig;
	destroy_player(mem, player);
	if (count_player(mem) == 0) {
		destroy_mem(mem, get_path(NULL));
	}
	if (mem->first)
		pthread_cancel(mem->th);
	free(mem->arena);
	exit(84);
}

bool init_sig(mem_t *mem, player_t *player, char *path)
{
	get_mem(mem);
	get_player(player);
	get_path(path);
	if (signal(SIGINT, destroy) == SIG_ERR
	    || signal(SIGTERM, destroy) == SIG_ERR
	    || signal(SIGSEGV, destroy) == SIG_ERR
	    || signal(SIGBUS, destroy) == SIG_ERR
	    || signal(SIGFPE, destroy) == SIG_ERR)
		return (false);
	return (true);
}

