/*
** EPITECH PROJECT, 2018
** lemipc
** File description:
** signal.h
*/


#ifndef LEMIPC_SIG_H
#define LEMIPC_SIG_H

bool init_sig(mem_t *mem, player_t *player, char *path);

#endif //LEMIPC_SIG_H
