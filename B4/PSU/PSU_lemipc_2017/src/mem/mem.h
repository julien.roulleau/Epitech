/*
** EPITECH PROJECT, 2018
** lemipc
** File description:
** map.h
*/


#ifndef LEMIPC_MAP_H
#define LEMIPC_MAP_H

#include <sys/ipc.h>
#include <stdbool.h>
#include <pthread.h>

typedef struct {
	bool first;
	int sem_id;
	int shm_id;
	int msg_id;
	pthread_t th;
	key_t key;
	char **arena;
} mem_t;

extern const int MAP_W;
extern const int MAP_H;

bool init_mem(mem_t *mem, char *path);

bool destroy_mem(mem_t *mem, char *path);

#endif //LEMIPC_MAP_H
