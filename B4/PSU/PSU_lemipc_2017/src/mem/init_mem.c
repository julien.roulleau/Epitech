/*
** EPITECH PROJECT, 2018
** lemipc
** File description:
** init_mem.c
*/

#include <stdlib.h>
#include <sys/shm.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/sem.h>
#include <sys/msg.h>

#include "mem.h"

const int MAP_W = 20;
const int MAP_H = 20;

static void set_arena(char **arena)
{
	for (int x = 0; x < MAP_W; x++)
		for (int y = 0; y < MAP_H; y++)
			arena[x][y] = 0;
}

static char **get_arena(int shm_id)
{
	char **arena = malloc(MAP_W * MAP_H * sizeof(char *));
	void *sh_arena;

	sh_arena = shmat(shm_id, NULL, SHM_R | SHM_W);
	if (sh_arena == (void *) -1)
		return (false);
	if (!arena)
		return (NULL);
	for (int x = 0; x < MAP_W; x++)
		arena[x] = &((char *) sh_arena)[MAP_H * x];
	return (arena);
}

static bool set_mem(mem_t *mem)
{
	mem->first = true;
	mem->shm_id = shmget(mem->key, (MAP_H * MAP_W),
			     IPC_CREAT | SHM_R | SHM_W);
	if (mem->shm_id == -1)
		return (false);
	mem->arena = get_arena(mem->shm_id);
	mem->sem_id = semget(mem->key, 1, SHM_R | SHM_W | IPC_CREAT);
	mem->msg_id = msgget(mem->key, SHM_R | SHM_W | IPC_CREAT);
	if (!mem->arena || mem->sem_id == -1 || mem->msg_id == -1
	    || semctl(mem->sem_id, 0, SETVAL, 1) == -1)
		return (false);
	set_arena(mem->arena);
	return (true);
}

static bool get_mem(mem_t *mem)
{
	mem->first = false;
	mem->arena = get_arena(mem->shm_id);
	mem->sem_id = semget(mem->key, 1, SHM_R | SHM_W);
	if (!mem->arena || mem->sem_id == -1)
		return (false);
	return (true);
}

bool init_mem(mem_t *mem, char *path)
{
	close(open(path, O_CREAT));
	mem->key = ftok(path, 0);
	if (mem->key == -1)
		return (false);
	mem->shm_id = shmget(mem->key, (MAP_H * MAP_W), SHM_R | SHM_W);
	if (mem->shm_id == -1) {
		if (!set_mem(mem))
			return (false);
	} else {
		usleep(500000);
		if (!get_mem(mem))
			return (false);
	}
	return (true);
}