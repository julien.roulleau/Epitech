/*
** EPITECH PROJECT, 2018
** lemipc
** File description:
** destroy_mem.c
*/

#include <sys/sem.h>
#include <sys/shm.h>
#include <unistd.h>
#include <sys/msg.h>
#include "mem.h"

bool destroy_mem(mem_t *mem, char *path)
{
	if (semctl(mem->sem_id, 0, IPC_RMID)
	    || shmctl(mem->msg_id, 0, IPC_RMID)
	    || msgctl(mem->shm_id, 0, IPC_RMID)
	    || unlink(path))
		return (false);
	return (true);
}