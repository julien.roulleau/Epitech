/*
** EPITECH PROJECT, 2018
** lemipc
** File description:
** player.h
*/


#ifndef LEMIPC_PLAYER_H
#define LEMIPC_PLAYER_H

#include <stdbool.h>
#include "mem/mem.h"

typedef struct {
	int x;
	int y;
} pos_t;

typedef struct {
	int team_id;
	pos_t pos;
} player_t;

#define ABS(x) ((x) > 0 ? (x) : -(x))

bool init_player(player_t *player, mem_t *mem, int team_id);

bool destroy_player(mem_t *mem, player_t *player);
int count_player(mem_t *mem);

#endif //LEMIPC_PLAYER_H
