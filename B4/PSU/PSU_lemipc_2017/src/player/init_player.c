/*
** EPITECH PROJECT, 2018
** lemipc
** File description:
** init_player.c
*/

#include <stdlib.h>
#include <time.h>

#include "sem/sem.h"
#include "player.h"

bool init_player(player_t *player, mem_t *mem, int team_nbr)
{
	int x = rand() % MAP_W;
	int y = rand() % MAP_H;

	srand((unsigned int) time(NULL));
	player->team_id = team_nbr;
	sem_lock(mem->sem_id);
	while (mem->arena[x][y]) {
		x = rand() % MAP_W;
		y = rand() % MAP_H;
	}
	player->pos.x = x;
	player->pos.y = y;
	mem->arena[x][y] = (char) team_nbr;
	sem_unlock(mem->sem_id);
	return (true);
}