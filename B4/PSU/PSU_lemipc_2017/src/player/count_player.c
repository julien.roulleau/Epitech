/*
** EPITECH PROJECT, 2018
** lemipc
** File description:
** count_player.c
*/

#include "mem/mem.h"
#include "sem/sem.h"
#include "player.h"

int count_player(mem_t *mem)
{
	int i = 0;

	sem_lock(mem->sem_id);
	for (int x = 0; x < MAP_W; x++)
		for (int y = 0; y < MAP_H; y++)
			if (mem->arena[x][y]) {
				i++;
			}
	sem_unlock(mem->sem_id);
	return (i);
}