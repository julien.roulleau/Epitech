/*
** EPITECH PROJECT, 2018
** lemipc
** File description:
** destroy_player.c
*/

#include <mem/mem.h>
#include "sem/sem.h"
#include "player.h"

bool destroy_player(mem_t *mem, player_t *player)
{
	sem_lock(mem->sem_id);
	mem->arena[player->pos.x][player->pos.y] = 0;
	sem_unlock(mem->sem_id);
	return (true);
}
