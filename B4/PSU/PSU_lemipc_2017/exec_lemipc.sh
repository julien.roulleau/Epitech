#!/bin/bash

if [ $# = 1 ] && [ $1 = "-h" ]
then
    echo "USAGE"
    echo "      ./exec_lemipc.sh PATH NUMBER_TEAM PLAYERS_IN_TEAM"
    echo ""
    echo "DESCRIPTION"
    echo "      PATH            path you’ll give to ftok"
    echo "      NUMBER_TEAM     number of team that will be create"
    echo "      PLAYERS_IN_TEAM number of players in each team"
    exit 0
fi

if [ $# != 3 ]
then
    echo "./exec_lemipc : Invalid number of arguments"
    exit 1
fi

re='^[0-9]+$'
if ! [[ $2 =~ $re ]] ; then
   echo "error: NUMBER_TEAM : Not a number" >&2; exit 1
fi

if ! [[ $3 =~ $re ]] ; then
   echo "error: PLAYERS_IN_TEAM : Not a number" >&2; exit 1
fi

for i in `seq $2`
do
    for j in `seq $3`
    do
        ./lemipc $1 $i &
    done
done