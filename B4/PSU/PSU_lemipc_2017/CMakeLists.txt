cmake_minimum_required(VERSION 3.9)
project(lemipc)

INCLUDE_DIRECTORIES(src)
set(CMAKE_C_STANDARD 90)

FILE(GLOB src src/*.c src/*.h)
FILE(GLOB run src/run/*.c src/run/*.h)
FILE(GLOB player src/player/*.c src/player/*.h)
FILE(GLOB mem src/mem/*.c src/mem/*.h)
FILE(GLOB sem src/sem/*.c src/sem/*.h)
FILE(GLOB display src/display/*.c src/display/*.h)
FILE(GLOB sig src/sig/*.c src/sig/*.h)

add_executable(not ${src} ${run} ${player} ${mem} ${sem} ${display} ${sig})
