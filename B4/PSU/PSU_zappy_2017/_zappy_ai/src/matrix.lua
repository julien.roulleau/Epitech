--- @module matrix

local matrix = {}

function matrix.new(rows, cols)
	local mat = {}
	local meta = {}
	
	if rows <= 0 or cols <= 0 then return nil end

	for i = 1, rows do
		mat[i] = {}
		for j = 1, cols do
			mat[i][j] = 0
		end
	end

	function meta.__index()
		return
	end

	function meta.__newindex()
		return
	end

	function meta:__add(m)
		local ret = matrix.new(rows, cols)
		if type(m) == 'number' then
			for i = 1, rows do
				for j = 1, cols do
				ret[i][j] = self[i][j] + m
				end
			end
		else
			local r, c = #m, #m[1]
			if rows ~= r or cols ~= c then return nil end
			for i = 1, rows do
				for j = 1, cols do
					ret[i][j] = self[i][j] + m[i][j]
				end
			end
		end
		return ret
	end

	function meta:__sub(m)
		local ret = matrix.new(rows, cols)
		if type(m) == 'number' then
			for i = 1, rows do
				for j = 1, cols do
					ret[i][j] = self[i][j] - m
				end
			end
		else
			local r, c = #m, #m[1]
			if rows ~= r or cols ~= c then return nil end
			for i = 1, rows do
				for j = 1, cols do
					ret[i][j] = self[i][j] - m[i][j]
				end
			end
		end
		return ret
	end

	function meta:__mul(m)
		if type(m) == 'number' then
			local ret = matrix.new(rows, cols)
			for i = 1, rows do
				for j = 1, cols do
					ret[i][j] = self[i][j] * m
				end
			end
			return ret
		end
		local r, c = #m, #m[1]
		if cols ~= r then return nil end
		local ret = matrix.new(rows, c)
		for i = 1, rows do
			for j = 1, c do
				local sum = 0
				for k = 1, cols do
					sum = sum + self[i][k] * m[k][j]
				end
				ret[i][j] = sum
			end
		end
		return ret
	end

	function meta:__div(m)
		if type(m) == 'number' then
			local ret = matrix.new(rows, cols)
			for i = 1, rows do
				for j = 1, cols do
					ret[i][j] = self[i][j] / m
				end
			end
			return ret
		end
		return self * -m
	end

	function meta:__mod(m)
		return 'not yet'
	end

	function meta:__pow(m)
		return 'not yet'
	end

	function meta:__unm()
		local ret = matrix.new(cols, rows)
		for i = 1, rows do
			for j = 1, cols do
				ret[j][i] = self[i][j]
			end
		end
		return ret
	end

	function meta:__tostring()
		s = ""
		for i = 1, rows do
			if rows == 1 then s = s .. "["
			elseif i == 1 then s = s .. "⎡"
			elseif i == rows then s = s .. "⎣"
			else s = s .. "⎢" end

			for j = 1, cols do
				if j ~= 1 then s = s .. ", " end
				s = s .. self[i][j]
			end

			if rows == 1 then s = s .. "]\n"
			elseif i == 1 then s = s .. "⎤\n"
			elseif i == rows then s = s .. "⎦\n"
			else s = s .. "⎥\n" end
		end
		return s
	end

	function meta:__concat(m)
		return 'not yet'
	end

	function meta:__eq(m)
		local r, c = #m, #m[1]
		if rows ~= r or cols ~= c then return false end
		for i = 1, rows do
			for j = 1, cols do
				if self[i][j] ~= m[i][j] then return false end
			end
		end
		return true
	end

	function meta.__lt(m1, m2)
		return false
	end

	function meta.__le(m1, m2)
		return false
	end

	function meta:__call(...)
		return
	end

	meta.__metatable = true

	setmetatable(mat, meta)
	return mat
end

function matrix.identity(rows, cols)
	cols = cols or rows
	local ret = matrix.new(rows, cols)
	local i = 1
	while i <= rows and i <= cols do
		ret[i][i] = 1
		i = i + 1
	end
	return ret
end

return matrix