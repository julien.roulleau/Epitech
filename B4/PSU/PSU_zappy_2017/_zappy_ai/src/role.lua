--
-- Created by IntelliJ IDEA.
-- User: na
-- Date: 24/06/18
-- Time: 12:41
-- To change this template use File | Settings | File Templates.
--

Net = require "src.Net"

proportion = {
    ["queen"] = 1,
    ["soldier"] = 1,
    ["miner"] = 2,
}

local Role = {
    role = {
        ["queen"] = 1,
        ["soldier"] = 0,
        ["miner"] = 0,
    }
}

Role.__index = Role

function Role:new()
    local role = {}
    setmetatable(role, Role)
    return role
end

function Role:giveRole()
    local role = "Queen"
    local min = self.role["queen"] * proportion["queen"]

    for key, nb in proportion do
        local p = self.role[key] * nb
        if p < min then
            min = p
            role = key
        end
    end
    Net:sendMsg("new " .. role)
end

function Role:questionRole()
    Net:sendMsg("role")
end

function Role.takeRole(AI)
    Net:sendMsg("new")
    Net.client:settimeout(2)
    local r = Net:recv()
    Net.client:settimeout(nil)
    if r == nil then
        AI.master = Role:new()
        AI.type = "Miner"

    else
        AI.type = r:substr(4)
    end
end

return Role