#! /usr/bin/lua5.3

Net = require "src.Net"
crypt = require "src.crypt"
IA = require "src.FSM.AI"
FSM = require "src.FSM.Finite_State_Machines"

function help()
    print("USAGE: ./zappy_ai -p port -n name -h machine")
    print("\tport\tis the port number")
    print("\tname\tis the name of the team")
    print("\tmachine\tis the name of the machine; localhost by default")
end

local port, name, ip = nil, nil, "localhost"

for i, value in ipairs(arg) do
    if value == "-p" then
        port = arg[i + 1]
    elseif value == "-n" then
        name = arg[i + 1]
    elseif value == "-h" then
        ip = arg[i + 1]
    elseif value == "-help" then
        help()
        os.exit(0)
    end
end

if port == nil or name == nil then
    help()
    os.exit(1)
end

local r, message
Net:connect(ip, port)
r, message = Net:recv()
if (r == nil) then
    print("Server not found")
    return
end

FSM(IA:new(nil, name))