--- @module Net

local crypt = require"src.crypt"
local socket = require"luapower.socket"

local Net = {
	client = false,
	id = -1,
	idx = 0,
	users = {}
}

function Net:msgCheck(id, idx)
	if not id then return false end
	idx = tonumber(idx or "0")
	if self.users[id] == nil and idx == 0 then self.users[id] = 0
	elseif self.users[id] == nil then return false
	elseif self.users[id] >= idx then return false
	end
	self.users[id] = idx
	return true
end

function Net:connect(host, port)
	if Net.client then Net.client:close() end
	Net.client, err = socket.connect(host, port)
	if not Net.client then io.stderr:write(err.."\n") end
	self.id = 0
	return not not Net.client, err
end

function Net:send(s)
	if not Net.client then return nil, 'not connected' end
	Net.client:send(s.."\n")
	print("→", s)
end

function Net:recv()
	if not Net.client then return nil, 'not connected' end
	return Net.client:receive()
end

function Net:sendMsg(s)
	local t, e = self:send("Broadcast " ..
		crypt.encrypt("#" .. self.id .. "%" .. self.idx .. "|" .. s))
	if not s then return t, e end
	self.idx = self.idx + 1
end

function Net:recvMsg()
	local s, e = self:recv()
	if not s then return 0, 0, s, e end
	if not s:gmatch"message [1-8], .+" then return 0, 0, nil, 'Bad message' end
	local pos = s:match"message (%d)"
	s = crypt.decrypt(s:match"message %d, (.+)")
	if not s:gmatch"#%d+%%%d+%|.+" then return 0, 0, nil, 'Bad format' end
	local id, idx, msg = s:match"#(%d+)%%", s:match"%%(%d+)%|", s:match"%|(.+)"
	if self:msgCheck(id, idx) then return pos, id, msg end
	return 0, 0, nil, 'Bad indexes'
end

local metatable = {
	__call = function()
		local self = {}
		setmetatable(self, {__index = Net})
		return self
	end
}
setmetatable(Net, metatable)
return Net
