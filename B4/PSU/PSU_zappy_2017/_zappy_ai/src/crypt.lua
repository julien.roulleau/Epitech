--- @module crypt

local crypt = {}
local seed1 = "1b791cd1dbc8bcf08d75cce3703c399a"
local seed2 = "9f9a747d1d6f49395ea5fcf085012984"
local seed3 = "59999afd8ea75cf42c5acddd25a5da79"
local alph = {
	"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
	"P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d",
	"e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s",
	"t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7",
	"8", "9", "+", "-", "*", "/", "_", " ", ".", ",", ":", ";", "?", "!", "=",
	"#", "|", "(", ")", "[", "]", "{", "}", "<", ">", "&", "@", "%", "~", "^"
}

local function find(c)
	for k, v in ipairs(alph) do
		if v == c then return k end
	end
end

local function sum(c, i)
	local t = find(c)
	if not t then return end
	return alph[(t
		+ tonumber(seed1:sub(i % 32 + 1, i % 32 + 1), 16)
		- tonumber(seed2:sub(i % 32 + 1, i % 32 + 1), 16)
		+ tonumber(seed3:sub(i % 32 + 1, i % 32 + 1), 16))
		% #alph + 1]
end

local function dec(c, i)
	local t = find(c)
	if not t then return end
	return alph[(t
		- tonumber(seed3:sub(i % 32 + 1, i % 32 + 1), 16)
		+ tonumber(seed2:sub(i % 32 + 1, i % 32 + 1), 16)
		- tonumber(seed1:sub(i % 32 + 1, i % 32 + 1), 16) - 2)
		% #alph + 1]
end

function crypt.encrypt(s)
	if not s then return "" end
	local ret = ""
	local k = 1
	for v in s:gmatch"." do
		ret = ret .. (sum(v, k) or v)
		k = k + 1
	end
	ret = ret:gsub(" ", "°")
	return ret
end

function crypt.decrypt(s)
	if not s then return "" end
	local ret = ""
	local k = 1
	s = s:gsub("°", " ")
	for v in s:gmatch"." do
		ret = ret .. (dec(v, k) or v)
		k = k + 1
	end
	return ret
end

return crypt