Queen = require "src.FSM.Queen"
Soldier = require "src.FSM.Soldier"
Miner = require "src.FSM.Miner"

function run(AI)
    local type
    print(AI.type)
    if AI.type == "Queen" then
        type = Queen:new(AI)
    elseif AI.type == "Miner" then
        type = Miner:new(AI)
    elseif AI.type == "Soldier" then
        type = Soldier:new(AI)
    else
        return -1
    end
    while AI.alive or AI.level < 8 do
        AI:messageRequest()
        if AI:eat() then
        elseif type:run() then
        else
            AI:wander()
        end
    end
    return 0
end

return run