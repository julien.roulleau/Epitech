--- @module AI
Net = require "src.Net"
Role = require "src.role"

totalLevelRequirement = {
    [1] = { ["player"] = 1, ["linemate"] = 9, ["deraumere"] = 8, ["sibur"] = 10, ["mendiane"] = 5, ["phiras"] = 6, ["thystame"] = 1 },
    [2] = { ["player"] = 2, ["linemate"] = 8, ["deraumere"] = 8, ["sibur"] = 10, ["mendiane"] = 5, ["phiras"] = 6, ["thystame"] = 1 },
    [3] = { ["player"] = 2, ["linemate"] = 7, ["deraumere"] = 7, ["sibur"] = 9, ["mendiane"] = 5, ["phiras"] = 6, ["thystame"] = 1 },
    [4] = { ["player"] = 4, ["linemate"] = 5, ["deraumere"] = 7, ["sibur"] = 8, ["mendiane"] = 5, ["phiras"] = 4, ["thystame"] = 1 },
    [5] = { ["player"] = 4, ["linemate"] = 4, ["deraumere"] = 6, ["sibur"] = 6, ["mendiane"] = 5, ["phiras"] = 3, ["thystame"] = 1 },
    [6] = { ["player"] = 6, ["linemate"] = 3, ["deraumere"] = 4, ["sibur"] = 5, ["mendiane"] = 2, ["phiras"] = 3, ["thystame"] = 1 },
    [7] = { ["player"] = 6, ["linemate"] = 2, ["deraumere"] = 2, ["sibur"] = 2, ["mendiane"] = 2, ["phiras"] = 2, ["thystame"] = 1 },
}

levelRequirement = {
    [1] = { ["player"] = 1, ["linemate"] = 1, ["deraumere"] = 0, ["sibur"] = 0, ["mendiane"] = 0, ["phiras"] = 0, ["thystame"] = 0 },
    [2] = { ["player"] = 2, ["linemate"] = 1, ["deraumere"] = 1, ["sibur"] = 1, ["mendiane"] = 0, ["phiras"] = 0, ["thystame"] = 0 },
    [3] = { ["player"] = 2, ["linemate"] = 2, ["deraumere"] = 0, ["sibur"] = 1, ["mendiane"] = 0, ["phiras"] = 2, ["thystame"] = 0 },
    [4] = { ["player"] = 4, ["linemate"] = 1, ["deraumere"] = 1, ["sibur"] = 2, ["mendiane"] = 0, ["phiras"] = 1, ["thystame"] = 0 },
    [5] = { ["player"] = 4, ["linemate"] = 1, ["deraumere"] = 2, ["sibur"] = 1, ["mendiane"] = 3, ["phiras"] = 0, ["thystame"] = 0 },
    [6] = { ["player"] = 6, ["linemate"] = 1, ["deraumere"] = 2, ["sibur"] = 3, ["mendiane"] = 0, ["phiras"] = 1, ["thystame"] = 0 },
    [7] = { ["player"] = 6, ["linemate"] = 2, ["deraumere"] = 2, ["sibur"] = 2, ["mendiane"] = 2, ["phiras"] = 2, ["thystame"] = 1 },
}

function findXY(i)
    local current, last, x = 1, 1, 0
    while current <= i do
        last = last + 2
        current = current + last
        x = x + 1
    end
    return x, i - (current - last)
end

function split(str, sep)
    local t = {}
    local i = 1

    if sep == nil then
        sep = "%s"
    end
    for str in string.gmatch(str, "([^" .. sep .. "]+)") do
        str = str:gsub("%[", "")
        str = str:gsub("%]", "")
        t[i] = str
        i = i + 1
    end
    return t
end

local AI = {
    minRequiredFood = 100,
    alive = nil,
    level = nil,
    team = nil,
    type = nil,
    requestInvocation = "Ahuu",
    requestWhoAreYou = "Who",
    responseWhoAreYou = "WithYou",
    orientation = 1,
    dirMessage = -1,
    invocRequest = false,
    whoAreYouRequest = false,
    countAlliesSameCase = -1,
    master = nil,
}

function AI:new(type, teamName)
    local ai = {}

    setmetatable(ai, self)
    self.__index = self
    if type == nil then
        Role.takeRole(ai)
    else
        self.type = type
    end
    self:join(teamName)
    return ai
end

function AI:join(teamName)
    self.team = teamName
    Net:send(teamName)
    local r = self:getRecv()
    if (r == nil) then
        return
    end
    r = self:getRecv()
    if (r == nil) then
        return
    end
    self.alive = true
    self.level = 1
end

function AI:run()
    print("AI " .. self.type .. " of the Team " .. self.team .. " run")
end

function AI:lay()
    Net:send("Fork")
    local r = self:getRecv()
    if (r == nil) then
        return
    end
    print("←", r)
end

function AI:forward()
    Net:send("Forward")
    local r = self:getRecv()
    if (r == nil) then
        return
    end
    print("←", r)
end

function AI:turnLeft()
    Net:send("Left")
    local r = self:getRecv()
    if (r == nil) then
        return
    end
    print("←", r)
end

function AI:incantation()
    local response

    self:eraseCase()
    self:setLevelObjects()
    Net.client:settimeout(nil)
    Net:send("Incantation")
    response = self:getRecv()
    if response == nil then
        return
    end
    if string.find(response, "ko") ~= nil then
        print("INCANTATION IMPOSSIBLE")
    else
        response = self:getRecv()
        if response == nil then
            return
        end
        if string.find(response, "ko") then
            print("INCANTATION has been stoped")
        else
            self.level = self.level + 1
        end
    end
end

function AI:turnRight()
    Net:send("Right")
    local r = self:getRecv()
    if (r == nil) then
        return
    end
    print("←", r)
end

function AI:takeObj(obj)
    local cmd = "Take " .. obj
    Net:send(cmd)
    local r = self:getRecv()
    if (r == nil) then
        return
    end
    print("←", r)
end

function AI:setObj(obj)
    local cmd = "Set " .. obj
    local r
    Net:send(cmd)
    r = self:getRecv()
    if (r == nil) then
        return
    end
    print("←", r)
    if r == "ok" then
        return true
    end

    return false
end

function AI:inventory()
    Net:send("Inventory")
    local inv = {}
    local inv_str = self:getRecv()
    if (inv_str == nil) then
        return nil
    end
    for _, elem in ipairs(split(inv_str, ",")) do
        local obj = split(elem, " ")
        inv[obj[1]] = obj[2]
    end
    return inv
end

function AI:look()
    Net:send("Look")

    local resultStr = self:getRecv()
    if (resultStr == nil) then
        return nil
    end
    if resultStr == "ko" then
        return nil
    end
    local r = split(resultStr, ",")
    return r
end

function AI:wander()

    self:turnLeft()
    if self.orientation < 4 then
        self.orientation = self.orientation + 1
    else
        self.orientation = 1
        self:forward()
    end
end

function AI:findFood()
    local looks = self:look()
    local nCase = 0
    local isFind = false
    local front
    local slide
    if (looks == nil) then
        return
    end
    for _, case in ipairs(looks) do
        if string.find(case, "food") then
            isFind = true
            break;
        end
        nCase = nCase + 1;
    end
    if isFind == false then
        self:wander()
        self:findFood()
    end
    front, slide = findXY(nCase)
    self:goTo(front, slide)
end

function AI:wanderEat()
    local looks = self:look()
    local nCase = 0
    local front
    local slide
    local isFind = false

    for _, case in ipairs(looks) do
        if string.find(case, "food") then
            isFind = true
            break;
        end
        nCase = nCase + 1;
    end
    if isFind == true then
        front, slide = findXY(nCase)
        self:goTo(front, slide)
        self:takeObj("food")
        return true
    end
    return false
end

function AI:eat()
    local looks = self:look()
    if (looks == nil) then
        return
    end
    local currentCase = looks[1]
    if self:isHungry() == true then
        if string.find(currentCase, "food") then
            self:takeObj("food")
            self.orientation = 1
        else
            self:findFood()
            self:eat()
        end
    end
end

function AI:followInvocation()
    if self.dirMessage == 2 then
        self:forward()
        self:turnLeft()
    elseif self.dirMessage == 3 then
        self:turnLeft()
    elseif self.dirMessage == 4 then
        self:turnLeft()
        self:forward()
        self:turnLeft()
    elseif self.dirMessage == 5 then
        self:turnLeft()
        self:turnLeft()
    elseif self.dirMessage == 6 then
        self:turnRight()
        self:forward()
        self:turnRight()
    elseif self.dirMessage == 7 then
        self:turnRight()
    elseif self.dirMessage == 8 then
        self:turnRight()
        self:forward()
    end
    if self.dirMessage ~= 0 then
        self:forward()
    end
end

function AI:goTo(front, slide)
    if front < 0 then
        self:turnLeft()
        self:turnLeft()
        front = front * -1
    end
    for i = 0, front do
        self:forward()
    end
    if slide < 0 then
        self:turnLeft()
        slide = slide * -1
    elseif slide > 0 then
        self:turnRight()
    end
    for i = 0, slide do
        self:forward()
    end
end

function AI:canUp()
    local inv
    inv = self:inventory()
    if not inv then
        return false
    end
    for k, count in pairs(levelRequirement[self.level]) do
        if k ~= nil and k ~= "player" and inv[k] and tonumber(inv[k]) < count then
            return false
        end
    end
    return true
end

function AI:needStone(stone)
    local inv = self:inventory()
    if (inv == nil) then
        return
    end
    if inv[stone] == nil then
        return false
    end
    if tonumber(inv[stone]) < totalLevelRequirement[self.level][stone] then
        return true
    else
        return false
    end
end

function AI:seeNeededStone()
    local look = self:look()
    if look == nil then
        return nil, 0, 0
    end
    for i, case in ipairs(look) do
        for _, obj in ipairs(split(case, ' ')) do
            if obj ~= "food" and obj ~= "player" and self:needStone(obj) then
                return obj, findXY(i)
            end
        end
    end
    return nil, 0, 0
end

function AI:messageRequest()
    Net.client:settimeout(0)
    local dir, id, r

    dir, id, r = Net:recvMsg()
    if r == nil then
        self.dirMessage = -1
        self.invocRequest = false
        Net.client:settimeout(nil)
        return
    end
    if string.find(r, self.requestInvocation .. " " .. self.level) ~= nil then
        self.invocRequest = true
        self.dirMessage = dir
    elseif string.find(r, self.requestWhoAreYou) ~= nil then
        Net:sendMsg(self.responseWhoAreYou)
    elseif string.find(r, self.responseWhoAreYou) ~= nil then
        if dir == 0 then
            self.countAlliesSameCase = self.countAlliesSameCase + 1
        end
    elseif self.master ~= nil then
        if r == "new" then
            self.master:giveRole()
        elseif r == "Queen" or r == "Miner" or r == "Soldier" then
            self.master.role[r] = self.master.role[r] + 1
        end
    elseif r == "role" then
        Net:sendMsg(self.type)
    end
    Net.client:settimeout(nil)
    self:messageRequest()
end

function AI:hasRequestInvocation()
    return self.invocRequest
end

function AI:canLay()
    if self:isHungry() == true then
        return false
    else
        return true
    end
end

function AI:seePlayer()
    local countLast = 0
    local tmpCount = 0
    local front = 0
    local slide = 0
    local looks = self:look()
    local arr_player
    if (looks == nil) then
        return 0, 0, 0
    end
    for i, case in ipairs(looks) do
        if i ~= 1 then
            if string.find(case, "player") then
                arr_player = split(case, " ")
                for _, stone in ipairs(arr_player) do
                    if stone == "player" then
                        tmpCount = tmpCount + 1
                    end
                end
            end
            if tmpCount > countLast then
                countLast = tmpCount
                front, slide = findXY(i)
            end
        end
    end
    return front, slide, countLast
end

function AI:enemyOnCurrentPlace()
    local currentCase
    local looks
    local countPlayer = 0
    local front,slide

    if self.countAlliesSameCase == -1 then
        Net:sendMsg(self.requestWhoAreYou)
        self.countAlliesSameCase = 0
    else
        looks = self:look()
        if looks == nil then
            return
        end
        currentCase = split(looks[1], " ")
        for _, obj in ipairs(currentCase) do
            if obj == "player" then
                countPlayer = countPlayer + 1
            end
        end
        if (countPlayer - self.countAlliesSameCase) >= 2 then

            Net:send("Eject")
        end
        self.countAlliesSameCase = -1
    end
end

function AI:makeInvocationRequest()
    local nbPlayer = 0
    local looks = self:look()
    local _lookCase
    if (looks == nil) then
        return
    end
    _lookCase = looks[1]
    for _, case in ipairs(split(_lookCase, " ")) do
        if string.find(case, "player") then
            nbPlayer = nbPlayer + 1
        end
    end
    if nbPlayer < totalLevelRequirement[self.level]["player"] then
        Net:sendMsg(self.requestInvocation .. " " .. self.level)
    else
        self:incantation()
    end
end

function AI:eraseCase()
    local currentCase = self:look()[1]

    if currentCase == nil then
        return false
    end
    for _, obj in ipairs(split(currentCase, ' ')) do
        if obj ~= nil and obj ~= "food" and obj ~= "player" then
            self:takeObj(obj)
        end
    end
    return true
end

function AI:setLevelObjects()
    local inventory = self:inventory()

    if inventory == false then
        return
    end
    for obj, count in pairs(levelRequirement[self.level]) do
        if obj ~= "player" and tonumber(count) > 0 then
            for i = 1, count do
                while self:setObj(obj) == false do end
            end
        end
    end
end

function AI:isHungry()
    local inventory = self:inventory()
    if (inventory == nil) then
        return
    end
    local currentFood = tonumber(inventory["food"])
    if currentFood == nil then
        return false
    end
    if currentFood > self.minRequiredFood then
        return false
    end
    return true
end

function AI:getRecv()
    local r = Net:recv()

    if r == nil then
        Net:send("inventory")
        r = Net:recv()
        if (r == nil) then
            self.alive = false
        end
        return nil
    end
    return r

end

return AI