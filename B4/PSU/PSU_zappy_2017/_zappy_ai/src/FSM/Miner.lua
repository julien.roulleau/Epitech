--- @module Miner

local Miner = {
    AI = nil
}

Miner.__index = Miner

function Miner:new(AI)
    local miner = {}
    setmetatable(miner, Miner)
    miner.AI = AI
    return miner
end

function Miner:seeNeededStone()
    local stone, x, y = self.AI:seeNeededStone()
    if stone then
        self.AI:goTo(x, y)
        self.AI:takeObj(stone)
        return true
    else
        return false
    end
end

function Miner:run()
    if self.AI:canUp() then
        self.AI:makeInvocationRequest()
    elseif self.AI:hasRequestInvocation() then
        self.AI:followInvocation()
    elseif self:seeNeededStone() then
    else
        return false
    end
    return true
end

return Miner
