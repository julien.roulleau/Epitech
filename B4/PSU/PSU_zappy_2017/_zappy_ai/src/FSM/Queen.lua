--- @module Queen


local Queen = {
    AI = nil
}

Queen.__index = Queen

function Queen:new(AI)
    local queen = {}
    setmetatable(queen, Queen)
    queen.AI = AI
    return queen
end

function Queen:run()
    if self.AI:canLay() then
        self.AI:lay()
    elseif self.AI:hasRequestInvocation() then
        self.AI:followInvocation()
    elseif self.AI:wanderEat() then
    else
        return false
    end
    return true
end

return Queen
