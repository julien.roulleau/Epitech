--- @module Soldier

local Soldier = {
    AI = nil
}

Soldier.__index = Soldier

function Soldier:new(AI)
    local soldier = {}
    setmetatable(soldier, Soldier)
    soldier.AI = AI
    return soldier
end

function Soldier:seePlayer()
    local x, y, nb = self.seePlayer()
    if nb >= 2 then
        self.AI:goTo(x, y)
        return true
    else
        return false
    end
end

function Soldier:run()
    if self.AI:enemyOnCurrentPlace() >= 2 then
        self.AI:eject()
    elseif self.seePlayer then
    elseif self.AI:hasRequestInvocation() then
        self.AI:followInvocation()
    else
        return false
    end
    return true
end

return Soldier
