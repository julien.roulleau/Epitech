/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** initGraph.cpp
*/

#include <tool/Pos.hpp>
#include "tool/Pos.hpp"
#include "Net/Net.hpp"
#include "Graph/Graph.hpp"

static bool init_map(Graph &graph)
{
	for (int y = 0; y < graph.size.Y; y++) {
		for (int x = 0; x < graph.size.X; x++) {
			usleep(5000);
			Net::AskContent(x, y);
			pos2i p;
			p.Y = y;
			p.X = x;
			graph.map.emplace(p, Tiles(0, 0, 0, 0, 0, 0, 0));
		}
	}
	return true;
}

bool init_graph(Graph &graph)
{
	Net::AskSize();
	auto size = Await(Net::GetSize());
	graph.size = pos2i{size.x, size.y};

	Net::AskTimeUnit();
	graph.TimeUnit = 100;

	Net::AskTeamNames();
	init_map(graph);
	Net::AskPlayerList();
	return true;
}
