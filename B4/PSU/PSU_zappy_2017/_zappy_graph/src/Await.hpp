/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** Await
*/

#ifndef AWAIT_HPP_
	#define AWAIT_HPP_

	#include <mutex>
	#include <memory>
	#include <thread>

template <class T>
class AsyncHolder {
	std::mutex mutex;
	T async;
public:
	AsyncHolder(T &as)
		: async(as)
	{}

	AsyncHolder(T &&as)
		: AsyncHolder(as)
	{}

	void Lock()
	{
		mutex.lock();
	}

	bool TryLock()
	{
		if (mutex.try_lock())
			mutex.unlock();
		else
			return (false);
		return (true);
	}

	void Unlock()
	{
		mutex.unlock();
	}

	T &Get()
	{
		return (async);
	}

	void Set(T &as)
	{
		async = as;
	}

	void Set(T &&as)
	{
		Set(as);
	}
};

template <class T>
class Async {
	AsyncHolder<T> *holder;
	int *copies;
public:
	Async()
		: holder(0), copies(new int(1))
	{}

	Async(T &as)
		: holder(new AsyncHolder<T>(as)), copies(new int(1))
	{}

	Async(T &&as)
		: Async(as)
	{}

	Async(Async<T> &as)
		: holder(as.Ident()), copies(as.Copies())
	{
		(*copies)++;
	}

	Async(Async<T> &&as)
		: Async(as)
	{}

	~Async()
	{
		(*copies)--;
		if (*copies == 0) {
			if (holder != 0)
				delete holder;
			delete copies;
		}
	}

	void Lock()
	{
		if (holder != 0)
			holder->Lock();
	}

	bool TryLock()
	{
		if (holder != 0)
			return (holder->TryLock());
		return (true);
	}

	void Unlock()
	{
		if (holder != 0)
			holder->Unlock();
	}

	T &Get()
	{
		return (holder->Get());
	}

	void Set(T &as)
	{
		if (holder != 0)
			holder->Set(as);
	}

	void Set(T &&as)
	{
		Set(as);
	}

	AsyncHolder<T> *Ident()
	{
		return (holder);
	}

	int *Copies()
	{
		return (copies);
	}
};

template <class T>
T &Await(Async<T> &task)
{
	task.Lock();
	task.Unlock();
	return (task.Get());
}

template <class T>
T Await(Async<T> &&task)
{
	return (Await<T>(task));
}

template <class T>
bool IsAwait(Async<T> &task)
{
	if (task.TryLock())
		task.Unlock();
	else
		return (false);
	return (true);
}

template <class T>
bool IsAwait(Async<T> &&task)
{
	return (IsAwait<T>(task));
}

template <class T>
T &AwaitContent(Async<T> &task)
{
	return (task.Get());
}

template <class T>
T AwaitContent(Async<T> &&task)
{
	return (AwaitContent<T>(task));
}

#endif /* !AWAIT_HPP_ */
