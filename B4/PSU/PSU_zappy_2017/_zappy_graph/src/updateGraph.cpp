/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** updateGraph.cpp
*/


#include <iostream>
#include <bitset>
#include <tool/Pos.hpp>

#include "Net/Net.hpp"
#include "Graph/Graph.hpp"

static void react_MSZ(Graph &graph)
{
	while (NET_MSZ(Net::Select())) {
		auto pt = Net::GetSize_Giver();
		graph.size.X = pt.x;
		graph.size.Y = pt.y;
	}
}

static void react_BCT(Graph &graph)
{
	while (NET_BCT(Net::Select())) {
		usleep(100);
		auto tile = Net::GetContent_Giver();
		pos2i p{tile.first.x, tile.first.y};
		if (graph.map.find(p) != graph.map.end()) {
			auto it = graph.map.find(p);
			it->second.ressources[0] = tile.second.food;
			it->second.ressources[1] = tile.second.linemate;
			it->second.ressources[2] = tile.second.deraumere;
			it->second.ressources[3] = tile.second.sibur;
			it->second.ressources[4] = tile.second.mendiane;
			it->second.ressources[5] = tile.second.phiras;
			it->second.ressources[6] = tile.second.thystame;
		} else {
			graph.map.emplace(p, Tiles(
				tile.second.food,
				tile.second.linemate,
				tile.second.deraumere,
				tile.second.sibur,
				tile.second.mendiane,
				tile.second.phiras,
				tile.second.thystame
				));
		}
	}
}

static void react_PNW(Graph &graph)
{
	while (NET_PNW(Net::Select())) {
		auto pl = Net::GetPlayerConnection_Giver();
		if (graph.playerList.find(std::get<0>(pl)) ==
		    graph.playerList.end()) {
			Net::AskPlayerInv(std::get<0>(pl));
			pos2i p{std::get<1>(pl).x, std::get<1>(pl).y};
			if (p == graph.posTile)
				graph.playersOnTile++;
			graph.playerList.emplace(std::get<0>(pl), Player(p,
							std::get<1>(pl).o,
							std::get<2>(pl),
							std::get<3>(pl)));
			if (graph.teams.find(std::get<3>(pl)) ==
			    graph.teams.end())
				graph.teams.emplace(std::get<3>(pl), 0);
			(graph.teams.find(std::get<3>(pl))->second)++;
		}
	}
}

static void react_PPO(Graph &graph)
{
	while (NET_PPO(Net::Select())) {
		auto pt = Net::GetPlayerPos_Giver();
		if (graph.playerList.find(pt.first) != graph.playerList.end()) {
			auto it = graph.playerList.find(pt.first);
			if (it->second.position == graph.posTile)
				graph.playersOnTile--;
			it->second.orientation = pt.second.o;
			it->second.position.X = pt.second.x;
			it->second.position.Y = pt.second.y;
			if (it->second.position == graph.posTile)
				graph.playersOnTile++;
		}
	}
}

static void react_PLV(Graph &graph)
{
	while (NET_PLV(Net::Select())) {
		auto pt = Net::GetPlayerLvl_Giver();
		if (graph.playerList.find(pt.first) != graph.playerList.end())
			graph.playerList.find(pt.first)->second.level
				= pt.second;
	}
}

static void react_PLT(Graph &graph)
{
	while (NET_PLT(Net::Select())) {
		auto pt = Net::GetPlayerTeam_Giver();
		if (graph.playerList.find(pt.first) != graph.playerList.end())
			graph.playerList.find(pt.first)->second.teamName
				= pt.second;
		if (graph.teams.find(pt.second) == graph.teams.end())
			graph.teams.emplace(pt.second, 0);
		(graph.teams.find(pt.second)->second)++;
	}
}

static void react_PLS(Graph &graph)
{
	while (NET_PLS(Net::Select())) {
		auto pl = Net::GetPlayerList_Giver();
		if (graph.playerList.find(pl) == graph.playerList.end()) {
			Net::AskPlayerInv(pl);
			Net::AskPlayerPos(pl);
			Net::AskPlayerTeam(pl);
			Net::AskPlayerLvl(pl);
			pos2i p{0, 0};
			std::string n("");
			graph.playerList.emplace(pl, Player(p, 0, 0, n));
		}
	}
}

static void react_PIN(Graph &graph)
{
	while (NET_PIN(Net::Select())) {
		auto p = Net::GetPlayerInv_Giver();
		if (graph.playerList.find(std::get<0>(p))
		    != graph.playerList.end()) {
			auto it = graph.playerList.find(std::get<0>(p));
			auto &r = std::get<2>(p);
			it->second.ressources[0] = r.food;
			it->second.ressources[1] = r.linemate;
			it->second.ressources[2] = r.deraumere;
			it->second.ressources[3] = r.sibur;
			it->second.ressources[4] = r.mendiane;
			it->second.ressources[5] = r.phiras;
			it->second.ressources[6] = r.thystame;
		}
	}
}

static void react_PEX(Graph &graph)
{
	while (NET_PIN(Net::Select()))
		Net::GetExpulsion_Giver();
	(void)graph;
}

static void react_PBC(Graph &graph)
{
	while (NET_PBC(Net::Select())) {
		auto pt = Net::GetBroadcast_Giver();
		std::string msg = std::to_string(pt.first)
				+ std::string(" : ") + pt.second;
		graph.broadcast.emplace_back(msg);
		if (graph.broadcast.size() > 15) {
			graph.broadcast.erase(graph.broadcast.begin());
		}
	}
}

static void react_PIC(Graph &graph)
{
	while (NET_PIC(Net::Select()))
		Net::GetIncantationStart_Giver();
	(void)graph;
}

static void react_PIE(Graph &graph)
{
	while (NET_PIE(Net::Select()))
		Net::GetIncantationEnd_Giver();
	(void)graph;
}

static void react_PFK(Graph &graph)
{
	(void)graph;
}

static void react_PDR(Graph &graph)
{
	while (NET_PDR(Net::Select())) {
		auto inf = Net::GetResourceDrop_Giver();
		auto it = graph.playerList.find(inf.first);
		if (it == graph.playerList.end())
			continue;
		Net::AskContent(it->second.position.X, it->second.position.X);
		Net::AskPlayerInv(inf.first);
	}
	(void)graph;
}

static void react_PGT(Graph &graph)
{
	while (NET_PGT(Net::Select())) {
		auto inf =Net::GetResourceCollect_Giver();
		auto it = graph.playerList.find(inf.first);
		if (it == graph.playerList.end())
			continue;
		Net::AskContent(it->second.position.X, it->second.position.X);
		Net::AskPlayerInv(inf.first);
	}
	(void)graph;
}

static void react_PDI(Graph &graph)
{
	while (NET_PDI(Net::Select())) {
		auto n = Net::GetPlayerDeath_Giver();
		if (graph.playerList.find(n) == graph.playerList.end())
			continue;
		auto it = graph.playerList.find(n);
		if (graph.teams.find(it->second.teamName) != graph.teams.end())
			(graph.teams.find(it->second.teamName)->second)--;
		if (it->second.position == graph.posTile)
			graph.playersOnTile--;
		graph.playerList.erase(graph.playerList.find(n));
	}
	(void)graph;

}

static void react_ENW(Graph &graph)
{
	while (NET_ENW(Net::Select())) {
		auto egg = Net::GetEggLaid_Giver();
		if (graph.eggList.find(std::get<0>(egg)) != graph.eggList.end())
			return;
		pos2i p{std::get<2>(egg).x, std::get<2>(egg).y};
		if (p == graph.posTile)
			graph.EggOnTile++;
		graph.eggList.emplace(std::get<0>(egg), Egg(p));
	}
	(void)graph;
}

static void react_EHT(Graph &graph)
{
	while (NET_EHT(Net::Select())) {
		Net::GetEggHatch_Giver();
	}
	(void)graph;
}

static void react_EBO(Graph &graph)
{
	while (NET_EBO(Net::Select())) {
		auto e = Net::GetEggConnect_Giver();
		if (graph.eggList.find(e) != graph.eggList.end()) {
			if (graph.eggList.find(e)->second.position
			    == graph.posTile)
				graph.EggOnTile--;
			graph.eggList.erase(graph.eggList.find(e));
		}
	}
}

static void react_EDI(Graph &graph)
{
	while (NET_EDI(Net::Select())) {
		auto e = Net::GetEggDeath_Giver();
		if (graph.eggList.find(e) != graph.eggList.end()) {
			if (graph.eggList.find(e)->second.position
			    == graph.posTile)
				graph.EggOnTile--;
			graph.eggList.erase(graph.eggList.find(e));
		}
	}
	(void)graph;
}

static void react_SGT(Graph &graph)
{
	while (NET_SGT(Net::Select())) {
		graph.TimeUnit = Net::GetTimeUnit_Giver();
	}
	(void)graph;
}

static void react_SST(Graph &graph)
{
	while (NET_SST(Net::Select())) {
		graph.TimeUnit = Net::SetTimeUnit_Giver();
	}
	(void)graph;
}

static void react_SMG(Graph &graph)
{
	while (NET_SMG(Net::Select())) {
		auto msg = Net::GetServerMessage_Giver();

	}
	(void)graph;
}

static void react_TNA(Graph &graph)
{
	while (NET_TNA(Net::Select())) {
		auto name = Net::GetTeamNames_Giver();
		if (graph.teams.find(name) != graph.teams.end())
			return;
		graph.teams.emplace(name, 0);
	}
	(void)graph;
}

bool update_graph(Graph &graph, int select, bool update)
{
	if (NET_BCT(select))
		react_BCT(graph);
	if (NET_PLS(select))
		react_PLS(graph);
	if (NET_MSZ(select))
		react_MSZ(graph);
	if (NET_PNW(select))
		react_PNW(graph);
	if (NET_PPO(select))
		react_PPO(graph);
	if (NET_PLV(select))
		react_PLV(graph);
	if (NET_PIN(select))
		react_PIN(graph);
	if (NET_PEX(select))
		react_PEX(graph);
	if (NET_PBC(select))
		react_PBC(graph);
	if (NET_PIC(select))
		react_PIC(graph);
	if (NET_PIE(select))
		react_PIE(graph);
	if (NET_PFK(select))
		react_PFK(graph);
	if (NET_PDR(select))
		react_PDR(graph);
	if (NET_PGT(select))
		react_PGT(graph);
	if (NET_PDI(select))
		react_PDI(graph);
	if (NET_ENW(select))
		react_ENW(graph);
	if (NET_EHT(select))
		react_EHT(graph);
	if (NET_EBO(select))
		react_EBO(graph);
	if (NET_EDI(select))
		react_EDI(graph);
	if (NET_SGT(select))
		react_SGT(graph);
	if (NET_SST(select))
		react_SST(graph);
	if (NET_SMG(select))
		react_SMG(graph);
	if (NET_PLT(select))
		react_PLT(graph);
	if (NET_TNA(select))
		react_TNA(graph);
	// For each Tiles On Each Players, ask Tile Informations
	if (update) {
		for (int y = 0; y < graph.size.Y; y++)
			for (int x = 0; x < graph.size.X; x++) {
				usleep(5000);
				Net::AskContent(x, y);
		}
		if (graph.IdPlayer != -1 &&
		    graph.playerList.find(graph.IdPlayer)
		    != graph.playerList.end()) {
			Net::AskPlayerInv(graph.IdPlayer);
			Net::AskPlayerLvl(graph.IdPlayer);
		}
	}
	return true;
}
