/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** main
*/

#include <iostream>
#include <bitset>
#include <tool/Pos.hpp>
#include <ctime>
#include <signal.h>

#include "Net/Net.hpp"
#include "Graph/Graph.hpp"
#include "Window/window.hpp"

bool stop = false;

static bool connect_to_serv(const char *addr, unsigned short port)
{
	auto b = Await(Net::Connect(addr, port));
	if (b) {
		usleep(10000);
		Net::SendGraphIdentification("GRAPH\n");
		usleep(40000);
	}
	return b;
}

static bool is_connected(int select)
{
	return !NET_SEG(select);
}

void sigpipe_handler(int unused)
{
	(void)unused;
	stop = true;
}


int main(int ac, char **av)
{
	if (ac != 3) {
		std::cout << "USAGE:" << std::endl
			  << "\t./zappy_graph ADDR PORT" << std::endl;
		return 1;
	}
	signal(SIGPIPE, sigpipe_handler);
	if (!connect_to_serv(av[1], atoi(av[2]))) {
		std::cout << "Couldn't connect to the server." << std::endl;
		return 1;
	}
	Graph graph;
	init_graph(graph);
	windowGraph window(&graph);
	int select = Net::Select();
	std::time_t t = std::time(nullptr);
	bool refresh = false;
	while (window.isOpen() && is_connected(select) && !stop) {
		if (refresh) {
			update_graph(graph, select, true);
			refresh = false;
		} else
			update_graph(graph, select, false);
		window.update();
		select = Net::Select();
		if (t + 3 < std::time(nullptr)) {
			t = std::time(nullptr);
			refresh = true;
		}
		window.Event();
	}
	return 0;
}