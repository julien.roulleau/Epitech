/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** Pos.cpp
*/

#include "Pos.hpp"

bool operator==(pos2i a, pos2i b)
{
	return (a.X == b.X && a.Y == b.Y);
}

bool operator<(pos2i a, pos2i b)
{
	if (a.Y < b .Y)
		return true;
	else if (a.Y == b.Y && a.X < b.X)
		return true;
	return false;
}