/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** Pos.hpp
*/

#ifndef ZAPPY_POS_HPP
#define ZAPPY_POS_HPP

typedef struct {
	int X;
	int Y;
} pos2i;

bool operator==(pos2i a, pos2i b);
bool operator<(pos2i a, pos2i b);
#endif //ZAPPY_POS_HPP
