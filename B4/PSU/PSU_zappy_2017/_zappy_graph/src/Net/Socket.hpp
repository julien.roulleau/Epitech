/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** Socket
*/

#ifndef SOCKET_HPP_
	#define SOCKET_HPP_

	#include <sys/socket.h>
	#include <netinet/in.h>
	#include <netdb.h>
	#include <unistd.h>
	#include <string>
	#include <stdexcept>

class Socket {
	int sock;

	int _Socket(int, int, int);
	int _Connect(int, const struct sockaddr *, socklen_t);
	struct hostent *_GetHostByName(std::string);
	unsigned short _HToNS(unsigned short);
public:
	Socket();
	Socket(std::string, unsigned short);
	~Socket();

	void Open(std::string host, unsigned short port);
	size_t Send(std::string s);
	std::string Recv();
};

#endif /* !SOCKET_HPP_ */
