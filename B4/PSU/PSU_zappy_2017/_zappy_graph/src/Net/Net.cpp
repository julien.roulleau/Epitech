/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** Net
*/

#include "Net.hpp"

bool Net::isConnected = false;
bool Net::instantiated = false;

std::unordered_map<std::string, std::queue<std::string>> Net::queues;
std::queue<std::string> Net::sendQueue;
std::mutex Net::sendQueueMutex;

Socket Net::sock;

bool operator==(Pos a, Pos b)
{
	return (a.x == b.x && a.y == b.y);
}

Async<bool> Net::Connect(std::string host, unsigned short port)
{
	auto as = Async<bool>(isConnected);
	if (instantiated)
		return (as);
	instantiated = true;
	as.Lock();
	std::thread(Net::Worker, host, port, std::ref(as)).detach();
	as.Lock();
	return (as);
}

int Net::_Select_2(int sum)
{
	if (!queues["EBO"].empty())
		sum |= NET_EBO(-1);
	if (!queues["EDI"].empty())
		sum |= NET_EDI(-1);
	if (!queues["SGT"].empty())
		sum |= NET_SGT(-1);
	if (!queues["SST"].empty())
		sum |= NET_SST(-1);
	if (!queues["SEG"].empty())
		sum |= NET_SEG(-1);
	if (!queues["SMG"].empty())
		sum |= NET_SMG(-1);
	if (!queues["PLS"].empty())
		sum |= NET_PLS(-1);
	if (!queues["PLT"].empty())
		sum |= NET_PLT(-1);
	return (sum);
}

int Net::_Select_1(int sum)
{
	if (!queues["PBC"].empty())
		sum |= NET_PBC(-1);
	if (!queues["PIC"].empty())
		sum |= NET_PIC(-1);
	if (!queues["PIE"].empty())
		sum |= NET_PIE(-1);
	if (!queues["PFK"].empty())
		sum |= NET_PFK(-1);
	if (!queues["PDR"].empty())
		sum |= NET_PDR(-1);
	if (!queues["PGT"].empty())
		sum |= NET_PGT(-1);
	if (!queues["PDI"].empty())
		sum |= NET_PDI(-1);
	if (!queues["ENW"].empty())
		sum |= NET_ENW(-1);
	if (!queues["EHT"].empty())
		sum |= NET_EHT(-1);
	return (_Select_2(sum));
}

int Net::Select()
{
	int sum = 0;
	if (!queues["MSZ"].empty())
		sum |= NET_MSZ(-1);
	if (!queues["BCT"].empty())
		sum |= NET_BCT(-1);
	if (!queues["MCT"].empty())
		sum |= NET_MCT(-1);
	if (!queues["TNA"].empty())
		sum |= NET_TNA(-1);
	if (!queues["PNW"].empty())
		sum |= NET_PNW(-1);
	if (!queues["PPO"].empty())
		sum |= NET_PPO(-1);
	if (!queues["PLV"].empty())
		sum |= NET_PLV(-1);
	if (!queues["PIN"].empty())
		sum |= NET_PIN(-1);
	if (!queues["PEX"].empty())
		sum |= NET_PEX(-1);
	return (_Select_1(sum));
}

Async<Pos> Net::GetSize()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<Pos>(Pos{0, 0});
	as.Lock();
	std::thread(Net::GetSize_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<std::pair<Pos, Stones>> Net::GetContent()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<std::pair<Pos, Stones>>
		(std::make_pair((Pos){0, 0}, (Stones){0, 0, 0, 0, 0, 0, 0}));
	as.Lock();
	std::thread(Net::GetContent_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<std::unordered_map<Pos, Stones>> Net::GetContents()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<std::unordered_map<Pos, Stones>>(
		std::unordered_map<Pos, Stones>());
	as.Lock();
	std::thread(Net::GetContents_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<std::string> Net::GetTeamNames()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<std::string>("");
	as.Lock();
	std::thread(Net::GetTeamNames_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<std::tuple<int, Orient, int, std::string>> Net::GetPlayerConnection()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<std::tuple<int, Orient, int, std::string>>(
		std::make_tuple<int, Orient, int, std::string>
		        (0, {0, 0, 0}, 0, ""));
	as.Lock();
	std::thread(Net::GetPlayerConnection_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
	
}

Async<std::pair<int, Orient>> Net::GetPlayerPos()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<std::pair<int, Orient>>(
		std::make_pair<int, Orient>(0, {0, 0, 0}));
	as.Lock();
	std::thread(Net::GetPlayerPos_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<std::pair<int, int>> Net::GetPlayerLvl()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<std::pair<int, int>>(std::make_pair<int, int>(0, 0));
	as.Lock();
	std::thread(Net::GetPlayerLvl_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<std::tuple<int, Pos, Stones>> Net::GetPlayerInv()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<std::tuple<int, Pos, Stones>>(
		std::make_tuple<int, Pos, Stones>(0, {0, 0}, {0, 0, 0, 0, 0, 0,
		0}));
	as.Lock();
	std::thread(Net::GetPlayerInv_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<int> Net::GetExpulsion()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<int>(0);
	as.Lock();
	std::thread(Net::GetExpulsion_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<std::pair<int, std::string>> Net::GetBroadcast()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<std::pair<int, std::string>>(
		std::make_pair<int, std::string>(0, ""));
	as.Lock();
	std::thread(Net::GetBroadcast_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<std::tuple<Pos, int, std::list<int>>> Net::GetIncantationStart()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<std::tuple<Pos, int, std::list<int>>>(
		std::make_tuple<Pos, int, std::list<int>>({0, 0}, 0,
		std::list<int>()));
	as.Lock();
	std::thread(Net::GetIncantationStart_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<std::pair<Pos, bool>> Net::GetIncantationEnd()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<std::pair<Pos, bool>>(
		std::make_pair<Pos, bool>({0, 0}, false));
	as.Lock();
	std::thread(Net::GetIncantationEnd_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<int> Net::GetPlayerFork()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<int>(0);
	as.Lock();
	std::thread(Net::GetPlayerFork_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<std::pair<int, int>> Net::GetResourceDrop()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<std::pair<int, int>>(
		std::make_pair<int, int>(0, STONE_FOOD));
	as.Lock();
	std::thread(Net::GetResourceDrop_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<std::pair<int, int>> Net::GetResourceCollect()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<std::pair<int, int>>(
		std::make_pair<int, int>(0, STONE_FOOD));
	as.Lock();
	std::thread(Net::GetResourceCollect_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<int> Net::GetPlayerDeath()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<int>(0);
	as.Lock();
	std::thread(Net::GetPlayerDeath_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<std::tuple<int, int, Pos>> Net::GetEggLaid()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<std::tuple<int, int, Pos>>(
		std::make_tuple<int, int, Pos>(0, 0, {0, 0}));
	as.Lock();
	std::thread(Net::GetEggLaid_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<int> Net::GetEggHatch()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<int>(0);
	as.Lock();
	std::thread(Net::GetEggHatch_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<int> Net::GetEggConnect()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<int>(0);
	as.Lock();
	std::thread(Net::GetEggConnect_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<int> Net::GetEggDeath()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<int>(0);
	as.Lock();
	std::thread(Net::GetEggDeath_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<int> Net::GetTimeUnit()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<int>(0);
	as.Lock();
	std::thread(Net::GetTimeUnit_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<int> Net::SetTimeUnit()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<int>(0);
	as.Lock();
	std::thread(Net::SetTimeUnit_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<bool> Net::GetEndGame()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<bool>(false);
	as.Lock();
	std::thread(Net::GetEndGame_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<std::string> Net::GetServerMessage()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<std::string>("");
	as.Lock();
	std::thread(Net::GetServerMessage_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

Async<int> Net::GetPlayerList() {
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<int>(0);
	as.Lock();
	std::thread(Net::GetPlayerList_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}


Async<std::pair<int, std::string>> Net::GetPlayerTeam()
{
	if (!isConnected)
		throw std::runtime_error("Not connected");
	auto as = Async<std::pair<int, std::string>>();
	as.Lock();
	std::thread(Net::GetPlayerTeam_Worker, std::ref(as)).detach();
	as.Lock();
	return (as);
}

void Net::Read_Parse(std::string s)
{
	istring t(s.substr(0, 3).c_str());
	if (s.length() < 3 || (t != "msz" && t != "bct" && t != "tna" && t !=
		"pnw" && t != "ppo" && t != "plv" && t != "pin" && t != "pex" &&
		t != "pbc" && t != "pic" && t != "pie" && t != "pfk" && t !=
		"pdr" && t != "pgt" && t != "pdi" && t != "enw" && t != "eht" &&
		t != "ebo" && t != "edi" && t != "sgt" && t != "sst" && t !=
		"seg" && t != "smg" && t != "pls" && t != "plt"))
		return;
	char u[4] = {(char)toupper(s[0]), (char)toupper(s[1]),
		(char)toupper(s[2]), 0};
	queues[std::string(u)].push(s);
}

void Net::Read_Worker()
{
	std::stringstream ss;
	while (isConnected) {
		auto s = sock.Recv();
		size_t p = s.find('\n');
		while (p != std::string::npos) {
			ss << s.substr(0, p);
			Read_Parse(ss.str().c_str());
			ss.str(std::string());
			s = s.length() > p ? s.substr(p + 1) : "";
			p = s.find('\n');
		}
		ss << s;
	}
}

void Net::Write_Worker()
{
	while (isConnected) {
		while (sendQueue.empty())
			usleep(1);
		auto s = Popper(sendQueue);
		size_t l = s.length();
		while (l > 0)
			l -= sock.Send(s);
	}
}

void Net::Worker(std::string host, unsigned short p, Async<bool> &as)
{
	auto m = as;
	m.Unlock();
	try {
		sock.Open(host, p);
	} catch (std::runtime_error e) {
		isConnected = false;
		instantiated = false;
		m.Set(isConnected);
		std::cerr << "Exception raised while connecting: " << e.what()
			<< std::endl;
		m.Unlock();
		return;
	}
	isConnected = true;
	m.Set(isConnected);
	std::cout << "Connected to: " << host << ", port: " << p << std::endl;
	m.Unlock();
	std::thread(Net::Read_Worker).detach();
	std::thread(Net::Write_Worker).detach();
}

void Net::Send(std::string s)
{
	sendQueueMutex.lock();
	sendQueue.push(s);
	sendQueueMutex.unlock();
}

void Net::GetSize_Worker(Async<Pos> &as)
{
	auto m = as;
	m.Unlock();
	do {
		while (queues["MSZ"].empty()) usleep(1);
		try {
			m.Set(Net::GetSize_Giver());
			m.Unlock();
			return;
		} catch (std::exception &e) {}
	} while (true);
}

void Net::GetContent_Worker(Async<std::pair<Pos, Stones>> &as)
{
	auto m = as;
	m.Unlock();
	do {
		while (queues["BCT"].empty()) usleep(1);
		try {
			m.Set(Net::GetContent_Giver());
			m.Unlock();
			return;
		} catch (std::exception) {}
	} while (true);
}

void Net::GetContents_Worker(Async<std::unordered_map<Pos, Stones>> &as)
{
	auto m = as;
	m.Unlock();
	std::this_thread::sleep_for(std::chrono::milliseconds(10));
	m.Unlock();
}

void Net::GetTeamNames_Worker(Async<std::string> &as)
{
	auto m = as;
	m.Unlock();
	do {
		while (queues["TNA"].empty()) usleep(1);
		try {
			m.Set(Net::GetTeamNames_Giver());
			m.Unlock();
			return;
		} catch (std::exception) {}
	} while (true);
}

void Net::GetPlayerConnection_Worker(Async<std::tuple<int, Orient, int,
	std::string>> &as)
{
	auto m = as;
	m.Unlock();
	do {
		while (queues["PNW"].empty()) usleep(1);
		try {
			m.Set(GetPlayerConnection_Giver());
			m.Unlock();
			return;
		} catch (std::exception) {}
	} while (true);
}

void Net::GetPlayerPos_Worker(Async<std::pair<int, Orient>> &as)
{
	auto m = as;
	m.Unlock();
	do {
		while (queues["PPO"].empty()) usleep(1);
		try {
			m.Set(GetPlayerPos_Giver());
			m.Unlock();
			return;
		} catch (std::exception) {}
	} while (true);
}

void Net::GetPlayerLvl_Worker(Async<std::pair<int, int>> &as)
{
	auto m = as;
	m.Unlock();
	do {
		while (queues["PLV"].empty()) usleep(1);
		try {
			m.Set(GetPlayerLvl_Giver());
			m.Unlock();
			return;
		} catch (std::exception) {}
	} while (true);
}

void Net::GetPlayerInv_Worker(Async<std::tuple<int, Pos, Stones>> &as)
{
	auto m = as;
	m.Unlock();
	do {
		while (queues["PIN"].empty()) usleep(1);
		try {
			m.Set(GetPlayerInv_Giver());
			m.Unlock();
			return;
		} catch (std::exception) {}
	} while (true);
}

void Net::GetExpulsion_Worker(Async<int> &as)
{
	auto m = as;
	m.Unlock();
	do {
		while (queues["PEX"].empty()) usleep(1);
		try {
			m.Set(GetExpulsion_Giver());
			m.Unlock();
			return;
		} catch (std::exception) {}
	} while (true);
}

void Net::GetBroadcast_Worker(Async<std::pair<int, std::string>> &as)
{
	auto m = as;
	m.Unlock();
	do {
		while (queues["PBC"].empty()) usleep(1);
		try {
			m.Set(GetBroadcast_Giver());
			m.Unlock();
			return;
		} catch (std::exception) {}
	} while (true);
}

void Net::GetIncantationStart_Worker(Async<std::tuple<Pos, int, std::list<int>>> &as)
{
	auto m = as;
	m.Unlock();
	do {
		while (queues["PIC"].empty()) usleep(1);
		try {
			m.Set(GetIncantationStart_Giver());
			m.Unlock();
			return;
		} catch (std::exception) {}
	} while (true);
}

void Net::GetIncantationEnd_Worker(Async<std::pair<Pos, bool>> &as)
{
	auto m = as;
	m.Unlock();
	do {
		while (queues["PIE"].empty()) usleep(1);
		try {
			m.Set(GetIncantationEnd_Giver());
			m.Unlock();
			return;
		} catch (std::exception) {}
	} while (true);
}

void Net::GetPlayerFork_Worker(Async<int> &as)
{
	auto m = as;
	m.Unlock();
	bool rep = true;
	do {
		while (queues["PFK"].empty()) usleep(1);
		try {
			m.Set(GetPlayerFork_Giver());
			m.Unlock();
			rep = false;
		} catch (std::exception) {}
	} while (rep);
}

void Net::GetResourceDrop_Worker(Async<std::pair<int, int>> &as)
{
	auto m = as;
	m.Unlock();
	do {
		while (queues["PDR"].empty()) usleep(1);
		try {
			m.Set(GetResourceDrop_Giver());
			m.Unlock();
			return;
		} catch (std::exception) {}
	} while (true);
}

void Net::GetResourceCollect_Worker(Async<std::pair<int, int>> &as)
{
	auto m = as;
	m.Unlock();
	do {
		while (queues["PGT"].empty()) usleep(1);
		try {
			m.Set(GetResourceCollect_Giver());
			m.Unlock();
			return;
		} catch (std::exception) {}
	} while (true);
}

void Net::GetPlayerDeath_Worker(Async<int> &as)
{
	auto m = as;
	m.Unlock();
	bool rep = true;
	do {
		while (queues["PDI"].empty()) usleep(1);
		try {
			m.Set(GetPlayerDeath_Giver());
			m.Unlock();
			rep = false;
		} catch (std::exception) {}
	} while (rep);
}

void Net::GetEggLaid_Worker(Async<std::tuple<int, int, Pos>> &as)
{
	auto m = as;
	m.Unlock();
	do {
		while (queues["ENW"].empty()) usleep(1);
		try {
			m.Set(GetEggLaid_Giver());
			m.Unlock();
			return;
		} catch (std::exception) {}
	} while (true);
}

void Net::GetEggHatch_Worker(Async<int> &as)
{
	auto m = as;
	m.Unlock();
	bool rep = true;
	do {
		while (queues["EHT"].empty()) usleep(1);
		try {
			m.Set(GetEggHatch_Giver());
			m.Unlock();
			rep = false;
		} catch (std::exception) {}
	} while (rep);
}

void Net::GetEggConnect_Worker(Async<int> &as)
{
	auto m = as;
	m.Unlock();
	bool rep = true;
	do {
		while (queues["EBO"].empty()) usleep(1);
		try {
			m.Set(GetEggConnect_Giver());
			m.Unlock();
			rep = false;
		} catch (std::exception) {}
	} while (rep);
}

void Net::GetEggDeath_Worker(Async<int> &as)
{
	auto m = as;
	m.Unlock();
	bool rep = true;
	do {
		while (queues["EDI"].empty()) usleep(1);
		try {
			m.Set(GetEggDeath_Giver());
			m.Unlock();
			rep = false;
		} catch (std::exception) {}
	} while (rep);
}

void Net::GetTimeUnit_Worker(Async<int> &as)
{
	auto m = as;
	m.Unlock();
	bool rep = true;
	do {
		while (queues["SGT"].empty()) usleep(1);
		try {
			m.Set(GetTimeUnit_Giver());
			m.Unlock();
			rep = false;
		} catch (std::exception) {}
	} while (rep);
}

void Net::SetTimeUnit_Worker(Async<int> &as)
{
	auto m = as;
	m.Unlock();
	bool rep = true;
	do {
		while (queues["SST"].empty()) usleep(1);
		try {
			m.Set(SetTimeUnit_Giver());
			m.Unlock();
			rep = false;
		} catch (std::exception) {}
	} while (rep);
}

void Net::GetEndGame_Worker(Async<bool> &as)
{
	auto m = as;
	m.Unlock();
	while (queues["SEG"].empty()) usleep(1);
	std::string s = Popper(queues["SEG"]);
	m.Set(true);
	m.Unlock();
}

void Net::GetServerMessage_Worker(Async<std::string> &as)
{
	auto m = as;
	m.Unlock();
	bool rep = true;
	do {
		while (queues["SMG"].empty()) usleep(1);
		try {
			m.Set(GetServerMessage_Giver());
			m.Unlock();
			rep = false;
		} catch (std::exception) {}
	} while (rep);
}

void Net::GetPlayerList_Worker(Async<int> &as)
{
	auto m = as;
	m.Unlock();
	do {
		while (queues["PLS"].empty()) usleep(1);
		try {
			m.Set(GetPlayerList_Giver());
			m.Unlock();
			return;
		} catch (std::exception) {}
	} while (true);
}


void Net::GetPlayerTeam_Worker(Async<std::pair<int, std::string>> &as)
{
	auto m = as;
	m.Unlock();
	do {
		while (queues["PLT"].empty()) usleep(1);
		try {
			m.Set(GetPlayerTeam_Giver());
			m.Unlock();
			return;
		} catch (std::exception) {}
	} while (true);
}

void Net::AskSize()
{
	Send("msz\n");
}

void Net::AskContent(int x, int y)
{
	std::stringstream ss;
	ss << "bct " << x << " " << y << "\n";
	Send(ss.str());
}

void Net::AskTeamNames()
{
	Send("tna\n");
}

void Net::AskPlayerPos(int n)
{
	std::stringstream ss;
	ss << "ppo " << n << "\n";
	Send(ss.str());
}

void Net::AskPlayerLvl(int n)
{
	std::stringstream ss;
	ss << "plv " << n << "\n";
	Send(ss.str());
}

void Net::AskPlayerInv(int n)
{
	std::stringstream ss;
	ss << "pin " << n << "\n";
	Send(ss.str());
}

void Net::AskPlayerTeam(int n)
{
	std::stringstream ss;
	ss << "plt " << n << "\n";
	Send(ss.str());
}

void Net::AskTimeUnit()
{
	Send("sgt\n");
}

void Net::AskTimeUnitSet(int t)
{
	std::stringstream ss;
	ss << "plt " << t << "\n";
	Send(ss.str());
}

void Net::AskPlayerList()
{
	Send("pls\n");
}

Pos Net::GetSize_Giver()
{
	if (queues["MSZ"].empty())
		throw std::exception();
	std::string s = Popper(queues["MSZ"]);
	size_t p = 4;
	size_t p2 = 0;
	auto x = stoi(s.substr(p), &p2);
	p += p2;
	auto y = stoi(s.substr(p), &p2);
	return (Pos){x, y};
}

int Net::GetPlayerList_Giver()
{

	if (queues["PLS"].empty())
		throw std::exception();
	std::string s = Popper(queues["PLS"]);
	if (s.length() < 5)
		throw std::exception();
	size_t p = 4;
	return stoi(s.substr(p), 0);
}

std::pair<int, Orient> Net::GetPlayerPos_Giver()
{
	if (queues["PPO"].empty())
		throw std::exception();
	std::string s = Popper(queues["PPO"]);
	size_t p = 4;
	size_t p2 = 0;
	auto n2 = stoi(s.substr(p), &p2);
	p += p2;
	auto x = stoi(s.substr(p), &(p2));
	p += p2;
	auto y = stoi(s.substr(p), &(p2));
	p += p2;
	auto o = stoi(s.substr(p), &(p2));
	return std::make_pair<int, Orient>((int) n2, (Orient) {x, y, o});
}

std::pair<int, std::string> Net::GetPlayerTeam_Giver()
{
	if (queues["PLT"].empty())
		throw std::exception();
	std::string s = Popper(queues["PLT"]);
	if (s.length() < 4)
	throw std::exception();
	size_t p = 4;
	size_t p2 = 0;
	int n = stoi(s.substr(p), &p2);
	p += p2;
	return std::make_pair<int, std::string>((int)n, s.substr(p));
}

int Net::GetExpulsion_Giver()
{
	if (queues["PEX"].empty())
		throw std::exception();
	std::string s = Popper(queues["PEX"]);
	return stoi(s.substr(4), 0);
}

std::pair<int, std::string> Net::GetBroadcast_Giver()
{
	if (queues["PBC"].empty())
		throw std::exception();
	std::string s = Popper(queues["PBC"]);
	size_t p = 4;
	size_t p2 = 0;
	auto n = stoi(s.substr(p), &p2);
	auto e = s.substr(p + 1);
	return std::make_pair((int)n, (std::string)e);
}

std::tuple<Pos, int, std::list<int>> Net::GetIncantationStart_Giver()
{
	if (queues["PIC"].empty())
		throw std::exception();
	std::string s = Popper(queues["PIC"]);
	size_t p = 4;
	size_t p2 = 0;
	Pos pos;
	std::list<int> pl;
	int level;
	pos.x = stoi(s.substr(p), &p2);
	p += p2;
	pos.y = stoi(s.substr(p), &p2);
	p += p2;
	level = stoi(s.substr(p), &p2);
	p += p2;
	while (s.length() > p) {
		pl.emplace_back(stoi(s.substr(p), &p2));
		p += p2;
	}
	return std::make_tuple(pos, level, pl);
}

std::pair<Pos, bool> Net::GetIncantationEnd_Giver()
{
	if (queues["PIE"].empty())
		throw std::exception();
	std::string s = Popper(queues["PIE"]);
	size_t p = 4;
	size_t p2 = 0;
	auto x = stoi(s.substr(p), &p2);
	p += p2;
	auto y = stoi(s.substr(p), &p2);
	p += p2;
	auto r = s.substr(p, 2) == "ok";
	return std::make_pair((Pos){x, y}, (bool)r);
}

int Net::GetPlayerFork_Giver()
{
	if (queues["PFK"].empty())
		throw std::exception();
	std::string s = Popper(queues["PFK"]);
	return stoi(s.substr(4), 0);
}

std::pair<int, int> Net::GetResourceDrop_Giver()
{
	if (queues["PDR"].empty())
		throw std::exception();
	std::string s = Popper(queues["PDR"]);
	size_t p = 4;
	size_t p2 = 0;
	auto n = stoi(s.substr(p), &p2);
	p += p2;
	EStone st = (EStone)stoi(s.substr(p), &p2);
	return std::make_pair((int)n, (EStone)st);
}

std::pair<int, int> Net::GetPlayerLvl_Giver()
{
	if (queues["PLV"].empty())
		throw std::exception();
	std::string s = Popper(queues["PLV"]);
	size_t p = 4;
	size_t p2 = 0;
	auto n2 = stoi(s.substr(p), &p2);
	p += p2;
	auto l = stoi(s.substr(p), &p2);
	return std::make_pair<int, int>((int)n2, (int)l);
}

std::tuple<int, Pos, Stones> Net::GetPlayerInv_Giver()
{
	if (queues["PIN"].empty())
		throw std::exception();
	std::string s = Popper(queues["PIN"]);
	size_t p = 4;
	size_t p2 = 0;
	Pos pos{};
	Stones st{};
	int player;
	player = stoi(s.substr(p), &p2);
	p += p2;
	pos.x = stoi(s.substr(p), &p2);
	p += p2;
	pos.y = stoi(s.substr(p), &p2);
	p += p2;
	st.food = stoi(s.substr(p), &p2);
	p += p2;
	st.linemate = stoi(s.substr(p), &p2);
	p += p2;
	st.deraumere = stoi(s.substr(p), &p2);
	p += p2;
	st.sibur = stoi(s.substr(p), &p2);
	p += p2;
	st.mendiane = stoi(s.substr(p), &p2);
	p += p2;
	st.phiras = stoi(s.substr(p), &p2);
	p += p2;
	st.thystame = stoi(s.substr(p), &p2);
	return std::make_tuple(player, pos, st);
}

std::pair<int, int> Net::GetResourceCollect_Giver()
{
	if (queues["PGT"].empty())
		throw std::exception();
	std::string s = Popper(queues["PGT"]);
	size_t p = 4;
	size_t p2 = 0;
	auto n = stoi(s.substr(p), &p2);
	p += p2;
	EStone st = (EStone)stoi(s.substr(p), &p2);
	return std::make_pair((int)n, (EStone)st);
}

int Net::GetPlayerDeath_Giver()
{
	if (queues["PDI"].empty())
		throw std::exception();
	std::string s = Popper(queues["PDI"]);
	return stoi(s.substr(4), 0);
}

std::tuple<int, int, Pos> Net::GetEggLaid_Giver()
{
	while (queues["ENW"].empty())
		throw std::exception();
	std::string s = Popper(queues["ENW"]);
	size_t p = 4;
	size_t p2 = 0;
	auto e = stoi(s.substr(p), &p2);
	p += p2;
	auto n = stoi(s.substr(p), &p2);
	p += p2;
	auto x = stoi(s.substr(p), &p2);
	p += p2;
	auto y = stoi(s.substr(p), &p2);
	return std::make_tuple((int)e, (int)n, (Pos){x, y});
}

int Net::GetEggHatch_Giver()
{
	if (queues["EHT"].empty())
		throw std::exception();
	std::string s = Popper(queues["EHT"]);
	return stoi(s.substr(4), 0);
}

int Net::GetEggConnect_Giver()
{
	if (queues["EBO"].empty())
		throw std::exception();
	std::string s = Popper(queues["EBO"]);
	return stoi(s.substr(4), 0);
}

int Net::GetEggDeath_Giver()
{
	if (queues["EDI"].empty())
		throw std::exception();
	std::string s = Popper(queues["EDI"]);
	return stoi(s.substr(4), 0);
}

int Net::GetTimeUnit_Giver()
{
	while (queues["SGT"].empty()) usleep(1);
	std::string s = Popper(queues["SGT"]);
	return stoi(s.substr(4), 0);
}

int Net::SetTimeUnit_Giver()
{
	while (queues["SST"].empty()) usleep(1);
	std::string s = Popper(queues["SST"]);
	return stoi(s.substr(4), 0);
}

bool Net::GetEndGame_Giver()
{
	if (queues["SEG"].empty())
		return false;
	Popper(queues["SEG"]);
	return true;
}

std::string Net::GetServerMessage_Giver()
{
	if (queues["SMG"].empty())
		throw std::exception();
	std::string s = Popper(queues["SMG"]);
	return s.substr(4);
}

std::pair<Pos, Stones> Net::GetContent_Giver()
{
	if (queues["BCT"].empty())
		throw std::exception();
	std::string s = Popper(queues["BCT"]);
	size_t p = 4;
	size_t p2 = 0;
	Stones st{};
	auto x = stoi(s.substr(p), &p2);
	p += p2;
	auto y = stoi(s.substr(p), &p2);
	p += p2;
	st.food = stoi(s.substr(p), &p2);
	p += p2;
	st.linemate = stoi(s.substr(p), &p2);
	p += p2;
	st.deraumere = stoi(s.substr(p), &p2);
	p += p2;
	st.sibur = stoi(s.substr(p), &p2);
	p += p2;
	st.mendiane = stoi(s.substr(p), &p2);
	p += p2;
	st.phiras = stoi(s.substr(p), &p2);
	p += p2;
	st.thystame = stoi(s.substr(p), &p2);
	return std::make_pair((Pos){x, y}, st);
}

std::string Net::GetTeamNames_Giver()
{
	if (queues["TNA"].empty())
		throw std::exception();
	std::string s = Popper(queues["TNA"]);
	return s.substr(4);
}

std::tuple<int, Orient, int, std::string> Net::GetPlayerConnection_Giver()
{
	if (queues["PNW"].empty())
		throw std::exception();
	std::string s = Popper(queues["PNW"]);
	size_t p = 4;
	size_t p2 = 0;
	auto n = stoi(s.substr(p), &p2);
	p += p2;
	auto x = stoi(s.substr(p), &(p2));
	p += p2;
	auto y = stoi(s.substr(p), &(p2));
	p += p2;
	auto o = stoi(s.substr(p), &(p2));
	p += p2;
	auto l = stoi(s.substr(p), &(p2));
	p += p2;
	std::string t(s.substr(p));
	return std::make_tuple(n, Orient{x, y, o}, l, t);
}

void Net::SendGraphIdentification(const char *s)
{
	Send(s);
}

/*
** Definition of `CI_char_traits` functions
** Used for case-insensitive strings
*/

bool CI_char_traits::eq(char c1, char c2)
{
	return (toupper(c1) == toupper(c2));
}

bool CI_char_traits::ne(char c1, char c2)
{
	return (toupper(c1) != toupper(c2));
}

bool CI_char_traits::lt(char c1, char c2)
{
	return (toupper(c1) < toupper(c2));
}

int CI_char_traits::compare(const char* s1, const char* s2, size_t n)
{
	while(n-- != 0) {
		if(toupper(*s1) < toupper(*s2))
			return -1;
		if(toupper(*s1) > toupper(*s2))
			return 1;
		++s1;
		++s2;
	}
	return (0);
}

const char* CI_char_traits::find(const char* s, int n, char a)
{
	while(n-- > 0 && toupper(*s) != toupper(a)) {
		++s;
	}
	return s;
}