/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** Socket
*/

#include "Socket.hpp"

Socket::Socket()
	: sock(-1)
{}

Socket::Socket(std::string host, unsigned short port)
{
	sock = _Socket(AF_INET, SOCK_STREAM, 0);
	if (sock == -1)
		throw std::runtime_error("Cannot create socket");
	struct hostent *hostinfo = _GetHostByName(host);
	if (!hostinfo)
		throw std::runtime_error("Cannot find host");
	struct sockaddr_in sin;
	sin.sin_addr = *((struct in_addr *)hostinfo->h_addr);
	sin.sin_port = htons(port);
	sin.sin_family = AF_INET;
	if (_Connect(sock, (struct sockaddr*)&sin, sizeof(struct sockaddr)))
		throw std::runtime_error("Cannot connect socket");
}

Socket::~Socket()
{
	close(sock);
}

void Socket::Open(std::string host, unsigned short port)
{
	if (sock > 0)
		close(sock);
	sock = _Socket(AF_INET, SOCK_STREAM, 0);
	if (sock == -1)
		throw std::runtime_error("Cannot create socket");
	struct hostent *hostinfo = _GetHostByName(host);
	if (!hostinfo)
		throw std::runtime_error("Cannot find host");
	struct sockaddr_in sin;
	sin.sin_addr = *((struct in_addr *)hostinfo->h_addr);
	sin.sin_port = htons(port);
	sin.sin_family = AF_INET;
	if (_Connect(sock, (struct sockaddr*)&sin, sizeof(struct sockaddr)))
		throw std::runtime_error("Cannot connect socket");
}

int Socket::_Socket(int a, int b, int c)
{
	return (socket(a, b, c));
}

int Socket::_Connect(int a, const struct sockaddr *b, socklen_t c)
{
	return (connect(a, b, c));
}

struct hostent *Socket::_GetHostByName(std::string host)
{
	return (gethostbyname(host.c_str()));
}

unsigned short Socket::_HToNS(unsigned short port)
{
	return (htons(port));
}

size_t Socket::Send(std::string s)
{
	return (write(sock, s.c_str(), s.length()));
}

std::string Socket::Recv()
{
	char t[4097];
	auto end = read(sock, t, 4096);
	end = end == -1 ? 0: end;
	t[end] = 0;
	return (std::string(t));
}
