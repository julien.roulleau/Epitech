/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** Net
*/

#ifndef NET_HPP_
	#define NET_HPP_

	#include <string>
	#include <thread>
	#include <utility>
	#include <list>
	#include <unordered_map>
	#include <queue>
	#include <iostream>
	#include <sstream>

	#include "Await.hpp"
	#include "Socket.hpp"

// Move outside ?
struct Stones {
	int food;
	int linemate;
	int deraumere;
	int sibur;
	int mendiane;
	int phiras;
	int thystame;
};

enum EStone {
	STONE_FOOD,
	STONE_LINEMATE,
	STONE_DERAUMERE,
	STONE_SIBUR,
	STONE_MENDIANE,
	STONE_PHIRAS,
	STONE_THYSTAME
};

struct Pos {
	int x;
	int y;
};

struct Orient {
	int x;
	int y;
	int o;
};

class Net {
	static bool isConnected;
	static bool instantiated;

	static std::unordered_map<std::string, std::queue<std::string>> queues;
	static std::queue<std::string> sendQueue;
	static std::mutex sendQueueMutex;

	static Socket sock;

	static void Send(std::string);
	static void Read_Parse(std::string);
	static void Read_Worker();
	static void Write_Worker();
	static void Worker(std::string host, unsigned short port, Async<bool>&);
protected:
	static int _Select_2(int);
	static int _Select_1(int);

	template <class T>
	static T Popper(std::queue<T> &q)
	{
		T t = q.front();
		q.pop();
		return (t);
	}
public:
	static Async<bool> Connect(std::string host, unsigned short port);
	static int Select();
	static void SendGraphIdentification(const char*);

	static Async<Pos> GetSize();
	static Async<std::pair<Pos, Stones>> GetContent();
	static Async<std::unordered_map<Pos, Stones>> GetContents();
	static Async<std::string> GetTeamNames();
	static Async<std::tuple<int, Orient, int, std::string>> GetPlayerConnection();
	static Async<int> GetPlayerList();
	static Async<std::pair<int, Orient>> GetPlayerPos();
	static Async<std::pair<int, int>> GetPlayerLvl();
	static Async<std::tuple<int, Pos, Stones>> GetPlayerInv();
	static Async<std::pair<int, std::string>> GetPlayerTeam();
	static Async<int> GetExpulsion();
	static Async<std::pair<int, std::string>> GetBroadcast();
	static Async<std::tuple<Pos, int, std::list<int>>> GetIncantationStart();
	static Async<std::pair<Pos, bool>> GetIncantationEnd();
	static Async<int> GetPlayerFork();
	static Async<std::pair<int, int>> GetResourceDrop();
	static Async<std::pair<int, int>> GetResourceCollect();
	static Async<int> GetPlayerDeath();
	static Async<std::tuple<int, int, Pos>> GetEggLaid();
	static Async<int> GetEggHatch();
	static Async<int> GetEggConnect();
	static Async<int> GetEggDeath();
	static Async<int> GetTimeUnit();
	static Async<int> SetTimeUnit();
	static Async<bool> GetEndGame();
	static Async<std::string> GetServerMessage();

	static Pos GetSize_Giver();
	static std::pair<Pos, Stones> GetContent_Giver();
	static std::unordered_map<Pos, Stones> GetContents_Giver();
	static std::string GetTeamNames_Giver();
	static std::tuple<int, Orient, int, std::string> GetPlayerConnection_Giver();
	static int GetPlayerList_Giver();
	static std::pair<int, Orient> GetPlayerPos_Giver();
	static std::pair<int, int> GetPlayerLvl_Giver();
	static std::tuple<int, Pos, Stones> GetPlayerInv_Giver();
	static std::pair<int, std::string> GetPlayerTeam_Giver();
	static int GetExpulsion_Giver();
	static std::pair<int, std::string> GetBroadcast_Giver();
	static std::tuple<Pos, int, std::list<int>> GetIncantationStart_Giver();
	static std::pair<Pos, bool> GetIncantationEnd_Giver();
	static int GetPlayerFork_Giver();
	static std::pair<int, int> GetResourceDrop_Giver();
	static std::pair<int, int> GetResourceCollect_Giver();
	static int GetPlayerDeath_Giver();
	static std::tuple<int, int, Pos> GetEggLaid_Giver();
	static int GetEggHatch_Giver();
	static int GetEggConnect_Giver();
	static int GetEggDeath_Giver();
	static int GetTimeUnit_Giver();
	static int SetTimeUnit_Giver();
	static bool GetEndGame_Giver();
	static std::string GetServerMessage_Giver();


	static void AskSize();
	static void AskContent(int x, int y);
	static void AskTeamNames();
	static void AskPlayerPos(int);
	static void AskPlayerLvl(int);
	static void AskPlayerInv(int);
	static void AskPlayerTeam(int);
	static void AskPlayerList();
	static void AskTimeUnit();
	static void AskTimeUnitSet(int);


private:
	static void GetSize_Worker(Async<Pos> &);
	static void GetContent_Worker(Async<std::pair<Pos, Stones>> &);
	static void GetContents_Worker(Async<std::unordered_map<Pos, Stones>> &);
	static void GetTeamNames_Worker(Async<std::string> &);
	static void GetPlayerConnection_Worker(Async<std::tuple<int, Orient, int, std::string>> &);
	static void GetPlayerList_Worker(Async<int> &);
	static void GetPlayerTeam_Worker(Async<std::pair<int, std::string>> &);
	static void GetPlayerPos_Worker(Async<std::pair<int, Orient>> &);
	static void GetPlayerLvl_Worker(Async<std::pair<int, int>> &);
	static void GetPlayerInv_Worker(Async<std::tuple<int, Pos, Stones>> &);
	static void GetExpulsion_Worker(Async<int> &);
	static void GetBroadcast_Worker(Async<std::pair<int, std::string>> &);
	static void GetIncantationStart_Worker(Async<std::tuple<Pos, int, std::list<int>>> &);
	static void GetIncantationEnd_Worker(Async<std::pair<Pos, bool>> &);
	static void GetPlayerFork_Worker(Async<int> &);
	static void GetResourceDrop_Worker(Async<std::pair<int, int>> &);
	static void GetResourceCollect_Worker(Async<std::pair<int, int>> &);
	static void GetPlayerDeath_Worker(Async<int> &);
	static void GetEggLaid_Worker(Async<std::tuple<int, int, Pos>> &);
	static void GetEggHatch_Worker(Async<int> &);
	static void GetEggConnect_Worker(Async<int> &);
	static void GetEggDeath_Worker(Async<int> &);
	static void GetTimeUnit_Worker(Async<int> &);
	static void SetTimeUnit_Worker(Async<int> &);
	static void GetEndGame_Worker(Async<bool> &);
	static void GetServerMessage_Worker(Async<std::string> &);
};

namespace std {
	template <>
	class hash<Pos> {
	public:
		std::size_t operator()(const Pos& k) const
		{
			return ((k.x << 24) + k.y);
		}
	};
}

bool operator==(Pos, Pos);

class CI_char_traits: public std::char_traits<char> {
public:
	static bool eq(char c1, char c2);
	static bool ne(char c1, char c2);
	static bool lt(char c1, char c2);
	static int compare(const char* s1, const char* s2, size_t n);
	static const char* find(const char* s, int n, char a);
};

using istring = std::basic_string<char, CI_char_traits>;

	#define NET_MSZ(x) (x & (1 <<  0))
	#define NET_BCT(x) (x & (1 <<  1))
	#define NET_MCT(x) (x & (1 <<  2))
	#define NET_TNA(x) (x & (1 <<  3))
	#define NET_PNW(x) (x & (1 <<  4))
	#define NET_PPO(x) (x & (1 <<  5))
	#define NET_PLV(x) (x & (1 <<  6))
	#define NET_PIN(x) (x & (1 <<  7))
	#define NET_PEX(x) (x & (1 <<  8))
	#define NET_PBC(x) (x & (1 <<  9))
	#define NET_PIC(x) (x & (1 << 10))
	#define NET_PIE(x) (x & (1 << 11))
	#define NET_PFK(x) (x & (1 << 12))
	#define NET_PDR(x) (x & (1 << 13))
	#define NET_PGT(x) (x & (1 << 14))
	#define NET_PDI(x) (x & (1 << 15))
	#define NET_ENW(x) (x & (1 << 16))
	#define NET_EHT(x) (x & (1 << 17))
	#define NET_EBO(x) (x & (1 << 18))
	#define NET_EDI(x) (x & (1 << 19))
	#define NET_SGT(x) (x & (1 << 20))
	#define NET_SST(x) (x & (1 << 21))
	#define NET_SEG(x) (x & (1 << 22))
	#define NET_SMG(x) (x & (1 << 23))
	#define NET_PLS(x) (x & (1 << 24))
	#define NET_PLT(x) (x & (1 << 25))

#endif /* !NET_HPP_ */
