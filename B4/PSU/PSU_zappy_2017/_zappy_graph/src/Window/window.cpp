/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** window.cpp
*/

#include <iostream>
#include <tool/Pos.hpp>
#include "window.hpp"

windowGraph::windowGraph(Graph *graph_)
{
	graph = graph_;
	window.create(sf::VideoMode(SCREEN_SIZE_X, SCREEN_SIZE_Y), "Zappy");
	if (!mainFont.loadFromFile("./_zappy_graph/media/arial-black.ttf"))
		if (!mainFont.loadFromFile("./media/arial-black.ttf"))
			exit(1);
	for (auto it = graph->map.begin();
		it != graph->map.end(); std::advance(it, 1)) {
		pos2i p = it->first;
		tileMap.emplace_back(windowTile(p, graph->size, &mainFont));
	}

	Title.setString("ZAPPY");
	Title.setCharacterSize(60);
	Title.setFont(mainFont);
	Title.setPosition((float)1200, (float)20);
	t.setFont(mainFont);
	t.setColor({0, 0, 0, 255});
	t.setCharacterSize(20);

	float r = graph->size.X < graph->size.Y ?
		  MAP_SIZE / static_cast<float>(graph->size.X) / (float)4 :
		  MAP_SIZE / static_cast<float>(graph->size.Y) / (float)4;
	PlayerShape.setPointCount(3);
	PlayerShape.setRadius(r);
	PlayerShape.setFillColor({228, 193, 129, 255});
	PlayerShape.setOutlineColor({0, 0, 0, 255});
	PlayerShape.setOutlineThickness(1);
	PlayerShape.setOrigin(PlayerShape.getRadius(), PlayerShape.getRadius());

	EggShape.setRadius(r);
	EggShape.setFillColor({230, 230, 230, 255});
	EggShape.setOrigin(EggShape.getRadius(), EggShape.getRadius());

	back.setFillColor({100, 100, 100, 255});
	back.setOutlineColor({0, 0, 0, 255});
	back.setOutlineThickness(3);
	if (!legendetext.loadFromFile("./_zappy_graph/media/legende.png"))
		if (!legendetext.loadFromFile("./media/legende.png"))
			exit(1);
	legendeSprite.setTexture(legendetext);
	legendeSprite.setPosition({1500, 0});
	legendeSprite.setScale({0.35f, 0.35f});
}

windowGraph::~windowGraph()
{
}

void windowGraph::update()
{
	window.clear({40, 40, 40, 255});
	this->updateContent();
	window.display();
}

void windowGraph::Event()
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			window.close();
	}
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		auto pos = sf::Mouse::getPosition(window);
		pos2i tPos{0, 0};

		tPos.X = (pos.x - ((SCREEN_SIZE_Y - MAP_SIZE) / 2))
			/ (MAP_SIZE / graph->size.X);
		tPos.Y = (pos.y - ((SCREEN_SIZE_Y - MAP_SIZE) / 2))
			/ (MAP_SIZE / graph->size.Y);
		if (tPos.X >= 0 && tPos.X < graph->size.X &&
			tPos.Y >= 0 && tPos.Y < graph->size.Y) {
			if (!(tPos == graph->posTile)) {
				graph->playersOnTile = 0;
				graph->EggOnTile = 0;
				for (auto it = graph->playerList.begin();
					it != graph->playerList.end();
					std::advance(it, 1))
					if (it->second.position == tPos)
						graph->playersOnTile++;
				for (auto it = graph->eggList.begin();
				     it != graph->eggList.end();
				     std::advance(it, 1))
					if (it->second.position == tPos)
						graph->EggOnTile++;
			}
			graph->posTile = tPos;
			int id = -1;
			for (auto it = graph->playerList.begin();
				it != graph->playerList.end();
				std::advance(it, 1)) {
				if (it->second.position == tPos) {
					id = it->first;
					break;
				}
			}
			if (id != -1)
				graph->IdPlayer = id;
		}
	}
}

bool windowGraph::isOpen()
{
	return window.isOpen();
}

void windowGraph::updateContent()
{
	window.draw(legendeSprite);
	for (auto it = tileMap.begin(); it != tileMap.end(); std::advance(it, 1))
		it->draw(&window, graph);
	window.draw(Title);
	drawEgg();
	drawPlayer();
	drawGlobal();
	drawDetailPlayer();
	drawDetailTile();
	drawBroadcasr();
}

void windowGraph::drawPlayer()
{
	for (auto it = graph->playerList.begin();
		it != graph->playerList.end(); std::advance(it, 1)) {
		pos2i tSize{0, 0};
		tSize.X = MAP_SIZE / graph->size.X;
		tSize.Y = MAP_SIZE / graph->size.Y;
		PlayerShape.setRotation(360 - it->second.orientation * 90);
		PlayerShape.setPosition({
			static_cast<float>((SCREEN_SIZE_Y - MAP_SIZE) / 2 +
				tSize.X * it->second.position.X) +
				static_cast<float>(tSize.X) * float(0.75),
			static_cast<float>((SCREEN_SIZE_Y - MAP_SIZE) / 2 +
				tSize.Y * it->second.position.Y) +
				static_cast<float>(tSize.Y) * float(0.5)});
		window.draw(PlayerShape);
	}
}

void windowGraph::drawEgg()
{
	for (auto it = graph->eggList.begin();
		it != graph->eggList.end(); std::advance(it, 1)) {
		pos2i tSize{0, 0};
		tSize.X = MAP_SIZE / graph->size.X;
		tSize.Y = MAP_SIZE / graph->size.Y;
		EggShape.setPosition({
			static_cast<float>((SCREEN_SIZE_Y - MAP_SIZE) / 2 +
				tSize.X * it->second.position.X) +
				static_cast<float>(tSize.X) * float(0.25),
			static_cast<float>((SCREEN_SIZE_Y - MAP_SIZE) / 2 +
				tSize.Y * it->second.position.Y) +
				static_cast<float>(tSize.Y) * float(0.5)});
		window.draw(EggShape);
	}
}

void windowGraph::drawGlobal()
{
	back.setPosition({1100, 120});
	back.setSize({350, 200});
	window.draw(back);
	std::string s = "Team Number :   " + std::to_string(graph->teams.size());
	t.setPosition({1120, 130});
	t.setString(s);
	window.draw(t);
	s = "Player Number : " + std::to_string(graph->playerList.size());
	t.setPosition({1120, 155});
	t.setString(s);
	window.draw(t);
	s = "Egg Number :    " + std::to_string(graph->eggList.size());
	t.setPosition({1120, 180});
	t.setString(s);
	window.draw(t);

	back.setPosition({1500, 120});
	window.draw(back);

	float y = 130;
	for (auto it = graph->teams.begin();
		it != graph->teams.end() && y <= 300; std::advance(it, 1)) {
		s = it->first + " : " + std::to_string(it->second);
		t.setPosition({1520, y});
		t.setString(s);
		window.draw(t);
		y += 25;
	}
}

void windowGraph::drawDetailPlayer()
{
	back.setPosition({1100, 360});
	back.setSize({350, 300});
	window.draw(back);
	std::string s = "PLAYER";
	t.setPosition({1120, 360});
	t.setString(s);
	window.draw(t);
	if (graph->IdPlayer != -1 && graph->playerList.find(graph->IdPlayer)
				     != graph->playerList.end()) {
		auto it = graph->playerList.find(graph->IdPlayer);
		s = "Pos X : " + std::to_string(it->second.position.X)
			+ "   Pos Y : " + std::to_string(it->second.position.Y);
		t.setPosition({1120, 390});
		t.setString(s);
		window.draw(t);
		s = "Orientation : " + std::to_string(it->second.orientation);
		t.setPosition({1120, 410});
		t.setString(s);
		window.draw(t);
		s = "Level : " + std::to_string(it->second.level);
		t.setPosition({1120, 430});
		t.setString(s);
		window.draw(t);
		s = "Team : " + it->second.teamName;
		t.setPosition({1120, 450});
		t.setString(s);
		window.draw(t);
		s = "Food : " + std::to_string(it->second.ressources[0]);
		t.setPosition({1120, 480});
		t.setString(s);
		window.draw(t);
		s = "Linemate : " + std::to_string(it->second.ressources[1]);
		t.setPosition({1120, 500});
		t.setString(s);
		window.draw(t);
		s = "Deraumere : " + std::to_string(it->second.ressources[2]);
		t.setPosition({1120, 520});
		t.setString(s);
		window.draw(t);
		s = "Sibur : " + std::to_string(it->second.ressources[3]);
		t.setPosition({1120, 540});
		t.setString(s);
		window.draw(t);
		s = "Mendiane : " + std::to_string(it->second.ressources[4]);
		t.setPosition({1120, 560});
		t.setString(s);
		window.draw(t);
		s = "Phiras : " + std::to_string(it->second.ressources[5]);
		t.setPosition({1120, 580});
		t.setString(s);
		window.draw(t);
		s = "Thystame : " + std::to_string(it->second.ressources[6]);
		t.setPosition({1120, 600});
		t.setString(s);
		window.draw(t);
	}
}

void windowGraph::drawDetailTile()
{
	back.setPosition({1500, 360});
	back.setSize({350, 300});
	window.draw(back);
	std::string s = "TILE";
	t.setPosition({1520, 360});
	t.setString(s);
	window.draw(t);

	if (graph->posTile.X != -1) {
		auto it = graph->map.find(graph->posTile);
		s = "Pos X : " + std::to_string(graph->posTile.X)
		    + "   Pos Y : " + std::to_string(graph->posTile.Y);
		t.setPosition({1520, 390});
		t.setString(s);
		window.draw(t);
		s = "Players On Tile : " + std::to_string(graph->playersOnTile);
		t.setPosition({1520, 420});
		t.setString(s);
		window.draw(t);
		s = "Eggs On Tile : " + std::to_string(graph->EggOnTile);
		t.setPosition({1520, 440});
		t.setString(s);
		window.draw(t);

		s = "Food : " + std::to_string(it->second.ressources[0]);
		t.setPosition({1520, 480});
		t.setString(s);
		window.draw(t);
		s = "Linemate : " + std::to_string(it->second.ressources[1]);
		t.setPosition({1520, 500});
		t.setString(s);
		window.draw(t);
		s = "Deraumere : " + std::to_string(it->second.ressources[2]);
		t.setPosition({1520, 520});
		t.setString(s);
		window.draw(t);
		s = "Sibur : " + std::to_string(it->second.ressources[3]);
		t.setPosition({1520, 540});
		t.setString(s);
		window.draw(t);
		s = "Mendiane : " + std::to_string(it->second.ressources[4]);
		t.setPosition({1520, 560});
		t.setString(s);
		window.draw(t);
		s = "Phiras : " + std::to_string(it->second.ressources[5]);
		t.setPosition({1520, 580});
		t.setString(s);
		window.draw(t);
		s = "Thystame : " + std::to_string(it->second.ressources[6]);
		t.setPosition({1520, 600});
		t.setString(s);
		window.draw(t);
	}
}

void windowGraph::drawBroadcasr()
{
	back.setPosition({1100, 680});
	back.setSize({750, 300});
	window.draw(back);
	std::string s = "Broadcast";
	t.setPosition({1120, 690});
	t.setString(s);
	window.draw(t);
	t.setCharacterSize(15);

	float y = 710;
	for (auto i = graph->broadcast.begin();
		i != graph->broadcast.end(); std::advance(i, 1)) {
		s = *i;
		t.setPosition({1120, y});
		t.setString(s);
		window.draw(t);
		y += 17;
	}


	t.setCharacterSize(20);

}

// Window Tile

windowTile::windowTile(pos2i &p, pos2i &size, sf::Font *f) :  font(f), pos(p)
{
	pos2i tSize{0, 0};
	pos2i tPos{0, 0};

	tSize.X = MAP_SIZE / size.X;
	tSize.Y = MAP_SIZE / size.Y;
	tPos.X = (SCREEN_SIZE_Y - MAP_SIZE) / 2 + tSize.X * p.X;
	tPos.Y = (SCREEN_SIZE_Y - MAP_SIZE) / 2 + tSize.Y * p.Y;
	tile.setSize({static_cast<float>(tSize.X),
				       static_cast<float>(tSize.Y)});
	tile.setPosition({static_cast<float>(tPos.X),
			   static_cast<float>(tPos.Y)});
	tile.setFillColor({67, 141, 67, 255});
	tile.setOutlineThickness(static_cast<float>(-2));
	tile.setOutlineColor({110, 80, 50, 255});

	food.setSize({
		static_cast<float>(tSize.X) * (float)0.2,
		static_cast<float>(tSize.Y) * (float)0.2});
	food.setFillColor({0, 255, 0, 255});
	food.setPosition({
		static_cast<float>(tPos.X) + static_cast<float>(tSize.X) * float(0.02),
		static_cast<float>(tPos.Y) + static_cast<float>(tSize.Y) * float(0.05)});

	linemate.setSize({
		static_cast<float>(tSize.X) * (float)0.2,
		static_cast<float>(tSize.Y) * (float)0.2});
	linemate.setFillColor({48, 15, 167, 255});
	linemate.setPosition({
		static_cast<float>(tPos.X) + static_cast<float>(tSize.X) * float(0.27),
		static_cast<float>(tPos.Y) + static_cast<float>(tSize.Y) * float(0.05)});

	deraumere.setSize({
		static_cast<float>(tSize.X) * (float)0.2,
		static_cast<float>(tSize.Y) * (float)0.2});
	deraumere.setFillColor({110, 64, 124, 255});
	deraumere.setPosition({
		static_cast<float>(tPos.X) + static_cast<float>(tSize.X) * float(0.52),
		static_cast<float>(tPos.Y) + static_cast<float>(tSize.Y) * float(0.05)});

	sibur.setSize({
		static_cast<float>(tSize.X) * (float)0.2,
		static_cast<float>(tSize.Y) * (float)0.2});
	sibur.setFillColor({230, 230, 0, 255});
	sibur.setPosition({
		static_cast<float>(tPos.X) + static_cast<float>(tSize.X) * float(0.02),
		static_cast<float>(tPos.Y) + static_cast<float>(tSize.Y) * float(0.75)});

	mendiane.setSize({
		static_cast<float>(tSize.X) * (float)0.2,
		static_cast<float>(tSize.Y) * (float)0.2});
	mendiane.setFillColor({230, 20, 20, 255});
	mendiane.setPosition({
		static_cast<float>(tPos.X) + static_cast<float>(tSize.X) * float(0.27),
		static_cast<float>(tPos.Y) + static_cast<float>(tSize.Y) * float(0.75)});

	phiras.setSize({
		static_cast<float>(tSize.X) * (float)0.2,
		static_cast<float>(tSize.Y) * (float)0.2});
	phiras.setFillColor({230, 0, 230, 255});
	phiras.setPosition({
		static_cast<float>(tPos.X) + static_cast<float>(tSize.X) * float(0.52),
		static_cast<float>(tPos.Y) + static_cast<float>(tSize.Y) * float(0.75)});

	thystame.setSize({
		static_cast<float>(tSize.X) * (float)0.2,
		static_cast<float>(tSize.Y) * (float)0.2});
	thystame.setFillColor({255, 153, 0, 255});
	thystame.setPosition({
		static_cast<float>(tPos.X) + static_cast<float>(tSize.X) * float(0.77),
		static_cast<float>(tPos.Y) + static_cast<float>(tSize.Y) * float(0.75)});

	t.setFont(*font);
	t.setColor({0, 0, 0, 255});
}

windowTile::~windowTile()
{
}

void windowTile::draw(sf::RenderWindow *window, Graph *graph)
{
	auto it = graph->map.find(pos);
	if (it == graph->map.end())
		return;
	window->draw(tile);
	if (it->second.ressources[0] > 0) {
		window->draw(food);
		std::string n = std::to_string(it->second.ressources[0]);
		auto size = food.getSize().x;
		t.setString(n);
		t.setPosition({food.getPosition().x + size * (float)0.1,
			       food.getPosition().y + size * (float)0.1});
		t.setCharacterSize((unsigned int)(size * 0.8));
		window->draw(t);
	}
	if (it->second.ressources[1] > 0) {
		window->draw(linemate);
		std::string n = std::to_string(it->second.ressources[1]);
		auto size = linemate.getSize();
		t.setString(n);
		t.setPosition({linemate.getPosition().x + size.x * (float)0.1,
			       linemate.getPosition().y + size.y * (float)0.1});
		t.setCharacterSize((unsigned int)(size.x * 0.8));
		window->draw(t);
	}
	if (it->second.ressources[2] > 0) {
		window->draw(deraumere);
		std::string n = std::to_string(it->second.ressources[2]);
		auto size = deraumere.getSize().x;
		t.setString(n);
		t.setPosition({deraumere.getPosition().x + size * (float)0.1,
			       deraumere.getPosition().y + size * (float)0.1});
		t.setCharacterSize((unsigned int)(size * 0.8));
		window->draw(t);
	}
	if (it->second.ressources[3] > 0) {
		window->draw(sibur);
		std::string n = std::to_string(it->second.ressources[3]);
		auto size = sibur.getSize().x;
		t.setString(n);
		t.setPosition({sibur.getPosition().x + size * (float)0.1,
			       sibur.getPosition().y + size * (float)0.1});
		t.setCharacterSize((unsigned int)(size * 0.8));
		window->draw(t);
	}
	if (it->second.ressources[4] > 0) {
		window->draw(mendiane);
		std::string n = std::to_string(it->second.ressources[4]);
		auto size = mendiane.getSize().x;
		t.setString(n);
		t.setPosition({mendiane.getPosition().x + size * (float)0.1,
			       mendiane.getPosition().y + size * (float)0.1});
		t.setCharacterSize((unsigned int)(size * 0.8));
		window->draw(t);
	}
	if (it->second.ressources[5] > 0) {
		window->draw(phiras);
		std::string n = std::to_string(it->second.ressources[5]);
		auto size = phiras.getSize().x;
		t.setString(n);
		t.setPosition({phiras.getPosition().x + size * (float)0.1,
			       phiras.getPosition().y + size * (float)0.1});
		t.setCharacterSize((unsigned int)(size * 0.8));
		window->draw(t);
	}
	if (it->second.ressources[6] > 0) {
		window->draw(thystame);
		std::string n = std::to_string(it->second.ressources[6]);
		auto size = thystame.getSize().x;
		t.setString(n);
		t.setPosition({thystame.getPosition().x + size * (float)0.1,
			       thystame.getPosition().y + size * (float)0.1});
		t.setCharacterSize((unsigned int)(size * 0.8));
		window->draw(t);
	}
}

