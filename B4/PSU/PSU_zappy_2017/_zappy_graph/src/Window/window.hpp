/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** window.hpp
*/


#ifndef ZAPPY_WINDOW_HPP
#define ZAPPY_WINDOW_HPP

#include "Graph/Graph.hpp"
#include "tool/Pos.hpp"
#include <SFML/Graphics.hpp>

#define MAP_SIZE 950
#define SCREEN_SIZE_Y 1000
#define SCREEN_SIZE_X 1920

class windowTile {
public:
	explicit windowTile(pos2i &p, pos2i &size, sf::Font *f);
	~windowTile();
	void draw(sf::RenderWindow *window, Graph *graph);
private:
	sf::Text t;
	sf::Font *font;
	pos2i pos;
	sf::RectangleShape tile;
	sf::RectangleShape food;
	sf::RectangleShape linemate;
	sf::RectangleShape deraumere;
	sf::RectangleShape sibur;
	sf::RectangleShape mendiane;
	sf::RectangleShape phiras;
	sf::RectangleShape thystame;
};

class windowGraph {
public:
	windowGraph(Graph *graph);
	~windowGraph();
	void update();
	void updateContent();
	void Event();
	bool isOpen();

private:
	void drawPlayer();
	void drawEgg();
	void drawGlobal();
	void drawDetailPlayer();
	void drawDetailTile();
	void drawBroadcasr();

private:
	std::vector<windowTile> tileMap;
	sf::RenderWindow window{};
	Graph *graph;
	sf::Text Title;
	sf::Text t;
	sf::Font mainFont{};
	sf::CircleShape PlayerShape{};
	sf::CircleShape EggShape{};
	sf::RectangleShape back;
	sf::Sprite legendeSprite;
	sf::Texture legendetext;
};


#endif /* ZAPPY_WINDOW_HPP */
