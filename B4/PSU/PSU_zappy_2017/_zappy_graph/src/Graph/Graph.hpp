/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** Graph.hpp
*/


#ifndef ZAPPY_GRAPH_HPP
#define ZAPPY_GRAPH_HPP

#include <vector>
#include <map>
#include <string>
#include "tool/Pos.hpp"


class Egg {
public:
	Egg(pos2i pos) : position(pos) {};
	~Egg() = default;

	pos2i position;
};

class Player {
public:
	Player(pos2i &pos, int o, int l, std::string &team);
	~Player() = default;

	int orientation;
	pos2i position;
	int level;
	int ressources[7];
	std::string teamName;
	int incentation;
};

class Tiles {
public:
	Tiles(int q0, int q1, int q2, int q3, int q4, int q5, int q6);
	~Tiles() = default;

	int ressources[7];
};

class Graph {
public:
	Graph();
	~Graph();

	int TimeUnit;
	pos2i size;
	std::vector<std::string> broadcast;
	std::map<std::string, int> teams;
	std::map<int, Player> playerList;
	int IdPlayer;
	pos2i posTile;
	int playersOnTile;
	int EggOnTile;
	std::map<int, Egg> eggList;
	std::map<pos2i, Tiles> map;
};

bool init_graph(Graph &graph);
bool update_graph(Graph &graph, int select, bool refresh);

#endif /* ZAPPY_GRAPH_HPP */
