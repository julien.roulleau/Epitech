/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** Graph.cpp
*/


#include "Graph.hpp"

Tiles::Tiles(int q0, int q1, int q2, int q3, int q4, int q5, int q6)
{
	ressources[0] = q0;
	ressources[1] = q1;
	ressources[2] = q2;
	ressources[3] = q3;
	ressources[4] = q4;
	ressources[5] = q5;
	ressources[6] = q6;
};

Player::Player(pos2i &pos, int o, int l, std::string &team)
	: orientation(o), position(pos), level(l), teamName(team),
	  incentation(0)
{
	ressources[0] = 0;
	ressources[1] = 0;
	ressources[2] = 0;
	ressources[3] = 0;
	ressources[4] = 0;
	ressources[5] = 0;
	ressources[6] = 0;
}


Graph::~Graph()
{
}

Graph::Graph() : TimeUnit(100), size({10, 10}), IdPlayer(-1), posTile({-1, -1}),
		 playersOnTile(0), EggOnTile(0)
{
}
