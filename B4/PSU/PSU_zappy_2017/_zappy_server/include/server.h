/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** server
*/

#ifndef SERVER_H_
	#define SERVER_H_

	#include <socket.h>
	#include <dic.h>

typedef struct s_args {
	short port;
	int width;
	int height;
	dic_t names;
	int cnb;
	int freq;
} *args_t;

typedef struct s_stones {
	int food;
	int st1;
	int st2;
	int st3;
	int st4;
	int st5;
	int st6;
} stones_t;

typedef struct s_player {
	int x;
	int y;
	int o;
	int lvl;
	socket_t sock;
	stones_t inv;
	char *team;
	int tick_food;
	int tick_act;
	void (*resume)(char*, struct s_player*);
	char *last_cmd;
} *player_t;

typedef struct s_egg {
	int x;
	int y;
	char *owner;
	char *team;
	int ticks;
} *egg_t;

typedef struct s_tiles {
	stones_t stones;
	dic_t players;
} *tiles_t;

typedef struct s_game {
	args_t args;
	tiles_t map;
	dic_t teams;
} *game_t;

typedef struct s_global {
	game_t game;
	dic_t queue;
	dic_t registered;
	dic_t players;
	dic_t graphs;
	dic_t eggs;
	dic_t incantations;
	socket_t sock;
	int last_id;
} global_t;

#define ABS(x) ((x) < 0 ? -(x) : (x))
#define WIDTH (GM.game->args->width)
#define HEIGHT (GM.game->args->height)

void ctor(int, char**) __attribute__((constructor));

void add_player(socket_t);
void egg_tick(char*, egg_t);
void game_loop();
void make_game(args_t);
void map_generate();
void map_tick();
void graph_send(char*);
void tick_run();
void delete_player(char*, player_t);

int incantation_check(player_t);
int can_try(socket_t);

player_t new_player(socket_t, char*);

egg_t new_egg(player_t, char*);

void cl_play_die(char*, player_t);

int cl_play_broadcast(char*, player_t, char*);
int cl_play_cnb(char*, player_t, char*);
int cl_play_eject(char*, player_t, char*);
int cl_play_fork(char*, player_t, char*);
int cl_play_forward(char*, player_t, char*);
int cl_play_incantation(char*, player_t, char*);
int cl_play_inventory(char*, player_t, char*);
int cl_play_left(char*, player_t, char*);
int cl_play_look(char*, player_t, char*);
int cl_play_right(char*, player_t, char*);
int cl_play_set(char*, player_t, char*);
int cl_play_take(char*, player_t, char*);

int cl_queue_team(char*, socket_t, char*);

int cl_graph_bct(socket_t so, char *cmd);
int cl_graph_msz(socket_t so, char *cmd);
int cl_graph_pin(socket_t so, char *cmd);
int cl_graph_pls(socket_t so, char *cmd);
int cl_graph_plt(socket_t so, char *cmd);
int cl_graph_plv(socket_t so, char *cmd);
int cl_graph_ppo(socket_t so, char *cmd);
int cl_graph_sgt(socket_t so, char *cmd);
int cl_graph_sst(socket_t so, char *cmd);
int cl_graph_tna(socket_t so, char *cmd);

extern global_t GM;
extern int(*CMD_PLAY[])(char*, player_t, char*);
extern int(*CMD_GRAPH[])(socket_t, char*);

#endif /* !SERVER_H_ */
