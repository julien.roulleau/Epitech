/*
** EPITECH PROJECT, 2018
** socket
** File description:
** .mask.c
*/

#include <signal.h>

#include <socket.h>

static int sig_state(int st)
{
	static int sta = 0;

	if (st >= 0)
		sta = st;
	return (sta);
}

static void sig_handler(int sig)
{
	(void)sig;
	sig_state(1);
}

static void set_try()
{
	signal(SIGPIPE, sig_handler);
}

static int has_catch()
{
	int ret = sig_state(-1);

	sig_state(0);
	signal(SIGPIPE, SIG_DFL);
	return (ret);
}

int can_try(socket_t so)
{
	set_try();
	sock_update(so);
	return (!has_catch());
}