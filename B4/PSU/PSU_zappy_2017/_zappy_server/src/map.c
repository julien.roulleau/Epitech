/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** map
*/

#include <stdlib.h>

#include <log.h>

#include "server.h"

static long range(long min, long max)
{
	return (random() % (max - min) + min);
}

static int get_stone_prng(int pos)
{
	long st = random() & 0x7F;
	int i;

	if (dic_count(GM.game->map[pos].players))
		return (-1);
	for (i = 6; i >= 0; i--)
		if (st & (1 << i))
			return (6 - i);
	return (6);
}

void map_tick()
{
	int x = (int)range(0, GM.game->args->width);
	int y = (int)range(0, GM.game->args->height);
	int st = get_stone_prng(x + y * GM.game->args->width);
	stones_t sts = GM.game->map[x + y * GM.game->args->width].stones;

	if (!st && sts.food < 5)
		sts.food++;
	if (st == 1 && sts.st1 < 5)
		sts.st1++;
	if (st == 2 && sts.st2 < 5)
		sts.st2++;
	if (st == 3 && sts.st3 < 4)
		sts.st3++;
	if (st == 4 && sts.st4 < 3)
		sts.st4++;
	if (st == 5 && sts.st5 < 2)
		sts.st5++;
	if (st == 6 && sts.st6 < 1)
		sts.st6++;
	GM.game->map[x + y * GM.game->args->width].stones = sts;
}

void map_generate()
{
	long i = GM.game->args->width * GM.game->args->height;

	ilog("[PRNG] Seed for this run: %d", (int)((long)GM.game->map));
	srandom((int)((long)GM.game->map));
	for (i = range(i, i * 5); i >= 0; i--)
		map_tick();
}
