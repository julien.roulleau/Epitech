/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** ticker
*/

#include <sys/time.h>

#include "server.h"

static const int NSTOS = 1000000;

static void set_clk();

static struct timeval start = {0};

static struct timeval get_clk()
{
	struct timeval res = {0, 0};

	if (!gettimeofday(&res, 0)) {
		if (res.tv_usec < start.tv_usec) {
			res.tv_usec += NSTOS;
			res.tv_sec--;
		}
		res.tv_usec -= start.tv_usec;
		res.tv_sec -= start.tv_sec;
	}
	return (res);
}

static int is_clk()
{
	double f = (double)1 / GM.game->args->freq;
	struct timeval ts = get_clk();
	double f2 = ts.tv_sec + (double)ts.tv_usec / NSTOS;

	return (f2 >= f);
}

static void set_clk()
{
	struct timeval res;

	start = gettimeofday(&res, 0) ? (struct timeval){0, 0} : res;
}

static void reset_clk()
{
	double f = (double)1 / GM.game->args->freq;

	if (is_clk()) {
		start.tv_usec += f * NSTOS;
		while (start.tv_usec > NSTOS) {
			start.tv_usec -= NSTOS;
			start.tv_sec++;
		}
	}
}

void tick_run()
{
	if (start.tv_sec == 0 && start.tv_usec == 0)
		set_clk();
	if (is_clk())
		game_loop();
	reset_clk();
}