/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** incantation
*/

#include "server.h"

static const stones_t require[8] = {
	{1, 1, 0, 0, 0, 0, 0},
	{2, 1, 1, 1, 0, 0, 0},
	{2, 2, 0, 1, 0, 2, 0},
	{4, 1, 1, 2, 0, 1, 0},
	{4, 1, 2, 1, 3, 0, 0},
	{6, 1, 2, 3, 0, 1, 0},
	{6, 2, 2, 2, 2, 2, 1},
	{0, 0, 0, 0, 0, 0, 0},
};

int incantation_check(player_t pl)
{
	int pos = pl->x + pl->y * GM.game->args->width;
	stones_t req = require[pl->lvl - 1];
	stones_t ground = GM.game->map[pos].stones;

	if (dic_count(GM.game->map[pos].players) != req.food ||
		ground.st1 != req.st1 || ground.st2 != req.st2 || ground.st3 !=
		req.st3 || ground.st4 != req.st4 || ground.st5 != req.st5 ||
		ground.st6 != req.st6)
		return (0);
	foreach(GM.game->map[pos].players as k, v)
		if (((player_t)v)->lvl != pl->lvl)
			return (0);
	return (1);
}
