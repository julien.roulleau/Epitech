/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** parse
*/

#include <garbage_collector.h>
#include <log.h>

#include "server.h"

#define ASSERT(x, y) (x ? (void)0 : (void)y)

static void usage(char*, int) __attribute__((noreturn));
static void parse(char**, args_t, char*);

static void usage(char *name, int ret)
{
	printf("USAGE: %s -p port -x width -y height -n name1 name2 ... -c clie"
		"ntsNb -f freq\n\tport\t\tis the port number\n\twidth\t\tis the"
		" width of the world\n\theight\t\tis the height of the world\n"
		"\tnameX\t\tis the name of the team X\n\tclientsNb\tis the numb"
		"er of authorized clients per team\n\tfreq\t\tis the reciprocal"
		" of time unit for execution of actions\n", name);
	exit(ret);
}

static void init(args_t args)
{
	args->port = 4242;
	args->width = 10;
	args->height = 10;
	args->names = new_dic(50);
	args->cnb = 3;
	args->freq = 100;
}

static void parseNames(char **av, args_t ars, char *nm)
{
	int nop = 0;

	while (*(++av) && (**av != '-' || nop || !strcmp(*av, "--"))) {
		if (!strcmp(*av, "--")) {
			nop = 1;
			continue;
		}
		dic_set(ars->names, *av, *av);
	}
	if (!nop && *av)
		parse(av, ars, nm);
}

static void parse(char **av, args_t ars, char *nm)
{
	ASSERT(*(av + 1), usage(nm, 84));
	if (*av && !strcmp(*av, "-p")) {
		return (ASSERT((ars->port = atoi(av[1])) > 0 && atoi(av[1]) <
			0xFFFF, usage(nm, 84)), parse(av + 2, ars, nm));
	} else if (*av && !strcmp(*av, "-x")) {
		ASSERT((ars->width = atoi(av[1])) > 0, usage(nm, 84));
		parse(av + 2, ars, nm);
	} else if (*av && !strcmp(*av, "-y")) {
		ASSERT((ars->height = atoi(av[1])) > 0, usage(nm, 84));
		parse(av + 2, ars, nm);
	} else if (*av && !strcmp(*av, "-n")) {
		parseNames(av, ars, nm);
	} else if (*av && !strcmp(*av, "-c")) {
		ASSERT((ars->cnb = atoi(av[1])) > 0, usage(nm, 84));
		parse(av + 2, ars, nm);
	} else if (*av && !strcmp(*av, "-f")) {
		ASSERT((ars->freq = atoi(av[1])) > 0, usage(nm, 84));
		parse(av + 2, ars, nm);
	} else if (*av)
		usage(nm, 84);
}

void ctor(int argc, char **argv)
{
	args_t args = calloc(sizeof(struct s_args), 1);
	int i;

	log("starting args parser");
	for (i = 1; i < argc; i++) {
		if (!strcmp(argv[i], "--"))
			break;
		if (!strcmp(argv[i], "-help") || !strcmp(argv[i], "--help") ||
			!strcmp(argv[i], "-h"))
			usage(argv[0], 0);
	}
	init(args);
	if (argc > 1)
		parse(argv + 1, args, argv[0]);
	suc("args parser done");
	make_game(args);
	*argv = (char*)args;
}