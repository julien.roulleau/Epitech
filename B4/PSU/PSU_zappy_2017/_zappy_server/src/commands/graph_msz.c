/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** graph_msz.c
*/

#include <stdio.h>
#include "server.h"

int cl_graph_msz(socket_t so, char *cmd)
{
	char buff[512] = {0};

	if (strcasecmp("msz", cmd) != 0)
		return (0);
	sprintf(buff, "msz %d %d\n",
		GM.game->args->width, GM.game->args->height);
	sock_send(so, buff);
	return (1);
}