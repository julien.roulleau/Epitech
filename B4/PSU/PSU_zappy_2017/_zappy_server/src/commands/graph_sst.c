/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** graph_sst.c
*/

#include <stdio.h>
#include <stdlib.h>
#include "server.h"

int cl_graph_sst(socket_t so, char *cmd)
{
	char buff[512] = {0};
	int freq = 0;

	if (strncasecmp("sst ", cmd, 4) != 0)
		return (0);
	cmd += 4;
	if (!*cmd)
		return (sock_send(so, "sbp\n"), 1);
	freq = atoi(cmd);
	if (!freq)
		return (sock_send(so, "sbp\n"), 1);
	GM.game->args->freq = freq;
	sprintf(buff, "sst %d\n", freq);
	sock_send(so, buff);
	return (1);
}
