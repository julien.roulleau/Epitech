/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** play_inventory
*/

#include <stdio.h>

#include "server.h"

static void rs_play_inventory(char *id, player_t pl)
{
	char buff[512] = {0};
	stones_t st = pl->inv;

	(void)id;
	pl->resume = 0;
	sprintf(buff, "[food %d, linemate %d, deraumere %d, sibur %d, mendiane "
		"%d, phiras %d, thystame %d]\n", st.food, st.st1, st.st2,
		st.st3, st.st4, st.st5, st.st6);
	sock_send(pl->sock, buff);
}

int cl_play_inventory(char *id, player_t pl, char *s)
{
	(void)id;
	if (strcasecmp(s, "Inventory"))
		return (0);
	pl->tick_act = 1;
	pl->resume = rs_play_inventory;
	return (1);
}