/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** play_die
*/

#include <stdio.h>

#include <socket.h>
#include <log.h>

#include "server.h"

static void send(char *id, player_t pl)
{
	char buff[512] = {0};

	sprintf(buff, "pnw %s %d %d %d %d %s\n", id, pl->x, pl->y, pl->o,
		pl->lvl, pl->team);
	graph_send(buff);
}

static int insert_player(char *team, long nb)
{
	player_t pl = 0;
	char *id = 0;
	char buff[512] = {0};

	foreach(GM.registered as k, v)
		if (!strcasecmp(((player_t)v)->team, team)) {
			pl = v;
			dic_del(GM.registered, (id = k));
			break;
		}
	if (!pl)
		return (0);
	dic_set(GM.players, id, pl);
	dic_set(GM.game->map[pl->x + pl->y * GM.game->args->width].players, id,
		pl);
	sprintf(buff, "%ld\n%d %d\n", (nb & 0xFFFFFFFF) - (nb >> 32) - 1,
		GM.game->args->width, GM.game->args->width);
	sock_send(pl->sock, buff);
	send(id, pl);
	return (1);
}

void cl_play_die(char *id, player_t pl)
{
	long cnb = (long)dic_get(GM.game->teams, pl->team);
	char buff[512] = {0};

	sock_send(pl->sock, "dead\n");
	ilog("%s died", (long)id);
	can_try(pl->sock);
	sock_close(pl->sock);
	if (!insert_player(pl->team, cnb))
		dic_set(GM.game->teams, pl->team, (void*)(cnb >= (1L << 32) ?
		cnb - (1L << 32) : cnb));
	dic_del(GM.players, id);
	dic_del(GM.game->map[pl->x + pl->y * GM.game->args->width].players, id);
	sprintf(buff, "pdi %s\n", id);
	graph_send(buff);
}

void delete_player(char *id, player_t pl)
{
	long cnb = (long)dic_get(GM.game->teams, pl->team);
	char buff[512] = {0};

	ilog("%s aborted", (long)id);
	sock_close(pl->sock);
	if (!insert_player(pl->team, cnb))
		dic_set(GM.game->teams, pl->team, (void*)(cnb >= (1L << 32) ?
		cnb - (1L << 32) : cnb));
	dic_del(GM.players, id);
	dic_del(GM.game->map[pl->x + pl->y * GM.game->args->width].players, id);
	sprintf(buff, "pdi %s\n", id);
	graph_send(buff);
}