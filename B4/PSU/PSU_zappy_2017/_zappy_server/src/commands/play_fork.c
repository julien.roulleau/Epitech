/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** play_fork
*/

#include <stdio.h>

#include <garbage_collector.h>

#include "server.h"

static void send(char *id, egg_t egg)
{
	char buff[512] = {0};

	sprintf(buff, "pfk %s\nenw %d %s %d %d\n", id, GM.last_id + 1,
		egg->owner, egg->x, egg->y);
	graph_send(buff);
}

static void rs_play_fork(char *id, player_t pl)
{
	egg_t egg = new_egg(pl, id);

	pl->resume = 0;
	send(id, egg);
	dic_set(GM.eggs, gc_itoa(++(GM.last_id)), egg);
	sock_send(pl->sock, "ok\n");
}

int cl_play_fork(char *id, player_t pl, char *s)
{
	(void)id;
	if (strcasecmp(s, "Fork"))
		return (0);
	pl->tick_act = 42;
	pl->resume = rs_play_fork;
	return (1);
}
