/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** play_set
*/

#include <stdio.h>

#include "server.h"

static int get_stone(char *s)
{
	if (!strcasecmp(s, "food"))
		return (0);
	if (!strcasecmp(s, "linemate"))
		return (1);
	if (!strcasecmp(s, "deraumere"))
		return (2);
	if (!strcasecmp(s, "sibur"))
		return (3);
	if (!strcasecmp(s, "mendiane"))
		return (4);
	if (!strcasecmp(s, "phiras"))
		return (5);
	if (!strcasecmp(s, "thystame"))
		return (6);
	return (-1);
}

static int set_stone(int st, player_t pl, int pos)
{
	if (st == 0 && pl->inv.food > 0)
		return (pl->inv.food--, ++GM.game->map[pos].stones.food);
	if (st == 1 && pl->inv.st1 > 0)
		return (pl->inv.st1--, ++GM.game->map[pos].stones.st1);
	if (st == 2 && pl->inv.st2 > 0)
		return (pl->inv.st2--, ++GM.game->map[pos].stones.st2);
	if (st == 3 && pl->inv.st3 > 0)
		return (pl->inv.st3--, ++GM.game->map[pos].stones.st3);
	if (st == 4 && pl->inv.st4 > 0)
		return (pl->inv.st4--, ++GM.game->map[pos].stones.st4);
	if (st == 5 && pl->inv.st5 > 0)
		return (pl->inv.st5--, ++GM.game->map[pos].stones.st5);
	if (st == 6 && pl->inv.st6 > 0)
		return (pl->inv.st6--, ++GM.game->map[pos].stones.st6);
	return (0);
}

static void send(char *id, int st)
{
	char buff[512] = {0};

	sprintf(buff, "pgt %s %i\n", id, st);
	graph_send(buff);
}

static void rs_play_set(char *id, player_t pl)
{
	int st = get_stone(pl->last_cmd);
	int ret = set_stone(st, pl, pl->x + pl->y * GM.game->args->width);

	pl->resume = 0;
	send(id, st);
	sock_send(pl->sock, ret ? "ok\n" : "ko\n");
}

int cl_play_set(char *id, player_t pl, char *s)
{
	(void)id;
	if (strncasecmp(s, "Set ", 4) || get_stone(s + 4) < 0)
		return (0);
	pl->tick_act = 7;
	pl->resume = rs_play_set;
	pl->last_cmd = s + 4;
	return (1);
}
