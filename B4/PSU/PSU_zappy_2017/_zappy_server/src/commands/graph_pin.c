/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** graph_pin.c
*/

#include <stdio.h>
#include "server.h"

int cl_graph_pin(socket_t so, char *cmd)
{
	char buff[512] = {0};
	player_t player = NULL;

	if (strncasecmp("pin ", cmd, 4) != 0)
		return (0);
	cmd += 4;
	if (!*cmd)
		return (sock_send(so, "sbp\n"), 1);
	player = dic_get(GM.players, cmd);
	if (!player)
		return (sock_send(so, "sbp\n"), 1);
	sprintf(buff, "pin %s %d %d %d %d %d %d %d %d %d\n",
		cmd, player->x, player->y, player->inv.food,
		player->inv.st1, player->inv.st2, player->inv.st3,
		player->inv.st4, player->inv.st5, player->inv.st6);
	sock_send(so, buff);
	return (1);
}
