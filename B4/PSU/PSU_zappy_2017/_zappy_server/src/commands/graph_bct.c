/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** graph_bct.c
*/

#include <stdio.h>
#include <stdlib.h>
#include "server.h"

int cl_graph_bct(socket_t so, char *cmd)
{
	char buff[512] = {0};
	int x = 0;
	int y = 0;
	char *tmp;
	stones_t stones;

	if (strncasecmp("bct ", cmd, 4) != 0)
		return (0);
	cmd += 4;
	tmp = index(cmd, ' ');
	if (!*cmd || !tmp)
		return (sock_send(so, "sbp\n"), 1);
	x = atoi(cmd) % GM.game->args->width;
	y = atoi(tmp) % GM.game->args->height;
	stones = GM.game->map[x + y * GM.game->args->width].stones;
	sprintf(buff, "bct %d %d %d %d %d %d %d %d %d\n", x, y,
		stones.food, stones.st1, stones.st2, stones.st3, stones.st4,
		stones.st5, stones.st6);
	sock_send(so, buff);
	return (1);
}
