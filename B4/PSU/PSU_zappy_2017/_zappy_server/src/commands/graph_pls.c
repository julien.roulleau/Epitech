/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** graph_pls.c
*/

#include "server.h"

int cl_graph_pls(socket_t so, char *cmd)
{
	char buff[2048] = {0};

	if (strcasecmp("pls", cmd) != 0)
		return (0);
	foreach(GM.players as k, v) {
		strcpy(buff, "pls ");
		strcat(strcat(buff, k), " ");
		buff[strlen(buff) - 1] = '\n';
		sock_send(so, buff);
	}
	return (1);
}
