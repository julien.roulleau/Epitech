/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** graph_ppo.c
*/

#include <stdio.h>
#include "server.h"

int cl_graph_ppo(socket_t so, char *cmd)
{
	char buff[512] = {0};
	player_t player = NULL;

	if (strncasecmp("ppo ", cmd, 4) != 0)
		return (0);
	cmd += 4;
	if (!*cmd)
		return (sock_send(so, "sbp\n"), 1);
	player = dic_get(GM.players, cmd);
	if (!player)
		return (sock_send(so, "sbp\n"), 1);
	sprintf(buff, "ppo %s %d %d %d\n",
		cmd, player->x, player->y, player->o);
	sock_send(so, buff);
	return (1);
}
