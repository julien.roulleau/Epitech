/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** graph_tna.c
*/

#include "server.h"

int cl_graph_tna(socket_t so, char *cmd)
{
	char buff[2048] = {0};

	if (strcasecmp("tna", cmd) != 0)
		return (0);
	foreach(GM.game->teams as k, v) {
		strcpy(buff, "tna ");
		strcat(strcat(buff, k), " ");
		buff[strlen(buff) - 1] = '\n';
		sock_send(so, buff);
	}
	return (1);
}
