/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** play_incantation
*/

#include <stdio.h>

#include "server.h"

static void rs_play_incantation(char *id, player_t pl)
{
	char buff[512] = {0};
	int pos = pl->x + pl->y * GM.game->args->width;

	pl->resume = 0;
	if (!incantation_check(pl) || !dic_get(GM.incantations, id))
		sock_send(pl->sock, "ko\n");
	else {
		sprintf(buff, "Current level: %d\n", pl->lvl + 1);
		foreach(GM.game->map[pos].players as k, v) {
			((player_t)v)->lvl++;
			sock_send(((player_t)v)->sock, buff);
		}
		GM.game->map[pos].stones = (stones_t){
			GM.game->map[pos].stones.food, 0, 0, 0, 0, 0, 0};
		dic_del(GM.incantations, id);
	}
}

int cl_play_incantation(char *id, player_t pl, char *s)
{
	if (strcasecmp(s, "Incantation"))
		return (0);
	if (!incantation_check(pl))
		sock_send(pl->sock, "ko\n");
	else {
		sock_send(pl->sock, "Elevation underway\n");
		dic_set(GM.incantations, id, pl);
	}
	pl->tick_act = 300;
	pl->resume = rs_play_incantation;
	return (1);
}
