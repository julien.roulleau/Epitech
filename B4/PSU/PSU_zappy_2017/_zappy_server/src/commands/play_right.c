/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** play_right
*/

#include <stdio.h>

#include "server.h"

static void send(char *id, int x, int y, int o)
{
	char buff[512] = {0};

	sprintf(buff, "ppo %s %d %d %d\n", id, x, y, o);
	graph_send(buff);
}

static void rs_play_right(char *id, player_t pl)
{
	(void)id;
	pl->resume = 0;
	pl->o = (pl->o + 3) % 4;
	send(id, pl->x, pl->y, pl->o);
	sock_send(pl->sock, "ok\n");
}

int cl_play_right(char *id, player_t pl, char *s)
{
	(void)id;
	if (strcasecmp(s, "Right"))
		return (0);
	pl->tick_act = 7;
	pl->resume = rs_play_right;
	return (1);
}