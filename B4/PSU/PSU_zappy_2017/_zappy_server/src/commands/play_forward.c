/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** play_forward
*/

#include <stdio.h>

#include "server.h"

static void send(char *id, int x, int y, int o)
{
	char buff[512] = {0};

	sprintf(buff, "ppo %s %d %d %d\n", id, x, y, o);
	graph_send(buff);
}

static void rs_play_forward(char *id, player_t pl)
{
	int sz[2] = {GM.game->args->width, GM.game->args->height};

	dic_del(GM.game->map[pl->x + pl->y * sz[0]].players, id);
	pl->resume = 0;
	switch (pl->o) {
	case 0:
		pl->y = (pl->y + sz[1] - 1) % sz[1];
		break;
	case 1:
		pl->x = (pl->x + sz[0] - 1) % sz[0];
		break;
	case 2:
		pl->y = (pl->y + 1) % sz[1];
		break;
	case 3:
		pl->x = (pl->x + 1) % sz[0];
	}
	dic_set(GM.game->map[pl->x + pl->y * sz[0]].players, id, pl);
	send(id, pl->x, pl->y, pl->o);
	sock_send(pl->sock, "ok\n");
}

int cl_play_forward(char *id, player_t pl, char *s)
{
	(void)id;
	if (strcasecmp(s, "Forward"))
		return (0);
	pl->tick_act = 7;
	pl->resume = rs_play_forward;
	return (1);
}