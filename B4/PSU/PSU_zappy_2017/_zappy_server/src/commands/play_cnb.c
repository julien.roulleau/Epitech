/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** play_cnb
*/

#include <stdio.h>

#include "server.h"

int cl_play_cnb(char *id, player_t pl, char *s)
{
	char buff[512] = {0};
	long nb = (long)dic_get(GM.game->teams, pl->team);

	(void)id;
	if (strcasecmp(s, "Connect_nbr"))
		return (0);
	pl->tick_act = 0;
	pl->resume = 0;
	sprintf(buff, "%ld\n", (nb & 0xFFFFFFFF) - (nb >> 32));
	sock_send(pl->sock, buff);
	return (1);
}
