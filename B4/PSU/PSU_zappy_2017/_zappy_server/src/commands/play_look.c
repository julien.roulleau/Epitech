/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** play_look
*/

#include <stdio.h>

#include <garbage_collector.h>

#include "server.h"

static void dump_tile(char *buff, int x, int y)
{
	int s[2] = {GM.game->args->width, GM.game->args->height};
	int pos = ((x + s[0]) % s[0]) + ((y + s[1]) % s[1]) * s[0];
	int i;

	foreach(GM.game->map[pos].players as k, v)
		strcat(buff, " player");
	for (i = 0; i < GM.game->map[pos].stones.food; i++)
		strcat(buff, " food");
	for (i = 0; i < GM.game->map[pos].stones.st1; i++)
		strcat(buff, " linemate");
	for (i = 0; i < GM.game->map[pos].stones.st2; i++)
		strcat(buff, " deraumere");
	for (i = 0; i < GM.game->map[pos].stones.st3; i++)
		strcat(buff, " sibur");
	for (i = 0; i < GM.game->map[pos].stones.st4; i++)
		strcat(buff, " mendiane");
	for (i = 0; i < GM.game->map[pos].stones.st5; i++)
		strcat(buff, " phiras");
	for (i = 0; i < GM.game->map[pos].stones.st6; i++)
		strcat(buff, " thystame");
}

static int get_offset(int o, int n)
{
	if (n >= 3)
		o++;
	if (o == 0)
		return (n % 2 ? 0 : -1);
	if (o == 1)
		return (n % 2 ? -1 : 0);
	if (o == 2)
		return (n % 2 ? 0 : 1);
	return (n % 2 ? 1 : 0);
}

static void rs_play_look(char *id, player_t pl)
{
	char buff[8192] = {'[', 0};
	int i;
	int j;
	int fx = get_offset(pl->o, 1);
	int fy = get_offset(pl->o, 2);
	int lx = get_offset(pl->o, 3);
	int ly = get_offset(pl->o, 4);

	(void)id;
	pl->resume = 0;
	for (i = 0; i <= pl->lvl; i++)
		for (j = i; j >= -i; j--) {
			if (i)
				strcat(buff, ",");
			dump_tile(buff, pl->x + i * fx + j * lx, pl->y + i * fy
				+ j * ly);
		}
	strcat(buff, "]\n");
	sock_send(pl->sock, buff);
}

int cl_play_look(char *id, player_t pl, char *s)
{
	(void)id;
	if (strcasecmp(s, "Look"))
		return (0);
	pl->tick_act = 7;
	pl->resume = rs_play_look;
	return (1);
}
