/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** play_take
*/

#include <stdio.h>

#include "server.h"

static int get_stone(char *s)
{
	if (!strcasecmp(s, "food"))
		return (0);
	if (!strcasecmp(s, "linemate"))
		return (1);
	if (!strcasecmp(s, "deraumere"))
		return (2);
	if (!strcasecmp(s, "sibur"))
		return (3);
	if (!strcasecmp(s, "mendiane"))
		return (4);
	if (!strcasecmp(s, "phiras"))
		return (5);
	if (!strcasecmp(s, "thystame"))
		return (6);
	return (-1);
}

static int set_stone(int st, player_t pl, int pos)
{
	if (st == 0 && GM.game->map[pos].stones.food > 0)
		return (GM.game->map[pos].stones.food--,
			++pl->inv.food);
	if (st == 1 && GM.game->map[pos].stones.st1 > 0)
		return (GM.game->map[pos].stones.st1--, ++pl->inv.st1);
	if (st == 2 && GM.game->map[pos].stones.st2 > 0)
		return (GM.game->map[pos].stones.st2--, ++pl->inv.st2);
	if (st == 3 && GM.game->map[pos].stones.st3 > 0)
		return (GM.game->map[pos].stones.st3--, ++pl->inv.st3);
	if (st == 4 && GM.game->map[pos].stones.st4 > 0)
		return (GM.game->map[pos].stones.st4--, ++pl->inv.st4);
	if (st == 5 && GM.game->map[pos].stones.st5 > 0)
		return (GM.game->map[pos].stones.st5--, ++pl->inv.st5);
	if (st == 6 && GM.game->map[pos].stones.st6 > 0)
		return (GM.game->map[pos].stones.st6--, ++pl->inv.st6);
	return (0);
}

static void send(char *id, int st)
{
	char buff[512] = {0};

	sprintf(buff, "pgt %s %i\n", id, st);
	graph_send(buff);
}

static void rs_play_take(char *id, player_t pl)
{
	int st = get_stone(pl->last_cmd);
	int ret = set_stone(st, pl, pl->x + pl->y * GM.game->args->width);

	pl->resume = 0;
	send(id, st);
	sock_send(pl->sock, ret ? "ok\n" : "ko\n");
}

int cl_play_take(char *id, player_t pl, char *s)
{
	(void)id;
	if (strncasecmp(s, "Take ", 5) || get_stone(s + 5) < 0)
		return (0);
	pl->tick_act = 7;
	pl->resume = rs_play_take;
	pl->last_cmd = s + 5;
	return (1);
}
