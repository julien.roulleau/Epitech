/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** queue_team
*/

#include <stdio.h>

#include <garbage_collector.h>
#include <log.h>

#include "server.h"

static void send(char *id, player_t pl)
{
	char buff[512] = {0};

	sprintf(buff, "pnw %s %d %d %d %d %s\n", id, pl->x, pl->y, pl->o,
		pl->lvl, pl->team);
	graph_send(buff);
}

static int cl_queue_team_graph(char *id, socket_t so)
{
	dic_del(GM.queue, id);
	dic_set(GM.graphs, id, so);
	return (1);
}

static int cl_queue_team_new(char *id, socket_t so, char *s)
{
	long nb = ((long)dic_get(GM.game->teams, s) ?:
		(long)dic_get(GM.game->args->names, s) ?:
		(long)GM.game->args->cnb);
	player_t pl;
	char buff[512] = {0};

	dic_del(GM.queue, id);
	pl = new_player(so, s);
	if ((nb >> 32) >= (nb & 0xFFFFFFFF))
		return (dic_set(GM.registered, id, pl), 0);
	dic_set(GM.players, id, pl);
	dic_set(GM.game->map[pl->x + pl->y * GM.game->args->width].players, id,
		pl);
	dic_set(GM.game->teams, s, (void*)((long)nb + (1L << 32)));
	sprintf(buff, "%ld\n%d %d\n", (nb & 0xFFFFFFFF) - (nb >> 32) - 1,
		GM.game->args->width, GM.game->args->width);
	sock_send(so, buff);
	send(id, pl);
	return (1);
}

int cl_queue_team(char *id, socket_t so, char *s)
{
	char *s2 = s;

	if (!strcmp(s, "GRAPH"))
		return (cl_queue_team_graph(id, so));
	if (dic_count(GM.game->args->names)) {
		foreach(GM.game->args->names as k, v)
			if (!strcmp(s, k))
				return (cl_queue_team_new(id, so, s));
		return (0);
	}
	if (strncasecmp(s, "Team", 4))
		return (0);
	s += 3;
	while (*++s && *s >= '0' && *s <= '9');
	if (!*s)
		return (cl_queue_team_new(id, so, s2));
	return (0);
}