/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** play_broadcast
*/

#include <stdio.h>

#include "server.h"

static void send(char *id, char *msg)
{
	char buff[1024] = {0};

	sprintf(buff, "pbc %s %s\n", id, msg);
	graph_send(buff);
}

static int get_angle(int x, int y, player_t p2)
{
	int ret = 0;
	int osx = (WIDTH / 2 - x);
	int osy = (HEIGHT / 2 - y);

	x = (x + osx) % WIDTH - (p2->x + osx) % WIDTH;
	y = (y + osy) % HEIGHT - (p2->y + osy) % HEIGHT;
	if (x == 0 && y == 0)
		return (0);
	else if (ABS(x) == ABS(y)) {
		ret = x < 0 && y < 0 ? 2 : ret;
		ret = x < 0 && y > 0 ? 4 : ret;
		ret = x > 0 && y > 0 ? 6 : ret;
		ret = x > 0 && y < 0 ? 8 : ret;
	} else {
		ret = y < 0 && ABS(x) < ABS(y) ? 1 : ret;
		ret = x < 0 && ABS(y) < ABS(x) ? 3 : ret;
		ret = y > 0 && ABS(x) < ABS(y) ? 5 : ret;
		ret = x > 0 && ABS(y) < ABS(x) ? 7 : ret;
	}
	return (ret - 2 * p2->o <= 0 ? 8 + (ret - 2 * p2->o) : ret - 2 * p2->o);
}

static void rs_play_broadcast(char *id, player_t pl)
{
	char buff[1024] = {0};

	pl->resume = 0;
	send(id, pl->last_cmd);
	foreach (GM.players as k, v)
		if (strcmp(k, id)) {
			sprintf(buff, "message %d, %s\n", get_angle(pl->x,
				pl->y, v), pl->last_cmd);
			sock_send(((player_t)v)->sock, buff);
		}
	sock_send(pl->sock, "ok\n");
}

int cl_play_broadcast(char *id, player_t pl, char *s)
{
	(void)id;
	if (strncasecmp(s, "Broadcast ", 10))
		return (0);
	pl->tick_act = 7;
	pl->resume = rs_play_broadcast;
	pl->last_cmd = s + 10;
	return (1);
}
