/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** graph_sgt.c
*/

#include <stdio.h>
#include "server.h"

int cl_graph_sgt(socket_t so, char *cmd)
{
	char buff[512] = {0};

	if (strcasecmp("sgt", cmd) != 0)
		return (0);
	sprintf(buff, "sgt %d\n", GM.game->args->freq);
	sock_send(so, buff);
	return (1);
}