/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** play_eject
*/

#include <stdio.h>

#include "server.h"

static void send(char *id, player_t pl, int o)
{
	char buff[512] = {0};
	int sz[2] = {GM.game->args->width, GM.game->args->height};

	sprintf(buff, "eject: %d\n", (o + 2) % 4);
	sock_send(pl->sock, buff);
	if (o == 0)
		pl->y = (pl->y + sz[1] - 1) % sz[1];
	if (o == 1)
		pl->x = (pl->x + sz[0] - 1) % sz[0];
	if (o == 2)
		pl->y = (pl->y + 1) % sz[1];
	if (o == 3)
		pl->x = (pl->x + 1) % sz[0];
	sprintf(buff, "ppo %s %d %d %d\n", id, pl->x, pl->y, pl->o);
	graph_send(buff);
}

static void rs_play_eject(char *id, player_t pl)
{
	int sz[2] = {GM.game->args->width, GM.game->args->height};
	int pos[2] = {pl->x + pl->y * sz[0], pl->x + pl->y * sz[0]};

	if (pl->o == 0)
			pos[1] = (pos[1] + (sz[1] - 1) * sz[0]) % sz[1];
	if (pl->o == 1)
			pos[1] = (pos[1] + sz[0] - 1) % sz[0];
	if (pl->o == 2)
			pos[1] = (pos[1] + sz[0]) % sz[1];
	if (pl->o == 3)
			pos[1] = (pos[1] + 1) % sz[0];
	pl->resume = 0;
	foreach(GM.game->map[pos[0]].players as k, v)
		if (strcmp(k, id)) {
			dic_set(GM.game->map[pos[1]].players, k, v);
			dic_del(GM.game->map[pos[0]].players, k);
			send(k, v, pl->o);
		}
	sock_send(pl->sock, "ok\n");
}

int cl_play_eject(char *id, player_t pl, char *s)
{
	(void)id;
	if (strcasecmp(s, "Eject"))
		return (0);
	pl->tick_act = 7;
	pl->resume = rs_play_eject;
	return (1);
}
