/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** player
*/

#include <stdio.h>

#include <garbage_collector.h>
#include <dic.h>

#include "server.h"

static egg_t get_egg(char *team)
{
	char buff[512] = {0};

	foreach(GM.eggs as k, v)
		if (((egg_t)v)->ticks <= 0 && !strcmp(((egg_t)v)->team, team)) {
			sprintf(buff, "ebo %s\n", (char*)k);
			graph_send(buff);
			dic_del(GM.eggs, k);
			return (v);
		}
	return (0);
}

player_t new_player(socket_t sock, char *team)
{
	player_t pl = calloc(sizeof(struct s_player), 1);
	egg_t egg = get_egg(team);

	pl->x = egg ? egg->x : (((long)pl >> 16) & 0xFFFF) % GM.game->args->width;
	pl->y = egg ? egg->y : ((long)pl & 0xFFFF) % GM.game->args->height;
	pl->o = (long)(pl->sock) & 4;
	pl->lvl = 1;
	pl->sock = sock;
	pl->inv.food = 10;
	pl->tick_food = 126 + (egg ? egg->ticks : 0);
	while (pl->tick_food <= 0) {
		pl->tick_food += 126;
		pl->inv.food--;
	}
	pl->team = team;
	free(egg);
	return (pl);
}

void add_player(socket_t sock)
{
	dic_set(GM.queue, gc_itoa(++(GM.last_id)), sock);
	sock_send(sock, "WELCOME\n");
	sock_update(sock);
}