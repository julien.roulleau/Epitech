/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** main
*/

#include <stdlib.h>
#include <signal.h>

#include <socket.h>
#include <log.h>
#include <sys/select.h>

#include "server.h"

global_t GM = {0, 0, 0, 0, 0, 0, 0, 0, 0};
int (*CMD_PLAY[])(char*, player_t, char*) = {
	cl_play_broadcast,
	cl_play_cnb,
	cl_play_eject,
	cl_play_fork,
	cl_play_forward,
	cl_play_incantation,
	cl_play_inventory,
	cl_play_left,
	cl_play_look,
	cl_play_right,
	cl_play_set,
	cl_play_take,
	0
};
int (*CMD_GRAPH[])(socket_t, char*) = {
	cl_graph_bct,
	cl_graph_msz,
	cl_graph_pin,
	cl_graph_pls,
	cl_graph_plt,
	cl_graph_plv,
	cl_graph_ppo,
	cl_graph_sgt,
	cl_graph_sst,
	cl_graph_tna,
	0
};

static void sighandler(int sig)
{
	(void)sig;
	ierr("Signal received: %d", sig);
	err("Exiting");
	exit(0);
}

int main()
{
	fd_set s;
	struct timeval tv = {0, 0};

	signal(SIGINT,  sighandler);
	signal(SIGQUIT, sighandler);
	signal(SIGABRT, sighandler);
	signal(SIGKILL, sighandler);
	signal(SIGSEGV, sighandler);
	while (1) {
		FD_ZERO(&s);
		FD_SET(GM.sock->fd, &s);
		if (select(GM.sock->fd + 1, &s, 0, 0, &tv))
			add_player(sock_accept(GM.sock));
		tick_run();
	}
	return (0);
}
