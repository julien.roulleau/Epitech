/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** game
*/

#include <stdio.h>
#include <garbage_collector.h>
#include <socket.h>

#include "server.h"

#define CHECK_UPDATE(so, yes, no) if (can_try(so)) yes; else no

void make_game(args_t args)
{
	GM.game = calloc(sizeof(struct s_game), 1);
	GM.game->args = args;
	foreach(args->names as k, v)
		dic_set(args->names, k, (void*)((long)args->cnb));
	GM.game->map = calloc(sizeof(struct s_tiles), args->width *
		args->height);
	for (int i = 0; i < args->width * args->height; i++)
		GM.game->map[i].players = new_dic(100);
	map_generate();
	GM.game->teams = new_dic(50);
	GM.queue = new_dic(100);
	GM.registered = new_dic(50);
	GM.players = new_dic(100);
	GM.graphs = new_dic(50);
	GM.eggs = new_dic(100);
	GM.incantations = new_dic(50);
	GM.sock = new_socket();
	sock_bind(GM.sock, args->port);
	sock_listen(GM.sock, 10);
}

void parse_queue(char *id, socket_t so)
{
	char *s;

	if ((s = sock_recv(so)))
		cl_queue_team(id, so, s);
}

void parse_player(char *id, player_t pl)
{
	char *s;
	int i = 0;

	if (--(pl->tick_food) <= 0 && (pl->tick_food += 126))
		if (--(pl->inv.food) <= 0)
			return (cl_play_die(id, pl));
	if (pl->tick_act > 0) {
		pl->tick_act--;
		return;
	} else if (pl->resume)
		pl->resume(id, pl);
	if ((s = sock_recv(pl->sock)))
		for (i = 0; CMD_PLAY[i] && !CMD_PLAY[i](id, pl, s); i++);
	if (!CMD_PLAY[i])
		sock_send(pl->sock, "ko\n");
}

void parse_graph(socket_t so)
{
	char *s;
	int i;

	while ((s = sock_recv(so))) {
		for (i = 0; CMD_GRAPH[i] && !CMD_GRAPH[i](so, s); i++);
		if (!CMD_GRAPH[i])
			sock_send(so, "suc\n");
	}
}

void game_loop()
{
	foreach(GM.eggs as eid, egg)
		egg_tick(eid, egg);
	foreach(GM.queue as qid, qso)
		CHECK_UPDATE(qso, parse_queue(qid, qso),
			dic_del(GM.queue, qid));
	foreach(GM.players as pid, pl)
		CHECK_UPDATE(((player_t)pl)->sock, parse_player(pid, pl),
			delete_player(pid, pl));
	foreach(GM.graphs as k1, v1)
		CHECK_UPDATE(v1, parse_graph(v1), dic_del(GM.graphs, k1));
	foreach(GM.incantations as k, v)
		if (!incantation_check(v))
			dic_del(GM.incantations, k);
	map_tick();
}