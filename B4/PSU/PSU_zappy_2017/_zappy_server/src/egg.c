/*
** EPITECH PROJECT, 2018
** zappy
** File description:
** egg
*/

#include <stdio.h>

#include <garbage_collector.h>

#include "server.h"

egg_t new_egg(player_t pl, char *id)
{
	egg_t egg = calloc(sizeof(struct s_egg), 1);

	egg->x = pl->x;
	egg->y = pl->y;
	egg->owner = id;
	egg->team = pl->team;
	egg->ticks = 600;
	return (egg);
}

void egg_tick(char *id, egg_t egg)
{
	char buff[512] = {0};
	long team;

	if (egg->ticks > -1260)
		egg->ticks--;
	if (egg->ticks == 0) {
		sprintf(buff, "eht %s\n", id);
		graph_send(buff);
		team = (long)dic_get(GM.game->teams, egg->team);
		dic_set(GM.game->teams, egg->team, (void*)(++team));
	}
	if (egg->ticks == -1260) {
		sprintf(buff, "edi %s\n", id);
		graph_send(buff);
		team = (long)dic_get(GM.game->teams, egg->team);
		dic_set(GM.game->teams, egg->team, (void*)(--team));
		dic_del(GM.eggs, id);
		free(egg);
	}
}