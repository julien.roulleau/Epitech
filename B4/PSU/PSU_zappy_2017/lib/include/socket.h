/*
** EPITECH PROJECT, 2018
** socket
** File description:
** socket
*/

#ifndef SOCKET_H_
	#define SOCKET_H_

	#include "dic.h"

typedef struct s_socket {
	int fd;
	unsigned char send_pos:5;
	unsigned char recv_pos:5;
	char *send_tmp;
	char *recv_tmp;
	char *send_queue[32];
	char *recv_queue[32];
} *socket_t;

socket_t new_socket();

void sock_send(socket_t, char*);
void sock_close(socket_t);

int sock_bind(socket_t, short);
int sock_connect(socket_t, char*, short);
int sock_listen(socket_t, int);
int sock_update(socket_t);
int sock_update_all(dic_t);

char *sock_recv(socket_t);

socket_t sock_accept(socket_t);

#endif /* !SOCKET_H_ */
