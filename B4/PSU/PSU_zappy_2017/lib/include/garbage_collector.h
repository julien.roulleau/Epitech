/*
** EPITECH PROJECT, 2018
** gc
** File description:
** garbage_collector
*/

#ifndef GARBAGE_H__
# define GARBAGE_H__

# include <stdlib.h>

# define NULL ((void *)0)
# define METADATA_SIZE (sizeof(size_t) + sizeof(void*))
# define _freeable __attribute__((cleanup(gc_pfree)))
# define _closeable __attribute__((cleanup(gc_pclose)))

# define malloc(x) gc_malloc(x)
# define calloc(x, y) gc_calloc(x, y)
# define realloc(x, y) gc_realloc(x, y)
# define free(x) gc_free(x)
# define open(x, y, z) gc_open((x), (y), (z) + 0)
# define creat(x, y) gc_open((x), O_CREAT|O_WRONLY|O_TRUNC, (y) + 0)
# define socket(x, y, z) gc_socket((x), (y), (z))

typedef char* string;

union u_ptr {
	void **pp;
	size_t *ps;
	void *pv;
	char *pc;
	int *pi;
	unsigned long int ul;
};

extern union u_ptr g_garbageCollection;
extern void *g_socketCollection;

void gc_free(void*);
void gc_pfree(char**);
void gc_close(int);
void gc_pclose(int *fd);
void gc_closeAll() __attribute__((destructor(102)));
void gc_freeAll() __attribute__((destructor(101)));
void new_garbage_collector() __attribute__((constructor(101)));

int gc_open(const char*, int, mode_t);
int gc_socket(int, int, int);

void *gc_calloc(size_t, size_t);
void *gc_malloc(size_t);
void *gc_memcpy(void*, const void*, size_t);
void *gc_memmove(void*, const void*, size_t);
void *gc_memset(void*, int, size_t);
void *gc_realloc(void*, size_t);
void *gc_rel_realloc(void*, size_t);

string gc_itoa(int);
string gc_strdup(string str);
string gc_join(string, string);
string gc_join_sc(string, char);
string gc_join_scs(string, char, string);

size_t gc_size(void*);

#endif
