/*
** EPITECH PROJECT, 2018
** log
** File description:
** log
*/

#ifndef LOG_H_
# define LOG_H_

#include <string.h>
#include <unistd.h>
#include <stdio.h>

int logv(char*, int);
int logi(char*, long, int);
int lognull();

# ifdef DEBUG
#  define log(x) logv(x, 0)
#  define war(x) logv(x, 1)
#  define err(x) logv(x, 2)
#  define suc(x) logv(x, 3)
#  define ilog(x, y) logi(x, y, 0)
#  define iwar(x, y) logi(x, y, 1)
#  define ierr(x, y) logi(x, y, 2)
#  define isuc(x, y) logi(x, y, 3)
# else
#  define log(x) lognull()
#  define war(x) lognull()
#  define err(x) lognull()
#  define suc(x) lognull()
#  define ilog(x, y) lognull()
#  define iwar(x, y) lognull()
#  define ierr(x, y) lognull()
#  define isuc(x, y) lognull()
# endif

#endif
