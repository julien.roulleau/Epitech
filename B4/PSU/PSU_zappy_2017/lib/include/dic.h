/*
** EPITECH PROJECT, 2018
** dic
** File description:
** dic
*/

#ifndef DIC_H__
# define DIC_H__

#include <string.h>

#define true (1)
#define false (0)

typedef char bool;
typedef char* string;

typedef struct s_dic_elem {
	unsigned long hash;
	string key;
	void *value;
} dic_elem_t;

typedef struct s_dic {
	int size;
	dic_elem_t *dic;
} *dic_t;

dic_t new_dic(int);

void dic_unfragment(dic_t);

int dic_index(dic_t, string);
int dic_add(dic_t, string, void*);
int dic_set(dic_t, string, void*);
int dic_del(dic_t, string);
int dic_unserialize(dic_t, string*);
int dic_count(dic_t);

unsigned long checksum(char*);

void *dic_get(dic_t, string);
void *dic_find(dic_t, dic_t, string);
#define as ,
#define foreach_(d, k, v) void *k = 0, *v = 0;\
			for (dic_elem_t *e = 0; (e = dic_next(d, e)) && \
				((k = e->key) || 1) && ((v = e->value) || 1);)
#define foreach(x, v) foreach_(x, v)
void *dic_begin(dic_t);
void *dic_next(dic_t, void*);

string *dic_serialize(dic_t);

string join(string, char, string);

#endif
