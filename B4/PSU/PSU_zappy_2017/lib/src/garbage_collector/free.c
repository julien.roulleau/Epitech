/*
** EPITECH PROJECT, 2018
** gc
** File description:
** free
*/

#include "garbage_collector.h"

#undef free

void gc_free(void *f)
{
	union u_ptr tmp;
	union u_ptr ptr;
	union u_ptr p;

	if (!f)
		return;
	p.pv = f;
	p.ul -= METADATA_SIZE;
	tmp.pv = ptr.pv = g_garbageCollection.pv;
	while (ptr.pv && p.pv != ptr.pv) {
		tmp.pv = ptr.pv;
		ptr.pv = ptr.pp[0];
	}
	if (!ptr.pv)
		return;
	if (tmp.pv == ptr.pv)
		g_garbageCollection.pv = g_garbageCollection.pp[0];
	else
		tmp.pp[0] = ptr.pp[0];
	free(ptr.pv);
}

void gc_pfree(char **f)
{
	if (f && *f) {
		gc_free(*f);
		*f = 0;
	}
}