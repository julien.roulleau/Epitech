/*
** EPITECH PROJECT, 2018
** gc
** File description:
** garbage_collector
*/

#include "garbage_collector.h"
#include "log.h"

union u_ptr g_garbageCollection = {.pv = 0};
void *g_socketCollection = 0;

void new_garbage_collector()
{
	log("Initializing log function");
	war("Initializing war function");
	err("Initializing err function");
	suc("Initializing suc function");
	suc("All succeed");
	log("Ready to start…");
	log("Initializing garbage collector");
	g_garbageCollection.pp = 0;
	g_garbageCollection.pv = 0;
	g_garbageCollection.pc = 0;
	g_garbageCollection.pi = 0;
	g_garbageCollection.ul = 0;
	suc("garbage collector initialisation successful");
}
