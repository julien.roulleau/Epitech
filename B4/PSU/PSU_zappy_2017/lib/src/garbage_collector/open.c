/*
** EPITECH PROJECT, 2018
** gc
** File description:
** open
*/

#include <fcntl.h>

#include "garbage_collector.h"

#undef open

int gc_open(const char *s, int flags, mode_t mode)
{
	int ret = open(s, flags, mode);
	void *old = g_socketCollection;

	g_socketCollection = gc_malloc(sizeof(void*) + sizeof(int));
	*(void**)g_socketCollection = old;
	*(int*)(g_socketCollection + sizeof(void*)) = ret;
	return (ret);
}