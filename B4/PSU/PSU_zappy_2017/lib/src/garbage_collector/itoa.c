/*
** EPITECH PROJECT, 2018
** gc
** File description:
** itoa
*/

#include "garbage_collector.h"

string gc_itoa(int nb)
{
	string ret;
	int i = 1;
	int c = 0;
	int neg = 0;

	ret = gc_calloc(13, 1);
	if (nb < 0) {
		neg = 1;
		nb = -nb;
	}
	while ((nb / i) >= 10)
		i *= 10;
	if (neg)
		ret[c++] = '-';
	while (i > 0) {
		ret[c++] = (nb / i) % 10 + '0';
		i /= 10;
	}
	return (ret);
}
