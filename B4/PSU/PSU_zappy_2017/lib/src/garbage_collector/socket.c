/*
** EPITECH PROJECT, 2018
** gc
** File description:
** socket
*/

#include <sys/socket.h>

#include "garbage_collector.h"

#undef socket

int gc_socket(int dom, int type, int prot)
{
	int ret = socket(dom, type, prot);
	void *old = g_socketCollection;

	g_socketCollection = gc_malloc(sizeof(void*) + sizeof(int));
	*(void**)g_socketCollection = old;
	*(int*)(g_socketCollection + sizeof(void*)) = ret;
	return (ret);
}