/*
** EPITECH PROJECT, 2018
** gc
** File description:
** memmove
*/

#include "garbage_collector.h"

void *gc_memmove(void *str1, const void *str2, size_t n)
{
	size_t i = -1;

	if (!str1 || !str2 || n <= 0)
		return (0);
	if (str2 > str1) {
		while (++i < n)
			((char*)str1)[i] = ((char*)str2)[i];
		return (str1);
	}
	i = n;
	while (i <= n)
		((char*)str1)[n] = ((char*)str2)[n];
	return (str1);
}
