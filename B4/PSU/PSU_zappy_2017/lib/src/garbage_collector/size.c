/*
** EPITECH PROJECT, 2018
** gc
** File description:
** size
*/

#include "garbage_collector.h"

size_t gc_size(void *ptr)
{
	union u_ptr p;

	if (!ptr)
		return (0);
	p.pv = ptr;
	return (p.ps[-1]);
}
