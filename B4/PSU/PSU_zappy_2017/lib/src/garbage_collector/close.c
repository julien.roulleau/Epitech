/*
** EPITECH PROJECT, 2018
** gc
** File description:
** close
*/

#include <unistd.h>

#include "garbage_collector.h"

#undef close

void gc_close(int fd)
{
	void *ptr = g_socketCollection;
	void *old = 0;

	close(fd);
	while (ptr) {
		if (*((int*)(ptr + sizeof(void*))) == fd) {
			if (old)
				*(void**)old = *(void**)ptr;
			else
				g_socketCollection = *(void**)ptr;
			gc_free(ptr);
			return;
		}
		old = ptr;
		ptr = *(void**)ptr;
	}
}

void gc_pclose(int *fd)
{
	gc_close(*fd);
	*fd = 0;
}

void gc_closeAll()
{
	void *ptr = g_socketCollection;

	while (ptr) {
		close(*((int*)(ptr + sizeof(void*))));
		g_socketCollection = *(void**)g_socketCollection;
		gc_free(ptr);
		ptr = g_socketCollection;
	}
}