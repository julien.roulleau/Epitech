/*
** EPITECH PROJECT, 2018
** gc
** File description:
** malloc
*/

#include "garbage_collector.h"
#include "log.h"

#undef malloc

void *gc_malloc(size_t size)
{
	union u_ptr ptr;

	if (size < 1)
		return (0);
	size += METADATA_SIZE;
	ptr.pv = malloc(size + METADATA_SIZE);
	if (!ptr.pv)
		exit(84);
	if (!g_garbageCollection.pv)
		g_garbageCollection.pv = ptr.pv;
	else {
		ptr.pp[0] = g_garbageCollection.pp[0];
		g_garbageCollection.pp[0] = ptr.pv;
	}
	ptr.ul += sizeof(void**);
	*ptr.ps = size;
	ptr.ul += sizeof(size_t);
	return (ptr.pv);
}
