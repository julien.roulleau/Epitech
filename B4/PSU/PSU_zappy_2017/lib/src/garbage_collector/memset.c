/*
** EPITECH PROJECT, 2018
** gc
** File description:
** memset
*/

#include "garbage_collector.h"

void *gc_memset(void *str, int c, size_t n)
{
	size_t i = -1;

	if (!str || n <= 0)
		return (0);
	while (++i < n)
		((char*)str)[i] = c;
	return (str);
}
