/*
** EPITECH PROJECT, 2018
** irc
** File description:
** join
*/

#include <string.h>

#include "garbage_collector.h"

char *gc_join(char *s1, char *s2)
{
	char *r = gc_calloc(strlen(s1) + strlen(s2), 1);

	return (strcat(strcpy(r, s1), s2));
}

char *gc_join_sc(char *s, char c)
{
	char *r = gc_calloc(strlen(s) + 1, 1);
	char t[2] = {c, 0};

	return (strcat(strcpy(r, s), t));
}

char *gc_join_scs(char *s1, char c, char *s2)
{
	char *r = gc_calloc(strlen(s1) + strlen(s2) + 1, 1);
	char t[2] = {c, 0};

	return (strcat(strcat(strcpy(r, s1), t), s2));
}
