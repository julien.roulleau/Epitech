/*
** EPITECH PROJECT, 2018
** gc
** File description:
** memcpy
*/

#include "garbage_collector.h"

void *gc_memcpy(void *str1, const void *str2, size_t n)
{
	size_t i = -1;

	if (!str1 || !str2 || n <= 0)
		return (0);
	while (++i < n)
		((char*)str1)[i] = ((char*)str2)[i];
	return (str1);
}
