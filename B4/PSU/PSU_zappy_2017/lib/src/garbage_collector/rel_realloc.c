/*
** EPITECH PROJECT, 2018
** gc
** File description:
** relRealloc
*/

#include "garbage_collector.h"

void *gc_rel_realloc(void *ptr, size_t size)
{
	union u_ptr new;
	union u_ptr old;
	size_t i = -1;

	if (!ptr)
		return (gc_calloc(size, 1));
	old.pv = ptr;
	size = (long int)(size) + old.ps[-1];
	if (size <= 0)
		return (0);
	new.pv = gc_malloc(size);
	if (!new.pv)
		return (0);
	while (++i < size && i < old.ps[-1])
		*(new.pc + i) = *(old.pc + i);
	--i;
	while (++i < size)
		*(new.pc + i) = 0;
	return (new.pv);
}
