/*
** EPITECH PROJECT, 2018
** gc
** File description:
** freeAll
*/

#include "garbage_collector.h"
#include "log.h"

#undef free

void gc_freeAll()
{
	void *ptr;

	log("Freeing all memory");
	while (g_garbageCollection.pv &&
		g_garbageCollection.ul < 0x10000000000000) {
		ptr = g_garbageCollection.pp[0];
		free(g_garbageCollection.pv);
		g_garbageCollection.pv = ptr;
	}
	suc("All memory freed");
	logv("This is the last message of this program.", -1);
}
