/*
** EPITECH PROJECT, 2018
** gc
** File description:
** memcpy
*/

#include <string.h>

#include "garbage_collector.h"

char *gc_strdup(char *str)
{
	char *ret = 0;

	if (!str)
		return (0);
	ret = gc_calloc(strlen(str) + 1, 1);
	memcpy(ret, str, strlen(str));
	return (ret);
}
