/*
** EPITECH PROJECT, 2018
** gc
** File description:
** calloc
*/

#include "garbage_collector.h"

void *gc_calloc(size_t nbr, size_t size)
{
	union u_ptr ptr;

	if (nbr <= 0 || size <= 0)
		return (0);
	ptr.pv = gc_malloc(nbr * size);
	if (!ptr.pv)
		exit(84);
	gc_memset(ptr.pv, 0, ptr.ps[-1]);
	return (ptr.pv);
}
