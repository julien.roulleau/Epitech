/*
** EPITECH PROJECT, 2018
** log
** File description:
** log
*/

#include "log.h"

int lognull()
{
	return (0);
}

int logv(char *msg, int level)
{
	if (!msg)
		return (logv("A null value has been passed to logv()", 2));
	switch (level) {
	case 0:
		dprintf(2, " \033[1;37m[LOG]\033[22m  %s\033[0m\n", msg);
		break;
	case 1:
		dprintf(2, " \033[1;33m[WAR]\033[22m  %s\033[0m\n", msg);
		break;
	case 2:
		dprintf(2, " \033[1;31m[ERR]\033[22m  %s\033[0m\n", msg);
		break;
	case 3:
		dprintf(2, " \033[1;32m[LOG]\033[22;2;3m  → %s\033[0m\n", msg);
		break;
	case -1:
		dprintf(2, " \033[1;3;4;37m%s\033[0m\n", msg);
	}
	return (0);
}

int logi(char *msg, long value, int level)
{
	if (!msg)
		return (logv("A null value has been passed to logi()", 2));
	switch (level) {
	case 1:
		dprintf(2, " \033[1;33m[WAR]\033[22m  ");
		break;
	case 2:
		dprintf(2, " \033[1;31m[ERR]\033[22m  ");
		break;
	case 3:
		dprintf(2, " \033[1;32m[LOG]\033[22;2;3m  → ");
		break;
	default:
		dprintf(2, " \033[1;37m[LOG]\033[22m  ");
		break;
	}
	dprintf(2, msg, value);
	dprintf(2, "\033[0m\n");
	return (0);
}
