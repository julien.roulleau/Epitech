/*
** EPITECH PROJECT, 2018
** dic
** File description:
** get
*/

#include "dic.h"

void *dic_get(dic_t this, string key)
{
	int i;

	if (!key)
		return (0);
	if ((i = dic_index(this, key)) == -1)
		return (0);
	return (this->dic[i].value);
}
