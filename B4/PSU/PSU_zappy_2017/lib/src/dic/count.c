/*
** EPITECH PROJECT, 2018
** dic
** File description:
** count
*/

#include "dic.h"

int dic_count(dic_t this)
{
	int i = -1;
	int j = 0;

	while (++i < this->size)
		if (this->dic[i].key)
			j++;
	return (j);
}
