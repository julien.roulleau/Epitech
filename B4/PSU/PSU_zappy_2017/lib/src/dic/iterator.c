/*
** EPITECH PROJECT, 2018
** dic
** File description:
** iterator
*/

#include "dic.h"

void *dic_begin(dic_t dic)
{
	for (int i = 0; i < dic->size; i++)
		if (dic->dic[i].key)
			return (&(dic->dic[i]));
	return (0);
}

void *dic_next(dic_t dic, void *t)
{
	int i;

	if (t == 0)
		return (dic_begin(dic));
	for (i = 0; i < dic->size; i++)
		if (&(dic->dic[i]) == t)
			break;
	for (i++; i < dic->size; i++)
		if (dic->dic[i].key)
			return (&(dic->dic[i]));
	return (0);
}
