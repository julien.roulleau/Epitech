/*
** EPITECH PROJECT, 2018
** crc
** File description:
** crc
*/

#include "dic.h"
#include "garbage_collector.h"

#define POLY64REV 0xd800000000000000UL

static unsigned long *lookuptable = 0;

static void setTable()
{
	int i = -1;
	int j;
	unsigned long v;

	lookuptable = gc_calloc(0x100, sizeof(long));
	while (++i < 0x100) {
		v = i;
		j = -1;
		while (++j < 8)
			v = (v & 1) ? (v >> 1) ^ POLY64REV : (v >> 1);
		lookuptable[i] = v;
	}
}

unsigned long checksum(char *str)
{
	unsigned long sum = 0;
	size_t i = -1;
	size_t lookup;

	if (!lookuptable)
		setTable();
	while (str[++i]) {
		lookup = ((int) (sum ^ str[i])) & 0xff;
		sum = (sum >> 8) ^ lookuptable[lookup];
	}
	return (sum);
}
