/*
** EPITECH PROJECT, 2018
** dic
** File description:
** find
*/

#include "dic.h"

void *dic_find(dic_t fi, dic_t se, string key)
{
	void *ret;

	if (!(ret = dic_get(fi, key)))
		return (dic_get(se, key));
	return (ret);
}
