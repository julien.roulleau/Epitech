/*
** EPITECH PROJECT, 2018
** dic
** File description:
** unserialize
*/

#include "dic.h"

static void getItem(dic_t this, string str)
{
	int i;

	i = -1;
	while (str[++i] && str[i] != '=');
	if (!str[i] || !str[i + 1])
		dic_set(this, strndup(str, strlen(str) - 1), strdup(""));
	else
		dic_set(this, strndup(str, i), strdup(str + i + 1));
}

int dic_unserialize(dic_t this, string *darray)
{
	if (!darray || !*darray)
		return (0);
	while (*darray) {
		getItem(this, *darray);
		++darray;
	}
	return (1);
}
