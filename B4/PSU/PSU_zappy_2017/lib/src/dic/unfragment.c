/*
** EPITECH PROJECT, 2018
** dic
** File description:
** unfragment
*/

#include "dic.h"

void dic_unfragment(dic_t this)
{
	int i = -1;
	int j;

	while (++i < this->size) {
		j = i;
		if (this->dic[i].key) {
			while (--j > 0 && !this->dic[i].key);
			if (i != ++j) {
				this->dic[j].key = this->dic[i].key;
				this->dic[j].hash = this->dic[i].hash;
				this->dic[j].value = this->dic[i].value;
				this->dic[i].key = 0;
			}
		}
	}
}
