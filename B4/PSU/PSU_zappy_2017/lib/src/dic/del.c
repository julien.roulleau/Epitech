/*
** EPITECH PROJECT, 2018
** dic
** File description:
** del
*/

#include "dic.h"

int dic_del(dic_t this, string key)
{
	int i;

	if (!key)
		return (0);
	if ((i = dic_index(this, key)) != -1)
		this->dic[i].key = 0;
	return ((i == -1) ? 0 : 1);
}
