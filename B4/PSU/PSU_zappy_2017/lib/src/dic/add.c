/*
** EPITECH PROJECT, 2018
** dic
** File description:
** add
*/

#include "garbage_collector.h"
#include "dic.h"

int dic_add(dic_t this, string key, void *value)
{
	int i = -1;
	dic_elem_t *elems;

	if (!key)
		return (0);
	while (++i < this->size)
		if (!this->dic[i].key) {
			this->dic[i].key = key;
			this->dic[i].hash = checksum(key);
			this->dic[i].value = value;
			return (1);
		}
	if (!(elems = gc_rel_realloc(this->dic, this->size)))
		return (0);
	this->dic = elems;
	this->dic[i].key = key;
	this->dic[i].hash = checksum(key);
	this->dic[i].value = value;
	this->size *= 2;
	return (1);
}
