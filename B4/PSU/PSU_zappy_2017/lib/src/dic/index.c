/*
** EPITECH PROJECT, 2018
** dic
** File description:
** index
*/

#include "dic.h"

int dic_index(dic_t this, string key)
{
	int i;
	unsigned long hash;

	if (!key)
		return (-1);
	hash = checksum(key);
	i = -1;
	while (++i < this->size)
		if (this->dic[i].key && hash == this->dic[i].hash &&
			!strcmp(key, this->dic[i].key))
			return (i);
	return (-1);
}
