/*
** EPITECH PROJECT, 2018
** dic
** File description:
** serialize
*/

#include "dic.h"
#include "garbage_collector.h"

string join(string s1, char c, string s2)
{
	string ret;
	int i = -1;
	int j = -1;

	ret = gc_calloc(strlen(s1) + 1 + strlen(s2), 1);
	while (s1[++i] && (ret[i] = s1[i]));
	ret[i] = c;
	while ((ret[++i] = s2[++j]));
	return (ret);
}

static int getNumber(dic_t this)
{
	int i;
	int nb;

	nb = 0;
	i = -1;
	while (++i < this->size) {
		if (this->dic[i].key)
			++nb;
	}
	return (nb);
}

string *dic_serialize(dic_t this)
{
	int len;
	int i;
	string *ret;

	dic_unfragment(this);
	len = getNumber(this);
	i = -1;
	ret = gc_calloc(len + 12, sizeof(string*));
	while (++i < len)
		ret[i] = join(this->dic[i].key, '=', this->dic[i].value);
	return (ret);
}
