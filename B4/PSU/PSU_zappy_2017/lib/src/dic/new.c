/*
** EPITECH PROJECT, 2018
** dic
** File description:
** new
*/

#include "dic.h"
#include "garbage_collector.h"

dic_t new_dic(int size)
{
	dic_t ret;

	ret = gc_calloc(1, sizeof(struct s_dic));
	ret->size = size;
	ret->dic = gc_calloc(size, sizeof(dic_elem_t));
	return (ret);
}
