/*
** EPITECH PROJECT, 2018
** dic
** File description:
** set
*/

#include "dic.h"

int dic_set(dic_t this, string key, void *value)
{
	int in;

	if (!key)
		return (0);
	if ((in = dic_index(this, key)) != -1)
		this->dic[in].value = value;
	else
		return (dic_add(this, key, value));
	return (1);
}
