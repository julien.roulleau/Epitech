/*
** EPITECH PROJECT, 2018
** gnl
** File description:
** get_next_line
*/

#include <unistd.h>
#include <stdlib.h>

#include "garbage_collector.h"
#include "log.h"

char *get_next_line(const int fd)
{
	char *line = gc_calloc(1, sizeof(char));
	char buff;
	int i = -1;

	while (read(fd, &buff, 1) > 0) {
		if (buff == '\n')
			return (line);
		line = gc_rel_realloc(line, 1);
		line[++i] = buff;
		line[i + 1] = 0;
	}
	if (line[0])
		return (line);
	return (0);
}
