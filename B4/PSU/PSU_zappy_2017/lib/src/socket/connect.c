/*
** EPITECH PROJECT, 2018
** socket
** File description:
** socket
*/

#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <errno.h>

#include "socket.h"

int sock_connect(socket_t s, char *host, short port)
{
	struct sockaddr_in sin = {0};
	struct hostent *hostinfo = gethostbyname(host);
	
	if (!hostinfo)
		return (-errno);
	sin.sin_addr = *((struct in_addr *)hostinfo->h_addr);
	sin.sin_port = htons(port);
	sin.sin_family = AF_INET;
	if (connect(s->fd, (struct sockaddr*)&sin, sizeof(sin)) >= 0)
		return (0);
	return (-errno);
}
