/*
** EPITECH PROJECT, 2018
** socket
** File description:
** socket
*/

#include "socket.h"

char *sock_recv(socket_t s)
{
	int p = s->recv_pos;
	char *ret = s->recv_queue[p];

	if (!s->recv_queue[p])
		return (0);
	s->recv_queue[p] = 0;
	s->recv_pos++;
	if (ret[strlen(ret) - 1] == '\n')
		ret[strlen(ret) - 1] = 0;
	return (ret);
}
