/*
** EPITECH PROJECT, 2018
** socket
** File description:
** socket
*/

#include <errno.h>
#include <sys/socket.h>

#include "garbage_collector.h"
#include "log.h"
#include "socket.h"

socket_t new_socket()
{
	socket_t s = calloc(sizeof(struct s_socket), 1);

	s->fd = socket(AF_INET, SOCK_STREAM, 0);
	if (s->fd == -1) {
		free(s);
		ierr("Cannot create socket: %s", (long int)strerror(errno));
		return (0);
	}
	s->send_tmp = "";
	s->recv_tmp = "";
	return (s);
}
