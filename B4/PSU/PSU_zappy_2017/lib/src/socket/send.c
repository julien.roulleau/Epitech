/*
** EPITECH PROJECT, 2018
** socket
** File description:
** socket
*/

#include "garbage_collector.h"
#include "socket.h"

static int store(socket_t s, char *str, int i)
{
	int p = s->send_pos;

	if (s->send_queue[(s->send_pos + 31) % 32])
		return (0);
	while (s->send_queue[p % 32])
		p++;
	str[i] = 0;
	s->send_queue[p % 32] = gc_strdup(str);
	return (1);
}

void sock_send(socket_t s, char *str)
{
	int i;

	s->send_tmp = gc_join(s->send_tmp, str);
	while (strlen(s->send_tmp) > 0 && s->send_tmp[0] == '\n')
		s->send_tmp++;
	for (i = strlen(s->send_tmp) - 1; i >= 0 && s->send_tmp[i] != '\n';
		i--);
	if (i <= 0)
		return;
	if (store(s, s->send_tmp, i))
		s->send_tmp += i + 1;
}
