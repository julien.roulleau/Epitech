/*
** EPITECH PROJECT, 2018
** socket
** File description:
** socket
*/

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/sockios.h>
#include <unistd.h>

#include "garbage_collector.h"
#include "socket.h"

#define min(x, y) (x < y ? x : y)

static void put_cyclic(socket_t s, int len)
{
	int l;

	if (!(s->send_queue[s->send_pos]))
		return;
	l = strlen(s->send_queue[s->send_pos]);
	l = write(s->fd, s->send_queue[s->send_pos], min(len, l));
	l += write(s->fd, "\n", 1);
	if (l >= (int)strlen(s->send_queue[s->send_pos]) + 1) {
		len -= l;
		s->send_queue[s->send_pos++] = 0;
		if (len > 0)
			put_cyclic(s, len);
	} else
		s->send_queue[s->send_pos] += l;
}

static int store(socket_t s, char *str, int i)
{
	int p = s->recv_pos;

	if (s->recv_queue[(s->recv_pos + 31) % 32])
		return (0);
	while (s->recv_queue[p % 32])
		p++;
	str[i] = 0;
	s->recv_queue[p % 32] = gc_strdup(str);
	return (1);
}

static void get_cyclic(socket_t s, int len)
{
	char *reader = calloc(len + 1, 1);
	_freeable char *mem = reader;
	int i;

	len = read(s->fd, reader, len);
	if (len <= 0)
		return;
	while (*reader == '\n') {
		len--;
		reader++;
	}
	for (i = 0; i < len; i++)
		if (reader[i] == '\n' && store(s, reader, i)) {
			reader += i + 1;
			len -= i + 1;
			i = 0;
		}
	if (len > 0)
		s->recv_tmp = gc_join(s->recv_tmp, reader);
}

int sock_update(socket_t s)
{
	int sz;
	int szm;
	socklen_t optlen = 4;

	if (getsockopt(s->fd, SOL_SOCKET, SO_SNDBUF, &szm, &optlen) != -1 &&
		ioctl(s->fd, SIOCOUTQ, &sz) != -1 && szm / 2 - sz > 0)
		put_cyclic(s, szm / 2 - sz);
	if (ioctl(s->fd, FIONREAD, &sz) != -1 && sz > 0)
		get_cyclic(s, sz);
	return (0);
}

int sock_update_all(dic_t d)
{
	int sum = 0;

	foreach(d as k, v)
		sum += sock_update(v);
	return (sum);
}
