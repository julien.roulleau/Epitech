/*
** EPITECH PROJECT, 2018
** socket
** File description:
** close
*/

#include <signal.h>

#include "garbage_collector.h"
#include "socket.h"

static void sighandler(int sig)
{
	(void)sig;
}

void sock_close(socket_t sock)
{
	void (*old)(int) = signal(SIGPIPE, sighandler);

	if (sock) {
		sock_update(sock);
		gc_close(sock->fd);
		free(sock);
	}
	signal(SIGPIPE, old);
}