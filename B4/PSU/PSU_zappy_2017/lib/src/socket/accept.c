/*
** EPITECH PROJECT, 2018
** socket
** File description:
** socket
*/

#include <sys/socket.h>
#include <netinet/in.h>

#include "garbage_collector.h"
#include "socket.h"

socket_t sock_accept(socket_t s)
{
	struct sockaddr_in sin = {0};
	socklen_t szsin = sizeof(sin);
	int fd = accept(s->fd, (struct sockaddr*)&sin, &szsin);
	socket_t s2 = calloc(sizeof(struct s_socket), 1);

	if (fd < 1) {
		free(s2);
		return (0);
	}
	s2->fd = fd;
	s2->send_tmp = "";
	s2->recv_tmp = "";
	return (s2);
}
