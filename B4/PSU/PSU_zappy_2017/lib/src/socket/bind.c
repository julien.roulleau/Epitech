/*
** EPITECH PROJECT, 2018
** socket
** File description:
** socket
*/

#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>

#include "socket.h"

int sock_bind(socket_t s, short port)
{
	struct sockaddr_in sin = {0};

	sin.sin_addr.s_addr = INADDR_ANY;
	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);
	if (bind(s->fd, (struct sockaddr*)&sin, sizeof(sin)) < 0)
		return (-errno);
	return (0);
}
