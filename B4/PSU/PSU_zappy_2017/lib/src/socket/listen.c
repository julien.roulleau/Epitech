/*
** EPITECH PROJECT, 2018
** socket
** File description:
** socket
*/

#include <sys/socket.h>
#include <errno.h>

#include "socket.h"

int sock_listen(socket_t s, int n)
{
	if (s->fd > 0)
		return (listen(s->fd, n));
	return (-errno);
}
