#!/usr/bin/env bash

SERVER="zappy_server"
CLIENT="zappy_ai"
GRAPHICAL="zappy_graph"

function start {
    PORT=4242
    NB_TEAM=5
    NB_BY_TEAM=10
    FREQ=100
    MAP_SIZE_X=10
    MAP_SIZE_Y=10
    INIT_NB_AI=3

    TEAMS_NAME=""

    make debug

    for i in `seq ${NB_TEAM}`; do
        TEAMS_NAME+="TEAM${i}"
        if [ ${i} != ${NB_TEAM} ]; then
            TEAMS_NAME+=" "
        fi
    done

    ./${SERVER} -p ${PORT} \
        -x ${MAP_SIZE_X=20} -y ${MAP_SIZE_Y=20} \
        -n ${TEAMS_NAME} -c ${INIT_NB_AI} \
        -f ${FREQ} &

    sleep 1
    ./${GRAPHICAL} 127.0.0.1 ${PORT} &

    for i in `seq ${NB_BY_TEAM}`; do
        for name in ${TEAMS_NAME}; do
            echo ${name} ${i}
            sleep 1
            ./${CLIENT} -p ${PORT} -n ${name} &
        done
    done
}

function stop {
    killall ${SERVER}
    killall ${CLIENT}
}

function usage {
    echo "$0 [start|stop]"
    exit 1
}

if [ $# == 1 ]; then
    case $1 in
        "start")
            start
            ;;
        "stop")
            stop
            ;;
        *)
            usage
            ;;
    esac
else
    usage
fi
