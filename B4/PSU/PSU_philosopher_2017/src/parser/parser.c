/*
** EPITECH PROJECT, 2018
** philosophers
** File description:
** parser.c
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <philosopher/philosopher.h>
#include "parser.h"

bool arg_parser(table_t *arg, int ac, char **av)
{
	if (ac != 5) {
		dprintf(2, "Invalid number of arguments\n");
		return (false);
	}
	for (int i = 1; i < 5; i++) {
		if (!strcmp("-p", av[i]) && arg->nb_p == 0)
			arg->nb_p = atoi(av[++i]);
		else if (!strcmp("-e", av[i]) && arg->max_eat == 0)
			arg->max_eat = atoi(av[++i]);
		else {
			dprintf(2, "Invalid argument\n");
			return (false);
		}
	}
	return (true);
}
