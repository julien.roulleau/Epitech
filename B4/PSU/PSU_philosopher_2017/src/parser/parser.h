/*
** EPITECH PROJECT, 2018
** philosophers
** File description:
** parser.h
*/


#ifndef PHILOSOPHERS_PARSER_H
#define PHILOSOPHERS_PARSER_H

#include <stdbool.h>
#include "philosopher/philosopher.h"

bool arg_parser(table_t *arg, int ac, char **av);

#endif //PHILOSOPHERS_PARSER_H
