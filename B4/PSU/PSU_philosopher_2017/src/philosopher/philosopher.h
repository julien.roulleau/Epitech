/*
** EPITECH PROJECT, 2018
** philosophers
** File description:
** philosopher.h
*/

#ifndef PHILOSOPHERS_PHILOSOPHER_H
#define PHILOSOPHERS_PHILOSOPHER_H

#include <stdbool.h>
#include "extern/extern.h"

typedef struct {
	int max_eat;
	int nb_p;
	pthread_mutex_t *chopsticks;
} table_t;

typedef struct {
	pthread_t th;
	int id;
	table_t *table;
} philosopher_t;

#define SELF_C() (self->id)
#define NEXT_C() (self->id + 1 < table->nb_p ? self->id + 1 : 0)

void *philosopher(void *data);

#endif //PHILOSOPHERS_PHILOSOPHER_H
