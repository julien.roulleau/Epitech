/*
** EPITECH PROJECT, 2018
** philosophers
** File description:
** philosopher.c
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "philosopher.h"

const __useconds_t EAT_TIME = 2;
const __useconds_t THINK_TIME = 1;

bool take(bool *chopsticks, table_t *table, int nb)
{
	if (!chopsticks[nb]) {
		if (!pthread_mutex_trylock(&table->chopsticks[nb])) {
			lphilo_take_chopstick(&table->chopsticks[nb]);
			chopsticks[nb] = true;
			return (true);
		} else
			return (false);
	} else
		return (true);
}

bool release(bool *chopsticks, table_t *table, int nb)
{
	if (chopsticks[nb]) {
		pthread_mutex_unlock(&table->chopsticks[nb]);
		lphilo_release_chopstick(&table->chopsticks[nb]);
		chopsticks[nb] = false;
		return (true);
	} else
		return (false);
}

void *philosopher(void *data)
{
	philosopher_t *self = data;
	table_t *table = self->table;
	bool chopsticks[table->nb_p];

	for (int i = 0; i < self->table->nb_p; chopsticks[i] = false, i++);
	for (int i = 0; i < self->table->max_eat; lphilo_sleep(), i++) {
		while (!take(chopsticks, table, SELF_C())
		       && !take(chopsticks, table, NEXT_C()));
		lphilo_think();
		usleep(THINK_TIME);
		while (!take(chopsticks, table, SELF_C())
		       || !take(chopsticks, table, NEXT_C())) {
			release(chopsticks, table, SELF_C());
			release(chopsticks, table, NEXT_C());
		};
		lphilo_eat();
		usleep(EAT_TIME);
		release(chopsticks, table, SELF_C());
		release(chopsticks, table, NEXT_C());
	}
	return (NULL);
}

