/*
** EPITECH PROJECT, 2018
** philosophers
** File description:
** main.c
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "philosopher/philosopher.h"
#include "parser/parser.h"

int help(void)
{
	printf("USAGE\n"
		       "\t./philo -p nbr_p -e nbr_e\n"
		       "\n"
		       "DESCRIPTION\n"
		       "\t-p nbr_p number of philosophers\n"
		       "\t-e nbr_e maximum number times a philosopher "
		       "eats before exiting the program\n");
	return (0);
}

bool init_chopstick(table_t *table)
{
	table->chopsticks = malloc(sizeof(pthread_mutex_t) * table->nb_p);

	if (!table->chopsticks)
		return (false);
	for (int i = 0; i < table->nb_p; i++) {
		if (pthread_mutex_init(&table->chopsticks[i], NULL))
			return (false);
	}
	return (true);
}

bool run(table_t *table)
{
	philosopher_t philosophers[table->nb_p];

	if (!init_chopstick(table))
		return (false);
	for (int i = 0; i < table->nb_p; i++) {
		philosophers[i].id = i;
		philosophers[i].table = table;
		if (pthread_create(&philosophers[i].th, NULL,
				   &philosopher, &philosophers[i]))
			return (false);
	}
	for (int i = 0; i < table->nb_p; i++)
		if (pthread_join(philosophers[i].th, NULL))
			return (false);
	for (int i = 0; i < table->nb_p; i++)
		if (pthread_mutex_destroy(&table->chopsticks[i]))
			return (false);
	free(table->chopsticks);
	return (true);
}

int main(int ac, char **av)
{
	table_t table = {0, 0, NULL};

	if (ac > 1 && !strcmp("--help", av[1]))
		return (help());
	if (!arg_parser(&table, ac, av))
		return (84);
	RCFStartup(ac, av);
	if (!run(&table)) {
		RCFCleanup();
		return (84);
	}
	RCFCleanup();
	return (0);
}