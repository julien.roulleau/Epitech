/*
** EPITECH PROJECT, 2018
** malloc
** File description:
** show alloc mem
*/

#include "header.h"

void print(const char *args, ...)
{
	va_list ap;
	char BUF[2048];

	va_start(ap, args);
	sprintf(BUF, args, ap);
	write(1, BUF, strlen(BUF));
	va_end(ap);
}

void show_alloc_mem(void)
{
	header_t *block = first_block(NULL);
	void *ptr = (void *) block + sizeof(header_t);

	print("break : %p\n", sbrk(0));
	while (block != NULL) {
		print("%p - %p : %zu bytes\n",
		      ptr, ptr + block->size, block->size);
		block = block->next;
		if ((void *) block > sbrk(0)) {
			print("Error\n");
			break;
		}
	}
}
