/*
** EPITECH PROJECT, 2018
** malloc_bootstrap
** File description:
** header.h
*/


#ifndef MALLOC_BOOTSTRAP_HEADER_H
#define MALLOC_BOOTSTRAP_HEADER_H

#include <stddef.h>
#include <stdbool.h>
#include <unistd.h>
#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

void print(const char *args, ...);

#ifdef DEBUG
# define PRINTD(...) (print(__VA_ARGS__))
#else
# define PRINTD(...)
#endif

#define ALIGN_SIZE(r, s) ((r) * ((s) / (r) + 1))
#define ABS(nb) ((nb) > 0 ? (nb) : -(nb))

typedef struct header {
	size_t size;
	void *prev;
	void *next;
	bool is_free;
} header_t;

void *malloc(size_t size);
void *calloc(size_t, size_t);
void free(void *ptr);
void *realloc(void *ptr, size_t size);
void show_alloc_mem(void);

pthread_mutex_t *mutex();
header_t *first_block(header_t *value);
header_t *last_block(header_t *value);
int page_size(int value);

#endif /* MALLOC_BOOTSTRAP_HEADER_H */
