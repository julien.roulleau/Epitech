/*
** EPITECH PROJECT, 2018
** PSU_2017_malloc
** File description:
** free.c
*/

#include "header.h"

void free(void *ptr)
{
	header_t *header = NULL;

	pthread_mutex_lock(mutex());
	PRINTD("free 0x%8.8lX\n", (size_t) ptr);
	header = (header_t *) (ptr - sizeof(header_t));
	if (ptr && !header->is_free)
		header->is_free = true;
	pthread_mutex_unlock(mutex());
}
