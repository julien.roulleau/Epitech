/*
** EPITECH PROJECT, 2018
** malloc
** File description:
** contex.c
*/

#include "header.h"

pthread_mutex_t *mutex()
{
	static pthread_mutex_t gl = PTHREAD_MUTEX_INITIALIZER;
	return &gl;
}

header_t *first_block(header_t *value)
{
	static header_t *gl = NULL;

	if (value)
		gl = value;
	return gl;
}

header_t *last_block(header_t *value)
{
	static header_t *gl = NULL;

	if (value)
		gl = value;
	return gl;
}

int page_size(int value)
{
	static int gl = 0;

	if (value)
		gl = value;
	return gl;
}
