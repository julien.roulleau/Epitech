/*
** EPITECH PROJECT, 2018
** PSU_2017_malloc
** File description:
** calloc.c
*/

#include "header.h"

void *realloc(void *ptr, size_t size)
{
	char *new_ptr = NULL;
	header_t *header = NULL;

	PRINTD("realloc ");
	if (size == 0) {
		if (ptr)
			free(ptr);
		return (NULL);
	} else if (!ptr)
		return (malloc(size));
	header = (header_t *) (ptr - sizeof(header_t));
	if (size <= header->size)
		return (ptr);
	else {
		new_ptr = malloc(size);
		memmove(new_ptr, ptr, size);
		free(ptr);
		return (new_ptr);
	}
}
