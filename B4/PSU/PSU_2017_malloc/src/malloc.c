/*
** EPITECH PROJECT, 2018
** PSU_2017_malloc
** File description:
** malloc.c
*/

#include "header.h"

static bool init_malloc(void)
{
	header_t *block = NULL;

	PRINTD(" init ");
	page_size(getpagesize());
	block = sbrk(page_size(0));
	if (block == (void *) -1)
		return (false);
	block->size = page_size(0) - sizeof(header_t);
	block->is_free = true;
	block->prev = NULL;
	block->next = NULL;
	first_block(block);
	last_block(block);
	return (true);
}

static header_t *add_pages(size_t size)
{
	PRINTD(" add_page ");
	size_t malloc_size = ALIGN_SIZE(
		page_size(0),
		ABS(size + sizeof(header_t) - last_block(NULL)->size));

	if (sbrk(malloc_size) == (void *) -1)
		return (NULL);
	last_block(NULL)->size += malloc_size;
	return (last_block(NULL));
}

static header_t *find_place(header_t *block, size_t size)
{
	while (block) {
		if (block->is_free && (block->size > size + sizeof(header_t)
				       || (block != last_block(NULL) &&
					   block->size == size)))
			return (block);
		block = block->next;
	}
	return (NULL);
}

static void *alloc(header_t *block, size_t size)
{
	header_t *new_block = (void *) block + sizeof(header_t) + size;

	if (block->size != size) {
		new_block->size = block->size - size - sizeof(header_t);
		new_block->is_free = true;
		new_block->prev = block;
		new_block->next = block->next;
		block->size = size;
		block->next = new_block;
	}
	block->is_free = false;
	if (new_block > last_block(NULL))
		last_block(new_block);
	return ((void *) block + sizeof(header_t));
}

void *malloc(size_t size)
{
	void *new_ptr = NULL;
	header_t *block = NULL;

	pthread_mutex_lock(mutex());
	PRINTD("malloc ");
	if (!first_block(NULL) && !init_malloc()) {
		pthread_mutex_unlock(mutex());
		return (NULL);
	}
	size = ALIGN_SIZE(8, size);
	if (!(block = find_place(first_block(NULL), size)) &&
	    !(block = add_pages(size))) {
		pthread_mutex_unlock(mutex());
		return (NULL);
	}
	PRINTD(" %zu %p %zu %i ", size, block, block->size, block->is_free);
	new_ptr = alloc(block, size);
	PRINTD("\n");
	pthread_mutex_unlock(mutex());
	return (new_ptr);
}
