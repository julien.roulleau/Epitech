/*
** EPITECH PROJECT, 2018
** malloc
** File description:
** calloc.c
*/

#include "header.h"

void *calloc(size_t nb, size_t sz)
{
	void *ptr = malloc(sz * nb);

	PRINTD("calloc ");
	if (ptr)
		memset(ptr, 0, sz * nb);
	return (ptr);
}