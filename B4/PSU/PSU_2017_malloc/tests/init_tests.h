/*
** EPITECH PROJECT, 2018
** malloc
** File description:
** init_tests.h
*/


#ifndef MALLOC_INIT_TESTS_H
#define MALLOC_INIT_TESTS_H

#include <criterion/criterion.h>
#include <dlfcn.h>
#include <stdio.h>

extern void *lib;
extern void *(*my_malloc)(size_t);
extern void (*my_free)(void *);
extern void *(*my_realloc)(void *, size_t);
extern void (*show_alloc_mem)(void);

void setup(void);
void teardown(void);

#endif /* MALLOC_INIT_TESTS_H */
