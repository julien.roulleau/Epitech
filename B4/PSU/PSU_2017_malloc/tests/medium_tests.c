/*
** EPITECH PROJECT, 2018
** malloc
** File description:
** medium_tests.c
*/

#include "init_tests.h"

Test(Medium, malloc_place, .init = setup, .fini = teardown)
{
	void *tmp1 = NULL;
	void *tmp2 = NULL;
	void *tmp3 = NULL;

	tmp1 = my_malloc(100);
	tmp2 = my_malloc(100);
	tmp3 = my_malloc(100);
	if (!tmp1 || !tmp2 || !tmp3)
		cr_expect(0, "Malloc fail");
	my_free(tmp2);
	tmp2 = my_malloc(50);
	if (!tmp2)
		cr_expect(0, "Malloc fail");
	if (!(tmp1 < tmp2 || tmp2 > tmp3))
		cr_expect(0, "Malloc does not have a good place");
	my_free(tmp1);
	my_free(tmp3);
	my_free(tmp2);
	cr_expect(1, "Malloc success");
}

Test(Medium, big_malloc, .init = setup, .fini = teardown)
{
	void *tmp = NULL;

	tmp = my_malloc(1000);
	if (!tmp)
		cr_expect(0, "Malloc fail");
	my_free(tmp);
	tmp = my_malloc(1000000);
	if (!tmp)
		cr_expect(0, "Malloc fail");
	my_free(tmp);
	tmp = my_malloc(1000000000);
	if (!tmp)
		cr_expect(0, "Malloc fail");
	my_free(tmp);
	cr_expect(1, "Malloc success");
}

Test(Medium, lot_of_malloc, .init = setup, .fini = teardown)
{
	for (int i = 0; i < 1000; i++)
		if (!my_malloc(1000000))
			cr_expect(0, "Malloc fail");
	cr_expect(1, "Malloc success");
}

Test(Medium, concat_free, .init = setup, .fini = teardown)
{
	void *tmp1 = NULL;
	void *tmp2 = NULL;
	void *tmp3 = NULL;
	void *tmp4 = NULL;

	tmp1 = my_malloc(100);
	tmp2 = my_malloc(100);
	tmp3 = my_malloc(100);
	tmp4 = my_malloc(100);
	if (!tmp1 || !tmp2 || !tmp3 || !tmp4)
		cr_expect(0, "Malloc fail");
	my_free(tmp2);
	my_free(tmp3);
	tmp2 = my_malloc(150);
	if (!tmp2)
		cr_expect(0, "Malloc fail");
	if (!(tmp1 < tmp2 || tmp2 > tmp4))
		cr_expect(0, "Malloc does not have a good place");
	cr_expect(1, "Malloc success");
}

/*
Test(Medium, data_realloc, .init = setup, .fini = teardown)
{
	char *ptr = my_malloc(10 * sizeof(char));

	if (!ptr)
		cr_expect(0, "Malloc fail");
	for (int i = 0; i < 9; i++)
		ptr[i] = 'A';
	ptr[9] = 0;
	ptr = my_realloc(ptr, 15);
	if (!ptr)
		cr_expect(0, "Realloc fail");
	for (int i = 0; i < 9; i++)
		if (ptr[i] != 'A')
			cr_expect(0, "Realloc fail");
	cr_expect(1, "Malloc success");
}
 */
