/*
** EPITECH PROJECT, 2018
** malloc
** File description:
** init_tests.c
*/

#include "init_tests.h"

void *lib = NULL;
void *(*my_malloc)(size_t) = NULL;
void (*my_free)(void *) = NULL;
void *(*my_realloc)(void *, size_t) = NULL;
void (*show_alloc_mem)(void) = NULL;

void setup(void)
{
	lib = dlopen("../libmy_malloc.so", RTLD_LOCAL | RTLD_LAZY);
	my_malloc = dlsym(lib, "malloc");
	my_realloc = dlsym(lib, "realloc");
	my_free = dlsym(lib, "free");
	show_alloc_mem = dlsym(lib, "show_alloc_mem");
}

void teardown(void)
{
	dlclose(lib);
}
