/*
** EPITECH PROJECT, 2018
** malloc_bootstrap
** File description:
** malloc.c
*/

#include "init_tests.h"

Test(Basic, malloc, .init = setup, .fini = teardown)
{
	if (!my_malloc(10))
		cr_assert(0, "Malloc fail");
	cr_assert(1, "Malloc success");
}

Test(Basic, free, .init = setup, .fini = teardown)
{
	char *tmp = my_malloc(10);

	if (!tmp)
		cr_assert(0, "Malloc fail");
	my_free(tmp);
	cr_assert(1, "Free success");
}

Test(Basic, show_alloc_mem, .init = setup, .fini = teardown)
{
	void *tmp1 = NULL;
	void *tmp2 = NULL;
	void *tmp3 = NULL;
	void *tmp4 = NULL;

	tmp1 = my_malloc(100);
	tmp2 = my_malloc(100);
	tmp3 = my_malloc(100);
	tmp4 = my_malloc(100);
	my_free(tmp2);
	my_free(tmp3);
	show_alloc_mem();
	my_free(tmp1);
	my_free(tmp2);
	my_free(tmp3);
	my_free(tmp4);
	cr_assert(1, "Malloc success");
}

/*
Test(Basic, realloc, .init = setup, .fini = teardown)
{
	void *tmp = my_malloc(10);

	if (!tmp)
		cr_assert(0, "Malloc fail");
	tmp = my_realloc(tmp, 15);
	if (!tmp)
		cr_assert(0, "Realloc fail");
	cr_expect(1, "Realloc success");
}
*/
