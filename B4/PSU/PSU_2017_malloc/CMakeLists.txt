cmake_minimum_required(VERSION 3.9)
project(malloc)

set(CMAKE_C_STANDARD 90)

FILE(GLOB src src/*.c src/*.h)
FILE(GLOB tests tests/*.c tests/*.h)

add_executable(dont_use ${src} ${tests})
