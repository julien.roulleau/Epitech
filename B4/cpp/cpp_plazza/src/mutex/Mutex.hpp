/*
** EPITECH PROJECT, 2018
** plazza
** File description:
** Mutex.hpp
*/


#ifndef PLAZZA_MUTEX_HPP
#define PLAZZA_MUTEX_HPP

#include <pthread.h>

class Mutex {
public:
	Mutex();
	~Mutex() = default;

	void lock();
	void unlock();
	void trylock();

private:
	pthread_mutex_t _mutex;
};


#endif /* PLAZZA_MUTEX_HPP */
