/*
** EPITECH PROJECT, 2018
** plazza
** File description:
** 04/05/18: window.hpp
*/

#ifndef PLAZZA_WINDOW_HPP
#define PLAZZA_WINDOW_HPP

#include <QApplication>
#include <QPushButton>
#include <QLCDNumber>
#include <QSlider>
#include <QLineEdit>
#include <QObject>
#include <QCheckBox>
#include <QLabel>
#include <iostream>
#include <QTimer>
#include "cmd/CmdQueue.hpp"

class MyWindow : public QWidget
{
	Q_OBJECT

public:
	MyWindow(CmdQueue *q);
	~MyWindow();

public slots:
	void pushQueue();
private:
	QPushButton *buttonPush;
	QLineEdit *textZone;
	QCheckBox *Phone_Box;
	QCheckBox *Mail_Box;
	QCheckBox *Ip_Box;
	QLabel *Phone_label;
	QLabel *Mail_label;
	QLabel *Ip_label;
	CmdQueue *queue;
	QTimer *output;
};
#endif //PLAZZA_WINDOW_HPP
