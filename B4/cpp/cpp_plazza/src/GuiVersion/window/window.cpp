/*
** EPITECH PROJECT, 2018
** plazza
** File description:
** 04/05/18: window.cpp
*/

#include "window.hpp"

MyWindow::MyWindow(CmdQueue *q) : QWidget()
{
	queue = q;
	setFixedSize(1000, 700);

	buttonPush = new QPushButton(this);
	buttonPush->setText("Scrap");
	buttonPush->move(750, 10);
	buttonPush->setFixedSize(70, 20);

	textZone = new QLineEdit(this);
	textZone->move(10, 10);
	textZone->setFixedSize(700, 20);

	Phone_Box = new QCheckBox(this);
	Mail_Box = new QCheckBox(this);
	Ip_Box = new QCheckBox(this);

	Phone_Box->setFixedSize(10, 10);
	Phone_Box->move(20, 35);
	Ip_Box->setFixedSize(10, 10);
	Ip_Box->move(140, 35);
	Mail_Box->setFixedSize(10, 10);
	Mail_Box->move(260, 35);

	Phone_label = new QLabel(this);
	Ip_label = new QLabel(this);
	Mail_label = new QLabel(this);

	Phone_label->setText("PHONE_NUMBER");
	Phone_label->move(35, 35);
	Ip_label->setText("IP_ADDRESS");
	Ip_label->move(155, 35);
	Mail_label->setText("EMAIL_ADDRESS");
	Mail_label->move(275, 35);

	QObject::connect(buttonPush, SIGNAL(clicked()),
			 this, SLOT(pushQueue()));
}

MyWindow::~MyWindow()
{
	delete buttonPush;
	delete textZone;
	delete Mail_label;
	delete Mail_Box;
	delete Ip_label;
	delete Ip_Box;
	delete Phone_Box;
	delete Phone_label;
}

void MyWindow::pushQueue()
{
	std::string fileName = (textZone->text()).toStdString();
	textZone->clear();
	if (Phone_Box->isChecked())
		queue->push((cmd_t) {fileName, PHONE_NUMBER});
	if (Ip_Box->isChecked())
		queue->push((cmd_t) {fileName, IP_ADDRESS});
	if (Mail_Box->isChecked())
		queue->push((cmd_t) {fileName, EMAIL_ADDRESS});
}

