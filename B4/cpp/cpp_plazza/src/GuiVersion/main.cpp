/*
** EPITECH PROJECT, 2018
** plazza
** File description:
** 04/05/18: main.cpp
*/

#include <iostream>
#include "cmd/CmdQueue.hpp"
#include "thread/Thread.hpp"
#include "GuiVersion/window/window.hpp"
#include "slave/SlaveManager.hpp"

void *test(void *data)
{
	auto *sm = (SlaveManager *) data;
	sm->run();
	return nullptr;
}

int main(int ac, char **av)
{
	if (ac != 2)
		return 84;
	int mb_thread = atoi(av[1]);
	if (mb_thread < 1)
		return 84;
	CmdQueue cmd;
	Thread manager;
	Pipe pipe;
	pipe.link_cout();
	SlaveManager *sm = SlaveManager::getInstance(mb_thread, &cmd, pipe);
	if (manager.create(nullptr, test, (void *) sm))
		return 84;

	QApplication app(ac, av);
	MyWindow window(&cmd);
	window.show();
	app.exec();

	sm->stop();
	manager.join();
	return 0;
}