/*
** EPITECH PROJECT, 2018
** plazza
** File description:
** Pipe.hpp
*/


#ifndef PLAZZA_PIPE_HPP
#define PLAZZA_PIPE_HPP

#include <iostream>

class Pipe {
public:
	Pipe();
	~Pipe() = default;

	int close_in();
	int close_out();

	bool link_cout();

	bool send(std::string &str);
	bool recv(std::string &str);
	bool recv(std::string &str, char delim);

private:
	bool can_use();
	std::string gnl(char delim);

	int _in;
	int _out;
};


#endif /* PLAZZA_PIPE_HPP */
