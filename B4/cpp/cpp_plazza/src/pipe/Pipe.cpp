/*
** EPITECH PROJECT, 2018
** plazza
** File description:
** Pipe.cpp
*/


#include <zconf.h>
#include <fstream>
#include "Pipe.hpp"

Pipe::Pipe()
{
	int pipes[2];
	if (pipe(pipes) == -1) {
		perror("pipe");
		exit(84);
	}
	_out = pipes[0];
	_in = pipes[1];
}


int Pipe::close_in()
{
	if (_in == 1)
		return -1;
	int ret = close(_in);
	_in = -1;
	return ret;
}

int Pipe::close_out()
{
	int ret = close(_out);
	_out = -1;
	return ret;
}

bool Pipe::send(std::string &str)
{
	if (!can_use())
		return false;
	return write(_in, str.c_str(), str.size()) != -1;
}

bool Pipe::recv(std::string &str)
{
	return this->recv(str, '\n');
}

bool Pipe::recv(std::string &str, char delim)
{
	if (!can_use())
		return false;
	std::string buffer = this->gnl(delim);
	if (buffer.empty()) {
		return false;
	} else {
		str = buffer;
		return true;
	}
}

bool Pipe::can_use()
{
	return !((_in == -1 && _out == -1) || (_in != -1 && _out != -1));
}

std::string Pipe::gnl(char delim)
{
	std::string line;
	char buff;

	while (read(_out, &buff, 1) > 0) {
		if (buff == delim)
			return line;
		line += buff;
	}
	return (line);
}

bool Pipe::link_cout()
{
	if (_in == -1)
		return false;
	close(_in);
	_in = 1;
	return true;
}
