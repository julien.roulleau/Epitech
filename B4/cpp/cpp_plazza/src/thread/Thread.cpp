/*
** EPITECH PROJECT, 2018
** plazza
** File description:
** Thread.cpp
*/


#include "Thread.hpp"

int Thread::create(const pthread_attr_t *__restrict attr,
		   void *(*start_routine)(void *),
		   void *__restrict arg)
{
	return pthread_create(&_th, attr, start_routine, arg);
}

int Thread::tryjoin()
{
	return pthread_tryjoin_np(_th, &_ret);
}

int Thread::join()
{
	return pthread_join(_th, &_ret);
}
