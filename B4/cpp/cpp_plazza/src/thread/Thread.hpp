/*
** EPITECH PROJECT, 2018
** plazza
** File description:
** Thread.hpp
*/


#ifndef PLAZZA_THREAD_HPP
#define PLAZZA_THREAD_HPP

#include <pthread.h>

class Thread {
public:
	Thread() = default;
	~Thread() = default;
	int create(const pthread_attr_t *__restrict __attr,
		   void *(*__start_routine)(void *),
		   void *__restrict __arg);
	int join();
	int tryjoin();

private:
	pthread_t _th{};
	void *_ret = nullptr;
};


#endif /* PLAZZA_THREAD_HPP */
