/*
** EPITECH PROJECT, 2018
** plazza
** File description:
** information.hpp
*/


#ifndef PLAZZA_INFORMATION_HPP
#define PLAZZA_INFORMATION_HPP

enum Information {
	PHONE_NUMBER,
	EMAIL_ADDRESS,
	IP_ADDRESS
};

#endif //PLAZZA_INFORMATION_HPP
