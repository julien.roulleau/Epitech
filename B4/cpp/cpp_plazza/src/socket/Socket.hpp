/*
** EPITECH PROJECT, 2018
** plazza
** File description:
** Socket.hpp
*/


#ifndef PLAZZA_SOCKET_HPP
#define PLAZZA_SOCKET_HPP

#include <iostream>
#include <cmd/CmdQueue.hpp>

enum Status {
	OK,
	BUSY,
	QUIT,
	NOTHING,
	ERROR,
};

class Socket {
public:
	Socket();
	~Socket();

	int close_parrent();
	int close_child();

	Status send_task(cmd_t &cmd);
	Status recv_task(cmd_t &cmd, Status status);

private:
	std::string serialise_cmd(cmd_t &cmd);
	cmd_t unserialise_cmd(std::string &str);

	bool can_use();
	int use();

	int _fdp;
	int _fdc;
};


#endif /* PLAZZA_SOCKET_HPP */
