/*
** EPITECH PROJECT, 2018
** plazza
** File description:
** Socket.cpp
*/


#include <sys/socket.h>
#include <cstdio>
#include <cstdlib>
#include <zconf.h>
#include <sstream>
#include <cmd/CmdQueue.hpp>
#include "Socket.hpp"

Socket::Socket()
{
	int fds[2];
	if (socketpair(AF_UNIX, SOCK_STREAM, 0, fds) < 0) {
		perror("socket pair");
		exit(84);
	}
	_fdp = fds[0];
	_fdc = fds[1];
}

Socket::~Socket()
{
	if (_fdp != -1)
		close(_fdp);
	if (_fdc != -1)
		close(_fdc);
}

int Socket::close_parrent()
{
	int ret = close(_fdp);
	_fdp = -1;
	return ret;
}

int Socket::close_child()
{
	int ret = close(_fdc);
	_fdc = -1;
	return ret;
}

bool Socket::can_use()
{
	return !((_fdp == -1 && _fdc == -1) || (_fdp != -1 && _fdc != -1));
}

int Socket::use()
{
	if (!can_use())
		return -1;
	if (_fdp != -1)
		return _fdp;
	else if (_fdc != -1)
		return _fdc;
	else
		return -1;
}

Status Socket::send_task(cmd_t &cmd)
{
	if (!can_use())
		return Status::ERROR;
	std::string msg = serialise_cmd(cmd);
	unsigned long sss = msg.size();
	ssize_t ret = send(this->use(), &sss, sizeof(int), 0);
	if (ret <= 0)
		return ret == 0 ? Status::QUIT : Status::ERROR;
	Status status;
	ret = recv(this->use(), &status, sizeof(Status), 0);
	if (ret <= 0)
		return ret == 0 ? Status::QUIT : Status::ERROR;
	if (status == OK) {
		ret = send(this->use(), msg.c_str(),
			   sizeof(char) * msg.size(), 0);
		if (ret <= 0)
			return ret == 0 ? Status::QUIT : Status::ERROR;
	}
	return status;
}

Status Socket::recv_task(cmd_t &cmd, Status status)
{
	int size;
	if (!can_use())
		return Status::ERROR;
	ssize_t ret = recv(this->use(), &size, sizeof(Status), MSG_DONTWAIT);
	if (ret <= 0)
		return ret == 0 ? Status::QUIT :
		       (errno == EAGAIN ? Status::NOTHING : Status::ERROR);
	Status stat = Status::OK;
	if (status == OK) {
		ret = send(this->use(), &stat, sizeof(Status), 0);
		if (ret <= 0)
			return ret == 0 ? Status::QUIT : Status::ERROR;
		std::vector<char> buffer(size);
		ret = recv(this->use(), &buffer[0], buffer.size(), 0);
		if (ret <= 0)
			return ret == 0 ? Status::QUIT : Status::ERROR;
		std::string msg_cmd(buffer.data());
		cmd = unserialise_cmd(msg_cmd);
		return Status::OK;
	}
	stat = Status::BUSY;
	ret = send(this->use(),  &stat,
		   sizeof(Status), 0);
	return ret > 0 ? Status::OK : (ret == 0 ? Status::QUIT : Status::ERROR);
}

std::string Socket::serialise_cmd(cmd_t &cmd)
{
	std::ostringstream oss;
	oss << cmd.info << cmd.file;
	return oss.str();
}

cmd_t Socket::unserialise_cmd(std::string &str)
{
	int info = str[0] - '0';
	str.erase(0, 1);
	return (cmd_t) {str, (Information) info};
}
