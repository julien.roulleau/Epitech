/*
** EPITECH PROJECT, 2018
** plazza
** File description:
** interpreter.cpp
*/


#include <iostream>
#include <sstream>
#include <list>
#include <regex>
#include "cmd/CmdQueue.hpp"
#include "interpreter.hpp"

static bool get_info(std::string &buffer, Information &info)
{
	if (buffer == "PHONE_NUMBER")
		info = PHONE_NUMBER;
	else if (buffer == "EMAIL_ADDRESS")
		info = EMAIL_ADDRESS;
	else if (buffer == "IP_ADDRESS")
		info = IP_ADDRESS;
	else {
		std::cerr << "Invalid option" << std::endl;
		return false;
	}
	return true;
}


static std::string clean_space(std::string &str)
{
	str = std::regex_replace(str, std::regex("^ +"), "");
	str = std::regex_replace(str, std::regex(" +$"), "");
	str = std::regex_replace(str, std::regex(" +"), " ");
	return str;
}

static CmdQueue parse_command(std::string &input)
{
	CmdQueue cmd_queue;
	std::string buffer;
	std::stringstream input_cmd(input);
	while (getline(input_cmd, buffer, ';')) {
		std::stringstream input_arg(clean_space(buffer));
		std::string arg;
		std::list<std::string> args;
		while (getline(input_arg, arg, ' '))
			args.push_back(arg);
		Information info;
		if (!get_info(args.back(), info))
			continue;
		args.pop_back();
		while (!args.empty()) {
			cmd_queue.push((cmd_t) {args.front(), info});
			args.pop_front();
		}
	}
	return cmd_queue;
}

void *interpreter(void *data)
{
	auto *cmd_queue = (CmdQueue *) data;
	std::string buffer;

	while (std::getline(std::cin, buffer)) {
		cmd_queue->merge(parse_command(buffer));
	}
	return nullptr;
}
