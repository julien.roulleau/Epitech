/*
** EPITECH PROJECT, 2018
** plazza
** File description:
** interpreter.hpp
*/


#ifndef PLAZZA_INTERPRETER_HPP
#define PLAZZA_INTERPRETER_HPP

#include "cmd/CmdQueue.hpp"

void *interpreter(void *data);

#endif //PLAZZA_INTERPRETER_HPP
