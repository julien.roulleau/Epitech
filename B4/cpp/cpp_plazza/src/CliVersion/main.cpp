/*
** EPITECH PROJECT, 2018
** my_ftp
** File description:
** main.cpp
*/

#include <iostream>
#include <zconf.h>
#include "cmd/CmdQueue.hpp"
#include "thread/Thread.hpp"
#include "interpreter/interpreter.hpp"
#include "slave/SlaveManager.hpp"

void *test(void *data)
{
	auto *sm = (SlaveManager *) data;
	sm->run();
	return nullptr;
}

int main(int ac, char **av)
{
	if (ac != 2)
		return 84;
	int mb_thread = atoi(av[1]);
	if (mb_thread < 1)
		return 84;
	Thread interpret;
	CmdQueue cmd;
	if (interpret.create(nullptr, interpreter, (void *) &cmd))
		return 84;
	Thread manager;
	Pipe pipe;
	pipe.link_cout();
	SlaveManager *sm = SlaveManager::getInstance(mb_thread, &cmd, pipe);
	if (manager.create(nullptr, test, (void *) sm))
		return 84;
	interpret.join();
	sm->stop();
	manager.join();
	return 0;
}
