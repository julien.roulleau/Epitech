/*
** EPITECH PROJECT, 2018
** plazza
** File description:
** SlaveManager.hpp
*/


#ifndef PLAZZA_SLAVEMANAGER_HPP
	#define PLAZZA_SLAVEMANAGER_HPP

#include <vector>
#include <sys/types.h>
#include <sys/wait.h>
#include "slave/Slave.hpp"

class SlaveManager {
public:
	~SlaveManager();
	static SlaveManager *getInstance(int nbThread, CmdQueue *queue,
					const Pipe &pipe);
	void run();
	void stop();
	static void handleChild(int sig);
	static int _nbSlave;
	static  Mutex *_lockNbSlave;
private:
	SlaveManager(int nbThread, CmdQueue *queue, const Pipe &pipe);
	std::vector<Slave*> slaves;
	void createSlave();
	void handleRequest();
	int _nbThread { 0 };
	bool stopped { false };
	CmdQueue *_queue;
	const Pipe _pipe;
	static SlaveManager *_instance;
};
#endif /* PLAZZA_SLAVEMANAGER_HPP */