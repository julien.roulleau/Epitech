/*
** EPITECH PROJECT, 2018
** plazza
** File description:
** Slave.cpp
*/


#include "Slave.hpp"
#include "file_parser/FileParser.hpp"
#include <stdexcept>
#include <iostream>
#include <utility>

/**
 *
 * @param maxThread
 * @param queue
 */
 Slave::Slave(int maxThread, Socket *socket, const Pipe &pipe)
	: _maxThread(maxThread), _socket(socket), _pipe(pipe)
{
	threads = new std::vector<Thread*>();
}

Slave::~Slave()
{
	for (auto &thread : *threads) {
		delete(thread);
	}
}

pid_t Slave::getPid()
{
	return _pid;
}

bool Slave::manage()
{
	_pid = fork();
	switch (_pid) {
		case -1:
			perror("Slave Creation");
			return false;
		case 0:
			_socket->close_parrent();
			_pipe.close_out();
			runProcess();
			break;
		default:
			_socket->close_child();
			break;
	}
	return  true;
}

void Slave::setDead()
{
	dead = true;
}

bool Slave::isDead()
{
	return  dead;
}

void Slave::bind(cmd_t cmd)
{
	Thread *n_thread;
	data_thread_t data;

	if (!full) {
		n_thread = new Thread;
		data.cmd = std::move(cmd);
		data.pipe = _pipe;
		data.parent = this;
		n_thread->create(nullptr, &Slave::routineTread, &data);
		threads->push_back(n_thread);
		_mutex.lock();
		currentNbThread++;
		_mutex.unlock();
	}
	full =  (currentNbThread == _maxThread);

}

void Slave::runProcess()
{
	Status status;
	cmd_t cmd;

	while (true) {
		status = OK;
		if (full && !dead)
			status = BUSY;
		else if (dead) {
			exit (0);
		}
		status = _socket->recv_task(cmd, status);
		if (!dead && status == OK) {
				bind(cmd);
		}
	}
}

void Slave::minusThread()
{
	_mutex.lock();
	currentNbThread--;
	if (currentNbThread == 0)
		setDead();
	_mutex.unlock();
}

void *Slave::routineTread(void *arg)
{
	auto *item = (data_thread_t*) arg;
	cmd_t cmd = item->cmd;
	Pipe pipe = item->pipe;

	auto *parser = new FileParser(cmd.file, cmd.info, &pipe);
	parser->pars();
	item->parent->minusThread();
	pthread_exit(nullptr);
}

Socket* Slave::getSocket()
{
	return  _socket;
}