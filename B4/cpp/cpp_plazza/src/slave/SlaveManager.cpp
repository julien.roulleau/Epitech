/*
** EPITECH PROJECT, 2018
** plazza
** File description:
** SlaveManager.cpp
*/


#include "SlaveManager.hpp"
#include <iostream>
#include <cmd/CmdQueue.hpp>

SlaveManager *SlaveManager::_instance = nullptr;
Mutex *SlaveManager::_lockNbSlave = nullptr;
int SlaveManager::_nbSlave = 0;

SlaveManager::SlaveManager(int nbThread, CmdQueue *queue, const Pipe &pipe)
	: _nbThread(nbThread), _queue(queue), _pipe(pipe)
{
	signal(SIGCHLD, SlaveManager::handleChild);
}

SlaveManager::~SlaveManager() = default;



void SlaveManager::handleChild(int sig)
{
	int status;
	int pid = wait(&status);

	(void) sig;
	if (pid == -1) {
		perror("wait");
		exit(84);
	}
	SlaveManager::_lockNbSlave->trylock();
	SlaveManager::_nbSlave--;
	SlaveManager::_lockNbSlave->unlock();
	std::cerr << " Process: " << pid << " return " << status << std::endl;
}

void SlaveManager::run()
{
	while (!_queue->empty() || !stopped) {
		handleRequest();
		if (!_queue->empty())
			createSlave();
	}
}

void SlaveManager::stop()
{
	stopped = true;
}

void SlaveManager::handleRequest()
{
	cmd_t cmd;
	Status status;
	bool pop = true;

	if (_queue->empty())
		return;
	cmd.file = "";
	for (auto &slave : slaves) {
		if (slave->isDead())
			continue;
		if (pop) {
			if (!_queue->tryPop(cmd))
				return;
		}
		status = slave->getSocket()->send_task(cmd);
		if (status == QUIT)
			slave->setDead();
		pop = (status == OK);
	}
	if (!pop)
		_queue->push(cmd);
}


/**
 *
 * @param nbThread maximum thread authorized
 * @param queue treatment
 * @return SlaveManager
 */
SlaveManager *SlaveManager::getInstance(int nbThread,
					CmdQueue *queue, const Pipe &pipe)
{
	if (SlaveManager::_instance == nullptr) {
		SlaveManager::_instance = new SlaveManager(nbThread, queue,
							   pipe);
		SlaveManager::_lockNbSlave = new Mutex;
	}
	return SlaveManager::_instance;
}

void SlaveManager::createSlave()
{
	Socket *socket;
	Slave *slave;

	socket = new Socket;
	slave = new Slave(_nbThread, socket, _pipe);
	if (slave->manage()) {
		slaves.push_back(slave);
		SlaveManager::_lockNbSlave->trylock();
		SlaveManager::_nbSlave++;
		SlaveManager::_lockNbSlave->unlock();
	}
}