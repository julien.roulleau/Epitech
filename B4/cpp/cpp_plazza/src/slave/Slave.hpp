/*
** EPITECH PROJECT, 2018
** plazza
** File description:
** Slave.hpp
*/


#ifndef PLAZZA_SLAVE_HPP
	# define PLAZZA_SLAVE_HPP

#include <cerrno>
#include <cstdio>
#include <unistd.h>
#include <cstdlib>
#include <sys/types.h>
#include <sys/wait.h>
#include "cmd/CmdQueue.hpp"
#include "thread/Thread.hpp"
#include "socket/Socket.hpp"
#include "pipe/Pipe.hpp"

class Slave {
public:
	Slave(int maxThread, Socket *socket, const Pipe &pipe);
	~Slave();
	void bind(cmd_t cmd);
	Socket *getSocket();
	pid_t getPid();
	void setDead();
	bool isDead();
	void minusThread();
	static void *routineTread(void *arg);
	bool manage();
private:
	void runProcess();
	bool full { false };
	bool dead { false };
	int _maxThread;
	Socket *_socket;
	Pipe _pipe;
	Mutex _mutex;
	int currentNbThread { 0 };
	pid_t _pid;
	std::vector<Thread*> *threads;
};

typedef struct {
	cmd_t cmd;
	Slave *parent;
	Pipe pipe;
} data_thread_t;


#endif /* PLAZZA_SLAVE_HPP */
