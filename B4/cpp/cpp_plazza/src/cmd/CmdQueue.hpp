/*
** EPITECH PROJECT, 2018
** plazza
** File description:
** cmd.hpp
*/


#ifndef PLAZZA_CMD_HPP
#define PLAZZA_CMD_HPP

#include <queue>
#include <string>
#include "mutex/Mutex.hpp"
#include "information/information.hpp"

typedef struct {
	std::string file;
	Information info;
} cmd_t;

class CmdQueue {
public:
	CmdQueue() = default;
	~CmdQueue() = default;
	void push(cmd_t value);
	bool tryPop(cmd_t &value);

	bool empty();

	void merge(CmdQueue src);
private:
	std::queue<cmd_t> _queue;
	Mutex _mutex;
};


#endif /* PLAZZA_CMD_HPP */
