/*
** EPITECH PROJECT, 2018
** plazza
** File description:
** cmd.cpp
*/

#include "CmdQueue.hpp"

void CmdQueue::push(cmd_t value)
{
	_queue.push(value);
}

bool CmdQueue::tryPop(cmd_t &value)
{
	_mutex.lock();
	if (_queue.empty()) {
		_mutex.unlock();
		return false;
	} else {
		value = _queue.front();
		_queue.pop();
		_mutex.unlock();
		return true;
	}
}

void CmdQueue::merge(CmdQueue src)
{
	cmd_t tmp;

	while (src.tryPop(tmp)) {
		_queue.push(tmp);
	}
}

bool CmdQueue::empty()
{
	return _queue.empty();
}
