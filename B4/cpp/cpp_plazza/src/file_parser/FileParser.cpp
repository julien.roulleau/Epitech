/*
** EPITECH PROJECT, 2018
** plazza
** File description:
** 30/04/18: FileParser.cpp
*/

#include <dlfcn.h>
#include "FileParser.hpp"

void FileParser::setFile(const std::string &fileName)
{
	file = fileName;
}

void FileParser::setType(const Information &inf)
{
	type = inf;
}

void FileParser::setData(const std::string &dataLine)
{
	data.emplace_back(dataLine);
}

std::string FileParser::getFile() const
{
	return file;
}

Information FileParser::getType() const
{
	return type;
}

std::vector<std::string> FileParser::getData() const
{
	return data;
}

std::string FileParser::serialiaze() const
{
	return file;
}

void FileParser::unserialiaze(std::string const &data)
{
	(void)data;
}

FileParser::~FileParser() = default;

FileParser::FileParser(std::string &fileName, Information infType, Pipe *pipe_)
	: file(fileName), type(infType), pipe(pipe_)
{
}

FileParser::FileParser(std::string &fileName, Information infType) :
	file(fileName), type(infType), pipe(nullptr)
{
}

std::vector<std::string> &FileParser::pars() {
	if (!data.empty())
		return data;
	std::smatch m;
	std::ifstream fileContent;
	std::string line;

	if (infReg.find(type) == infReg.end()) {
		std::cerr << file << ": Unknow Type" << std::endl;
		return data;
	}
	fileContent.open(file);
	if (!fileContent.is_open()) {
		std::cerr << file << ": Unknow File" << std::endl;
		return data;
	}
	while (getline(fileContent, line)) {
		while (std::regex_search (line, m, infReg.find(type)->second)) {
			data.emplace_back(*(m.begin()));
			line = m.suffix().str();
		}
	}
	fileContent.close();
	for (auto it = data.begin(); it != data.end(); ++it) {
		if (pipe != nullptr) {
			(*it) += "\n";
			pipe->send(*it);
		}
		else
			std::cout << *it << std::endl;
	}
	return data;
}

std::ostream& operator<<(std::ostream &flux, const FileParser &pars)
{
	flux << pars.serialiaze();
	return flux;
}

void operator>>(std::string &flux, FileParser &pars)
{
	pars.unserialiaze(flux);
}

void operator<<(FileParser &pars, std::string &flux)
{
	pars.unserialiaze(flux);
}

std::ostream& operator>>(const FileParser &pars, std::ostream &flux)
{
	flux << pars.serialiaze();
	return flux;
}