/*
** EPITECH PROJECT, 2018
** plazza
** File description:
** 30/04/18: FileParser.hpp
*/

#ifndef PLAZZA_FILEPARSER_HPP
#define PLAZZA_FILEPARSER_HPP

#include <iostream>
#include <vector>
#include <regex>
#include <fstream>
#include <exception>
#include <map>
#include "information/information.hpp"
#include "pipe/Pipe.hpp"

const std::map<Information, std::regex> infReg = {
	{PHONE_NUMBER, std::regex{"([0-9] ?){10}"}},
	{EMAIL_ADDRESS, std::regex{"[a-zA-Z0-9_.-]+@[a-zA-Z0-9_.-]+"}},
	{IP_ADDRESS, std::regex{
		R"(([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3}))"}}
};

class FileParser {
public:
	FileParser(std::string &file, Information infType);
	FileParser(std::string &file, Information infType, Pipe *pipe);
	~FileParser();
	void setFile(const std::string &file);
	void setType(const Information &inf);
	void setData(const std::string &data);
	std::string getFile() const;
	Information getType() const;
	std::vector<std::string> getData() const;
	std::string serialiaze() const;
	void unserialiaze(std::string const &data);
	std::vector<std::string> &pars();
private:
	std::string file;
	Information type;
	std::vector<std::string> data;
	Pipe *pipe;
};

std::ostream& operator<<(std::ostream &flux, const FileParser &pars);
void operator>>(std::ostream &flux, FileParser &pars);

void operator<<(FileParser &pars, std::ostream &flux);
std::ostream& operator>>(const FileParser &pars, std::ostream &flux);

#endif //PLAZZA_FILEPARSER_HPP
