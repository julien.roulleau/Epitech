QT += widgets

INCLUDEPATH += src

QMAKE_CXXFLAGS += -std=c++11

SOURCES += \
    src/cmd/CmdQueue.cpp \
    src/file_parser/FileParser.cpp \
    src/GuiVersion/window/window.cpp \
    src/GuiVersion/main.cpp \
    src/mutex/Mutex.cpp \
    src/pipe/Pipe.cpp \
    src/slave/Slave.cpp \
    src/slave/SlaveManager.cpp \
    src/socket/Socket.cpp \
    src/thread/Thread.cpp

HEADERS += \
    src/cmd/CmdQueue.hpp \
    src/file_parser/FileParser.hpp \
    src/GuiVersion/window/window.hpp \
    src/information/information.hpp \
    src/mutex/Mutex.hpp \
    src/pipe/Pipe.hpp \
    src/slave/Slave.hpp \
    src/slave/SlaveManager.hpp \
    src/socket/Socket.hpp \
    src/thread/Thread.hpp

unix:!macx: LIBS += -lpthread
