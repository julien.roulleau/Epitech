/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** main.cpp
*/

#include "simulator/Simulator.hpp"

int main(int ac, char **av)
{
	Simulator sim;
	list3d<std::string> list;

	if (ac < 2)
		return 0;
	try {
		sim.init(av[1]);
		for (int i = 2; i < ac; i++)
			sim.setInput(av[i]);
		sim.run();
	} catch(std::exception &e) {
		return 84;
	}
	return 0;
}