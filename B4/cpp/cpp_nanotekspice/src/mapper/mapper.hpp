/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** mapper.hpp
*/


#ifndef NANOTECKSPICE_MAPPER_HPP
#define NANOTECKSPICE_MAPPER_HPP

#include <map>
#include "parser/parser.hpp"
#include "component/clock/ClockComponent.hpp"
#include "component/true/TrueComponent.hpp"
#include "component/false/FalseComponent.hpp"
#include "component/Input/InputComponent.hpp"
#include "component/output/OutputComponent.hpp"
#include "component/4071/Component4071.hpp"
#include "component/4001/Component4001.hpp"
#include "component/4081/Component4081.hpp"
#include "component/4030/Component4030.hpp"
#include "component/4011/Component4011.hpp"
#include "component/4069/Component4069.hpp"

typedef std::map<std::string, std::unique_ptr<nts::IComponent>> Component_map_t;

Component_map_t mapper(
	list3d<std::string> list);

#define GETFIRSTPART(str, sep) ((str).substr(0, (str).find(sep)))
#define GETSECONDPART(str, sep) ((str).substr((str).find(sep) + 1))

#endif //NANOTECKSPICE_MAPPER_HPP
