/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** mapper.cpp
*/

#include <memory>
#include <iostream>
#include "mapper.hpp"

std::map<std::string, nts::IComponent *(*)()> Component_list = {
	{"clock", &nts::ClockComponent::create},
	{"input",  &nts::InputComponent::create},
	{"output", &nts::OutputComponent::create},
	{"true", &nts::TrueComponent::create},
	{"false", &nts::FalseComponent::create},
	{"4071", &nts::Component4071::create},
	{"4001", &nts::Component4001::create},
	{"4081", &nts::Component4081::create},
	{"4011", &nts::Component4011::create},
	{"4030", &nts::Component4030::create},
	{"4069", &nts::Component4069::create}
};

std::unique_ptr<nts::IComponent> createComponent(const std::string &type,
						 const std::string &value)
{
	(void) value;

	if (Component_list.count(type))
		return std::unique_ptr<nts::IComponent>(
			Component_list[type]());
	else
		return nullptr;
}

void map_chipsets(Component_map_t &map, list2d<std::string> &chipsets)
{
	while (!chipsets.empty()) {
		std::list<std::string> input = chipsets.front();
		std::string index = input.back() + ';' + input.front();
		map[index] = createComponent(
			input.front(), input.back());
		if (map[index] == nullptr) {
			std::cout << "Unknown component" << std::endl;
			throw std::exception();
		}
		chipsets.pop_front();
	}

}

Component_map_t::iterator get_in_map(
	Component_map_t &map, const std::string &str)
{
	for (auto it = map.begin(); it != map.end(); ++it)
		if (GETFIRSTPART(it->first, ';') == str)
			return it;
	return map.end();
}

void map_links(Component_map_t &map, list2d<std::string> &links)
{
	while (!links.empty()) {
		std::list<std::string> input = links.front();
		std::string front = links.front().front();
		std::string back = links.front().back();
		if (get_in_map(map, GETFIRSTPART(front, ':')) == map.end()
			|| get_in_map(map, GETFIRSTPART(back, ':'))
			   == map.end()) {
			std::cout << "Invalid link" << std::endl;
			throw std::exception();
		}
		get_in_map(map, GETFIRSTPART(front, ':'))->second->setLink(
			static_cast<size_t>(atoi(
				GETSECONDPART(front, ':').c_str())),
			*(get_in_map(map, GETFIRSTPART(back, ':'))->second),
			static_cast<size_t>(atoi(
				GETSECONDPART(back, ':').c_str())));
		links.pop_front();
	}
}


Component_map_t mapper(list3d<std::string> list)
{
	Component_map_t components;
	map_chipsets(components, list.front());
	map_links(components, list.back());
	return components;
}