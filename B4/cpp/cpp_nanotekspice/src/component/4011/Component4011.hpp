/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 04/03/18: Component4011.hpp
*/

#ifndef NANOTECKSPICE_COMPONENT4011_HPP
#define NANOTECKSPICE_COMPONENT4011_HPP

#include <vector>
#include <map>
#include "component/Component.hpp"

namespace nts {
	class Component4011: public Component {
	public:
		Component4011();
		~Component4011() override;
		nts::Tristate compute(std::size_t pin = 1) override;
		void dump() const;

		static IComponent *create()
		{
			return new Component4011();
		}

		std::map<size_t , std::vector<size_t>> relation;
	};
}


#endif //NANOTECKSPICE_COMPONENT4011_HPP
