/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 07/02/18: outputComponent.hpp
*/


#ifndef NANOTECKSPICE_FALSECOMPONENT_HPP
#define NANOTECKSPICE_FALSECOMPONENT_HPP

#include "../Component.hpp"

namespace nts {
	class FalseComponent: public Component {
	public:
		FalseComponent();
		~FalseComponent() override;
		nts::Tristate compute(std::size_t pin = 1) override;
		void dump() const;
		static IComponent *create()
		{
			return new FalseComponent();
		}
	};
}

#endif //NANOTECKSPICE_FALSECOMPONENT_HPP
