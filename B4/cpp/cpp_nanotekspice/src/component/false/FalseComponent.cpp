/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 07/02/18: outputComponent.cpp
*/


#include "FalseComponent.hpp"

nts::FalseComponent::FalseComponent()
{
	pinNb = 1;
	_pinList.emplace_back(Pin(0));
}

nts::FalseComponent::~FalseComponent() = default;

nts::Tristate nts::FalseComponent::compute(size_t pin)
{
	if (pin == 0)
		return clearPin();
	if (pin != 1)
		throw std::exception();
	return FALSE;
}

void nts::FalseComponent::dump() const
{
	std::cout << "- False" << std::endl;
}
