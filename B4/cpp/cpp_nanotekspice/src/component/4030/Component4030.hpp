/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 04/03/18: Component4030.hpp
*/

#ifndef NANOTECKSPICE_COMPONENT4030_HPP
#define NANOTECKSPICE_COMPONENT4030_HPP

#include <vector>
#include <map>
#include "component/Component.hpp"

namespace nts {
	class Component4030: public Component {
	public:
		Component4030();
		~Component4030() override;
		nts::Tristate compute(std::size_t pin = 1) override;
		void dump() const;

		static IComponent *create()
		{
			return new Component4030();
		}

		std::map<size_t , std::vector<size_t>> relation;
	};
}


#endif //NANOTECKSPICE_COMPONENT4030_HPP
