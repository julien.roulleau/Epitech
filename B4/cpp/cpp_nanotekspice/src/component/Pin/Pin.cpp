/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 08/02/18: Pin.cpp
*/


#include "Pin.hpp"

nts::Pin::Pin(size_t id, size_t linkId, nts::IComponent *linkComponent) :
	state(nts::UNDEFINED), _id(id), _linkPinId(linkId), _linkComponent(linkComponent)
{
}

nts::Pin::Pin(size_t id) : state(nts::UNDEFINED), _id(id), _linkPinId(0), _linkComponent(nullptr)
{
}


nts::Pin::~Pin()
{
}

size_t nts::Pin::getLinkPinId() const
{
	return _linkPinId;
}

nts::IComponent *nts::Pin::getLinkComponent() const
{
	return _linkComponent;
}

size_t nts::Pin::getId() const
{
	return _id;
}

void nts::Pin::setLinkPinId(size_t id)
{
	_linkPinId = id;
}

void nts::Pin::setLinkComponent(nts::IComponent *linkCpnt)
{
	_linkComponent = linkCpnt;
}
