/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 08/02/18: Pin.hpp
*/

#ifndef NANOTECKSPICE_PIN_HPP
#define NANOTECKSPICE_PIN_HPP

#include "../IComponent.hpp"

namespace nts {
	class Pin {
	public:
		Pin(size_t id);
		Pin(size_t id, size_t linkId, nts::IComponent *linkCpnt);
		~Pin();
		size_t getLinkPinId() const;
		nts::IComponent *getLinkComponent() const;
		size_t getId() const;
		void setLinkPinId(size_t id);
		void setLinkComponent(nts::IComponent *linkCpnt = nullptr);

	public:
		nts::Tristate state;
	private:
		size_t _id;
		size_t _linkPinId;
		nts::IComponent *_linkComponent;
	};
}

#endif //NANOTECKSPICE_PIN_HPP
