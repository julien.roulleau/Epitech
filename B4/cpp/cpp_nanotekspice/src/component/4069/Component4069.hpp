/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 04/03/18: Component4069.hpp
*/

#ifndef NANOTECKSPICE_COMPONENT4069_HPP
#define NANOTECKSPICE_COMPONENT4069_HPP

#include <vector>
#include <map>
#include <component/4069/Component4069.hpp>
#include "component/Component.hpp"

namespace nts {
	class Component4069: public Component {
	public:
		Component4069();
		~Component4069() override;
		nts::Tristate compute(std::size_t pin = 1) override;
		void dump() const;

		static IComponent *create()
		{
			return new Component4069();
		}

		std::map<size_t , size_t> relation;
	};
}

#endif //NANOTECKSPICE_COMPONENT4069_HPP
