/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 04/03/18: Component4069.cpp
*/

#include "Component4069.hpp"

nts::Component4069::Component4069()
{
	pinNb = 14;
	for (unsigned int i = 0; i < pinNb; i++)
		_pinList.emplace_back(Pin(i));
	relation.emplace(2, 1);
	relation.emplace(4, 3);
	relation.emplace(6, 5);
	relation.emplace(8, 9);
	relation.emplace(10, 11);
	relation.emplace(12, 13);
}

nts::Component4069::~Component4069() = default;

nts::Tristate nts::Component4069::compute(size_t pin)
{
	if (pin == 0)
		return clearPin();
	if ((_pinList.at(pin - 1).state) != nts::UNDEFINED) {
		return _pinList.at(pin - 1).state;
	}
	if (relation.find(pin) != relation.end()) {
		_pinList.at(pin - 1).state =
			getPin(relation.at(pin) - 1) == nts::FALSE
			? nts::TRUE : nts ::FALSE;
	} else {
		std::cout << "Invalid Request Pin" << std::endl;
		throw std::exception();
	}
	return _pinList.at(pin - 1).state;
}

void nts::Component4069::dump() const
{
	std::cout << "4069" << std::endl;
}
