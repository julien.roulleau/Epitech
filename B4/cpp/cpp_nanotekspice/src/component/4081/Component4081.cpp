/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 04/03/18: Component4081.cpp
*/

#include "Component4081.hpp"

nts::Component4081::Component4081()
{
	pinNb = 14;
	for (unsigned int i = 0; i < pinNb; i++)
		_pinList.emplace_back(Pin(i));
	relation.emplace(3, std::vector<size_t>());
	relation.at(3).push_back(1);
	relation.at(3).push_back(2);
	relation.emplace(4, std::vector<size_t>());
	relation.at(4).push_back(5);
	relation.at(4).push_back(6);
	relation.emplace(10, std::vector<size_t>());
	relation.at(10).push_back(8);
	relation.at(10).push_back(9);
	relation.emplace(11, std::vector<size_t>());
	relation.at(11).push_back(12);
	relation.at(11).push_back(13);
}

nts::Component4081::~Component4081() = default;

nts::Tristate nts::Component4081::compute(size_t pin)
{
	if (pin == 0)
		return clearPin();
	if ((_pinList.at(pin - 1).state) != nts::UNDEFINED) {
		return _pinList.at(pin - 1).state;
	}
	if (relation.find(pin) != relation.end()) {
		_pinList.at(pin - 1).state =
			(getPin(relation.at(pin).front() - 1) == nts::TRUE &&
				getPin(relation.at(pin).back() - 1) == nts::TRUE)
			? nts::TRUE : nts ::FALSE;
	} else {
		std::cout << "Invalid Request Pin" << std::endl;
		throw std::exception();
	}
	return _pinList.at(pin - 1).state;
}

void nts::Component4081::dump() const
{
	std::cout << "4081" << std::endl;
}
