/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 04/03/18: Component4001.hpp
*/

#ifndef NANOTECKSPICE_COMPONENT4001_HPP
#define NANOTECKSPICE_COMPONENT4001_HPP

#include <vector>
#include <map>
#include <component/4001/Component4001.hpp>
#include "component/Component.hpp"

namespace nts {
	class Component4001: public Component {
	public:
		Component4001();
		~Component4001() override;
		nts::Tristate compute(std::size_t pin = 1) override;
		void dump() const;

		static IComponent *create()
		{
			return new Component4001();
		}

		std::map<size_t , std::vector<size_t>> relation;
	};
}

#endif //NANOTECKSPICE_COMPONENT4001_HPP
