/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 07/02/18: inputComponent.hpp
*/


#ifndef NANOTECKSPICE_INPUTCOMPONENT_HPP
#define NANOTECKSPICE_INPUTCOMPONENT_HPP

#include "../Component.hpp"

namespace nts {
	class InputComponent : public Component {
	public:
		InputComponent();
		~InputComponent() override;
		nts::Tristate compute(std::size_t pin = 1) final;
		void dump() const final;

		static IComponent *create()
		{
			return new InputComponent();
		}

	public:
		nts::Tristate value;
	};
}

#endif //NANOTECKSPICE_INPUTCOMPONENT_HPP
