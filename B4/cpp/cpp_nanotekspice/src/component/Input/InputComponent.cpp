/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 07/02/18: InputComponent.cpp
*/


#include "InputComponent.hpp"

nts::InputComponent::InputComponent() : value(nts::UNDEFINED)
{
	pinNb = 1;
	for (size_t i = 1; i <= pinNb; i++)
		_pinList.emplace_back(Pin(i));
}

nts::InputComponent::~InputComponent() = default;

nts::Tristate nts::InputComponent::compute(size_t pin)
{
	if (pin == 0)
		return clearPin();
	if (pin == 2 || pin == 3) {
		value = pin == 2 ? nts::TRUE : nts::FALSE;
		return value;
	}
	if (pin > 3)
		throw std::exception();
	if (_pinList.at(0).state != nts::UNDEFINED)
		return _pinList.at(0).state;
	_pinList.at(0).state = value;
	if (_pinList.at(0).state == nts::UNDEFINED)
		throw std::exception();
	return _pinList.at(0).state;
}

void nts::InputComponent::dump() const
{
	std::cout << "- Input: ";
	if (value == nts::TRUE)
		std::cout << "True" << std::endl;
	else if (value == nts::FALSE)
		std::cout << "False" << std::endl;
	else
		std::cout << "Undefined" << std::endl;
}
