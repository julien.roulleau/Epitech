/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 09/02/18: Component.cpp
*/


#include "Component.hpp"

nts::Component::Component() : pinNb(0)
{
}

nts::Tristate nts::Component::compute(size_t pin)
{
	if (pin == 0)
		clearPin();
	return nts::UNDEFINED;
}

void nts::Component::setLink(std::size_t pin, nts::IComponent &other,
			std::size_t otherPin)
{
	if (pin > pinNb)
		throw std::exception();
	if (_pinList[pin - 1].getLinkPinId() == otherPin ||
	    _pinList[pin - 1].getLinkComponent() == &other) {
		return;
	}
	if (_pinList[pin - 1].getLinkPinId() != 0 ||
		_pinList[pin - 1].getLinkComponent() != nullptr)
		throw std::exception();
	_pinList[pin - 1].setLinkPinId(otherPin);
	_pinList[pin - 1].setLinkComponent(&other);
	other.setLink(otherPin, *this, pin);
}

void nts::Component::dump() const
{
	std::cout << "- Unknow Component!" << std::endl;
}

nts::Tristate nts::Component::clearPin()
{
	auto itPin = _pinList.begin();
	while (itPin != _pinList.end()) {
		itPin->state = nts::Tristate::UNDEFINED;
		std::advance(itPin, 1);
	}
	return nts::UNDEFINED;
}

nts::Tristate nts::Component::getPin(size_t pin)
{
	if (pin >= pinNb) {
		std::cout << "GetPin: Invalid Request Pin : "
			  << pin << std::endl;
		throw std::exception();
	}
	if (_pinList.at(pin).state == nts::UNDEFINED) {
		if (_pinList.at(pin).getLinkComponent() != nullptr) {
			_pinList.at(pin).state = _pinList.at(pin)
				.getLinkComponent()
				->compute(_pinList.at(pin).getLinkPinId());
		} else {
			std::cout << "GetPin: Unset Pin" << std::endl;
			throw std::exception();
		}
	}
	return _pinList.at(pin).state;
}
