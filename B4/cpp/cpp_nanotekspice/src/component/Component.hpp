/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 09/02/18: Component.hpp
*/


#ifndef NANOTECKSPICE_COMPONENT_HPP
#define NANOTECKSPICE_COMPONENT_HPP

#include <vector>
#include "IComponent.hpp"
#include "Pin/Pin.hpp"

namespace nts {
	class Component : public nts::IComponent {
	public:
		Component();
		~Component() override = default;
		nts::Tristate compute(std::size_t pin = 1) override;
		void setLink(std::size_t pin,
			     nts::IComponent &other,
			     std::size_t otherPin) override;
		void dump() const override;
		nts::Tristate clearPin();
		nts::Tristate getPin(size_t pin);

	protected:
		std::vector<Pin> _pinList;
		size_t pinNb;
	};
}

#endif //NANOTECKSPICE_COMPONENT_HPP
