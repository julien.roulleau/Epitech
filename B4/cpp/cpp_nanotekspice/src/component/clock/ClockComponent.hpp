/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 07/02/18: outputComponent.hpp
*/


#ifndef NANOTECKSPICE_CLOCKCOMPONENT_HPP
#define NANOTECKSPICE_CLOCKCOMPONENT_HPP

#include "component/Component.hpp"

namespace nts {
	class ClockComponent: public Component {
	public:
		ClockComponent();
		~ClockComponent() override;
		nts::Tristate compute(std::size_t pin = 1) final;
		void dump() const final;

		static IComponent *create()
		{
			return new ClockComponent();
		}
	public:
		nts::Tristate value;
	};
}

#endif //NANOTECKSPICE_CLOCKCOMPONENT_HPP
