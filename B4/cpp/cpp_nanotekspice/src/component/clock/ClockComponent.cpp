/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 07/02/18: outputComponent.cpp
*/


#include "ClockComponent.hpp"

nts::ClockComponent::ClockComponent() : value(nts::FALSE)
{
	pinNb = 1;
	_pinList.emplace_back(Pin(0));
}

nts::ClockComponent::~ClockComponent() = default;

nts::Tristate nts::ClockComponent::compute(size_t pin)
{
	if (pin == 0)
		return clearPin();
	if (pin != 1)
		throw std::exception();
	if (_pinList.at(0).state != nts::UNDEFINED)
		return _pinList.at(0).state;
	_pinList.at(0).state = value;
	value = value == nts::TRUE ? nts::FALSE : nts::TRUE;
	if (_pinList.at(0).state == nts::UNDEFINED)
		throw std::exception();
	return _pinList.at(0).state;
}

void nts::ClockComponent::dump() const
{
	std::cout << "- Clock: ";
	if (value == nts::TRUE)
		std::cout << "True" << std::endl;
	else if (value == nts::FALSE)
		std::cout << "False" << std::endl;
	else
		std::cout << "Undefined" << std::endl;
}
