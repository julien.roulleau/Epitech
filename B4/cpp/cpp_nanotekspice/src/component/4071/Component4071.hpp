/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 04/03/18: Component4071.hpp
*/

#ifndef NANOTECKSPICE_COMPONENT4071_HPP
#define NANOTECKSPICE_COMPONENT4071_HPP

#include <vector>
#include <map>
#include "component/Component.hpp"

namespace nts {
	class Component4071: public Component {
	public:
		Component4071();
		~Component4071() override;
		nts::Tristate compute(std::size_t pin = 1) override;
		void dump() const;

		static IComponent *create()
		{
			return new Component4071();
		}

		std::map<size_t , std::vector<size_t>> relation;
	};
}


#endif //NANOTECKSPICE_COMPONENT4071_HPP
