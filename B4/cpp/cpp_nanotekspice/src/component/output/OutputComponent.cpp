/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 07/02/18: outputComponent.cpp
*/


#include "OutputComponent.hpp"

nts::OutputComponent::OutputComponent() : value(nts::UNDEFINED)
{
	pinNb = 1;
	_pinList.emplace_back(Pin(0));
}

nts::OutputComponent::~OutputComponent() = default;

nts::Tristate nts::OutputComponent::compute(size_t pin)
{
	if (pin == 0)
		return clearPin();
	if (pin != 1)
		throw std::exception();
	if (_pinList.at(0).state == nts::UNDEFINED) {
		if (_pinList.at(0).getLinkComponent() != nullptr) {
			value = _pinList.at(0)
				.getLinkComponent()
				->compute(_pinList.at(pin - 1).getLinkPinId());
			_pinList.at(0).state = value;
		} else
			throw std::exception();
	}
	return value;
}

void nts::OutputComponent::dump() const
{
	std::cout << "=";
	if (value == nts::TRUE)
		std::cout << "1" << std::endl;
	else if (value == nts::FALSE)
		std::cout << "0" << std::endl;
	else
		std::cout << "Undefined" << std::endl;
}
