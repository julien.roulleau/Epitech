/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 07/02/18: outputComponent.hpp
*/


#ifndef NANOTECKSPICE_OUTPUTCOMPONENT_HPP
#define NANOTECKSPICE_OUTPUTCOMPONENT_HPP

#include "component/Component.hpp"

namespace nts {
	class OutputComponent: public Component {
	public:
		OutputComponent();
		~OutputComponent() override;
		nts::Tristate compute(std::size_t pin = 1) override;
		void dump() const;

		static IComponent *create()
		{
			return new OutputComponent();
		}
	public:
		nts::Tristate value;
	};
}

#endif //NANOTECKSPICE_OUTPUTCOMPONENT_HPP
