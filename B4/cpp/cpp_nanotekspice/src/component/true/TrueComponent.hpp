/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 07/02/18: outputComponent.hpp
*/


#ifndef NANOTECKSPICE_TRUECOMPONENT_HPP
#define NANOTECKSPICE_TRUECOMPONENT_HPP

#include "component/Component.hpp"

namespace nts {
	class TrueComponent: public Component {
	public:
		TrueComponent();
		~TrueComponent() override;
		nts::Tristate compute(std::size_t pin = 1) override;
		void dump() const;
		static IComponent *create()
		{
			return new TrueComponent();
		}
	};
}

#endif //NANOTECKSPICE_TRUECOMPONENT_HPP
