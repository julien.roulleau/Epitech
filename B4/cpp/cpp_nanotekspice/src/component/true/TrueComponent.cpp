/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 07/02/18: outputComponent.cpp
*/


#include "TrueComponent.hpp"

nts::TrueComponent::TrueComponent()
{
	pinNb = 1;
	_pinList.emplace_back(Pin(0));
}

nts::TrueComponent::~TrueComponent() = default;

nts::Tristate nts::TrueComponent::compute(size_t pin)
{
	if (pin == 0)
		return clearPin();
	if (pin != 1)
		throw std::exception();
	return nts::TRUE;
}

void nts::TrueComponent::dump() const
{
	std::cout << "- True" << std::endl;
}
