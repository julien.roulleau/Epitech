/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** parser.hpp
*/


#ifndef NANOTECKSPICE_PARSER_HPP
#define NANOTECKSPICE_PARSER_HPP

#include <list>

template <typename T>
using list2d = std::list<std::list<T>>;

template <typename T>
using list3d = std::list<list2d<T>>;

list3d<std::string> parser(char *path);

#endif //NANOTECKSPICE_PARSER_HPP
