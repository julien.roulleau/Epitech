/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** parser.cpp
*/

#include <fstream>
#include <algorithm>
#include <iostream>
#include <sstream>
#include "parser.hpp"

static void get_file_buffer(char *path, std::string &buffer)
{
	std::ifstream file(path);
	char c;

	if (!file.is_open()) {
		std::cout << "File not found" << std::endl;
		throw std::exception();
	}
	while (file.get(c)) {
		if (c == '#') {
			while (file.get(c) && c != '\n');
			buffer += '\n';
		} else
			buffer += c;
	}
	file.close();
}

static void purge_str(std::string &buffer, char del)
{
	bool previous = false;
	buffer.erase(std::remove_if(
		buffer.begin(), buffer.end(), [&](char c) -> bool {
			if (c == del && previous)
				return true;
			previous = c == del;
			return false;
		}), buffer.end());
	if (*(buffer.begin()) == del)
		buffer.erase(buffer.begin());
	if (*(buffer.end()) == del)
		buffer.erase(buffer.end());
}

static void format_file_buffer(std::string &buffer)
{
	std::replace(buffer.begin(), buffer.end(), '\t', ' ');
	purge_str(buffer, ' ');
	purge_str(buffer, '\n');
}

static std::list<std::list<std::string>> list_obj(std::string &buffer)
{
	std::istringstream iss(buffer);
	std::list<std::list<std::string>> list;
	std::string line;

	std::getline(iss, line);
	while (std::getline(iss, line)) {
		std::size_t pos = line.find(' ');
		if (pos == (size_t) -1) {
			std::cout << "Invalid file" << std::endl;
			throw std::exception();
		}
		std::list<std::string> part;
		part.push_back(line.substr(0, pos));
		part.push_back(line.substr(pos + 1));
		list.push_back(part);
	}
	return list;
}

list3d<std::string> parser(char *path)
{
	std::string buffer;
	get_file_buffer(path, buffer);
	format_file_buffer(buffer);
	std::size_t chipsets_pos = buffer.find(".chipsets:");
	std::size_t links_pos = buffer.find(".links:");
	std::string chipsets = buffer.substr(chipsets_pos, links_pos);
	std::string links = buffer.substr(links_pos);
	std::list<std::list<std::list<std::string>>> list;
	list.push_back(list_obj(chipsets));
	list.push_back(list_obj(links));
	return list;
}

