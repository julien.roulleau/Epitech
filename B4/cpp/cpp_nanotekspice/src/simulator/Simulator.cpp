/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 05/02/18: simulator.cpp
*/


#include "Simulator.hpp"

bool inLoop = false;


void sigHandlerLoop(int signal)
{
	(void)signal;
	inLoop = false;
}

void sigHandler(int signal)
{
	(void)signal;
	exit(1);
}

Simulator::Simulator()
{
	funct.emplace("display", &Simulator::display);
	funct.emplace("dump", &Simulator::dump);
	funct.emplace("loop", &Simulator::loop);
	funct.emplace("simulate", &Simulator::simulate);
	inLoop = false;
}

Simulator::~Simulator()
{
}

void Simulator::init(const char *file)
{
	list3d<std::string> list = parser(const_cast<char*>(file));
	compList = mapper(list);
}

void Simulator::run()
{
	std::string input;

	simulate();
	display();
	while(!std::cin.eof()) {
		std::cout << "> ";
		std::cin >> input;
		if (input == "exit")
			break;
		auto iter = funct.find(input);
		try {
			if (iter == funct.end())
				setInput(input);
			else
				(*this.*(iter->second))();
		}
		catch (const std::exception &e) {
			std::cout << e.what() << std::endl;
			throw e;
		}
	}
}

void Simulator::simulate()
{
	auto itComp = compList.begin();
	while (itComp != compList.end()) {
		if (getType(itComp->first) == "output")
			(itComp->second)->compute(1);
		std::advance(itComp, 1);
	}
	clearPin();
}

void Simulator::loop()
{
	inLoop = true;

	signal(SIGINT, sigHandlerLoop);
	while (inLoop) {
		simulate();
	}
	signal(SIGINT, sigHandler);
}

void Simulator::dump()
{
	auto itCmp = compList.begin();

	while (itCmp != compList.end()) {
		(itCmp->second)->dump();
		std::advance(itCmp, 1);
	}
}

void Simulator::display()
{
	auto itCmp = compList.begin();

	while (itCmp != compList.end()) {
		if (getType(itCmp->first) == "output") {
			std::cout << getName(itCmp->first);
			(itCmp->second)->dump();
		}
		std::advance(itCmp, 1);
	}
}

void Simulator::setInput(const std::string &line)
{
	size_t pos = 0;

	if ((pos = line.find("=")) == std::string::npos)
		throw std::exception();
	std::string name = line.substr(0, pos);
	std::string value = line.substr(pos + 1, line.length());
	if (value != "0" && value != "1")
		throw std::exception();
	auto iter = compList.find(name + ";input");
	if (iter == compList.end())
		throw std::exception();
	if (getType(iter->first) != "input")
		throw std::exception();
	(iter->second)->compute((value == "1" ? 2 : 3));
}

void Simulator::clearPin()
{
	auto itComp = compList.begin();
	while (itComp != compList.end()) {
		(itComp->second)->compute(0);
		std::advance(itComp, 1);
	}
}

std::string Simulator::getType(const std::string &line)
{
	std::size_t limiter = line.find(";");

	return line.substr(limiter + 1);
}

std::string Simulator::getName(const std::string &line)
{
	std::size_t limiter = line.find(";");

	return line.substr(0, limiter);
}
