/*
** EPITECH PROJECT, 2018
** nanoteckspice
** File description:
** 05/02/18: simulator.hpp
*/


#ifndef NANOTECKSPICE_SIMULATOR_HPP
#define NANOTECKSPICE_SIMULATOR_HPP

#include <iostream>
#include <vector>
#include <map>
#include <regex>
#include <exception>
#include <signal.h>
#include "component/IComponent.hpp"
#include "parser/parser.hpp"
#include "mapper/mapper.hpp"

typedef std::map<std::string, std::unique_ptr<nts::IComponent>> CompMap;

class Simulator {
public:
	Simulator();
	~Simulator();
	void init(const char*);
	void run();
	void simulate();
	void loop();
	void dump();
	void display();
	void setInput(const std::string &line);
	void clearPin();
	std::string getType(const std::string &line);
	std::string getName(const std::string &line);

public:
	CompMap compList;
	std::map<std::string, void (Simulator::*)(void)> funct;
};

#endif //NANOTECKSPICE_SIMULATOR_HPP
