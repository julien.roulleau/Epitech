/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** main.cpp
*/

#include "Core/Core.hpp"

int main(int ac, char **av)
{
	if (ac != 2) {
		std::cout << "Not enought arguments." << std::endl;
		std::cout << "USAGE :" << std::endl;
		std::cout << "\t./arcade start_lib.so" << std::endl;
		std::cout << "COMMANDS LIST" << std::endl;
		std::cout << "\tF1 : prev Game"
			"\tF2 : next Game" << std::endl <<
			"\tF3 : prev Window" << std::endl <<
			"\tF4 : next Window" << std::endl <<
			"\tF5 : restart Game" << std::endl <<
			"\tF6 : Go Back To The Menu" << std::endl <<
			"\tF7 : Pause / Resume" << std::endl;
		return 0;
	}
	try {
		Core *core = new Core(av[1]);
		core->run();
		delete core;
	} catch (std::exception &e) {
		return 84;
	}
	return 0;
}