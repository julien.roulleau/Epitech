/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 26/03/18: LibLoader.cpp
*/

#include "DLLoader.hpp"

DLLoader::DLLoader() : currentWinLib(nullptr), currentGameLib(nullptr)
{
}

engine::Window *DLLoader::getWindowInstance(const std::string &path)
{
	currentWinLib = dlopen(path.c_str(), RTLD_LAZY);
	if (currentWinLib == nullptr) {
		std::cerr << dlerror() << std::endl;
		throw std::exception();
	}
	void *mkr = dlsym(currentWinLib, "instantiateWindow");
	if (!mkr) {
		std::cerr << dlerror() << std::endl;
		throw std::exception();
	}
	engine::Window *(*maker)() = reinterpret_cast<engine::Window *(*)()>(mkr);
	engine::Window *instance = maker();
	if (instance == nullptr)
		throw std::exception();
	return instance;
}

arcade::Game *DLLoader::getGameInstance(const std::string &path)
{
	currentGameLib = dlopen(path.c_str(), RTLD_LAZY);
	if (currentGameLib == nullptr) {
		std::cerr << dlerror() << std::endl;
		throw std::exception();
	}
	void *mkr = dlsym(currentGameLib, "instantiateGame");
	if (!mkr) {
		std::cerr << dlerror() << std::endl;
		throw std::exception();
	}
	arcade::Game *(*maker)() = reinterpret_cast<arcade::Game *(*)()>(mkr);
	arcade::Game *instance = maker();
	if (instance == nullptr)
		throw std::exception();
	return instance;
}

DLLoader *DLLoader::closeWinLib()
{
	if (currentWinLib != nullptr) {
		if (dlclose(currentWinLib)) {
			std::cerr << dlerror() << std::endl;
			throw std::exception();
		}
		currentWinLib = nullptr;
	}
	return this;
}

DLLoader *DLLoader::closeGameLib()
{
	if (currentGameLib != nullptr) {
		if (dlclose(currentGameLib)) {
			std::cerr << dlerror() << std::endl;
			throw std::exception();
		}
		currentGameLib = nullptr;
	}
	return this;
}
