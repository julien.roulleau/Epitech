/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 26/03/18: LibLoader.hpp
*/

#ifndef ARCADE_DLLOADER_HPP
#define ARCADE_DLLOADER_HPP

#include "Game/Game.hpp"
#include "Engine/Window/Window.hpp"
#include <dlfcn.h>
#include <exception>
#include <iostream>

class DLLoader {
public:
	DLLoader();
	~DLLoader() = default;
	engine::Window *getWindowInstance(const std::string &);
	arcade::Game *getGameInstance(const std::string &);
	DLLoader *closeWinLib();
	DLLoader *closeGameLib();
public:
	void *currentWinLib;
	void *currentGameLib;
};


#endif //ARCADE_DLLOADER_HPP
