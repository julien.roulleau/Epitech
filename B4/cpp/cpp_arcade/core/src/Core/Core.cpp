/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 29/03/18: Core.cpp
*/

#include "Core.hpp"

Core *Core::getGameLibsPath()
{
	DIR* dirp = opendir("./games");
	struct dirent *dp;

	if (!dirp) {
		std::cerr << dlerror() << std::endl;
		throw std::exception();
	}
	while ((dp = readdir(dirp)) != nullptr) {
		std::string name(dp->d_name);
		if (name.substr(0, 11) == "lib_arcade_" &&
			name.substr(name.length() - 3, 3) == ".so") {
			gameLibPath.emplace_back(
				"./games/" + std::string(name));
		}
	}
	closedir(dirp);
	if (gameLibPath.empty()) {
		std::cerr << dlerror() << std::endl;
		throw std::exception();
	}
	return this;
}

Core *Core::getWindowLibsPath()
{
	DIR* dirp = opendir("./lib");
	struct dirent *dp;

	if (!dirp) {
		std::cerr << dlerror() << std::endl;
		throw std::exception();
	}
	while ((dp = readdir(dirp)) != nullptr) {
		std::string name(dp->d_name);
		if (name.substr(0, 11) == "lib_arcade_" &&
		    name.substr(name.length() - 3, 3) == ".so") {
			windowLibPath.
				emplace_back("./lib/" + std::string(name));
		}
	}
	closedir(dirp);
	if (windowLibPath.empty()) {
		std::cerr << "Graphics libraries not found." << std::endl;
		throw std::exception();
	}
	return this;
}

Core *Core::switchGame(const std::string &path)
{
	Game->quit();
	Game.reset();
	this->Loader->closeGameLib();
	Game = std::unique_ptr<arcade::Game>(
		this->Loader->getGameInstance(path));
	return this;
}

Core *Core::switchNextGame()
{
	if (gameLibPath.empty())
		return this;
	gameIndex = gameIndex == gameLibPath.size() - 1 ? 0 : gameIndex + 1;
	auto gameIt = gameLibPath.begin();
	std::advance(gameIt, gameIndex);
	this->switchGame(*gameIt);
	return this;
}

Core *Core::switchPrevGame()
{
	if (gameLibPath.empty())
		return this;
	gameIndex = gameIndex == 0 ? gameLibPath.size() - 1 : gameIndex - 1;
	auto gameIt = gameLibPath.begin();
	std::advance(gameIt, gameIndex);
	this->switchGame(*gameIt);
	return this;
}

Core *Core::switchWin(const std::string &path)
{
	std::cout << path << std::endl;
	Window->close();
	Window.reset();
	this->Loader->closeWinLib();
	Window = std::unique_ptr<engine::Window>(
		this->Loader->getWindowInstance(path));
	Window->create(title, (engine::Vector2i){960, 720},
		       (engine::RenderSettings){960, 720, false});
	return this;
}

Core *Core::switchNextWin()
{
	if (windowLibPath.empty())
		return this;
	winIndex = winIndex == windowLibPath.size() - 1 ? 0 : winIndex + 1;
	auto winIt = windowLibPath.begin();
	std::advance(winIt, winIndex);
	this->switchWin(*winIt);
	return this;
}

Core *Core::switchPrevWin()
{
	if (windowLibPath.empty())
		return this;
	winIndex = winIndex == 0 ? windowLibPath.size() - 1 : winIndex - 1;
	auto winIt = windowLibPath.begin();
	std::advance(winIt, winIndex);
	this->switchWin(*winIt);
	return this;
}

Core *Core::restartGame()
{
	Game->restart();
	return this;
}

Core *Core::loadMenu()
{
	title = "Menu";
	Game->quit();
	Game.reset();
	this->Loader->closeGameLib();
	Game = std::unique_ptr<arcade::Game>(new Menu(this));
	auto winIt = windowLibPath.begin();
	std::advance(winIt, winIndex);
	this->switchWin(*winIt);
	return this;
}

void Core::run()
{
	while (Window != nullptr && Window->isOpen()) {
		engine::Event evt;
		while (Window != nullptr && Window->pollEvent(evt)) {
			Game->handleEvent(evt);
			this->handleEvent(evt);
		}
		if (Game != nullptr && Window != nullptr && Game->isRunning()) {
			Game->update();
			Window->clear();
			Game->render(Window);
			Window->display();
		}
		else if (Window != nullptr)
			Window->close();
	}
}

Core::Core(const std::string &libStart)
{
	title = "Menu";
	Loader = new DLLoader();
	menu = new Menu(this);
	Game = std::unique_ptr<arcade::Game>(menu);
	try {
		this->getWindowLibsPath()->getGameLibsPath();
	} catch (std::exception &e) {}
	Window = std::unique_ptr<engine::Window>(
		this->Loader->getWindowInstance(libStart));
	Window->create(title, (engine::Vector2i){960, 720},
		       (engine::RenderSettings){960, 720, false});
	winIndex = gameIndex = 0;
	eventKeyFunct.emplace(engine::KeyCode::F1, &Core::switchPrevGame);
	eventKeyFunct.emplace(engine::KeyCode::F2, &Core::switchNextGame);
	eventKeyFunct.emplace(engine::KeyCode::F3, &Core::switchPrevWin);
	eventKeyFunct.emplace(engine::KeyCode::F4, &Core::switchNextWin);
	eventKeyFunct.emplace(engine::KeyCode::F5, &Core::restartGame);
	eventKeyFunct.emplace(engine::KeyCode::F6, &Core::loadMenu);
	eventKeyFunct.emplace(engine::KeyCode::Escape, &Core::quitArcade);
	eventKeyFunct.emplace(engine::KeyCode::Escape, &Core::quitArcade);
	eventKeyFunct.emplace(engine::KeyCode::F7, &Core::pause);
}

Core *Core::handleEvent(const engine::Event &evt)
{
	if (evt.type == engine::EventType::KeyPressed) {
		auto it = eventKeyFunct.find(evt.key.code);
		if (it != eventKeyFunct.end())
			(*this.*(it->second))();
	}
	if (evt.type == engine::EventType::Closed)
		quitArcade();
	return this;
}

Core *Core::quitArcade() {
	delete this;
	exit(0);
}

Core::~Core()
{
}

Core *Core::pause() {
	if (Game->isPaused())
		Game->resume();
	else
		Game->pause();
	return this;
}
