/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 29/03/18: Core.hpp
*/

#ifndef ARCADE_CORE_HPP
#define ARCADE_CORE_HPP

class Core;

#include <vector>
#include <sys/types.h>
#include <dirent.h>
#include <iostream>
#include "Game/Game.hpp"
#include "Engine/Window/Window.hpp"
#include "DLLoader/DLLoader.hpp"
#include "Menu/Menu.hpp"

class Core {
public:
	Core(const std::string &);
	~Core();
	void run();
	Core *getGameLibsPath();
	Core *getWindowLibsPath();
	Core *switchGame(const std::string&);
	Core *switchWin(const std::string&);
	Core *switchNextGame();
	Core *switchPrevGame();
	Core *switchNextWin();
	Core *switchPrevWin();
	Core *restartGame();
	Core *loadMenu();
	Core *quitArcade();
	Core *handleEvent(const engine::Event &evt);
	Core *pause();
public:
	DLLoader *Loader;
	std::vector<std::string> gameLibPath;
	std::vector<std::string> windowLibPath;
	std::unique_ptr<arcade::Game>Game;
	std::unique_ptr<engine::Window>Window;
	Menu *menu;
	size_t winIndex;
	size_t gameIndex;
	std::map<engine::KeyCode, Core *(Core::*)(void)> eventKeyFunct;
	std::string title;
};


#endif //ARCADE_CORE_HPP
