/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 29/03/18: Menu.cpp
*/

#include "Menu.hpp"

extern "C" const uint8_t cWhite[3] = {255, 255, 255};
extern "C" const uint8_t cRed[3] = {255, 55, 55};
extern "C" const uint8_t cBlue[3] = {55, 55, 255};
extern "C" const uint8_t cGreen[3] = {55, 255, 55};

engine::Color color{};

Menu::Menu(Core *core) : cores(core) {
	iWin = 0;
	iGame = 0;
	curseur = false;
	m_sprite.emplace_back();
	m_sprite.back().setAsciiFile("./core/src/Menu/src/background.txt");
	m_sprite.back().setImageFile("./core/src/Menu/src/background.jpg");
	m_sprite.back().setSize((engine::Vector2f) {960, 720});
	startGame = false;
}

void Menu::loadHighscores(const arcade::Game::Scores &scores)
{
	(void) scores;
}

void Menu::handleEvent(engine::Event const &event)
{
	if (event.type != engine::EventType::KeyPressed)
		return;
	switch (event.key.code) {
		case engine::KeyCode::Left:
		case engine::KeyCode::Q:
			curseur = false;
			break;
		case engine::KeyCode::Right:
		case engine::KeyCode::D:
			curseur = true;
			break;
		case engine::KeyCode::Down:
		case engine::KeyCode::S:
			if (!curseur)
				iWin = iWin >= cores->windowLibPath.size() - 1 ?
				       iWin : iWin + 1;
			else
				iGame = iGame >= cores->gameLibPath.size() - 1 ?
					iGame : iGame + 1;
			break;
		case engine::KeyCode::Up:
		case engine::KeyCode::Z:
			if (!curseur)
				iWin = iWin == 0 ? 0 : iWin - 1;
			else
				iGame = iGame == 0 ? 0 : iGame - 1;
			break;
		case engine::KeyCode::Space:
		case engine::KeyCode::Return:
			cores->gameIndex = iGame;
			cores->winIndex = iWin;
			cores->title = cores->gameLibPath.at(iGame);
			if (cores->gameLibPath.at(iGame).substr(0, 19) ==
			    "./games/lib_arcade_" &&
			    cores->gameLibPath.at(iGame).substr(
				    cores->gameLibPath.at(iGame).length() - 3,
				    3) == ".so")
				cores->title = cores->gameLibPath.at(
					iGame).substr(19, cores->gameLibPath.at(
					iGame).length() - 22);
			cores->switchWin(cores->windowLibPath.at(iWin));
			cores->switchGame(cores->gameLibPath.at(iGame));
			break;
		default:
			break;
	}
}

void Menu::update()
{
	m_text.clear();
	m_text.emplace_back();
	printText(m_text.back(),
		  (engine::Vector2f) {480 - (2 * 20), 10},
		  50, "lib/sdl2/comic.ttf", "MENU", cWhite);
	m_text.emplace_back();
	printText(m_text.back(), (engine::Vector2f) {80, 150}, 40,
		  "lib/sdl2/comic.ttf", "WINDOW", (!curseur ? cRed : cWhite));
	m_text.emplace_back();
	printText(m_text.back(),
		  (engine::Vector2f) {600, 150}, 40,
		  "lib/sdl2/comic.ttf", "GAMES", (curseur ? cRed : cWhite));

	for (size_t i = 0; i < cores->windowLibPath.size(); i++) {
		m_text.emplace_back();
		std::string name;
		name = cores->windowLibPath.at(i);
		if (name.substr(0, 17) == "./lib/lib_arcade_" &&
		    name.substr(name.length() - 3, 3) == ".so")
			name = name.substr(17, name.length() - 20);
		printText(m_text.back(),
			  (engine::Vector2f) {100, 200 + (float) i * 30},
			  25, "lib/sdl2/comic.ttf",
			  name, (i == iWin ? cRed : cWhite));
		if (i != iWin)
			continue;
		m_text.emplace_back();
		printText(m_text.back(),
			  (engine::Vector2f) {80, 200 + (float) i * 30},
			  25, "lib/sdl2/comic.ttf", ">", cRed);
	}
	for (size_t i = 0; i < cores->gameLibPath.size(); i++) {
		m_text.emplace_back();
		std::string name;
		name = cores->gameLibPath.at(i);
		if (name.substr(0, 19) == "./games/lib_arcade_" &&
		    name.substr(name.length() - 3, 3) == ".so")
			name = name.substr(19, name.length() - 22);
		printText(m_text.back(),
			  (engine::Vector2f) {620, 200 + (float) i * 30},
			  25,
			  "lib/sdl2/comic.ttf",
			  name,
			  (i == iGame ? cRed : cWhite));
		if (i != iGame)
			continue;
		m_text.emplace_back();
		printText(m_text.back(),
			  (engine::Vector2f) {600, 200 + (float) i * 30},
			  25, "lib/sdl2/comic.ttf", ">", cRed);
	}
}

void Menu::pause()
{
}

void Menu::resume()
{
}

void Menu::quit()
{
}

void Menu::restart()
{
}

bool Menu::isPaused() const
{
	return false;
}

bool Menu::isRunning() const
{
	return true;
}

void Menu::printText(engine::Text &txt, engine::Vector2f pos, size_t size,
		     const std::string &fontPath, const std::string &text,
		     const uint8_t rgb[3])
{
	txt.moveTo(pos);
	txt.setSize(size);
	color.r = rgb[0];
	color.g = rgb[1];
	color.b = rgb[2];
	txt.setColor(color);
	txt.setText(text);
	txt.setFont(fontPath);
}

void Menu::render(std::unique_ptr<engine::Window> &window) const
{
	for (engine::Sprite const &part : m_sprite)
		window->draw(part);
	for (engine::Text const &part : m_text)
		window->draw(part);
}

const arcade::Game::Scores &Menu::highscores() const
{
	return score;
}
