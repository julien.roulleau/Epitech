/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 29/03/18: Menu.hpp
*/

#ifndef ARCADE_MENU_HPP
#define ARCADE_MENU_HPP

class Menu;

#include "Core/Core.hpp"
#include "Game/Game.hpp"
#include "Engine/Renderer/Sprite.hpp"
#include "Engine/Renderer/Text.hpp"

class Menu : public arcade::Game {
public:
	Menu(Core *core_ptr);
	~Menu() override = default;
	void loadHighscores(Scores const &scores) override;
	Scores const &highscores() const override;
	void handleEvent(engine::Event const &event) override;
	void update() override;
	void pause() override;
	void resume() override;
	void quit() override;
	void restart() override;
	bool isPaused() const override;
	bool isRunning() const override;
	void render(std::unique_ptr<engine::Window> &window) const override;

private:
	void printText(engine::Text &, engine::Vector2f, size_t,
		   const std::string &, const std::string &, const uint8_t[3]);
	Core *cores;
	size_t iWin;
	size_t iGame;
	bool curseur;
	arcade::Game::Scores score;
	std::vector<engine::Sprite>m_sprite;
	std::vector<engine::Text>m_text;
	bool startGame;
};


#endif //ARCADE_MENU_HPP
