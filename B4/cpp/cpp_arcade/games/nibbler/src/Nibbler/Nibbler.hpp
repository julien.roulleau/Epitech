/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** Game.hpp
*/


#ifndef ARCADE_GAME_HPP
#define ARCADE_GAME_HPP

#include <vector>
#include <iostream>
#include "Engine/Renderer/Sprite.hpp"
#include "Engine/Renderer/Text.hpp"
#include "Game/Game.hpp"

namespace nibbler {

	enum state {
		STOP,
		START,
		PAUSE
	};

	enum direction {
		UP,
		LEFT,
		DOWN,
		RIGHT
	};

	class Nibbler : public arcade::Game {
	public:
		Nibbler();
		void loadHighscores(Scores const &scores) override;
		const Scores &highscores() const override;
		void handleEvent(engine::Event const &event) override;
		void update() override;
		void pause() override;
		void resume() override;
		void quit() override;
		void restart() override;
		bool isPaused() const override;
		bool isRunning() const override;
		void render(
			std::unique_ptr<engine::Window> &window) const override;
		void addPart(engine::Vector2f pos);
		void move();
		bool isDie();
		bool isOccuped(engine::Vector2f pos);
		void eat();
		void setMap(std::string const &path);
		void addHead();
		void addLoseText();
		void addFood();
		void addWall(engine::Vector2f pos);
		engine::Vector2f foodNextPos();
		float findKey(engine::Event const &event);
	private:
		const float m_size = 20;
		state m_state{START};
		Scores m_scores;
		u_int64_t m_point = 0;
		std::vector<engine::Sprite> m_snake_part;
		std::vector<engine::Vector2f> m_wall;
		direction m_dir{RIGHT};
		engine::Sprite m_food_sprite{};
		engine::Text m_loose{};
		bool m_is_die = false;
		clock_t m_interval;
	};
}

#endif //ARCADE_GAME_HPP
