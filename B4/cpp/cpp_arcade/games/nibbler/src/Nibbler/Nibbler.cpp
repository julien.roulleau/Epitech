/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** Game.cpp
*/

#include <fstream>
#include <sstream>
#include <ctime>
#include "Nibbler.hpp"

nibbler::Nibbler::Nibbler()
{
	restart();
}

void nibbler::Nibbler::loadHighscores(const arcade::Game::Scores &scores)
{
	m_scores = scores;
}

const arcade::Game::Scores &nibbler::Nibbler::highscores() const
{
	return m_scores;
}

void nibbler::Nibbler::handleEvent(engine::Event const &event)
{
	if (event.type == engine::EventType::KeyPressed) {
		m_snake_part.back().setRotation(
			m_snake_part.back().getRotation() + findKey(event));
	}
}

void nibbler::Nibbler::update()
{
	auto speed = static_cast<float>(0.12 - 0.001 * (m_point / 100));
	speed = static_cast<float>(speed > 0.05 ? speed : 0.05);
	if (!isPaused() && !m_is_die &&
	    (float(clock() - m_interval)) / CLOCKS_PER_SEC > speed) {
		m_interval = clock();
		move();
		if (isDie()) {
			m_is_die = true;
			pause();
		}
		eat();
	}
}

void nibbler::Nibbler::pause()
{
	m_state = nibbler::state::PAUSE;
}

void nibbler::Nibbler::resume()
{
	m_state = nibbler::state::START;
}

void nibbler::Nibbler::quit()
{
	m_scores.emplace(m_point, "player");
	m_state = nibbler::state::STOP;
}

void nibbler::Nibbler::restart()
{
	srandom(static_cast<unsigned int>(time(nullptr)));
	m_interval = clock();
	m_dir = RIGHT;
	m_state = START;
	m_is_die = false;
	m_point = 0;
	m_snake_part.clear();
	addHead();
	addPart({460, 360});
	addPart({440, 360});
	addPart({420, 360});
	addFood();
	setMap("assets/nibbler/map/map.txt");
	addLoseText();
}

bool nibbler::Nibbler::isPaused() const
{
	return m_state == nibbler::state::PAUSE;
}

bool nibbler::Nibbler::isRunning() const
{
	return m_state != nibbler::state::STOP;
}

void nibbler::Nibbler::render(std::unique_ptr<engine::Window> &window) const
{
	window->draw(m_food_sprite);
	for (auto const &part : m_snake_part) {
		window->draw(part);
	}
	engine::Sprite sprite{};
	sprite.setAsciiFile("assets/nibbler/wall/wall.txt");
	sprite.setImageFile("assets/nibbler/wall/wall.png");
	sprite.setSize({m_size, m_size});
	for (auto const &wall : m_wall) {
		sprite.moveTo(wall);
		window->draw(sprite);
	}
	if (m_is_die)
		window->draw(m_loose);
}

void nibbler::Nibbler::addPart(engine::Vector2f pos)
{
	m_snake_part.emplace(m_snake_part.begin());
	m_snake_part.front().setImageFile("./assets/nibbler/parts/body.png");
	m_snake_part.front().setAsciiFile("./assets/nibbler/parts/body.txt");
	m_snake_part.front().setSize(engine::Vector2f{m_size, m_size});
	m_snake_part.front().moveTo(pos);
}

void nibbler::Nibbler::move()
{
	for (unsigned int i = 1; i < m_snake_part.size(); i++) {
		m_snake_part[i - 1].moveTo(m_snake_part[i].getPosition());
	}
	switch (m_dir) {
		case UP:
			m_snake_part.back().move({0, -m_size});
			break;
		case DOWN:
			m_snake_part.back().move({0, m_size});
			break;
		case LEFT:
			m_snake_part.back().move({-m_size, 0});
			break;
		case RIGHT:
			m_snake_part.back().move({m_size, 0});
			break;
	}
}

bool nibbler::Nibbler::isDie()
{
	if (m_snake_part.back().getPosition().x < 0
	    || m_snake_part.back().getPosition().y < 0
	    || m_snake_part.back().getPosition().x >= 960
	    || m_snake_part.back().getPosition().y >= 720)
		return true;
	return isOccuped(m_snake_part.back().getPosition());
}

bool nibbler::Nibbler::isOccuped(engine::Vector2f pos)
{
	for (auto it = m_snake_part.begin();
	     it != m_snake_part.end() - 1; ++it) {
		if (it->getPosition().x == pos.x
		    && it->getPosition().y == pos.y)
			return true;
	}
	for (auto const &wall: m_wall) {
		if (wall.x == pos.x
		    && wall.y == pos.y)
			return true;
	}
	return false;
}

void nibbler::Nibbler::eat()
{
	auto food_pos = m_food_sprite.getPosition();
	if (m_snake_part.back().getPosition().x > food_pos.x - m_size
	    && m_snake_part.back().getPosition().x < food_pos.x + m_size
	    && m_snake_part.back().getPosition().y > food_pos.y - m_size
	    && m_snake_part.back().getPosition().y < food_pos.y + m_size) {
		m_food_sprite.moveTo(foodNextPos());
		for (int i = 0; i < random() % 10; i++)
			addPart(m_snake_part.front().getPosition());
		m_point += 100;
	}
}

void nibbler::Nibbler::setMap(std::string const &path)
{
	std::ifstream file(path);
	if (file.fail()) {
		std::cerr << "Could not find file " << path << std::endl;
		throw std::exception();
	}
	std::string buffer;
	float x = 0;
	float y = 0;
	while (std::getline(file, buffer)) {
		x = 0;
		for (auto c: buffer) {
			if (c == '#')
				addWall({x, y});
			x += m_size;
		}
		y += m_size;
	}
}

void nibbler::Nibbler::addWall(engine::Vector2f pos)
{
	m_wall.push_back(pos);
}

void nibbler::Nibbler::addHead()
{
	m_snake_part.emplace(m_snake_part.begin());
	m_snake_part.front().setImageFile("./assets/nibbler/parts/head.png");
	m_snake_part.front().setAsciiFile("./assets/nibbler/parts/head.txt");
	m_snake_part.front().setSize(engine::Vector2f{m_size, m_size});
	m_snake_part.front().moveTo({480, 360});
}

void nibbler::Nibbler::addLoseText()
{
	m_loose.setFont("assets/nibbler/comic.ttf");
	m_loose.setSize(20);
	m_loose.moveTo({475, 360});
	m_loose.setText("You loose");
	m_loose.setColor({255, 0, 0, 255});
}

void nibbler::Nibbler::addFood()
{
	m_food_sprite.setImageFile("./assets/nibbler/food/food.jpg");
	m_food_sprite.setAsciiFile("./assets/nibbler/food/food.txt");
	m_food_sprite.setSize(engine::Vector2f{m_size, m_size});
	m_food_sprite.moveTo(foodNextPos());
}

engine::Vector2f nibbler::Nibbler::foodNextPos()
{
	engine::Vector2f pos{};
	do {
		pos = {float(random() % 960), float(random() % 720)};
		pos = {pos.x - int(pos.x) % int(m_size),
		       pos.y - int(pos.y) % int(m_size)};
	} while (isOccuped(pos));
	return pos;
}

float nibbler::Nibbler::findKey(engine::Event const &event)
{
	float rotate = 0;
	if (event.key.code == engine::KeyCode::Up
	    && (m_dir == LEFT || m_dir == RIGHT)) {
		rotate = m_dir == LEFT ? 90 : -90;
		m_dir = UP;
	} else if (event.key.code == engine::KeyCode::Down
		   && (m_dir == LEFT || m_dir == RIGHT)) {
		rotate = m_dir == RIGHT ? 90 : -90;
		m_dir = DOWN;
	} else if (event.key.code == engine::KeyCode::Left
		   && (m_dir == UP || m_dir == DOWN)) {
		rotate = m_dir == DOWN ? 90 : -90;
		m_dir = LEFT;
	} else if (event.key.code == engine::KeyCode::Right
		   && (m_dir == UP || m_dir == DOWN)) {
		rotate = m_dir == UP ? 90 : -90;
		m_dir = RIGHT;
	}
	return rotate;
}
