/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** main.cpp
*/

#include "Nibbler/Nibbler.hpp"

extern "C" {
void *instantiateGame()
{
	return new nibbler::Nibbler();
}
}