/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 
*/

#include <iostream>
#include <unistd.h>
#include "Direction.hpp"
#include "Engine/Renderer/Text.hpp"
#include "Pacman.hpp"
#include "Utils.hpp"


Pacman::Pacman() : _map(Level("assets/pacman/pacman_map.txt")),
                   _animation(PacmanAnimation()),
                   _ghost(GhostHandler(clock(), _map)), pacgum(Pacgum(_map))

{
        _pause = false;
        _running = true;

        _nextDir = EAST;
        _direction = EAST;
        pos = {15, 17};
        _begin_time = clock();
        _interval = _begin_time;
        i = -1;
}


Pacman::~Pacman()
{

}

void Pacman::loadHighscores(const arcade::Game::Scores &scores)
{
        _scores = scores;
}

const arcade::Game::Scores &Pacman::highscores() const
{
        return _scores;
}

void Pacman::handleEvent(engine::Event const &ev)
{
        if (ev.type == engine::EventType::Closed ||
            (ev.type == engine::EventType::KeyPressed &&
             ev.key.code == engine::KeyCode::Escape))
                quit();
        else
        if (ev.type == engine::EventType::KeyPressed)
                handleKeyEvent(ev);
}

void Pacman::handleKeyEvent(const engine::Event &ev)
{
        switch (ev.key.code) {
                case engine::KeyCode::Down:
                case engine::KeyCode::S:
                        _nextDir = SOUTH;
                        break;
                case engine::KeyCode::Up:
                case engine::KeyCode::Z:
                        _nextDir = NORTH;
                        break;
                case engine::KeyCode::Right:
                case engine::KeyCode::D:
                        _nextDir = EAST;
                        break;
                case engine::KeyCode::Left:
                case engine::KeyCode::Q:
                        _nextDir = WEST;
                        break;
                default:
                        break;
        }
}

void Pacman::update()
{
        if (!isPaused() && isRunning() &&
            (float(clock() - _interval)) / CLOCKS_PER_SEC > 0.06) {
                _interval = clock();
                _animation.getNextAnimation();
                unsigned long maxSize = _map.get_map()[0].length() - 1;
                if (_map.canTurn(pos, _nextDir)) {
                        _direction = _nextDir;
                } else
                        _map.canMoveToDirection(pos, _direction);
                if (pos.x < 0)
                        pos.x = maxSize;
                else if (pos.x > maxSize)
                        pos.x = 0;
                if (++i > 4) {
                        _ghost.updateGhosts();
                        i = 0;
                }
                _sprite = _animation.getNextSpriteAnimation(_direction);
                pacgum.updatePacgum(pos);
                if (_ghost.isPlayerDead(pos)) {
                        restart();
                }
                if (pacgum.checkEnd())
                       quit();
        }
}

void Pacman::pause()
{
        _pause = true;
}

void Pacman::resume()
{
        _running = true;
}

void Pacman::quit()
{
        _running = false;
}

void Pacman::restart()
{
        _nextDir = EAST;
        _direction = EAST;
        pos = {15, 17};
        _begin_time = clock();
        _interval = _begin_time;
        _map.restart();
        pacgum.restart();
        _ghost.restart();
}

bool Pacman::isPaused() const
{
        return _pause;
}

bool Pacman::isRunning() const
{
        return _running;
}

void Pacman::render(std::unique_ptr<engine::Window> &window) const
{
        _map.drawLevel(window);
        if (_sprite != nullptr) {
                _sprite->moveTo({pos.x * SPRITE_SIZE, pos.y * SPRITE_SIZE});
                window->draw(*_sprite);
        }
        pacgum.drawPacgum(window);
        _ghost.drawGhosts(window);
}
