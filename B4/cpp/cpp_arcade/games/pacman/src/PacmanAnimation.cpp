/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 
*/

#include <iostream>
#include "Utils.hpp"
#include "PacmanAnimation.hpp"

PacmanAnimation::PacmanAnimation() : currentAnimation(0)
{
        _still = newSprite("assets/pacman/pacman_still.png", "assets/pacman/pacman_d1.txt");

        _left.push_back(newSprite("assets/pacman/pacman_l1.png", "assets/pacman/pacman_d1.txt"));
        _left.push_back(newSprite("assets/pacman/pacman_l2.png", "assets/pacman/pacman_d1.txt"));
       // _left.push_back(_still);

        _right.push_back(newSprite("assets/pacman/pacman_r1.png", "assets/pacman/pacman_d1.txt"));
        _right.push_back(newSprite("assets/pacman/pacman_r2.png", "assets/pacman/pacman_d1.txt"));
       // _right.push_back(_still);

        _up.push_back(newSprite("assets/pacman/pacman_u1.png", "assets/pacman/pacman_d1.txt"));
        _up.push_back(newSprite("assets/pacman/pacman_u2.png", "assets/pacman/pacman_d1.txt"));
     // _up.push_back(_still);

        _down.push_back(newSprite("assets/pacman/pacman_d1.png", "assets/pacman/pacman_d1.txt"));
        _down.push_back(newSprite("assets/pacman/pacman_d2.png", "assets/pacman/pacman_d1.txt"));
       // _down.push_back(_still);

        if (_left.size() != 2 || _right.size() != 2 ||
            _up.size() != 2 || _down.size() != 2) {
                std::cerr << "Could not find pacman's assets "
                             "(directory is assets/pacman)" << std::endl;
                throw std::runtime_error("Asset loading error");
        }
}

std::shared_ptr<engine::Sprite> &PacmanAnimation::getNextSpriteAnimation(Direction direction)
{
        switch (direction) {
                case NORTH:
                        return _up[currentAnimation];
                case SOUTH:
                        return _down[currentAnimation];
                case WEST:
                        return _left[currentAnimation];
                case EAST:
                        return _right[currentAnimation];
        }
        return _still;
}

void PacmanAnimation::getNextAnimation()
{
        if (currentAnimation >= 1)
                currentAnimation = -1;
        currentAnimation++;
}

PacmanAnimation::~PacmanAnimation() {

}
