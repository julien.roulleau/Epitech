/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 
*/

#include "GhostHandler.hpp"
#include "Level.hpp"

GhostHandler::GhostHandler(clock_t timeOfstart, Level &level)
        : _time(timeOfstart), _level(level)
{
        createGhosts();
}

void GhostHandler::createGhosts()
{
        _ghosts.push_back(Ghost((engine::Vector2f){13, 13}, YELLOW_GHOST, GHOST_ASCII, _level));
        _ghosts.push_back(Ghost((engine::Vector2f){14, 13}, BLUE_GHOST, GHOST_ASCII, _level));
        _ghosts.push_back(Ghost((engine::Vector2f){13, 14}, RED_GHOST, GHOST_ASCII, _level));
        _ghosts.push_back(Ghost((engine::Vector2f){14, 14}, PINK_GHOST, GHOST_ASCII, _level));

}

void GhostHandler::updateGhosts()
{
        for (auto &i : _ghosts) {
                i.udateMove();
                i.moveGhost();
        }
}

void GhostHandler::drawGhosts(std::unique_ptr<engine::Window> &window) const
{
        for (auto &i : _ghosts) {
                i.drawGhost(window);
        }
}

void GhostHandler::restart()
{
        _ghosts.clear();
        _time = clock();
        createGhosts();
}

bool GhostHandler::isPlayerDead(engine::Vector2f pos) {
        int x = static_cast<int>(pos.x);
        int y = static_cast<int>(pos.y);

        for (auto &i: _ghosts) {
                if (x == static_cast<int>(i.get_pos().x)
                    && y == static_cast<int>(i.get_pos().y))
                        return true;
        }
        return false;
}

