/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 
*/

#ifndef ARCADE_GHOSTHANDLER_HPP
#define ARCADE_GHOSTHANDLER_HPP


#include <ctime>
#include "Level.hpp"
#include "Ghost.hpp"

#define YELLOW_GHOST "assets/pacman/yellow_ghost.png"
#define BLUE_GHOST "assets/pacman/blue_ghost.png"
#define PINK_GHOST "assets/pacman/pink_ghost.png"
#define RED_GHOST "assets/pacman/red_ghost.png"

#define GHOST_ASCII "assets/pacman/ghost_text.txt"

class GhostHandler {
public:
        GhostHandler(clock_t timeOfstart, Level &level);

        void drawGhosts(std::unique_ptr<engine::Window> &window) const;
        void updateGhosts();

        bool isPlayerDead(engine::Vector2f pos);

        void restart();

private:
        clock_t _time;
        Level &_level;
        void createGhosts();

        std::vector<Ghost> _ghosts;
};


#endif //ARCADE_GHOSTHANDLER_HPP
