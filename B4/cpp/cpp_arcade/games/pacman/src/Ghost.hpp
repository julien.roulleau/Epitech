/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 
*/

#pragma once

#include <memory>
#include <Engine/Renderer/Sprite.hpp>
#include "Engine/Renderer/Vector.hpp"
#include "Direction.hpp"
#include "Level.hpp"

class Ghost {
public:
        Ghost(engine::Vector2f startPos, const std::string &img, const std::string &ascii, Level level);

        void moveGhost();
        void udateMove();
        void drawGhost(std::unique_ptr<engine::Window> &window) const;
        const engine::Vector2f &get_pos() const;


private:
        engine::Vector2f _pos;
        Level _level;
        Direction _direction;

        std::shared_ptr<engine::Sprite> _sprite;

        bool canTurn(Direction direction);
        void move();
        std::vector<Direction> getAvailibleDirections();

        void printDirection();
};