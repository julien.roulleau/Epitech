/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 
*/

#pragma once

#include <string>
#include <memory>
#include <Engine/Renderer/Color.hpp>
#include "Engine/Window/Window.hpp"
#include "Direction.hpp"

std::shared_ptr<engine::Sprite> newSprite(const std::string &fname, const std::string &fpath);
int  getRandomNumber(int min, int max);
std::shared_ptr<engine::Text> newText(engine::Vector2f pos, size_t size, engine::Color color);