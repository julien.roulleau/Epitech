/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 
*/

#pragma once

#include <string>
#include <vector>
#include <memory>
#include "Direction.hpp"
#include "Engine/Window/Window.hpp"

#define SPRITE_SIZE 20
#define SPEED 0.25

#define WALL '#'
#define FRUIT '1'
#define VOID '0'
#define DOOR '2'

class Level
{
public:
        Level(const std::string &);
        const std::vector<std::string> &get_map() const;
        void drawLevel(std::unique_ptr<engine::Window> &window) const;
        bool canMoveToPos(engine::Vector2f pos);
        bool canMoveToDirection(engine::Vector2f &pos, Direction direction);
        bool canTurn(engine::Vector2f &pos, Direction direction);
        bool canGhostMoveToPos(engine::Vector2f pos, engine::Vector2f ghostPost);
        void eatPacgum(engine::Vector2f pos);


        void restart();

private:
        std::vector<std::string> _map;
        std::string _str;
        std::shared_ptr<engine::Sprite> _sprite;

        void getMapFile();
};