/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 
*/

#ifndef ARCADE_PACGUM_HPP
#define ARCADE_PACGUM_HPP


#include "Level.hpp"

class Pacgum {
public:
        Pacgum(Level &level);
        void drawPacgum(std::unique_ptr<engine::Window> &window) const;
        void updatePacgum(engine::Vector2f pos);


        bool checkEnd();

        void restart();

private:

        std::shared_ptr<engine::Sprite> _sprite;
        std::shared_ptr<engine::Text> _text;
        Level &_level;
        int _maxPacgum;
        int _pickedPacgum;

        void countPacgum();
};


#endif //ARCADE_PACGUM_HPP
