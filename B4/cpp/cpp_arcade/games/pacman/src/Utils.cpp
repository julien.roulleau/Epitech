/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 
*/

#include <random>
#include "Utils.hpp"
#include "Level.hpp"
#include "Engine/Renderer/Sprite.hpp"
#include "Engine/Renderer/Text.hpp"


std::shared_ptr<engine::Sprite> newSprite(const std::string &fname, const std::string &fpath)
{
        auto sprite = std::make_shared<engine::Sprite>(engine::Sprite());

        sprite->setRotation(0);
        sprite->setImageFile(fname);
        sprite->setAsciiFile(fpath);
        sprite->setSize({SPRITE_SIZE, SPRITE_SIZE});
        return sprite;
}

std::shared_ptr<engine::Text> newText(engine::Vector2f pos, size_t size, engine::Color color)
{
        auto sprite = std::make_shared<engine::Text>(engine::Text());

        sprite->setFont("assets/nibbler/arial.ttf");
        sprite->setText("");
        sprite->moveTo(pos);
        sprite->setColor(color);
        sprite->setSize(size);
        return sprite;
}

int  getRandomNumber(int min, int max)
{
        std::random_device rd;
        std::mt19937 rng(rd());
        std::uniform_int_distribution<int> uni(min,max);

        auto i = uni(rng);
        return i;
}