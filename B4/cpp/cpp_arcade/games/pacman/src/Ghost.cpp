/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 
*/

#include <vector>
#include <cmath>
#include <algorithm>
#include <random>
#include <unistd.h>
#include <iostream>
#include "Ghost.hpp"
#include "Utils.hpp"
#include "Direction.hpp"
#include "Level.hpp"

Ghost::Ghost(engine::Vector2f startPos, const std::string &img,
             const std::string &ascii, Level level) : _level(level)
{
        _pos = startPos;
        _direction = NORTH;
        _sprite = newSprite(img, ascii);
}

void Ghost::udateMove()
{
        std::vector<Direction > directions = getAvailibleDirections();
        int randomNumber = getRandomNumber(0, static_cast<int>(directions.size() - 1));
        if (directions.size() == 2 && canTurn(_direction))
                return;
        _direction = directions[randomNumber];
}

void Ghost::drawGhost(std::unique_ptr<engine::Window> &window) const
{
        _sprite.get()->moveTo({_pos.x * SPRITE_SIZE, _pos.y * SPRITE_SIZE});
        window->draw(*_sprite.get());
}

void Ghost::moveGhost()
{
        move();
}

std::vector<Direction> Ghost::getAvailibleDirections()
{
        std::vector<Direction > directions;

        if (canTurn(WEST)) {
                directions.push_back(WEST);
        }
        if (canTurn(EAST)) {
                directions.push_back(EAST);
        }
        if (canTurn(NORTH)) {
                directions.push_back(NORTH);
        }
        if (canTurn(SOUTH)) {
                directions.push_back(SOUTH);
        }
        return directions;
}

bool Ghost::canTurn(Direction direction)
{
        engine::Vector2f vec;

        vec.x = _pos.x;
        vec.y = _pos.y;

        switch (direction) {
                case NORTH:
                        vec.y -= 1;
                        break;
                case SOUTH:
                        vec.y += 1;
                        break;
                case WEST:
                        vec.x -= 1;
                        break;
                case EAST:
                        vec.x += 1;
                        break;
        }
        return _level.canGhostMoveToPos(vec, _pos);
}

void Ghost::printDirection()
{
        switch (_direction) {
                case NORTH:
                       printf("NORTH\n");
                        break;
                case SOUTH:
                        printf("SOUTH\n");
                        break;
                case WEST:
                        printf("WEST\n");
                        break;
                case EAST:
                        printf("EAST\n");
                        break;
        }
}

void Ghost::move()
{
        switch (_direction) {
                case NORTH:
                        _pos.y -= 1;
                        break;
                case SOUTH:
                        _pos.y += 1;
                        break;
                case WEST:
                        _pos.x -= 1;
                        break;
                case EAST:
                        _pos.x += 1;
                        break;
        }
}

const engine::Vector2f &Ghost::get_pos() const {
        return _pos;
}
