/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 
*/

#ifndef ARCADE_PACMANANIMATION_HPP
#define ARCADE_PACMANANIMATION_HPP


#include <vector>
#include "Engine/Renderer/Sprite.hpp"

class PacmanAnimation {
public:
        PacmanAnimation();

        ~PacmanAnimation();
        std::shared_ptr<engine::Sprite> &getNextSpriteAnimation(Direction direction);
        void getNextAnimation();

private:
        std::shared_ptr<engine::Sprite> _still;
        std::vector<std::shared_ptr<engine::Sprite>> _left;
        std::vector<std::shared_ptr<engine::Sprite>> _right;
        std::vector<std::shared_ptr<engine::Sprite>> _up;
        std::vector<std::shared_ptr<engine::Sprite>> _down;
        int currentAnimation;
};


#endif //ARCADE_PACMANANIMATION_HPP
