/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 
*/

#include "Engine/Renderer/Text.hpp"
#include "Engine/Renderer/Sprite.hpp"
#include "Pacgum.hpp"
#include "Utils.hpp"

Pacgum::Pacgum(Level &level) :  _level(level)
{
        _sprite = newSprite("assets/pacman/pacgum.png", "assets/pacman/pacgum.txt");
        _text = newText({600, 0}, 24, {225, 0, 0, 225});
        _pickedPacgum = 0;
        countPacgum();
}

void Pacgum::countPacgum()
{
        std::vector<std::string> map = _level.get_map();
        _maxPacgum = 0;

        for (unsigned long y = 0; y < map.size(); y++) {
                for (unsigned int x = 0; x < map[y].size(); x++) {
                        if (map[y][x] == FRUIT) {
                             _maxPacgum++;
                        }
                }
        }
}

void Pacgum::drawPacgum(std::unique_ptr<engine::Window> &window) const
{
        std::vector<std::string> map = _level.get_map();

        std::string str = "Score: ";
        str = str + std::to_string(_pickedPacgum * 100);
        _text.get()->setText(str);
        for (unsigned long y = 0; y < map.size(); y++) {
                for (unsigned int x = 0; x < map[y].size(); x++) {
                        if (map[y][x] == FRUIT) {
                                _sprite->moveTo(
                                        {static_cast<float>
                                         (x * SPRITE_SIZE),
                                         static_cast<float>
                                         (y * SPRITE_SIZE)});
                                window->draw(*_sprite);
                        }
                }
        }
        window->draw(*_text);
}

void Pacgum::updatePacgum(engine::Vector2f pos)
{
        if (_level.get_map()[pos.y][pos.x] == FRUIT) {
                _level.eatPacgum(pos);
                _pickedPacgum++;
        }

}

bool Pacgum::checkEnd() {
        return _pickedPacgum == _maxPacgum;
}

void Pacgum::restart()
{
        _pickedPacgum = 0;
        _sprite = newSprite("assets/pacman/pacgum.png", "assets/pacman/pacgum.txt");
        countPacgum();
}
