/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 
*/

#pragma once

#include "Game/Game.hpp"
#include "Level.hpp"
#include "PacmanAnimation.hpp"
#include "GhostHandler.hpp"
#include "Pacgum.hpp"

class Pacman : public arcade::Game
{
public:
        Pacman();

        ~Pacman() override;

	void loadHighscores(Scores const &scores) override;

	const Scores &highscores() const override;

	void handleEvent(engine::Event const &event) override;

	void update() override;

	void pause() override;

	void resume() override;

	void quit() override;

	void restart() override;

	bool isPaused() const override;

	bool isRunning() const override;

	void render(std::unique_ptr<engine::Window> &window) const override;

private:
	void handleKeyEvent(const engine::Event &ev);

	std::multimap<std::uint64_t, std::string> _scores;
	bool _running;
	bool _pause;
	int i;

	engine::Vector2f pos;
	Direction _direction;
	Direction _nextDir;
	Level _map;
	clock_t _begin_time;
	clock_t _interval;
	PacmanAnimation _animation;
        std::shared_ptr<engine::Sprite> _sprite;
        GhostHandler _ghost;
        Pacgum pacgum;
};