/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 
*/

#include <sstream>
#include <fstream>
#include <iostream>
#include <cmath>
#include "Engine/Renderer/Sprite.hpp"
#include "Engine/Window/Window.hpp"
#include "Level.hpp"
#include "Utils.hpp"
#include "Pacman.hpp"


Level::Level(const std::string &str) : _str(str)
{
        std::ifstream file(str);
        std::string buffer;
        if (file.fail()) {
                std::cerr << "Could not find file "  << str << std::endl;
                throw std::runtime_error("File not found exception");
        }

        while (std::getline(file, buffer))
                _map.push_back(buffer);
        _sprite = newSprite("assets/pacman/sprite_wall.png", "assets/pacman/sprite_wall.txt");
}

void Level::getMapFile()
{
        std::ifstream file(_str);
        std::string buffer;
        if (file.fail()) {
                std::cerr << "Could not find file "  << _str << std::endl;
                throw std::runtime_error("File not found exception");
        }

        while (std::getline(file, buffer))
                _map.push_back(buffer);
        _sprite = newSprite("assets/pacman/sprite_wall.png", "assets/pacman/sprite_wall.txt");
}

const std::vector<std::string> &Level::get_map() const {
        return _map;
}

void Level::eatPacgum(engine::Vector2f pos)
{
        _map[pos.y][pos.x] = VOID;
}

void Level::drawLevel(std::unique_ptr<engine::Window> &window) const
{
        for (unsigned long y = 0; y < _map.size() ; y++) {
                for (unsigned long x = 0; x < _map[y].length(); x++) {
                        if (_map[y][x] == WALL || _map[y][x] == DOOR) {
                                _sprite->moveTo(
                                        {static_cast<float>
                                         (x * SPRITE_SIZE),
                                         static_cast<float>
                                         (y * SPRITE_SIZE)});
                                window->draw(*_sprite);
                        }
                }
        }
}

bool Level::canMoveToPos(engine::Vector2f pos)
{
        char c;
        if (pos.y >= _map.size()) {
                return false;
        }
        if ( pos.x < 0 || pos.y >= _map.size() || pos.x >= _map[pos.y].length())
                return true;
        c = _map[static_cast<int>(pos.y)][static_cast<int>(pos.x)];
        return c != WALL && c != DOOR;
}

bool Level::canGhostMoveToPos(engine::Vector2f pos, engine::Vector2f ghostPost)
{
        char c;
        if (pos.y >= _map.size()) {
                return false;
        }
        if ( pos.x < 0 || pos.y >= _map.size() || pos.x >= _map[pos.y].length())
                return false;
        c = _map[static_cast<int>(pos.y)][static_cast<int>(pos.x)];
        if (ghostPost.y < 12 && c == DOOR)
                return false;
        return (c != WALL);
}

bool Level::canMoveToDirection(engine::Vector2f &pos, Direction direction)
{
        bool result = false;
        engine::Vector2f copy;

        copy.y = pos.y;
        copy.x = pos.x;
        switch (direction) {
                case NORTH:
                        copy.y -= SPEED;
                        if ((result = canMoveToPos(copy)))
                                pos.y -= SPEED;
                        break;
                case SOUTH:
                        copy.y = static_cast<float>(ceil(copy.y + SPEED));
                        if ((result = canMoveToPos(copy)))
                                pos.y += SPEED;
                        break;
                case WEST:
                        copy.x -= SPEED;
                        if ((result = canMoveToPos(copy)))
                                pos.x -= SPEED;
                        break;
                case EAST:
                        copy.x = static_cast<float>(ceil(copy.x + SPEED));
                        if ((result = canMoveToPos(copy)))
                                pos.x += SPEED;
                        break;
        }
        return result;
}

bool Level::canTurn(engine::Vector2f &pos, Direction direction)
{
        bool result = false;
        engine::Vector2f copy;

        if (pos.x == floor(pos.x) && pos.y == floor(pos.y)) {
                copy.y = pos.y;
                copy.x = pos.x;
                switch (direction) {
                        case NORTH:
                                copy.y -= 1;
                                if ((result = canMoveToPos(copy)))
                                        pos.y -= SPEED;
                                break;
                        case SOUTH:
                                copy.y += 1;
                                if ((result = canMoveToPos(copy)))
                                        pos.y += SPEED;
                                break;
                        case WEST:
                                copy.x -= 1;
                                if ((result = canMoveToPos(copy)))
                                        pos.x -= SPEED;
                                break;
                        case EAST:
                                copy.x += 1;
                                if ((result = canMoveToPos(copy)))
                                        pos.x += SPEED;
                                break;
                }
                return result;
        }
        return result;
}

void Level::restart()
{
        _map.clear();
        getMapFile();
}
