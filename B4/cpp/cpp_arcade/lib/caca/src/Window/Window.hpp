/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** Window.hpp
*/


#ifndef ARCADE_WINDOW_HPP
#define ARCADE_WINDOW_HPP

#include "Engine/Window/Window.hpp"
#include "../RenderTarget/RenderTarget.hpp"

namespace caca {
	class Window : public engine::Window {
	public:
		void
		create(std::string const &title, const engine::Vector2i &size,
		       const engine::RenderSettings &settings) override;
		bool isOpen() const override;
		void close() override;
		void clear() override;
		void draw(const engine::Drawable &drawable) override;
		bool pollEvent(engine::Event &event) override;
		void display() override;

	private:
		caca_display_t *m_display;
		caca_canvas_t *m_canvas;
		caca::RenderTarget *m_renderer;
	};
}

#endif /* ARCADE_WINDOW_HPP */
