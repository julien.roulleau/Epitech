/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** Window.cpp
*/
#include <iostream>
#include <map>
#include "Window.hpp"

void caca::Window::create(
	std::string const &title, const engine::Vector2i &size,
	const engine::RenderSettings &settings)
{
	(void) size;
	m_canvas = caca_create_canvas(static_cast<int>(settings.width / 10),
				      static_cast<int>(settings.height / 20));
	if (!m_canvas) {
		std::cout << "libcaca: Create canvas fail" << std::endl;
		throw std::exception();
	}
	m_display = caca_create_display(m_canvas);
	if (!m_display) {
		std::cout << "libcaca: Create display fail" << std::endl;
		throw std::exception();
	}
	m_renderer = new caca::RenderTarget{m_canvas};
	caca_set_display_title(m_display, title.c_str());
}

void caca::Window::close()
{
	caca_free_canvas(m_canvas);
	m_canvas = nullptr;
	caca_free_display(m_display);
	m_display = nullptr;
}

bool caca::Window::isOpen() const
{
	return m_display != nullptr;
}

void caca::Window::clear()
{
	caca_clear_canvas(m_canvas);
}

void caca::Window::draw(const engine::Drawable &drawable)
{
	drawable.draw(*m_renderer);
}

void caca::Window::display()
{
	caca_refresh_display(m_display);
}

std::map<int, engine::EventType> event_list = {
	{CACA_EVENT_NONE,          engine::EventType::None},
	{CACA_EVENT_KEY_PRESS,     engine::EventType::KeyPressed},
	{CACA_EVENT_KEY_RELEASE,   engine::EventType::KeyReleased},
	{CACA_EVENT_MOUSE_PRESS,   engine::EventType::MouseButtonPressed},
	{CACA_EVENT_MOUSE_RELEASE, engine::EventType::MouseButtonReleased},
	{CACA_EVENT_MOUSE_MOTION,  engine::EventType::MouseMoved},
	{CACA_EVENT_RESIZE,        engine::EventType::Resized},
	{CACA_EVENT_QUIT,          engine::EventType::Closed},
	{CACA_EVENT_ANY,           engine::EventType::None},
};

std::map<int, engine::KeyCode> key_map = {
	{'a',    engine::KeyCode::A},
	{'b',    engine::KeyCode::B},
	{'c',    engine::KeyCode::C},
	{'d',    engine::KeyCode::D},
	{'e',    engine::KeyCode::E},
	{'f',    engine::KeyCode::F},
	{'g',    engine::KeyCode::G},
	{'h',    engine::KeyCode::H},
	{'i',    engine::KeyCode::I},
	{'j',    engine::KeyCode::J},
	{'k',    engine::KeyCode::K},
	{'l',    engine::KeyCode::L},
	{'m',    engine::KeyCode::M},
	{'n',    engine::KeyCode::N},
	{'o',    engine::KeyCode::O},
	{'p',    engine::KeyCode::P},
	{'q',    engine::KeyCode::Q},
	{'r',    engine::KeyCode::R},
	{'s',    engine::KeyCode::S},
	{'t',    engine::KeyCode::T},
	{'u',    engine::KeyCode::U},
	{'v',    engine::KeyCode::V},
	{'w',    engine::KeyCode::W},
	{'x',    engine::KeyCode::X},
	{'y',    engine::KeyCode::Y},
	{'z',    engine::KeyCode::Z},
	{CACA_KEY_UNKNOWN,   engine::KeyCode::Unknown},
	{CACA_KEY_BACKSPACE, engine::KeyCode::BackSpace},
	{CACA_KEY_TAB,       engine::KeyCode::Tab},
	{CACA_KEY_RETURN,    engine::KeyCode::Return},
	{CACA_KEY_PAUSE,     engine::KeyCode::Pause},
	{CACA_KEY_ESCAPE,    engine::KeyCode::Escape},
	{CACA_KEY_DELETE,    engine::KeyCode::Delete},
	{CACA_KEY_UP,        engine::KeyCode::Up},
	{CACA_KEY_DOWN,      engine::KeyCode::Down},
	{CACA_KEY_LEFT,      engine::KeyCode::Left},
	{CACA_KEY_RIGHT,     engine::KeyCode::Right},
	{CACA_KEY_INSERT,    engine::KeyCode::Insert},
	{CACA_KEY_HOME,      engine::KeyCode::Home},
	{CACA_KEY_END,       engine::KeyCode::End},
	{CACA_KEY_PAGEUP,    engine::KeyCode::PageUp},
	{CACA_KEY_PAGEDOWN,  engine::KeyCode::PageDown},
	{CACA_KEY_F1,        engine::KeyCode::F1},
	{CACA_KEY_F2,        engine::KeyCode::F2},
	{CACA_KEY_F3,        engine::KeyCode::F3},
	{CACA_KEY_F4,        engine::KeyCode::F4},
	{CACA_KEY_F5,        engine::KeyCode::F5},
	{CACA_KEY_F6,        engine::KeyCode::F6},
	{CACA_KEY_F7,        engine::KeyCode::F7},
	{CACA_KEY_F8,        engine::KeyCode::F8},
	{CACA_KEY_F9,        engine::KeyCode::F9},
	{CACA_KEY_F10,       engine::KeyCode::F10},
	{CACA_KEY_F11,       engine::KeyCode::F11},
	{CACA_KEY_F12,       engine::KeyCode::F12},
	{CACA_KEY_F13,       engine::KeyCode::F13},
	{CACA_KEY_F14,       engine::KeyCode::F14},
	{CACA_KEY_F15,       engine::KeyCode::F15}
};

bool caca::Window::pollEvent(engine::Event &event)
{
	caca_event_t ev{};
	if (caca_get_event(m_display, CACA_EVENT_ANY, &ev, 0)) {
		event.type = event_list[caca_get_event_type(&ev)];
		if (event.type == engine::EventType::KeyPressed
		    || event.type == engine::EventType::KeyReleased)
			event.key.code = key_map[caca_get_event_key_ch(&ev)];
		else if (event.type == engine::EventType::MouseMoved) {
			event.mouseMove.x = caca_get_event_mouse_x(&ev);
			event.mouseMove.y = caca_get_event_mouse_y(&ev);
		} else if (event.type == engine::EventType::Resized) {
			event.resize.width = static_cast<size_t>(
				caca_get_event_resize_width(&ev));
			event.resize.height = static_cast<size_t>(
				caca_get_event_resize_height(&ev));
		}
		return true;
	} else
		return false;
}
