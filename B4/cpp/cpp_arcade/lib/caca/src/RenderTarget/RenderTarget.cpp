/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** RenderTarget.cpp
*/

#include <iostream>
#include <fstream>
#include "RenderTarget.hpp"
#include "Engine/Renderer/Sprite.hpp"
#include "Engine/Renderer/Text.hpp"

caca::RenderTarget::RenderTarget(caca_canvas_t *canvas) : m_canvas(canvas)
{}

void caca::RenderTarget::drawSprite(const engine::Sprite &sprite)
{
	engine::Vector2f pos = sprite.getPosition();
	std::string str;
	std::ifstream file(sprite.getAsciiFile(), std::ios::in);
	caca_set_color_argb(m_canvas, 0xffff, 0);
	if (file) {
		while (getline(file, str)) {
			caca_put_str(m_canvas,
				     static_cast<int>(pos.x / 10),
				     static_cast<int>(pos.y / 20),
				     (str + "\n").c_str());
			pos.y++;
		}
		file.close();
	}
}

void caca::RenderTarget::drawText(const engine::Text &text)
{
	uint16_t argb = 0;
	argb |= text.getColor().a / 16;
	argb <<= 4;
	argb |= text.getColor().r / 16;
	argb <<= 4;
	argb |= text.getColor().g / 16;
	argb <<= 4;
	argb |= text.getColor().b / 16;
	caca_set_color_argb(m_canvas, argb, 0);
	caca_put_str(
		m_canvas,
		static_cast<int>(text.getPosition().x / 10),
		static_cast<int>(text.getPosition().y / 20),
		text.getText().c_str());
}
