/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** RenderTarget.hpp
*/


#ifndef ARCADE_RENDERTARGET_HPP
#define ARCADE_RENDERTARGET_HPP

#include <caca.h>
#include "Engine/Renderer/RenderTarget.hpp"

namespace caca {
	class RenderTarget : public engine::RenderTarget {
	public:
		explicit RenderTarget(caca_canvas_t *canvas);
		void drawSprite(const engine::Sprite &sprite) override;
		void drawText(const engine::Text &text) override;

	private:
		caca_canvas_t *m_canvas;
	};
}

#endif //ARCADE_RENDERTARGET_HPP
