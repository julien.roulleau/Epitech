/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** Sprite.cpp
*/

#include "Engine/Renderer/Sprite.hpp"

void engine::Sprite::draw(RenderTarget &renderer) const
{
	renderer.drawSprite(*this);
}


void engine::Sprite::moveTo(Vector2f const &newPos)
{
	m_position = newPos;
}


void engine::Sprite::move(Vector2f const &offset)
{
	m_position.x += offset.x;
	m_position.y += offset.y;
}


engine::Vector2f const &engine::Sprite::getPosition() const
{
	return m_position;
}


void engine::Sprite::setImageFile(std::string const &filepath)
{
	m_imageFile = filepath;
}


void engine::Sprite::setAsciiFile(std::string const &filepath)
{
	m_asciiFile = filepath;
}


void engine::Sprite::setSize(Vector2f const &size)
{
	m_size = size;
}


void engine::Sprite::setRotation(float rotation)
{
	m_rotation = rotation;
}


std::string const &engine::Sprite::getImageFile() const
{
	return m_imageFile;
}


std::string const &engine::Sprite::getAsciiFile() const
{
	return m_asciiFile;
}


engine::Vector2f const &engine::Sprite::getSize() const
{
	return m_size;
}


float engine::Sprite::getRotation() const
{
	return m_rotation;
}