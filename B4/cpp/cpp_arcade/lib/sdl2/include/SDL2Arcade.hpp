/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 
*/

#pragma once

#include <SDL2/SDL.h>
#include <map>
#include "Engine/System/Event.hpp"

std::map<int, engine::KeyCode> keyMap = {
	{SDLK_ESCAPE, engine::KeyCode::Escape},
        {SDLK_UP, engine::KeyCode::Up},
        {SDLK_DOWN, engine::KeyCode::Down},
        {SDLK_RIGHT, engine::KeyCode::Right},
        {SDLK_LEFT, engine::KeyCode::Left},
        {SDLK_a, engine::KeyCode::A},
	{SDLK_b, engine::KeyCode::B},
	{SDLK_c, engine::KeyCode::C},
        {SDLK_d, engine::KeyCode::D},
	{SDLK_e, engine::KeyCode::E},
	{SDLK_f, engine::KeyCode::F},
	{SDLK_g, engine::KeyCode::G},
	{SDLK_h, engine::KeyCode::H},
	{SDLK_i, engine::KeyCode::I},
	{SDLK_j, engine::KeyCode::J},
	{SDLK_k, engine::KeyCode::K},
	{SDLK_l, engine::KeyCode::L},
	{SDLK_m, engine::KeyCode::M},
	{SDLK_n, engine::KeyCode::N},
	{SDLK_o, engine::KeyCode::O},
	{SDLK_p, engine::KeyCode::P},
	{SDLK_q, engine::KeyCode::Q},
	{SDLK_r, engine::KeyCode::R},
	{SDLK_s, engine::KeyCode::S},
	{SDLK_t, engine::KeyCode::T},
	{SDLK_u, engine::KeyCode::U},
	{SDLK_v, engine::KeyCode::V},
	{SDLK_w, engine::KeyCode::W},
	{SDLK_x, engine::KeyCode::X},
	{SDLK_y, engine::KeyCode::Y},
	{SDLK_z, engine::KeyCode::Z},
	{SDLK_0, engine::KeyCode::Num0},
	{SDLK_1, engine::KeyCode::Num1},
	{SDLK_2, engine::KeyCode::Num2},
	{SDLK_3, engine::KeyCode::Num3},
	{SDLK_4, engine::KeyCode::Num4},
	{SDLK_5, engine::KeyCode::Num5},
	{SDLK_6, engine::KeyCode::Num6},
	{SDLK_7, engine::KeyCode::Num7},
	{SDLK_8, engine::KeyCode::Num8},
	{SDLK_9, engine::KeyCode::Num9},
	{SDLK_0, engine::KeyCode::Num0},
	{SDLK_F1, engine::KeyCode::F1},
	{SDLK_F2, engine::KeyCode::F2},
	{SDLK_F3, engine::KeyCode::F3},
	{SDLK_F4, engine::KeyCode::F4},
	{SDLK_F5, engine::KeyCode::F5},
	{SDLK_F6, engine::KeyCode::F6},
	{SDLK_F7, engine::KeyCode::F7},
	{SDLK_F8, engine::KeyCode::F8},
	{SDLK_F9, engine::KeyCode::F9},
	{SDLK_F10, engine::KeyCode::F10},
	{SDLK_F11, engine::KeyCode::F11},
	{SDLK_F12, engine::KeyCode::F12},
	{SDLK_F13, engine::KeyCode::F13},
	{SDLK_F14, engine::KeyCode::F14},
	{SDLK_F15, engine::KeyCode::F15},
	{SDLK_BACKSPACE, engine::KeyCode::BackSpace},
	{SDLK_RETURN, engine::KeyCode::Return}
};

std::map<int, engine::MouseButton> mouseButtonMap =
	{
		{SDL_BUTTON_LEFT, engine::MouseButton::Left},
		{SDL_BUTTON_RIGHT, engine::MouseButton::Right},
		{SDL_BUTTON_MIDDLE, engine::MouseButton::Middle}
	};