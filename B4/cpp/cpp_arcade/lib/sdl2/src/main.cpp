/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** main.cpp
*/

#include <iostream>
#include <zconf.h>
#include <Engine/Renderer/Sprite.hpp>
#include <Engine/Renderer/Text.hpp>
#include "SDLWindow.hpp"
#include "SDLRenderer.hpp"


extern "C" {
void *instantiateWindow()
{
	return new SDLWindow;
}
}

/*int main()
{
	SDLWindow window;
	engine::Event ev;
	engine::RenderSettings settings;

	settings.fullscreen = false;
	settings.height = 600;
	settings.width = 800;

	window.create("Arcade", {static_cast<int>(settings.width),
				 static_cast<int>(settings.height)}, settings);
	engine::Sprite sprite;
	sprite.setRotation(0);
	sprite.moveTo({0, 0});
	sprite.setSize({300, 300});
	sprite.setImageFile("sdl2/urss.jpg");
	engine::Sprite sprite2;
	sprite2.moveTo({400, 0});
	sprite2.setRotation(0);
	sprite2.setSize({300, 300});
	sprite2.setImageFile("sdl2/urss.jpg");
        engine::Text blyat;
        blyat.moveTo({0, 0});
        blyat.setSize(34);
        engine::Color color{};
        color.r = 100;
        color.g = 100;
        color.b = 100;
        blyat.setColor(color);
        blyat.setText("Cyka blyat");
        blyat.setFont("sdl2/comic.ttf");
	while (window.isOpen()) {
		window.pollEvent(ev);
		if (ev.type == engine::EventType::Closed ||
		    (ev.type == engine::EventType::KeyPressed &&
		     ev.key.code == engine::KeyCode::Escape)) {
			window.close();
		}
		else {
			window.clear();
			sprite.move({1, 1.5});
			sprite2.move({1, 1});
			if (sprite.getPosition().y > settings.height)
				sprite.moveTo({0, 0});
			if (sprite2.getPosition().y > settings.height)
				sprite2.moveTo({400, 0});
			sprite2.setRotation(sprite2.getRotation() + 1 % 360);
			sprite.setRotation(sprite.getRotation() + 1 % 360);
			window.draw(sprite2);
			window.draw(sprite);
			SDL_Delay(14);
                        blyat.move({1, 0});
                        window.draw(blyat);
                        window.display();
		}
	}
	TTF_Quit();
	return (0);
}*/