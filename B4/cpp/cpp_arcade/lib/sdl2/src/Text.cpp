/*
** EPITECH PROJECT, 2018
** arcade
** File description:
**
*/

#include "Engine/Renderer/Text.hpp"

void engine::Text::draw(RenderTarget &renderer) const
{
	renderer.drawText(*this);
}

void engine::Text::moveTo(Vector2f const &newPos)
{
	m_position = newPos;
}

void engine::Text::move(Vector2f const &offset)
{
	m_position.x += offset.x;
	m_position.y += offset.y;
}

engine::Vector2f const &engine::Text::getPosition() const
{
	return m_position;
}

void engine::Text::setText(std::string const &text)
{
	m_text = text;
}

void engine::Text::setFont(std::string const &fontpath)
{
	m_font = fontpath;
}

void engine::Text::setSize(std::size_t size)
{
	m_size = size;
}

void engine::Text::setColor(Color const &color)
{
	m_color = color;
}

std::string const &engine::Text::getText() const
{
	return m_text;
}

std::string const &engine::Text::getFont() const
{
	return m_font;
}

std::size_t engine::Text::getSize() const
{
	return m_size;
}

engine::Color const &engine::Text::getColor() const
{
	return m_color;
}