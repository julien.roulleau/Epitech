/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 
*/

#include <SDL2/SDL.h>
#include <stdexcept>
#include <iostream>
#include <Engine/Renderer/Sprite.hpp>
#include <zconf.h>
#include "SDLWindow.hpp"
#include "../include/SDL2Arcade.hpp"
#include "SDLRenderer.hpp"
#include "Log.hpp"

SDLWindow::SDLWindow() : Window()
{
        if (SDL_Init(SDL_INIT_VIDEO) != 0)
		sdlError("Failed to init sdl");
      	if (TTF_Init() < 0) {
		sdlError("Failed to init ttf");
        }
        _is_open = false;
}

SDLWindow::~SDLWindow()
{
        for (auto &i : _renderMap) {
                delete i.second;
        }
}

void SDLWindow::create(std::string const &title, const engine::Vector2i &size, const engine::RenderSettings &settings)
{
        Uint32 flags = 0;
        if (settings.fullscreen)
                flags = SDL_WINDOW_FULLSCREEN;
        _window = SDL_CreateWindow(title.c_str(), size.x, size.y, static_cast<int>(settings.width),
				   static_cast<int>(settings.height), flags);
        if (_window == nullptr) {
                std::cerr << "Error: " << SDL_GetError() << std::endl;
                throw std::runtime_error("Cannot create SDL Window ");
        }
        _renderer = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE);
	if (_renderer == nullptr)
		sdlError("Renderer creation failed");
        _is_open = true;
}

void SDLWindow::close()
{
        _is_open = false;
        SDL_DestroyRenderer(_renderer);
        SDL_DestroyWindow(_window);
        SDL_Quit();
}

bool SDLWindow::isOpen() const
{
        return _is_open;
}

bool SDLWindow::pollEvent(engine::Event &event)
{
        if (!isOpen())
                std::cerr << "Warning: pollEvent after display()" << std::endl;
        SDL_Event ev = {0};
        std::map<int, engine::KeyCode>::iterator it;
        SDL_PollEvent(&ev);

        switch (ev.type) {
                case SDL_QUIT:
                        event.type = engine::EventType::Closed;
                        return true;
                case SDL_KEYUP:
                case SDL_KEYDOWN:
                        if (ev.type == SDL_KEYUP)
                        event.type = engine::EventType::KeyReleased;
                        else
                                event.type = engine::EventType::KeyPressed;
                        it = keyMap.find(ev.key.keysym.sym);
                        if (it == keyMap.end()) {
                                event.key.code = engine::KeyCode::Unknown;
                        }
                        else
                                event.key.code = it->second;
                        return true;
                case SDL_MOUSEMOTION:
                        event.type = engine::EventType::MouseMoved;
                        event.mouseMove.x = ev.wheel.x;
                        event.mouseMove.y = ev.wheel.y;
                        return true;
                case SDL_MOUSEBUTTONDOWN:
                case SDL_MOUSEBUTTONUP:
                        if (ev.type == SDL_MOUSEBUTTONDOWN)
                                event.type = engine::EventType::MouseButtonPressed;
                        else
                                event.type = engine::EventType::MouseButtonReleased;
                        auto mb = mouseButtonMap.find(ev.button.button);
                        if (mb != mouseButtonMap.end()) {
                                event.mouseButton.button = mb->second;
                        }
                        else {
                                std::cerr << "Unknown mouse button" << std::endl;
                                event.type = engine::EventType::None;
                        }
                        return true;

        }
        return false;
}

void SDLWindow::clear()
{
        if (!isOpen())
                std::cerr << "Warning: Clear after close()" << std::endl;
        SDL_SetRenderDrawColor(_renderer, 0, 0, 0, 255);
        SDL_RenderClear(_renderer);
        //SDL_RenderPresent(_renderer);
}

void SDLWindow::draw(const engine::Drawable &drawable)
{
        if (!isOpen())
                std::cerr << "Warning: Draw after close()" << std::endl;
        if (_renderMap.find(&drawable) == _renderMap.end()) {
                _renderMap.insert(std::make_pair(&drawable, new SDLRenderer(_renderer)));
        }
        engine::RenderTarget *target = _renderMap.at(&drawable);
        drawable.draw(*target);
}

void SDLWindow::display()
{
        if (!isOpen())
                std::cerr << "Warning: Draw after display()" << std::endl;
        SDL_RenderPresent(_renderer);
}
