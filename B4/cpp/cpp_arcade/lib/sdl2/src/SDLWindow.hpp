/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 
*/

#pragma once

#include <memory>
#include <SDL2/SDL.h>
#include <map>
#include "Engine/Window/Window.hpp"

class SDLWindow : public engine::Window
{
public:
	SDLWindow();

	~SDLWindow() override;

	void create(std::string const &title, const engine::Vector2i &size, const engine::RenderSettings &settings) override;

	void close() override;

	bool isOpen() const override;

	bool pollEvent(engine::Event &event) override;

	void clear() override;

	void draw(const engine::Drawable &drawable) override;

	void display() override;

private:
	SDL_Window *_window;
	bool _is_open;
        SDL_Renderer *_renderer;
        std::map<const engine::Drawable *, engine::RenderTarget *> _renderMap;
};