/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 
*/

#include <iostream>
#include <SDL2/SDL_image.h>
#include "Log.hpp"
#include "Engine/Renderer/Sprite.hpp"
#include "Engine/Renderer/Text.hpp"
#include "SDLRenderer.hpp"

SDLRenderer::SDLRenderer(SDL_Renderer *windowRenderer) :
	_renderer(windowRenderer), _init(false) , _rect((SDL_Rect){0, 0, 0, 0})
{
        _surface = nullptr;
        _texture = nullptr;
        _font = nullptr;
}

SDLRenderer::~SDLRenderer() {
	freeComponents();
}

void SDLRenderer::freeComponents()
{
	if (_font)
		TTF_CloseFont(_font);
	if (_texture)
		SDL_DestroyTexture(_texture);
	if (_surface)
		SDL_FreeSurface(_surface);
}

void SDLRenderer::drawSprite(const engine::Sprite &sprite) {
	if (!_init || strcmp(sprite.getImageFile().c_str(), _text.c_str()))
		initSprite(sprite);
	_rect = {static_cast<int>(sprite.getPosition().x),
		 static_cast<int>(sprite.getPosition().y),
		 static_cast<int>(sprite.getSize().x),
		 static_cast<int>(sprite.getSize().y)};
	int result =
		SDL_RenderCopyEx(_renderer, _texture, nullptr, &_rect,
                                 sprite.getRotation(), nullptr, SDL_FLIP_NONE);
	if (result == -1)
		sdlError("Rendercopy error");
}

void SDLRenderer::drawText(const engine::Text &text) {
	if (!_init || isTextDifferent(text)) {
		initText(text);
	}
	_rect = {static_cast<int>(text.getPosition().x),
		 static_cast<int>(text.getPosition().y),
		 _surface->w, _surface->h};
	SDL_RenderCopy(_renderer, _texture, nullptr, &_rect);
}

bool SDLRenderer::isTextDifferent(const engine::Text &text) {
	if (strcmp(text.getText().c_str(), _text.c_str()))
		return true;
	if (text.getColor().rgba != _color || text.getSize() != _size)
		return true;
	return (strcmp(text.getText().c_str(), _text.c_str()) != 0);
}

void SDLRenderer::initSprite(const engine::Sprite &sprite)
{
	freeComponents();
	_surface = IMG_Load(sprite.getImageFile().c_str());
	if (!_surface)
		sdlError("Error while creating Surface");
	_texture = SDL_CreateTextureFromSurface(_renderer, _surface);
	if (!_texture)
		sdlError("Error while creating Texture");
	_text = sprite.getImageFile();
	_init = true;
}

void SDLRenderer::initText(const engine::Text &text) {
	freeComponents();
	SDL_Color color = {text.getColor().r, text.getColor().g,
			   text.getColor().b, text.getColor().a};
	_font = TTF_OpenFont(text.getFont().c_str(),
			     static_cast<int>(text.getSize()));
	if (_font == NULL)
		sdlError("Failed to load font");
	_surface = TTF_RenderText_Solid(_font, text.getText().c_str(), color);
	if (_surface == nullptr)
		sdlError("Error");
	_texture = SDL_CreateTextureFromSurface(_renderer, _surface);
	if (_texture == nullptr)
		sdlError("Error");
	_size = text.getSize();
	_text = text.getText().c_str();
	_color = text.getColor().rgba;
	_init = true;
}
