/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 
*/

#include <SDL2/SDL_quit.h>
#include "Log.hpp"

void info(const std::string &str)
{
	char buff[100];
	time_t now = time (0);
	strftime (buff, 100, "%H:%M:%S.000", localtime (&now));
	std::cout << "[INFO] " << buff << ": " << str << std::endl;
}

void sdlError(const std::string &str)
{
	std::cerr << "Error: " << SDL_GetError() << std::endl;
	throw std::runtime_error(str);
}