/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 
*/

#pragma once

#include <SDL2/SDL_ttf.h>
#include "Engine/Renderer/RenderTarget.hpp"
#include "SDLWindow.hpp"

class SDLRenderer : public engine::RenderTarget{

public:

        SDLRenderer(SDL_Renderer *windowRenderer);

        ~SDLRenderer() override;

        void drawSprite(const engine::Sprite &sprite) override;

        void drawText(const engine::Text &text) override;


private:
        bool isTextDifferent(const engine::Text &text);
        void initText(const engine::Text &text);


        SDL_Renderer *_renderer;
        bool _init;
	SDL_Rect _rect;

	TTF_Font *_font;
        SDL_Texture *_texture;
        SDL_Surface *_surface;

        std::string _text;
        std::size_t _size;
        std::uint32_t _color;
        engine::Vector2f pos;

        void freeComponents();

	void initSprite(const engine::Sprite &sprite);
};
