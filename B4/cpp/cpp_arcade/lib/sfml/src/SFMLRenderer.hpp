/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 08/04/18: SFMLRenderer.hpp
*/

#ifndef ARCADE_SFMLRENDERER_HPP
#define ARCADE_SFMLRENDERER_HPP

class SFMLRenderer;

#include "Engine/Renderer/RenderTarget.hpp"
#include "SFMLWindow.hpp"

class SFMLRenderer : public engine::RenderTarget {
public:
	SFMLRenderer(sf::RenderWindow &);

	~SFMLRenderer() override = default;

	void drawSprite(const engine::Sprite &sprite) override;

	void drawText(const engine::Text &text) override;
private:
	sf::RenderWindow &m_window;
	sf::Sprite m_sprite;
	sf::Texture m_texture;
	sf::Font m_font;
	sf::Text m_text;
	bool m_isInit{ false };
	std::string path;
};


#endif //ARCADE_SFMLRENDERER_HPP
