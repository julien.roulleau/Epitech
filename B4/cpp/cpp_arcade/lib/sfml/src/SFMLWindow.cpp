/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 08/04/18: SFMLWindow.cpp
*/

#include "SFMLWindow.hpp"

SFMLWindow::SFMLWindow()
{
	m_renderer = new SFMLRenderer(_window);
}

SFMLWindow::~SFMLWindow()
{
	delete m_renderer;
}

void SFMLWindow::create(std::string const &title, const engine::Vector2i &size,
			const engine::RenderSettings &settings)
{
	(void)settings;
	_window.create(sf::VideoMode((unsigned int)size.x,
						     (unsigned int)size.y),
				       title, sf::Style::Default);
}

void SFMLWindow::close()
{
	_window.close();
}

bool SFMLWindow::isOpen() const
{
	return _window.isOpen();
}

std::map<int, engine::KeyCode> keyMap = {
	{sf::Keyboard::Return, engine::KeyCode::Return},
	{sf::Keyboard::A, engine::KeyCode::A},
	{sf::Keyboard::B, engine::KeyCode::B},
	{sf::Keyboard::C, engine::KeyCode::C},
	{sf::Keyboard::D, engine::KeyCode::D},
	{sf::Keyboard::E, engine::KeyCode::E},
	{sf::Keyboard::F, engine::KeyCode::F},
	{sf::Keyboard::G, engine::KeyCode::G},
	{sf::Keyboard::H, engine::KeyCode::H},
	{sf::Keyboard::I, engine::KeyCode::I},
	{sf::Keyboard::J, engine::KeyCode::J},
	{sf::Keyboard::K, engine::KeyCode::K},
	{sf::Keyboard::L, engine::KeyCode::L},
	{sf::Keyboard::M, engine::KeyCode::M},
	{sf::Keyboard::N, engine::KeyCode::N},
	{sf::Keyboard::O, engine::KeyCode::O},
	{sf::Keyboard::P, engine::KeyCode::P},
	{sf::Keyboard::Q, engine::KeyCode::Q},
	{sf::Keyboard::R, engine::KeyCode::R},
	{sf::Keyboard::S, engine::KeyCode::S},
	{sf::Keyboard::T, engine::KeyCode::T},
	{sf::Keyboard::U, engine::KeyCode::U},
	{sf::Keyboard::V, engine::KeyCode::V},
	{sf::Keyboard::W, engine::KeyCode::W},
	{sf::Keyboard::X, engine::KeyCode::X},
	{sf::Keyboard::Y, engine::KeyCode::Y},
	{sf::Keyboard::Z, engine::KeyCode::Z},
	{sf::Keyboard::Num0, engine::KeyCode::Num0},
	{sf::Keyboard::Num1, engine::KeyCode::Num1},
	{sf::Keyboard::Num2, engine::KeyCode::Num2},
	{sf::Keyboard::Num3, engine::KeyCode::Num3},
	{sf::Keyboard::Num4, engine::KeyCode::Num4},
	{sf::Keyboard::Num5, engine::KeyCode::Num5},
	{sf::Keyboard::Num6, engine::KeyCode::Num6},
	{sf::Keyboard::Num7, engine::KeyCode::Num7},
	{sf::Keyboard::Num8, engine::KeyCode::Num8},
	{sf::Keyboard::Num9, engine::KeyCode::Num9},
	{sf::Keyboard::Escape, engine::KeyCode::Escape},
	{sf::Keyboard::LControl, engine::KeyCode::LControl},
	{sf::Keyboard::LShift, engine::KeyCode::LShift},
	{sf::Keyboard::LAlt, engine::KeyCode::LAlt},
	{sf::Keyboard::LSystem, engine::KeyCode::LSystem},
	{sf::Keyboard::RControl, engine::KeyCode::RControl},
	{sf::Keyboard::RShift, engine::KeyCode::RShift},
	{sf::Keyboard::RAlt, engine::KeyCode::RAlt},
	{sf::Keyboard::RSystem, engine::KeyCode::RSystem},
	{sf::Keyboard::SemiColon, engine::KeyCode::SemiColon},
	{sf::Keyboard::Comma, engine::KeyCode::Comma},
	{sf::Keyboard::Period, engine::KeyCode::Quote},
	{sf::Keyboard::Quote, engine::KeyCode::Quote},
	{sf::Keyboard::Slash, engine::KeyCode::Slash},
	{sf::Keyboard::BackSlash, engine::KeyCode::BackSlash},
	{sf::Keyboard::Tilde, engine::KeyCode::Tilde},
	{sf::Keyboard::Equal, engine::KeyCode::Equal},
	{sf::Keyboard::Dash, engine::KeyCode::Dash},
	{sf::Keyboard::Space, engine::KeyCode::Space},
	{sf::Keyboard::Return, engine::KeyCode::Return},
	{sf::Keyboard::BackSpace, engine::KeyCode::BackSpace},
	{sf::Keyboard::Tab, engine::KeyCode::Tab},
	{sf::Keyboard::PageUp, engine::KeyCode::PageUp},
	{sf::Keyboard::PageDown, engine::KeyCode::PageDown},
	{sf::Keyboard::End, engine::KeyCode::End},
	{sf::Keyboard::Home, engine::KeyCode::Home},
	{sf::Keyboard::Insert, engine::KeyCode::Insert},
	{sf::Keyboard::Delete, engine::KeyCode::Delete},
	{sf::Keyboard::Left, engine::KeyCode::Left},
	{sf::Keyboard::Right, engine::KeyCode::Right},
	{sf::Keyboard::Up, engine::KeyCode::Up},
	{sf::Keyboard::Down, engine::KeyCode::Down},
	{sf::Keyboard::Numpad0, engine::KeyCode::Num0},
	{sf::Keyboard::Numpad1, engine::KeyCode::Num1},
	{sf::Keyboard::Numpad2, engine::KeyCode::Num2},
	{sf::Keyboard::Numpad3, engine::KeyCode::Num3},
	{sf::Keyboard::Numpad4, engine::KeyCode::Num4},
	{sf::Keyboard::Numpad5, engine::KeyCode::Num5},
	{sf::Keyboard::Numpad6, engine::KeyCode::Num6},
	{sf::Keyboard::Numpad7, engine::KeyCode::Num7},
	{sf::Keyboard::Numpad8, engine::KeyCode::Num8},
	{sf::Keyboard::Numpad9, engine::KeyCode::Num9},
	{sf::Keyboard::F1, engine::KeyCode::F1},
	{sf::Keyboard::F2, engine::KeyCode::F2},
	{sf::Keyboard::F3, engine::KeyCode::F3},
	{sf::Keyboard::F4, engine::KeyCode::F4},
	{sf::Keyboard::F5, engine::KeyCode::F5},
	{sf::Keyboard::F6, engine::KeyCode::F6},
	{sf::Keyboard::F7, engine::KeyCode::F7},
	{sf::Keyboard::F8, engine::KeyCode::F8},
	{sf::Keyboard::F9, engine::KeyCode::F9},
	{sf::Keyboard::F10, engine::KeyCode::F10},
	{sf::Keyboard::F11, engine::KeyCode::F11},
	{sf::Keyboard::F12, engine::KeyCode::F12},
	{sf::Keyboard::F13, engine::KeyCode::F13},
	{sf::Keyboard::F14, engine::KeyCode::F14},
	{sf::Keyboard::F15, engine::KeyCode::F15}
};

bool SFMLWindow::pollEvent(engine::Event &eventArcade)
{
	sf::Event eventSfml;

	if (_window.pollEvent(eventSfml)) {
		if (eventSfml.type == sf::Event::Closed)
		{
			close();
			return false;
		}
		if (eventSfml.type == sf::Event::KeyPressed &&
			keyMap.find(eventSfml.key.code) != keyMap.end()) {
			eventArcade.key.code = keyMap.find(eventSfml.key.code)
				->second;
			eventArcade.type = engine::EventType::KeyPressed;
		}
		return true;
	}
	return false;
}

void SFMLWindow::clear()
{
	_window.clear();
}

void SFMLWindow::draw(const engine::Drawable &drawable)
{
	if (_renderMap.find(&drawable) == _renderMap.end()) {
		_renderMap.insert(std::make_pair(&drawable, new SFMLRenderer(_window)));
	}
	engine::RenderTarget *target = _renderMap.at(&drawable);
	drawable.draw(*target);
}

void SFMLWindow::display()
{
	_window.display();
}
