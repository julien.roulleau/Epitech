/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 08/04/18: main.cpp
*/

#include "SFMLWindow.hpp"

extern "C" {
void *instantiateWindow() {
	return new SFMLWindow();
}
}