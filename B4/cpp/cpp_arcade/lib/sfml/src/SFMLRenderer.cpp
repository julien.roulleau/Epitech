/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 08/04/18: SFMLRenderer.cpp
*/

#include "SFMLRenderer.hpp"

SFMLRenderer::SFMLRenderer(sf::RenderWindow &window) : m_window(window),
						       path("")
{
}

void SFMLRenderer::drawSprite(const engine::Sprite &sprite)
{
	if (!m_isInit || sprite.getImageFile() != path) {
		if (!m_texture.loadFromFile(sprite.getImageFile())) {
			std::cerr << dlerror() << std::endl;
			throw std::exception();
		}
		m_sprite.setTexture(m_texture);
		// Set position, size, etc.
		path = sprite.getImageFile();
		m_isInit = true;
	}
	m_sprite.setScale(sprite.getSize().x / m_texture.getSize().x,
			  sprite.getSize().y / m_texture.getSize().y);
	m_sprite.setOrigin(m_texture.getSize().x / (float) 2.0,
			   m_texture.getSize().y / (float) 2.0);
	m_sprite.setPosition(
		sprite.getPosition().x + sprite.getSize().x / (float) 2,
		sprite.getPosition().y + sprite.getSize().y / (float) 2);
	m_sprite.setRotation(sprite.getRotation());
	m_window.draw(m_sprite);
}

void SFMLRenderer::drawText(const engine::Text &text) {
	if (!m_isInit) {
		if (!m_font.loadFromFile(text.getFont())) {
			std::cerr << dlerror() << std::endl;
			throw std::exception();
		}
		m_isInit = true;
	}
	m_text.setFont(m_font);
	m_text.setPosition(text.getPosition().x, text.getPosition().y);
	m_text.setCharacterSize((unsigned int) text.getSize());
	sf::Color color(text.getColor().r, text.getColor().g,
			text.getColor().b);
	m_text.setColor(color);
	m_text.setString(text.getText());
	m_window.draw(m_text);
}
