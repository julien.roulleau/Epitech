/*
** EPITECH PROJECT, 2018
** arcade
** File description:
** 08/04/18: SFMLWindow.hpp
*/

#ifndef ARCADE_SFMLWINDOW_HPP
#define ARCADE_SFMLWINDOW_HPP

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <dlfcn.h>
#include <iostream>
#include <exception>
#include "Engine/Window/Window.hpp"
#include "SFMLRenderer.hpp"
#include "Engine/Renderer/Sprite.hpp"
#include "Engine/Renderer/Text.hpp"

class SFMLWindow : public engine::Window {
public:
	SFMLWindow();

	~SFMLWindow() override;

	void create(std::string const &title, const engine::Vector2i &size, const engine::RenderSettings &settings) override;

	void close() override;

	bool isOpen() const override;

	bool pollEvent(engine::Event &event) override;

	void clear() override;

	void draw(const engine::Drawable &drawable) override;

	void display() override;
private:
	sf::RenderWindow _window;
	SFMLRenderer *m_renderer;
	std::map<const engine::Drawable *, engine::RenderTarget *> _renderMap;
};


#endif //ARCADE_SFMLWINDOW_HPP
