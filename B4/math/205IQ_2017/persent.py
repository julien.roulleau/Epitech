import math


def gauss(m, d, i):
    return (1.0 / (d * math.sqrt(2 * math.pi))) * math.exp(-0.5 * ((i - m) / d)**2)


def calc(mean, der, min_value, max_value=-1):
    if mean == 0 or der == 0:
        print("Invalid argument", file=stderr)
        exit(84)
    if max_value == -1:
        max_value = min_value
        min_value = 0
    ret = 0
    t = min_value
    while t < max_value:
        ret += gauss(mean, der, t) * 100
        t += 0.001
    return ret / 1000


def display(value, min_value, max_value=-1):
    if max_value == -1:
        print("{:0.1f}% of people have an IQ inferior to {}".format(value, min_value))
    else:
        print("{:0.1f}% of people have an IQ between {} and {}".format(value, min_value, max_value))
