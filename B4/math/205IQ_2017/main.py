#!/usr/bin/env python3

from sys import argv, stderr
import gauss
import persent


def main():
    if len(argv) == 2 and argv[1] == "-h":
        display_help()
    elif 3 <= len(argv) <= 5:
        check_argument(argv)
        av = [*map(lambda x: int(x), argv[1:])]
        if len(argv) == 3:
            gauss.display(gauss.calc(*av))
        elif 4 <= len(argv) <= 5:
            if len(argv) == 5 and av[-2] > av[-1]:
                print("Invalid argument", file=stderr)
                exit(84)
            persent.display(persent.calc(*av), *av[2:])
    else:
        print("Invalid number of argument", file=stderr)
        exit(84)


def check_argument(values):
    for value in values[1:]:
        if not value.isdigit():
            print("Invalid argument", file=stderr)
            exit(84)


def display_help():
    print("""USAGE
            ./205IQ µ σ [IQ1] [IQ2]

DESCRIPTION
            µ predetermined mean
            σ predetermined standard deviation
            IQ1 minimum IQ
            IQ2 maximum IQ""")


if __name__ == '__main__':
    main()