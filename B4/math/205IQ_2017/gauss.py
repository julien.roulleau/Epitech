import math


def calc(mean, der):
    return [(100 * (1.0 / (der * math.sqrt(2 * math.pi))) * math.exp(-0.5 * ((i - mean) / der)**2)) for i in range(0, 200)]


def display(values):
    i = 0
    for value in values:
        print("{} {:0.2f}".format(i, value))
        i += 1
