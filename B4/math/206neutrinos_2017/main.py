#!/usr/bin/env python3

from sys import argv, stderr
import neutinos


def main():
    values = check_arg(argv[1:])
    while True:
        inp = input("indtast din vaerdi :\t")
        if inp == "ENDE":
            break
        try:
            inp = float(inp)
        except ValueError:
            print("Need number")
            continue
        values, quadratic_mean = neutinos.calc(*values, inp)
        neutinos.display(values, quadratic_mean)


def check_arg(values):
    arg = []
    for value in values:
        try:
            arg.append(float(value))
            if arg[-1] <= 0:
                print("Only greater than zero parameter", file=stderr)
                exit(84)
        except ValueError:
            print("Only int parameter", file=stderr)
            exit(84)
    if len(arg) != 4:
        print("Need 4 parameters", file=stderr)
        exit(84)
    return arg


if __name__ == '__main__':
    main()
