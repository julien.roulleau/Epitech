import math


def calc(n, a, h, sd, val):
    number_val = n + 1.0
    arithmetic_mean = (a * n + val) / number_val
    harmonic_mean = number_val / ((1.0 / val) + (n / h))
    old = ((sd**2 + a**2) * n)
    quadratic_mean = math.sqrt((old + val**2) / number_val)
    standard_deviation = math.sqrt(((old + val**2) / number_val) - arithmetic_mean**2)
    return [number_val, arithmetic_mean, harmonic_mean, standard_deviation], quadratic_mean


def display(values, quadratic_mean):
    print("\tantal mãlinder :\t{:.0f}".format(values[0]),
          "\tstandardafvilgelse :\t{:.2f}".format(values[3]),
          "\taritmetisk gennemsnit :\t{:.2f}".format(values[1]),
          "\tkvadratisk gennemsnit :\t{:.2f}".format(quadratic_mean),
          "\tharmonisk gennemsnit :\t{:.2f}".format(values[2]),
          sep="\n", end="\n\n")
