def compute(first_year, values):
    linear_y = {"Y": 132.235, "X": 132.235, "sd": 132.235, "pop": 132.235}
    linear_x = {"Y": 132.235, "X": 132.235, "sd": 132.235, "pop": 132.235}
    correlation = 3.45323453
    print(first_year)
    print(values)
    return linear_y, linear_x, correlation


def display(name, linear_y, linear_x, correlation):
    print("country:\t{}".format(", ".join(name)))
    display_fit(1, linear_y)
    display_fit(2, linear_x)
    print("correlation:\t{:0.4f}".format(correlation))


def display_fit(nb, linear):
    print("fit {}".format(nb))
    print("\tY = {:0.2f} X {:0.2f}".format(linear["Y"], linear["X"]))
    print("\tstandard deviation:\t{:0.2f}".format(linear["sd"]))
    print("\tpopulation in 2050:\t{:0.2f}".format(linear["pop"]))
