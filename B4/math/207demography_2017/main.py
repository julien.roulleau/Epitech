#!/usr/bin/env python3

from sys import argv, stderr
import Linear_regression


def main(av):
    if len(av) < 2:
        print("Invalid number of argument", file=stderr)
        exit(84)
    if av[1] == "-h":
        display_help()
        exit(0)
    first_year, name, values = get_data(av[1:])
    Linear_regression.display(name, *Linear_regression.compute(first_year, values))


def get_data(countries):
    name = []
    values = []
    with open("207demography_data.csv") as file:
        first_year = file.readline().split(';')[2]
        if not first_year.isdigit():
            print("Invalid file data", file=stderr)
            exit(84)
        first_year = int(first_year)
        for line in file:
            line = line.replace(',', '.')
            data = line[:-1].split(';')
            if data[1] in countries:
                if not check_data(data[2:]):
                    print("Invalid file data", file=stderr)
                    exit(84)
                name.append(data[0])
                values.append([float(x) if x != '' else -1 for x in data[2:]])
    if len(name) != len(countries):
        print("Invalid country code", file=stderr)
        exit(84)
    return first_year, name, values


def check_data(values):
    for value in values:
        try:
            float(value)
        except ValueError:
            if value != '':
                return False
    return True


def display_help():
    print("""./207demography -h
USAGE
\t./207demography code1 [...]

DESCRIPTION
\tcode1 country code""")


if __name__ == '__main__':
    main(argv)