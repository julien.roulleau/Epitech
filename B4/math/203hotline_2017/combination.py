from binomial import coeff_binomial


def process_combination(n, k):
    return coeff_binomial(n, k)


def display_combination(n, k, value):
    print("{}-combination of a {} set:\n{}".format(k, n, value))
