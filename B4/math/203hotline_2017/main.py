#!/usr/bin/env python3
import sys
from BinomialDistribution import process_binomial_distribution, display_binomial_distribution
from PoissonDistribution import process_poisson_distribution, display_poisson_distribution
from combination import process_combination, display_combination


def main():
    if len(sys.argv) == 2:
        if sys.argv[1] == "-h":
            help()
            exit(0)
        else:
            if not sys.argv[1].isdigit():
                print("Invalid argument", file=sys.stderr)
                exit(84)
            display_binomial_distribution(*process_binomial_distribution(int(sys.argv[1])))
            print()
            display_poisson_distribution(*process_poisson_distribution(int(sys.argv[1])))
            exit(0)
    elif len(sys.argv) == 3:
        if not sys.argv[1].isdigit() or not sys.argv[2].isdigit():
            print("Invalid argument", file=sys.stderr)
            exit(84)
        n, k = int(sys.argv[1]), int(sys.argv[2])
        if k > n:
            print("Invalid value", file=sys.stderr)
            exit(84)
        display_combination(n, k, process_combination(n, k))
        exit(0)
    else:
        print("Invalid number of arguments", file=sys.stderr)
        exit(84)


def help():
    print("""USAGE
            ./203hotline [n k | d]
    
DESCRIPTION
            n   n value for the computation of (nk)
            k   k value for the computation of (nk)
            d   average duration of calls (in seconds)""")


main()
