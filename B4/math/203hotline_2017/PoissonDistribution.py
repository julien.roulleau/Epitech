import math
import functools

from datetime import datetime


def poisson_calc(l, k):
    try:
        return (math.exp(-l) * l ** k) / math.factorial(k)
    except:
        return 0


def poisson_process(l, k):
    while 1:
        result = poisson_calc(l, k)
        if not result < 0.00001:
            yield result
            k += 1
            break
        yield 0.
        k += 1
    while 1:
        result = poisson_calc(l, k)
        if result < 0.00001:
            break
        yield result
        k += 1
    while k < 51:
        yield 0.
        k += 1


def process_poisson_distribution(d):
    start_time = datetime.now()
    if d > 750:
        return [0 for i in range(51)], 100, (datetime.now() - start_time).microseconds
    else:
        y = 3500 * d / 28800
        tab = [i for i in poisson_process(y, 0)]
        return tab, functools.reduce(lambda x, j: x + j, tab[26:]) * 100, (datetime.now() - start_time).microseconds


def display_poisson_distribution(values, overload, time):
    print("Poisson distribution:")
    for i in range(51):
        print("{} -> {:<3.3f}".format(i, values[i]), end="")
        if (i + 1) % 6 and i != 50:
            print("\t", end="")
        else:
            print()
    print("overload: {:0.1f}%".format(overload))
    print("computation time: {:0.2f} ms".format(time))
