import functools

from datetime import datetime

from binomial import binomial_rule


def binomial_process(n, k, p):
    while 1:
        result = binomial_rule(n, k, p)
        if not result < 0.000001:
            yield result
            k += 1
            break
        yield 0.
        k += 1
    while 1:
        result = binomial_rule(n, k, p)
        if result < 0.000001:
            break
        yield result
        k += 1
    while k < 51:
        yield 0.
        k += 1


def process_binomial_distribution(d):
    start_time = datetime.now()
    if d > 750:
        return [0 for i in range(51)], 100, (datetime.now() - start_time).microseconds * 10
    else:
        nb_calls = 3500
        total_time = 28800
        p = d / total_time
        tab = [i for i in binomial_process(nb_calls, 0, p)]
        return tab, functools.reduce(lambda x, y: x + y, tab[26:]) * 100, (datetime.now() - start_time).microseconds


def display_binomial_distribution(values, overload, time):
    print("Binomial distribution:")
    for i in range(51):
        print("{} -> {:<3.3f}".format(i, values[i]), end="")
        if (i + 1) % 6 and i != 50:
            print("\t", end="")
        else:
            print()
    print("overload: {:0.1f}%".format(overload))
    print("computation time: {:0.2f} ms".format(time))
