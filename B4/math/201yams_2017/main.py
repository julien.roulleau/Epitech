#!/usr/bin/env python3
import sys
from algo import pair, three, four, full, straight, yams


def main():
    if len(sys.argv) == 2 and sys.argv[1] == "-h":
        help()
        exit(0)
    if len(sys.argv) != 7:
        print("Invalid number of arguments", file=sys.stderr)
        exit(84)
    algorithms = {"pair": pair, "three": three, "four": four, "full": full, "straight": straight, "yams": yams}
    dices = [sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5]]
    check_number_argument(dices)
    combination = sys.argv[6].split('_')
    check_number_argument(combination[1:])
    if combination[0] not in algorithms:
        print("Invalid algorithm", file=sys.stderr)
        exit(84)
    algorithms[combination[0]]([int(dice) for dice in dices], *[int(value) for value in combination[1:]])


def check_number_argument(array):
    for value in array:
        if not value.isdigit() or not 0 <= int(value) <= 6:
            print("Invalid number", file=sys.stderr)
            exit(84)


def help():
    print("""USAGE
    ./201yams d1 d2 d3 d4 d5 c

DESCRIPTION
    d1 value of the first die (0 if not thrown)
    d2 value of the second die (0 if not thrown)
    d3 value of the third die (0 if not thrown)
    d4 value of the fourth die (0 if not thrown)
    d5 value of the fifth die (0 if not thrown)
    c expected combination""")


main()
