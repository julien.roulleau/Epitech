import sys

from algo.binomial import binomial_process


def yams(dices, whant=0, *args):
    if whant == 0 or len(args):
        print("Invalid combination", file=sys.stderr)
        exit(84)
    print("chances to get a {} yams:  {:0.2f}%".format(whant, binomial_process(5, dices.count(whant)) * 100))
