import math


def coeff_binomial(n, k):
    return math.factorial(n) / (math.factorial(k) * math.factorial((n - k)))


def binomial_rule(n, k, p):
    return coeff_binomial(n, k) * math.pow(p, k) * math.pow(1 - p, n - k)


def binomial_process(whant, have):
    n = 5 - have
    k = whant - have
    if k > 0:
        P = 0
        while whant <= 5:
            P += binomial_rule(n, k, 1/6)
            whant += 1
            k += 1
        return P
    else:
        return 1
