import math

import sys

from algo.binomial import coeff_binomial


def full(dices, whant1=0, whant2=0, *args):
    if whant1 == 0 or whant2 == 0 or whant1 == whant2 or len(args):
        print("Invalid combination", file=sys.stderr)
        exit(84)
    three = 1
    pair = 1
    count1 = dices.count(whant1)
    count1 = count1 if count1 < 3 else 3
    count2 = dices.count(whant2)
    count2 = count2 if count2 < 2 else 2
    if 3 - count1 > 0:
        three = coeff_binomial(5 - count1 - count2, 3 - count1)
    if 2 - count2 > 0:
        pair = coeff_binomial(2 - count2, 2 - count2)
    print("chances to get a {} full of {}:  {:0.2f}%".format(
        whant1, whant2, ((three * pair) / math.pow(6, 5 - count1 - count2)) * 100))
