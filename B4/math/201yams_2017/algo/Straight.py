import math

import sys


def straight(dices, whant=0, *args):
    if whant != 5 and whant != 6 or len(args):
        print("Invalid combination", file=sys.stderr)
        exit(84)
    count = 5 - len(set(dices) - {0, 6 if whant == 5 else 1})
    print("chances to get a {} straight:  {:0.2f}%".format(whant, math.factorial(count) / math.pow(6, count) * 100))
