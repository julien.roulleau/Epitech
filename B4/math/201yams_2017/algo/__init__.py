from .Pair import pair
from .Three import three
from .Four import four
from .Full import full
from .Straight import straight
from .Yams import yams
