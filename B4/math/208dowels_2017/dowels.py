import math

def get_proba(brut):
    n = 0
    p = 0
    for i in brut:
        p += n * i
        n += 1
    p /= 10000
    return p


def combinaison(n, k):
    num = math.factorial(k)
    denum1 = math.factorial(n)
    denum2 = math.factorial(k - n)
    denum1 *= denum2
    return num / denum1


def get_TX(p, Ox, ref):
    Tx = []
    for i in range(len(Ox) - 1):
        result = 0
        for x in ref[i]:
            result += 100 * combinaison(x, 100) * math.pow(p, x) * math.pow((1 - p), 100 - x)
        Tx.append(result)
    Tx.append(100 - sum(Tx))
    return Tx


def get_ssd(Ox, Tx):
    X = 0
    for i in range(len(Ox)):
        X += (Ox[i] - Tx[i])**2 / Tx[i]
    return X


def compute(ref, brut, concat):
    p = get_proba(brut)
    Tx = get_TX(p, concat, ref)
    ssd = get_ssd(concat, Tx)
    return Tx, p, ssd


def str_head(values):
    heads = []
    for value in values:
        if len(value) == 1:
            heads.append(value[0])
        else:
            heads.append("{}-{}".format(value[0], value[-1]))
    heads[-1] = "{}+".format(values[-1][0])
    return heads


def display_ref(first, values, last):
    print("{:^7}|".format(first), end="")
    for value in str_head(values):
        print("{:^7}|".format(value), end="")
    print("{:>7}".format(last))


def display_concat(first, values, last):
    print("{:^7}|".format(first), end="")
    for value in values:
        print("{:^7d}|".format(int(value)), end="")
    print("{:>7}".format(last))


def display_line(first, values, last):
    print("{:^7}|".format(first), end="")
    for value in values:
        print("{:^7.1f}|".format(value), end="")
    print("{:>7}".format(last))


def str_fit(sq, free):
    tab = [
        [99, 90, 80, 70, 60, 50, 40, 30, 20, 10, 5, 2, 1],
        [0.00, 0.02, 0.06, 0.15, 0.27, 0.45, 0.71, 1.07, 1.64, 2.71, 3.84, 5.41, 6.63],
        [0.02, 0.21, 0.45, 0.71, 1.02, 1.39, 1.83, 2.41, 3.22, 4.61, 5.99, 7.82, 9.21],
        [0.11, 0.58, 1.01, 1.42, 1.87, 2.37, 2.95, 3.66, 4.64, 6.25, 7.81, 9.84, 11.35],
        [0.30, 1.06, 1.65, 2.19, 2.75, 3.36, 4.04, 4.88, 5.99, 7.78, 9.49, 11.67, 13.28],
        [0.55, 1.61, 2.34, 3.00, 3.66, 4.35, 5.13, 6.06, 7.29, 9.24, 11.07, 13.33, 15.01],
        [0.70, 2.20, 3.07, 3.83, 4.57, 5.35, 6.21, 7.23, 8.56, 10.64, 12.59, 15.03, 16.81],
        [1.24, 2.83, 3.82, 4.67, 5.49, 6.35, 7.28, 8.38, 9.80, 12.02, 14.07, 16.62, 18.48],
        [1.65, 3.49, 4.59, 5.53, 6.42, 7.34, 8.35, 9.52, 11.03, 13.36, 15.51, 18.17, 20.09],
        [2.09, 4.17, 5.38, 6.39, 7.36, 8.34, 9.41, 10.66, 12.24, 14.68, 16.92, 19.63, 21.67],
        [2.56, 4.87, 6.18, 7.27, 8.30, 9.34, 10.47, 11.78, 13.44, 15.99, 18.31, 21.16, 23.21]
    ]
    if sq < tab[free][0]:
        return "P>99%"
    for i, x in enumerate(tab[free]):
        if sq < x:
            return "{}%<P<{}%".format(tab[0][i], tab[0][i - 1])
    return "P<1%"


def display(ref, concat, line, distribution, sq):
    display_ref("x", ref, "total")
    display_concat("Ox", concat, "100")
    display_line("Tx", line, "100")
    print("distribution:\t\t\tB(100, {:0.4f})".format(distribution))
    print("sum of the square differences:\t{:0.3f}".format(sq))
    print("freedom degrees:\t\t{:0.0f}".format(len(ref) - 2))
    print("fit validity:\t\t\t{}".format(str_fit(sq, len(ref) - 2)))
