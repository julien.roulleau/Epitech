import math


def coeff_binomial(n, k):
    try:
        return math.factorial(n) // (math.factorial(k) * math.factorial((n - k)))
    except:
        return 0


def binomial_rule(n, k, p):
    try:
        return coeff_binomial(n, k) * math.pow(p, k) * math.pow(1 - p, n - k)
    except:
        return 0
