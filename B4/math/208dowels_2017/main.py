#!/usr/bin/env python3

from sys import argv, stderr
import dowels


def main():
    if len(argv) == 2 and argv[1] == "-h":
        display_help()
        exit(0)
    elif len(argv) != 10:
        print("Invalid number of arguments", file=stderr)
        exit(84)
    brut = get_args(argv[1:])
    ref, concat = concat_values(brut)
    dowels.display(ref, concat, *dowels.compute(ref, brut, concat))


def concat_values(values):
    ref, brut = [[x] for x in range(9)], [*values]
    i = 0
    m = 8
    while i < len(brut):
        if brut[i] < 10:
            if 0 < i < m:
                if brut[i - 1] <= brut[i + 1]:
                    ref[i - 1] += ref.pop(i)
                    brut[i - 1] += brut.pop(i)
                    m -= 1
                else:
                    ref[i] += ref.pop(i + 1)
                    brut[i] += brut.pop(i + 1)
                    m -= 1
            elif 0 < i:
                ref[i - 1] += ref.pop(i)
                brut[i - 1] += brut.pop(i)
                m -= 1
            elif i < m:
                ref[i] += ref.pop(i + 1)
                m -= 1
                brut[i] += brut.pop(i + 1)
            i -= 1
        i += 1
    return ref, brut


def get_args(values):
    try:
        tab = [int(value) for value in values]
        if sum(tab) != 100:
            print("Invalid parameter", file=stderr)
            exit(84)
        return tab
    except ValueError:
        print("Invalid parameter", file=stderr)
        exit(84)


def display_help():
    print("""/208dowels -h
USAGE
\t\t./208dowels O0 O1 O2 O3 O4 O5 O6 O7 O8+

DESCRIPTION
\t\tOi\tsize of the observed class""")


if __name__ == '__main__':
    main()
