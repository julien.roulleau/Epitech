#!/usr/bin/env python3

import sys
from calc import calc, display


def main():
    if len(sys.argv) != 2:
        print("Invalid number of arguments", file=sys.stderr)
        exit(84)
    if sys.argv[1] == "-h":
        help()
        exit(0)
    if not isfloat(sys.argv[1]):
        print("Invalid parameters", file=sys.stderr)
        exit(84)
    value = float(sys.argv[1])
    if value < 0 or value > 2.5:
        print("Out of range", file=sys.stderr)
        exit(84)
    display(calc(value))


def isfloat(value):
    try:
        float(value)
        return True
    except ValueError:
        return False


def help():
    print("""USAGE
            ./204ducks a

DESCRIPTION
            a   constant""")


main()
