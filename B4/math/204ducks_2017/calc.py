import math
import datetime


def func(t, a):
    return (2*a-4)*(-math.exp(-4*t)/4)+(4-3*a)*(-math.exp(-2*t)/2)+a*(-math.exp(-t))

def func2(t, a):
    return a * math.exp(-t) + (4 - 3 * a) * math.exp(-2 * t) + (2 * a - 4) * math.exp(-4 * t)


def back(time_back, value):
    i = 1.0
    while True:
        if func(i / 60, value) - func(0, value) > time_back:
            return i
        i += 0.1



def integration(value):
    a = 0
    b = 10

    n = 50
    h = (b - a) / (n + 1)

    ret = 0

    for i in range(0, n - 1):
        ret += func(a + i * h, value)
    ret *= (b - a) / n
    return ret


def var(value, esp):
    #a = 0
    #b = 10
    #n = 500
    #h = (b - a) / n
    #var = 0
    #for i in range(1, n):
    #    var += ((func(i*h, value) * -b * 60) - (esp))**2
    #var = var / n / 60 * h / 3
    return 0
    #return math.sqrt((value - esp)**2 * float(integration(value)))


def esp(value):
    a = 0
    b = 10
    n = 1000
    h = (b - a) / n
    area = 0
    for i in range(0, n):
        area += func(i * h, value)
    area = area * -1 * (b / n)
    return area * 60


def calc(value):
    average_time = int(esp(value))
    standart_variation = var(value, average_time)
    back_50 = back(0.5, value)
    back_99 = back(0.99, value)
    percent_1 = int(((func(1, value) - func(0, value)) * 100) * 10) / 10
    percent_2 = int(((func(2, value) - func(0, value)) * 100) * 10) / 10
    return [datetime.datetime(2000, 1, 1) + datetime.timedelta(seconds=round(average_time)),
            standart_variation,
            datetime.datetime(2000, 1, 1) + datetime.timedelta(seconds=round(back_50)),
            datetime.datetime(2000, 1, 1) + datetime.timedelta(seconds=round(back_99)),
            percent_1,
            percent_2]


def display(values):
    print("average return time: {}m {:02d}s".format(values[0].minute, values[0].second),
          "standard deviation: {:0.3f}".format(values[1]),
          "time after which 50% of the ducks come back: {}m {:02d}s".format(values[2].minute, values[2].second),
          "time after which 99% of the ducks come back: {}m {:02d}s".format(values[3].minute, values[3].second),
          "percentage of ducks back after 1 minute: {:0.1f}%".format(values[4]),
          "percentage of ducks back after 2 minutes: {:0.1f}%".format(values[5]),
          sep="\n")
