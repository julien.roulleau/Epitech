def proba_simple(a, x):
    return (a - x) / (5 * a - 150)


def proba_double(a, x, b, y):
    return ((a - x) * (b - y)) / ((5 * a - 150) * (5 * b - 150))
