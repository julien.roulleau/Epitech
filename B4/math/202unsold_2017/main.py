#!/usr/bin/env python3
import sys

from calc import cal_tab
from display import display
from part2 import part2
from part3 import part3


def main():
    if len(sys.argv) == 2 and sys.argv[1] == "-h":
        display_help()
        exit(0)
    if len(sys.argv) != 3:
        print("Invalid number of arguments", file=sys.stderr)
        exit(84)
    arg = [sys.argv[1], sys.argv[2]]
    check_argument(arg)
    arg = [*map(float, arg)]
    tab = cal_tab(*arg)
    display(tab, part2(*arg), part3(tab))


def check_argument(array):
    for value in array:
        if not value.isdigit() or not int(value) > 50:
            print("Invalid number", file=sys.stderr)
            exit(84)


def display_help():
    print("""USAGE
    ./202unsold a b

DESCRIPTION
    a constant computed from the past results
    b constant computed from the past results""")


main()
