from proba import proba_double, proba_simple


def part2(a, b):
    return [
        proba_double(a, 10, b, 10),
        proba_double(a, 10, b, 20) + proba_double(a, 20, b, 10),
        proba_double(a, 10, b, 30) + proba_double(a, 20, b, 20) + proba_double(a, 30, b, 10),
        proba_double(a, 10, b, 40) + proba_double(a, 20, b, 30) + proba_double(a, 30, b, 20) + proba_double(a, 40, b, 10),
        proba_double(a, 10, b, 50) + proba_double(a, 20, b, 40) + proba_double(a, 30, b, 30) + proba_double(a, 40, b, 20) + proba_double(a, 50, b, 10),
        proba_double(a, 20, b, 50) + proba_double(a, 30, b, 40) + proba_double(a, 40, b, 30) + proba_double(a, 50, b, 20),
        proba_double(a, 30, b, 50) + proba_double(a, 40, b, 40) + proba_double(a, 50, b, 30),
        proba_double(a, 40, b, 50) + proba_double(a, 50, b, 40),
        proba_double(a, 50, b, 50),
        1
    ]
