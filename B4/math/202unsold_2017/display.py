def display(tab, Z, esperence):
    print("––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––")
    print("\tX=10\tX=20\tX=30\tX=40\tX=50\tY law",
          "Y=10\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}".format(*tab[0]),
          "Y=20\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}".format(*tab[1]),
          "Y=30\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}".format(*tab[2]),
          "Y=40\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}".format(*tab[3]),
          "Y=50\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}".format(*tab[4]),
          "X law\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:1}".format(*tab[5]), sep='\n')
    print("––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––")
    print("z\t20\t30\t40\t50\t60\t70\t80\t90\t100\ttotal",
          "p(Z=z)\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:0.3f}\t{:1}".format(*Z),
          sep='\n')
    print("––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––")
    print("expected value of X:\t{:0.1f}".format(esperence[0]),
          "variance of X:\t\t{:0.1f}".format(esperence[1]),
          "expected value of Y:\t{:0.1f}".format(esperence[2]),
          "variance of Y:\t\t{:0.1f}".format(esperence[3]),
          "expected value of Z:\t{:0.1f}".format(esperence[4]),
          "variance of Z:\t\t{:0.1f}".format(esperence[5]),
          sep="\n")
    print("––––––––––––––––––––––––––––––––––––––––––––––––––––––––––––")
