#!/usr/bin/env python3

from sys import argv, stderr
import poll


def main():
    if len(argv) == 2 and argv[1]:
        display_help()
        exit(0)
    ppsize, ssize, p = test_args(argv[1:])
    poll.display(ppsize, ssize, p, poll.compute(ppsize, ssize, p))


def test_args(args):
    if len(args) != 3:
        print("Invalid number of arguments", file=stderr)
        exit(84)
    try:
        ppsize = int(args[0])
        ssize = int(args[1])
        p = float(args[2])
        if not (ppsize > 0 and ssize > 0 and 0 <= p <= 100 and ppsize >= ssize):
            raise ValueError
        return ppsize, ssize, p
    except ValueError:
        print("Invalid parameter", file=stderr)
        exit(84)


def display_help():
    print("""USAGE
\t\t./209poll pSize sSize p

DESCRIPTION
\t\tppSize\tsize of the population
\t\tsSize\tsize of the sample (supposed to be representative)
\t\tp\tpercentage of voting intentions for a specific candidate""")


if __name__ == '__main__':
    main()
