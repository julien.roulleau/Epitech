import math


def compute(ppsize, ssize, p):
    p /= 100
    return ((ppsize - ssize) / (ppsize - 1)) * ((p * (1 - p)) / ssize)


def display(ppsize, ssize, p, v):
    print("population size:\t\t{}".format(ppsize))
    print("sample size:\t\t\t{}".format(ssize))
    if int(p) == p:
        print("voting intentions:\t\t{:d}%".format(int(p)))
    else:
        print("voting intentions:\t\t{:0.2f}%".format(p))
    print("variance:\t\t\t{:0.6f}".format(v))
    ci95 = 100 * 1.96 * math.sqrt(v)
    print("95% confidence interval:\t[{:0.2f}% ; {:0.2f}%]".format(
        p - ci95 if p - ci95 > 0 else 0, p + ci95 if p + ci95 < 100 else 100))
    ci99 = 100 * 2.58 * math.sqrt(v)
    print("99% confidence interval:\t[{:0.2f}% ; {:0.2f}%]".format(
        p - ci99 if p - ci99 > 0 else 0, p + ci99 if p + ci99 < 100 else 100))
