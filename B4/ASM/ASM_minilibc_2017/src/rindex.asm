BITS 64

SECTION .text

GLOBAL rindex

rindex:
    push rbp
    mov rbp, rsp
    xor rax, rax ;; recherche \0
    mov rcx, -1
    repne scasb ;; while (--rcx && *rdi != *al) {rdi++}
    not rcx
    inc rcx
    dec rdi
    std ;; string <-
    mov al, sil ;; recherche sil
    repne scasb ;; while (--rcx && *rdi != *al) {rdi++}
    cmp rcx, 0
    je null ;; is befor the string
    inc rdi
	mov rax, rdi
	jmp return
null:
    xor rax, rax
return:
	cld ;; string ->
    leave
    ret