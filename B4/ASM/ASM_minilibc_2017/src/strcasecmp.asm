BITS 64

SECTION .text

GLOBAL strcasecmp

strcasecmp:
    push rbp
    mov rbp, rsp

start:
    mov r9b, [rdi]
    mov r8b, [rsi]

lower_rdi:
    cmp r9b, 'Z'
    jg lower_rsi
    cmp r9b, 'A'
    jl lower_rsi
    add r9b, 32

lower_rsi:
    cmp r8b, 'Z'
    jg cmp_rsi_rdi
    cmp r8b, 'A'
    jl cmp_rsi_rdi
    add r8b, 32

cmp_rsi_rdi:
    cmp r9b, 0
    je diff
    cmp r8b, 0
    je fail
    cmp r9b, r8b
    jne fail
    inc rdi
    inc rsi
    jmp start

diff:
    cmp r8b, 0
    jne fail

success:
    xor rax, rax
    jmp end

fail:
    movsx rax, r9b ;; move with sign char in response
    sub al, r8b ;; diff two char
    movsx rax, al ;; etend al for negative number

end:
    leave
    ret