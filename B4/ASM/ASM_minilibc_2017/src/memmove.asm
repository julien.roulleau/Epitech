BITS 64

SECTION .text

GLOBAL memmove

memmove:
    push rbp
    mov rbp, rsp
    push rdi
    cmp rdi, rsi
    jl cpy ;; *rdi < *rsi
    std ;; string <-
    add rdi, rdx ;; go end
    dec rdi
    add rsi, rdx ;; go end
    dec rsi

cpy:
    mov rcx, rdx ;; len
    rep movsb ;; while (--rcx) {*rdi = *rsi; rdi++; rsi++}
    cld ;; string ->
    pop rax
    leave
    ret