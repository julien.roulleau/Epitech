BITS 64

SECTION .text

GLOBAL strchr

strchr:
    push rbp
    mov rbp, rsp
	push rdi

loop:
	cmp BYTE [rdi], sil
	je success
	cmp BYTE [rdi], 0
	je fail
	inc rdi
	jmp loop

success:
	mov rax, rdi
    jmp end

fail:
    xor rax, rax

end:
    pop rdi
    leave
    ret