BITS 64

SECTION .text

GLOBAL write

write:
    push rbp
    mov rbp, rsp
    mov rax, 1
    syscall
    leave
    ret