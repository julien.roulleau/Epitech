BITS 64

SECTION .text

GLOBAL strcspn

strcspn:
    push rbp
    mov rbp, rsp
    xor rdx, rdx

loop:
    xor rcx, rcx
    cmp byte [rdi + rdx], 0
    je end

cmp_loop:
    mov al, byte [rsi + rcx]
    cmp al, 0
    je next
    cmp byte [rdi + rdx], al
    je end
    inc rcx
    jmp cmp_loop

next:
    inc rdx
    jmp loop

end:
    mov rax, rdx

    leave
    ret