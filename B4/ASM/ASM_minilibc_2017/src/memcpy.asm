BITS 64

SECTION .text

GLOBAL memcpy

memcpy:
    push rbp
    mov rbp, rsp
    mov rcx, rdx ;; len
    push rdi
    rep movsb ;; while (--rcx) {*rdi = *rsi; rdi++; rsi++}
    pop rax
    leave
    ret