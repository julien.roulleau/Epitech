BITS 64

SECTION .text

GLOBAL strcmp

strcmp:
    push rbp
    mov rbp, rsp
    push rdi
        xor rax, rax ;; recherche \0
        mov rcx, -1
        repne scasb ;; while (--rcx && *rdi != *al) {rdi++}
        not rcx ;; rdi size
        dec rcx
        mov rdx, rcx
    pop rdi
    push rdi
        mov rdi, rsi
        xor rax, rax ;; recherche \0
        mov rcx, -1
        repne scasb ;; while (--rcx && *rdi != *al) {rdi++}
        not rcx ;; rsi size
        dec rcx
    pop rdi
    cmp rcx, rdx ;; take bigger
    jg cmp
    mov rcx, rdx

cmp:
    repe cmpsb ;; while (--rcx && *rdi == *rsi) {rdi++; rsi++}
    dec rdi ;; rdi --
    dec rsi ;; rsi --
    movsx rax, byte [rdi] ;; move with sign char in response
    sub al, [rsi] ;; diff two char
    movsx rax, al ;; etend al for negative number
    leave
    ret