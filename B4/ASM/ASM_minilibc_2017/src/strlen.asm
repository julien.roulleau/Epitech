BITS 64

SECTION .text

GLOBAL strlen

strlen:
    push rbp
    mov rbp, rsp
    xor rax, rax ;; recherche \0
    mov rcx, -1
    repne scasb ;; while (--rcx && *rdi != *al) {rdi++}
	mov rax, rcx
	not rax ;; rax *= -1
    dec rax ;; rax--
    leave
    ret