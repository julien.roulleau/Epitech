BITS 64

SECTION .text

GLOBAL strncmp

strncmp:
    push rbp
    mov rbp, rsp
    push rdi
        xor rax, rax ;; recherche \0
        mov rcx, -1
        repne scasb ;; while (--rcx && *rdi != *al) {rdi++}
        not rcx ;; rdi size
        dec rcx
        mov r8, rcx
    pop rdi
    push rdi
        mov rdi, rsi
        xor rax, rax ;; recherche \0
        mov rcx, -1
        repne scasb ;; while (--rcx && *rdi != *al) {rdi++}
        not rcx ;; rsi size
        dec rcx
    pop rdi
    cmp rcx, r8 ;; take bigger
    jg setn
    mov rcx, r8

setn:
    cmp rcx, rdx ;; take smaller
    jl cmp
    mov rcx, rdx

cmp:
    repe cmpsb ;; while (--rcx && *rdi == *rsi) {rdi++; rsi++}
    dec rdi ;; rdi --
    dec rsi ;; rsi --
    movsx rax, byte [rdi] ;; move with sign char in response
    sub al, [rsi] ;; diff two char
    movsx rax, al ;; etend al for negative number
    leave
    ret