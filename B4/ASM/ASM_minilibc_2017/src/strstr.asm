BITS 64

SECTION .text

GLOBAL strstr

strstr:
    push rbp
    mov rbp, rsp

    xor rcx, rcx ;; init
    jmp loop

restart:
    cmp byte [rdi], 0
    je fail
    inc rdi
    xor rcx, rcx

loop:
    mov r8b, [rsi + rcx]
    cmp r8b, 0
    je success
    cmp byte [rdi + rcx], r8b
    jne restart
    inc rcx
    jmp loop

success:
    mov rax, rdi
    jmp end

fail:
    xor rax, rax

end:
    leave
    ret