BITS 64

SECTION .text

GLOBAL memset

memset:
    push rbp
    mov rbp, rsp
    mov al, sil ;; set value
    mov rcx, rdx ;; len
    push rdi
    rep stosb ;; while (--rcx) {*rdi = al; rdi++}
    pop rax
    leave
    ret