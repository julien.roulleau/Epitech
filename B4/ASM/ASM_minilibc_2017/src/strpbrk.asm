BITS 64

SECTION .text

GLOBAL strpbrk

strpbrk:
    push rbp
    mov rbp, rsp

loop:
    xor rcx, rcx
    cmp byte [rdi], 0
    je none

cmp_loop:
    mov al, byte [rsi + rcx]
    cmp al, 0
    je next
    cmp byte [rdi], al
    je find
    inc rcx
    jmp cmp_loop

next:
    inc rdi
    jmp loop

none:
    xor rax, rax
    jmp end

find:
    mov rax, rdi

end:
    leave
    ret