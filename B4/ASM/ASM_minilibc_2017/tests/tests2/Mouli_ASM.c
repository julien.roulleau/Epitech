#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#ifndef __VALIDATE_TEST__
#define __VALIDATE_TEST__(a) ((a) ? "\033[32mOK\033[0m" : "\033[31mKO\033[0m") 
#endif // ! __VALIDATE_TEST__

char *strdup(char const *);

extern unsigned long strlen(char const * const);
extern char *strchr(char const * const, int);
extern void *memset(void *, int, size_t);
extern void *memmove(void *, const void *, size_t);
extern char *rindex(char const * const, int c);
extern int strcmp(char const * const, char const * const);
extern int strcasecmp(char const * const, char const * const);
extern int strncmp(char const * const, char const * const, unsigned long);
extern char *strstr(char const * const, char const * const);
extern char *strpbrk(char const * const, char const * const);
extern unsigned long strcspn(char const * const, char const * const);

char const * const test_s[] = 
  {
    "Hello world !\n",
    "HelloHello wor",
    "HelloHello wor",
    "Hello wor",
    "HHelloHello wor",
    "aaaa",
    "aac",
    "a",
    "zz",
    "a",
    "b",
    "dd",
    "da",
    "",
  };

static
int
str_equal(char const * const a,
	  char const * const b)
{
  int i = 0;

  while (a[i] && b[i]) {
    if (a[i] != b[i]) {
      return 0;
    }
    ++i;
  }
  return !a[i] && !b[i];
}

static
int
test_strcspn()
{
  int tp = 0;

  tp += strcspn("32222aaaaa11123456", "a2") == 1;
  tp += strcspn("2222aaaaa11123456", "b1") == 9;
  tp += strcspn("1111", "abcdef1") == 0;
  return tp == 3;
}

static
int
test_strpbrk()
{
  int tp = 0;

  tp += strpbrk(*test_s, "zab") == 0;
  tp += strpbrk(*test_s, "zaw") == *test_s + 6;
  tp += strpbrk(*test_s, "H") == *test_s;
  tp += strpbrk(*test_s, "\n") == *test_s + 13;
  return tp == 4;
}

static
int
test_strstr()
{
  int tp = 0;

  tp += strstr(*test_s, "Hello") == *test_s;
  tp += strstr(*test_s, "world") == *test_s + 6;
  tp += strstr(*test_s, "zeus") == 0;
  tp += strstr(*test_s, "!\n") == *test_s + 12;
  tp += strstr(*test_s, "") == *test_s;
  tp += strstr("", "Hey") == NULL;
  tp += strstr(test_s[13], "") == test_s[13];
  return tp == 7;
}

static
int
test_strcasecmp()
{
  int tp = 0;

  tp += strcasecmp("A", "a") == 0;
  tp += strcasecmp("aaAa", "AAaA") == 0;
  tp += strcasecmp("b", "B") == 0;
  tp += strcasecmp("a", "Z") == -25;
  tp += strcasecmp("A", "z") == -25;
  tp += strcasecmp("A", "_") == 2;
  tp += strcasecmp("a", "_") == 2;
  tp += strcasecmp("abcdefgha", "ABCDEFGHZ") == -25;
  tp += strcasecmp("", "") == 0;
  return tp == 9;
}

static
int
test_strncmp()
{
  int tp = 0;

  tp += strncmp(*test_s, *test_s, 50) == 0;
  tp += strncmp(*test_s, *test_s, 0) == 0;
  tp += strncmp(*test_s, *test_s, 10) == 0;
  tp += strncmp(*test_s, *(test_s + 1), 50) == -40;
  tp += strncmp(*test_s, *(test_s + 1), 5) == 0;
  tp += strncmp(*test_s, *(test_s + 1), 6) == -40;
  tp += strncmp(*(test_s + 9), *(test_s + 10), 100) == -1;
  tp += strncmp(*(test_s + 9), *(test_s + 10), 0) == 0;
  tp += strncmp(*(test_s + 5), *(test_s + 6), 5) == -2;
  tp += strncmp(*(test_s + 11), *(test_s + 12), 1) == 0;
  tp += strncmp(*(test_s + 11), *(test_s + 12), 2) == 3;
  tp += strncmp(*(test_s + 11), *(test_s + 12), 3) == 3;
  return tp == 12;
}

static
int
test_strcmp()
{
  int tp = 0;

  tp += strcmp(*(test_s + 5), *(test_s + 6)) == -2;
  tp += strcmp(*(test_s + 7), *(test_s + 8)) == -25;
  tp += strcmp(*(test_s + 7), *(test_s + 11)) == -3;
  tp += strcmp(*(test_s + 9), *(test_s + 10)) == -1;
  tp += strcmp(*(test_s + 6), *(test_s + 5)) == 2;
  tp += strcmp(*(test_s + 8), *(test_s + 7)) == 25;
  tp += strcmp("", "") == 0;
  return tp == 7;
}

static
int
test_strlen()
{
  int tp = 0;

  tp += strlen("") == 0L;
  tp += strlen("\0hey you") == 0L;
  tp += strlen("a") == 1L;
  tp += strlen("0321590I342") == 11L;
  tp += strlen("	 ezae 	 				\neza	0") == 19L;
  tp += strlen(" \t") == 2L;
  return tp == 6;
}

static
int
test_memset()
{
  int tp = 0;
  char *str = strdup("Hey, how are you?");

  tp += !strcmp("ooooooooooooooooo", memset(str, 'o', 17));
  tp += !strlen(memset(str, '\0', 17));
  free(str);
  return tp == 2;
}

static
int
test_memcpy()
{
  int tp = 0;
  char *str = strdup("Hey, how are you?");

  tp += !strcmp("ooooooooooooooooo", memcpy(str, "ooooooooooooooooo", 17));
  tp += !strcmp("ooooooooooooooooo", memcpy(str, "ddddddddddddddddd", 0));
  tp += !strcmp("ddddooooooooooooo", memcpy(str, "ddddddddddddddddd", 4));
  free(str);
  return tp == 3;
}

static
int
test_strchr()
{
  int tp = 0;

  tp += strchr(*test_s, 'H') == *test_s;
  tp += strchr(*test_s, ' ') == *test_s + 5;
  tp += strchr(*test_s, '\n') == *test_s + 13;
  tp += strchr(*test_s, 0) == *test_s + 14;
  tp += strchr(*test_s, '0') == *test_s + 14;
  return tp == 4;
}

static
int
test_memmove()
{
  int tp = 0;
  char b[20];

  for (int i = 0; i < 20; ++i){b[i] = 0;}
  tp += str_equal(*test_s, memmove(b, *test_s, 15));
  tp += str_equal(*(test_s + 3), memmove(b + 5, b, 9));
  tp += str_equal(*(test_s + 1), b);
  tp += str_equal(*(test_s + 2), memmove(b + 1, b, 14));
  tp += str_equal(*(test_s + 4), b);
  return tp == 5;
}

static
int
test_rindex()
{
  int tp = 0;

  tp += rindex(*test_s, 'H') == *test_s;
  tp += rindex(*test_s, 'e') == (*test_s) + 1;
  tp += rindex(*test_s, 'z') == 0;
  tp += rindex(*test_s, '\n') == (*test_s) + 13;
  return tp == 4;
}

int
main()
{
  printf("\n===automatic tests [asm_minilibc]===\n\n");
  printf("strlen:     [%s]\n", __VALIDATE_TEST__(test_strlen()));
  printf("strchr:     [%s]\n", __VALIDATE_TEST__(test_strchr()));
  printf("memset:     [%s]\n", __VALIDATE_TEST__(test_memset()));
  printf("memcpy:     [%s]\n", __VALIDATE_TEST__(test_memcpy()));
  printf("memmove:    [%s]\n", __VALIDATE_TEST__(test_memmove()));
  printf("rindex:     [%s]\n", __VALIDATE_TEST__(test_rindex()));
  printf("strcmp:     [%s]\n", __VALIDATE_TEST__(test_strcmp()));
  printf("strncmp:    [%s]\n", __VALIDATE_TEST__(test_strncmp()));
  printf("strcasecmp: [%s]\n", __VALIDATE_TEST__(test_strcasecmp()));
  printf("strstr:     [%s]\n", __VALIDATE_TEST__(test_strstr()));
  printf("strpbrk:    [%s]\n", __VALIDATE_TEST__(test_strpbrk()));
  printf("strcspn:    [%s]\n", __VALIDATE_TEST__(test_strcspn()));
  return 0;
}
