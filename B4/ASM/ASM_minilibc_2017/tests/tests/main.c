/*
** EPITECH PROJECT, 2018
** ASM_minilibc
** File description:
** main.c
*/

#include <dlfcn.h>
#include <stdio.h>

unsigned long (*my_strlen)(char const *const) = NULL;
char *(*my_strchr)(char const *const, int) = NULL;
void *(*my_memset)(void *, int, size_t) = NULL;
void *(*my_memmove)(void *, const void *, size_t) = NULL;
void *(*my_memcpy)(void *, const void *, size_t) = NULL;
char *(*my_rindex)(char const *const, int c) = NULL;
int (*my_strcmp)(char const *const, char const *const) = NULL;
int (*my_strcasecmp)(char const *const, char const *const) = NULL;
int (*my_strncmp)(char const *const, char const *const, unsigned long) = NULL;
char *(*my_strstr)(char const *const, char const *const) = NULL;
char *(*my_strpbrk)(char const *const, char const *const) = NULL;
unsigned long (*my_strcspn)(char const *const, char const *const) = NULL;

void init(void *dl)
{
	my_strlen = dlsym(dl, "strlen");
	my_strchr = dlsym(dl, "strchr");
	my_memset = dlsym(dl, "memset");
	my_memmove = dlsym(dl, "memmove");
	my_memcpy = dlsym(dl, "memcpy");
	my_rindex = dlsym(dl, "rindex");
	my_strcmp = dlsym(dl, "strcmp");
	my_strcasecmp = dlsym(dl, "strcasecmp");
	my_strncmp = dlsym(dl, "strncmp");
	my_strstr = dlsym(dl, "strstr");
	my_strpbrk = dlsym(dl, "strpbrk");
	my_strcspn = dlsym(dl, "strcspn");
}

int main()
{
	void *dl = dlopen("../../libasm.so", RTLD_LOCAL | RTLD_LAZY);

	init(dl);
	printf("%zu\n", my_strlen("aze"));
	dlclose(dl);
}
