#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

#ifndef __VALIDATE_TEST__
#define __VALIDATE_TEST__(a) ((a) ? "\033[32mOK\033[0m" : "\033[31mKO\033[0m")
#endif // ! __VALIDATE_TEST__

unsigned long (*my_strlen)(char const *const) = NULL;
char *(*my_strchr)(char const *const, int) = NULL;
void *(*my_memset)(void *, int, size_t) = NULL;
void *(*my_memmove)(void *, const void *, size_t) = NULL;
void *(*my_memcpy)(void *, const void *, size_t) = NULL;
char *(*my_rindex)(char const *const, int c) = NULL;
int (*my_strcmp)(char const *const, char const *const) = NULL;
int (*my_strcasecmp)(char const *const, char const *const) = NULL;
int (*my_strncmp)(char const *const, char const *const, unsigned long) = NULL;
char *(*my_strstr)(char const *const, char const *const) = NULL;
char *(*my_strpbrk)(char const *const, char const *const) = NULL;
unsigned long (*my_strcspn)(char const *const, char const *const) = NULL;

char const *const test_s[] =
	{
		"Hello world !\n",
		"HelloHello wor",
		"HelloHello wor",
		"Hello wor",
		"HHelloHello wor",
		"aaaa",
		"aac",
		"a",
		"zz",
		"a",
		"b",
		"dd",
		"da",
		"",
	};

static
int
str_equal(char const *const a,
	  char const *const b)
{
	int i = 0;

	while (a[i] && b[i]) {
		if (a[i] != b[i]) {
			return 0;
		}
		++i;
	}
	return !a[i] && !b[i];
}

static
int
test_strcspn()
{
	int tp = 0;

	tp += my_strcspn("32222aaaaa11123456", "a2") == 1;
	tp += my_strcspn("2222aaaaa11123456", "b1") == 9;
	tp += my_strcspn("1111", "abcdef1") == 0;
	return tp == 3;
}

static
int
test_strpbrk()
{
	int tp = 0;

	tp += my_strpbrk(*test_s, "zab") == 0;
	tp += my_strpbrk(*test_s, "zaw") == *test_s + 6;
	tp += my_strpbrk(*test_s, "H") == *test_s;
	tp += my_strpbrk(*test_s, "\n") == *test_s + 13;
	return tp == 4;
}

static
int
test_strstr()
{
	int tp = 0;

	tp += my_strstr(*test_s, "Hello") == *test_s;
	tp += my_strstr(*test_s, "world") == *test_s + 6;
	tp += my_strstr(*test_s, "zeus") == 0;
	tp += my_strstr(*test_s, "!\n") == *test_s + 12;
	tp += my_strstr(*test_s, "") == *test_s;
	tp += my_strstr("", "Hey") == NULL;
	tp += my_strstr(test_s[13], "") == test_s[13];
	return tp == 7;
}

static
int
test_strcasecmp()
{
	int tp = 0;

	tp += my_strcasecmp("A", "a") == 0;
	tp += my_strcasecmp("aaAa", "AAaA") == 0;
	tp += my_strcasecmp("b", "B") == 0;
	tp += my_strcasecmp("a", "Z") == -25;
	tp += my_strcasecmp("A", "z") == -25;
	tp += my_strcasecmp("A", "_") == 2;
	tp += my_strcasecmp("a", "_") == 2;
	tp += my_strcasecmp("abcdefgha", "ABCDEFGHZ") == -25;
	tp += my_strcasecmp("", "") == 0;
	return tp == 9;
}

static
int
test_strncmp()
{
	int tp = 0;

	tp += my_strncmp(*test_s, *test_s, 50) == 0;
	tp += my_strncmp(*test_s, *test_s, 0) == 0;
	tp += my_strncmp(*test_s, *test_s, 10) == 0;
	tp += my_strncmp(*test_s, *(test_s + 1), 50) == -40;
	tp += my_strncmp(*test_s, *(test_s + 1), 5) == 0;
	tp += my_strncmp(*test_s, *(test_s + 1), 6) == -40;
	tp += my_strncmp(*(test_s + 9), *(test_s + 10), 100) == -1;
	tp += my_strncmp(*(test_s + 9), *(test_s + 10), 0) == 0;
	tp += my_strncmp(*(test_s + 5), *(test_s + 6), 5) == -2;
	tp += my_strncmp(*(test_s + 11), *(test_s + 12), 1) == 0;
	tp += my_strncmp(*(test_s + 11), *(test_s + 12), 2) == 3;
	tp += my_strncmp(*(test_s + 11), *(test_s + 12), 3) == 3;
	return tp == 12;
}

static
int
test_strcmp()
{
	int tp = 0;

	tp += my_strcmp(*(test_s + 5), *(test_s + 6)) == -2;
	tp += my_strcmp(*(test_s + 7), *(test_s + 8)) == -25;
	tp += my_strcmp(*(test_s + 7), *(test_s + 11)) == -3;
	tp += my_strcmp(*(test_s + 9), *(test_s + 10)) == -1;
	tp += my_strcmp(*(test_s + 6), *(test_s + 5)) == 2;
	tp += my_strcmp(*(test_s + 8), *(test_s + 7)) == 25;
	tp += my_strcmp("", "") == 0;
	return tp == 7;
}

static
int
test_strlen()
{
	int tp = 0;

	tp += my_strlen("") == 0L;
	tp += my_strlen("\0hey you") == 0L;
	tp += my_strlen("a") == 1L;
	tp += my_strlen("0321590I342") == 11L;
	tp += my_strlen(
		"	 ezae 	 				\neza	0") ==
	      19L;
	tp += my_strlen(" \t") == 2L;
	return tp == 6;
}

static
int
test_memset()
{
	int tp = 0;
	char *str = strdup("Hey, how are you?");

	tp += !strcmp("ooooooooooooooooo", my_memset(str, 'o', 17));
	tp += !strlen(my_memset(str, '\0', 17));
	free(str);
	return tp == 2;
}

static
int
test_memcpy()
{
	int tp = 0;
	char *str = strdup("Hey, how are you?");

	tp += !strcmp("ooooooooooooooooo",
		      my_memcpy(str, "ooooooooooooooooo", 17));
	tp += !strcmp("ooooooooooooooooo",
		      my_memcpy(str, "ddddddddddddddddd", 0));
	tp += !strcmp("ddddooooooooooooo",
		      my_memcpy(str, "ddddddddddddddddd", 4));
	free(str);
	return tp == 3;
}

static
int
test_strchr()
{
	int tp = 0;

	tp += my_strchr(*test_s, 'H') == *test_s;
	tp += my_strchr(*test_s, ' ') == *test_s + 5;
	tp += my_strchr(*test_s, '\n') == *test_s + 13;
	tp += my_strchr(*test_s, 0) == *test_s + 14;
	tp += my_strchr(*test_s, '0') == *test_s + 14;
	return tp == 4;
}

static
int
test_memmove()
{
	int tp = 0;
	char b[20];

	for (int i = 0; i < 20; ++i) { b[i] = 0; }
	tp += str_equal(*test_s, my_memmove(b, *test_s, 15));
	tp += str_equal(*(test_s + 3), my_memmove(b + 5, b, 9));
	tp += str_equal(*(test_s + 1), b);
	tp += str_equal(*(test_s + 2), my_memmove(b + 1, b, 14));
	tp += str_equal(*(test_s + 4), b);
	return tp == 5;
}

static
int
test_rindex()
{
	int tp = 0;

	tp += my_rindex(*test_s, 'H') == *test_s;
	tp += my_rindex(*test_s, 'e') == (*test_s) + 1;
	tp += my_rindex(*test_s, 'z') == 0;
	tp += my_rindex(*test_s, '\n') == (*test_s) + 13;
	return tp == 4;
}

int
main()
{
	void *dl = dlopen("../../libasm.so", RTLD_LOCAL | RTLD_LAZY);

	printf("\n===automatic tests [asm_minilibc]===\n\n");
	if ((my_strlen = dlsym(dl, "strlen")))
		printf("strlen:     [%s]\n", __VALIDATE_TEST__(test_strlen()));
	else
		printf("strlen:     [\033[31mUnknown\033[0m]\n");
	if ((my_strchr = dlsym(dl, "strchr")))
		printf("strchr:     [%s]\n", __VALIDATE_TEST__(test_strchr()));
	else
		printf("strchr:     [\033[31mUnknown\033[0m]\n");
	if ((my_memset = dlsym(dl, "memset")))
		printf("memset:     [%s]\n", __VALIDATE_TEST__(test_memset()));
	else
		printf("memset:     [\033[31mUnknown\033[0m]\n");
	if ((my_memmove = dlsym(dl, "memmove")))
		printf("memmove:    [%s]\n", __VALIDATE_TEST__(test_memmove()));
	else
		printf("memmove:    [\033[31mUnknown\033[0m]\n");
	if ((my_memcpy = dlsym(dl, "memcpy")))
		printf("memcpy:     [%s]\n", __VALIDATE_TEST__(test_memcpy()));
	else
		printf("memcpy:     [\033[31mUnknown\033[0m]\n");
	if ((my_rindex = dlsym(dl, "rindex")))
		printf("rindex:     [%s]\n", __VALIDATE_TEST__(test_rindex()));
	else
		printf("rindex:     [\033[31mUnknown\033[0m]\n");
	if ((my_strcmp = dlsym(dl, "strcmp")))
		printf("strcmp:     [%s]\n", __VALIDATE_TEST__(test_strcmp()));
	else
		printf("strcmp:     [\033[31mUnknown\033[0m]\n");
	if ((my_strcasecmp = dlsym(dl, "strcasecmp")))
		printf("strcasecmp: [%s]\n", __VALIDATE_TEST__(test_strcasecmp()));
	else
		printf("strcasecmp: [\033[31mUnknown\033[0m]\n");
	if ((my_strncmp = dlsym(dl, "strncmp")))
		printf("strncmp:    [%s]\n", __VALIDATE_TEST__(test_strncmp()));
	else
		printf("strncmp:    [\033[31mUnknown\033[0m]\n");
	if ((my_strstr = dlsym(dl, "strstr")))
		printf("strstr:     [%s]\n", __VALIDATE_TEST__(test_strstr()));
	else
		printf("strstr:     [\033[31mUnknown\033[0m]\n");
	if ((my_strpbrk = dlsym(dl, "strpbrk")))
		printf("strpbrk:    [%s]\n", __VALIDATE_TEST__(test_strpbrk()));
	else
		printf("strpbrk:    [\033[31mUnknown\033[0m]\n");
	if ((my_strcspn = dlsym(dl, "strcspn")))
		printf("strcspn:    [%s]\n", __VALIDATE_TEST__(test_strcspn()));
	else
		printf("strcspn:    [\033[31mUnknown\033[0m]\n");
	dlclose(dl);
	return 0;
}
