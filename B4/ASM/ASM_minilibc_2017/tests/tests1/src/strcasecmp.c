/*
** EPITECH PROJECT, 2018
** ASM_minilibc_2017
** File description:
** strcasecmp.c
*/

#include <stdio.h>
#include <string.h>

int tests_strcasecmp()
{
	printf("strcasecmp 1: %i\n", strcasecmp("azerty", "AzeRty"));
	printf("strcasecmp 2: %i\n", strcasecmp("azerty", "azerty"));
	printf("strcasecmp 3: %i\n", strcasecmp("azerty", "azzrty"));
	printf("strcasecmp 4: %i\n", strcasecmp("azerty", "qsdfgh"));
	return (0);
}
