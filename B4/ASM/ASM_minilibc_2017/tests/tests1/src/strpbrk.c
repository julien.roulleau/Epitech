/*
** EPITECH PROJECT, 2018
** ASM_minilibc_2017
** File description:
** strpbrk.c
*/

#include <stdio.h>
#include <string.h>

int tests_strpbrk()
{
	printf("strpbrk 1: %s\n", strpbrk("azertyuiop", "qsdfyhjklm"));
	printf("strpbrk 2: %s\n", strpbrk("azertyuiop", "qsdrfgyhjkolm"));
	return (0);
}
