/*
** EPITECH PROJECT, 2018
** ASM_minilibc_2017
** File description:
** memset.c
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int tests_memset()
{
	int len = 20;
	char *str = malloc(sizeof(char) * len);

	if (!str)
		return (1);
	memset(str, 'a', (size_t) len);
	str[len - 1] = 0;
	printf("memset: %s\n", str);
	return (0);
}
