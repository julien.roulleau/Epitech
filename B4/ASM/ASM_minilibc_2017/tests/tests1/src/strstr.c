/*
** EPITECH PROJECT, 2018
** ASM_minilibc_2017
** File description:
** strstr.c
*/

#include <stdio.h>
#include <string.h>

int tests_strstr()
{
	printf("strstr 1: %s\n", strstr("azertyuiop", "rtyu"));
	printf("strstr 2: %p\n", (void *) strstr("azertyuiop", "fghj"));
	return (0);
}
