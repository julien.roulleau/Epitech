/*
** EPITECH PROJECT, 2018
** ASM_minilibc_2017
** File description:
** rindex.c
*/

#include <string.h>
#include <stdio.h>

int tests_rindex()
{
	char ref[] = "azertyuiopqsdfghjklmwxcvbn\0";

	printf("rindex 1: %s\n", rindex(ref, 'h'));
	printf("rindex 2: %s\n", rindex(ref, ','));
	return (0);
}
