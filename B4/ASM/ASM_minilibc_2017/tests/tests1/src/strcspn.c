/*
** EPITECH PROJECT, 2018
** ASM_minilibc_2017
** File description:
** strcspn.c
*/

#include <stdio.h>
#include <string.h>

int tests_strcspn()
{
	printf("strcspn 1: %zu\n", strcspn("azertyuiop", "qsdfyhjklm"));
	printf("strcspn 2: %zu\n", strcspn("azertyuiop", "qsdrfgyhjkolm"));
	return (0);
}
