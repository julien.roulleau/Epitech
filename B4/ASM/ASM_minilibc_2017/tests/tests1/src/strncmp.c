/*
** EPITECH PROJECT, 2018
** ASM_minilibc_2017
** File description:
** strncmp.c
*/

#include <stdio.h>
#include <string.h>

int tests_strncmp()
{
	printf("strncmp 1: %i\n", strncmp("azertyuiop", "azertyuiop", 10));
	printf("strncmp 2: %i\n", strncmp("azertyuiop", "azertyuiop", 15));
	printf("strncmp 3: %i\n", strncmp("azertyuiop", "azerdyuiop", 10));
	printf("strncmp 4: %i\n", strncmp("azertyuiop", "azertytiop", 6));
	return (0);
}
