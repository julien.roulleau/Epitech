/*
** EPITECH PROJECT, 2018
** ASM_minilibc_2017
** File description:
** strcmp.c
*/

#include <stdio.h>
#include <string.h>

int tests_strcmp()
{
	printf("strcmp 1: %i\n", strcmp("azerty", "AzeRty"));
	printf("strcmp 2: %i\n", strcmp("azerty", "azerty"));
	printf("strcmp 3: %i\n", strcmp("azerty", "azzrty"));
	printf("strcmp 4: %i\n", strcmp("azerty", "qsdfgh"));
	return (0);}
