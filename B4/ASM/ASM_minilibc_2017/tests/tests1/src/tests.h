/*
** EPITECH PROJECT, 2018
** ASM_minilibc_2017
** File description:
** tests.hpp
*/


#ifndef ASM_MINILIBC_2017_TESTS_HPP
#define ASM_MINILIBC_2017_TESTS_HPP

int tests_memcpy();
int tests_memmove();
int tests_memset();
int tests_rindex();
int tests_strcasecmp();
int tests_strchr();
int tests_strcmp();
int tests_strcspn();
int tests_strlen();
int tests_strncmp();
int tests_strpbrk();
int tests_strstr();

#endif //ASM_MINILIBC_2017_TESTS_HPP
