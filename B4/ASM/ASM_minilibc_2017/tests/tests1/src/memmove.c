/*
** EPITECH PROJECT, 2018
** ASM_minilibc_2017
** File description:
** memmove.c
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int tests_memmove()
{
	char ref[] = {"azertyuiopqsdfghjklmwxcvbn\0"};
	int len = 27;
	char *str = malloc(sizeof(char *) * len);

	if (!str)
		return (1);
	memmove(str, ref, (size_t) len);
	printf("memmove ref: %s\n", ref);
	printf("memmove str: %s\n", str);
	return (0);
}
