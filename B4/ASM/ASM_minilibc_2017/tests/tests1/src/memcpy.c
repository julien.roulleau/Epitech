/*
** EPITECH PROJECT, 2018
** ASM_minilibc_2017
** File description:
** memcpy.c
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

int tests_memcpy()
{
	char ref[] = {"azertyuiopqsdfghjklmwxcvbn\0"};
	int len = 27;
	char *str = malloc(sizeof(char *) * len);

	if (!str)
		return (1);
	memcpy(str, ref, (size_t) len);
	printf("memcpy ref: %s\n", ref);
	printf("memcpy str: %s\n", str);
	return (0);
}
