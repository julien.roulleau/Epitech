/*
** EPITECH PROJECT, 2018
** ASM_minilibc_2017
** File description:
** strchr.c
*/

#include <string.h>
#include <stdio.h>

int tests_strchr()
{
	char ref[] = "azertyuiopqsdfghjklmwxcvbn\0";

	printf("strchr 1: %s\n", strchr(ref, 'h'));
	printf("strchr 2: %s\n", strchr(ref, ','));
	return (0);
}
