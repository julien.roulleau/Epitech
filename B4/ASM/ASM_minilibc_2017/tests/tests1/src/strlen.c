/*
** EPITECH PROJECT, 2018
** ASM_minilibc_2017
** File description:
** strlen.c
*/

#include <stdio.h>
#include <string.h>

int tests_strlen()
{
	printf("strlen 1: %zu\n", strlen("azertyuiop"));
	printf("strlen 2: %zu\n", strlen("pdsofkl"));
	printf("strlen 3: %zu\n", strlen("pgoerkpirpsoid"));
	printf("strlen 4: %zu\n", strlen("dsf"));
	return (0);
}
