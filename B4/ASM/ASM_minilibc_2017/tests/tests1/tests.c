/*
** EPITECH PROJECT, 2018
** ASM_minilibc_2017
** File description:
** tests.c
*/

#include "src/tests.h"

int main()
{
	if (tests_memcpy()
	    || tests_memmove()
	    || tests_memset()
	    || tests_rindex()
	    || tests_strcasecmp()
	    || tests_strchr()
	    || tests_strcmp()
	    || tests_strcspn()
	    || tests_strlen()
	    || tests_strncmp()
	    || tests_strpbrk()
	    || tests_strstr())
		return (1);
	return (0);
}
