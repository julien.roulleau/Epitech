/*
** struct.h for 42sh in /home/na/Dropbox/Job/En_Cours/PSU_2016_minishell2/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Mar 23 12:07:23 2017 Roulleau Julien
** Last update Thu Mar 23 12:16:41 2017 Roulleau Julien
*/

#ifndef STRUCT_H
# define STRUCT_H

typedef void 	(*handler_t)(int);
handler_t 	signal (int, handler_t);

typedef struct 		s_flag
{
	char 		*flag;
	char 		**(*flag_pointer)(char **, char **);
}			t_flag;

#endif /* STRUCT_H */
