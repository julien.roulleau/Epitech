/*
** tree.h for tree in /home/na/Dropbox/Job/En_Cours/PSU_2016_minishell2/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Apr  5 20:31:22 2017 Roulleau Julien
** Last update Sun Apr  9 21:04:35 2017 Roulleau Julien
*/

#ifndef TREE_H
# define TREE_H

typedef enum 		e_type
{
	NOT, READ, WRITE, RW, PUT
}			t_type;

typedef struct		s_tree
{
	t_type		type;
	char 		*data;
	struct s_tree	*left;
	struct s_tree	*right;
}			t_tree;

t_tree		*make_tree(char *);
int		free_tree(t_tree *);

#endif /* TREE_H */
