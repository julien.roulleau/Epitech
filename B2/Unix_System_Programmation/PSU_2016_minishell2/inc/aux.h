/*
** aux.h for 42sh in /home/na/Dropbox/Job/En_Cours/PSU_2016_minishell2/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Mar 23 12:16:07 2017 Roulleau Julien
** Last update Sun Apr  9 15:54:57 2017 Roulleau Julien
*/

#ifndef AUX_H
# define AUX_H

int	find_name(char **, char *);
char	*name_to_str(char *);
int	nb_param(char **);
int	find_char(char *, char);
int	check_priority(char *);
int	check_name(char *);
char	**add_tmp(char **);

#endif /* AUX_H */
