/*
** builtins.h for builtins in /home/na/Dropbox/Job/En_Cours/PSU_2016_minishell2/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Mar 23 12:10:59 2017 Roulleau Julien
** Last update Thu Mar 23 12:12:27 2017 Roulleau Julien
*/

#ifndef BUILTINS_H
# define BUILTINS_H

char	**my_env(char **, char **);
char	**my_setenv(char **, char **);
char	**my_unsetenv(char **, char **);

char	**my_cd(char **, char **);

#endif /* BUILTINS_H */
