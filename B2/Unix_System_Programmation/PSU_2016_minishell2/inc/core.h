/*
** core.h for 42sh in /home/na/Dropbox/Job/En_Cours/PSU_2016_minishell2/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Mar 23 12:19:23 2017 Roulleau Julien
** Last update Sun Apr  9 22:57:05 2017 Roulleau Julien
*/

#ifndef CORE_H
# define CORE_H

# include "tree.h"

# define TMP_FILE "/tmp/.mysh.tmp"
# define SIZE 1000000

int g_error;

void		mysh(char **, char ***, t_type, char *);
int		my_exit(char *);
int		my_exec(char **, char **, int, t_type);

#endif /* CORE_H */
