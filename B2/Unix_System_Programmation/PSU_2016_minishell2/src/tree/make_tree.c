/*
** make_tree.c for 42sh in /home/na/Dropbox/Job/En_Cours/PSU_2016_minishell2/src/tree/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Apr  5 20:33:50 2017 Roulleau Julien
** Last update Sun Apr  9 22:54:56 2017 Roulleau Julien
*/

#include <stdlib.h>
#include "tree.h"
#include "aux.h"
#include "src.h"

t_tree		*add_node(char *data, char type)
{
	t_tree	*tree;

	if (!(tree = malloc(sizeof(t_tree))))
		return (0);
	tree->data = data;
	tree->type = type;
	tree->left = 0;
	tree->right = 0;
	return (tree);
}

t_tree		*set_semicolon(char *buffer)
{
	t_tree	*tree;
	t_tree	*tmp;
	char	**tab;
	int	i;

	i = -1;
	tab = str_to_wordtab(buffer, ';');
	while (tab[++i]);
	if (!(tree = add_node(tab[--i], NOT)))
		return (0);
	tmp = tree;
	while (--i >= 0)
	{
		if (!(tree->left = add_node(tab[i], NOT)))
			return (0);
		tree = tree->left;
	}
	tree = tmp;
	return (tree);
}

t_tree		*set_pipe(t_tree *tree)
{
	t_tree	*tmp;
	char	**tab;
	int	i;

	if (tree->left)
		set_pipe(tree->left);
	if (tree->right)
		set_pipe(tree->right);
	if (!find_char(tree->data, '|'))
		return (tree);
	tab = str_to_wordtab(tree->data, '|');
	if (!(i = nb_param(tab)) || tab[1][0] == 0)
		return (0);
	tree->type = tree->type == WRITE ? RW : READ;
	tree->data = tab[i];
	tmp = tree;
	while (--i >= 0)
	{
		if (!(tmp->right = add_node(tab[i], RW)))
			return (0);
		tmp = tmp->right;
	}
	tmp->type = WRITE;
	return (tree);
}

t_tree		*set_redirect(t_tree *tree)
{
	char	**tab;

	if (tree->left)
		set_redirect(tree->left);
	if (!find_char(tree->data, '>'))
		return (tree);
	if (!check_priority(tree->data))
	{
		PRINT("Ambiguous output redirect.\n");
		return (0);
	}
	if (!check_name(tree->data))
	{
		PRINT("Missing name for redirect.\n");
		return (0);
	}
	tab = str_to_wordtab(tree->data, '>');
	tree->type = PUT;
	tree->data = tab[1];
	if (!(tree->right = add_node(tab[0], WRITE)))
		return (0);
	return (tree);
}

t_tree		*make_tree(char *buffer)
{
	t_tree	*tree;
	int	i;

	i = -1;
	while (buffer[++i] && buffer[i + 1])
		if (buffer[i] == '<' ||
			(buffer[i] == '<' && buffer[i + 1] == '<') ||
			(buffer[i] == '>' && buffer[i + 1] == '>'))
		{
			PRINT("Bad parameter.\n");
			return (0);
		}
	if (!(tree = set_semicolon(buffer)))
	{
		PRINT("Error.\n");
		return (0);
	}
	if (!(tree = set_redirect(tree)))
		return (0);
	if (!(tree = set_pipe(tree)))
	{
		PRINT("Invalid null command.\n");
		return (0);
	}
	return (tree);
}
