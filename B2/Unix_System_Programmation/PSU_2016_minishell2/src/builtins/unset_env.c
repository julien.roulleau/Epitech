/*
** unset_env.c for 42sh in /home/na/Dropbox/Job/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Apr  6 15:00:44 2017 Roulleau Julien
** Last update Fri Apr  7 12:04:05 2017 Roulleau Julien
*/

#include "core.h"
#include "aux.h"
#include "src.h"

static char	**lessmalloc_env(char **env, int pos)
{
	char	**new_env;
	int	i;
	int	n;

	n = -1;
	if (!(new_env = malloc(sizeof(char *) * (nb_param(env) + 1))))
		return (0);
	i = -1;
	while (env[++i])
		if (i != pos)
			new_env[++n] = env[i];
	new_env[++n] = 0;
	return (new_env);
}

char		**my_unsetenv(char **param, char **env)
{
	int	pos;

	if (nb_param(param) == 0 && (g_error = 1))
		my_putstr("unsetenv: Too few arguments.\n");
	else if ((pos = find_name(env, param[1])) != -1)
		env = lessmalloc_env(env, pos);
	return (env);
}
