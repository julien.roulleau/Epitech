/*
** set_env.c for 42sh in /home/na/Dropbox/Job/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Apr  6 15:00:17 2017 Roulleau Julien
** Last update Sun Apr  9 16:06:01 2017 Roulleau Julien
*/

#include "core.h"
#include "aux.h"
#include "src.h"

char		**my_env(char **param, char **env)
{
	int 	i;
	int 	n;

	i = -1;
	n = 1;
	if (param[1])
	{
		if (my_strcmp(param[1], "-i") && param[2])
			{
				while (param[++n])
					param[++i] = param[n];
				param[++i] = 0;
				env[0] = 0;
				my_exec(param, env, 1, 0);
			}
		else if (!my_strcmp(param[1], "-i") && (g_error = 1))
			my_putstr("Invalid flag\n");
		return (env);
	}
	while (env[++i])
	{
		my_putstr(env[i]);
		my_putstr("\n");
	}
	return (env);
}

static char	**addmalloc_env(char **env, char *name, char *value)
{
	char	**new_env;
	int	i;

	if (!(new_env = malloc(sizeof(char *) * (nb_param(env) + 3))))
		return (0);
	i = -1;
	while (env[++i])
		new_env[i] = env[i];
	new_env[i] = merge_str(merge_str(name, "="), value);
	new_env[++i] = 0;
	return (new_env);
}

char		**my_setenv(char **param, char **env)
{
	int	pos;
	int	nb;

	if (!(nb = nb_param(param)) && (g_error = 1))
		my_env(param, env);
	else if (!is_alpha_num(param[1]) &&
		PRINT("setenv: Variable name must contain") &&
		PRINT("alphanumeric characters.\n") && (g_error = 1));
	else if (!IS_ALPHA(param[1][0]) && (g_error = 1) &&
		PRINT("setenv: Variable name must begin with a letter.\n"));
	else if (!(pos = find_name(env, param[1])))
	{
		if (nb == 1);
		else
			env[pos] = merge_str(
					merge_str(param[1], "="), param[2]);
	}
	else
	{
		if (nb == 1)
			env = addmalloc_env(env, param[1], "\0");
		else
			env = addmalloc_env(env, param[1], param[2]);
	}
	return (env);
}
