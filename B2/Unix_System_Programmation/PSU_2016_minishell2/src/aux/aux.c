/*
** aux.c for 42sh in /home/na/Dropbox/Job/En_Cours/PSU_2016_minishell2/src/aux/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sat Apr  8 15:29:59 2017 Roulleau Julien
** Last update Sun Apr  9 21:22:01 2017 Roulleau Julien
*/

#include "aux.h"
#include "core.h"
#include "src.h"

int		find_char(char *str, char find)
{
	int 	i;

	i = -1;
	while (str[++i] && str[i] != find);
	if (!str[i])
		return (0);
	return (1);
}

int		check_priority(char *str)
{
	int	i;

	i = -1;
	while (str[++i] && str[i] != '>');
	while (str[++i])
		if (str[i] == '|' || str[i] == '>')
			return (0);
	return (1);
}

int		check_name(char *str)
{
	int	i;

	i = -1;
	while (str[++i] && str[i] != '>');
	if (str[i] == '>')
	{
		if (!str[++i] || str[i] != ' ')
			return (0);
		if (!str[++i] || !IS_ALPHA_NUM(str[i]))
			return (0);
	}
	return (1);
}

char		**add_tmp(char **param)
{
	char	**new;
	int	i;

	if (!(new = malloc(sizeof(char *) * (nb_param(param) + 3))))
		return (param);
	i = -1;
	while (param[++i])
		new[i] = param[i];
	if (access(param[1], F_OK))
		new[i] = TMP_FILE;
	else
		new[i] = 0;
	new[++i] = 0;
	free(param);
	return (new);
}
