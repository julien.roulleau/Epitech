/*
** main.c for 42sh in /home/infocraft/Job/En_Cours/PSU_2016_my_exec/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Jan  5 11:01:15 2017 Roulleau Julien
** Last update Sun Apr  9 22:46:54 2017 Roulleau Julien
*/

#include "core.h"
#include "tree.h"
#include "src.h"

static int	print_prompt()
{
	char	*pwd;

	if (!(pwd = malloc(sizeof(char) * (1000))))
		return (0);
	pwd = getcwd(pwd, 999);
	my_putstr(pwd);
	my_putstr(" § ");
	return (1);
}

static char	*epur(char *data)
{
	int 	i;

	i = -1;
	while (data[++i] == ' ');
	data += i;
	while (data[++i]);
	while (data[--i] == ' ')
		data[i] = 0;
	return (data);
}

int		exec_node(char *data, char ***env, t_type type)
{
	char	**param;

	data = epur(data);
	param = str_to_wordtab(data, ' ');
	if (my_strcmp(param[0], ""));
	else if (my_strcmp(data, "echo $?"))
	{
		my_putnbr(g_error);
		PRINT("\n");
		g_error = 0;
	}
	else if (my_strcmp(param[0], "exit"))
	{
		PRINT("exit\n");
		g_error = 0;
		return (0);
	}
	else
		mysh(param, env, type, data);
	return (1);
}

int		exec_tree(t_tree *tree, char ***env)
{
	if (!tree)
		return (0);
	if (tree->left)
		exec_tree(tree->left, env);
	if (tree->right)
		exec_tree(tree->right, env);
	if (!exec_node(tree->data, env, tree->type))
		return (0);
	if (tree->left)
		free(tree->left);
	if (tree->right)
		free(tree->right);
	return (1);
}

int 		main(int argc, char **argv, char **env)
{
	t_tree	*tree;
	char	*buffer;

	(void) argc;
	(void) argv;
	g_error = 0;
	while (print_prompt() && (buffer = get_next_line(0)))
	{
		buffer = purge_str(buffer, '\t');
		buffer = epur_str(buffer, ' ');
		if (!(tree = make_tree(buffer)));
		else if (!(exec_tree(tree, &env)))
			return (g_error);
		free(tree);
		free(buffer);
	}
	PRINT("exit\n");
	return (g_error);
}
