/*
** mysh.c for 42sh in /home/infocraft/Job/En_Cours/PSU_2016_minishell1/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Jan 16 18:58:27 2017 Roulleau Julien
** Last update Sun Apr  9 22:45:47 2017 Roulleau Julien
*/

#include "builtins.h"
#include "struct.h"
#include "core.h"
#include "src.h"

t_flag		g_flag[] =
{
	{"cd", &my_cd},
	{"env", &my_env},
	{"setenv", &my_setenv},
	{"unsetenv", &my_unsetenv},
	{NULL, NULL}
};

static int	set_fd(t_type type, char *name)
{
	int 	fd;

	if (type == NOT || type == READ)
		return (1);
	else if (type == RW)
		return (0);
	else if (type == WRITE)
	{
		if ((fd = open(TMP_FILE, O_CREAT | O_WRONLY | O_TRUNC,
				S_IRWXU | S_IRWXG | S_IRWXO)) == -1)
			return (1);
		return (fd);
	}
	else if (type == PUT)
	{
		if ((fd = open(name, O_CREAT | O_WRONLY | O_TRUNC,
				S_IRUSR | S_IWUSR |
				S_IRGRP | S_IWGRP | S_IROTH)) == -1)
			return (1);
		return (fd);
	}
	return (1);
}

static int 	put(int fd)
{
	char	buffer[SIZE];
	int	size;
	int	fdd;

	if ((fdd = open(TMP_FILE, O_RDONLY)) == -1)
		return (1);
	size = read(fdd, buffer, SIZE - 1);
	buffer[size] = 0;
	my_putstr_fd(fd, buffer);
	return (1);
}

void		mysh(char **param, char ***env, t_type type, char *name)
{
	int	f;
	int 	last;
	int	fd;

	f = -1;
	last = 0;
	while (g_flag[++f].flag != NULL &&
			!my_strcmp(param[0], g_flag[f].flag));
	if (g_flag[f].flag == NULL)
	{
		fd = set_fd(type, name);
		if (type == READ || type == RW)
			last = 1;
		if (type == PUT && put(fd));
		else if ((my_exec(param, *env, fd, last)) == -1)
		{
			my_putstr(param[0]);
			PRINT(": Command not found.\n");
			g_error = 1;
		}
		if (fd != 1 && fd != 0)
			close(fd);
	}
	else
		*env = g_flag[f].flag_pointer(param, *env);
}
