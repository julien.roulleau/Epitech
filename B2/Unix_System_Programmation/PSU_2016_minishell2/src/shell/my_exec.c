/*
** my_exec.c for 42sh in /home/infocraft/Job/En_Cours/PSU_2016_my_exec/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Jan  5 11:04:42 2017 Roulleau Julien
** Last update Tue Apr 11 18:59:22 2017 Roulleau Julien
*/

#include <sys/wait.h>
#include "core.h"
#include "aux.h"
#include "src.h"

static char 	*set_param(char **param, char **env)
{
	char	**path;
	int 	i;

	i = 0;
	if ((param[0][0] == '.' && param[0][1] == '/') ||
		param[0][0] == '/')
	{
		if (access(param[0], F_OK))
			return (0);
	}
	else
	{
		path = str_to_wordtab(
			env[find_name(env, "PATH")] + 5, ':');
		while (path[++i] &&
			access(merge_str(merge_str(path[i], "/"),
						param[0]), F_OK) &&
			access(merge_str(merge_str(path[i], "/"),
						param[0]), X_OK));
		if (!path[i])
			return (0);
		param[0] = merge_str(merge_str(path[i], "/"), param[0]);
	}
	return (param[0]);
}

static int	rw(int	pipefd[0], int fd)
{
	char	buffer[SIZE];
	int	size;

	if (!fd)
	{
		if ((fd = open(TMP_FILE, O_CREAT | O_WRONLY | O_TRUNC,
				S_IRWXU | S_IRWXG | S_IRWXO)) == -1)
			fd = 1;
	}
	size = read(pipefd[0], buffer, SIZE - 1);
	buffer[size] = 0;
	my_putstr_fd(fd, buffer);
	return (0);
}

static int	father(int pid, int pipefd[2], int fd)
{
	int	status;
	int	wait;

	close(pipefd[1]);
	signal(SIGINT, SIG_IGN);
	wait = waitpid(pid, &status, WUNTRACED | WCONTINUED);
	if (wait == -1)
		return (0);
	g_error = status;
	if (WIFEXITED(status) && WEXITSTATUS(status) == 0);
	else if (WIFSIGNALED(status) && WTERMSIG(status) == 11)
		PRINT("Segmentation fault (core dumped)\n");
	else if (WCOREDUMP(status))
		PRINT("Core dumped\n");
	signal(SIGINT, 0);
	rw(pipefd, fd);
	return (1);
}

static int	child(char **param, char **env, int pipefd[2], t_type type)
{
	close(pipefd[0]);
	dup2(pipefd[1], 1);
	if (type == READ || type == RW)
		param = add_tmp(param);
	if (execve(param[0], param, env) == -1)
		exit (1);
	return (1);
}

int		my_exec(char **param, char **env, int fd, t_type type)
{
	char	*tmp;
	int	pipefd[2];
	int	pid;

	if (pipe(pipefd) == -1)
		return (84);
	if (env)
	{
		if (!(tmp = set_param(param, env)))
			return (-1);
		param[0] = tmp;
	}
	pid = fork();
	if (pid == 0 && !child(param, env, pipefd, type))
		return (-1);
	else if (pid > 0 && !father(pid, pipefd, fd))
		return (84);
	else
		return (84);
	return (0);
}
