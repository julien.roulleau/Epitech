/*
** option_func.c for tetris in /home/na/Dropbox/Job/En_Cours/PSU_2016_tetris/option/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Feb 22 15:22:04 2017 Roulleau Julien
** Last update Mon Mar 13 15:24:33 2017 Roulleau Julien
*/

#include "struct.h"
#include "src.h"

int 	option_level(t_option *option, char *param)
{
	if (!param || !is_num(param))
		return (0);
	option->level = my_getnbr(param);
	return (1);
}

int 	option_key_left(t_option *option, char *param)
{
	if (!param || !is_alpha(param))
		return (0);
	option->key_left = my_strdup(param);
	return (1);
}

int 	option_key_right(t_option *option, char *param)
{
	if (!param || !is_alpha(param))
		return (0);
	option->key_right = my_strdup(param);
	return (1);
}

int 	option_key_turn(t_option *option, char *param)
{
	if (!param || !is_alpha(param))
		return (0);
	option->key_turn = my_strdup(param);
	return (1);
}

int 	option_key_drop(t_option *option, char *param)
{
	if (!param || !is_alpha(param))
		return (0);
	option->key_drop = my_strdup(param);
	return (1);
}
