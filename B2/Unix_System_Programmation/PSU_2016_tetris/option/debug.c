/*
** debug.c for tetris in /home/na/Dropbox/Job/En_Cours/PSU_2016_tetris/option/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Mar 13 23:34:40 2017 Roulleau Julien
** Last update Sun Mar 19 15:11:33 2017 Fesquet David
*/

#include <unistd.h>
#include "struct.h"
#include "src.h"
#include "read.h"

int	debug_tetrimino(t_tet *);

static void 	test_char(char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		if (str[i] == ' ')
			PRINT("(space)");
		else if (str[i] == 27)
			PRINT("^E");
		else
			write(1, &(str[i]), 1);
		i++;
	}
}

void		debug_option(t_option option)
{
	PRINT("KEY Left : ");
	test_char(option.key_left);
	PRINT("\nKEY Right : ");
	test_char(option.key_right);
	PRINT("\nKEY Turn : ");
	test_char(option.key_turn);
	PRINT("\nKEY Drop : ");
	test_char(option.key_drop);
	PRINT("\nKEY Quit : ");
	test_char(option.key_quit);
	PRINT("\nKEY Pause : ");
	test_char(option.key_pause);
	PRINT("\nNext : ");
	if (option.without_next == 1)
		PRINT("Yes");
	else
		PRINT("NO");
	PRINT("\nLevel : ");
	my_putnbr(option.level);
	PRINT("\nSize : ");
	my_putnbr(option.map_size.x);
	PRINT("*");
	my_putnbr(option.map_size.y);
	PRINT("\n");
}

void		print_debug(t_tet tet, t_option option)
{
	(void)option;
	read_mode_debug(0);
	debug_option(option);
	debug_tetrimino(&tet);
	PRINT("Press any key to start Tetris\n");
	scan_key_debug();
	read_mode_debug(1);
}
