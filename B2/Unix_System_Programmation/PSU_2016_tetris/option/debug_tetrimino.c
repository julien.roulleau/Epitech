/*
** debug_tetrimino.c for tetris in /PSU_2016_tetris/option/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Thu Mar 16 21:45:51 2017 Fesquet David
** Last update Fri Mar 17 09:48:23 2017 Roulleau Julien
*/

#include "struct.h"
#include "src.h"

static int	print_obj(int **tab, int sx, int sy)
{
	int	x;
	int	y;
	int	x2;
	int	rep;

	y = -1;
	while (++y < sy)
	{
		x = -1;
		while (++x < sx)
		{
			x2 = x - 1;
			rep = 0;
			while (++x2 < sx)
				if (tab[y][x2] == 1)
					rep = 1;
			if (rep == 0)
				break;
			if (tab[y][x] == 1)
				write(1, "*", 1);
			else (write(1, " ", 1));
		}
		write(1, "\n", 1);
	}
	return (0);
}

static int	print_tetrimino(t_objd *obj)
{
	write(1, "Tetriminos :  Name ", 19);
	my_putstr(obj->name);
	if (obj->error == 1 && (write(1, " :  Error\n", 10)))
	return (1);
	my_putstr(" :  Size ");
	my_putnbr(obj->size.x);
	my_putstr("*");
	my_putnbr(obj->size.y);
	my_putstr(" :  Color ");
	my_putnbr(obj->color);
	my_putstr(" :\n");
	print_obj(obj->obj, obj->size.x, obj->size.y);
	return (0);
}

int	debug_tetrimino(t_tet *tet)
{
	int	i;

	my_putstr("Tetriminos :  ");
	my_putnbr(tet->d_size);
	my_putstr("\n");
	i = 0;
	while (i < tet->d_size)
	{
		print_tetrimino(tet->objd[i]);
		i++;
	}
	return (0);
}
