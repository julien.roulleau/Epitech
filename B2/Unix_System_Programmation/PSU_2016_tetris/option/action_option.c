/*
** action_option.c for tetris in /home/na/Dropbox/Job/En_Cours/PSU_2016_tetris/option/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Mar 13 14:06:23 2017 Roulleau Julien
** Last update Mon Mar 13 23:28:16 2017 Roulleau Julien
*/

#include "struct.h"
#include "option.h"
#include "src.h"

int		check_option(t_option option)
{
	if (my_strcmp(option.key_left, option.key_right) ||
		my_strcmp(option.key_left, option.key_turn) ||
		my_strcmp(option.key_left, option.key_drop) ||
		my_strcmp(option.key_left, option.key_quit) ||
		my_strcmp(option.key_left, option.key_pause) ||
		my_strcmp(option.key_right, option.key_turn) ||
		my_strcmp(option.key_right, option.key_drop) ||
		my_strcmp(option.key_right, option.key_quit) ||
		my_strcmp(option.key_right, option.key_pause) ||
		my_strcmp(option.key_turn, option.key_drop) ||
		my_strcmp(option.key_turn, option.key_quit) ||
		my_strcmp(option.key_turn, option.key_pause) ||
		my_strcmp(option.key_drop, option.key_quit) ||
		my_strcmp(option.key_drop, option.key_pause) ||
		my_strcmp(option.key_quit, option.key_pause))
		option_help(&option, NULL);
	{
		return (0);
	}
	return (1);
}
