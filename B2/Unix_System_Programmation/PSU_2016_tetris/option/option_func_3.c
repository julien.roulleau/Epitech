/*
** option_func.c for tetris in /home/na/Dropbox/Job/En_Cours/PSU_2016_tetris/option/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Feb 22 15:22:04 2017 Roulleau Julien
** Last update Fri Mar 17 13:43:26 2017 Roulleau Julien
*/

#include "struct.h"
#include "src.h"

int 	option_help(t_option *option, char *param)
{
	(void) option;
	(void) param;
	if (param)
		return (0);
	PRINT("Options:\n");
	PRINT("--help			Display this help\n");
	PRINT("-l --level={num}	Start Tetris at level num (def:  1)\n");
	PRINT("-kl --key-left={K}	");
	PRINT("Move the tetrimino LEFT using the K key (def:  left arrow)\n");
	PRINT("-kr --key-right={K}	");
	PRINT("Move the tetrimino RIGHT using the K key (def:  right arrow)\n");
	PRINT("-kt --key-turn={K}	");
	PRINT("TURN the tetrimino clockwise 90d using the K key ");
	PRINT("(def:  top arrow)\n");
	PRINT("-kd --key-drop={K}	");
	PRINT("DROP the tetrimino using the K key (def:  down arrow)\n");
	PRINT("-kq --key-quit={K}	");
	PRINT("QUIT the game using the K key (def:  'q' key)\n");
	PRINT("-kp --key-pause={K}	");
	PRINT("PAUSE/RESTART the game using the K key (def:  space bar)\n");
	PRINT("--map-size={row,col}	");
	PRINT("Set the numbers of rows and columns of the map (def:  20,10)\n");
	PRINT("-w --without-next	Hide next tetrimino (def:  false)\n");
	PRINT("-d --debug		Debug mode (def:  false)\n");
	return (1);
}
