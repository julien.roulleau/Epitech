/*
** option_func.c for tetris in /home/na/Dropbox/Job/En_Cours/PSU_2016_tetris/option/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Feb 22 15:22:04 2017 Roulleau Julien
** Last update Fri Mar 17 10:19:03 2017 Roulleau Julien
*/

#include "struct.h"
#include "src.h"
#include "option.h"

int 	option_key_quit(t_option *option, char *param)
{
	if (!param || !is_alpha(param))
		return (0);
	option->key_quit = my_strdup(param);
	return (1);
}

int 	option_key_pause(t_option *option, char *param)
{
	if (!param || !is_alpha(param))
		return (0);
	option->key_pause = my_strdup(param);
	return (1);
}

int 		option_map_size(t_option *option, char *param)
{
	char	**map_size;

	if (!param)
		return (0);
	map_size = str_to_wordtab(param, ',');
	if (!is_num(map_size[0]) || !is_num(map_size[1]))
		return (0);
	option->map_size.x = my_getnbr(map_size[0]);
	option->map_size.y = my_getnbr(map_size[1]);
	return (1);
}

int 	option_without_next(t_option *option, char *param)
{
	(void) param;
	option->without_next = 1;
	return (1);
}

int 	option_debug(t_option *option, char *param)
{
	(void) param;
	print_debug(option->tet, *option);
	option->debug = 1;
	return (1);
}
