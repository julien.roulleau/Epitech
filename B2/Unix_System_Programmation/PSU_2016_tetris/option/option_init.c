/*
** option_init.c for tetris in /PSU_2016_tetris/option/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Sat Mar 18 20:18:47 2017 Fesquet David
** Last update Sat Mar 18 22:20:54 2017 Fesquet David
*/

#include <curses.h>
#include <stdlib.h>
#include <term.h>
#include <unistd.h>
#include "src.h"

char	*find_term(char	**env, char *str)
{
	int	i;

	i = 0;
	if (env[0] == NULL)
		return (NULL);
	while (env[0][i] && str[i] && env[0][i] == str[i])
		i++;
	if (str[i] == 0 && env[0][i] == '=')
		return (env[0] + (i + 1));
	else
		return (find_term(env + 1, str));

}

char	**init_with_term(char **env)
{
	char	*term;
	char	**opt;
	int	err;

	term = find_term(env, "TERM");
	if (term == NULL)
		return (0);
	setupterm(term, 1, &err);
	opt = malloc(sizeof(char*) * 4);
	if (!opt || !err)
		return (NULL);
	putp(tigetstr("smkx"));
	opt[0] = tigetstr("kcub1");
	opt[1] = tigetstr("kcuf1");
	opt[2] = tigetstr("kcuu1");
	opt[3] = tigetstr("kcud1");
	return (opt);
}
