/*
** option.c for tetris in /home/na/Dropbox/Job/En_Cours/PSU_2016_tetris/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Feb 21 16:20:15 2017 Roulleau Julien
** Last update Sun Mar 19 16:40:21 2017 Roulleau Julien
*/

#include <curses.h>
#include <stdlib.h>
#include "option.h"
#include "src.h"

#define STW str_to_wordtab

t_pttab		g_option[] =
{
	{"-l", "--level", &option_level},
	{"-kl", "--key_left", &option_key_left},
	{"-kr", "--key_right", &option_key_right},
	{"-kt", "--key_turn", &option_key_turn},
	{"-kd", "--key_drop", &option_key_drop},
	{"-kq", "--key_quit", &option_key_quit},
	{"-kp", "--key_pause", &option_key_pause},
	{"-w", "--without_next", &option_without_next},
	{"-d", "--debug", &option_debug},
	{0, "--help", &option_help},
	{0, "--map-size", &option_map_size},
	{0, 0, 0}
};

t_option		init_option(char **env)
{
	t_option	opt;
	char		**option;

	option = init_with_term(env);
	if (!option)
	{
		opt.level = -1;
		return (opt);
	}
	opt.level = 1;
	opt.key_left = option[0];
	opt.key_right = option[1];
	opt.key_turn = option[2];
	opt.key_drop = option[3];
	opt.key_quit = my_strdup("q");
	opt.key_pause = my_strdup(" ");
	opt.map_size.x = 10;
	opt.map_size.y = 20;
	opt.without_next = 0;
	opt.debug = 0;
	return (opt);
}

int		fd_mini_f(t_option *option, char *param, char *value)
{
	int	f;

	f = -1;
	while (g_option[++f].flag_mini &&
		!my_strcmp(g_option[f].flag_mini, param));
	if (g_option[f].flag_mini == 0)
		return (0);
	else
		if (!g_option[f].flag_pointer(option, value))
			return (0);
	return (1);
}

int		find_full_flag(t_option *option, char *param, char *value)
{
	int	f;

	f = -1;
	while (g_option[++f].flag_full &&
		!my_strcmp(g_option[f].flag_full, param));
	if (g_option[f].flag_full == 0)
		return (0);
	else
		if (!g_option[f].flag_pointer(option, value))
			return (0);
	return (1);
}

int		set_option(t_option *o, char **a)
{
	char	**opt;
	int	i;

	i = 0;
	while (a[++i])
	{
		if (a[i][0] == '-' && a[i][1] == '-' && (opt = STW(a[i], '=')))
		{
			if (!find_full_flag(o, opt[0], opt[1]))
				return (0);
		}
		else if (a[i][0] == '-' && a[i][1] != '-')
		{
			if (my_strcmp(a[i], "-d") || my_strcmp(a[i], "-w"))
			{
				if (!fd_mini_f(o, a[i], a[i]))
					return (0);
			}
			else if (!a[++i] || !fd_mini_f(o, a[i - 1], a[i]))
					return (0);
		}
		else
			return (0);
	}
	return (1);
}
