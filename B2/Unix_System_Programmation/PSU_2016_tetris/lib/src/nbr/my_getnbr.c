/*
** get_nbr.c for src in /home/infocraft/Job/Lib/my_printf/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun Jan  8 20:39:31 2017 Roulleau Julien
** Last update Thu Feb 23 13:56:52 2017 Roulleau Julien
*/

# define IS_NUM(c) (c >= '0' && c <= '9')

static int	my_getnbr_count(char *str, int nb)
{
	if (*str == '+')
		return (my_getnbr_count(++str, nb));
	if (*str == '-')
		return (-my_getnbr_count(++str, nb));
	if (*str == '\0' || !IS_NUM(*str))
		return (nb);
	nb = (nb * 10) + (*str - '0');
	return (my_getnbr_count(++str, nb));
}

int		my_getnbr(char *str)
{
	return (my_getnbr_count(str, 0));
}
