/*
** check_block.c for tetris in /home/david/PSU_2016_tetris/field/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Fri Mar 10 14:50:53 2017 Fesquet David
** Last update Sun Mar 19 21:25:58 2017 Fesquet David
*/

#include "struct.h"
#include <unistd.h>
#include <stdlib.h>

int	check_block(t_field *field)
{
	int	x;
	int	y;

	y = 0;
	while (y < field->sy)
	{
		x = 0;
		while (x < field->sx)
		{
			if (field->field[y][x].move == 2 &&
			(y == field->sy - 1 ||
				field->field[y + 1][x].move == 1))
				return (1);
			x++;
		}
		y++;
	}
	return (0);
}
