/*
** translate_block.c for tetris in /home/david/PSU_2016_tetris/field/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Tue Mar 14 11:32:36 2017 Fesquet David
** Last update Sun Mar 19 20:35:38 2017 Fesquet David
*/

#include "struct.h"

static int	check_left_col(t_field *field)
{
	int	x;
	int	y;

	y = 0;
	while (y < field->sy)
	{
		x = 0;
		while (x < field->sx)
		{
			if (field->field[y][x].move == 2 &&
				(x == 0 || field->field[y][x - 1].move == 1))
				return (1);
			x++;
		}
		y++;
	}
	return (0);
}

static int	check_right_col(t_field *field)
{
	int	x;
	int	y;

	y = 0;
	while (y < field->sy)
	{
		x = 0;
		while (x < field->sx)
		{
			if (field->field[y][x].move == 2 &&
				(x == field->sx - 1 ||
					field->field[y][x + 1].move == 1))
				return (1);
			x++;
		}
		y++;
	}
	return (0);
}

int	right_block(t_field *field, t_pos *pos)
{
	int	x;
	int	y;

	y = 0;
	if (check_right_col(field))
		return (1);
	while (y < field->sy)
	{
		x = field->sx - 1;
		while (x >= 0)
		{
			if (field->field[y][x].move == 2)
			{
				field->field[y][x].move = 0;
				field->field[y][x + 1].move = 2;
				field->field[y][x + 1].color =
					field->field[y][x].color;
				field->field[y][x].color = 0;
			}
			x--;
		}
		y++;
	}
	pos->x += 1;
	return (0);
}

int	left_block(t_field *field, t_pos *pos)
{
	int	x;
	int	y;

	y = 0;
	if (check_left_col(field))
		return (1);
	while (y < field->sy)
	{
		x = 1;
		while (x < field->sx)
		{
			if (field->field[y][x].move == 2)
			{
				field->field[y][x].move = 0;
				field->field[y][x - 1].move = 2;
				field->field[y][x - 1].color =
					field->field[y][x].color;
				field->field[y][x].color = 0;
			}
			x++;
		}
		y++;
	}
	pos->x -= 1;
	return (0);
}
