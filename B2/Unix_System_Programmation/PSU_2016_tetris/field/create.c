/*
** create.c for tetris in /home/david/project/en_cour/PSU_2016_tetris/field/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Thu Mar  9 10:43:30 2017 Fesquet David
** Last update Thu Mar  9 10:54:22 2017 Fesquet David
*/

#include "struct.h"
#include <stdlib.h>
#include <unistd.h>

t_field		field_create(int sy, int sx)
{
	t_field	field;
	int	x;
	int	y;

	y = 0;
	if (!(field.field = malloc(sizeof(t_block*) * sy)))
		return (field);
	while (y < sy)
	{
		x = 0;
		if (!(field.field[y] = malloc(sizeof(t_block) * sx)))
			return (field);
		while (x < sx)
		{
			field.field[y][x].color = 0;
			field.field[y][x].move = 0;
			x++;
		}
		y++;
	}
	field.sx = sx;
	field.sy = sy;
	return (field);
}
