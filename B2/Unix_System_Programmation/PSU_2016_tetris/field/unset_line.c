/*
** unset_line.c for tetris in /home/david/PSU_2016_tetris/field/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Thu Mar  9 10:50:08 2017 Fesquet David
** Last update Sun Mar 19 20:26:06 2017 Fesquet David
*/

#include "struct.h"
#include <unistd.h>
#include <stdlib.h>

static int	check_line(t_field *field, int index)
{
	int	i;

	i = 0;
	while (i < field->sx)
	{
		if (field->field[index][i].move == 0)
			return (0);
		i++;
	}
	return (1);
}

static int	delete_line(t_field *field, int index)
{
	int	i;
	t_block	*line;

	i = index;
	free(field->field[index]);
	while (i > 0)
	{
		field->field[i] = field->field[i - 1];
		i--;
	}
	if (!(line = malloc(sizeof(t_block) * field->sx)))
		return (1);
	while (i < field->sx)
	{
		line[i].move = 0;
		line[i].color = 0;
		i++;
	}
	field->field[0] = line;
	return (0);
}

int		field_clear(t_field *field)
{
	int	i;
	int	nb;

	i = nb = 0;
	while (i < field->sy)
	{
		if (check_line(field, i))
		{
			delete_line(field, i);
			nb++;
			i--;
		}
		i++;
	}
	return (nb);
}
