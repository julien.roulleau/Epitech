/*
** set_block_in_field.c for tetris in /home/david/PSU_2016_tetris/field/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Fri Mar 10 15:46:42 2017 Fesquet David
** Last update Sun Mar 19 20:18:16 2017 Fesquet David
*/

#include "struct.h"
#include <unistd.h>
#include <stdlib.h>

int	set_block(t_obj *block, t_field *field, t_pos *pos)
{
	int	x;
	int	y;
	int	x2;

	y = -1;
	while (++y < block->size.y)
	{
		x = -1;
		while (++x < block->size.x)
		{
			x2 = (field->sx / 2) - (block->size.x / 2) + x;
			if (field->field[y][x2].move == 1)
				return (1);
			if (block->obj[y][x] == 1)
			{
				field->field[y][x2].move = 2;
				field->field[y][x2].color = block->color;
			}
		}
	}
	pos->y = block->size.y / 2;
	pos->r = 0;
	pos->x = field->sx / 2;
	return (0);
}
