/*
** mv_in_fix.h for tetris in /PSU_2016_tetris/field/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Sun Mar 19 17:21:17 2017 Fesquet David
** Last update Sun Mar 19 18:16:15 2017 Fesquet David
*/

#include "struct.h"

int	mv_in_fix(t_field *field)
{
	int	x;
	int	y;

	y = -1;
	while (++y < field->sy)
	{
		x = -1;
		while (++x < field->sx)
			if (field->field[y][x].move == 2)
				field->field[y][x].move = 1;
	}
	return (0);
}
