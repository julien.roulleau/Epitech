/*
** rotate.c for  in /PSU_2016_tetris/field/rotate/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Sun Mar 19 17:54:39 2017 Fesquet David
** Last update Sun Mar 19 20:31:37 2017 Fesquet David
*/

#include "struct.h"
#include "field.h"
#include <stdio.h>

static int	clear_field(t_field *field)
{
	int	x;
	int	y;

	y = -1;
	while (++y < field->sy)
	{
		x = -1;
		while (++x < field->sx)
		{
			if (field->field[y][x].move == 2)
			{
				field->field[y][x].move = 0;
				field->field[y][x].color = 0;
			}
		}
	}
	return (0);
}

static int	choose_rotate(t_field *field, t_obj *tet, t_pos *pos)
{
	int	val;

	if (pos->r == 0)
		val = rotate_up(field, pos, tet);
	else if (pos->r == 1)
		val = rotate_right(field, pos, tet);
	else if (pos->r == 2)
		val = rotate_down(field, pos, tet);
	else if (pos->r == 3)
		val = rotate_left(field, pos, tet);
	else
		val = -1;
	return (val);
}

int	rotate(t_field *field, t_obj *tet, t_pos *pos)
{
	int	val;

	clear_field(field);
	pos->r += 1;
	if (pos->r > 3)
		pos->r = 0;
	val = choose_rotate(field, tet, pos);
	if (val)
	{
		clear_field(field);
		pos->r -= 1;
		if (pos->r < 0)
			pos->r = 3;
		val = choose_rotate(field, tet, pos);
		if (val)
		{
			field->field[0][0].move = 2;
			field->field[0][0].color = val;
		}
	}
	return (1);
}
