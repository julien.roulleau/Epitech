/*
** rotate_left.c for tetris in /PSU_2016_tetris/field/rotate/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Wed Mar 15 17:50:04 2017 Fesquet David
** Last update Sun Mar 19 19:46:45 2017 Fesquet David
*/

#include "struct.h"

#define BTW(n, nmin, nmax) ((n >= nmin && n < nmax)? 0 : 1)

int	rotate_left(t_field *field, t_pos *pos, t_obj *tet)
{
	int	x[2];
	int	y[2];

	y[0] = -1;
	while (++(y[0]) < tet->size.y)
	{
		*x = -1;
		while (++(*x) < tet->size.x)
		{
			y[1] = pos->y - tet->size.y / 2 + (tet->size.x - *x);
			x[1] = pos->x - tet->size.x / 2 + y[0];
			if (BTW(x[1], 0, field->sx) || BTW(y[1], 0, field->sy))
				return (1);
			if (field->field[y[1]][x[1]].move == 0 &&
				tet->obj[y[0]][x[0]] == 1)
			{
				field->field[y[1]][x[1]].color = tet->color;
				field->field[y[1]][x[1]].move = 2;
			}
			else if (field->field[y[1]][x[1]].move != 0)
				return (1);
		}
	}
	return (0);
}
