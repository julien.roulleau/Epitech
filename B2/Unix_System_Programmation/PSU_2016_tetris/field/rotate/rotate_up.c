/*
** rotate_up.c for  in /home/david/project/en_cour/PSU_2016_tetris/field/rotate/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Tue Mar 14 15:51:13 2017 Fesquet David
** Last update Sun Mar 19 20:17:13 2017 Fesquet David
*/

#include "struct.h"

#define BTW(n, nmin, nmax) ((n >= nmin && n < nmax)? 0 : 1)

int	rotate_up(t_field *field, t_pos *pos, t_obj *tet)
{
	int	x[2];
	int	y[2];

	y[0] = -1;
	while (++(y[0]) < tet->size.y)
	{
		x[0] = -1;
		while (++(x[0]) < tet->size.x)
		{
			y[1] = pos->y - tet->size.y / 2 + y[0];
			x[1] = pos->x - tet->size.x / 2 + x[0];
			if (BTW(x[1], 0, field->sx) || BTW(y[1], 0, field->sy))
				return (1);
			if (field->field[y[1]][x[1]].move == 0 &&
				tet->obj[y[0]][x[0]] == 1)
			{
				field->field[y[1]][x[1]].color = tet->color;
				field->field[y[1]][x[1]].move = 2;
			}
			else if (field->field[y[1]][x[1]].move != 0)
				return (1);
		}
	}
	return (0);
}
