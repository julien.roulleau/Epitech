/*
** rotate_down.c for tetris in /home/david/PSU_2016_tetris/field/rotate/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Wed Mar 15 12:34:35 2017 Fesquet David
** Last update Sun Mar 19 19:47:44 2017 Fesquet David
*/

#include "struct.h"

#define BTW(n, nmin, nmax) ((n >= nmin && n <= nmax)? 0 : 1)

int	rotate_down(t_field *field, t_pos *pos, t_obj *tet)
{
	int	x[2];
	int	y[2];

	*y = -1;
	while (++(*y) < tet->size.y)
	{
		*x = -1;
		while (++(*x) < tet->size.x)
		{
			y[1] = pos->y - tet->size.y / 2 + (tet->size.y - *y);
			x[1] = pos->x - tet->size.x / 2 + (tet->size.x - *x);
			if (BTW(x[1], 0, field->sx) || BTW(y[1], 0, field->sy))
				return (1);
			if (field->field[y[1]][x[1]].move == 0 &&
				tet->obj[*y][*x] == 1)
			{
				field->field[y[1]][x[1]].color = tet->color;
				field->field[y[1]][x[1]].move = 2;
			}
			else if (field->field[y[1]][x[1]].move != 0)
				return (1);
		}
	}
	return (0);
}
