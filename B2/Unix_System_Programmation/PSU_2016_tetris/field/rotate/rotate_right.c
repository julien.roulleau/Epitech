/*
** rotate_right.c for tetris in /PSU_2016_tetris/field/rotate/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Sun Mar 19 17:26:49 2017 Fesquet David
** Last update Sun Mar 19 21:26:14 2017 Fesquet David
*/

#include "struct.h"

#define BTW(n, nmin, nmax) ((n >= nmin && n < nmax)? 0 : 1)

int	rotate_right(t_field *field, t_pos *pos, t_obj *tet)
{
	int	x[2];
	int	y[2];

	y[0] = -1;
	while (++(y[0]) < tet->size.y)
	{
		x[0] = -1;
		while (++(x[0]) < tet->size.x)
		{
			x[1] =
			pos->x - (tet->size.x / 2) + (tet->size.y - 1 - y[0]);
			y[1] = pos->y - (tet->size.y / 2) + x[0];
			if (BTW(x[1], 0, field->sx) || BTW(y[1], 0, field->sy))
				return (2);
			if (field->field[y[1]][x[1]].move == 0 &&
				tet->obj[y[0]][x[0]] == 1)
			{
				field->field[y[1]][x[1]].color = tet->color;
				field->field[y[1]][x[1]].move = 2;
			}
			else if (field->field[y[1]][x[1]].move != 0)
				return (1);
		}
	}
	return (0);
}
