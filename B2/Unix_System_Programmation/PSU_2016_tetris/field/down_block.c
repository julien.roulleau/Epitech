/*
** down_block.c for tetris in /home/david/project/PSU_2016_tetris/field/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Fri Mar 10 15:30:21 2017 Fesquet David
** Last update Sun Mar 19 20:31:46 2017 Fesquet David
*/

#include "struct.h"

int	down_block(t_field *field, t_pos *pos)
{
	int	y;
	int	x;

	y = field->sy - 2;
	while (y >= 0)
	{
		x = 0;
		while (x < field->sx)
		{
			if (field->field[y][x].move == 2)
			{
				field->field[y + 1][x].move = 2;
				field->field[y + 1][x].color =
				field->field[y][x].color;
				field->field[y][x].move = 0;
				field->field[y][x].color = 0;
			}
			x++;
		}
		y--;
	}
	pos->y += 1;
	return (1);
}
