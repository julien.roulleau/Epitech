/*
** color.h for tetris in /home/na/Dropbox/Job/En_Cours/PSU_2016_tetris/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Feb 23 13:42:12 2017 Roulleau Julien
** Last update Sun Mar 19 21:19:38 2017 Fesquet David
*/

#ifndef GAME_H
# define GAME_H

#include <curses.h>
#include "struct.h"

t_tet	get_obj();
int	set_game();
int	close_game();
void 	set_color();
void 	print_color(char *);
int 	print_title(int);
int	print_stat(int, t_game);
int	print_next(int, t_obj);
int	print_board(t_field*, int);
int	my_rand(double, double);
int	match_str(char *, char *);
int	print_lose(int);
int	bad_size(t_field*);
int	print_game(t_option option, t_game game, t_obj *next, t_field *field);
int	action(t_option option, t_field *field, char *buff, t_pos *pos);
int	sleep_time(t_game game, t_option option, t_pos *pos , t_field *field);

#endif /* GAME_H */
