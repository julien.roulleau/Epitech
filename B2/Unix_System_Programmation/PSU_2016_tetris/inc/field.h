/*
** field.h for tetris in /PSU_2016_tetris/inc/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Sun Mar 19 14:52:34 2017 Fesquet David
** Last update Sun Mar 19 18:26:03 2017 Fesquet David
*/

# ifndef FIELD_H_
# define FIELD_H_

#include "struct.h"

int	field_clear(t_field *field);
int	right_block(t_field *field, t_pos *pos);
int	left_block(t_field *field, t_pos *pos);
int	set_block(t_obj *block, t_field *field, t_pos *pos);
t_field	field_create(int sy, int sx);
int	check_block(t_field *field);
int	down_block(t_field *field, t_pos *pos);
int	mv_in_fix(t_field *field);
int	rotate_right(t_field *field, t_pos *pos, t_obj *tet);
int	rotate_left(t_field *field, t_pos *pos, t_obj *tet);
int	rotate_down(t_field *field, t_pos *pos, t_obj *tet);
int	rotate_up(t_field *field, t_pos *pos, t_obj *tet);
int	rotate(t_field *field, t_obj *tet, t_pos *pos);

# endif
