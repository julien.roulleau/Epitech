/*
** obj.h for tetris in /home/david/project/en_cour/PSU_2016_tetris/inc/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Thu Feb 23 14:05:42 2017 Fesquet David
** Last update Mon Feb 27 14:58:42 2017 Fesquet David
*/

#ifndef OBJ_H
# define OBJ_H

#include "struct.h"

char	*get_name(char *str);
int	get_size_and_color(t_objd *obj, int fd);
int	set_tetrimino(t_objd *obj, int fd);
int	alph_sorting(t_tet*);
int	set_obj_with_objd(t_tet*);

#endif
