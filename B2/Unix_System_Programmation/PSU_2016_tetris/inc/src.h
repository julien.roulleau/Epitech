/*
** my.h for src in /home/infocraft/Job/Lib/src/include/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun Jan  8 23:52:16 2017 Roulleau Julien
** Last update Wed Feb 22 16:49:20 2017 Roulleau Julien
*/

#ifndef SRC_H
# define SRC_H

# include <unistd.h>
# include <stdlib.h>
# include <fcntl.h>

# define STRLEN(str) (sizeof(str) / sizeof(str[0]) - 1)
# define PRINT(str) (write(1, str, STRLEN(str)))

# define ABS(nb) (nb < 0 ? nb * -1 : nb)

# define IS_ALPHA_UP(c) (c >= 'A' && c <= 'Z')
# define IS_ALPHA_DOWN(c) (c >= 'a' && c <= 'z')
# define IS_NUM(c) (c >= '0' && c <= '9')
# define IS_ALPHA(c) (IS_ALPHA_UP(c) || IS_ALPHA_DOWN(c))
# define IS_ALPHA_NUM(c) (IS_ALPHA(c) || IS_NUM(c))

/* is */
int		is_alpha_up(char *);
int		is_alpha_down(char *);
int		is_num(char *);
int		is_alpha(char *);
int		is_alpha_num(char *);

/* nbr */
int		my_getnbr(char *);
int		my_nbrlen(int);
void		my_putnbr(int);
void		my_putnbr_base(int, char *);
void		my_putnbr_base_unsigned(unsigned int, char *);
void		my_putnbr_base_pointer(unsigned long int, char *);

/* str */
void		my_putstr(char *);
void		my_putstr_s(char *);
char		*my_strdup(char *);
int		my_strcmp(char *, char *);
int		my_strlen(char *);
char		*merge_str(char *, char *);
char		**str_to_wordtab(char *, char);
void 		malloc_tab(char ***, char *, char);
char		*get_next_line(const int);

#endif		/* !SRC_H */
