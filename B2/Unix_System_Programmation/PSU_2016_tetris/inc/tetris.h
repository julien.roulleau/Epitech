/*
** tetris.h for tetris in /home/na/Dropbox/Job/En_Cours/PSU_2016_tetris/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Mar 14 10:20:35 2017 Roulleau Julien
** Last update Sun Mar 19 17:28:10 2017 Roulleau Julien
*/

#ifndef TETRIS_H
# define TETRIS_H

#include "struct.h"

int 		tetris(t_option, t_tet tet);

#endif /* TETRIS_H */
