/*
** struct.c for tetris in /home/na/Dropbox/Job/En_Cours/PSU_2016_tetris/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Feb 21 11:45:59 2017 Roulleau Julien
** Last update Sun Mar 19 20:44:58 2017 Roulleau Julien
*/

#ifndef STRUCT_H
# define STRUCT_H

typedef struct	s_size
{
	int	x;
	int	y;
}		t_size;

typedef struct	s_objd
{
	t_size	size;
	char	*name;
	int	color;
	int	error;
	int	**obj;
}		t_objd;

typedef struct	s_obj
{
	t_size	size;
	int	color;
	int	**obj;
}		t_obj;

typedef struct	s_tet
{
	t_obj	**obj;
	int	size;
	t_objd	**objd;
	int	d_size;
	int	error;

}		t_tet;

typedef struct	s_option
{
	t_tet	tet;
	int	level;
	char	*key_left;
	char	*key_right;
	char	*key_turn;
	char	*key_drop;
	char	*key_quit;
	char	*key_pause;
	t_size	map_size;
	int	without_next;
	int	debug;
}		t_option;

typedef struct	s_block
{
	int	color;
	int	move;
}		t_block;

typedef struct	s_pos
{
	int	x;
	int	y;
	int	r;
}		t_pos;

typedef struct	s_field
{
	int	sy;
	int	sx;
	t_block	**field;
	t_obj	*next;
	t_obj	*curent;
}		t_field;

typedef struct 	s_pttab
{
	char 	*flag_mini;
	char 	*flag_full;
 	int	(*flag_pointer)(t_option *, char *);
}		t_pttab;


typedef struct	s_game
{
	int	hscore;
	int	score;
	int	line;
	int	level;
	int	timer;
	int	speed;
}		t_game;

#endif /* STRUCT_H */
