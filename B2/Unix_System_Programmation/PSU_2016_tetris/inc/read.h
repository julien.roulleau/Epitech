/*
** read.h for tetris in /home/david/project/en_cour/PSU_2016_tetris/inc/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Wed Mar  8 10:45:17 2017 Fesquet David
** Last update Sun Mar 19 15:05:30 2017 Fesquet David
*/

# ifndef READ_MODE_H
# define READ_MODE_H

char	*scan_key(int opt);
int	scan_key_debug();
int	read_mode_debug(int i);
int	set_read_mode();
int	unset_read_mode();

# endif
