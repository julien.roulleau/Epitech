/*
** option.h for tetris in /home/na/Dropbox/Job/En_Cours/PSU_2016_tetris/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Feb 22 15:22:26 2017 Roulleau Julien
** Last update Sat Mar 18 20:21:11 2017 Fesquet David
*/

#ifndef OPTION_H
# define OPTION_H

#include "struct.h"

t_option	init_option(char **env);
char		**init_with_term(char **env);
int		set_option(t_option *, char **);
int 		option_level(t_option *, char *);
int 		option_key_left(t_option *, char *);
int 		option_key_right(t_option *, char *);
int 		option_key_turn(t_option *, char *);
int 		option_key_drop(t_option *, char *);
int 		option_key_quit(t_option *, char *);
int 		option_key_pause(t_option *, char *);
int 		option_map_size(t_option *, char *);
int 		option_without_next(t_option *, char *);
int 		option_debug(t_option *, char *);
int 		option_help(t_option *, char *);
int		check_option(t_option);
void		print_debug(t_tet, t_option);

#endif /* OPTION_H */
