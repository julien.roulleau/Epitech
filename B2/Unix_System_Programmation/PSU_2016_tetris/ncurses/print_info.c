/*
** print_info.c for tetris in /home/na/Dropbox/Job/En_Cours/PSU_2016_tetris/ncurses/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun Mar 19 13:47:38 2017 Roulleau Julien
** Last update Sun Mar 19 16:46:02 2017 Roulleau Julien
*/

#include "game.h"
#include "src.h"

#define STAT_SIZE 30
#define DIST 10

static void 	print_head(int line, int pos, int nb)
{
	int	i;

	i = -1;
	move(line, pos);
	while (++i < nb)
		print_color("8");
}

static void 	print_line(int line, int pos, char *str, int value)
{
	(void) pos;
	move(line, 4);
	print_color("8");
	attron(COLOR_PAIR(9));
	move(line,  8);
	printw(str);
	move(line,  (STAT_SIZE - 5) * 2);
	if (value != -1)
		printw("%i", value);
	move(line,  (STAT_SIZE + 1) * 2);
	print_color("8");

}

int		print_stat(int line, t_game game)
{
	print_head(++line, 4, STAT_SIZE);
	print_line(++line, 0, "Hight score", game.hscore);
	print_line(++line, 0, "Score", game.score);
	print_line(++line, 0, "", -1);
	print_line(++line, 0, "Lines", game.line);
	print_line(++line, 0, "Level", game.level);
	print_line(++line, 0, "", -1);
	print_line(++line, 0, "Timer", game.timer);
	print_head(++line, 4, STAT_SIZE);
	return (line);
}

static void 	print_box(int line, int nb, int size, int dist)
{
	int	i;

	i = -1;
	print_head(line, (STAT_SIZE + dist) * 2, 10);
	while (++i < nb)
	{
		move(++line, (STAT_SIZE + dist) * 2);
		print_color("8");
		move(line, (STAT_SIZE + dist + size) * 2);
		print_color("8");
	}
	print_head(++line, (STAT_SIZE + dist) * 2, 10);
}

int		print_next(int line, t_obj obj)
{
	int	i;
	int	n;

	i = -1;
	print_box(++line, 7, 9, DIST);
	line += 2;
	while (++i < obj.size.y && (n = -1))
	{
		move(++line, (STAT_SIZE + DIST + obj.size.y / 2 + 1) * 2);
		while (++n < obj.size.x)
		{
			if (obj.obj[i][n] == 1)
				attron(COLOR_PAIR(obj.color + 1));
			else
				attron(COLOR_PAIR(1));
			printw("a");
			printw("a");
		}
	}
	return (line);
}
