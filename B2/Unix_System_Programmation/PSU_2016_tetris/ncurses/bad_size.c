/*
** bad_size.c for tetris in /home/na/Dropbox/Job/En_Cours/PSU_2016_tetris/ncurses/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun Mar 19 19:25:47 2017 Roulleau Julien
** Last update Sun Mar 19 21:12:12 2017 Fesquet David
*/
#include "game.h"

int		bad_size(t_field *field)
{
	while (COLS < 100 || COLS < field->sy * 2 || LINES < (field->sx + 40))
	{
		clear();
		attron(COLOR_PAIR(9));
		mvprintw(LINES / 2, COLS / 2 - 4, "bad_size");
		refresh();
	}
	return (0);
}
