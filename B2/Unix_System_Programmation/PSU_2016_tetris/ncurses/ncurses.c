/*
** ncurses.c for tetris in /home/na/Dropbox/Job/En_Cours/PSU_2016_tetris/ncurses/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Feb 23 13:37:40 2017 Roulleau Julien
** Last update Sun Mar 19 19:19:13 2017 Roulleau Julien
*/

#include "game.h"
#include "read.h"

int		set_game()
{
	initscr();
	start_color();
	set_color();
	raw();
	noecho();
	curs_set(FALSE);
	keypad(stdscr, true);
	set_read_mode();
	return (1);
}

int		close_game()
{
	endwin();
	return (1);
}
