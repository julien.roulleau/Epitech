/*
** print.c for tetris in /home/na/Dropbox/Job/En_Cours/PSU_2016_tetris/ncurses/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sat Mar 18 18:57:48 2017 Roulleau Julien
** Last update Sun Mar 19 19:21:36 2017 Roulleau Julien
*/

#include <fcntl.h>
#include "game.h"
#include "src.h"
#include "read.h"

#define TITLE_FILE "other/title.obj"
#define LOSE_FILE "other/lose.obj"

int 		print_title(int line)
{
	char	*buffer;
	int	size;
	int	fd;

	line = size = 0;
	if ((fd = open(TITLE_FILE, O_RDONLY)) == -1)
		return (0);
	while ((buffer = get_next_line(fd)))
	{
		if (size == 0)
			size = my_strlen(buffer);
		move(++line, 4);
		print_color(buffer);
	}
	return (line);
}

int 		print_lose(int line)
{
	char	*buffer;
	int	size;
	int	fd;

	line = size = 0;
	clear();
	if ((fd = open(LOSE_FILE, O_RDONLY)) == -1)
		return (0);
	while ((buffer = get_next_line(fd)))
	{
		if (size == 0)
			size = my_strlen(buffer);
		move(++line, 4);
		print_color(buffer);
	}
	refresh();
	read_mode_debug(0);
	scan_key_debug();
	read_mode_debug(1);
	return (line);
}
