/*
** color.c for tetris in /home/na/Dropbox/Job/En_Cours/PSU_2016_tetris/ncurses/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Feb 23 13:40:54 2017 Roulleau Julien
** Last update Sun Mar 19 13:30:10 2017 Roulleau Julien
*/
#include <curses.h>
#include "src.h"

void 		set_color()
{
	init_pair(1, COLOR_BLACK, COLOR_BLACK);
	init_pair(2, COLOR_RED, COLOR_RED);
	init_pair(3, COLOR_GREEN, COLOR_GREEN);
	init_pair(4, COLOR_YELLOW, COLOR_YELLOW);
	init_pair(5, COLOR_BLUE, COLOR_BLUE);
	init_pair(6, COLOR_MAGENTA, COLOR_MAGENTA);
	init_pair(7, COLOR_CYAN, COLOR_CYAN);
	init_pair(8, COLOR_WHITE, COLOR_WHITE);
	init_pair(9, COLOR_WHITE, COLOR_BLACK);
}

void 		print_color(char *str)
{
	char	c[2];
	int	i;

	i = -1;
	while (str[++i])
	{
		c[0] = str[i];
		c[1] = 0;
		if (!IS_NUM(c[0]))
			attron(COLOR_PAIR(1));
		else
			attron(COLOR_PAIR(my_getnbr(c)));
		printw(c);
		printw(c);
	}
}
