/*
** print_board.c for tetris in /home/na/Dropbox/Job/En_Cours/PSU_2016_tetris/ncurses/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun Mar 19 14:26:56 2017 Roulleau Julien
** Last update Sun Mar 19 21:11:31 2017 Fesquet David
*/

#include "struct.h"
#include "game.h"

static void 	print_head(int line, int pos, int nb)
{
	int	i;

	i = -1;
	move(line, pos);
	while (++i < nb)
		print_color("8");
}

static void 	print_box(int line, int nb, int size, int dist)
{
	int	i;

	i = -1;
	print_head(line, (dist) * 2, size + 1);
	while (++i < nb)
	{
		move(++line, (dist) * 2);
		print_color("8");
		move(line, (dist + size) * 2);
		print_color("8");
	}
	print_head(++line, (dist) * 2, size + 1);
}

int		print_board(t_field *field, int line)
{
	int	y;
	int	x;

	y = -1;
	print_box(line, field->sy + 2, field->sx + 2, 4);
	line++;
	while (++y < field->sy && (x = -1))
	{
		move(++line, 11);
		while (++x < field->sx)
		{
			attron(COLOR_PAIR(field->field[y][x].color + 1));
			printw("  ");
		}
	}
	return (0);
}
