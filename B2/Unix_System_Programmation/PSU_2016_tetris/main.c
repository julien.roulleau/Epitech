/*
** main.c for tetris in /home/na/Dropbox/Job/En_Cours/PSU_2016_tetris/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Feb 21 11:43:52 2017 Roulleau Julien
** Last update Sun Mar 19 19:21:17 2017 Roulleau Julien
*/

#include <stdlib.h>
#include <time.h>
#include "option.h"
#include "game.h"
#include "tetris.h"
#include "read.h"

int			main(int argc, char **argv, char **env)
{
	(void) argc;
	t_option	option;
	t_tet		tet;

	srand(time(0));
	option = init_option(env);
	option.tet = tet = get_obj();
	if (tet.error == 1)
		return (84);
	if (!set_option(&option, argv) || check_option(option))
		return (84);
	set_game();
	if (tetris(option, tet) == 2)
	{
		unset_read_mode();
		print_lose(1);
	}
	close_game();
	return (0);
}
