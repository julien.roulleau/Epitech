/*
** read_mode.c for  in /home/david/project/en_cour/PSU_2016_tetris/read/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Mon Mar  6 17:38:16 2017 Fesquet David
** Last update Sun Mar 19 15:10:04 2017 Fesquet David
*/

#include <stdio.h>
#include <curses.h>
#include <term.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/ioctl.h>

static int	read_mode(int i)
{
	static struct termios savT;
	static struct termios newT;
	static int c = 0;

	if (c == 0 && i == 0)
	{
		ioctl(0, TCGETS, &savT);
		ioctl(0, TCGETS, &newT);
		newT.c_lflag &= ~ECHO;
		newT.c_lflag &= ~ICANON;
		newT.c_cc[VMIN] = 0;
		newT.c_cc[VTIME] = 1;
		ioctl(0, TCSETS, &newT);
		c = 1;
	}
	else if (i == 1)
		ioctl(0, TCSETS, &savT);
	return (0);
}

int	read_mode_debug(int i)
{
	static struct termios sav2T;
	static struct termios new2T;
	static int c = 0;

	if (c == 0 && i == 0)
	{
		ioctl(0, TCGETS, &sav2T);
		ioctl(0, TCGETS, &new2T);
		new2T.c_lflag &= ~ECHO;
		new2T.c_lflag &= ~ICANON;
		new2T.c_cc[VMIN] = 1;
		new2T.c_cc[VTIME] = 0;
		ioctl(0, TCSETS, &new2T);
		c = 1;
	}
	else if (i == 1)
		ioctl(0, TCSETS, &sav2T);
	return (0);
}

int	set_read_mode()
{
	read_mode(0);
	return (0);
}

int	unset_read_mode()
{
	read_mode(1);
	return (0);
}
