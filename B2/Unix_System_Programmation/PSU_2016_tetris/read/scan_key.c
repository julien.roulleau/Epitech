/*
** scan_key.c for  in /home/david/project/en_cour/PSU_2016_tetris/read/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Wed Mar  8 10:42:59 2017 Fesquet David
** Last update Sun Mar 19 20:40:11 2017 Fesquet David
*/

#include <unistd.h>
#include <stdlib.h>
#include <src.h>
#include <curses.h>

static char	*str_cat(char **str, char **add)
{
	char	*new_str;
	int	i;
	int	n;

	i = -1;
	n = -1;
	if (!(new_str = malloc(sizeof(char) *
		(my_strlen((*str)) + my_strlen((*add)) + 1))))
		return (0);
	while ((*str)[++i])
		new_str[++n] = (*str)[i];
	i = -1;
	while ((*add)[++i])
		new_str[++n] = (*add)[i];
	new_str[++n] = 0;
	free(*str);
	return (new_str);
}

int	scan_key_debug()
{
	char	buff[2];

	read(0, buff, 1);
	return (1);
}

char	*scan_key(int opt)
{
	static char 	*buff;
	static int	i = -1;
	char		*line;
	int		size;

	if (opt == 1 && i != -1 && (i = -1))
		free(buff);
	if (i == -1 && !(i = 0))
	{
		if (!(buff = malloc(sizeof(char) * 2)))
			return (NULL);
		buff[0] = '\0';
	}
	if (opt == 1)
		return (buff);
	if (!(line = malloc(sizeof(char) * 100)))
		return (NULL);
	size = read(0, line, 99);
	line[size] = '\0';
	buff = str_cat(&buff, &line);
	free(line);
	return (buff);
}
