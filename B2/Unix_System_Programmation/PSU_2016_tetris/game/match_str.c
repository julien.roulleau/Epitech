/*
** match_str.c for tetris in /PSU_2016_tetris/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Sun Mar 19 15:58:49 2017 Fesquet David
** Last update Sun Mar 19 16:09:00 2017 Fesquet David
*/

static int	match(char *str, char *to_find)
{
	int	i;

	i = 0;
	while (str[i] && to_find[i] && str[i] == to_find[i])
		i++;
	if (to_find[i] == 0)
		return (1);
	return (0);
}

int	match_str(char *src, char *to_find)
{
	int	i;

	i = -1;
	while (src[++i])
	{
		if (match(src + i, to_find))
			return (1);
	}
	return (0);
}
