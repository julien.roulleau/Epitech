/*
** tetris.c for tetris in /home/na/Dropbox/Job/En_Cours/PSU_2016_tetris/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Feb 21 16:17:51 2017 Roulleau Julien
** Last update Sun Mar 19 21:36:36 2017 Fesquet David
*/

#include "game.h"
#include "read.h"
#include "src.h"
#include "field.h"

static t_game 	init_game(t_option option)
{
	t_game	game;

	game.hscore = 0;
	game.score = 0;
	game.line = 0;
	game.level = option.level;
	game.timer = 0;
	game.speed = 500000;
	return (game);
}

int	print_game(t_option option, t_game game, t_obj *next, t_field *field)
{
	int	line;
	int	linebis;

	line = 0;
	bad_size(field);
	clear();
	line = print_title(line);
	line += 4;
	linebis = print_stat(line, game);
	if (!option.without_next)
		line = print_next(line, *next);
	line = line > linebis ? line : linebis;
	line += 4;
	print_board(field, line);
	refresh();
	return (0);
}

int		lunch(t_game *game, t_field *field, t_pos *pos, t_tet *tet)
{
	int	nb;

	if (!check_block(field))
		down_block(field, pos);
	else
	{
		mv_in_fix(field);
		if ((nb = field_clear(field)) > 0)
		{
			game->line += nb;
			if (game->line % 10 == 0 && game->line != 1)
			{
				game->level++;
			}
			game->score += 100 * nb;
		}
		if (set_block(field->next, field, pos) == 1)
			return (1);
		field->curent = field->next;
		field->next = tet->obj[my_rand(0, tet->size)];
	}
	return (0);
}

int		action(t_option option, t_field *field, char *buff, t_pos *pos)
{
	if (match_str(buff, option.key_left) && left_block(field, pos) >= 0)
		return (1);
	if (match_str(buff, option.key_right) && right_block(field, pos) >= 0)
		return (1);
	if (match_str(buff, option.key_turn) &&
		rotate(field, field->curent, pos))
		return (1);
	if (match_str(buff, option.key_drop) && !check_block(field) &&
		down_block(field, pos))
		return (1);
	if (match_str(buff, option.key_quit))
		return (2);
	if (match_str(buff, option.key_pause) && scan_key(1))
	{
		while ((buff = scan_key(0)) &&
			!match_str(buff, option.key_pause));
		return (1);
	}
	return (0);
}

int 		tetris(t_option option, t_tet tet)
{
	t_game	game;
	t_field	field;
	t_pos	pos;

	game = init_game(option);
	field = field_create(option.map_size.y, option.map_size.x);
	field.curent = tet.obj[my_rand(0, tet.size)];
	field.next = tet.obj[my_rand(0, tet.size)];
	set_block(field.curent, &field, &pos);
	while (1)
	{
		game.speed = 500000 - (100000 * game.level);
		if (game.speed < 0)
			game.speed = 0;
		if (sleep_time(game, option, &pos, &field))
			return (1);
		if (lunch(&game, &field, &pos, &tet) == 1)
			return (2);
	}
	return (1);
}
