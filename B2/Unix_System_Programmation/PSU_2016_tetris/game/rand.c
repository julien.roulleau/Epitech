/*
** rand.c for tetris in /home/na/Dropbox/Job/En_Cours/PSU_2016_tetris/game/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun Mar 19 17:37:10 2017 Roulleau Julien
** Last update Sun Mar 19 17:37:52 2017 Roulleau Julien
*/

#include <stdlib.h>

int		my_rand(double min, double max)
{
	return (rand() / (double) RAND_MAX * (max - min) + min);
}
