/*
** tetris2.c for tetris in /PSU_2016_tetris/game/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Sun Mar 19 21:16:53 2017 Fesquet David
** Last update Sun Mar 19 21:32:40 2017 Fesquet David
*/

#include "game.h"
#include "read.h"
#include "src.h"
#include "field.h"

int	sleep_time(t_game game, t_option option, t_pos *pos , t_field *field)
{
	int	s;
	char	*buffer;
	int	act;

	s = game.speed;
	while (s > 0)
	{
		if (!(buffer = scan_key(0)))
			return (1);
		usleep(100000);
		s -= 100000;
		act = action(option, field, buffer, pos);
		if (act == 1)
			scan_key(1);
		else if (act == 2)
			return (1);
		print_game(option, game, field->next, field);
	}
	return (0);
}
