/*
** get_inf_obj.c for tetris in /home/david/project/en_cour/PSU_2016_tetris/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Thu Feb 23 13:55:13 2017 Fesquet David
** Last update Tue Mar 14 11:16:20 2017 Fesquet David
*/

#include "struct.h"
#include "src.h"

#include <dirent.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

#include <stdio.h>

char	*get_name(char *str)
{
	char	*name;
	int	i;
	int	j;

	i = my_strlen(str) - 1;
	j = 0;
	while (i != 0 && str[i--] != '.');
	while (i != 0 && str[i--] != '/' && (j = j + 1));
	if (!(name = malloc(sizeof(char) * (j + 2))))
		return (0);
	i += 2;
	j = 0;
	while (str[i] != '.' && str[i] != '\0')
		name[j++] = str[i++];
	name[j] = '\0';
	return (name);
}

int	get_size_and_color(t_objd *obj, int fd)
{
	char	*str;
	int	i;

	i = 0;
	if (!(str = get_next_line(fd)))
		return (1);
	while (str[i] && str[i] == ' ')
		i++;
	if (!(obj->size.x = my_getnbr(str + i)))
		return (1);
	while (str[i] && str[i] != ' ')
		i++;
	while (str[i] && str[i] == ' ')
		i++;
	if (!(obj->size.y = my_getnbr(str + i)))
		return (1);
	while (str[i] && str[i] != ' ')
		i++;
	while (str[i] && str[i] == ' ')
		i++;
	if (!(obj->color = my_getnbr(str + i)))
		return (1);
	return (0);
}

int	*set_line(int *line, char *str, int size)
{
	int	i;

	i = 0;
	while (i < size)
	{
		line[i] = 0;
		if (i < my_strlen(str) && str[i] != ' ' && str[i] != '*')
			return (NULL);
		if (i < my_strlen(str) && str[i] == '*')
			line[i] = 1;
		i++;
	}
	return (line);
}

static int	count_size(char *str)
{
	int	i;

	i = my_strlen(str) - 1;
	while (str[i--] == ' ');
	return (i + 1);
}

int	set_tetrimino(t_objd *obj, int fd)
{
	int	y;
	char	*str;

	if (!(obj->obj = malloc(sizeof(int*) * obj->size.y)))
		return (1);
	y = 0;
	while (y < obj->size.y)
	{
		if (!(obj->obj[y] = malloc(sizeof(int) * obj->size.x)) ||
			!(str = get_next_line(fd)) ||
			count_size(str) > obj->size.x)
			return (2);
		if (!(obj->obj[y] = set_line(obj->obj[y], str,
			obj->size.x)))
			return (3);
		y++;
	}
	if (get_next_line(fd))
		return (4);
	y = 0;
	while (y < obj->size.y)
		if (obj->obj[y++][obj->size.x - 1] == 1)
			return (0);
	return (5);
}
