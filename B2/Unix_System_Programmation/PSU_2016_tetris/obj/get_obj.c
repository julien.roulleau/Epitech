/*
** get_obj.c for tetris in /home/david/project/en_cour/PSU_2016_tetris/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Tue Feb 21 15:09:38 2017 Fesquet David
** Last update Sat Mar 18 12:59:28 2017 Fesquet David
*/

#include "struct.h"
#include "src.h"
#include "obj.h"

#include <dirent.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

#include <stdio.h>

#define FOLDER_NAME "tetrimino"

static int	is_tet(struct dirent *file, char *end)
{
	int	i1;
	int	i2;

	if (file->d_type != DT_REG)
		return (0);
	i1 = my_strlen(end) - 1;
	i2 = my_strlen(file->d_name) - 1;
	while (i1 >= 0 && i2 >= 0)
	{
		if (file->d_name[i2] != end[i1])
			return (0);
		i1--;
		i2--;
	}
	return (1);
}

static int	file_count()
{
	DIR			*rep;
	struct dirent	*filere;
	int			count;

	count = 0;
	if (!(rep = opendir(FOLDER_NAME)))
		return (1);
	while ((filere = readdir(rep)) != NULL)
	{
		if (is_tet(filere, ".tetrimino"))
			count++;
	}
	closedir(rep);
	return (count);
}

static	t_objd	*add_obj(char *str)
{
	int	fd;
	t_objd	*objd;

	if (!(objd = malloc(sizeof(t_objd))))
		return (NULL);
	objd->error = 0;
	objd->name = get_name(str);
	if ((fd = open(str, O_RDONLY)) < 0 && (objd->error = 1))
		return (objd);
	if (get_size_and_color(objd, fd) && (objd->error = 1))
		return (objd);
	if (set_tetrimino(objd, fd) && (objd->error = 1))
		return (objd);
	return (objd);
}

t_tet	get_obj()
{
	DIR			*rep;
	struct dirent	*file;
	int			i;
	t_tet			tet;

	rep = opendir(FOLDER_NAME);
	tet.size = tet.error = i =0;
	tet.d_size = file_count();
	tet.objd = malloc(sizeof(t_objd*) * (tet.d_size + 1));
	if (((!rep || !tet.objd) || (tet.d_size == 0)) && (tet.error = 1))
		return (tet);
	while ((file = readdir(rep)) != NULL)
		if (is_tet(file, ".tetrimino"))
		{
			tet.objd[i] = add_obj(merge_str("tetrimino/",
						file->d_name));
			if (tet.objd[i]->error == 0)
				tet.size += 1;
			i++;
		}
	alph_sorting(&tet);
	set_obj_with_objd(&tet);
	closedir(rep);
	return (tet);
}
