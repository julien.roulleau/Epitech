/*
** objd_sorting.c for tetris in /home/david/project/en_cour/PSU_2016_tetris/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Thu Feb 23 19:04:34 2017 Fesquet David
** Last update Tue Mar 14 11:21:36 2017 Fesquet David
*/

#include "struct.h"
#include "src.h"
#include "obj.h"

#include <stdio.h>

static int	str_alph_cmp(char *s1, char *s2)
{
	int	i;

	i = 0;
	while (s1[i] && s2[i])
	{
		if (s1[i] < s2[i])
			return (0);
		else if (s1[i] > s2[i])
			return (1);
		i++;
	}
	if (s1[i] < s2[i])
		return (0);
	if (s1[i] > s2[i])
		return (1);
	return (2);
}

int	alph_sorting(t_tet *tet)
{
	int		i;
	t_objd	*tmp;

	i = 1;
	while (i < tet->d_size)
	{
		if (str_alph_cmp(tet->objd[i - 1]->name, tet->objd[i]->name))
		{
			tmp = tet->objd[i];
			tet->objd[i] = tet->objd[i - 1];
			tet->objd[i - 1] = tmp;
			i = 0;
		}
		i++;
	}
	return (0);
}

int	set_obj_with_objd(t_tet *tet)
{
	int	i;
	int	j;

	i = j = 0;
	if (!(tet->obj = malloc(sizeof(t_obj*) * (tet->size + 1)))
		&& !(tet->d_size = 0) && !(tet->size = 0))
		return (1);
	while (i < tet->d_size)
	{
		if (tet->objd[i]->error != 1)
		{
			if (!(tet->obj[j] = malloc(sizeof(t_obj)))
				&& !(tet->d_size = 0) && !(tet->size = 0))
				return (1);
			tet->obj[j]->color = tet->objd[i]->color;
			tet->obj[j]->obj = tet->objd[i]->obj;
			tet->obj[j]->size = tet->objd[i]->size;
			j++;
		}
		i++;
	}
	return (0);
}
