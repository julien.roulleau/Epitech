/*
** format.h for Project-Master in /home/thomas/epitech/PSU_2016_42sh
** 
** Made by thibault thomas
** Login   <thibault.thomas@epitech.eu>
** 
** Started on  Wed May 10 13:03:20 2017 thibault thomas
** Last update Sat May 20 22:18:08 2017 thibault thomas
*/

#ifndef FORMAT_H_
# define FORMAT_H_

#include "tree.h"
#define IS_QUOTE(v) ((v == '\'') || (v == '`') || (v == '"'))

extern const char *g_separator[];

void	remove_all_quotes(t_node *);
char	*remove_quotes(char *);
char	*decale(char *, int);
char	**args_to_wordtab(char *, char);
char	*format_str(char *);
char	**str_to_args(char *);
void	show_wordtab(char **);
char	*decale_right(char *, int);
void	insert(char *, int);
char	*reset_separator(char *);
int	get_sep_pos(char *, char);
int	rm_sep(char *);
int	check_separator(char *);
char	*remove_char(char *, char);

#endif
