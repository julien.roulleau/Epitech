/*
** env.h for 42sh in /home/na/Dropbox/Job/En_Cours/PSU_2016_42sh/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri May 19 00:12:18 2017 Roulleau Julien
** Last update Sat May 20 18:09:18 2017 Roulleau Julien
*/

#ifndef ENV_H
# define ENV_H

# include "list.h"

typedef struct 	s_env
{
	char	*name;
	char 	*value;
}		t_env;

t_list		*init_env(char **);
char		**tab_env(t_list *);
int		add_in_env(t_list *, char *, char *);
int		dell_in_env(t_list *, char *);
int		print_env(t_point *);
t_point		*find_in_env(t_point *, char *);
int		free_env(t_list *);

/* Builtin */
int	unset_env(t_list *, char **);
int	set_env(t_list *, char **);
int	my_env(t_list *, char **);
int	my_exit(t_list *, char **);
int	cd(t_list *, char **);
int	echo(t_list *, char **);

#endif /* ENV_H */
