/*
** utils.h for Project-Master in /home/thomas/epitech/PSU_2016_42sh
**
** Made by thibault thomas
** Login   <thibault.thomas@epitech.eu>
**
** Started on  Wed Apr 19 15:24:34 2017 thibault thomas
** Last update Sun May 21 16:31:10 2017 thibault thomas
*/

#ifndef UTILS_H_
# define UTILS_H_

#include <stdlib.h>
#include "tree.h"
#include "list.h"

void	free_wordtab(char **);
void	free_tree(t_node *);
void	*xmalloc(size_t);
int	print_tab(char **tab);
void	free_list_trees(t_list *);

#endif
