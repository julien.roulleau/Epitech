/*
** exe.h for exe.h in /home/benjamin/chikho_b/rendu/PSU/PSU_2016_42sh/inc
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Tue May 16 16:13:24 2017 Benjamin
** Last update Sun May 21 19:10:53 2017 Benjamin
*/

#ifndef EXE_H_
# define EXE_H_

# include "tree.h"
# include "env.h"

typedef struct	s_ptr
{
  char		*flag;
  int		(*ptr)(t_node *, t_list *);
}		t_ptr;

typedef struct	s_builtin
{
  char		*flag;
  int		(*ptr)(t_list *, char **);
}		t_builtin;

int		execute_cmd(t_node *tree, t_list *env);
int		execute_builtins(t_node *, t_list *);
int		simple_execution(char *exe, char **args, t_list *env);
int		binary_tree_exe(t_node *tree, t_list *env);
char		**get_path(t_point *path);
char            *find_pathway(char **pathway, char *ex);
void		print_exit_message(int status);

/* PTR FUNCTION */

int		doubl_ind(t_node *tree, t_list *env);
int		ind(t_node *tree, t_list *env);
int		redir(t_node *tree, t_list *env);
int		doubl_redir(t_node *tree, t_list *env);
int		my_pipe(t_node *tree, t_list *env);
int		double_pipe(t_node *tree, t_list *env);
int		double_and(t_node *tree, t_list *env);

# endif
