/*
** tree.h for Project-Master in /home/thomas/epitech/PSU_2016_42sh
**
** Made by thibault thomas
** Login   <thibault.thomas@epitech.eu>
**
** Started on  Wed May 10 13:20:22 2017 thibault thomas
** Last update Sun May 21 21:07:26 2017 
*/

#ifndef TREE_H_
# define TREE_H_

typedef enum
  {
    LREDIR,
    D_LREDIR,
    RREDIR,
    D_RREDIR,
    PIPE,
    D_PIPE,
    COMMAND,
    NONE,
    AND,
  }t_type;

# define IS_REDIRECT(value)     ((value >= 0) && (value <= 3))
# define IS_PIPE(value)         ((value == 4) || (value == 5))

typedef struct	s_node
{
  char		*cmd;
  char		*path;
  char		**args;
  t_type	type;
  struct s_node	*left;
  struct s_node *right;
}		t_node;

t_node	*create_node(char *);
t_node	*nd_lft(t_node *, char *);
t_node	*nd_rgt(t_node *, char *);
t_node	*get_tree(char *);
int	get_next_separator(char *);
void	print_tree(t_node *);
void	get_type(t_node *);
int     check_all_nodes(t_node *);
int     is_node_valid(t_node *);
int     is_null_command(t_node *);
int     has_missing_redirect(t_node *);
int     is_ambigious_redirect(t_node *);

#endif
