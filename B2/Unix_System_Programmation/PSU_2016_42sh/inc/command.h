/*
** command.h for Project-Master in /home/thomas/epitech/PSU_2016_42sh
**
** Made by thibault thomas
** Login   <thibault.thomas@epitech.eu>
**
** Started on  Mon May 15 16:01:49 2017 thibault thomas
** Last update Sun May 21 16:28:00 2017 thibault thomas
*/

#ifndef COMMAND_H_
# define COMMAND_H_

# include "env.h"
# include "list.h"

int	g_error;

int	user_input(t_list *);
int	handle_input(char *, t_list *);
void	print_prompt();
t_list	*get_tree_list(char **);

#endif
