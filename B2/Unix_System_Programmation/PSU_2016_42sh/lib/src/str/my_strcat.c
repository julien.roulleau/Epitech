/*
** merge_str.c for src in /home/infocraft/Job/Lib/src/str/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Jan 18 00:05:37 2017 Roulleau Julien
** Last update Sun Apr 16 01:54:28 2017 Roulleau Julien
*/

#include <stdlib.h>

int		my_strlen(char *);

char		*my_strcat(char *str, char *add)
{
	char	*new_str;
	int	i;
	int	n;

	i = -1;
	n = -1;
	if (!(new_str = malloc(sizeof(char) *
		(my_strlen(str) + my_strlen(add) + 1))))
		return (0);
	while (str[++i])
		new_str[++n] = str[i];
	i = -1;
	while (add[++i])
		new_str[++n] = add[i];
	new_str[++n] = 0;
	return (new_str);
}
