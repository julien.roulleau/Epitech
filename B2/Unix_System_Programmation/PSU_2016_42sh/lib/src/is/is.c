/*
** is.c for is.c in /home/na/Dropbox/Job/Lib/src/is/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Feb 22 16:41:31 2017 Roulleau Julien
** Last update Fri May 12 17:26:29 2017 Roulleau Julien
*/

#include "src.h"

int is_alpha_up(char *str)
{
	int	i;

	i = -1;
	while (str[++i])
		if (!IS_ALPHA_UP(str[i]))
			return (0);
	return (1);
}

int is_alpha_down(char *str)
{
	int	i;

	i = -1;
	while (str[++i])
		if (!IS_ALPHA_DOWN(str[i]))
			return (0);
	return (1);
}

int is_num(char *str)
{
	int	i;

	i = -1;
	while (str[++i])
		if (!IS_NUM(str[i]))
			return (0);
	return (1);
}

int is_alpha(char *str)
{
	int	i;

	i = -1;
	while (str[++i])
		if (!IS_ALPHA(str[i]))
			return (0);
	return (1);
}

int is_alpha_num(char *str)
{
	int	i;

	i = -1;
	while (str[++i])
		if (!IS_ALPHA_NUM(str[i]))
			return (0);
	return (1);
}
