/*
** print_type_base.c for my_printf in /home/infocraft/Job/PSU_2016_my_printf/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Nov 16 20:59:08 2016 Roulleau Julien
** Last update Mon Jan  9 00:42:01 2017 Roulleau Julien
*/

#include "my_printf.h"

void		my_print_base_decimal(va_list va)
{
	int	nb;

	nb = va_arg(va, int);
	my_putnbr_base(nb, "0123456789");
}

void		my_print_base_hexdown(va_list va)
{
	int	nb;

	nb = va_arg(va, int);
	my_putnbr_base_unsigned(nb, "0123456789abcdef");
}

void		my_print_base_hexup(va_list va)
{
	int	nb;

	nb = va_arg(va, int);
	my_putnbr_base_unsigned(nb, "0123456789ABCDEF");
}

void		my_print_base_octal(va_list va)
{
	int	nb;

	nb = va_arg(va, int);
	my_putnbr_base(nb, "01234567");
}

void		my_print_base_binary(va_list va)
{
	int	nb;

	nb = va_arg(va, int);
	my_putnbr_base(nb, "01");
}
