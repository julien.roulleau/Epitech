/*
** change_env.c for 42sh in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri May 19 00:30:48 2017 Roulleau Julien
** Last update Sun May 21 11:08:45 2017 Roulleau Julien
*/

#include "src.h"
#include "env.h"

static t_env	*set_index(char *name, char *value)
{
	t_env	*env;

	if (!(env = malloc(sizeof(t_env))))
		return (0);
	env->name = my_strdup(name);
	env->value = my_strdup(value);
	return (env);
}

t_point		*find_in_env(t_point *env, char *name)
{
	t_env	*index;

	while (env)
	{
		index = env->data;
		if (my_strcmp(index->name, name))
			break;
		env = env->next;
	}
	return (env);
}

int		add_in_env(t_list *env, char *name, char *value)
{
	t_point	*node;
	t_env	*index;

	if (!(node = find_in_env(env->first, name)))
	{
		if (!(index = set_index(name, value)))
			return (0);
		add_last_node(env, index);
	}
	else
	{
		index = node->data;
		free(index->value);
		index->value = value;
	}
	return (1);
}

int		dell_in_env(t_list *env, char *name)
{
	t_point	*node;

	if ((node = find_in_env(env->first, name)))
	{
		free(node->data);
		free_node(env, node);
	}
	return (1);
}
