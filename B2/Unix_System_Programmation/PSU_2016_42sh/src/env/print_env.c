/*
** print_env.c for print_env.c in /home/jack-dan/exos/PSU_2016_42sh/src/env
**
** Made by Jack-Dan
** Login   <jack-dan@epitech.net>
**
** Started on  Fri May 19 10:05:21 2017 Jack-Dan
** Last update Fri May 19 11:28:35 2017 Roulleau Julien
*/

#include <stdio.h>
#include "env.h"

int	print_env(t_point *env)
{
  t_env	*index;

  while (env)
    {
      index = env->data;
      printf("%s=%s\n", index->name, index->value);
      env = env->next;
    }
  return (0);
}
