/*
** set_list.c for 42sh in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri May 19 00:08:11 2017 Roulleau Julien
** Last update Tue May 30 11:36:33 2017 Roulleau Julien
*/

#include "src.h"
#include "env.h"

static t_env	*set_index(char *index)
{
	char	**tab;
	t_env	*env;

	if (!(env = malloc(sizeof(t_env))))
		return (0);
	if (!(tab = str_to_wordtab(index, '=')))
		return (0);
	env->name = my_strdup(tab[0]);
	if (!tab[0])
		return (0);
	if (!tab[1])
		env->value = my_strdup("");
	else
		env->value = my_strdup(tab[1]);
	free_tab(tab);
	return (env);
}

static int	set_null_env(t_list *list)
{
	char	*pwd;

	if (!(pwd = malloc(sizeof(char) * 1000)))
		return (0);
	getcwd(pwd, 999);
	if (!add_last_node(list, set_index(my_strcat("PWD=", pwd))))
		return (0);
	if (!add_last_node(list, set_index(my_strcat("OLDPWD=", pwd))))
		return (0);
	if (!add_last_node(list, set_index(my_strcat("HOME=", "/"))))
		return (0);
	if (!add_last_node(list, set_index(my_strcat("PATH=", "/"))))
		return (0);
	return (1);
}

t_list		*init_env(char **env)
{
	t_list	*list;
	t_env	*index;
	int	i;

	if (!(list = create_list(LINKED)))
		return (0);
	i = -1;
	if (!env[0])
	{
		if (!set_null_env(list))
			return (0);
		return (list);
	}
	while (env[++i])
	{
		if (!(index = set_index(env[i])))
			return (0);
		add_last_node(list, index);
	}
	return (list);
}

char		**tab_env(t_list *env)
{
	char 	**tab;
	t_env	*index;
	int	i;

	if (!(tab = malloc(sizeof(char *) * (env->size + 1))))
		return (0);
	i = -1;
	while (++i < env->size)
	{
		index = env->first->data;
		if (!(tab[i] = my_strcat(index->name,
				my_strcat("=", index->value))))
			return (0);
		free_node(env, env->first);
	}
	tab[env->size] = 0;
	return (tab);
}
