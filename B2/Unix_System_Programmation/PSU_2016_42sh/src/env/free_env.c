/*
** free_env.c for 42sh in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sat May 20 18:11:58 2017 Roulleau Julien
** Last update Sat May 20 18:16:44 2017 Roulleau Julien
*/

#include "src.h"
#include "env.h"

static int	free_index(t_point *env)
{
	t_env	*index;

	if (env->next)
		free_index(env->next);
	index = env->data;
	free(index->name);
	free(index->value);
	free(index);
	return (1);
}

int		free_env(t_list *env)
{
	free_index(env->first);
	free_list(env);
	return (0);
}
