/*
** get_type.c for Project-Master in /home/thomas/epitech/PSU_2016_42sh
** 
** Made by thibault thomas
** Login   <thibault.thomas@epitech.eu>
** 
** Started on  Wed May 17 15:57:18 2017 thibault thomas
** Last update Wed May 17 16:52:56 2017 thibault thomas
*/

#include <string.h>
#include <stdlib.h>
#include "tree.h"

static t_type	get_f_type(char *);
static t_type	get_s_type(char *);

void	get_type(t_node *node)
{
  if (node->args == NULL)
    node->type = get_f_type(node->cmd);
  else
    node->type = COMMAND;
}

static t_type get_f_type(char *cmd)
{
  if (!strcmp(cmd, "|"))
    return (PIPE);
  if (!strcmp(cmd, "<"))
    return (LREDIR);
  if (!strcmp(cmd, "<<"))
    return (D_LREDIR);
  return (get_s_type(cmd));
}

static t_type	get_s_type(char *cmd)
{
  if (!strcmp(cmd, ">"))
    return (RREDIR);
  if (!strcmp(cmd, ">>"))
    return (D_RREDIR);
  if (!strcmp(cmd, "|"))
    return (D_PIPE);
  return (NONE);
}
