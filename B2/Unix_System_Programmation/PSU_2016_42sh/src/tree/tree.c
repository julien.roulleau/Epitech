/*
** tree.c for Project-Master in /home/thomas/epitech/PSU_2016_minishell2
** 
** Made by thibault thomas
** Login   <thibault.thomas@epitech.eu>
** 
** Started on  Thu Apr  6 15:43:54 2017 thibault thomas
** Last update Wed May 17 16:11:25 2017 thibault thomas
*/

#include <string.h>
#include <stdio.h>
#include "format.h"
#include "tree.h"
#include "utils.h"

t_node	*create_node(char *command)
{
  t_node	*node;

  if (strlen(command) == 0)
    return (NULL);
  node = xmalloc(sizeof(*node));
  node->cmd = remove_quotes(strdup(command));
  if (*command != '>' && *command != '<' && *command != '|')
    {
      node->args = args_to_wordtab(command, ' ');
      remove_all_quotes(node);
    }
  get_type(node);
  node->left = NULL;
  node->right = NULL;
  return (node);
}

t_node	*nd_lft(t_node *tree, char *command)
{
  t_node	*next;

  next = create_node(command);
  if (tree == NULL || next == NULL)
    return (next);
  while (tree->right != NULL)
    tree = tree->right;
  tree->left = next;
  return (next);
}

t_node	*nd_rgt(t_node *tree, char *command)
{
  t_node	*new;

  new = create_node(command);
  if (tree == NULL || new == NULL)
    return (new);
  while (tree->right != NULL)
    tree = tree->right;
  tree->right = new;
  return (new);
}

void	print_tree(t_node *tree)
{
  if (tree == NULL)
    return;
  if (tree->left)
    print_tree(tree->left);
  printf("Clé = :%s:\n", tree->cmd);
  if (tree->right)
    print_tree(tree->right);
}
