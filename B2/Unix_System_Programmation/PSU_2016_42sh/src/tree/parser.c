/*
** parser.c for Project-Master in /home/thomas/epitech/PSU_2016_minishell2
** 
** Made by thibault thomas
** Login   <thibault.thomas@epitech.eu>
** 
** Started on  Thu Apr  6 15:53:11 2017 thibault thomas
** Last update Tue May 16 14:40:25 2017 thibault thomas
*/

#include "utils.h"
#include "src.h"
#include "format.h"
#include <string.h>
#include <stdio.h>

const char	*g_separator[] =
  {"|", "||", ">", ">>", "<<", "<", 0};

t_node	*get_tree(char *str)
{
  int		sep;
  char		*cmd;
  t_node	*tree;
  char		*separator;

  tree = NULL;
  str = format_str(str);
  while ((sep = get_next_separator(str)) != -1 && strlen(str) > 0)
    {
      cmd = NULL;
      if (sep > 0)
	cmd = strncpy(calloc(sizeof(char), sep + 1), str, sep);
      separator = calloc(sizeof(char), 3);
      str = str + sep;
      separator = format_str(strncpy(separator, str, 2));
      tree == NULL ? tree = nd_rgt(tree, separator) : nd_rgt(tree, separator);
      if (cmd != NULL)
	nd_lft(tree, format_str(cmd));
      str = format_str(str + my_strlen(separator));
      free(separator);
      free(cmd);
    }
  tree == NULL ? tree = nd_lft(tree, str): nd_rgt(tree, str);
  return (tree);
}

int	get_next_separator(char *str)
{
  char	**array;
  int	i;
  int	z;

  array = args_to_wordtab(str, ' ');
  i = 0;
  z = 0;
  while (array[i])
    {
      if (*array[i] == '&' || *array[i] == '|' || *array[i] == '<' ||
	  *array[i] == '>')
	{
	  free_wordtab(array);
	  return (z);
	}
      z += my_strlen(array[i]) + 1;
      i++;
    }
  free_wordtab(array);
  return (-1);
}
