/*
** recognize_error.c for  in /home/thomas/epitech/PSU_2016_42sh
**
** Made by
** Login   <thibault.thomas@epitech.eu>
**
** Started on  Thu May 18 18:14:57 2017
** Last update Sun May 21 21:46:34 2017 thibault thomas
*/

#include <stdio.h>
#include <stdlib.h>
#include "tree.h"

int	check_all_nodes(t_node *node)
{
  if (node == NULL)
    return (1);
  if (node->left && !check_all_nodes(node->left))
    	return (0);
  if (is_node_valid(node) == 0)
    return (0);
  if (node->right && !check_all_nodes(node->right))
    	return (0);
  return (1);
}

int	is_node_valid(t_node *tree)
{
  if (tree->type != COMMAND)
    {
      if (IS_REDIRECT(tree->type) && has_missing_redirect(tree))
	return (0);
      else if ((tree->type == PIPE || tree->type == D_PIPE)
          && is_null_command(tree))
	return (0);
      else if (is_ambigious_redirect(tree))
	return (0);
    }
  return (1);
}

int	has_missing_redirect(t_node *node)
{
  int	i;

  i = 0;
  if (node->right == NULL)
    i = 1;
  else if (node->left != NULL && node->left->type != COMMAND)
    i = 1;
  if (i == 1)
    printf("Missing name for redirect.\n");
  return (i);
}

int	is_null_command(t_node *node)
{
  int	i;

  i = 0;
  if (node->right == NULL)
    i = 1;
  else if (node->right->type != COMMAND && node->right->left == NULL)
    i = 1;
  if (i == 1)
    printf("Invalid null command.\n");
  return (i);
}

int	is_ambigious_redirect(t_node *node)
{
  int	i;

  i = 0;
  if (IS_PIPE(node->type) && node->right &&
      (node->right->type == LREDIR || node->right->type == D_LREDIR) &&
      node->right->left != NULL && node->right->right != NULL)
    {
      printf("Ambiguous input redirect.\n");
      i = 1;
    }
  else if ((node->type == RREDIR || node->type == D_RREDIR)
	   && node->right && node->right->type == PIPE)
    {
      i = 1;
      printf("Ambiguous output redirect.\n");
    }
  return (i);
}
