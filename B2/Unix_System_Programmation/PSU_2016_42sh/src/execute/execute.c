/*
** eecute.c for exe in /home/benjamin/chikho_b/rendu/PSU/PSU_2016_42sh/src/execute
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Mon May 15 15:21:20 2017 Benjamin
** Last update Tue May 30 11:46:35 2017 Roulleau Julien
*/

# include <unistd.h>
# include <string.h>
# include <stdio.h>
# include "src.h"
# include "env.h"
# include "exe.h"
# include "tree.h"
# include "utils.h"
# include "command.h"

static char	*my_super_strdup(char *s1, char *s2, char *e)
{
  int		i;
  int		j;
  char		*new;

  if ((new = malloc(sizeof(char) *
		    (strlen(s1) + strlen(s2) + strlen(e) + 1))) == NULL)
    return (NULL);
  j = -1;
  i = -1;
  while (s1[++i])
    new[++j] = s1[i];
  i = -1;
  while (e[++i])
    new[++j] = e[i];
  i = -1;
  while (s2[++i])
    new[++j] = s2[i];
  new[++j] = '\0';
  return (new);
}

char		*find_pathway_next(char *exe, char *ex)
{
  char		*buf;
  char		*pwd;

  if ((buf = malloc(sizeof(char) * (4096))) == NULL)
    return (NULL);
  if (ex && ex[0] == '.' && ex[1] == '/')
    {
      getcwd(buf, 4095);
      pwd = my_super_strdup(buf, ex + 2, "/");
      return (pwd);
    }
  if (ex && ex[0] == '/')
      return (ex);
  if (access(exe, F_OK) == -1)
    {
      printf("%s: Command not found.\n", ex);
      g_error = 1;
      return (NULL);
    }
  return (exe);
}

char		*find_pathway(char **pathway, char *ex)
{
  char		*exe;
  char		**tab;
  char		buf[1056];
  int		i;

  if (!(i = 0) && pathway[i])
    exe = my_super_strdup(pathway[i], ex, "/");
  while (pathway[++i] && (access(exe, F_OK)) == -1)
    {
      free(exe);
      exe = my_super_strdup(pathway[i], ex, "/");
    }
  if (pathway[i] == NULL)
    {
      confstr(_CS_PATH, buf, 1056);
      tab = str_to_wordtab(buf, ':');
      i = -1;
      while (tab[++i])
	{
	  exe = my_super_strdup(tab[i], ex, "/");
	  if ((access(exe, F_OK)) == 0)
	    return (exe);
	}
    }
  return (find_pathway_next(exe, ex));
}

char		**get_path(t_point *path)
{
  t_env		*env;

  if (!path)
    return (0);
  if (path && path->data)
    env = path->data;
  else
    return (NULL);
  return (str_to_wordtab(env->value, ':'));
}

int		execute_cmd(t_node *tree, t_list *env)
{
  char		**pathway;
  char		*exe;
  int		tmp;

  exe = 0;
  if (tree->left == NULL && tree->right == NULL)
    {
      if (!(pathway = get_path(find_in_env(env->first, "PATH"))))
        return (0);
      if ((tmp = execute_builtins(tree, env)) == -1)
	if ((exe = find_pathway(pathway, tree->args[0])) == NULL)
	  {
	    free_wordtab(pathway);
	    return (0);
	  }
      if (tmp == -1)
	simple_execution(exe, tree->args, env);
      free_tab(pathway);
    }
  else
    binary_tree_exe(tree, env);
  return (0);
}
