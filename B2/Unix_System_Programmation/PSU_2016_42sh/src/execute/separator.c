/*
** separator.c for  in /home/zak/Documents/Modules/PSU/PSU_2016_42sh/src/execute
**
** Made by david zakrzewski
** Login   <zak@epitech.net>
**
** Started on  Sun May 21 16:32:52 2017 david zakrzewski
** Last update Sun May 21 17:11:32 2017 david zakrzewski
*/

# include "tree.h"
# include "src.h"
# include "env.h"
# include "exe.h"
# include "command.h"

int		double_and(t_node *tree, t_list *env)
{
  execute_cmd(tree->left, env);
  if (g_error == 0)
    execute_cmd(tree->right, env);
  return (0);
}

int		double_pipe(t_node *tree, t_list *env)
{
  execute_cmd(tree->left, env);
  if (g_error != 0)
    execute_cmd(tree->right, env);
  return (0);
}
