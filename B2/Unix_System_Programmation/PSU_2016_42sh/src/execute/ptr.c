/*
** ptr.c for ptr.c in /home/benjamin/chikho_b/rendu/PSU/PSU_2016_42sh/src
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Wed May 17 16:07:33 2017 Benjamin
** Last update Sun May 21 16:47:32 2017 Benjamin
*/

# include <fcntl.h>
# include <unistd.h>
# include <string.h>
# include <stdio.h>
# include "tree.h"
# include "src.h"
# include "env.h"
# include "exe.h"

int		redirect_input(char *name, t_node *tree, t_list *env)
{
  int		fd;
  int		old_fd;

  if ((fd = open(name, O_RDONLY)) == -1)
    return (-1);
  if ((old_fd = dup(0)) == -1)
    return (-1);
  if ((dup2(fd, 0)) == -1)
    return (-1);
  if ((execute_cmd(tree->left, env)) == -1)
    return (-1);
  if ((dup2(old_fd, 0)) == -1)
    return (-1);
  return (0);
}

int		doubl_ind(t_node *tree, t_list *env)
{
  int		fd;
  char		*buf;

  if ((fd = open("/tmp/.BallonDeau.ss",
		 O_CREAT | O_WRONLY | O_TRUNC, 0700)) == -1)
    return (-1);
  write(1, "> ", 2);
  if ((buf = get_next_line(0)) == NULL)
    return (-1);
  while ((strcmp(buf, tree->right->cmd)) != 0)
    {
      write(fd, buf, strlen(buf));
      write(fd, "\n", 1);
      write(1, "> ", 2);
      if ((buf = get_next_line(0)) == NULL)
	return (-1);
    }
  close(fd);
  if ((redirect_input("/tmp/.BallonDeau.ss", tree, env)) == -1)
    return (-1);
  remove("/tmp/.BallonDeau.ss");
  return (0);
}

int		ind(t_node *tree, t_list *env)
{
  int		fd;
  int		old_fd;

  if (tree->right == NULL || tree->left == NULL)
    return (-1);
  if ((fd = open(tree->right->cmd, O_RDONLY)) == -1)
    return (-1);
  if ((old_fd = dup(0)) == -1)
    return (-1);
  if ((dup2(fd, 0)) == -1)
    return (-1);
  if ((execute_cmd(tree->left, env)) == -1)
    return (-1);
  if ((dup2(old_fd, 0)) == -1)
    return (-1);
  return (0);
}

int		redir(t_node *tree, t_list *env)
{
  int		fd;
  int		old_fd;

  if (tree->right == NULL || tree->left == NULL)
    return (-1);
  if ((fd = open(tree->right->cmd, O_RDWR | O_CREAT | O_TRUNC, 0600)) == -1)
    return (-1);
  if ((old_fd = dup(1)) == -1)
    return (-1);
  if ((dup2(fd, 1)) == -1)
    return (-1);
  if ((execute_cmd(tree->left, env)) == -1)
    return (-1);
  if ((dup2(old_fd, 1)) == -1)
    return (-1);
  return (0);
}

int		doubl_redir(t_node *tree, t_list *env)
{
  int		fd;
  int		old_fd;

  if (tree->right == NULL || tree->left == NULL)
    return (-1);
  if ((fd = open(tree->right->cmd, O_RDWR | O_CREAT | O_APPEND, 0600)) == -1)
    return (-1);
  if ((old_fd = dup(1)) == -1)
    return (-1);
  if ((dup2(fd, 1)) == -1)
    return (-1);
  if ((execute_cmd(tree->left, env)) == -1)
    return (-1);
  if ((dup2(old_fd, 1)) == -1)
    return (-1);
  return (0);
}
