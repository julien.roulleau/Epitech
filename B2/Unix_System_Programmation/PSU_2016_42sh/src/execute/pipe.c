/*
** pipe.c for test in /home/benjamin/chikho_b/rendu/PSU/PSU_2016_42sh/src/execute
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Mon May 15 15:24:19 2017 Benjamin
** Last update Sun May 21 19:49:11 2017 Benjamin
*/

# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include <sys/types.h>
# include <sys/wait.h>
# include "command.h"
# include "tree.h"
# include "env.h"
# include "exe.h"
# include "src.h"

static int	print_err(int pipefd[])
{
  char		*buff;

  while ((buff = get_next_line(pipefd[0])) != NULL)
    printf("%s\n", buff);
  return (0);
}

static int	godfather(int pid, int pipefd[2], t_node *tree, t_list *env)
{
  int		status;
  int		save;

  close(pipefd[1]);
  save = dup(0);
  if (dup2(pipefd[0], 0) == -1)
    return (-1);
  if (waitpid(pid, &status, WUNTRACED | WCONTINUED) == -1)
    return (-1);
  if (!WEXITSTATUS(status) &&
      execute_cmd(tree->right, env) == -1)
    return (-1);
  else
    print_err(pipefd);
  if (dup2(save, 0) == -1)
    return (-1);
  close(pipefd[0]);
  g_error = WEXITSTATUS(status);
  return (0);
}

int		my_pipe(t_node *tree, t_list *env)
{
  int		pipefd[2];
  int		pid;
  int		save;

  pipe(pipefd);
  if ((pid = fork()) == -1)
    return (-1);
  if (pid == 0)
    {
      close(pipefd[0]);
      save = dup(1);
      if (dup2(pipefd[1], 1) == -1)
	return (-1);
      if (execute_cmd(tree->left, env) == -1)
	exit(-1);
      if (dup2(save, 1) == -1)
	return (-1);
      close(pipefd[1]);
      exit(g_error);
    }
  else if ((godfather(pid, pipefd, tree, env)) == -1)
    return (-1);
  return (0);
}
