/*
** redirection.c for red in /home/benjamin/chikho_b/rendu/PSU/PSU_2016_42sh/src/execute
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Wed May 17 15:50:15 2017 Benjamin
** Last update Sun May 21 18:20:32 2017 thibault thomas
*/

# include <stdio.h>
# include <sys/types.h>
# include <sys/wait.h>
# include "src.h"
# include "env.h"
# include "tree.h"
# include "utils.h"
# include "command.h"

void	print_exit_message(int status)
{
  if (status == 136 || status == 139 || status == 135)
    {
      if (status == 139)
	printf("Segmentation fault");
      if (status == 136)
	printf("Floating exception");
      if (status == 135)
	printf("Bus error");
      if (WCOREDUMP(status))
	printf(" (core dumped)");
      printf("\n");
      g_error = status;
    }
}
