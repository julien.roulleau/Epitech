/*
** exe.c for exe in /home/benjamin/chikho_b/rendu/PSU/PSU_2016_42sh/src/execute
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Wed May 17 15:17:39 2017 Benjamin
** Last update Sun May 21 19:11:13 2017 Benjamin
*/

# include <sys/types.h>
# include <sys/wait.h>
# include <string.h>
# include <stdio.h>
# include "src.h"
# include "command.h"
# include "env.h"
# include "exe.h"
# include "tree.h"
# include "utils.h"

t_ptr		ptr[] =
  {
    {"<<", &doubl_ind},
    {"<", &ind},
    {">", &redir},
    {">>", &doubl_redir},
    {"|", &my_pipe},
    {"||", &double_pipe},
    {"&&", &double_and},
    {0, NULL}
  };

t_builtin		built[] =
  {
    {"echo", &echo},
    {"cd", &cd},
    {"env", &my_env},
    {"setenv", &set_env},
    {"unsetenv", &unset_env},
    {"exit", &my_exit},
    {0, NULL}
  };

int		simple_execution(char *exe, char **args, t_list *env)
{
  char		**tab;
  int 		status;
  int		pid;

  pid = fork();
  if (pid == 0)
    {
      tab = tab_env(env);
      status = execve(exe, args, tab);
      free_wordtab(tab);
      if (status == -1)
	printf("%s: Permission denied.\n", args[0]);
      exit(status);
    }
  else
    {
      if (waitpid(pid, &status, WUNTRACED | WCONTINUED) == -1)
	return (-1);
      if ((g_error = WEXITSTATUS(status)) == -1)
	g_error = 1;
      print_exit_message(status);
    }
  return (0);
}

int		execute_builtins(t_node *tree, t_list *env)
{
  int		i;

  i = -1;
  while (built[++i].ptr)
    {
      if ((strcmp(built[i].flag, tree->args[0])) == 0)
	{
	  built[i].ptr(env, tree->args);
	  return (0);
	}
    }
  return (-1);
}

int		binary_tree_exe(t_node *tree, t_list *env)
{
  int		i;

  i = -1;
  while (ptr[++i].ptr)
    {
      if ((strcmp(ptr[i].flag, tree->cmd)) == 0)
	if ((ptr[i].ptr(tree, env)) == -1)
	  return (-1);
    }
  return (0);
}
