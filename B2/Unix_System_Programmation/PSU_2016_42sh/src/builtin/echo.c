/*
** echo.c for 42sh in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri May 19 10:45:23 2017 Roulleau Julien
** Last update Sun May 21 18:20:23 2017 Roulleau Julien
*/

#include "env.h"
#include "src.h"
#include "command.h"

static int	special_echo(char *arg)
{
	if (my_strcmp(arg, "$?"))
	{
		my_putnbr(g_error);
		return (0);
	}
	my_putstr(arg);
	return (1);
}

int		echo(t_list *env, char **arg)
{
	int	opt;
	int	i;

	(void) env;
	if (!arg[1])
	{
		g_error = 0;
		my_putstr("\n");
		return (0);
	}
	i = my_strcmp(arg[1], "-n");
	opt = i > 0 ? 1 : 0;
	while (arg[++i])
	{
		special_echo(arg[i]);
		g_error = 0;
		if (arg[i + 1])
			my_putstr(" ");
	}
	if (!opt)
		my_putstr("\n");
	return (1);
}
