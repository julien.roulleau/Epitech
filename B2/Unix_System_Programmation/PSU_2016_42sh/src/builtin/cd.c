/*
** cd.c for 42sh in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri May 19 10:46:19 2017 Roulleau Julien
** Last update Sun May 21 18:39:20 2017 Benjamin
*/

#include <unistd.h>
#include "command.h"
#include "env.h"
#include "src.h"

#define ERR_FD ": No such file or directory.\n"
#define ERR_D ": Not a directory.\n"
#define NOT(fold) (my_putstr(fold), PRINT(ERR_FD), g_error = 1)
#define NOT_D(fold) (my_putstr(fold), PRINT(ERR_D), g_error = 1)

static char	*set_path(t_list *env, char *find)
{
  t_point	*point;
  t_env	*index;

  point = find_in_env(env->first, find);
  index = point->data;
  return (index->value);
}

int		cd(t_list *env, char **args)
{
  char	*pwd;
  char	*path;

  g_error = 0;
  if (!(pwd = malloc(sizeof(char) * 1000)))
    return (0);
  getcwd(pwd, 999);
  if (args[1] == NULL)
    path = set_path(env, "HOME");
  else if (args[1][0] == '~')
    path = my_strcat(set_path(env, "HOME"), args[1] + 1);
  else if (my_strcmp(args[1], "-"))
    path = set_path(env, "OLDPWD");
  else if (args[1][0] == '/')
    path = args[1];
  else
    path = my_strcat(pwd, my_strcat("/", args[1]));
  add_in_env(env, "OLDPWD", my_strdup(pwd));
  if (chdir(path))
    access(path, F_OK) ? NOT(args[1]) : NOT_D(args[1]);
  getcwd(pwd, 999);
  add_in_env(env, "PWD", my_strdup(pwd));
  free(pwd);
  return (1);
}
