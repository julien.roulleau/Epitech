/*
** env.c for 42sh in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri May 19 10:01:16 2017 Roulleau Julien
** Last update Fri May 19 11:45:21 2017 Roulleau Julien
*/

#include "env.h"
#include "src.h"
#include "command.h"

#define ERR_AN "setenv: Variable name must contain alphanumeric characters.\n"
#define ERR_L "setenv: Variable name must begin with a letter.\n"
#define ERR_TFA "unsetenv: Too few arguments.\n"

int		err_env(char **arg)
{
	if (!is_alpha_num(arg[1]))
	{
		my_putstr(ERR_AN);
		g_error = 1;
		return (0);
	}
	if (!IS_ALPHA(arg[1][0]))
	{
		my_putstr(ERR_L);
		g_error = 1;
		return (0);
	}
	return (1);
}

int 		my_env(t_list *env, char **arg)
{
	(void) arg;
	print_env(env->first);
	g_error = 0;
	return (1);
}

int		set_env(t_list *env, char **arg)
{
	char 	*value;

	if (!arg[0] || !arg[1])
	{
		my_env(env, arg);
		return (0);
	}
	if (!err_env(arg))
		return (0);
	if (!arg[2])
		value = my_strdup("\0");
	else
		value = arg[2];
	if (!add_in_env(env, arg[1], value))
		return (0);
	g_error = 0;
	return (1);
}

int		unset_env(t_list *env, char **arg)
{
	if (!arg[0] || !arg[1])
	{
		g_error = 1;
		my_putstr(ERR_TFA);
		return (0);
	}
	if (!dell_in_env(env, arg[1]))
		return (0);
	g_error = 0;
	return (1);
}
