/*
** exit.c for 42sh in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri May 19 10:39:09 2017 Roulleau Julien
** Last update Sat May 20 18:21:25 2017 Roulleau Julien
*/

#include "src.h"
#include "env.h"
#include "command.h"

static void 	for_exit(t_list *env, int nbr)
{
	free_env(env);
	exit(nbr);
}

int		my_exit(t_list *env, char **arg)
{
	if (arg[0] && arg[1])
	{
		if (!(is_num(arg[1])))
		{
			my_putstr("exit: Expression Syntax.");
			return (0);
		}
		else
			for_exit(env, my_getnbr(arg[1]));
	}
	else
		for_exit(env, g_error);
	return (1);
}
