/*
** memutils.c for Project-Master in /home/thomas/epitech/PSU_2016_42sh
** 
** Made by thibault thomas
** Login   <thibault.thomas@epitech.eu>
** 
** Started on  Wed Apr 19 15:22:47 2017 thibault thomas
** Last update Sun May 21 16:32:39 2017 thibault thomas
*/

#include <stdlib.h>
#include "utils.h"
#include "src.h"
#include "list.h"

void	free_wordtab(char **array)
{
  int	i;

  i = -1;
  while (array[++i])
    {
      free(array[i]);
    }
  free(array);
}

void	free_tree(t_node *node)
{
  t_node	*right;

  if (!node)
    return;
  if (node->left)
    free_tree(node->left);
  right = node->right;
  free(node->cmd);
  if (node->args != NULL)
    free_wordtab(node->args);
  free(node);
  if (right)
    free_tree(right);
}

void	free_list_trees(t_list *trees)
{
  t_point	*cur;
  t_point	*tmp;

  cur = trees->first;
  while (cur != NULL)
    {
      tmp = cur->next;
      free_tree((t_node *) cur->data);
      free(cur);
      cur = tmp;
    }
  free(trees);
}
