/*
** print_tab.c for print_tab.c in /home/benjamin/chikho_b/rendu/PSU/PSU_2016_42sh/src/utils
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Tue May 16 14:33:02 2017 Benjamin
** Last update Tue May 16 16:17:19 2017 Benjamin
*/

# include <stdio.h>

int		print_tab(char **tab)
{
  int		i;

  i = -1;
  while (tab[++i])
    printf("%s\n", tab[i]);
  return (i);
}
