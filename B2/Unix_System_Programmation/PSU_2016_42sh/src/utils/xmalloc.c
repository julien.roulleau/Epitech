/*
** xmalloc.c for Project-Master in /home/thomas/epitech/CPE_2016_corewar/asm
** 
** Made by thibault thomas
** Login   <thibault.thomas@epitech.eu>
** 
** Started on  Tue Mar 14 17:10:33 2017 thibault thomas
** Last update Tue May 16 14:19:22 2017 thibault thomas
*/

#include <stdlib.h>

void		*xmalloc(size_t size)
{
  void		*ptr;
  char		*str;
  size_t	i;

  i = 0;
  ptr = NULL;
  if ((ptr = malloc(size)) == NULL)
    exit(84);
  str = (char *) ptr;
  while (size > i)
    {
      str[i] = 0;
      i++;
    }
  return (ptr);
}
