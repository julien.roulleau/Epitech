/*
** loop.c for Project-Master in /home/thomas/epitech/PSU_2016_42sh
**
** Made by thibault thomas
** Login   <thibault.thomas@epitech.eu>
**
** Started on  Mon May 15 15:34:18 2017 thibault thomas
** Last update Sun May 21 21:46:52 2017 thibault thomas
*/

# include <unistd.h>
# include <string.h>
# include "env.h"
# include "tree.h"
# include "src.h"
# include "format.h"
# include "utils.h"
# include "exe.h"
# include "command.h"

int		user_input(t_list *envs)
{
  char		*str;

  print_prompt();
  while ((str = get_next_line(0)) != NULL)
    {
      str = reset_separator(str);
      str = format_str(str);
      if (str != NULL && strlen(str) > 0 && check_separator(str) != 1)
	handle_input(str, envs);
      free(str);
      print_prompt();
    }
  return (0);
}

int		handle_input(char *str, t_list *envs)
{
  char		**array;
  t_list	*trees;
  t_point	*cur;

  array = args_to_wordtab(str, ';');
  if ((trees = get_tree_list(array)))
    {
      cur = trees->first;
      while (cur != NULL)
	{
	  execute_cmd((t_node *) cur->data, envs);
	  cur = cur->next;
	}
      free_list_trees(trees);
    }
  free_wordtab(array);
  return (0);
}

t_list	*get_tree_list(char **array)
{
  t_list	*trees;
  int		i;
  t_node	*tree;

  if (!(trees = create_list(LINKED)))
    return (0);
  i = -1;
  while (array[++i])
    {
      array[i] = remove_char(array[i], ';');
      tree = get_tree(array[i]);
      if (check_all_nodes(tree) == 0)
      	{
	  g_error = 1;
	  free_tree(tree);
	  free_list_trees(trees);
	  return (0);
	}
      if (!(add_last_node(trees, tree)))
	return (0);
    }
  return (trees);
}

void	print_prompt()
{
  if (isatty(0))
    my_printf("> ");
}
