/*
** main.c for lib in /home/na/Dropbox/Job/Lib/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Apr 13 20:01:48 2017 Roulleau Julien
** Last update Sun May 21 18:01:00 2017 thibault thomas
*/

#include "src.h"
#include "env.h"
#include "utils.h"
#include "format.h"
#include "tree.h"
#include "command.h"
#include <stdlib.h>
#include <string.h>

int		main(int argc, char **argv, char **env)
{
  t_list	*list;

  (void) argc;
  (void) argv;
  g_error = 0;
  list = init_env(env);
  user_input(list);
  free_env(list);
  return (g_error);
}
