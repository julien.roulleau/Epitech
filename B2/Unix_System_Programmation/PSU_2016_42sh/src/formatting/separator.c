/*
** separator.c for Project-Master in /home/thomas/epitech/PSU_2016_42sh
** 
** Made by thibault thomas
** Login   <thibault.thomas@epitech.eu>
** 
** Started on  Wed May 10 14:51:40 2017 thibault thomas
** Last update Sat May 20 19:33:57 2017 thibault thomas
*/

#include <string.h>
#include "tree.h"
#include "format.h"
#include "src.h"

static int	is_separator(char);
static int	need_space(char *);
static int	need_sep(char *);

char	*reset_separator(char *old)
{
  int	i;
  char	*str;
  int	d;

  d = 0;
  i = -1;
  if (need_sep(old) == 1)
    {
      str = realloc(old, strlen(old) * 2);
      while (str[++i])
	{
	  if (IS_QUOTE(str[i]))
	    d++;
	  if (d % 2 == 0 && need_space(str + i))
	    insert(str, ++i);
	}
      return (str);
    }
  else
    return (old);
}

void	insert(char *str, int i)
{
  char	*tmp;
  int	l;

  tmp = calloc(sizeof(char), strlen(str) + 4);
  strncpy(tmp, str, i);
  l = strlen(tmp);
  strcpy(tmp + l, " ");
  l += 1;
  strcpy(tmp + l, str + i);
  strcpy(str, tmp);
  free(tmp);
}

static int	is_separator(char c)
{
  return (c == '|' || c == '<' || c == '>' || c == '&' || c == ';');
}

static int	need_space(char *str)
{
  int	i;

  i = 0;
  if ((!is_separator(str[i]) && str[i] != ' '  && is_separator(str[i + 1]))
      || (is_separator(str[i]) &&  str[i + 1] != ' ' &&
	  !is_separator(str[i + 1]))
      || (is_separator(str[i]) && is_separator(str[i + 1]) &&
	  str[i] != str[i + 1]) || (str[i] == ';' && str[i + 1] == '\0'))
    return (1);
  return (0);
}

static int	need_sep(char *str)
{
  int	i;

  i = -1;
  while (str[++i])
    {
      if (need_space(str + i))
	return (1);
    }
  return (0);
}
