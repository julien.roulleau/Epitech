/*
** str_to_args.c for Project-Master in /home/thomas/epitech/PSU_2016_42sh
** 
** Made by thibault thomas
** Login   <thibault.thomas@epitech.eu>
** 
** Started on  Wed May 10 12:47:09 2017 thibault thomas
** Last update Wed May 17 15:55:00 2017 thibault thomas
*/

#include "src.h"
#include "format.h"
#include <stdio.h>

char	**str_to_args(char *args)
{
  char	**array;

  args = format_str(args);
  array = args_to_wordtab(args, ' ');
  if (array == NULL)
    {
      exit(84);
    }
  return (array);
}

void	show_wordtab(char **args)
{
  int	i;

  i = 0;
  while (args[i] != NULL)
    {
      printf("%s\n", args[i++]);
    }
}

void    remove_all_quotes(t_node *node)
{
  int   i;

  i = -1;
  while (node->args[++i])
    node->args[i] = remove_quotes(node->args[i]);
}
