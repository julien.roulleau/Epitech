/*
** format_str.c for Project-Master in /home/thomas/epitech/PSU_2016_42sh
** 
** Made by thibault thomas
** Login   <thibault.thomas@epitech.eu>
** 
** Started on  Fri Apr 14 15:27:55 2017 thibault thomas
** Last update Sun May 21 21:17:51 2017 thibault thomas
*/

#include "src.h"
#include "format.h"

static int	is_separator(char);

char	*format_str(char *str)
{
  int   i;
  int	smpl;

  str = remove_char(str, '\t');
  i = 0;
  smpl = 0;
  while (str[i] && is_separator(str[i]))
    str = decale(str, 0);
  while (str[i])
    {
      while (is_separator(str[i]) && is_separator(str[i + 1]) &&
	     !(smpl % 2))
        str = decale(str, i);
      if (IS_QUOTE(str[i]))
	smpl++;
      i++;
    }
  if (my_strlen(str) - 1 >= 0 && str[my_strlen(str) - 1] == ' ')
    str[my_strlen(str) - 1] = '\0';
  return (str);
}

char     *decale(char *str, int i)
{
  while (str[i])
    {
      if (str[i + 1] == '\t')
        str[i] = ' ';
      else
        str[i] = str[i + 1];
      i++;
    }
  return (str);
}

static int	is_separator(char c)
{
  return (c == '\t' || c == ' ');
}

char	*remove_char(char *str, char c)
{
  int	i;
  int	d;

  i = -1;
  d = 0;
  while (str[++i])
    {
      if (IS_QUOTE(str[i]))
	d++;
      if (d % 2 == 0 && str[i] == c)
	str[i] = ' ';
    }
  return (str);
}
