/*
** check_error.c for Project-Master in /home/thomas/epitech/PSU_2016_42sh
** 
** Made by thibault thomas
** Login   <thibault.thomas@epitech.eu>
** 
** Started on  Mon May 15 15:19:36 2017 thibault thomas
** Last update Tue May 16 13:29:49 2017 thibault thomas
*/

#include "src.h"
#include "format.h"
#include <stdio.h>
#include <string.h>

int	check_separator(char *cmd)
{
  char	*error;
  int	i;

  i = -1;
  if ((error = calloc(sizeof(char), strlen(cmd) + 1)) == NULL)
    return (1);
  while (*cmd)
    {
      if (IS_QUOTE(*cmd))
	error[++i] = *cmd;
      cmd++;
    }
  i = rm_sep(error);
  free(error);
  return (i);
}

int	rm_sep(char *str)
{
  int	i;

  while (*str)
    {
      if (*str != 'a')
	{
	  if ((i = get_sep_pos(str, *str)) == 0)
	    {
	      printf("Unmatched '%c'.\n", *str);
	      return (1);
	    }
	  else
	    while (i != 0)
	      str[i--] = 'a';
	}
      str++;
    }
  return (0);
}

int	get_sep_pos(char *str, char sep)
{
  int	i;

  i = 1;
  while (str[i])
    {
      if (str[i] == sep)
	return (i);
      i++;
    }
  return (0);
}

char	*remove_quotes(char *str)
{
  char	c;
  int	i;

  i = 0;
  c = '\0';
  while (str[i])
    {
      if (c == '\0' && IS_QUOTE(str[i]))
	{
	  c = str[i];
	  str = decale(str, i);
	  i--;
	}
      else if (str[i] == c)
	{
	  str = decale(str, i);
	  c = '\0';
	}
      i++;
    }
  return (str);
}
