/*
** args_to_wordtab.c for Project-Master in /home/thomas/epitech/PSU_2016_my_ls
** 
** Made by thibault thomas
** Login   <thibault@epitech.eu>
** 
** Started on  Wed Nov 30 16:40:27 2016 thibault thomas
** Last update Mon May 15 15:53:26 2017 thibault thomas
*/

#include <stdlib.h>
#include <string.h>
#include "format.h"

static int	get_nextsep(char *, char);
static int	get_size(char *, char);

char	**args_to_wordtab(char *str, char separator)
{
  char	**array;
  int	index;
  int	sep;
  int	size;

  while (*str == separator)
    str++;
  size = get_size(str, separator);
  index = 0;
  if ((array = calloc(sizeof(*array), size + 1)) == NULL)
    return (NULL);
  while (index != size)
    {
      sep = get_nextsep(str, separator);
      if ((array[index] = calloc(sizeof(char), sep + 1)) == NULL)
	return (NULL);
      array[index] = strncpy(array[index], str, sep);
      array[index][(int) sep] = '\0';
      index = index + 1;
      str = str + sep + 1;
    }
  array[index] = 0;
  return (array);
}

static int	get_nextsep(char *str, char sep)
{
  int	m;
  int	z;

  z = 0;
  m = 0;
  while (str[m] != 0)
    {
      if (IS_QUOTE(str[m]))
	z++;
      if (str[m] == sep && str[m + 1] != sep && str[m + 1] != '\0'
	  && z % 2 == 0)
	return (m);
      m += 1;
    }
  return (m);
}

static int	get_size(char *str, char sep)
{
  int	i;
  int	z;

  i = 1;
  z = 0;
  while (*str)
    {
      if (IS_QUOTE(*str))
	z++;
      if (sep == *str && *(str + 1) != sep && *(str + 1) != '\0' &&
	  z % 2 == 0)
	i += 1;
      str += 1;
    }
  return (i);
}
