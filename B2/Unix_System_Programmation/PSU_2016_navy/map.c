/*
** map.c for navy in /home/na/Dropbox/Job/En_Cours/PSU_2016_navy/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Feb  3 15:39:17 2017 Roulleau Julien
** Last update Tue Feb 14 14:21:41 2017 Antoine Falais
*/

#include "map.h"

static void	print_str_separator(char *str, char separator)
{
  int		i;

  i = -1;
  while (str[++i])
    {
      my_putchar(str[i]);
      if (str[i + 1])
	my_putchar(separator);
    }
}

void	set_value(char **map, int x, int y, char c)
{
  map[y][x] = c;
}

char	get_value(char **map, int x, int y)
{
  return (map[y][x]);
}

void	draw_map(char **map)
{
  int	i;
  int	n;
  char	line[MAP_X * 2 + 1];

  my_putstr(" |");
  i = n = -1;
  while (++i < MAP_X && (line[++n] = 'A' + i) && (line[n + 1] = ' '))
    n++;
  line[n] = 0;
  my_putstr(merge_str(line, "\n-+"));
  i = 0;
  while (++i < MAP_X * 2)
    my_putstr("-");
  my_putstr("\n");
  i = -1;
  line[1] = '|';
  line[2] = 0;
  while (++i < MAP_Y)
    {
      line[0] = '1' + i;
      my_putstr(line);
      print_str_separator(map[i], ' ');
      my_putstr("\n");
    }
}

void	draw_map_bonus(char **map)
{
  int	i;
  int	n;
  char	line[MAP_X * 2 + 1];

  my_putstr("  |");
  i = n = -1;
  while (++i < MAP_X && (line[++n] = 'A' + i) && (line[n + 1] = ' '))
    n++;
  line[n] = 0;
  my_putstr(merge_str(line, "\n--+"));
  i = 0;
  while (++i < MAP_X * 2)
    my_putstr("-");
  my_putstr("\n");
  i = -1;
  line[2] = '|';
  line[3] = 0;
  while (++i < MAP_Y)
    {
      line[0] = ('1' + (i + 1)) > '9' ? (i + 1) / 10 + '0' : '0';
      line[1] = '0' + (i + 1) % 10;
      my_putstr(line);
      print_str_separator(map[i], ' ');
      my_putstr("\n");
    }
}
