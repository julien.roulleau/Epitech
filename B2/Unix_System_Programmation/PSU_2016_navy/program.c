/*
** program.c for navy in /home/antoine.falais/Documents/{Epitech}/Projects/PSU/S2/PSU_2016_navy/project
**
** Made by Antoine Falais
** Login   <antoine.falais@epitech.net>
**
** Started on  Fri Feb  3 09:03:15 2017 Antoine Falais
** Last update Fri Feb 17 09:19:55 2017 Roulleau Julien
*/

#include "notif.h"
#include "game.h"
#include "parser.h"
#include "param.h"
#include "communication.h"

void	show_start(pid_t m_pid)
{
  notif(1);
  my_putnbr(m_pid);
  my_putstr("\n");
}

/* Start Client */
int	client()
{
  show_start(g_i_g->l_pid);
  if (!sending_signal(g_i_g->l_pid, 17))
    return (84);
  signal(SIGUSR2, confirm_connection);
  while (!g_i_g->connected);
  process_game();
  return (0);
}

/* Start Server */
int	server()
{
  show_start(g_i_g->l_pid);
  notif(2);
  signal(SIGUSR1, init_client);
  signal(SIGUSR2, init_client);
  while (!g_i_g->t_pid);
  signal(SIGUSR1, confirm_connection);
  g_i_g->playing = 1;
  kill(g_i_g->t_pid, SIGUSR2);
  while (g_i_g->connected == 0);
  process_game();
  return (0);
}

int	main(int ac, char **argv)
{
  if ((g_i_g = malloc(sizeof(t_game))) == NULL)
    return (84);
  if ((g_i_g->request_bin = malloc(sizeof(char) * 17)) == NULL)
    return (84);
  if (!verif_param(ac, argv))
    return (84);
  g_i_g->connected = 0;
  g_i_g->reception_bytes = 0;
  g_i_g->playing = 0;
  g_i_g->pos_atk[0] = -1;
  g_i_g->pos_atk[1] = -1;
  g_i_g->request_bin[16] = '\0';
  g_i_g->end_game = 0;
  if (ac == 2)
    return (server());
  else
    return (client());
  return (0);
}
