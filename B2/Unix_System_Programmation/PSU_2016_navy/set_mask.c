/*
** set_mask.c for navy in /home/na/Dropbox/Job/En_Cours/PSU_2016_navy/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Feb  3 15:54:22 2017 Roulleau Julien
** Last update Tue Feb 14 14:27:40 2017 Antoine Falais
*/

#include <stdlib.h>

int	set_action(int value)
{
  return (value & 0b11111);
}

int	set_posx(int value)
{
  return ((value << 5) & 0b1111100000);
}

int	set_posy(int value)
{
  return ((value << 10) & 0b111110000000000);
}
