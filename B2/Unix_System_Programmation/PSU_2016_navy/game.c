/*
** game.c for navy in /home/antoine.falais/Documents/{Epitech}/Projects/PSU/S2/PSU_2016_navy/project
**
** Made by Antoine Falais
** Login   <antoine.falais@epitech.net>
**
** Started on  Fri Feb  3 09:31:15 2017 Antoine Falais
** Last update Fri Feb 17 09:19:06 2017 Roulleau Julien
*/

#include "notif.h"
#include "communication.h"
#include "action.h"
#include "game.h"
#include "m_string.h"
#include "map.h"

void	init_client(int sig)
{
  if (g_i_g->connected)
    return;
  if (g_i_g->reception_bytes < 16)
    reception(sig);
  else
    {
      g_i_g->reception_bytes = 0;
      g_i_g->t_pid = get_response();
    }
}

void	process_game()
{
  if (!g_i_g->playing)
    showing_map(g_i_g->map_local.map, g_i_g->map_target.map);
  while (!g_i_g->end_game)
    {
      if (g_i_g->playing)
	{
	  showing_map(g_i_g->map_local.map, g_i_g->map_target.map);
	  attack();
	  signal(SIGUSR1, response_atk);
	  signal(SIGUSR2, response_atk);
	  while (g_i_g->playing);
	}
      else
	{
	  notif(10);
	  signal(SIGUSR1, recep_atk);
	  signal(SIGUSR2, recep_atk);
	  while (!g_i_g->playing);
	}
    }
}

void	showing_map(char **map_local, char **map_enemy)
{
  notif(5);
  draw_map(map_local);
  notif(6);
  draw_map(map_enemy);
}

int	*get_input(char *str)
{
  int	*cord;
  char	val_map_t;

  if (!(cord = malloc(sizeof(int) * 2)))
    return (0);
  if (!str[0] || (cord[0] = str[0] - 'A') > MAP_Y)
    return (0);
  if (!str[1] || (cord[1] = str[1] - '1') > MAP_X || str[1] == '0')
    return (0);
  if (str[2] && (cord[1] += str[2]- '1' > MAP_X))
    return (0);
  val_map_t = get_value(g_i_g->map_target.map, cord[0], cord[1]);
  if (val_map_t != '.')
    return (0);
  return (cord);
}
