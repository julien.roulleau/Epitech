/*
** param.c for navy in /home/na/Dropbox/Job/En_Cours/PSU_2016_navy/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Feb  6 15:38:03 2017 Roulleau Julien
** Last update Tue Feb 14 14:25:16 2017 Antoine Falais
*/

#include "game.h"
#include "param.h"
#include "m_math.h"

int	set_server(char **argv)
{
  g_i_g->l_pid = getpid();
  if (!(g_i_g->map_local.map = make_map(argv[1])))
    return (0);
  if (!(malloc_map(&g_i_g->map_target.map)))
    return (0);
  return (1);
}

int	set_client(char **argv)
{
  int 	i;

  i = -1;
  while (argv[1][++i])
    {
      if (!IS_NUM(argv[1][i]))
	return (0);
    }
  g_i_g->t_pid = my_getnbr(argv[1]);
  g_i_g->l_pid = getpid();
  if (!(g_i_g->map_local.map = make_map(argv[2])))
    return (0);
  if (!(malloc_map(&g_i_g->map_target.map)))
    return (0);
  return (1);
}

int	verif_param(int argc, char **argv)
{
  if (argc < 2)
    return (0);
  else if (argc == 2)
    {
      if (!(set_server(argv)))
	return (0);
    }
  else if (argc == 3)
    {
      if (!(set_client(argv)))
	return (0);
    }
  else
    return (0);
  return (1);
}
