/*
** communication.c for navy in /home/antoine.falais/Documents/{Epitech}/Projects/PSU/S2/PSU_2016_navy/project
**
** Made by Antoine Falais
** Login   <antoine.falais@epitech.net>
**
** Started on  Fri Feb  3 09:09:16 2017 Antoine Falais
** Last update Tue Feb 14 14:13:10 2017 Antoine Falais
*/

#include "game.h"
#include "notif.h"
#include "m_math.h"
#include "m_string.h"
#include "communication.h"
#include "action.h"

int	sending_signal(int bytes, int size_pkg)
{
  int	i;

  i = 0;
  while (i < size_pkg)
    {
      usleep(1000);
      if (((bytes >> i) & 1) == 1)
      	{
      	  if (kill(g_i_g->t_pid, SIGUSR1) == -1)
      	    return (0);
      	}
      else if (kill(g_i_g->t_pid, SIGUSR2) == -1)
	return (0);
      i++;
    }
  return (1);
}

void	reception(int sig)
{
  if (sig == SIGUSR1)
    g_i_g->request_bin[g_i_g->reception_bytes] = '1';
  else
    g_i_g->request_bin[g_i_g->reception_bytes] = '0';
  g_i_g->reception_bytes++;
}

void	confirm_connection(int sig)
{
  if (g_i_g->connected)
    return;
  if (sig == SIGUSR1)
    notif(3);
  else if (sig == SIGUSR2)
    {
      notif(4);
      kill(g_i_g->t_pid, SIGUSR1);
    }
  else
    return;
  g_i_g->connected = 1;
}

int	get_response()
{
  int	response;

  g_i_g->request_bin = my_rev_str(g_i_g->request_bin);
  response = (bin_to_int(g_i_g->request_bin));
  g_i_g->reception_bytes = 0;
  g_i_g->request_bin = clean_buffer(g_i_g->request_bin);
  return (response);
}
