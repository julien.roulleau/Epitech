/*
** notif.c for navy in /home/na/Dropbox/Job/En_Cours/PSU_2016_navy/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Feb  3 20:47:16 2017 Roulleau Julien
** Last update Sun Feb 19 22:19:03 2017 Antoine Falais
*/

#include "notif.h"

static void	notif_bis(int nb)
{
  if (nb == 11)
    PRINT(":  hit\n");
  else if (nb == 12)
    PRINT("I won\n");
  else if (nb == 13)
    PRINT("\nEnemy won\n");
  else if (nb == 14)
    PRINT(": flow\n");
  else
    PRINT("Invalid notif");
}

void	notif(int nb)
{
  if (nb == 1)
    PRINT("my_pid:  ");
  else if (nb == 2)
    PRINT("waiting for enemy connexion...\n");
  else if (nb == 3)
    PRINT("\nenemy connected\n");
  else if (nb == 4)
    PRINT("successfully connected\n");
  else if (nb == 5)
    PRINT("\nmy positions:\n");
  else if (nb == 6)
    PRINT("\nenemy's positions:\n");
  else if (nb == 7)
    PRINT("\nattack:  ");
  else if (nb == 8)
    PRINT("wrong position\n");
  else if (nb == 9)
    PRINT(": missed\n");
  else if (nb == 10)
    PRINT("waiting for enemy's attack...\n");
  else
    notif_bis(nb);
}
