/*
** check_hit.c for navy in /home/na/Dropbox/Job/En_Cours/PSU_2016_navy/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Feb 13 15:11:27 2017 Roulleau Julien
** Last update Fri Feb 17 09:18:25 2017 Roulleau Julien
*/

#include "game.h"
#include "m_math.h"
#include "map.h"

static	int	*set_boat()
{
	static int 	*boat = 0;
	int	i;

	i = 1;
	boat = malloc(sizeof(int) * (SIZE_MAX_BOAT + 1));
	boat[0] = boat[1] = boat[SIZE_MAX_BOAT] = 0;
	while (++i <= SIZE_MAX_BOAT)
	  boat[i] = i;
	return (boat);
}

static	int	check_lose(int *boat)
{
  int		i;

  i = -1;
  while (++i <= SIZE_MAX_BOAT)
    {
      if (boat[i] != 0)
	return (0);
    }
  return (1);
}

int	check_hit(int x, int y, char **map)
{
  static int	*boat = 0;
  int		ret;
  char		c;

  ret = 0;
  c = get_value(map, x, y);
  if (boat == 0)
    boat = set_boat();
  if (IS_NUM(c))
    {
      boat[c - '0']--;
      if (!boat[c - '0'])
	ret = 2;
      else
	ret = 1;
    }
  if (check_lose(boat))
    ret = 3;
  return (ret);
}
