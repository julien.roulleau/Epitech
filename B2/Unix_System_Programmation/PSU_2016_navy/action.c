/*
** action.c for navy in /home/na/Dropbox/Job/En_Cours/PSU_2016_navy/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Feb  3 16:04:16 2017 Roulleau Julien
** Last update Sun Feb 19 21:32:39 2017 Antoine Falais
*/

#include "m_math.h"
#include "game.h"
#include "parser.h"
#include "action.h"
#include "m_string.h"
#include "communication.h"
#include "check_hit.c"
#include "map.h"
#include "notif.h"

int	attack()
{
  char	*coord;
  int	*pos;

  notif(7);
  coord = get_next_line(0);
  while (!(pos = get_input(coord)))
    {
      notif(8);
      notif(7);
      coord = get_next_line(0);
    }
  if (!(sending_signal(pos[0], 17)))
    return (84);
  if (!(sending_signal(pos[1], 17)))
    return (84);
  g_i_g->pos_atk[0] = pos[0];
  g_i_g->pos_atk[1] = pos[1];
  return (1);
}

void	check_map_atk()
{
  int	*pos;
  int	r;

  if (!(pos = malloc(sizeof(int) * 2)))
    return;
  pos[0] = g_i_g->pos_atk[0];
  pos[1] = g_i_g->pos_atk[1];
  r = check_hit(pos[0], pos[1], g_i_g->map_local.map);
  showing_result(pos[0], pos[1], r);
  sending_signal(r, 5);
  usleep(1000);
  g_i_g->pos_atk[0] = -1;
  g_i_g->pos_atk[1] = -1;
  if (r == 3)
    g_i_g->end_game = 1;
  g_i_g->playing = 1;
}

void	response_atk(int sig)
{
  int   pos[2];
  int   r;

  if (!g_i_g->playing)
    return;
  if (g_i_g->reception_bytes < 4)
    reception();
  else
    {
      pos[0] = g_i_g->pos_atk[0];
      pos[1] = g_i_g->pos_atk[1];
      r = get_response();
      if (r)
	set_value(g_i_g->map_target.map, pos[0], pos[1], HIT);
      else
	set_value(g_i_g->map_target.map, pos[0], pos[1], MISS);
      showing_result(pos[0], pos[1], r);
      if (r == 3)
	g_i_g->end_game = 1;
      g_i_g->playing = 0;
    }
}

void	recep_atk(int sig)
{
  int	val;

  if (g_i_g->playing)
    return;
  if (g_i_g->reception_bytes < 16)
    reception(sig);
  else
    {
      val = get_response();
      if (g_i_g->pos_atk[0] == -1)
	g_i_g->pos_atk[0] = val;
      else
	{
	  g_i_g->pos_atk[1] = val;
	  check_map_atk();
	}
    }
}

void	showing_result(int x, int y, int r)
{
  my_putchar(x + 'A');
  my_putchar(y + '1');
  if (r == 3)
    {
      notif(11);
      if (g_i_g->playing)
	notif(12);
      else
	notif(13);
    }
  else if (r == 2)
    notif(14);
  else if (r == 1)
    notif(11);
  else
    notif(9);
  g_i_g->pos_atk[0] = -1;
  g_i_g->pos_atk[1] = -1;
  g_i_g->playing = 0;
  my_putstr("\n");
}
