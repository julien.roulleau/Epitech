/*
** parcing.c for navy in /home/infocraft/Job/En_Cours/PSU_2016_navy/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Jan 31 11:47:29 2017 Roulleau Julien
** Last update Tue Feb 14 14:26:26 2017 Antoine Falais
*/

#include <fcntl.h>
#include "parser.h"
#include "map.h"
#include "m_math.h"
#include "game.h"

int	malloc_map(char ***map)
{
  int	y;
  int	x;
  
  y = -1;
  if (!(*map = malloc(sizeof(int *) * (MAP_Y + 1))))
    return (0);
  while (++y < MAP_Y)
    {
      if (!((*map)[y] = malloc(sizeof(int *) * (MAP_X + 1))))
	return (0);
      (*map)[y][MAP_X] = 0;
    }
  (*map)[MAP_Y] = 0;
  y = -1;
  while ((*map)[++y])
    {
      x = -1;
      while (++x < MAP_X)
	(*map)[y][x] = '.';
      (*map)[y][x] = 0;
    }
  return (1);
}

static int	set_param(t_param *param, char *buffer)
{
  if (!IS_NUM(buffer[0]) && buffer[1] != ':' &&
      !IS_ALPHA_UP(buffer[2]) && !IS_NUM(buffer[3]) &&
		buffer[4] != ':' && buffer[7] == 0 &&
      !IS_ALPHA_UP(buffer[5]) && !IS_NUM(buffer[6]))
			return (0);
	param->lenght = buffer[0] - '0';
	param->x = buffer[2] - 'A';
	param->y = buffer[3] - '1';
	param->inx = param->iny = 0;
	if (param->x > MAP_X - 1 || param->y > MAP_Y - 1)
		return (0);
	if (param->x == buffer[5] - 'A' &&
		ABS((param->y - (buffer[6] - '1'))) == param->lenght - 1 &&
		(param->iny = 1))
			param->rev = param->y > buffer[6] - '1' ? -1 : 1;
	else if (param->y == buffer[6] - '1' &&
		ABS((param->x - (buffer[5] - 'A'))) == param->lenght - 1 &&
		(param->inx = 1))
			param->rev = param->x > buffer[5] - 'A' ? -1 : 1;
	else
		return (0);
	return (1);
}

static char	**change_map(t_param param, char **map)
{
	int	i;

	i = param.lenght;
	while (i--)
	{
		if (IS_NUM(map[param.x][param.y]))
			return (0);
		map[param.x][param.y] = param.lenght + '0';
		param.x += param.inx * param.rev;
		param.y += param.iny * param.rev;
	}
	return (map);
}

static char	**set_map(char **map, int fd)
{
	t_param	param;
	char	*buffer;
	int	boat[SIZE_MAX_BOAT + 1];
	int	i;

	boat[0] = boat[1] = i = 1;
	while (++i < SIZE_MAX_BOAT)
		boat[i] = 0;
	boat[SIZE_MAX_BOAT] = 0;
	while ((buffer = get_next_line(fd)))
	{
		if (!(set_param(&param, buffer)))
			return (0);
		if (param.lenght > SIZE_MAX_BOAT || boat[param.lenght])
			return (0);
		boat[param.lenght] = 1;
		if (!(map = change_map(param, map)))
			return (0);
		free(buffer);
	}
	i = -1;
	while (++i < SIZE_MAX_BOAT)
		if (!boat[i])
			return (0);
	return (map);
}

char		**make_map(char *file)
{
	char	**map;
	int	fd;

	if (!(malloc_map(&map)))
		return (0);
	if ((fd = open(file, O_RDONLY)) == -1)
		return (0);
	if (!(map = set_map(map, fd)))
		return (0);
	close(fd);
	return (map);
}
