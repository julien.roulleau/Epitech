/*
** mask.c for navy in /home/na/Dropbox/Job/En_Cours/PSU_2016_navy/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Feb  3 15:44:55 2017 Roulleau Julien
** Last update Tue Feb 14 14:19:53 2017 Antoine Falais
*/

int	get_action(int bit)
{
  return (bit & 0b11111);
}

int	get_posx(int bit)
{
  return ((bit & 0b1111100000) >> 5);
}

int	get_posy(int bit)
{
  return ((bit & 0b111110000000000) >> 10);
}
