/*
** notif.h for navy in /home/na/Dropbox/Job/En_Cours/PSU_2016_navy/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Feb  3 20:47:44 2017 Roulleau Julien
** Last update Fri Feb 17 09:13:16 2017 Roulleau Julien
*/

#ifndef NOTIF_H
# define NOTIF_H

# include <unistd.h>

# define STRLEN(str) (sizeof(str) / sizeof(str[0]) - 1)
# define PRINT(str) (write(1, str, STRLEN(str)))

void		my_putstr(char *str);
void		my_putnbr(int nbr);
void		notif(int);

#endif /* NOTIF_H */
