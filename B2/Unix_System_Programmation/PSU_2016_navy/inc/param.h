/*
** param.h for navy in /home/na/Dropbox/Job/En_Cours/PSU_2016_navy/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Feb  6 15:57:28 2017 Roulleau Julien
** Last update Fri Feb 17 09:13:02 2017 Roulleau Julien
*/

#ifndef PARAM_H
# define PARAM_H

int	malloc_map(char ***);
char	**make_map(char *);
int	my_getnbr(char *str);
int	verif_param(int argc, char **argv);

#endif /* PARAM_H */
