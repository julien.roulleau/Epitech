/*
** game.h for navy in /home/infocraft/Job/En_Cours/PSU_2016_navy/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Jan 31 11:52:21 2017 Roulleau Julien
** Last update Fri Feb 17 09:14:22 2017 Roulleau Julien
*/

#ifndef	GAME_H
# define GAME_H

# include <stdlib.h>
# include <sys/types.h>
# include <signal.h>
# include <unistd.h>

# define MAP_X 8
# define MAP_Y 8
# define SIZE_MAX_BOAT 5

typedef struct	s_map
{
  int		size_y;
  int		size_x;
  int		*boat;
  int		empty_case;
  char		**map;
}		t_map;

typedef struct	s_game
{
  t_map		map_local;
  t_map		map_target;
  int		reception_bytes;
  int		pos_atk[2];
  int   	end_game;
  int   	playing;
  int		connected;
  pid_t	  	l_pid;
  pid_t		t_pid;
  char		*request_bin;
}		t_game;

void	init_client(int sig);
void	init_game();
void	process_game();
void	showing_map(char**, char**);
int	*get_input(char *str);

t_game	*g_i_g; /* global_instance_game */

#endif /* GAME_H */
