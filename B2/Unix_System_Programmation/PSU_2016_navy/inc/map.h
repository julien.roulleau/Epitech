/*
** map.h for navy in /home/na/Dropbox/Job/En_Cours/PSU_2016_navy/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Feb  3 20:23:24 2017 Roulleau Julien
** Last update Fri Feb 17 09:13:35 2017 Roulleau Julien
*/

#ifndef MAP_H
# define MAP_H

# include "game.h"

void		my_putstr(char *);
void		my_putchar(char);
char		*merge_str(char *, char *);
void		draw_map(char **map);
void		set_value(char **map, int x, int y, char c);
char		get_value(char **map, int x, int y);

#endif /* MAP_H */
