/*
** communication.h for navy in /home/antoine.falais/Documents/{Epitech}/Projects/PSU/S2/PSU_2016_navy/inc
**
** Made by Antoine Falais
** Login   <antoine.falais@epitech.net>
**
** Started on  Fri Feb  3 13:57:13 2017 Antoine Falais
** Last update Fri Feb 17 09:14:33 2017 Roulleau Julien
*/

#ifndef COM_H
# define COM_H

int		sending_signal(int bytes, int size_pkg);
int 		get_response();
void		reception();
void		init_client(int sig);
void		confirm_connection(int);

#endif
