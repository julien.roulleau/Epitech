/*
** parser.h for navy in /home/antoine.falais/Documents/{Epitech}/Projects/PSU/S2/PSU_2016_navy/project/inc
**
** Made by Antoine Falais
** Login   <antoine.falais@epitech.net>
**
** Started on  Fri Feb  3 11:51:41 2017 Antoine Falais
** Last update Tue Feb 14 14:36:36 2017 Antoine Falais
*/

#ifndef PARSER_H
# define PARSER_H

typedef struct		s_param
{
	int		lenght;
	int		iny;
	int		inx;
	int		rev;
	int		y;
	int		x;
}			t_param;

char		*get_next_line(const int);
int		my_getnbr(char *);
char		*my_strdup(char *);
int		get_action(int bit);

#endif /* PARSER_H */
