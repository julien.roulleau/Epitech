/*
** m_string.h for navy in /home/antoine.falais/Documents/{Epitech}/Projects/PSU/S2/PSU_2016_navy/inc
**
** Made by Antoine Falais
** Login   <antoine.falais@epitech.net>
**
** Started on  Sun Feb  5 15:27:29 2017 Antoine Falais
** Last update Tue Feb 14 14:35:37 2017 Antoine Falais
*/

#ifndef M_STR_H
# define M_STR_H

char *my_rev_str(char *s);
char *clean_buffer();
char *add_char_in_str(char *origin, char n_char);
int my_strlen(char*);

#endif /* M_STR_H */
