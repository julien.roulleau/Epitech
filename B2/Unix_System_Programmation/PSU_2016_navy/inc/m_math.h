/*
** m_math.h for navy in /home/antoine.falais/Documents/{Epitech}/Projects/PSU/S2/PSU_2016_navy/project/inc
** 
** Made by Antoine Falais
** Login   <antoine.falais@epitech.net>
** 
** Started on  Fri Feb  3 11:49:53 2017 Antoine Falais
** Last update Sun Feb  5 15:47:18 2017 Antoine Falais
*/

#ifndef M_MATH_H
# define M_MATH_H

# define ABS(nb) (nb < 0 ? nb * -1 : nb)
# define IS_ALPHA_UP(c) (c >= 'A' && c <= 'Z')
# define IS_ALPHA_DOWN(c) (c >= 'a' && c <= 'z')
# define IS_NUM(c) (c >= '0' && c <= '9')
# define IS_ALPHA(c) (IS_ALPHA_UP(c) || IS_ALPHA_DOWN(c))
# define IS_ALPHA_NUM(c) (IS_ALPHA(c) || IS_NUM(c))

int bin_to_int(char *val_bin);

#endif
