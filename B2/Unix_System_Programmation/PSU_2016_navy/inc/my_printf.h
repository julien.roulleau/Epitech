/*
** my_printf.h for my_printf_header in /home/antoine.falais/rendu/Projets/Printf/PSU_2016_my_printf
**
** Made by Antoine Falais
** Login   <antoine.falais@epitech.net>
**
** Started on  Mon Nov 14 15:35:23 2016 Antoine Falais
** Last update Fri Feb 17 09:13:29 2017 Roulleau Julien
*/

#ifndef MY_PF
# define MY_PF

int	my_printf(char*, ...);

typedef	struct	s_fprint
{
  char	c;
  void	(*func)(void*);
}	      t_fprint;
t_fprint my_fprint[9];

#endif
