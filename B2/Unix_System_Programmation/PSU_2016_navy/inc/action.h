/*
** action.h for navy in /home/na/Dropbox/Job/En_Cours/PSU_2016_navy/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Feb  3 16:10:11 2017 Roulleau Julien
** Last update Fri Feb 17 09:14:41 2017 Roulleau Julien
*/

#ifndef ACTION_H
# define ACTION_H

# define MISS 'o'
# define HIT 'x'

int	attack();
void 	showing_result(int x, int y, int notif_id);
void	recep_atk(int sig);
void	response_atk(int sig);
void  	check_map_atk();

#endif /* ACTION_H */
