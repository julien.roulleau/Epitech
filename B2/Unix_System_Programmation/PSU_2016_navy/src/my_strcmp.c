/*
** strcmp.c for src in /home/infocraft/Job/Lib/src/str/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Jan 16 19:31:32 2017 Roulleau Julien
** Last update Fri Feb 17 09:15:05 2017 Roulleau Julien
*/

int		my_strcmp(char *str, char *cmp)
{
  int		i;

  i = -1;
  while (str[++i] && cmp[i])
    if (str[i] != cmp[i])
      return (0);
  if (!str[i] && !cmp[i])
    return (1);
  return (0);
}
