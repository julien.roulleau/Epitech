/*
** m_string.c for navy in /home/antoine.falais/Documents/{Epitech}/Projects/PSU/S2/PSU_2016_navy
**
** Made by Antoine Falais
** Login   <antoine.falais@epitech.net>
**
** Started on  Sun Feb  5 15:31:50 2017 Antoine Falais
** Last update Fri Feb 17 09:16:53 2017 Roulleau Julien
*/

#include <stdlib.h>
#include "notif.h"
#include "m_math.h"
#include "m_string.h"

char	*my_rev_str(char *s)
{
  int	i;
  int	len;
  char	*n_s;

  len = my_strlen(s);
  n_s = malloc(sizeof(char) * len);
  n_s[len] = '\0';
  len--;
  i = 0;
  while (s[i])
    {
      n_s[len - i] = s[i];
      i++;
    }
  return (n_s);
}

char	*add_char_in_str(char *origin, char n_char)
{
  int	len;
  char	*n_str;
  int	i;

  len = my_strlen(origin);
  n_str = malloc(sizeof(char) * len + 2);
  i = 0;
  while (origin[i])
    {
      n_str[i] = origin[i];
      i++;
    }
  n_str[i] = n_char;
  n_str[i + 1] = '\0';
  return (n_str);
}

char    *clean_buffer()
{
  char	*buffer;
  int	i;

  i = 0;
  buffer = malloc(sizeof(char) * 17);
  while (i < 17)
  {
    buffer[i] = 0;
    i++;
  }
  return (buffer);
}
