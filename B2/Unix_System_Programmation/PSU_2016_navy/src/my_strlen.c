/*
** my_strlen.c for src in /home/infocraft/Job/Lib/my_printf/src/str/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun Jan  8 22:29:31 2017 Roulleau Julien
** Last update Fri Feb 17 09:14:58 2017 Roulleau Julien
*/

static int	my_strlen_count(char *str, int i)
{
  return (*str != '\0' ? my_strlen_count(++str, ++i) : i);
}

int		my_strlen(char *str)
{
  return (my_strlen_count(str, 0));
}
