/*
** m_math.c for navy in /home/antoine.falais/Documents/{Epitech}/Projects/PSU/S2/PSU_2016_navy/src
**
** Made by Antoine Falais
** Login   <antoine.falais@epitech.net>
**
** Started on  Sun Feb  5 15:43:36 2017 Antoine Falais
** Last update Fri Feb 17 09:17:02 2017 Roulleau Julien
*/

#include "m_math.h"
#include "m_string.h"

int	my_pow(int val, int exp)
{
  int 	i;
  int 	p;

  i = 1;
  p = val;
  while (i < exp)
    {
      val = val * p;
      i++;
    }
  return (val);
}

int	bin_to_int(char *val_bin)
{
  int	len;
  int	val;
  int	i;

  i = 0;
  len = my_strlen(val_bin) - 1;
  val = 0;
  if (len == -1)
    return (0);
  while (i < len)
    {
      if (val_bin[i] == '1')
	val += my_pow(2, len - i);
      i++;
    }
  if (val_bin[i] == '1')
    val++;
  return (val);
}
