/*
** my_putstr.c for src in /home/infocraft/Job/Lib/my_printf/src/str/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun Jan  8 23:03:02 2017 Roulleau Julien
** Last update Tue Feb 14 14:31:09 2017 Antoine Falais
*/

#include <unistd.h>

void		my_putnbr(int);

void		my_putstr(char *str)
{
  if (*str != '\0')
    {
      write(1, &(*str), 1);
      my_putstr(++str);
    }
}

void		my_putstr_s(char *str)
{
  if (*str != '\0')
    {
      if (*str < 32)
	{
	  write(1, "\\0", 2);
	  if (*str < 10)
	    write(1, "0", 1);
	  my_putnbr(*str);
	}
      else
	write(1, str, 1);
      my_putstr_s(++str);
    }
}
