/*
** my_putchar.c for src in /home/infocraft/Job/Lib/my_printf/src/str/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun Jan  8 22:27:15 2017 Roulleau Julien
** Last update Sun Jan  8 22:28:24 2017 Roulleau Julien
*/

#include <unistd.h>

void		my_putchar(char c)
{
	write(1, &c, 1);
}
