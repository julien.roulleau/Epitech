/*
** main.c for lib in /home/na/Dropbox/Job/Lib/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Apr 14 19:43:16 2017 Roulleau Julien
** Last update Fri May 12 17:26:35 2017 Roulleau Julien
*/

#include "list.h"
#include "src.h"

int		main(int argc, char **argv, char **env)
{
	(void) argc;
	(void) argv;
	(void) env;

	return (0);
}
