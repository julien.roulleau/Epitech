/*
** list.h for list in /home/infocraft/Job/Lib/include/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Jan 17 11:38:07 2017 Roulleau Julien
** Last update Sat Apr 15 01:35:59 2017 Roulleau Julien
*/

#ifndef LIST
# define LIST

#include <stdlib.h>

typedef enum		e_type_list
{
  LINKED, CIRCULAR
}			t_type_list;

typedef struct 		s_node
{
	void 		*data;
	struct s_node	*previous;
	struct s_node	*next;
}			t_node;

typedef struct		s_list
{
	t_node		*first;
	t_node		*last;
	t_type_list	type;
	int		size;
}			t_list;

t_list 		*create_list(t_type_list);
int 		free_node(t_list *, t_node *);
int 		free_list(t_list *);
int		add_first_node(t_list *, void *);
int		add_last_node(t_list *, void *);
void 		**tab_list(t_list *);

#endif		/* !LIST */
