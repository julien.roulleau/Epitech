/*
** print_special.c for lib in /home/na/Dropbox/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Apr 18 16:05:31 2017 Roulleau Julien
** Last update Tue Apr 18 16:13:32 2017 Roulleau Julien
*/

#include "my_printf.h"

void		my_print_tab(va_list va)
{
	char 	**tab;
	int	i;

	tab = va_arg(va, char **);
	i = -1;
	PRINT("[");
	while (tab[++i])
	{
		my_putstr(tab[i]);
		if (tab[i + 1])
			PRINT(", ");
	}
	PRINT("]");
}
