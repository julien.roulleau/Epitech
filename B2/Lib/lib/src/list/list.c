/*
** list.c for list in /home/na/Dropbox/Job/Lib/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Apr  4 17:54:56 2017 Roulleau Julien
** Last update Fri May 12 17:13:53 2017 Roulleau Julien
*/

#include <stdlib.h>
#include "list.h"

t_list 		*create_list(t_type_list type)
{
	t_list	*list;

	if (!(list = malloc(sizeof(t_list))))
		return (0);
	list->first = NULL;
	list->last = NULL;
	list->type = type;
	list->size = 0;
	return (list);
}

int 		free_node(t_list *list, t_node *node)
{
	if (!list || !node)
		return (0);
	if (node == list->first)
		list->first = node->next ? node->next : 0;
	if (node == list->last)
		list->last = node->previous ? node->previous : 0;
	if (node->previous)
		node->previous->next = node->next ? node->next : 0;
	if (node->next)
		node->next->previous = node->previous ? node->previous : 0;
	free(node);
	list->size -= 1;
	return (1);
}

int		clean_list(t_list *list)
{
	int i;

	if (!list)
		return (0);
	i = -1;
	while (i++ < list->size)
		if (!free_node(list, list->first))
			return (0);
	return (1);
}

int 		free_list(t_list *list)
{
	if (!list || !clean_list(list))
		return (0);
	free(list);
	return (1);
}
