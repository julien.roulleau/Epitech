/*
** my_strdup.c for src in /home/infocraft/Job/Lib/my_printf/src/str/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun Jan  8 23:15:19 2017 Roulleau Julien
** Last update Mon Apr 10 17:30:53 2017 Roulleau Julien
*/

#include <stdlib.h>

int		my_strlen(char *);

char		*my_strdup(char *src)
{
	char	*str;
	int	i;

	if (!(str = malloc(sizeof(char) * (my_strlen(src) + 1))))
		return (0);
	i = -1;
	while (src[++i])
		str[i] = src[i];
	str[i] = 0;
	return (str);
}
