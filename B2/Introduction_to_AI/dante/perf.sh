#!/bin/bash

MAP="$PWD/map"
RESULT="$PWD/result"
ASTAR="$PWD/astar/solver"
BREADTH="$PWD/breadth/solver"
DEPTH="$PWD/depth/solver"
TOURNAMENT="$PWD/tournament/solver"

DURATION="10"
TIMEOUT=`which timeout`
LS=`which ls`
ECHO=`which echo`
BC=`which bc`
MKDIR=`which mkdir`
DATE=`which date`
EXPR=`which expr`

test ()
{
	PATH=$1
	NAME=$2
	TOTAL=0
	if [ ! -d $RESULT/$NAME ]
	then
		$MKDIR $RESULT/$NAME
	fi
	$LS $MAP|while read -r; do
		echo "	$REPLY:"
		START=$($DATE +%s.%N)
		$TIMEOUT $DURATION $PATH $MAP/$REPLY > $RESULT/$NAME/$REPLY
		TIME=$($ECHO "$($DATE +%s.%N) - $START" | $BC)
		echo "		Time: $TIME"
		TOTAL=$($ECHO "$TOTAL + $TIME" | $BC)
		echo "		Total time: $TOTAL"
		echo "---------------------------------------------------"
	done
}

launch_test ()
{
	PATH=$1
	NAME=$2
	echo "$NAME:"
	if [ -f $PATH ]
	then
		test $PATH $NAME
	else
		echo "	$PATH not found"
	fi
}

separator ()
{
	echo "###################################################"
	echo ""
	echo "###################################################"
}
#################################################################################
#					Start					#
#################################################################################

if [ ! -d $RESULT ]
then
	mkdir $RESULT
fi

if [ $# -eq 1 ]
then
	case "$1" in
		astar)
			launch_test $ASTAR Astar
			;;
		breadth)
			launch_test $BREADTH Breadth
			;;
		depth)
			launch_test $DEPTH Depth
			;;
		tournament)
			launch_test $TOURNAMENT Tournament
			;;
		*)
			echo "Usage: $0 [astar|breadth|depth|tournament]"
			;;
	esac
	exit 1
fi

echo "###################################################"
launch_test $ASTAR Astar
separator
launch_test $BREADTH Breadth
separator
launch_test $DEPTH Depth
separator
launch_test $TOURNAMENT Tournament
echo "###################################################"
