/*
** lobby.h for dante in /home/na/Dropbox/Job/En_Cours/dante/gen/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Apr 21 00:01:52 2017 Roulleau Julien
** Last update Tue May  2 17:02:33 2017 Roulleau Julien
*/

#ifndef LOBBY_H
# define LOBBY_H

#include "struct.h"

char		**backtrack_gen(t_pos);
char		**speed_backtrack_gen(t_pos);
char		**imperfect(t_pos);

#endif /* LOBBY_H */
