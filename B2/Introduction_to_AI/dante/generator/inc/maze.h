/*
** set_map.h for dante in /home/na/Dropbox/Job/En_Cours/dante/gen/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Apr 20 11:32:37 2017 Roulleau Julien
** Last update Thu Apr 20 12:15:53 2017 Roulleau Julien
*/

#ifndef SET_MAP_H
# define SET_MAP_H

# include "struct.h"

char		**set_tab(t_pos, char);

#endif /* SET_MAP_H */
