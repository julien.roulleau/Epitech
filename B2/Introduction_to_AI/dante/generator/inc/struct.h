/*
** struct.h for dante in /home/na/Dropbox/Job/En_Cours/dante/gen/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Apr 20 12:14:23 2017 Roulleau Julien
** Last update Sat May 13 18:03:17 2017 Roulleau Julien
*/

#ifndef STRUCT_H
# define STRUCT_H

typedef enum 	e_slt
{
	BACKTRACK,
	IMPERFECT
}		t_slt;

typedef struct	s_pos
{
	int	y;
	int	x;
}		t_pos;

#endif /* STRUCT_H */
