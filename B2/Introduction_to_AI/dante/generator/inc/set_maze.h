/*
** set_map.h for dante in /home/na/Dropbox/Job/En_Cours/dante/gen/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Apr 20 11:32:37 2017 Roulleau Julien
** Last update Sat May 13 18:02:00 2017 Roulleau Julien
*/

#ifndef SET_MAZE_H
# define SET_MAZE_H

# include "struct.h"

char	**set_tab(t_pos, char);
char	**backtrack(char **, t_pos *size, t_pos *pos);
void 	fix_maze(char **);

#endif /* SET_MAZE_H */
