/*
** main.c for generator in /home/na/Dropbox/Job/En_Cours/dante/gen/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Apr 14 21:11:01 2017 Roulleau Julien
** Last update Sat May 13 23:59:07 2017 Roulleau Julien
*/

#include "src.h"
#include "lobby.h"
#include "set_maze.h"

void 		print_tab(char **tab)
{
	int	i;

	i = -1;
	while (tab[++i])
	{
		my_putstr_fd(1, tab[i]);
		if (tab[i + 1])
			PRINT("\n");
	}
}

char		**lobby(t_pos size, t_slt slt)
{
	char	**tab;

	tab = 0;
	if (slt == BACKTRACK && !(tab = backtrack_gen(size)))
		return (0);
	else if (slt == IMPERFECT && !(tab = imperfect(size)))
		return (0);
	return (tab);
}

t_slt		select_gen(int argc, char **argv)
{
	t_slt	slt;

	if (argc > 3 && my_strcmp(argv[3], "perfect"))
		slt = BACKTRACK;
	else
		slt = IMPERFECT;
	return (slt);
}

int		special_maze(t_pos size)
{
	int	i;

	i = -1;
	if (size.x == 1)
		while (++i < size.y)
			PRINT("*\n");
	else
	{
		while (++i < size.x)
			PRINT("*");
		PRINT("\n");
	}
	return (1);
}

int 		main(int argc, char **argv)
{
	char	**tab;
	t_pos	size;
	t_slt	slt;

	if (argc < 3 || !is_num(argv[1]) || !is_num(argv[2]))
		return (84);
	slt = select_gen(argc, argv);
	size.x = my_getnbr(argv[1]);
	size.y = my_getnbr(argv[2]);
	if (size.x == 0 || size.y == 0)
	{
		PRINT("Invalid size.\n");
		return (84);
	}
	if (size.x == 1 || size.y == 1)
	{
		special_maze(size);
		return (0);
	}
	if (!(tab = lobby(size, slt)))
		return (84);
	print_tab(tab);
	free_tab(tab);
	return (0);
}
