/*
** select_gen.c for dante in /home/na/Dropbox/Job/En_Cours/dante/gen/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Apr 20 23:59:24 2017 Roulleau Julien
** Last update Sat May 13 18:02:56 2017 Roulleau Julien
*/

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include "set_maze.h"

char		**backtrack_gen(t_pos size)
{
	char	**tab;
	int	i;

	size.y--;
	size.x--;
	i = 0;
	while (i == 0 || tab[size.y][size.x] != '*')
	{
		if (!(tab = set_tab(
			(t_pos) {size.y + 1, size.x + 1}, 'A')))
			return (0);
		srand(time(0) * ++i);
		tab[0][0] = '*';
		tab[0][1] = 'B';
		tab[1][0] = 'B';
		if (!(backtrack(tab, &size, &(t_pos) {0, 0})))
			return (0);
		fix_maze(tab);
	}
	return (tab);
}

char		**imperfect(t_pos size)
{
	char	**tab;
	t_pos	pos;

	if (!(tab = backtrack_gen(size)))
		return (0);
	pos.y = 0;
	pos.x = -1;
	while (tab[pos.y][++pos.x] != 'X')
	{
		if (!tab[pos.y][pos.x + 1])
		{
			pos.y++;
			pos.x = -1;
		}
	}
	tab[pos.y][pos.x] = '*';
	return (tab);
}
