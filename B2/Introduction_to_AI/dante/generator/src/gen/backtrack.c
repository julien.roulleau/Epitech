/*
** maze_gen.c for dante in /home/na/Dropbox/Job/En_Cours/dante/gen/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Apr 20 12:09:06 2017 Roulleau Julien
** Last update Sat May 13 18:04:57 2017 Roulleau Julien
*/

#include "src.h"
#include "struct.h"
#include "list.h"

#define IS_WALL(yy, xx, c) (tab[pos->y + yy][pos->x + xx] == c)
#define IF_WALL(y, x) (IS_WALL(y, x, 'A') || IS_WALL(y, x, 'B'))
#define MY_RAND(min, max) (rand() / (double) RAND_MAX * (max + 1 - min) + min)

static t_pos 	*set_pos(int y, int x)
{
	t_pos	*pos;

	if (!(pos = malloc(sizeof(t_pos))))
		return (0);
	pos->y = y;
	pos->x = x;
	return (pos);
}

static t_pos	*next_pos(char **tab, t_pos *size, t_pos *pos)
{
	int	r;
	int	i;

	r = MY_RAND(0, 3);
	r = r > 3 ? 0 : r;
	i = -1;
	while (++i < 4)
	{
		if (r == 0 && pos->y > 0 && IF_WALL(-1, 0))
			return (set_pos(pos->y - 1, pos->x));
		else if (r == 1 && pos->x > 0 && IF_WALL(0, -1))
			return (set_pos(pos->y, pos->x - 1));
		else if (r == 2 && pos->y < size->y && IF_WALL(1, 0))
			return (set_pos(pos->y + 1, pos->x));
		else if (r == 3 && pos->x < size->x && IF_WALL(0, 1))
			return (set_pos(pos->y, pos->x + 1));
		r++;
		r = r > 3 ? 0 : r;
	}
	return (0);
}

static void 	set_wall(char **tab, t_pos *pos, t_pos *size, char c[2])
{
	if (pos->y > 0 && IS_WALL(-1, 0, c[0]))
		tab[pos->y - 1][pos->x] = c[1];
	if (pos->x > 0 && IS_WALL(0, -1, c[0]))
		tab[pos->y][pos->x - 1] = c[1];
	if (pos->y < size->y && IS_WALL(1, 0, c[0]))
		tab[pos->y + 1][pos->x] = c[1];
	if (pos->x < size->x && IS_WALL(0, 1, c[0]))
		tab[pos->y][pos->x + 1] = c[1];
}

static int	head(t_list *list, char **tab, t_pos *size, int i)
{
	t_pos	*pos;

	pos = list->first->data;
	if (i == 1)
	{
		tab[pos->y][pos->x] = '*';
		set_wall(tab, pos, size, (char [2]) {'B', 'X'});
		set_wall(tab, pos, size, (char [2]) {'A', 'B'});
	}
	if ((pos = next_pos(tab, size, pos)))
	{
		if (!(add_first_node(list, pos)))
			return (0);
		i = 1;
	}
	else
	{
		free_node(list, list->first);
		i = -1;
	}
	return (i);
}

char		**backtrack(char **tab, t_pos *size, t_pos *pos)
{
	t_list	*list;
	int	i;

	i = 1;
	if (!(list = create_list(LINKED)))
		return (0);
	if (!(add_first_node(list, next_pos(tab, size, pos))))
		return (0);
	while (list->size != 0)
		if (!(i = head(list, tab, size, i)))
			return (0);
	free_list(list);
	free(list);
	return (tab);
}
