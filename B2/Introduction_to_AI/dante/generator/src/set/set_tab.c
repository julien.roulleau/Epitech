/*
** set_tab.c for dante in /home/na/Dropbosize.x/Job/En_Cours/dante/gen/src/
**
** Made bsize.y Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Apr 20 11:18:55 2017 Roulleau Julien
** Last update Thu Apr 20 12:12:32 2017 Roulleau Julien
*/

#include "src.h"
#include "struct.h"

char		**set_tab(t_pos size, char init_char)
{
	char	**tab;
	int	i;
	int	n;

	if (!(tab = malloc(sizeof(char *) * (size.y + 1))))
		return (0);
	i = -1;
	while (++i < size.y)
	{
		if (!(tab[i] = malloc(sizeof(char) * (size.x + 1))))
			return (0);
		n = -1;
		while (++n < size.x)
			tab[i][n] = init_char;
		tab[i][size.x] = 0;
	}
	tab[i] = 0;
	return (tab);
}

void		fix_maze(char **tab)
{
	t_pos	pos;

	pos.y = -1;
	while (tab[++pos.y] && (pos.x = -1))
		while (tab[pos.y][++pos.x])
			if (tab[pos.y][pos.x] == 'A' ||
					tab[pos.y][pos.x] == 'B')
				tab[pos.y][pos.x] = 'X';
}
