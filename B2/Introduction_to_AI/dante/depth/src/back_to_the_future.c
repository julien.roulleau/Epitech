/*
** back_to_the_future.c for  in /home/zak/Documents/Modules/AI/dante/breadth/src
**
** Made by david zakrzewski
** Login   <zak@epitech.net>
**
** Started on  Thu May 11 01:19:37 2017 david zakrzewski
** Last update Sun May 14 13:25:18 2017 david zakrzewski
*/

#include "src.h"
#include "solver.h"

int		get_moves(char c)
{
  if (c == '1')
    return (1);
  if (c == '2')
    return (2);
  if (c == '3')
    return (3);
  if (c == '4')
    return (4);
  return (0);
}

void		change_to_o(t_maze *maze, t_pos *pos)
{
  while (pos->x != 0 || pos->y != 0)
    {
      if (get_moves(maze->maze[pos->y * maze->w + pos->x]) == 1)
	{
	  maze->maze[pos->y * maze->w + pos->x] = 'o';
	  pos->x++;
	}
      else if (get_moves(maze->maze[pos->y * maze->w + pos->x]) == 2)
	{
	  maze->maze[pos->y * maze->w + pos->x] = 'o';
	  pos->y++;
	}
      else if (get_moves(maze->maze[pos->y * maze->w + pos->x]) == 3)
	{
	  maze->maze[pos->y * maze->w + pos->x] = 'o';
	  pos->x--;
	}
      else if (get_moves(maze->maze[pos->y * maze->w + pos->x]) == 4)
	{
	  maze->maze[pos->y * maze->w + pos->x] = 'o';
	  pos->y--;
	}
    }
}

void		last_parsing(char *s)
{
  int		i;

  i = -1;
  while (s[++i])
    if (s[i] != '#' && s[i] != '*' && s[i] != '\n' && s[i] != 'o' &&
	s[i] != 'X')
      s[i] = '*';
}

void		back_to_the_future(t_maze *maze)
{
  t_pos		pos;

  pos.x = maze->w - 2;
  pos.y = maze->h;
  change_to_o(maze, &pos);
  last_parsing(maze->maze);
}
