/*
** get_maze.c for  in /home/zak/Documents/Modules/AI/dante/slv/src
**
** Made by david zakrzewski
** Login   <zak@epitech.net>
**
** Started on  Thu Apr 20 18:08:09 2017 david zakrzewski
** Last update Fri May 12 00:34:49 2017 david zakrzewski
*/

#include "solver.h"
#include "src.h"

char		*read_file(char *file, struct stat stfile)
{
  char		*maze;
  int		fd;

  if ((fd = open(file, O_RDONLY)) < 0)
    return (NULL);
  if ((maze = malloc(sizeof(char *) * stfile.st_size)) == NULL)
    return (NULL);
  if (read(fd, maze, stfile.st_size) < 0)
    return (NULL);
  maze[stfile.st_size] = '\0';
  close(fd);
  return (maze);
}

t_maze		get_maze(char *file)
{
  struct stat	stfile;
  t_maze	maze;
  unsigned int	w;
  unsigned int	h;

  stat(file, &stfile);
  maze.maze = NULL;
  if (!(maze.maze = read_file(file, stfile)))
    return (maze);
  w = -1;
  while (maze.maze[++w] != '\n' && maze.maze[w] != '\0');
  maze.w = w + 1;
  h = 0;
  maze.h = -1;
  while (maze.maze[++maze.h])
    if (maze.maze[maze.h] == '\n')
      h++;
  maze.h = h;
  return (maze);
}
