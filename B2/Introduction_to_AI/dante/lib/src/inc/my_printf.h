/*
** my.h for my_printf in /home/infocraft/Job/PSU_2016_my_printf/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Nov 15 11:27:49 2016 Roulleau Julien
** Last update Tue Apr 18 16:13:35 2017 Roulleau Julien
*/

#ifndef MY_PRINTF
# define MY_PRINTF

# include <stdio.h>
# include <stdarg.h>
# include <unistd.h>
# include <stdlib.h>

# include "src.h"

int 			g_count;

typedef struct 		s_list
{
	char 		*flag;
	void 		(*flag_pointer)(va_list);
}			t_list;

/* print_type */
void		my_print_char(va_list);
void		my_print_str(va_list);
void		my_print_percent(va_list);
void		my_print_str_s(va_list);
void		my_print_base_decimal(va_list);
void		my_print_base_hexdown(va_list);
void		my_print_base_hexup(va_list);
void		my_print_base_octal(va_list);
void		my_print_base_binary(va_list);
void		my_print_base_unsigned(va_list);
void		my_print_base_pointer(va_list);
void		my_print_tab(va_list va);

#endif		/* !MY_PRINTF */
