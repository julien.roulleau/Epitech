/*
** exec_list.c for lib in /home/na/Dropbox/Job/Lib/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sat Apr 15 00:29:32 2017 Roulleau Julien
** Last update Tue Apr 18 14:16:39 2017 Roulleau Julien
*/

#include "list.h"

static void 	free_tab(void **tab, int size)
{
	int 	i;

	i = -1;
	while (++i < size)
		free(tab[i]);
	free(tab);
}

void 		**tab_list(t_list *list)
{
	t_node	*node;
	void	**tab;
	int	i;

	if (!list || !list->first ||
		!(tab = malloc(sizeof(void *) * (list->size + 1))))
			return (0);
	i = -1;
	node = list->first;
	while (++i < list->size)
	{
		tab[i] = node->data;
		if (!node->next)
		{
			free_tab(tab, i);
			return (0);
		}
		node = node->next;
	}
	tab[list->size] = 0;
	return (tab);
}
