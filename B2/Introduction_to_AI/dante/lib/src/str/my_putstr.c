/*
** my_putstr.c for src in /home/infocraft/Job/Lib/my_printf/src/str/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun Jan  8 23:03:02 2017 Roulleau Julien
** Last update Fri Apr 28 15:20:04 2017 Roulleau Julien
*/

#include <unistd.h>

void		my_putnbr(int);
int		my_strlen(char *);

void		my_putstr(char *str)
{
	write(1, str, my_strlen(str));
}

void		my_putstr_s(char *str)
{
	if (*str != '\0')
	{
		if (*str < 32)
		{
			write(1, "\\0", 2);
			if (*str < 10)
				write(1, "0", 1);
			my_putnbr(*str);
		}
		else
			write(1, str, 1);
		my_putstr_s(++str);
	}
}

void		my_putstr_fd(int fd, char *str)
{
	if (*str != '\0')
	{
		write(fd, &(*str), 1);
		my_putstr_fd(fd, ++str);
	}
}
