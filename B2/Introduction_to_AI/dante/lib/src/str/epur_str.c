/*
** epur_str.c for src in /home/na/Dropbox/Job/Lib/src/str/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Mar 24 15:29:33 2017 Roulleau Julien
** Last update Mon Apr 10 17:30:26 2017 Roulleau Julien
*/

#include <stdlib.h>
int		my_strlen(char *);

char		*epur_str(char *str, char epur)
{
	char	*select;
	char	*clean;
	int	i;
	int	n;
	int	size;

	size = my_strlen(str);
	if (!(select = malloc(sizeof(char) * (size + 1))))
		return (0);
	i = n = -1;
	while (++i < size)
		if (!(str[i] == epur && str[i + 1] == epur))
			select[++n] = str[i];
	select[++n] = 0;
	size = my_strlen(select);
	if (!(clean = malloc(sizeof(char) * (size + 1))))
		return (0);
	i = n = -1;
	while (++i < size)
		clean[++n] = select[i];
	clean[++n] = 0;
	free(select);
	free(str);
	return (clean);
}

char		*purge_str(char *str, char epur)
{
	char	*select;
	char	*clean;
	int	i;
	int	n;
	int	size;

	size = my_strlen(str);
	if (!(select = malloc(sizeof(char) * (size + 1))))
		return (0);
	i = n = -1;
	while (++i < size)
		if (!(str[i] == epur))
			select[++n] = str[i];
	select[++n] = 0;
	size = my_strlen(select);
	if (!(clean = malloc(sizeof(char) * (size + 1))))
		return (0);
	i = n = -1;
	while (++i < size)
		clean[++n] = select[i];
	clean[++n] = 0;
	free(select);
	free(str);
	return (clean);
}
