/*
** solver.h for  in /home/zak/Documents/Modules/AI/dante/slv/inc
**
** Made by david zakrzewski
** Login   <zak@epitech.net>
**
** Started on  Thu Apr 20 18:05:29 2017 david zakrzewski
** Last update Thu May 11 23:09:01 2017 david zakrzewski
*/

#ifndef SOLVER_H_
# define SOLVER_H_

# include <stdio.h>
# include <stdlib.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <unistd.h>
# include "list.h"


typedef struct	s_maze
{
  unsigned int	w;
  unsigned int	h;
  char		*maze;
}		t_maze;

typedef struct	s_pos
{
  unsigned int	x;
  unsigned int	y;
}		t_pos;

/* main.c */
void		set_cur(t_pos *, t_pos);
int		return_mess(char *, int);
int		breadth_search(t_maze *);
void		solve(t_maze *, struct s_list *);
int		solver(char *);

/* adjacents.c */
t_pos		*get_pos(unsigned int, unsigned int);
void		check_adjacents(t_maze *, t_pos *, t_list *);

/* get_maze.c */
t_maze		get_maze(char *);
char	        *read_file(char *, struct stat);
int		get_wh(char **, char);

/* back_to_the_future.c */
void		back_to_the_future(t_maze *);

#endif
