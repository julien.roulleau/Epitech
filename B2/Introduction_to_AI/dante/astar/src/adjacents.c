/*
** adjacents.c for  in /home/zak/Documents/Modules/AI/dante/breadth/src
**
** Made by david zakrzewski
** Login   <zak@epitech.net>
**
** Started on  Mon May  8 01:54:00 2017 david zakrzewski
** Last update Sun May 14 13:26:40 2017 david zakrzewski
*/

#include "src.h"
#include "solver.h"

t_pos		*get_pos(unsigned int y, unsigned int x)
{
  t_pos		*pos;

  if (!(pos = malloc(sizeof(t_pos))))
    return (0);
  pos->x = x;
  pos->y = y;
  return (pos);
}

int		is_not_marked(char c)
{
  if (c == '*')
    return (1);
  return (0);
}

void		check_west_east(t_maze *maze, t_pos *cur, t_list *queue)
{
  if ((int) cur->x + 1 < (int) maze->w &&
      maze->maze[cur->y * maze->w + cur->x + 1] == '*')
    {
      add_last_node(queue, get_pos(cur->y, (cur->x + 1)));
      maze->maze[cur->y * maze->w + cur->x + 1] = '3';
    }
  if ((int) cur->x - 1 >= 0 &&
      maze->maze[cur->y * maze->w + cur->x - 1] == '*')
    {
      add_last_node(queue, get_pos(cur->y, cur->x - 1));
      maze->maze[cur->y * maze->w + cur->x - 1] = '1';
    }
}

void		check_north_south(t_maze *maze, t_pos *cur, t_list *queue)
{
  if ((int) cur->y + 1 <= (int) maze->h &&
      maze->maze[(cur->y + 1) * maze->w + cur->x] == '*')
    {
      add_last_node(queue, get_pos(cur->y + 1, cur->x));
      maze->maze[(cur->y + 1) * maze->w + cur->x] = '4';
    }
  if ((int) cur->y - 1 >= 0 &&
      maze->maze[(cur->y - 1) * maze->w + cur->x] == '*')
    {
      add_last_node(queue, get_pos(cur->y - 1, cur->x));
      maze->maze[(cur->y - 1) * maze->w + cur->x] = '2';
    }
}

void		check_adjacents(t_maze *maze, t_pos *cur, t_list *queue)
{
  check_north_south(maze, cur, queue);
  check_west_east(maze, cur, queue);
}
