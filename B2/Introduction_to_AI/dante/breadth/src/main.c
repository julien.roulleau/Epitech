/*
** main.c for solver in /home/na/Dropbox/Job/En_Cours/dante/slv/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Apr 14 21:09:12 2017 Roulleau Julien
** Last update Sun May 14 22:37:42 2017 Roulleau Julien
*/

#include "src.h"
#include "solver.h"

int		return_mess(char *str, int ret_value)
{
  my_printf("%s", str);
  return (ret_value);
}

int		solver(char *file)
{
  t_maze	maze;

  maze = get_maze(file);
  if (!maze.maze)
    return (0);
  if (!breadth_search(&maze))
    return (return_mess("No solution\n", 0));
  back_to_the_future(&maze);
  write(1, maze.maze, maze.w * (maze.h + 1));
  free(maze.maze);
  return (1);
}

void		set_cur(t_pos *cur, t_pos pos)
{
  cur->x = pos.x;
  cur->y = pos.y;
}

int		breadth_search(t_maze *maze)
{
  t_list	*queue;
  t_pos 	*cur;

  queue = create_list(LINKED);
  if (!(cur = malloc(sizeof(t_pos))))
    return (0);
  cur->x = 0;
  cur->y = 0;
  add_last_node(queue, cur);
  maze->maze[0] = 'o';
  while (queue->first)
    {
      cur = queue->first->data;
      cur = get_pos(cur->y, cur->x);
      if (cur->x == maze->w - 2 && cur->y == maze->h)
	break;
      free_node(queue, queue->first);
      check_adjacents(maze, cur, queue);
      free(cur);
    }
  free_list(queue);
  if (!queue->first)
    return (0);
  return (1);
}

int 		main(int ac, char **av)
{
  if (ac != 2)
    return (84);
  if (!(solver(av[1])))
    return (84);
  return (0);
}
