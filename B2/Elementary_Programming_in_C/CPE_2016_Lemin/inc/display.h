/*
** display.h for Project-Master in /home/thomas/epitech/CPE_2016_Lemin
**
** Made by thibault thomas
** Login   <thibault.thomas@epitech.eu>
**
** Started on  Tue Apr 25 16:11:00 2017 thibault thomas
** Last update Sun Apr 30 02:36:10 2017 Roulleau Julien
*/

#ifndef DISPLAY_H_
# define DISPLAY_H_

# include "struct.h"

void		display_line(t_ant *);
int		display_ants(t_map *, t_list *);
t_ant		*create_ants(t_ant *, int *, t_list *);
t_ant		*new_ant(t_node *, int, t_ant *);
t_ant		*delete_ant(t_ant *);
void		move_ants(t_ant *);
t_ant		*delete_head(t_ant *);
int		need_display(t_ant *);
void		free_ants(t_ant *);

#endif
