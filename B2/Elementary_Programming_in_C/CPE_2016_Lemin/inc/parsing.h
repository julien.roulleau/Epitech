/*
** lemin.h for Lemin in /home/julien/Documents/CPE_2016_Lemin/inc/
**
** Made by Julien Marescaux
** Login   <julien.marescaux@epitech.eu>
**
** Started on  Mon Apr 24 16:20:32 2017 Julien Marescaux
** Last update Mon Apr 24 17:10:05 2017 Julien Marescaux
*/

#ifndef _PARSING_H_
# define _PARSING_H_

int   check_sharp(char *str);

#endif /*_PARSING_H_*/
