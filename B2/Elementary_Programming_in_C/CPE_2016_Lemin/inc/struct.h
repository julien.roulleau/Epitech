/*
** struct.h for lemin in /home/na/Dropbox/Job/En_Cours/CPE_2016_Lemin/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Apr 10 16:45:31 2017 Roulleau Julien
** Last update Sun Apr 30 03:28:45 2017 
*/

#ifndef STRUCT_H
# define STRUCT_H

# include "list.h"

typedef enum 	e_type
{
	START,
	END,
	NODE
}		t_type;

typedef struct	s_pos
{
	int	x;
	int	y;
}		t_pos;

typedef struct		s_graph
{
	t_type		type;
	int		id;
	char		*name;
	t_pos		pos;
	struct s_graph	*previous;
	struct s_graph	**graph;
	int		mark;
}			t_graph;

typedef struct		s_map
{
	int		antz;
	t_graph		*graph;
	t_list		*list;
	t_list		*tunnel;
	int		start;
	int		end;
}			t_map;

typedef struct		s_path
{
	int		**path;
	int		*size;
}			t_path;

typedef struct		s_ant
{
  t_node		*path;
  int			id;
  int			used;
  struct s_ant		*next;
}			t_ant;
#endif /* STRUCT_H */
