/*
** path.h for lemin in /home/na/Dropbox/Job/En_Cours/CPE_2016_Lemin/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sat Apr 22 23:53:12 2017 Roulleau Julien
** Last update Sun Apr 30 12:53:28 2017 Roulleau Julien
*/

#ifndef PATH_H
# define PATH_H

# include "struct.h"

int	print_stat(t_map *);
t_list	*make_path(t_map *);
t_map	*make_map();
int	error(t_map *);

#endif /* PATH_H */
