/*
** graph.h for lem-in in /home/na/Dropbox/Job/En_Cours/CPE_2016_Lemin/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Apr  6 09:32:00 2017 Roulleau Julien
** Last update Mon Apr 24 19:48:48 2017 Julien Marescaux
*/

#ifndef GRAPH_H
# define GRAPH_H

# include "struct.h"

t_graph		*create_node(t_type, int, t_pos);
int		connect_node(t_graph *from, t_graph *to);
void 		free_graph(t_graph *);

#endif /* GRAPH_H */
