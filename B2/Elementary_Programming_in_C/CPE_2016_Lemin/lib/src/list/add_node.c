/*
** add_node.c for list in /home/na/Dropbox/Job/Lib
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Apr  4 23:04:30 2017 Roulleau Julien
** Last update Sun Apr 30 12:50:17 2017 Roulleau Julien
*/

#include <stdlib.h>
#include "list.h"

int		add_first_node(t_list *list, void *data)
{
	t_node	*node;

	if (!list || !data || !(node = malloc(sizeof(t_node))))
		return (0);
	node->data = data;
	node->next = list->first;
	if (list->type == CIRCULAR)
	{
		node->previous = list->last;
		if (list->last)
			list->last->next = node;
	}
	else
		node->previous = 0;
	if (list->first)
		list->first->previous = node;
	list->first = node;
	if (list->size == 0)
		list->last = node;
	list->size++;
	return (1);
}

int		add_last_node(t_list *list, void *data)
{
	t_node	*node;

	if (!list || !data || !(node = malloc(sizeof(t_node))))
		return (0);
	node->data = data;
	node->previous = list->last;
	if (list->type == CIRCULAR)
	{
		node->next = list->first;
		if (list->first)
			list->first->previous = node;
	}
	else
		node->next = 0;
	if (list->last)
		list->last->next = node;
	list->last = node;
	if (list->size == 0)
		list->first = node;
	list->size++;
	return (1);
}
