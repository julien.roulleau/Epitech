/*
** print_type_base.c for my_printf in /home/infocraft/Job/PSU_2016_my_printf/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Nov 16 20:59:08 2016 Roulleau Julien
** Last update Sat Apr 29 16:48:33 2017 Roulleau Julien
*/

#include "my_printf.h"

void			my_print_base_unsigned(va_list va)
{
	unsigned int	nb;

	nb = va_arg(va, unsigned int);
	my_putnbr_base_unsigned(nb, "0123456789");
}

void		my_print_base_pointer(va_list va)
{
	void	*pointer;

	pointer = va_arg(va, void *);
	if (pointer == NULL)
		write(1, "(nil)", 5);
	else
	{
		write(1, "0x", 2);
		my_putnbr_base_pointer((unsigned long int) pointer,
			"0123456789abcdef");
	}
}
