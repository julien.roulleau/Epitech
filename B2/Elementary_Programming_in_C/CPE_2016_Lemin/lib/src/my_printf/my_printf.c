/*
** my_printf.c for my_printf in /home/infocraft/Job/PSU_2016_my_printf/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Nov 14 11:53:59 2016 Roulleau Julien
** Last update Sat Apr 29 16:48:47 2017 Roulleau Julien
*/

#include "my_printf.h"

t_list		g_flag[] =
{
	{"%", &my_print_percent},
	{"d", &my_print_base_decimal},
	{"s", &my_print_str},
	{"i", &my_print_base_decimal},
	{"o", &my_print_base_octal},
	{"u", &my_print_base_unsigned},
	{"x", &my_print_base_hexdown},
	{"X", &my_print_base_hexup},
	{"c", &my_print_char},
	{"p", &my_print_base_pointer},
	{"b", &my_print_base_binary},
	{"S", &my_print_str_s},
	{"t", &my_print_tab},
	{NULL, NULL}
};

static void 	invalid_flag(char c)
{
	write(1, "%", 1);
	write(1, &c, 1);
	g_count += 2;
}

static void	print_text(char c)
{
	write(1, &c, 1);
	g_count += 1;
}

int 		my_printf(const char *arg, ...)
{
	va_list	va;
	int	i;
	int	f;

	g_count = 0;
	i = -1;
	va_start(va, arg);
	while (arg[++i])
	{
		if (arg[i] == '%')
		{
			i++;
			f = -1;
			while (g_flag[++f].flag != NULL &&
				arg[i] != g_flag[f].flag[0]);
			if (g_flag[f].flag == NULL)
				invalid_flag(arg[i]);
			else
				g_flag[f].flag_pointer(va);
		}
		else
			print_text(arg[i]);
	}
	va_end(va);
	return (g_count);
}
