/*
** str_to_wordtab.c for 42sh.c in /home/infocraft/Job/Lib/src/str/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Jan  5 11:10:01 2017 Roulleau Julien
** Last update Sun Apr 16 01:55:55 2017 Roulleau Julien
*/

#include <stdlib.h>

static void 	malloc_tab(char ***tab, char *str, char separator)
{
	int	nb;
	int	s;
	int	n;

	nb = 1;
	s = n = -1;
	while (str[++s])
		if (str[s] == separator)
			nb++;
	(*tab) = malloc(sizeof(char *) * (nb + 1));
	(*tab)[nb] = 0;
	s = -1;
	nb = 0;
	while (str[++s])
	{
		if (str[s] == separator)
		{
			(*tab)[++n] = malloc(sizeof(char) * (nb + 1));
			nb = 0;
		}
		else
			nb++;
	}
	(*tab)[++n] = malloc(sizeof(char) * (nb + 1));
	nb = 0;
}

char		**str_to_wordtab(char *str, char separator)
{
	char 	**tab;
	int	s;
	int	i;
	int	n;

	s = -1;
	i = -1;
	n = 0;
	malloc_tab(&tab, str, separator);
	while (str[++s])
	{
		if (str[s] == separator)
		{
			tab[n][++i] = 0;
			n++;
			i = -1;
		}
		else
			tab[n][++i] = str[s];
	}
	tab[n][++i] = 0;
	return (tab);
}

void 		free_tab(char **tab)
{
	int	i;

	i = -1;
	while (tab[++i])
		free(tab[i]);
	free(tab);
}
