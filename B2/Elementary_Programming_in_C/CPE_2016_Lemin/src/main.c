/*
** main.c for lemin in /home/na/Dropbox/Job/En_Cours/CPE_2016_Lemin/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Apr 10 16:42:36 2017 Roulleau Julien
** Last update Sun Apr 30 12:56:56 2017 Roulleau Julien
*/

#include "src.h"
#include "graph.h"
#include "path.h"
#include "display.h"

static void 	free_map(t_map *map)
{
	free_list(map->list);
	free_list(map->tunnel);
	free(map);
}

int		main(int argc, char **argv)
{
	t_map	*map;
	t_list	*list;
	(void)	argc;
	(void)	argv;

	if (!(map = make_map()))
		return (84);
	if (!error(map))
		return (84);
	if (!(list = make_path(map)))
		return (84);
	if (!print_stat(map))
		return (84);
	if (!display_ants(map, list))
		return (84);
	free_map(map);
	return (0);
}
