/*
** path.c for lemin in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Apr 19 23:49:09 2017 Roulleau Julien
** Last update Sat Apr 29 23:49:16 2017 Roulleau Julien
*/

#include "struct.h"
#include "src.h"

static int	delete_link_in_tab(t_graph **graph, int i)
{
	int	size;

	size = -1;
	while (graph[++size]);
	while (graph[++i])
		graph[i - 1] = graph[i];
	graph[i - 1] = 0;
	return (1);
}

static int	rm_return_link(t_graph *node, t_graph *next)
{
	int	i;

	i = -1;
	while (next->graph[++i] && next->graph[i] != node);
	if (next->graph[i] == node)
		delete_link_in_tab(next->graph, i);
	return (1);
}

static t_graph	*set_previous(t_map *map)
{
	t_list 	*file;
	t_graph	*tmp;
	int	i;

	if (!(file = create_list(LINKED)) || !add_last_node(file, map->graph))
		return (0);
	map->graph->mark = 1;
	while (file->first && (i = -1))
	{
		tmp = file->first->data;
		if (!free_node(file, file->first))
			return (0);
		while (tmp->graph[++i] && rm_return_link(tmp, tmp->graph[i]))
		{
			tmp->graph[i]->previous = tmp;
			if (tmp->graph[i]->type == END)
				return (tmp->graph[i]);
			else if (tmp->graph[i]->mark == 1);
			else if ((tmp->graph[i]->mark = 1) &&
				!add_last_node(file, tmp->graph[i]))
					return (0);
		}
	}
	return (0);
}

t_list		*make_path(t_map *map)
{
	t_list	*list;
	t_graph	*node;

	if (!(node = set_previous(map)) || !(list = create_list(LINKED)))
		return (0);
	if (!add_first_node(list, node))
		return (0);
	while (node->previous)
	{
		node = node->previous;
		if (!add_first_node(list, node))
			return (0);
	}
	return (list);
}
