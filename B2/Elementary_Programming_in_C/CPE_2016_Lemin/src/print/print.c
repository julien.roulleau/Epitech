/*
** print.c for lemin in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sat Apr 29 18:44:15 2017 Roulleau Julien
** Last update Sat Apr 29 20:45:49 2017 Roulleau Julien
*/

#include "src.h"
#include "struct.h"

static t_graph	*find_in_list(t_node *node, int id)
{
	t_graph *current;

	current = node->data;
	if (current->id == id)
		return (node->data);
	if (node->next)
		return (find_in_list(node->next, id));
	return (0);
}

static void 	print_room(t_node *node)
{
	t_graph	*graph;

	graph = node->data;
	if (graph->type != START && graph->type != END)
		my_printf("%s %i %i\n", graph->name,
			graph->pos.x, graph->pos.y);
	if (node->previous)
		print_room(node->previous);
}

static void 	print_tunnel(t_node *node)
{
	char	*str;

	str = (char *) node->data;
	my_putstr(str);
	PRINT("\n");
	if (node->next)
		print_tunnel(node->next);
}

int 		print_stat(t_map *map)
{
	t_graph	*graph;

	PRINT("#number_of_ants\n");
	my_putnbr(map->antz);
	PRINT("\n");
	PRINT("#rooms\n");
	PRINT("##start\n");
	if (!(graph = find_in_list(map->list->first, map->start)))
		return (0);
	my_printf("%s %i %i\n", graph->name, graph->pos.x, graph->pos.y);
	PRINT("##end\n");
	if (!(graph = find_in_list(map->list->first, map->end)))
		return (0);
	my_printf("%s %i %i\n", graph->name, graph->pos.x, graph->pos.y);
	print_room(map->list->last);
	PRINT("#tunnels\n");
	print_tunnel(map->tunnel->first);
	PRINT("#moves\n");
	return (1);
}
