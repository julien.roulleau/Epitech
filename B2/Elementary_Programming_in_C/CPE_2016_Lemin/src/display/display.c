/*
** display.c for Project-Master in /home/thomas/epitech/CPE_2016_Lemin
**
** Made by thibault thomas
** Login   <thibault.thomas@epitech.eu>
**
** Started on  Tue Apr 25 15:49:38 2017 thibault thomas
** Last update Sun Apr 30 02:36:24 2017 Roulleau Julien
*/

#include <stdlib.h>
#include "display.h"
#include "struct.h"
#include "src.h"

int	display_ants(t_map *map, t_list *list)
{
  int	i;
  t_ant	*master;

  i = 1;
  master = NULL;
  while (i <= map->antz)
    {
      if (master == NULL)
	{
	  if (!(master = create_ants(master, &i, list)))
	    return (0);
	}
      else
	create_ants(master, &i, list);
      move_ants(master);
      display_line(master);
    }
  while (need_display(master))
    {
      move_ants(master);
      display_line(master);
    }
  free_ants(master);
  return (1);
}

void	display_line(t_ant *ants)
{
  while (ants != NULL)
    {
      if (ants->path != NULL)
	{
	  if (ants->next != NULL)
	    my_printf("P%d-%s ", ants->id,
	    	((t_graph *) ants->path->data)->name);
	  else
	    my_printf("P%d-%s\n", ants->id,
	    	((t_graph *) ants->path->data)->name);
	}
      ants = ants->next;
    }
}

int	need_display(t_ant *ant)
{
  while (ant != NULL)
    {
      if (ant->path != NULL && ant->path->data != NULL)
	return (1);
      ant = ant->next;
    }
  return (0);
}
