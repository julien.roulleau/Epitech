/*
** display.c for Project-Master in /home/thomas/epitech/CPE_2016_Lemin
**
** Made by thibault thomas
** Login   <thibault.thomas@epitech.eu>
**
** Started on  Tue Apr 25 15:49:38 2017 thibault thomas
** Last update Sun Apr 30 02:34:49 2017 Roulleau Julien
*/

#include <stdlib.h>
#include "display.h"
#include "struct.h"

t_ant	*create_ants(t_ant *master, int *nb_ant, t_list *list)
{
  if (master == NULL)
  {
     if (!(master = new_ant(list->first, *nb_ant, master)))
       return (NULL);
  }
  else
  {
     if (!(new_ant(list->first, *nb_ant, master)))
       return (0);
  }
  *nb_ant = *nb_ant + 1;
  return (master);
}

t_ant	*new_ant(t_node *list, int ant_id, t_ant *prev)
{
  t_ant	*ant;

  if (!(ant = malloc(sizeof(*ant))))
    return (NULL);
  if (ant == NULL)
    return (NULL);
  ant->used = 0;
  ant->path = list;
  ant->id = ant_id;
  if (prev == NULL)
    {
      ant->next = NULL;
      return (ant);
    }
  while (prev->next != NULL)
    prev = prev->next;
  prev->next = ant;
  ant->next = NULL;
  return (ant);
}

void	move_ants(t_ant *ants)
{
  while (ants != NULL)
    {
      if (ants->path != NULL)
	ants->path = ants->path->next;
      ants = ants->next;
    }
}

void	free_ants(t_ant *ants)
{
  t_ant	*tmp;

  while (ants != NULL)
    {
      tmp = ants->next;
      free(ants);
      ants = tmp;
    }
}
