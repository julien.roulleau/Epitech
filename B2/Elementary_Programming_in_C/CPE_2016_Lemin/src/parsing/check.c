/*
** errors.c for Lemin in /home/julien/Documents/CPE_2016_Lemin/src/parsing/
**
** Made by Julien Marescaux
** Login   <julien.marescaux@epitech.eu>
**
** Started on  Mon Apr 24 15:25:38 2017 Julien Marescaux
** Last update Mon Apr 24 19:04:56 2017 Julien Marescaux
*/

#include "struct.h"
#include "src.h"

#define IS_COMMENT(str, i) (str[i] == '#' && str[i + 1] != '#')

int   check_sharp(char *str)
{
  int i;
  int link;

  i = -1;
  link = 0;
  while (str[++i])
  {
    if (IS_COMMENT(str, i) && !(str[i] = '\0'))
      break;
    else if ((str[i] == '#') && (str[i + 1] == '#'))
    {
      if (my_strcmp(str, "##start"))
        return (1);
      else if (my_strcmp(str, "##end"))
        return (2);
    }
    else if (str[i] == '-')
      link = 1;
  }
  if (i == 0)
    return (0);
  if (link == 1)
    return (3);
  return (-1);
}
