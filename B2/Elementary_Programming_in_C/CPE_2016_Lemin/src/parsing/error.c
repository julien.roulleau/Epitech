/*
** error.c for lemin in /home/na/Dropbox/Job/En_Cours/CPE_2016_Lemin/src/parsing/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun Apr 30 12:52:11 2017 Roulleau Julien
** Last update Sun Apr 30 13:09:40 2017 Roulleau Julien
*/

#include "src.h"
#include "struct.h"

static int	check_name_pos(t_node *node)
{
	t_node	*tmp;
	t_graph	*graph;
	t_graph	*current;

	while (node->next)
	{
		current = node->data;
		tmp = node->next;
		while (tmp)
		{
			graph = tmp->data;
			if (my_strcmp(current->name, graph->name) ||
				(current->pos.x == graph->pos.x &&
				current->pos.y == graph->pos.y))
				return (0);
			tmp = tmp->next;
		}
		node = node->next;
	}
	return (1);
}

int		error(t_map *map)
{
	if (map->start == -1 || map->end == -1)
		return (0);
	if (!check_name_pos(map->list->first))
		return (0);
	return (1);
}
