/*
** parsing.c for lemin in /home/na/Dropbox/Job/En_Cours/CPE_2016_Lemin/src/parsing/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Apr 10 16:45:14 2017 Roulleau Julien
** Last update Sun Apr 30 12:46:06 2017 Roulleau Julien
*/

#include "src.h"
#include "struct.h"
#include "parsing.h"
#include "graph.h"

#define C_LIST create_list
#define STW str_to_wordtab
#define ADD_TUNNEL() (add_last_node(map->tunnel, my_strdup(str)))

static t_graph	*find_in_list(t_node *node, char *name)
{
  t_graph 	*current;

  current = node->data;
  if (my_strcmp(current->name, name))
    return (node->data);
  if (node->next)
      return (find_in_list(node->next, name));
  return (0);
}

int           st_lnk(t_list *list, char *str)
{
  char        **tab;

  if (!(tab = STW(str, '-')))
    return (0);
  if (!(tab[0]) || !(tab[1]))
    return (0);
  if (!connect_node(find_in_list(list->first, tab[0]),
        find_in_list(list->first, tab[1])) ||
      !connect_node(find_in_list(list->first, tab[1]),
            find_in_list(list->first, tab[0])))
    return (0);
  free_tab(tab);
  return (1);
}

t_graph       *init(t_map *map, char *str, t_type type)
{
  t_graph     *graph;
  char        **tab;
  static int  id = -1;

  if (!(graph = malloc(sizeof(t_graph))) || !(tab = STW(str, ' ')))
    return (0);
  if (!tab[0] || !tab[1] || !tab[2])
    return (0);
  graph->type = type;
  graph->id = ++id;
  if ((type == START && map->start != -1) || (type == END && map->end != -1))
    return (0);
  if (type == START && (map->graph = graph))
    map->start = graph->id;
  else if (type == END)
    map->end = graph->id;
  graph->name = my_strdup(tab[0]);
  graph->pos.x = my_getnbr(tab[1]);
  graph->pos.y = my_getnbr(tab[2]);
  if (!(graph->graph = malloc(sizeof(t_graph *))))
    return (0);
  graph->graph[0] = graph->previous = 0;
  graph->mark = 0;
  free_tab(tab);
  return (graph);
}

t_map     *make_map()
{
   int    ret;
   char   *str;
   t_map  *map;

   if (!(map = malloc(sizeof(t_map))) || !(map->list = C_LIST(LINKED)) ||
   	!(map->tunnel = C_LIST(LINKED)) ||
   	!(map->antz = my_getnbr(get_next_line(0))))
    return (0);
   map->start = map->end = -1;
   while ((str = get_next_line(0)))
   {
     if (!(ret = check_sharp(str)));
     else if (ret == 1 &&
       !add_first_node(map->list, init(map, get_next_line(0), START)))
       return (0);
     else if (ret == 2 && !add_first_node(map->list,
       init(map, get_next_line(0), END)))
      return (0);
     else if (ret == 3 && (!ADD_TUNNEL() || !st_lnk(map->list, str)))
      return (0);
     else if (ret == -1 && !add_first_node(map->list, init(map, str, NODE)))
      return (0);
     free(str);
   }
   return (map);
}
