/*
** create_link.c for lemin in /home/na/Dropbox/Job/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Apr  6 09:48:34 2017 Roulleau Julien
** Last update Mon Apr 24 20:52:21 2017 Roulleau Julien
*/

#include <stdlib.h>
#include "struct.h"

t_graph		*create_node(t_type type, int id, t_pos pos)
{
	t_graph	*node;

	if (!(node = malloc(sizeof(t_graph))))
		return (0);
	node->type = type;
	node->id = id;
	node->pos = pos;
	node->graph = NULL;
	return (node);
}
