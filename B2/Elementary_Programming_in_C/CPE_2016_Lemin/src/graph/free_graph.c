/*
** free.c for lemin in /home/na/Dropbox/Job/En_Cours/CPE_2016_lemin_bootstrap/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Apr  6 11:38:55 2017 Roulleau Julien
** Last update Sat Apr 29 19:50:10 2017 Roulleau Julien
*/

#include "stdlib.h"
#include "struct.h"

void 			free_graph(t_graph *graph)
{
	int	i;

	i = -1;
	graph->mark = -1;
	if (graph->graph)
		while (graph->graph[++i])
			if (graph->graph[i]->mark != -1)
				free_graph(graph->graph[i]);
	free(graph->graph);
	free(graph);
}
