/*
** connect_link.c for lemin in /home/na/Dropbox/Job/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Apr  6 10:05:17 2017 Roulleau Julien
** Last update Mon Apr 10 17:20:35 2017 Roulleau Julien
*/

#include <stdlib.h>
#include "struct.h"

static int	last_param(t_graph **param)
{
	int	i;

	i = -1;
	if (!param)
		return (0);
	while (param[++i]);
	return (i);
}

static t_graph	**my_realloc(t_graph **node)
{
	t_graph	**tmp;
	int	size;
	int	i;

	i = -1;
	size = last_param(node) + 2;
	if (!(tmp = malloc(sizeof(t_graph) * (size))))
		return (0);
	while (++i < size)
		tmp[i] = 0;
	i = -1;
	if (node)
		while (node[++i])
			tmp[i] = node[i];
	free(node);
	return (tmp);
}

int		connect_node(t_graph *from, t_graph *to)
{
	if (from && to)
	{
		if (!(from->graph = my_realloc(from->graph)))
			return (0);
		from->graph[last_param(from->graph)] = to;
		return (1);
	}
	else
		return (0);
}
