/*
** matchstick.h for matchstick in /home/na/Dropbox/Job/En_Cours/CPE_2016_matchstick/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Feb 13 14:20:32 2017 Roulleau Julien
** Last update Thu Feb 23 15:50:47 2017 Roulleau Julien
*/

#ifndef MATCHSTICK_H
# define MATCHSTICK_H

# include <stdlib.h>

void 		print_map(int *, int);
void 		print_map_nb(int *, int);
int		win(int *, int);
int 		player(int *, int, int);
void 		ai_max(int *, int, int);
void 		ai_rand(int *, int, int);
void 		ai_grundy(int *, int, int);
void		my_putnbr(int);

#endif /* MATCHSTICK_H */
