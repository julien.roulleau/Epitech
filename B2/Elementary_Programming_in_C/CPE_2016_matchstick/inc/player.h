/*
** player.h for matchstick in /home/na/Dropbox/Job/En_Cours/CPE_2016_matchstick/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Feb 13 20:51:08 2017 Roulleau Julien
** Last update Thu Feb 16 11:34:08 2017 Roulleau Julien
*/

#ifndef PLAYER_H
# define PLAYER_H

# include <stdlib.h>

char		*get_next_line(const int);
void		my_putstr(char *);
int		my_getnbr(char *);
int		is_nbr(char *);

#endif /* PLAYER_H */
