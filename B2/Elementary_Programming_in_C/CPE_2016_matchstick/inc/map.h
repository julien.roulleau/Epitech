/*
** map.h for matchstick in /home/na/Dropbox/Job/En_Cours/CPE_2016_matchstick/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Feb 13 14:03:14 2017 Roulleau Julien
** Last update Thu Feb 16 09:56:13 2017 Roulleau Julien
*/

#ifndef MAP_H
# define MAP_H

# include <stdlib.h>

void 	my_putnbr(int);

#endif /* MAP_H */
