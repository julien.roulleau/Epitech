/*
** main.h for matchstick in /home/na/Dropbox/Job/En_Cours/CPE_2016_matchstick/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Feb 13 13:33:15 2017 Roulleau Julien
** Last update Mon Feb 20 14:23:29 2017 Roulleau Julien
*/

#ifndef MAIN_H
# define MAIN_H

# include <time.h>
# include <stdlib.h>

int		*makemap(int);
int		matchstick(int *, int, int);
int		my_getnbr(char *);
int		is_nbr(char *);

#endif /* MAIN_H */
