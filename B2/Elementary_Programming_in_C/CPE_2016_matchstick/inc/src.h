/*
** src.h for matchstick in /home/na/Dropbox/Job/En_Cours/CPE_2016_matchstick/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Feb 13 13:28:29 2017 Roulleau Julien
** Last update Thu Feb 16 11:34:05 2017 Roulleau Julien
*/

#ifndef STR_H
# define STR_H

# include <unistd.h>

# define STRLEN(str) (sizeof(str) / sizeof(str[0]) - 1)
# define PRINT(str) (write(1, str, STRLEN(str)))

# define ABS(nb) (nb < 0 ? nb * -1 : nb)

# define IS_ALPHA_UP(c) (c >= 'A' && c <= 'Z')
# define IS_ALPHA_DOWN(c) (c >= 'a' && c <= 'z')
# define IS_NUM(c) (c >= '0' && c <= '9')
# define IS_ALPHA(c) (IS_ALPHA_UP(c) || IS_ALPHA_DOWN(c))
# define IS_ALPHA_NUM(c) (IS_ALPHA(c) || IS_NUM(c))

#endif /* STR_H */
