/*
** ai_max.c for matchstick in /home/na/Dropbox/Job/En_Cours/CPE_2016_matchstick/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Feb 23 15:45:31 2017 Roulleau Julien
** Last update Sun Feb 26 11:54:44 2017 Roulleau Julien
*/

#include "ai.h"
#include "src.h"

void 		ai_max(int *map, int size, int max)
{
	int	match;
	int	line;

	PRINT("AI's turn...\n");
	line = -1;
	size++;
	while (!map[--size - 1]);
	while (++line < size - 1 && map[line] < 2);
	if (map[line] == 1 || map[line] > max + 1)
		match = 1;
	else
		match = map[line] - 1;
	map[line] -= match;
	print_info(line, match);
}
