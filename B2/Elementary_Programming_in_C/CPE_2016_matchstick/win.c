/*
** win.c for matchstick in /home/na/Dropbox/Job/En_Cours/CPE_2016_matchstick/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Feb 13 20:00:05 2017 Roulleau Julien
** Last update Mon Feb 13 21:43:17 2017 Roulleau Julien
*/

int		win(int *map, int size)
{
	int	i;

	i = -1;
	while (++i < size)
		if (map[i] != 0)
			return (1);
	return (0);
}
