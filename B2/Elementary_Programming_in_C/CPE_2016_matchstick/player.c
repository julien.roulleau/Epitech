/*
** player.c for matchstick in /home/na/Dropbox/Job/En_Cours/CPE_2016_matchstick/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Feb 13 19:59:36 2017 Roulleau Julien
** Last update Mon Feb 20 11:58:38 2017 Roulleau Julien
*/

#include "src.h"
#include "player.h"

static int	error_line(char *line, int size)
{
	if (!is_nbr(line))
	{
		PRINT("Error: invalid input (positive number expected)\n");
		return (0);
	}
	if (my_getnbr(line) < 1 || my_getnbr(line) > size)
	{
		PRINT("Error: this line is out of range\n");
		return (0);
	}
	return (1);
}

static int	error_match(char *match, char *line, int *map, int max)
{
	if (!is_nbr(match))
	{
		PRINT("Error: invalid input (positive number expected)\n");
		return (0);
	}
	if (my_getnbr(match) < 1 || my_getnbr(match) > max)
	{
		PRINT("Error: you have to remove at least one match\n");
		return (0);
	}
	if (map[my_getnbr(line) - 1] - my_getnbr(match) < 0)
	{
		PRINT("Error: not enough matches on this line\n");
		return (0);
	}
	return (1);
}

static	void 	print_info(char *line, char *match)
{
	PRINT("Player removed ");
	my_putstr(match);
	PRINT(" match(es) from line ");
	my_putstr(line);
	PRINT("\n");
}

int 		player(int *map, int size, int max)
{
	char	*line;
	char	*match;

	PRINT("Your turn:\n");
	while (1)
		while (1)
		{
			PRINT("Line: ");
			if (!(line = get_next_line(0)))
				return (0);
			if (!error_line(line, size))
				break;
			PRINT("Matches: ");
			if (!(match = get_next_line(0)))
				return (0);
			if (!error_match(match, line, map, max))
				break;
			print_info(line, match);
			map[my_getnbr(line) - 1] -= my_getnbr(match);
			free(line);
			free(match);
			return (1);
		}
}
