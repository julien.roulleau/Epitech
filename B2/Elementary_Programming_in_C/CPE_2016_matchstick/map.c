/*
** map.c for matchstick in /home/na/Dropbox/Job/En_Cours/CPE_2016_matchstick/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Feb 13 13:55:39 2017 Roulleau Julien
** Last update Fri Feb 17 22:04:16 2017 Roulleau Julien
*/

#include "map.h"
#include "src.h"

int		*makemap(int size)
{
	int 	*map;

	if (!(map = malloc(sizeof(int) * (size + 1))))
		return (0);
	map[size] = 0;
	while (--size != -1)
		map[size] = size * 2 + 1;
	return (map);
}

static void 	print_head(int size)
{
	int 	i;

	i = 0;
	while (++i < size)
	{
		PRINT("*");
	}
	PRINT("\n");
}

static void 	print_space(int size)
{
	int 	i;

	i = 0;
	while (++i < size)
	{
		PRINT(" ");
	}
}

void 		print_map(int *map, int size)
{
	int 	i;
	int	n;
	int	s;

	i = -1;
	s = size;
	size = size * 2 + 2;
	print_head(size);
	while (++i < s)
	{
		n = map[i];
		PRINT("*");
		print_space(size / 2 - i - 1);
		while (n--)
			PRINT("|");
		print_space(size / 2 + i - map[i]);
		PRINT("*");
		PRINT("\n");
	}
	print_head(size);
	PRINT("\n");
}

void 		print_map_nb(int *map, int size)
{
	int	i;

	i = -1;
	while (++i < size)
	{
		PRINT("Line ");
		my_putnbr(i + 1);
		PRINT(": ");
		my_putnbr(map[i]);
		PRINT(" match(es)");
		PRINT("\n");
	}
}
