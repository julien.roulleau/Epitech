/*
** error.c for matchstick in /home/na/Dropbox/Job/En_Cours/CPE_2016_matchstick/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Feb 13 21:19:41 2017 Roulleau Julien
** Last update Thu Feb 16 10:36:13 2017 Roulleau Julien
*/

#include "src.h"

int		is_nbr(char *str)
{
	int	i;

	i = -1;
	while (str[++i] && str[i] != '\n')
		if (!IS_NUM(str[i]))
			return (0);
	return (1);
}
