/*
** main.c for matchstick in /home/na/Dropbox/Job/En_Cours/CPE_2016_matchstick/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Feb 13 13:21:32 2017 Roulleau Julien
** Last update Mon Feb 20 14:23:24 2017 Roulleau Julien
*/

#include "main.h"

int		main(int argc, char **argv)
{
	int	*map;
	int	size;

	srand(time(0));
	if (argc != 3 || !is_nbr(argv[1]) || !is_nbr(argv[2]))
		return (84);
	size = my_getnbr(argv[1]);
	if (!(map = makemap(size)))
		return (84);
	return (matchstick(map, size, my_getnbr(argv[2])));
}
