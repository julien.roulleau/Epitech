/*
** ai_xor.c for matchstick in /home/na/Dropbox/Job/En_Cours/CPE_2016_matchstick/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Feb 16 11:47:11 2017 Roulleau Julien
** Last update Thu Feb 23 15:48:56 2017 Roulleau Julien
*/

#include "src.h"
#include "ai.h"

int		xor_map(int *map, int size)
{
	int	xor;
	int 	i;

	xor = map[0];
	i = 0;
	while (++i < size)
		xor ^= map[i];
	return (xor & 0b111);
}

int		find_map(int *map, int size, int nb)
{
	int	i;

	i = -1;
	if (nb == 0)
	{
		while (++i < size)
			if ((map[i] & 1))
				return (i);
	}
	else
	{
		while (++i < size)
			if (nb <= map[i])
				return (i);
	}
	return (0);
}

void		xor_ip(int *map, int size, int xor)
{
	int	line;
	int	match;

	match = 0;
	if ((line = find_map(map, size, xor)) ||
		(line = find_map(map, size, 7)))
			match = xor;
	else if ((line = find_map(map, size, xor - 1)) ||
		(line = find_map(map, size, 6)))
			match = xor - 2;
	map[line] -= match;
	print_info(line, match);
}

void		xor_p(int *map, int size, int xor)
{
	int	line;
	int 	match;

	match = 0;
	if ((line = find_map(map, size, xor)) ||
		(line = find_map(map, size, xor + 1)) ||
		(line = find_map(map, size, 6)) ||
		(line = find_map(map, size, 7)))
			match = xor;
	else if ((line = find_map(map, size, 4)) ||
		(line = find_map(map, size, 5)))
			match = 2;
	map[line] -= match;
	print_info(line, match);
}

void 		ai_grundy(int *map, int size, int max)
{
	int	xor;
	int	line;

	(void) max;
	PRINT("AI's turn...\n");
	xor = xor_map(map, size);
	if (xor == 1)
	{
		map[(line = find_map(map, size, 0))] -= 1;
		print_info(line, 1);
	}
	else if (xor == 7)
	{
		map[(line = find_map(map, size, 4))] -= 1;
		print_info(line, 1);
	}
	else if (!(xor & 1))
		xor_p(map, size, xor);
	else if ((xor & 1))
		xor_ip(map, size, xor);
	else
		PRINT("##############Error xor##############");
}
