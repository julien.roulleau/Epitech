/*
** matchstick.c for matchstick in /home/na/Dropbox/Job/En_Cours/CPE_2016_matchstick/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Feb 13 13:26:20 2017 Roulleau Julien
** Last update Sun Feb 26 23:51:38 2017 Roulleau Julien
*/

#include "matchstick.h"
#include "src.h"

void 	print_info(int line, int match)
{
	PRINT("AI removed ");
	my_putnbr(match);
	PRINT(" match(es) from line ");
	my_putnbr(line);
	PRINT("\n");
}

int		matchstick(int *map, int size, int max)
{
	print_map(map, size);
	while (1)
	{
		if (!player(map, size, max))
		{
			free(map);
			return (0);
		}
		print_map(map, size);
		if (!win(map, size))
		{
			PRINT("You lost, too bad...\n");
			free(map);
			return (2);
		}
		ai_rand(map, size, max);
		print_map(map, size);
		if (!win(map, size))
		{
			PRINT(
			"I lost... snif... but I'll get you next time!!\n");
			free(map);
			return (1);
		}
	}
}
