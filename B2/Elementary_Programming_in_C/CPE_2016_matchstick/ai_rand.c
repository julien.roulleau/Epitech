/*
** ai.c for matchstick in /home/na/Dropbox/Job/En_Cours/CPE_2016_matchstick/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Feb 13 19:59:49 2017 Roulleau Julien
** Last update Thu Feb 23 15:50:20 2017 Roulleau Julien
*/

#include <stdlib.h>
#include "src.h"
#include "ai.h"

static int		my_rand(double min, double max)
{
	return (rand() / (double) RAND_MAX * (max - min) + min);
}

#include "stdio.h"

static int	last(int mapl, int max)
{
	if (mapl == max + 1)
		return (max);
	else if (mapl > max)
		return (1);
	else if (mapl == 1)
		return (1);
	else
		return (mapl - 1);
}

void 		ai_rand(int *map, int size, int max)
{
	int	line;
	int	match;
	int	min;

	PRINT("AI's turn...\n");
	min = -1;
	size++;
	while (!map[++min]);
	while (!map[--size] - 1);
	line = my_rand(1, size + 1);
	while (line < 1 || line > size || !map[line - 1])
		line = my_rand(1, size + 1);
	max = max < map[line - 1] ? max : map[line - 1];
	if (size - min == 1)
		match = last(map[min], max);
	else
	{
		if (size - min == 2)
			max--;
		match = my_rand(1, max + 1);
		while (match < 1 || map[line - 1] - match < 0)
			match = my_rand(1, max + 1);
	}
	map[line - 1] -= match;
	print_info(line, match);
}
