/*
** get.h for n4s in /CPE_2016_n4s/inc/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Mon May 15 14:22:26 2017 Fesquet David
** Last update Wed May 31 16:43:45 2017 Fesquet David
*/

# ifndef __GET__
# define __GET__

#include "struct.h"

typedef struct	s_get_type
{
	t_get	id;
	int	type;
	char	*command;
	t_union	*(*get_type)();
}		t_get_type;

t_union	*get_type1();
t_union	*get_type2();
t_union	*get_type3();
t_union	*get_type4();
t_info	*get_commands(t_get);
void	free_info(t_info*);


# endif /* __GET__ */
