/*
** struct.h for n4s in /home/na/Dropbox/Job/En_Cours/CPE_2016_n4s/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu May  4 15:06:36 2017 Roulleau Julien
** Last update Sun May 28 16:11:37 2017 Fesquet David
*/

#ifndef STRUCT_H
# define STRUCT_H

typedef enum 		e_get
{
	GET_INFO_LIDAR,
	GET_CURRENT_SPEED,
	GET_CURRENT_WHEELS,
	GET_CAR_SPEED_MAX,
	GET_CAR_SPEED_MIN,
	GET_INFO_SIMTIME,
	GET_STOP
}			t_get;

typedef enum 		e_print
{
	START_SIMULATION,
	STOP_SIMULATION,
	CAR_FORWARD,
	CAR_BACKWARDS,
	WHEELS_DIR,
	CYCLE_WAIT,
	PRINT_STOP
}			t_print;

typedef struct		s_type1
{
	int		value;
	int		status;
	char		*code_str;
	char		*info;
}			t_type1;

typedef struct		s_type2
{
	int		value;
	int		status;
	char		*code_str;
	float		data[32];
	char		*info;
}			t_type2;

typedef struct		s_type3
{
	int		value;
	int		status;
	char		*code_str;
	float		data;
	char		*info;
}			t_type3;

typedef struct		s_type4
{
	int		value;
	int		status;
	char		*code_str;
	long int	data[2];
	char		*info;
}			t_type4;

typedef union		s_union
{
	t_type1		*type1;
	t_type2 	*type2;
	t_type3		*type3;
	t_type4		*type4;
}			t_union;

typedef struct		s_info
{
	int		type;
	t_union		*data;
}			t_info;

#endif /* STRUCT_H */
