/*
** ai.h for n4s in /home/na/Dropbox/Job/En_Cours/CPE_2016_n4s/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun May 28 17:06:46 2017 Roulleau Julien
** Last update Wed May 31 18:30:38 2017 Antoine Briaux
*/

#ifndef AI_H
# define AI_H

int	g_start;
float	g_dir;

void	ai();

#endif /* AI_H */
