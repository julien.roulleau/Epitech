/*
** print.h for n4s in /home/david/project/en_cour/CPE_2016_n4s/inc/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Sun May 28 16:16:00 2017 Fesquet David
** Last update Wed May 31 16:43:55 2017 Fesquet David
*/

# ifndef __PRINT__
# define __PRINT__

#include "struct.h"

typedef struct	s_print_command
{
	t_print	id;
	char	*command;
	int	type;
}		t_print_command;

t_union	*get_type1();
t_info	*print_command(t_print, float);
void	free_info(t_info*);

# endif /* __PRINT__ */
