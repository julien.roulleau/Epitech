/*
** fix.h for n4s in /home/na/Dropbox/Job/En_Cours/CPE_2016_n4s/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun May 28 17:04:10 2017 Roulleau Julien
** Last update Sun May 28 17:35:57 2017 Roulleau Julien
*/

#ifndef FIX_H
# define FIX_H

# include "struct.h"

int	fix_speed();
int	fix_direction();

#endif /* FIX_H */
