/*
** setings.h for n4s in /home/na/Dropbox/Job/En_Cours/CPE_2016_n4s/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue May 30 20:45:18 2017 Roulleau Julien
** Last update Thu Jun  1 19:48:26 2017 Roulleau Julien
*/

#ifndef SETINGS_H
# define SETINGS_H

# define S_MUL 1.2
# define S_MAX 3010
# define S_MARGE 20
# define S_MIN 150
# define S_BACK 0.3
# define S_NB_RAY 32
# define S_MORE 0.15
# define S_LESS 0.1

# define D_NB_RAY_TOT 32
# define D_NB_RAY_AVER 4
# define D_MAX_DIST 	3010
# define D_WALL_AVOID 0.3
# define D_NB_PAS 	2.

#endif /* SETINGS_H */
