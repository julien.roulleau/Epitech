/*
** fix_direction.c for n4s in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun May 28 17:05:13 2017 Roulleau Julien
** Last update Wed May 31 21:07:41 2017 Antoine Briaux
*/

#include <stdio.h>
#include <stdlib.h>
#include "get.h"
#include "print.h"
#include "setings.h"
#include "ai.h"

static float 	average(float nbr[D_NB_RAY_TOT])
{
	float 		average_left;
	float 		average_right;
	int 		i;

	average_left = 0;
	i = 0;
	while (i < D_NB_RAY_AVER)
	{
		average_left += nbr[i];
		i++;
	}
	average_left /= D_NB_RAY_AVER;
	average_right = 0;
	i = D_NB_RAY_TOT - 1;
	while (i >= D_NB_RAY_TOT - D_NB_RAY_AVER)
	{
		average_right += nbr[i];
		i--;
	}
	average_right /= D_NB_RAY_AVER;
	return ((average_left - average_right) / D_MAX_DIST);
}

static float 	check_ray(float nbr[D_NB_RAY_TOT])
{
	int			i;

	i = 0;
	while (i < D_NB_RAY_TOT)
	{
		if (nbr[i] >= 1000)
			return (1);
		i++;
	}
	return (0);
}

static void 	change_dir(float aver_diff)
{
	t_info		*info_dir;
	float 		increm;
	int 		i;

	if (!(info_dir = get_commands(GET_CURRENT_WHEELS)))
		return ;
	increm = (aver_diff - info_dir->data->type3->data) / D_NB_PAS;
	i = 0;
	while (i < D_NB_PAS)
	{
		info_dir->data->type3->data += increm;
		print_command(WHEELS_DIR, info_dir->data->type3->data);
		i++;
	}
}

int				fix_direction()
{
	t_info		*info_lidar;
	float		aver_diff;

	print_command(WHEELS_DIR, 0);
	if (!(info_lidar = get_commands(GET_INFO_LIDAR)))
		return (0);
	aver_diff = average(info_lidar->data->type2->data);
	if (check_ray(info_lidar->data->type2->data))
		change_dir(g_dir * aver_diff);
	else
	{
		if (1. / aver_diff > 1.)
			change_dir(g_dir * D_WALL_AVOID);
		else if (1. / aver_diff < - 1.)
			change_dir(g_dir * - D_WALL_AVOID);
		else
			change_dir(g_dir * D_WALL_AVOID / aver_diff);
	}
	return (1);
}
