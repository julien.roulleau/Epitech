/*
** fix_speed.c for n4s in /home/na/Dropbox/Job/En_Cours/CPE_2016_n4s/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun May 28 16:59:35 2017 Roulleau Julien
** Last update Thu Jun  1 22:12:02 2017 Antoine Briaux
*/

#include <stdio.h>
#include <math.h>
#include "setings.h"
#include "print.h"
#include "get.h"
#include "src.h"
#include "ai.h"

static int	wheels_dir()
{
	t_info	*info;
	float	wheels;
	int	dir;

	if (!(info = get_commands(GET_CURRENT_WHEELS)))
		return (S_NB_RAY / 2);
	if (info->type != 3)
		return (S_NB_RAY / 2);
	wheels = info->data->type3->data;
	dir = S_NB_RAY / 2;
	if (wheels > 0)
		dir += dir * wheels;
	else if (wheels < 0)
		dir -= dir * wheels;
	return (dir);
}

static float	average(float nbr[32])
{
	float	average;
	int	up;
	int	down;
	int	count;
	int	divv;

	up = wheels_dir();
	down = up - 1 >= 0 ? up - 1 : 0;
	count = up = down + 1;
	average = divv = 0;
	while (up < S_NB_RAY || down >= 0)
	{
		if (up < S_NB_RAY)
			average += nbr[up++] * count;
		if (down >= 0)
			average += nbr[down--] * count;
		divv += count--;
	}
	average /= divv * 2;
	return (average);
}

static float		cal_speed(float nbr[S_NB_RAY])
{
	float 		speed;
	static float	last = 0;

	speed = average(nbr);
	if (last - S_MARGE < speed && speed < last + S_MARGE)
		g_dir = -1;
	if (g_dir == -1 && speed > S_MIN)
		g_dir = 1;
	last = speed;
	speed /= S_MAX;
	speed *= S_MUL;
	if (speed > 1)
		return (1);
	return ((speed) * g_dir);
}

int			fix_speed()
{
	static float	last = 0;
	t_info	*info;
	float	speed;

	if (!(info = get_commands(GET_INFO_LIDAR)))
		return (0);
	if (info->type != 2)
		return (0);
	speed = cal_speed(info->data->type2->data);
	if (speed > 0)
	{
		speed = speed >= last ?
			last + S_MORE * speed : last - S_LESS * speed;
		dprintf(2, "CAR_FORWARD:%f\n", speed);
		free_info(print_command(CAR_FORWARD, speed));
		last = speed;
	}
	else
	{
		dprintf(2, "CAR_BACKWARDS:%f\n", -S_BACK);
		free_info(print_command(CAR_BACKWARDS, S_BACK));
		last = S_BACK;
	}
	free_info(info);
	return (1);
}
