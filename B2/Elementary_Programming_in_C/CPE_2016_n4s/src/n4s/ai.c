/*
** ai.c for n4s in /home/na/Dropbox/Job/En_Cours/CPE_2016_n4s/src/n4s/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun May 28 16:57:12 2017 Roulleau Julien
** Last update Wed May 31 18:07:18 2017 Roulleau Julien
*/

#include <stdio.h>
#include "src.h"
#include "fix.h"
#include "print.h"
#include "ai.h"

void 	exit_simulation(int nbr)
{
	t_info *info;

	if (!(info = print_command(CAR_FORWARD, 0)))
		exit (84);
	free_info(info);
	if (!(info = print_command(STOP_SIMULATION, 0)))
		exit (84);
	free_info(info);
	exit(nbr);
}

void 		start_simulation()
{
	t_info	*info;

	g_start = 1;
	g_dir = 1;
	if (!(info = print_command(START_SIMULATION, 0)))
		exit(84);
	free_info(info);
	if (!(info = print_command(CAR_FORWARD, 0)))
		exit(84);
	free_info(info);
}

void		ai()
{
	start_simulation();
	while (g_start)
		if (!fix_speed() || !fix_direction())
			exit_simulation(84);
	exit_simulation(0);
}
