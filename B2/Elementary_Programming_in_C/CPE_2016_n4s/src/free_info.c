/*
** free_info.c for n4s in /CPE_2016_n4s/src/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Wed May 31 16:36:30 2017 Fesquet David
** Last update Thu Jun  1 19:42:50 2017 Roulleau Julien
*/

#include <stdlib.h>
#include "struct.h"

static void	free_info_type1(t_type1 *info)
{
	free(info->code_str);
	free(info->info);
}

static void	free_info_type2(t_type2 *info)
{
	free(info->code_str);
	free(info->info);
}

static void	free_info_type3(t_type3 *info)
{
	free(info->code_str);
	free(info->info);
}

static void	free_info_type4(t_type4 *info)
{
	free(info->code_str);
	free(info->info);
}

void	free_info(t_info *info)
{
	if (!info)
		return;
	if (info->type == 1)
		free_info_type1(info->data->type1);
	else if (info->type == 2)
		free_info_type2(info->data->type2);
	else if (info->type == 3)
		free_info_type3(info->data->type3);
	else if (info->type == 4)
		free_info_type4(info->data->type4);
	free(info->data);
	free(info);
}
