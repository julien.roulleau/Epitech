/*
** main.c for lib in /home/na/Dropbox/Job/Lib/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Apr 14 19:43:16 2017 Roulleau Julien
** Last update Thu Jun  1 22:11:11 2017 Antoine Briaux
*/

#include <stdio.h>
#include "list.h"
#include "src.h"
#include "get.h"
#include "ai.h"

int		main()
{
	ai();
	return (0);
}
