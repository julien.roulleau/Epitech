/*
** type2.c for n4s in /CPE_2016_n4s/src/get_commands/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Mon May 15 14:20:28 2017 Fesquet David
** Last update Sun May 28 21:39:18 2017 Fesquet David
*/

#include <stdlib.h>
#include <string.h>
#include "get.h"
#include "ai.h"
#include "src.h"

static int	tab_len(char **tab)
{
	int	i;

	i = -1;
	while (tab[++i]);
	return (i);
}

static int	get_status(char *str)
{
	if (my_strcmp(str, "OK"))
		return (1);
	if (my_strcmp(str, "KO"))
		return (0);
	return (-1);
}

static int	get_data_type2(char **tab, t_union *data)
{
	int	i;

	i = -1;
	while (++i < 32)
		data->type2->data[i] = atof(tab[i]);
	return (i);
}

t_union		*get_type2()
{
	char	*output;
	char	**tab;
	t_union	*data;
	int	i;

	i = 0;
	if (!(output = get_next_line(0)))
		return (NULL);
	if (!(tab = str_to_wordtab(output, ':')))
		return (NULL);
	if (tab_len(tab) < 35)
		return (NULL);
	if (!(data = malloc(sizeof(t_union))))
		return (NULL);
	if (!(data->type2 = malloc(sizeof(t_type2))))
		return (NULL);
	data->type2->value = atoi(tab[i++]);
	data->type2->status = get_status(tab[i++]);
	data->type2->code_str = strdup(tab[i++]);
	i += get_data_type2(tab + i, data);
	data->type2->info = strdup(tab[i]);
	if (my_strcmp(data->type2->info, "Track Cleared"))
		g_start = 0;
	free(output);
	free_tab(tab);
	return (data);
}
