/*
** get_commands.c for n4s in /CPE_2016_n4s/src/get_commands/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Mon May 15 14:21:41 2017 Fesquet David
** Last update Sun May 28 16:11:37 2017 Fesquet David
*/

#include "stdlib.h"
#include "get.h"
#include "src.h"

t_get_type	g_get_fct[] =
{
		{GET_INFO_LIDAR, 2, "GET_INFO_LIDAR\n", &get_type2},
		{GET_CURRENT_SPEED, 3, "GET_CURRENT_SPEED\n", &get_type3},
		{GET_CURRENT_WHEELS, 3, "GET_CURRENT_WHEELS\n", &get_type3},
		{GET_CAR_SPEED_MAX, 3, "GET_CAR_SPEED_MAX\n", &get_type3},
		{GET_CAR_SPEED_MIN, 3, "GET_CAR_SPEED_MIN\n", &get_type3},
		{GET_INFO_SIMTIME, 4, "GET_INFO_SIMTIME\n", &get_type4},
		{GET_STOP, 0, NULL, NULL}
};

t_info	*get_commands(t_get command)
{
	t_info	*data;
	t_union	*data_union;
	int	i;

	i = 0;
	while (g_get_fct[i].id != GET_STOP && g_get_fct[i].id != command)
		i++;
	if (g_get_fct[i].id == GET_STOP)
		return (NULL);
	my_putstr(g_get_fct[i].command);
	if (!(data_union = g_get_fct[i].get_type()))
		return (NULL);
	if (!(data = malloc(sizeof(t_info))))
		return (NULL);
	data->type = g_get_fct[i].type;
	data->data = data_union;
	return (data);
}
