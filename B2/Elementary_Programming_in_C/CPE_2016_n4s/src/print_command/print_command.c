/*
** print_command.c for n4s in /CPE_2016_n4s/src/print_command/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Sun May 28 15:55:49 2017 Fesquet David
** Last update Sun May 28 21:13:04 2017 Fesquet David
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "struct.h"
#include "src.h"
#include "print.h"

t_print_command		g_print[] = {
	{START_SIMULATION, "START_SIMULATION", 1},
	{STOP_SIMULATION, "STOP_SIMULATION", 1},
	{CAR_FORWARD, "CAR_FORWARD", 2},
	{CAR_BACKWARDS, "CAR_BACKWARDS", 2},
	{WHEELS_DIR, "WHEELS_DIR", 2},
	{CYCLE_WAIT, "CYCLE_WAIT", 3},
	{PRINT_STOP, NULL, 0}
};

int	print_type(char *cmd, int type, float number)
{
	if (type == 1)
		dprintf(1, "%s\n", cmd);
	else if (type == 2)
		dprintf(1, "%s:%f\n", cmd, number);
	else if (type == 3)
		dprintf(1, "%s:%i\n", cmd, (int)number);
	return (0);
}

t_info		*print_command(t_print command, float number)
{
	t_info	*data;
	t_union	*data_union;
	int	i;

	i = 0;
	while (g_print[i].id != PRINT_STOP && g_print[i].id != command)
		i++;
	if (g_print[i].id == PRINT_STOP)
		return (NULL);
	print_type(g_print[i].command, g_print[i].type, number);
	if (!(data_union = get_type1()))
		return (NULL);
	if (!(data = malloc(sizeof(t_info))))
		return (NULL);
	data->type = 1;
	data->data = data_union;
	return (data);
}
