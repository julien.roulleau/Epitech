/*
** strcmp.c for src in /home/infocraft/Job/Lib/src/str/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Mon Jan 16 19:31:32 2017 Roulleau Julien
** Last update Mon Apr 10 17:28:45 2017 Roulleau Julien
*/

/*
** = if return 1
** != if return 0
*/
int		my_strcmp(char *str, char *cmp)
{
	int	i;

	i = -1;
	while (str[++i] && cmp[i])
		if (str[i] != cmp[i])
			return (0);
	if (!str[i] && !cmp[i])
		return (1);
	return (0);
}
