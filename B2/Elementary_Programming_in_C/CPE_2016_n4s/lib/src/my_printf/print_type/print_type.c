/*
** print_type.c for my_printf in /home/infocraft/Job/PSU_2016_my_printf/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Nov 16 14:52:29 2016 Roulleau Julien
** Last update Mon Jan  9 00:59:08 2017 Roulleau Julien
*/

#include "my_printf.h"

void		my_print_char(va_list va)
{
	char 	c;

	c = va_arg(va, int);
	write(1, &c, 1);
	g_count += 1;
}

void		my_print_str(va_list va)
{
	char 	*str;

	str = va_arg(va, char *);
	if (str == NULL)
	{
		write(1, "(null)", 6);
		g_count += 6;
	}
	else
	{
		my_putstr(str);
		g_count += my_strlen(str);
	}
}

void		my_print_percent(va_list va)
{
	(void) va;
	write(1, "%", 1);
	g_count += 1;
}

void		my_print_str_s(va_list va)
{
	char 	*str;

	str = va_arg(va, char *);
	if (str == NULL)
	{
		write(1, "(null)", 6);
		g_count += 6;
	}
	my_putstr_s(str);
}
