/*
** main.c for main.c in /home/benjamin/chikho_b/Corewar/src
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Wed Mar  1 17:29:26 2017 Benjamin
** Last update Wed Mar 29 16:34:00 2017 Benjamin
*/

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include "get_next_line.h"
#include "is_func.h"
#include "parsing.h"
#include "code.h"
#include "lib.h"
#include "op.h"

t_write		*init_w(int inst)
{
  t_write	*w;

  if ((w = malloc(sizeof(t_write))) == NULL)
    return (NULL);
  w = memset(w, 0, sizeof(t_write));
  w->c = inst;
  w->coding = 0;
  return (w);
}

static void	cut_str(char **str)
{
  int		i;

  i = my_strlen(*str);
  while (--i != 0 && (*str)[i] != '/');
  if ((*str)[i] == '/')
    ++i;
  *str = *str + i;
}

char		*new_name(char *str)
{
  char		*new;
  int		i;

  cut_str(&str);
  if ((new = malloc(sizeof(char) * (my_suuuper_strlen(str, '.') + 5))) == NULL)
    return (NULL);
  i = -1;
  while (str[++i] && str[i] != '.')
    new[i] = str[i];
  new[i] = '.';
  new[++i] = 'c';
  new[++i] = 'o';
  new[++i] = 'r';
  new[++i] = '\0';
  return (new);
}

t_list		*start_asm(char *name, header_t *header, t_data *data)
{
  t_list	*list;
  int		fd;

  data->line = 0;
  data->size = 0;
  header = my_super_memset(header, sizeof(header_t), 0);
  header->magic = reverse_bytes(COREWAR_EXEC_MAGIC).n;
  if ((fd = open(name, O_RDONLY)) == -1)
    return (NULL);
  if ((receipt_name_and_comment(header, fd, data)) == -1)
    {
      close(fd);
      return (NULL);
    }
  if ((list = init_list()) == NULL)
    return (NULL);
  if ((init_label_and_count(fd, list, data)) == -1)
    return (NULL);
  header->prog_size = reverse_bytes(data->size).n;
  close(fd);
  return (list);
}

int		main(int argc, char **argv)
{
  t_list	*list;
  header_t	header;
  t_data	data;

  if (argc == 2)
    {
      data.tb = NULL;
      if ((data.prog_name = correct_param(argv[0])) == NULL)
	return (84);
      if ((data.prog_param = correct_param(argv[1])) == NULL)
	return (84);
      if ((list = start_asm(argv[1], &header, &data)) == NULL)
	return (84);
      get_next_line(-1);
      data.line = 0;
      data.size = 0;
      if ((write_file(header, list, argv[1], &data)) == -1)
      	return (-1);
    }
  return (0);
}
