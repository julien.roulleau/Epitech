/*
** write_file.c for write_file.c in /home/benjamin/chikho_b/Corewar/src
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Wed Mar 15 19:13:49 2017 Benjamin
** Last update Tue Mar 28 11:40:37 2017 Benjamin
*/

#include <unistd.h>
#include <fcntl.h>
#include "pt_func.h"
#include "lib.h"
#include "get_next_line.h"
#include "parsing.h"
#include "is_func.h"

#define PEI print_error_int
#define STW my_str_to_wordtab

t_ptrw   wr[] =
  {
    {"live", &live_function_wr},
    {"ld", &ld_function_wr},
    {"st", &st_function_wr},
    {"add", &add_function_wr},
    {"sub", &sub_function_wr},
    {"and", &and_function_wr},
    {"or", &or_function_wr},
    {"xor", &xor_function_wr},
    {"zjmp", &zjump_function_wr},
    {"ldi", &ldi_function_wr},
    {"sti", &sti_function_wr},
    {"fork", &fork_function_wr},
    {"lld", &lld_function_wr},
    {"lldi", &lldi_function_wr},
    {"lfork", &lfork_function_wr},
    {"aff", &aff_function_wr},
    {0, NULL}
  };

int		pars_label_bis(char **tab, int p,
				t_list *list, t_data *data)
{
  int		i;
  int		j;

  i = my_suuuper_strlen(tab[0], LABEL_CHAR);
  if (tab[0][i] && tab[0][i] == LABEL_CHAR)
    {
      j = 0;
      tab[0] = tab[0] + (i + 1);
      tab[0] = spec_epur_str(tab[0], " \t");
      while (((p = my_strncmp(wr[j].flag, tab[0],
			      my_strlen(wr[j].flag))) != 0)
	     && (p < 1000000000))
	++j;
      if (wr[j].ptr != NULL && p < 1000000000)
	if ((wr[j].ptr(tab, list, data)) == -1)
	  return (-1);
    }
  return (0);
}

int		little_lamb(char **tab, t_data *data,
			t_list *list, int fd)
{
  int		p;
  int		i;

  i = 0;
  data->new_fd = fd;
  while (((p = my_strncmp(wr[i].flag, tab[0],
	  	my_strlen(wr[i].flag))) != 0)
	 && (p < 1000000000))
    ++i;
  if (wr[i].ptr != NULL)
    {
      if ((wr[i].ptr(tab, list, data)) == -1)
	return (PEI("invalid arrgument. ", 2, data, -1));
    }
  else if ((p > 1000000000 && p < 2000000000) ||
  		(is_label(tab[0], data)) != 0)
    {
      if ((pars_label_bis(tab, p, list, data)) == -1)
	return (PEI("invalid arrgument. ", 2, data, -1));
    }
  return (0);
}

int		write_file(header_t header, t_list *list,
				char *name, t_data *data)
{
  int		new_fd;
  int		fd;
  char		*str;
  char		**tab;
  int		i;

  str = new_name(name);
  if ((new_fd = open(str, O_RDWR | O_CREAT, 0666)) == -1)
    return (-1);
  free(str);
  write(new_fd, &header, sizeof(header_t));
  if ((fd = open(name, O_RDONLY)) == -1)
    return (-1);
  if ((pars_name_and_comment(fd, data)) == -1)
    return (-1);
  while ((tab = STW(get_next_line(fd), ',')) != NULL)
    {
      i = -1;
      data->line = data->line + 1;
      while (tab[++i] && (tab[i] = spec_epur_str(tab[i], " \t")));
      if (tab[0] == NULL);
      else if ((little_lamb(tab, data, list, new_fd)) == -1)
	return (-1);
    }
  return (0);
}
