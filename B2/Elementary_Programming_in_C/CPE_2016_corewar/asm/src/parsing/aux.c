/*
** norme.c for norme.c in /home/benjamin/chikho_b/Corewar/src
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Wed Mar 22 00:20:43 2017 Benjamin
** Last update Thu Mar 23 15:22:24 2017 Benjamin
*/

#include <unistd.h>
#include "parsing.h"
#include "code.h"

char		**init_tab(int *j, t_data *data, char **tab)
{
  int		i;

  i = -1;
  *j = 0;
  data->line = data->line + 1;
  while (tab[++i])
    tab[i] = spec_epur_str(tab[i], " \t");
  if (tab[0] == NULL)
    return (NULL);
  return (tab);
}

int		increase_str(char **str, t_elem *head, t_data *data)
{
  *str = *str + 1;
  return (replace_label(*str, head, data));
}
