/*
** reverse_bytes.c for California Love in /home/benjamin/
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Sun Mar 19 14:26:31 2017 Benjamin
** Last update Thu Mar 30 16:20:06 2017 Roulleau Julien
*/

#include "struct.h"
#include "define.h"
#include "lib.h"

u_bytes		reverse_bytes(int nb)
{
  char          n;
  u_bytes       bytes;

  bytes.n = nb;
  if ((is_big_endian()) == 1)
    return (bytes);
  n = bytes.b[0];
  bytes.b[0] = bytes.b[3];
  bytes.b[3] = n;
  n = bytes.b[1];
  bytes.b[1] = bytes.b[2];
  bytes.b[2] = n;
  return (bytes);
}

s_bytes		reverse_two_bytes(short int nb)
{
  char		n;
  s_bytes	bytes;

  bytes.n = nb;
  if ((is_big_endian()) == 1)
    return (bytes);
  n = bytes.b[0];
  bytes.b[0] = bytes.b[1];
  bytes.b[1] = n;
  return (bytes);
}

int		is_big_endian()
{
  int		x;

  x = 1;
  if ((int) (((char *)&x)[0]) == 1)
    return (0);
  return (1);
}
