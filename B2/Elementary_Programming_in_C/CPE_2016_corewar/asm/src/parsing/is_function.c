/*
** is_function.c for OOOOOKKKKKKK in /home/benjamin/chikho_b/Corewar/src
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Thu Mar  9 23:00:36 2017 Benjamin
** Last update Tue Mar 28 11:14:25 2017 Benjamin
*/

#include "parsing.h"
#include "lib.h"
#include "is_func.h"

int	fill_coding_byte(char *c, char *line, int place, t_bool *tb)
{
  if (tb->d == 1 && line[0] == DIRECT_CHAR)
    {
      *c |= 1 << place;
      *c |= 0 << (place - 1);
    }
  else if (tb->i == 1 && (is_legal_char(line[0], ":-0123456789")) == 1)
    {
      *c |= 1 << place;
      *c |= 1 << (place - 1);
    }
  else if (tb->r == 1 && line[0] == 'r')
    {
      *c |= 0 << place;
      *c |=  1<< (place - 1);
    }
  return (0);
}

unsigned int	which_value(char *str, t_list *list, t_data *data, t_bool *tb)
{
  if (tb->d == 1 && str[0] == DIRECT_CHAR)
    {
      str = str + 1;
      if (str[0] == LABEL_CHAR)
	return (increase_str(&str, list->head, data));
      else
  	return (my_getnbr(str));
    }
  else if (tb->i == 1 && (is_legal_char(str[0], ":-0123456789")) == 1)
    {
      if (str[0] == LABEL_CHAR)
	return (increase_str(&str, list->head, data));
      else
  	return (my_getnbr(str));
    }
  else if (tb->r == 1 && str[0] == 'r')
    {
      str = str + 1;
      return (my_getnbr(str));
  }
  return (0);
}

int		which_bytes(char *str, t_bool *tb)
{
  if (str == 0)
    return (-1);
  if (tb->d == 1 && str[0] == DIRECT_CHAR)
    {
      if ((pars_direct(str + 1)) == -1)
      	return (-1);
      return (4);
    }
  else if (tb->i == 1 && (is_legal_char(str[0], ":-0123456789")) == 1)
    {
      if ((pars_indirect(str)) == -1)
	return (-1);
      return (2);
    }
  else if (tb->r == 1 && str[0] == 'r')
    {
      if ((pars_register(str + 1)) == -1)
	return (-1);
      return (1);
    }
  else
    return (-1);
}

int		is_legal_char(char c, char *cmp)
{
  int		i;

  i = -1;
  while (cmp[++i])
    if (cmp[i] == c)
      return (1);
  if (cmp[i] == '\0' && c == '\0')
    return (1);
  return (0);
}

int		is_label(char *str, t_data *data)
{
  int		i;
  int		j;

  i = -1;
  if (!str)
    return (print_error_int("invalid instruction. ", 2, data, -1));
  while ((str[++i] && (is_legal_char(str[i], LABEL_CHARS)) == 1) &&
	 (str[i] != LABEL_CHAR));
  if (str[i] == '\0')
    return (3);
  if (str[i] != LABEL_CHAR)
    return (-1);
  j = i;
  while ((--j > 0) && (is_legal_char(str[j], LABEL_CHARS)) == 1);
  if (j == 0)
    return (1);
  return (1);
}
