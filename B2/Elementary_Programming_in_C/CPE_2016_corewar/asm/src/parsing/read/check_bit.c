/*
** check_bit.c for check_bit in /home/benjamin/chikho_b/Corewar/src
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Sat Mar 18 18:57:21 2017 Benjamin
** Last update Wed Mar 22 21:03:54 2017 Roulleau Julien
*/

#include "struct.h"
#include "define.h"

int		check_coding_byte(char coding, int place, t_data *data)
{
  if (((CHECK_BIT(coding, place)) == 1) &&
      (CHECK_BIT(coding, (place - 1))) == 0)
    {
      (void)data;
      return (4);
    }
  else if (((CHECK_BIT(coding, place)) == 1) &&
	   (CHECK_BIT(coding, (place - 1))) == 1)
    {
      (void)data;
      return (2);
    }
  else if (((CHECK_BIT(coding, place)) == 0) &&
	   (CHECK_BIT(coding, (place - 1))) == 1)
    {
      (void)data;
      return (1);
    }
  return (0);
}

int		increase_data_size(char coding, t_data *data, char b)
{
  int		nb;
  int		res;

  res = 0;
  nb = 0;
  nb = check_coding_byte(coding, 7, data);
  if (nb == 4 && b == 1)
    nb = nb - 2;
  res = res + nb;
  nb = check_coding_byte(coding, 5, data);
  if (nb == 4 && b == 1)
    nb = nb - 2;
  res = res + nb;
  nb = check_coding_byte(coding, 3, data);
  if (nb == 4 && b == 1)
    nb = nb - 2;
  res = res + nb;
  data->size = data->size + res;
  data->size = data->size + 2;
  return (0);
}
