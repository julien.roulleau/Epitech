/*
** init_label_and_comment.c for init_label_and_comment.c in
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Thu Mar  9 11:11:10 2017 Benjamin
** Last update Tue Mar 28 11:39:49 2017 Benjamin
*/

#include <unistd.h>
#include "pt_func.h"
#include "lib.h"
#include "is_func.h"
#include "parsing.h"

t_ptr   ptr[] =
  {
    {"live", &live_function},
    {"ld", &ld_function},
    {"st", &st_function},
    {"add", &add_function},
    {"sub", &sub_function},
    {"and", &and_function},
    {"or", &or_function},
    {"xor", &xor_function},
    {"zjmp", &zjump_function},
    {"ldi", &ldi_function},
    {"sti", &sti_function},
    {"fork", &fork_function},
    {"lld", &lld_function},
    {"lldi", &lldi_function},
    {"lfork", &lfork_function},
    {"aff", &aff_function},
    {0, NULL}
  };

int		pars_label(char **tab, int p, t_list *list, t_data *data)
{
  int   i;
  int   j;

  (void) list;
  i = my_suuuper_strlen(tab[0], LABEL_CHAR);
  if (tab[0][i] && tab[0][i] == LABEL_CHAR)
    {
      j = 0;
      tab[0] = tab[0] + (i + 1);
      tab[0] = spec_epur_str(tab[0], " \t");
      while (((p = my_strncmp(ptr[j].flag, tab[0],
			      my_strlen(ptr[j].flag))) != 0)
	     && (p < 1000000000))
	++j;
      if (ptr[j].ptr != NULL && p < 1000000000)
	if ((ptr[j].ptr(tab, data)) == -1)
	  return (-1);
    }
  return (0);
}

int		init_label_bis(char **tab, t_data *data, t_list *list, int p)
{
  int		j;

  if ((tab = init_tab(&j, data, tab)) == NULL)
    return (1);
  while (((p = my_strncmp(ptr[j].flag, tab[0],
			  my_strlen(ptr[j].flag))) != 0)
	 && (p < 1000000000))
    ++j;
  if (ptr[j].ptr != NULL &&
      (tab[0][my_strlen(ptr[j].flag)] != LABEL_CHAR))
    {
      if ((ptr[j].ptr(tab, data)) == -1)
	return (print_error_int("invalid arrgument.", 2, data, -1));
    }
  else if ((p > 1000000000 && p < 2000000000) ||
	   ((p = is_label(tab[0], data))) == 1)
    {
      add_in_list(list, give_label(tab[0]), data->size);
      if ((pars_label(tab, p, list, data)) == -1)
	return (print_error_int("invalid arrgument.", 2, data, -1));
    }
  else if (tab[0][0] != '\0')
    return (print_error_int("invalid instruction.", 2, data, -1));
  return (p);
}

int		init_label_and_count(int fd, t_list *list, t_data *data)
{
  char		**tab;
  int		p;

  p = 0;
  while ((tab = my_str_to_wordtab(spec_epur_str(get_next_line(fd), ","), ','))
	 != NULL)
    {
      if ((init_label_bis(tab, data, list, p)) == -1)
	{
	  free_list_and_data(list, data);
	  free_tab(tab, tablen(tab));
	  return (-1);
	}
      free_tab(tab, tablen(tab));
    }
  return (0);
}
