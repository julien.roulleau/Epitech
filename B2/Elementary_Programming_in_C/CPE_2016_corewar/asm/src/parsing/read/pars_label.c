/*
** pars_label.c for Origin of Symmetry in /home/benjamin/
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Fri Mar 10 16:35:53 2017 Benjamin
** Last update Thu Mar 30 16:19:47 2017 Roulleau Julien
*/

#include "parsing.h"
#include "lib.h"
#include "get_next_line.h"

int		replace_label(char *str, t_elem *head, t_data *data)
{
  t_elem	*tmp;

  tmp = head;
  while (tmp)
    {
      if ((my_strcmp(str, tmp->name)) == 0)
	return (tmp->size - data->size);
      tmp = tmp->next;
    }
  my_suuper_putstr(data->prog_name, 2);
  write(2, ", ", 2);
  my_suuper_putstr(data->prog_param, 2);
  write(2, ", line ", 7);
  my_put_nbr(data->line);
  my_suuper_putstr(": Undefined label\n", 2);
  exit(84);
  return (-1);
}

int		pars_name_and_comment(int fd, t_data *data)
{
  char		*buf;
  int		comment;
  int		name;

  comment = 0;
  name = 0;
  while ((name == 0 || comment == 0) &&
	 (buf = get_next_line(fd)) != NULL)
    {
      data->line = data->line + 1;
      buf = spec_epur_str(buf, " \t");
      if (buf[0] == '.')
	{
	  if (name == 0)
	    if ((my_strncmp(buf, ".name", 5)) == 0)
	      name = 1;
	  if (name == 1 && comment == 0)
	    if ((my_strncmp(buf, ".comment", 8)) == 0)
	      comment = 1;
	}
      else if (buf[0] != '\0')
	return (-1);
    }
  return (0);
}

char		*give_label(char *str)
{
  int		i;
  char		*new;

  i = my_suuuper_strlen(str, ':');
  if ((new = malloc(sizeof(char) * (i + 1))) == NULL)
    return (NULL);
  i = -1;
  while (str[++i] && str[i] != ':')
    new[i] = str[i];
  new[i] = '\0';
  return (new);
}
