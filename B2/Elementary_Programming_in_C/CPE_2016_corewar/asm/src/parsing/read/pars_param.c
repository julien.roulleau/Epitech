/*
** pars_param.c for pars_param.c in /home/benjamin/chikho_b/Corewar/src
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Tue Mar 14 19:51:43 2017 Benjamin
** Last update Thu Mar 23 15:08:05 2017 Benjamin
*/

#include <stdlib.h>
#include "is_func.h"
#include "parsing.h"
#include "lib.h"

t_bool		*mk_bool(int r, int i, int d)
{
  t_bool	*bool;

  if ((bool = malloc(sizeof(t_bool))) == NULL)
    return (NULL);
  bool->r = r;
  bool->i = i;
  bool->d = d;
  return (bool);
}

int		pars_indirect(char *str)
{
  int		i;

  i = 0;
  if (str[i] == ':')
    {
      ++i;
      return (0);
    }
  while (str[i])
    {
      if ((is_legal_char(str[i], "-0123456789")) == 0)
	return (-1);
      ++i;
    }
  return (0);
}

int		pars_direct(char *str)
{
  int		i;

  i = 0;
  if (str[i] == 0)
    return (-1);
  if (str[i] == ':')
    {
      ++i;
      return (0);
    }
  while (str[i])
    {
      if ((is_legal_char(str[i], "-0123456789")) == 0)
	return (-1);
      ++i;
    }
  return (0);
}

int		pars_register(char *str)
{
  int		nb;

  nb = my_getnbr(str);
  if (nb >= REG_NUMBER || nb <= 0)
    return (-1);
  return (0);
}
