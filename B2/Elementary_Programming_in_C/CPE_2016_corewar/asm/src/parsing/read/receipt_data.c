/*
** receipt_data.c for receipt_data.c in /home/benjamin/chikho_b/
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Mon Mar  6 15:32:52 2017 Benjamin
** Last update Thu Mar 30 16:19:37 2017 Roulleau Julien
*/

#include "get_next_line.h"
#include "lib.h"
#include "parsing.h"

static	int	fill_data(char *name, char *str, int max, t_data *data)
{
  int		i;

  i = -1;
  while (str[++i] && str[i] != '"')
    if (i < max)
      name[i] = str[i];
  name[i] = '\0';
  if ((str[i] == '\0') || (name[i] == '\0' && i == 0) || str[++i] != '\0')
    {
      my_suuper_putstr(data->prog_name, 2);
      write(2, ", ", 2);
      my_suuper_putstr(data->prog_param, 2);
      write(2, ", line ", 7);
      my_put_nbr(data->line);
      my_suuper_putstr(": Syntax error.\n", 2);
      exit(84);
    }
  return (i);
}

char		*skip_shit_line(int fd, t_data *data)
{
  char		*buf;
  char		ok;

  ok = 0;
  while (ok == 0)
    {
      if ((buf = get_next_line(fd)) == NULL)
	return (NULL);
      data->line = data->line + 1;
      buf = spec_epur_str(buf, " \t\0");
      if (buf[0] == '.')
	ok = 1;
      else if (buf[0] != '\0' && buf[0] != '.')
	return (NULL);
      if (ok == 0)
      	free(buf);
    }
  return (buf);
}

int		receipt_data(char *name, char *str, int fd, t_data *data)
{
  char		*buf;
  int		i;

  i = 0;
  if ((buf = skip_shit_line(fd, data)) == NULL)
    return (print_error_int("Invalid instruction.", 2, data, -1));
  while (buf[i] && buf[i] != '.')
    ++i;
  buf = buf + i;
  if (buf[i] == '\0')
    return (print_error_int("Invalid instruction.", 2, data, -1));
  if ((my_strncmp(buf, str, my_strlen(str))) != 0)
    return (print_error_int("Invalid instruction.", 2, data, -1));
  while (buf[i] && buf[i] != '"')
    i = i + 1;
  if (((my_strncmp(buf, ".name", 5)) == 0) &&
      (fill_data(name, buf + (i + 1),
		 PROG_NAME_LENGTH, data)) > PROG_NAME_LENGTH)
    return (print_error_int("The program name is too long.", 2, data, -1));
  else if (((my_strncmp(buf, ".comment", 8)) == 0) &&
	   (fill_data(name, buf + (i + 1),
		      COMMENT_LENGTH, data)) > COMMENT_LENGTH)
    return (print_error_int("The comment is too long.", 2, data, -1));
  return (0);
}

int		receipt_name_and_comment(header_t *header,
				int fd, t_data *data)
{
  my_memset(header->prog_name, PROG_NAME_LENGTH, '\0');
  my_memset(header->comment, COMMENT_LENGTH, '\0');
  if ((receipt_data(header->prog_name, ".name", fd, data)) == -1)
    return (-1);
  if ((receipt_data(header->comment, ".comment", fd, data)) == -1)
    return (-1);
  return (0);
}
