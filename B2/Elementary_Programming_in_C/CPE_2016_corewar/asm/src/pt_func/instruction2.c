/*
** instruction2.c for We don't need no more trouble in
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Thu Mar  9 21:36:40 2017 Benjamin
** Last update Wed Mar 22 21:43:55 2017 Roulleau Julien
*/

#include "pt_func.h"
#include "lib.h"
#include "is_func.h"

int		and_function(char **line, t_data *data)
{
  int		nb;
  int		i;

  i = 0;
  data->size = data->size + 2;
  line[i] = line[i] + 4;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  if ((nb = which_bytes(line[i], mk_bool(1, 1, 1))) == -1)
    return (-1);
  data->size = data->size + nb;
  if ((nb = which_bytes(line[++i], mk_bool(1, 1, 1))) == -1)
    return (-1);
  data->size = data->size + nb;
  if ((nb = which_bytes(line[++i], mk_bool(1, 0, 0))) == -1)
    return (-1);
  data->size = data->size + nb;
  return ((line[++i]) ? -1 : 0);
}

int		or_function(char **line, t_data *data)
{
  int		nb;
  int		i;

  i = 0;
  data->size = data->size + 2;
  line[i] = line[i] + 3;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  if ((nb = which_bytes(line[i], mk_bool(1, 1, 1))) == -1)
    return (-1);
  data->size = data->size + nb;
  if ((nb = which_bytes(line[++i], mk_bool(1, 1, 1))) == -1)
    return (-1);
  data->size = data->size + nb;
  if ((nb = which_bytes(line[++i], mk_bool(1, 0, 0))) == -1)
    return (-1);
  data->size = data->size + nb;
  return ((line[++i]) ? -1 : 0);
}

int		xor_function(char **line, t_data *data)
{
  int		nb;
  int		i;

  i = 0;
  data->size = data->size + 2;
  line[i] = line[i] + 4;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  if ((nb = which_bytes(line[i], mk_bool(1, 1, 1))) == -1)
    return (-1);
  data->size = data->size + nb;
  if ((nb = which_bytes(line[++i], mk_bool(1, 1, 1))) == -1)
    return (-1);
  data->size = data->size + nb;
  if ((nb = which_bytes(line[++i], mk_bool(1, 0, 0))) == -1)
    return (-1);
  data->size = data->size + nb;
  return ((line[++i]) ? -1 : 0);
}

int		zjump_function(char **line, t_data *data)
{
  int		i;

  i = 0;
  line[i] = line[i] + 5;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  if ((which_bytes(line[i], mk_bool(0, 0, 1))) == -1)
    return (-1);
  data->size = data->size + 3;
  return ((line[++i]) ? -1 : 0);
}

int		ldi_function(char **line, t_data *data)
{
  int		nb;
  int		i;

  i = 0;
  line[i] = line[i] + 4;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  data->size = data->size + 2;
  if ((nb = which_bytes(line[i], mk_bool(1, 1, 1))) == -1)
    return (-1);
  if (nb == 2 || nb == 4)
    data->size = data->size + 2;
  else if (nb == 1)
    data->size = data->size + 1;
  if ((nb = which_bytes(line[++i], mk_bool(1, 0, 1))) == -1)
    return (-1);
  if (nb == 4)
    data->size = data->size + 2;
  else if (nb == 1)
    data->size = data->size + 1;
  if ((nb = which_bytes(line[++i], mk_bool(1, 0, 0))) == -1)
    return (-1);
  if (nb == 1)
    data->size = data->size + 1;
  return ((line[++i]) ? -1 : 0);
}
