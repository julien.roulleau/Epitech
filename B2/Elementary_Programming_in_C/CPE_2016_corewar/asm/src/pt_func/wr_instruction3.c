/*
** wr_instruction3.c for Chronic in /home/benjamin
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Thu Mar 16 00:38:29 2017 Benjamin
** Last update Thu Mar 30 16:21:00 2017 Roulleau Julien
*/

#include <unistd.h>
#include "pt_func.h"
#include "lib.h"
#include "is_func.h"

int		sti_function_wr(char **line, t_list *list, t_data *data)
{
  t_write	*w;
  int		i;

  i = 0;
  w = init_w(11);
  line[i] = line[i] + 4;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  fill_coding_byte(&(w->coding), line[i], 7, mk_bool(1, 0, 0));
  fill_coding_byte(&(w->coding), line[++i], 5, mk_bool(1, 1, 1));
  fill_coding_byte(&(w->coding), line[++i], 3, mk_bool(1, 0, 1));
  write(data->new_fd, &(w->c), sizeof(unsigned char));
  write(data->new_fd, &(w->coding), sizeof(unsigned char));
  i = i - 2;
  data->place = 7;
  data->tb = mk_bool(1, 0, 0);
  ldi_function_wr_next(line[i], list, data, w);
  data->place = 5;
  data->tb = mk_bool(1, 1, 1);
  ldi_function_wr_next(line[++i], list, data, w);
  data->place = 3;
  data->tb = mk_bool(1, 0, 1);
  ldi_function_wr_next(line[++i], list, data, w);
  increase_data_size(w->coding, data, 1);
  return (0);
}

int		fork_function_wr(char **line, t_list *list, t_data *data)
{
  t_write	*w;
  int		i;
  int		nb;

  i = 0;
  if ((w = init_w(12)) == NULL)
    return (-1);
  line[i] = line[i] + 5;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  nb = which_value(line[i], list, data, mk_bool(0, 0, 1));
  write(data->new_fd, &(w->c), sizeof(unsigned char));
  w->k = reverse_two_bytes((unsigned short int)nb).n;
  write(data->new_fd, &(w->k), sizeof(unsigned short int));
  data->size = data->size + 3;
  return (0);
}

int		lld_function_wr(char **line, t_list *list, t_data *data)
{
  t_write	*w;
  int		i;

  i = 0;
  if ((w = init_w(13)) == NULL)
    return (-1);
  line[i] = line[i] + 4;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  fill_coding_byte(&(w->coding), line[i], 7, mk_bool(0, 1, 1));
  fill_coding_byte(&(w->coding), line[++i], 5, mk_bool(1, 0, 0));
  write(data->new_fd, &(w->c), sizeof(unsigned char));
  write(data->new_fd, &(w->coding), sizeof(unsigned char));
  --i;
  data->place = 7;
  data->tb = mk_bool(0, 1, 1);
  lld_function_wr_next(line[i], list, data, w);
  data->place = 5;
  data->tb = mk_bool(1, 0, 0);
  lld_function_wr_next(line[++i], list, data, w);
  increase_data_size(w->coding, data, 0);
  return (0);
}

int		lldi_function_wr(char **line, t_list *list, t_data *data)
{
  t_write	*w;
  int		i;

  i = 0;
  w = init_w(14);
  line[i] = line[i] + 5;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  fill_coding_byte(&(w->coding), line[i], 7, mk_bool(1, 1, 1));
  fill_coding_byte(&(w->coding), line[++i], 5, mk_bool(1, 0, 1));
  fill_coding_byte(&(w->coding), line[++i], 3, mk_bool(1, 0, 0));
  write(data->new_fd, &(w->c), sizeof(unsigned char));
  write(data->new_fd, &(w->coding), sizeof(unsigned char));
  i = i - 2;
  data->place = 7;
  data->tb = mk_bool(1, 1, 1);
  ldi_function_wr_next(line[i], list, data, w);
  data->place = 5;
  data->tb = mk_bool(1, 0, 1);
  ldi_function_wr_next(line[++i], list, data, w);
  data->place = 3;
  data->tb = mk_bool(1, 0, 0);
  ldi_function_wr_next(line[++i], list, data, w);
  increase_data_size(w->coding, data, 1);
  return (0);
}

int		lfork_function_wr(char **line, t_list *list, t_data *data)
{
  t_write	*w;
  int		i;
  int		nb;

  i = 0;
  if ((w = init_w(15)) == NULL)
    return (-1);
  line[i] = line[i] + 6;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  nb = which_value(line[i], list, data, mk_bool(0, 0, 1));
  write(data->new_fd, &(w->c), sizeof(unsigned char));
  w->k = reverse_two_bytes((unsigned short int)nb).n;
  write(data->new_fd, &(w->k), sizeof(unsigned short int));
  data->size = data->size + 3;
  return (0);
}
