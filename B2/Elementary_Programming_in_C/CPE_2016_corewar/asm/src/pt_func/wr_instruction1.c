/*
** wr_instruction1.c for Hououin Kyouma in /home/benjamin/
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Thu Mar 16 00:31:51 2017 Benjamin
** Last update Thu Mar 30 16:20:52 2017 Roulleau Julien
*/

#include <unistd.h>
#include "pt_func.h"
#include "lib.h"
#include "is_func.h"

int		live_function_wr(char **line, t_list *list, t_data *data)
{
  t_write	*w;
  int		i;
  int		nb;

  i = 0;
  if ((w = init_w(1)) == NULL)
    return (-1);
  line[i] = line[i] + 5;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  nb = which_value(line[i], list, data, mk_bool(0, 0, 1));
  w->n = reverse_bytes(nb).n;
  write(data->new_fd, &(w->c), sizeof(unsigned char));
  write(data->new_fd, &(w->n), sizeof(unsigned int));
  data->size = data->size + 5;
  return (0);
}

int		ld_function_wr(char **line, t_list *list, t_data *data)
{
  t_write	*w;
  int		i;
  unsigned int	nb;

  i = 0;
  if ((w = init_w(2)) == NULL)
    return (-1);
  line[i] = line[i] + 3;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  fill_coding_byte(&(w->coding), line[i], 7, mk_bool(0, 1, 1));
  fill_coding_byte(&(w->coding), line[++i], 5, mk_bool(1, 0, 0));
  --i;
  write(data->new_fd, &(w->c), sizeof(unsigned char));
  write(data->new_fd, &(w->coding), sizeof(unsigned char));
  ld_function_wr_next(line[i], list, data, w);
  nb = which_value(line[++i], list, data, mk_bool(1, 0, 0));
  if ((check_coding_byte(w->coding, 5, data)) == 1)
    {
      w->c = nb;
      write(data->new_fd, &(w->c), sizeof(unsigned char));
    }
  increase_data_size(w->coding, data, 0);
  return (0);
}

int		st_function_wr(char **line, t_list *list, t_data *data)
{
  t_write	*w;
  int		i;
  int		nb;

  i = 0;
  if ((w = init_w(3)) == NULL)
    return (-1);
  line[i] = line[i] + 3;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  fill_coding_byte(&(w->coding), line[i], 7, mk_bool(1, 0, 0));
  fill_coding_byte(&(w->coding), line[++i], 5, mk_bool(1, 1, 0));
  --i;
  write(data->new_fd, &(w->c), sizeof(unsigned char));
  write(data->new_fd, &(w->coding), sizeof(unsigned char));
  nb = which_value(line[i], list, data, mk_bool(1, 0, 0));
  if ((check_coding_byte(w->coding, 7, data)) == 1)
    {
      w->c = nb;
      write(data->new_fd, &(w->c), sizeof(unsigned char));
    }
  st_function_wr_next(line[++i], list, data, w);
  increase_data_size(w->coding, data, 0);
  return (0);
}

int		add_function_wr(char **line, t_list *list, t_data *data)
{
  t_write	*w;
  int		i;

  i = 0;
  if ((w = init_w(4)) == NULL)
    return (-1);
  line[i] = line[i] + 4;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  fill_coding_byte(&(w->coding), line[i], 7, mk_bool(1, 0, 0));
  fill_coding_byte(&(w->coding), line[++i], 5, mk_bool(1, 0, 0));
  fill_coding_byte(&(w->coding), line[++i], 3, mk_bool(1, 0, 0));
  write(data->new_fd, &(w->c), sizeof(unsigned char));
  write(data->new_fd, &(w->coding), sizeof(unsigned char));
  i = i - 2;
  data->place = 7;
  data->tb = mk_bool(1, 0, 0);
  add_sub_function_wr_next(line[i], list, data, w);
  data->place = 5;
  add_sub_function_wr_next(line[++i], list, data, w);
  data->place = 3;
  add_sub_function_wr_next(line[++i], list, data, w);
  increase_data_size(w->coding, data, 0);
  return (0);
}

int		sub_function_wr(char **line, t_list *list, t_data *data)
{
  t_write	*w;
  int		i;

  i = 0;
  if ((w = init_w(5)) == NULL)
    return (-1);
  line[i] = line[i] + 4;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  fill_coding_byte(&(w->coding), line[i], 7, mk_bool(1, 0, 0));
  fill_coding_byte(&(w->coding), line[++i], 5, mk_bool(1, 0, 0));
  fill_coding_byte(&(w->coding), line[++i], 3, mk_bool(1, 0, 0));
  write(data->new_fd, &(w->c), sizeof(unsigned char));
  write(data->new_fd, &(w->coding), sizeof(unsigned char));
  i = i - 2;
  data->place = 7;
  data->tb = mk_bool(1, 0, 0);
  add_sub_function_wr_next(line[i], list, data, w);
  data->place = 5;
  add_sub_function_wr_next(line[++i], list, data, w);
  data->place = 3;
  add_sub_function_wr_next(line[++i], list, data, w);
  increase_data_size(w->coding, data, 0);
  return (0);
}
