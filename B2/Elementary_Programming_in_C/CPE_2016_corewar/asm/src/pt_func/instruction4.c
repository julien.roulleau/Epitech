/*
** instruction4.c for Positive Vibration in /home/benjamin/
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Thu Mar  9 21:40:41 2017 Benjamin
** Last update Thu Mar 30 16:20:48 2017 Roulleau Julien
*/

#include "pt_func.h"
#include "lib.h"
#include "is_func.h"

int		aff_function(char **line, t_data *data)
{
  int		i;

  i = 0;
  line[i] = line[i] + 4;
  if ((which_bytes(line[i], mk_bool(1, 0, 0))) == -1)
    return (-1);
  data->size = data->size + 3;
  return ((line[++i]) ? -1 : 0);
}
