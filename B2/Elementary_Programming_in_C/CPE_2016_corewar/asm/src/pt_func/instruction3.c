/*
** instruction3.c for No Woman no Cry in /home/benjamin/
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Thu Mar  9 21:38:27 2017 Benjamin
** Last update Thu Mar 30 16:20:43 2017 Roulleau Julien
*/

#include "pt_func.h"
#include "lib.h"
#include "is_func.h"

int		sti_function(char **line, t_data *data)
{
  int		nb;
  int		i;

  i = 0;
  line[i] = line[i] + 4;
  data->size = data->size + 2;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  if ((nb = which_bytes(line[i], mk_bool(1, 0, 0))) == -1)
    return (-1);
  data->size = data->size + 1;
  if ((nb = which_bytes(line[++i], mk_bool(1, 1, 1))) == -1)
    return (-1);
  if (nb == 4 || nb == 2)
    data->size = data->size + 2;
  else if (nb == 1)
    data->size = data->size + 1;
  if ((nb = which_bytes(line[++i], mk_bool(1, 0, 1))) == -1)
    return (-1);
  if (nb == 4)
    data->size = data->size + 2;
  else if (nb == 1)
    data->size = data->size + 1;
  return ((line[++i]) ? -1 : 0);
}

int		fork_function(char **line, t_data *data)
{
  int		i;

  i = 0;
  line[i] = line[i] + 5;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  if (which_bytes(line[i], mk_bool(0, 0, 1)) == -1)
    return (-1);
  data->size = data->size + 3;
  return ((line[++i]) ? -1 : 0);
}

int		lld_function(char **line, t_data *data)
{
  int		nb;
  int		i;

  i = 0;
  line[0] = line[0] + 4;
  data->size = data->size + 2;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  if ((nb = which_bytes(line[i], mk_bool(0, 1, 1))) == -1)
    return (-1);
  data->size = data->size + nb;
  if ((nb = which_bytes(line[++i], mk_bool(1, 0, 0))) == -1)
    return (-1);
  data->size = data->size + nb;
  return ((line[++i]) ? -1 : 0);
}

int		lldi_function(char **line, t_data *data)
{
  int		nb;
  int		i;

  i = 0;
  line[0] = line[0] + 5;
  data->size = data->size + 2;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  if ((nb = which_bytes(line[i], mk_bool(1, 1, 1))) == -1)
    return (-1);
  if (nb == 4 || nb == 2)
    data->size = data->size + 2;
  else if (nb == 1)
    data->size = data->size + 1;
  if ((nb = which_bytes(line[++i], mk_bool(1, 0, 1))) == -1)
    return (-1);
  if (nb == 4)
    data->size = data->size + 2;
  else if (nb == 1)
    data->size = data->size + 1;
  if ((nb = which_bytes(line[++i], mk_bool(1, 0, 0))) == -1)
    return (-1);
  data->size = data->size + 1;
  return ((line[++i]) ? -1 : 0);
}

int		lfork_function(char **line, t_data *data)
{
  int		i;

  i = 0;
  line[i] = line[i] + 6;
  #include <stdio.h>
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  if ((which_bytes(line[i], mk_bool(0, 0, 1))) == -1)
    return (-1);
  data->size = data->size + 3;
  return ((line[++i]) ? -1 : 0);
}
