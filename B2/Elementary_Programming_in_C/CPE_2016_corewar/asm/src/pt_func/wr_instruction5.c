/*
** wr_instruction5.c for wr_instruction5.c in /home/benjamin/
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Sun Mar 19 20:44:36 2017 Benjamin
** Last update Wed Mar 22 21:36:12 2017 Roulleau Julien
*/

#include <unistd.h>
#include "pt_func.h"
#include "lib.h"
#include "is_func.h"

int		ldi_function_wr_next(char *str, t_list *list, t_data *data,
				     t_write *w)
{
  int		nb;

  nb = which_value(str, list, data, data->tb);
  if ((check_coding_byte(w->coding, data->place, data)) == 4)
    {
      w->k = reverse_two_bytes((unsigned short int)nb).n;
      write(data->new_fd, &(w->k), sizeof(unsigned short int));
    }
  else if ((check_coding_byte(w->coding, data->place, data)) == 2)
    {
      w->k = reverse_two_bytes((unsigned short int)nb).n;
      write(data->new_fd, &(w->k), sizeof(unsigned short int));
    }
  else if ((check_coding_byte(w->coding, data->place, data)) == 1)
    {
      w->c = nb;
      write(data->new_fd, &(w->c), sizeof(unsigned char));
    }
  return (0);
}

int		lld_function_wr_next(char *str, t_list *list, t_data *data,
				     t_write *w)
{
  int		nb;

  nb = which_value(str, list, data, data->tb);
  if ((check_coding_byte(w->coding, data->place, data)) == 4)
    {
      w->n = reverse_bytes(nb).n;
      write(data->new_fd, &(w->n), sizeof(unsigned int));
    }
  else if ((check_coding_byte(w->coding, data->place, data)) == 2)
    {
      w->k = reverse_two_bytes((short int)nb).n;
      write(data->new_fd, &(w->k), sizeof(unsigned short int));
    }
  else if ((check_coding_byte(w->coding, data->place, data)) == 1)
    {
      w->c = nb;
      write(data->new_fd, &(w->c), sizeof(unsigned char));
    }
  return (0);
}
