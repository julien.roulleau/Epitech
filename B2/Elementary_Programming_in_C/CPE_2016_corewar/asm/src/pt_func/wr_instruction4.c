/*
** wr_instruction4.c for alone in /home/benjamin/
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Thu Mar 16 00:40:45 2017 Benjamin
** Last update Thu Mar 30 16:21:06 2017 Roulleau Julien
*/
#include <unistd.h>
#include "pt_func.h"
#include "lib.h"
#include "is_func.h"

int		aff_function_wr(char **line, t_list *list, t_data *data)
{
  t_write	*w;
  int		i;
  int		nb;

  i = 0;
  if ((w = init_w(16)) == NULL)
    return (-1);
  line[i] = line[i] + 4;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  fill_coding_byte(&(w->coding), line[i], 7, mk_bool(1, 0, 0));
  nb = which_value(line[i], list, data, mk_bool(1, 0, 0));
  write(data->new_fd, &(w->c), sizeof(unsigned char));
  write(data->new_fd, &(w->coding), sizeof(unsigned char));
  w->c = nb;
  write(data->new_fd, &(w->c), sizeof(unsigned char));
  data->size = data->size + 3;
  return (0);
}

int		ld_function_wr_next(char *str, t_list *list, t_data *data,
				    t_write *w)
{
  int		nb;

  nb = which_value(str, list, data, mk_bool(0, 1, 1));
  if ((check_coding_byte(w->coding, 7, data)) == 4)
    {
      w->n = reverse_bytes(nb).n;
      write(data->new_fd, &(w->n), sizeof(unsigned int));
    }
  else if ((check_coding_byte(w->coding, 7, data)) == 2)
    {
      w->k = reverse_two_bytes((unsigned short int)nb).n;
      write(data->new_fd, &(w->k), sizeof(unsigned short int));
    }
  return (nb);
}

int		st_function_wr_next(char *str, t_list *list, t_data *data,
				    t_write *w)
{
  int		nb;

  nb = which_value(str, list, data, mk_bool(1, 1, 0));
  if ((check_coding_byte(w->coding, 5, data)) == 1)
    {
      w->c = nb;
      write(data->new_fd, &(w->c), sizeof(unsigned char));
    }
  else if ((check_coding_byte(w->coding, 5, data)) == 2)
    {
      w->k = reverse_two_bytes((unsigned short int)nb).n;
      write(data->new_fd, &(w->k), sizeof(unsigned short int));
    }
  return (nb);
}

int		and_or_xor_function_wr_next(char *str, t_list *list,
				    t_data *data, t_write *w)
{
  int		nb;

  nb = which_value(str, list, data, data->tb);
  if ((check_coding_byte(w->coding, data->place, data)) == 4)
    {
      w->n = reverse_bytes(nb).n;
      write(data->new_fd, &(w->n), sizeof(unsigned int));
    }
  else if ((check_coding_byte(w->coding, data->place, data)) == 2)
    {
      w->k = reverse_two_bytes((unsigned short int)nb).n;
      write(data->new_fd, &(w->k), sizeof(unsigned short int));
    }
  else if ((check_coding_byte(w->coding, data->place, data)) == 1)
    {
      w->c = nb;
      write(data->new_fd, &(w->c), sizeof(unsigned char));
    }
  return (nb);
}

int		add_sub_function_wr_next(char *str, t_list *list,
				    t_data *data, t_write *w)
{
  int		nb;

  nb = which_value(str, list, data, data->tb);
  if ((check_coding_byte(w->coding, data->place, data)) == 1)
    {
      w->c = nb;
      write(data->new_fd, &(w->c), sizeof(unsigned char));
    }
  return (0);
}
