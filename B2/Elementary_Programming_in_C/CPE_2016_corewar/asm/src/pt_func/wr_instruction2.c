/*
** wr_instruction2.c for Insane in the brain in /home
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Thu Mar 16 00:36:49 2017 Benjamin
** Last update Thu Mar 30 16:20:56 2017 Roulleau Julien
*/

#include <unistd.h>
#include "pt_func.h"
#include "lib.h"
#include "is_func.h"

int		and_function_wr(char **line, t_list *list, t_data *data)
{
  t_write	*w;
  int		i;

  i = 0;
  if ((w = init_w(6)) == NULL)
    return (-1);
  line[i] = line[i] + 4;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  fill_coding_byte(&(w->coding), line[i], 7, mk_bool(1, 1, 1));
  fill_coding_byte(&(w->coding), line[++i], 5, mk_bool(1, 1, 1));
  fill_coding_byte(&(w->coding), line[++i], 3, mk_bool(1, 0, 0));
  write(data->new_fd, &(w->c), sizeof(unsigned char));
  write(data->new_fd, &(w->coding), sizeof(unsigned char));
  i = i - 2;
  data->place = 7;
  data->tb = mk_bool(1, 1, 1);
  and_or_xor_function_wr_next(line[i], list, data, w);
  data->place = 5;
  and_or_xor_function_wr_next(line[++i], list, data, w);
  data->place = 3;
  data->tb = mk_bool(1, 0, 0);
  and_or_xor_function_wr_next(line[++i], list, data, w);
  increase_data_size(w->coding, data, 0);
  return (0);
}

int		or_function_wr(char **line, t_list *list, t_data *data)
{
  t_write	*w;
  int		i;

  i = 0;
  if ((w = init_w(7)) == NULL)
    return (-1);
  line[i] = line[i] + 3;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  fill_coding_byte(&(w->coding), line[i], 7, mk_bool(1, 1, 1));
  fill_coding_byte(&(w->coding), line[++i], 5, mk_bool(1, 1, 1));
  fill_coding_byte(&(w->coding), line[++i], 3, mk_bool(1, 0, 0));
  write(data->new_fd, &(w->c), sizeof(unsigned char));
  write(data->new_fd, &(w->coding), sizeof(unsigned char));
  i = i - 2;
  data->place = 7;
  data->tb = mk_bool(1, 1, 1);
  and_or_xor_function_wr_next(line[i], list, data, w);
  data->place = 5;
  and_or_xor_function_wr_next(line[++i], list, data, w);
  data->place = 3;
  data->tb = mk_bool(1, 0, 0);
  and_or_xor_function_wr_next(line[++i], list, data, w);
  increase_data_size(w->coding, data, 0);
  return (0);
}

int		xor_function_wr(char **line, t_list *list, t_data *data)
{
  t_write	*w;
  int		i;

  i = 0;
  if ((w = init_w(8)) == NULL)
    return (-1);
  line[i] = line[i] + 4;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  fill_coding_byte(&(w->coding), line[i], 7, mk_bool(1, 1, 1));
  fill_coding_byte(&(w->coding), line[++i], 5, mk_bool(1, 1, 1));
  fill_coding_byte(&(w->coding), line[++i], 3, mk_bool(1, 0, 0));
  write(data->new_fd, &(w->c), sizeof(unsigned char));
  write(data->new_fd, &(w->coding), sizeof(unsigned char));
  i = i - 2;
  data->place = 7;
  data->tb = mk_bool(1, 1, 1);
  and_or_xor_function_wr_next(line[i], list, data, w);
  data->place = 5;
  and_or_xor_function_wr_next(line[++i], list, data, w);
  data->place = 3;
  data->tb = mk_bool(1, 0, 0);
  and_or_xor_function_wr_next(line[++i], list, data, w);
  increase_data_size(w->coding, data, 0);
  return (0);
}

int		zjump_function_wr(char **line, t_list *list, t_data *data)
{
  t_write	*w;
  int		i;
  int		nb;

  i = 0;
  if ((w = init_w(9)) == NULL)
    return (-1);
  line[i] = line[i] + 5;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  nb = which_value(line[i], list, data, mk_bool(0, 0, 1));
  write(data->new_fd, &(w->c), sizeof(unsigned char));
  w->k = reverse_two_bytes((unsigned short int)nb).n;
  write(data->new_fd, &(w->k), sizeof(unsigned short int));
  data->size = data->size + 3;
  return (0);
}

int		ldi_function_wr(char **line, t_list *list, t_data *data)
{
  t_write	*w;
  int		i;

  i = 0;
  w = init_w(10);
  line[i] = line[i] + 4;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  fill_coding_byte(&(w->coding), line[i], 7, mk_bool(1, 1, 1));
  fill_coding_byte(&(w->coding), line[++i], 5, mk_bool(1, 0, 1));
  fill_coding_byte(&(w->coding), line[++i], 3, mk_bool(1, 0, 0));
  write(data->new_fd, &(w->c), sizeof(unsigned char));
  write(data->new_fd, &(w->coding), sizeof(unsigned char));
  i = i - 2;
  data->place = 7;
  data->tb = mk_bool(1, 1, 1);
  ldi_function_wr_next(line[i], list, data, w);
  data->place = 5;
  data->tb = mk_bool(1, 0, 1);
  ldi_function_wr_next(line[++i], list, data, w);
  data->place = 3;
  data->tb = mk_bool(1, 0, 0);
  ldi_function_wr_next(line[++i], list, data, w);
  increase_data_size(w->coding, data, 1);
  return (0);
}
