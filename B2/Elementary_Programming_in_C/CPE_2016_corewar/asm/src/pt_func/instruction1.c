/*
** instruction1.c for instuction1.c in /home/benjamin/
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Thu Mar  9 20:21:58 2017 Benjamin
** Last update Thu Mar 30 16:20:40 2017 Roulleau Julien
*/

#include "pt_func.h"
#include "lib.h"
#include "is_func.h"

int		live_function(char **line, t_data *data)
{
  int		i;

  i = 0;
  line[i] = line[i] + 5;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  if (which_bytes(line[i], mk_bool(0, 0, 1)) == -1)
    return (-1);
  data->size = data->size + 5;
  return ((line[++i]) ? -1 : 0);
}

int		ld_function(char **line, t_data *data)
{
  int		nb;
  int		i;

  i = 0;
  data->size = data->size + 2;
  line[i] = line[i] + 3;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  if ((nb = which_bytes(line[i], mk_bool(0, 1, 1))) == -1)
    return (-1);
  else
    data->size = data->size + nb;
  if ((nb = which_bytes(line[++i], mk_bool(1, 0, 0))) == -1)
    return (-1);
  else
    data->size = data->size + nb;
  return (0);
  return ((line[++i]) ? -1 : 0);
}

int		st_function(char **line, t_data *data)
{
  int		nb;
  int		i;

  i = 0;
  line[i] = line[i] + 3;
  data->size = data->size + 2;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  if ((nb = which_bytes(line[i], mk_bool(1, 0, 0))) == -1)
    return (-1);
  if (nb == 1)
    data->size = data->size + 1;
  if ((nb = which_bytes(line[++i], mk_bool(1, 1, 0))) == -1)
    return (-1);
  if (nb == 1)
    data->size = data->size + 1;
  else if (nb == 2)
     data->size = data->size + 2;
  return ((line[++i]) ? -1 : 0);
}

int		add_function(char **line, t_data *data)
{
  int		nb;
  int		i;

  i = 0;
  line[0] = line[0] + 4;
  data->size = data->size + 2;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  if ((nb = which_bytes(line[i], mk_bool(1, 0, 0))) == -1)
    return (-1);
  if (nb == 1)
    data->size = data->size + 1;
  if ((nb = which_bytes(line[++i], mk_bool(1, 0, 0))) == -1)
    return (-1);
  if (nb == 1)
    data->size = data->size + 1;
  if ((nb = which_bytes(line[++i], mk_bool(1, 0, 0))) == -1)
    return (-1);
  if (nb == 1)
    data->size = data->size + 1;
  return ((line[++i]) ? -1 : 0);
}

int		sub_function(char **line, t_data *data)
{
  int		nb;
  int		i;

  i = 0;
  line[0] = line[0] + 4;
  data->size = data->size + 2;
  if ((is_legal_char(line[i][0], " \t\0")) == 1)
    ++i;
  if ((nb = which_bytes(line[i], mk_bool(1, 0, 0))) == -1)
    return (-1);
  if (nb == 1)
    data->size = data->size + 1;
  if ((nb = which_bytes(line[++i], mk_bool(1, 0, 0))) == -1)
    return (-1);
  if (nb == 1)
    data->size = data->size + 1;
  if ((nb = which_bytes(line[++i], mk_bool(1, 0, 0))) == -1)
    return (-1);
  if (nb == 1)
    data->size = data->size + 1;
  return ((line[++i]) ? -1 : 0);
}
