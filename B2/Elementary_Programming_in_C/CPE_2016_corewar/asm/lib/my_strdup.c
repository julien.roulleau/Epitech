/*
** my_strdup.c for my_strdup.c in /home/B_chikho/
**
** Made by Chikhoune Benjamin
** Login   <B_chikho@epitech.net>
**
** Started on  Wed Oct 12 09:52:04 2016 Chikhoune Benjamin
** Last update Thu Mar 30 16:18:44 2017 Roulleau Julien
*/

#include <stdlib.h>

static int		my_strlen(char *str)
{
  int		i;

  i = -1;
  while (str[++i]);
  return (i);
}

char		*my_strdup(char *str)
{
  char		*new;
  int		i;

  i = -1;
  if ((new = malloc(sizeof(char) * (my_strlen(str)) + 1)) == NULL)
    return (NULL);
  while (str[++i])
    new[i] = str[i];
  new[i] = '\0';
  return (new);
}

char		*correct_param(char *str)
{
  char		*new;
  int		i;

  i = my_strlen(str);
  new = 0;
  while (str[--i] && str[i] != '/');
  if (str[i] == '/')
    {
      if ((new = my_strdup(str + (i + 1))) == NULL)
	return (NULL);
    }
  else if (str[i] == '\0')
    if ((new = my_strdup(str)) == NULL)
      return (NULL);
  return (new);
}
