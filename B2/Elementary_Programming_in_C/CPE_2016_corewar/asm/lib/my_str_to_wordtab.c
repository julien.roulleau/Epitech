/*
** my_str_to_wordtab.c for my_str_to_wordtab.c in /home/B_chikho/
**
** Made by Chikhoune Benjamin
** Login   <B_chikho@epitech.net>
**
** Started on  Wed Oct 12 12:57:43 2016 Chikhoune Benjamin
** Last update Thu Mar 30 16:19:00 2017 Roulleau Julien
*/

#include <stdlib.h>

static char	*my_strncpy(char *dest, char *src, int n)
{
  int           i;

  i = 0;
  if (src == NULL)
    return (NULL);
  while (src[i] && i < n)
    {
      dest[i] = src[i];
      i = i + 1;
    }
  dest[i] = '\0';
  return (dest);
}

static char	*alloc_this_string(char *str, char c)
{
  char		*new;
  int		i;
  int		j;

  i = j = 0;
  new = 0;
  while (str[i + j] && str[i + j] == c)
    j = j + 1;
  while (str[i + j] && str[i + j] != c)
    ++i;
  if (i > 0)
    {
      if ((new = malloc(sizeof(char) * (i + 1))) == NULL)
	return (NULL);
      new = my_strncpy(new, str + j, i);
    }
  return (new);
}

static char	**alloc_this_tab(char *str, char c)
{
  int		i;
  int		j;
  int		sep;
  char		**tab;

  i = -1;
  j = -1;
  sep = 1;
  while (str[++i])
    {
      if (str[i] == c)
	  sep = sep + 1;
    }
  if ((tab = malloc(sizeof(char *) * (sep + 1))) == NULL)
    return (NULL);
  i = -1;
  while (str[++i])
    {
      if (((str[i] != c) && (i == 0)) ||
	  (str[i] == c))
	tab[++j] = alloc_this_string(str + i, c);
    }
  tab[++j] = NULL;
  return (tab);
}

char		**my_str_to_wordtab(char *str, char c)
{
  char		**tab;

  if (str == NULL)
    return (NULL);
  tab = alloc_this_tab(str, c);
  return (tab);
}
