/*
** my_memset.c for my_memset.c in /home/benjamin/chikho_b/Corewar/src
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Wed Mar 15 19:56:20 2017 Benjamin
** Last update Thu Mar 23 17:03:59 2017 Benjamin
*/

void		my_memset(char *str, int len, char set)
{
  int		i;

  i = -1;
  while (++i < len)
    str[i] = set;
}

void		*my_super_memset(void *s, int len, char set)
{
  int		i;
  char		*str;

  i = -1;
  str = s;
  while (++i < len)
    str[i] = set;
  return (s);
}
