/*
** basic_function.c for basic_function.c in /home/benjamin/chikho_b/
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Wed Mar  1 17:18:13 2017 Benjamin
** Last update Wed Mar 22 21:35:08 2017 Roulleau Julien
*/

#include <unistd.h>

int		my_suuuper_strlen(char *str, char c)
{
  int		i;

  i = -1;
  while (str[++i] && str[i] != c);
  return (i);
}

void		my_suuper_putstr(char *str, int fd)
{
  write(fd, str, my_suuuper_strlen(str, '\0'));
}

void		my_suuuper_putchar(char c, int fd)
{
  write(fd, &c, 1);
}

int		my_strlen(char *str)
{
  int		i;

  i = -1;
  if (str == NULL)
    return (-1);
  while (str[++i]);
  return (i);
}

void		super_print_tab(char **tab, int fd)
{
  int		i;

  i = -1;
  while (tab[++i])
    {
      my_suuper_putstr(tab[i], fd);
      write(fd, "\n", 1);
    }
}
