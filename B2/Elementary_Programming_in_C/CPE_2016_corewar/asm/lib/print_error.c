/*
** print_error.c for Moonlight Sonata in /home/
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Fri Mar 10 00:23:56 2017 Benjamin
** Last update Thu Mar 30 16:18:37 2017 Roulleau Julien
*/

#include <unistd.h>
#include <stdio.h>
#include "lib.h"
#include "struct.h"

int		print_error_int(char *msg, int fd, t_data *data, int ret)
{
  if (data && data->prog_name && data->prog_param)
    {
      my_suuper_putstr(data->prog_name, fd);
      write(fd, ", ", 2);
      my_suuper_putstr(data->prog_param, fd);
      write(fd, ", line ", 7);
      my_put_nbr(data->line);
      write(fd, ": ", 2);
      my_suuper_putstr(msg, fd);
      write(fd, "\n", 1);
    }
  else
    my_suuper_putstr("Erreur\n", fd);
  return (ret);
}
