/*
** my_epur_str.c for my_epur_str.c in /home/B_chikho/test_rendu
**
** Made by Chikhoune Benjamin
** Login   <B_chikho@epitech.net>
**
** Started on  Wed Oct 12 16:17:48 2016 Chikhoune Benjamin
** Last update Wed Mar 22 21:28:37 2017 Roulleau Julien
*/

#include <stdio.h>

static void	move_left(char *str)
{
  int		i;

  i = 0;
  while (str[i])
    {
      str[i] = str[i + 1];
      if (str[i] != '\0')
	i = i + 1;
    }
}

int		is_sep(char c, char *sep)
{
  int           i;

  i = -1;
  while (sep[++i])
    {
      if (c == sep[i])
	return (1);
    }
  return (0);
}

char		*spec_epur_str(char *str, char *sep)
{
  int		i;

  i = 0;
  if (str == NULL)
    return (NULL);
  while ((is_sep(str[0], sep) == 1) && str[0])
    move_left(str);
  while (str[i] != '\0')
    {
      while (((is_sep(str[i], sep) == 1) && str[i]) &&
	     ((is_sep(str[i + 1], sep) == 1) && str[i + 1]))
	move_left(str + i);
      if (str[i] == '#')
	str[i] = '\0';
      else if (str[i] != '\0')
	i = i + 1;
    }
  if (str[i] != '\0')
    if (is_sep(str[i - 1], sep) == 1)
      str[i - 1] = '\0';
  if (i > 0 && str[i] == '\0' && (is_sep(str[i - 1], sep) == 1))
    str[i - 1] = '\0';
  return (str);
}
