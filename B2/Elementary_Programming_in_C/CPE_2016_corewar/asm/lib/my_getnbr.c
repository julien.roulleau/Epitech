/*
** my_getnbr.c for my_getnbr.c in /home/B_chikho/rendu/CPool_Day04
**
** Made by Chikhoune Benjamin
** Login   <B_chikho@epitech.net>
**
** Started on  Thu Oct  6 13:21:50 2016 Chikhoune Benjamin
** Last update Tue Mar 21 19:44:10 2017 Benjamin
*/

#include <stdlib.h>

static int		is_num(char c)
{
  if ((c > 47) && (c < 58))
    return (0);
  return (1);
}

static int		how_many_sign(char *str)
{
  int		i;
  int		nb;

  i = 0;
  nb = 0;
  while ((str[i] == '-') || (str[i] == '+'))
    {
      if (str[i] == '-')
	nb = nb + 1;
      i = i + 1;
    }
  return (nb);
}

static int		skip_this(char *str)
{
  int		i;

  i = -1;
  while (str[++i] && (is_num(str[i])) != 0);
  return (i);
}

int		my_getnbr(char *str)
{
  int		nb;
  int		i;
  int		sign;
  int		skip;

  if (str == NULL)
    return (-1);
  sign = how_many_sign(str);
  i = -1;
  skip = skip_this(str);
  nb = 0;
  while (str[++i + skip] && ((is_num(str[i + skip])) == 0))
    {
      nb = nb * 10;
      nb = nb + ((str[i + skip] - 48));
    }
  if (((sign % 2) != 0) && (sign != 0))
    nb = nb * (-1);
  return (nb);
}
