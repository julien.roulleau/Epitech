/*
** liked_list.c for linked_list.c in /home/benjamin/chikho_b/minishell/src
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Tue Jan  3 16:14:18 2017 Benjamin
** Last update Wed Mar 22 21:27:54 2017 Roulleau Julien
*/

#include <unistd.h>
#include <stdlib.h>
#include "lib.h"

void		print_list(t_elem *elem)
{
  t_elem	*tmp;

  tmp = elem;
  while (tmp)
    {
      my_suuper_putstr("label = [", 0);
      my_suuper_putstr(tmp->name, 0);
      my_suuper_putstr("] data = [", 0);
      my_put_nbr(tmp->size);
      write(1, "]\n", 2);
      tmp = tmp->next;
    }
}

t_list		*init_list()
{
  t_list	*new;
  if ((new = malloc(sizeof(t_list))) == NULL)
    return (NULL);
  new->head = NULL;
  new->tail = NULL;
  new->size = 0;
  return (new);
}

t_elem		*init_elem(char *str, int current)
{
  t_elem	*elem;

  if ((elem = malloc(sizeof(t_elem))) == NULL)
    return (NULL);
  elem->name = str;
  elem->size = current;
  elem->next = NULL;
  elem->prev = NULL;
  return (elem);
}

int		add_in_list(t_list *list, char *str, int current)
{
  t_elem	*elem;

  if (list == NULL)
    return (-1);
  if ((elem = init_elem(str, current)) == NULL)
    return (-1);
  if (list->size == 0)
    {
      list->tail = elem;
      list->head = elem;
    }
  else
    {
      elem->prev = list->tail;
      list->tail->next = elem;
      list->tail = elem;
    }
  list->size += 1;
  return (0);
}
