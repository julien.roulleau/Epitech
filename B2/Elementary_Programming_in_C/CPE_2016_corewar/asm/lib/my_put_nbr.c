/*
** my_put_nbr.c for my_putnbr.c in /home/B_chikho/rendu/CPool_Day03
**
** Made by Chikhoune Benjamin
** Login   <B_chikho@epitech.net>
**
** Started on  Wed Oct  5 09:41:58 2016 Chikhoune Benjamin
** Last update Wed Mar 22 21:32:51 2017 Roulleau Julien
*/

#include <unistd.h>

void		my_putchar(char c)
{
  write(1, &c, 1);
}

static void	dirty_function()
{
  my_putchar('-');
  my_putchar('2');
  my_putchar('1');
  my_putchar('4');
  my_putchar('7');
  my_putchar('4');
  my_putchar('8');
  my_putchar('3');
  my_putchar('6');
  my_putchar('4');
  my_putchar('8');
}
int		my_put_nbr(int nb)
{
  if (nb == (-2147483648))
    {
      dirty_function();
      return (0);
    }
  if (nb < 0)
    {
      nb = nb * -1;
      my_putchar('-');
    }
  if (nb < 10)
    my_putchar(nb + 48);
  else if (nb >= 10)
    {
      my_put_nbr(nb / 10);
      my_putchar((nb % 10) + 48);
    }
  return (0);
}
