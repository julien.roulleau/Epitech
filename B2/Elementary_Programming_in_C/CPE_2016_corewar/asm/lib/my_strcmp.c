/*
** my_strcmp.c for my_strcmp.c in /home/
**
** Made by Chikhoune Benjamin
** Login   <B_chikho@epitech.net>
**
** Started on  Mon Oct 10 12:27:06 2016 Chikhoune Benjamin
** Last update Thu Mar 30 16:18:53 2017 Roulleau Julien
*/

#include <unistd.h>

int		my_strcmp(char *s1, char *s2)
{
  int		i;

  i = 0;
  if ((s1 == NULL) || (s2 == NULL))
    return (-1);
  while (s1[i] == s2[i] && s1[i] != '\0')
    i = i + 1;
  return ((s1[i] - s2[i]));
}
