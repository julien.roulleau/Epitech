/*
** my_strcmp.c for my_strcmp.c in /home/B_chikho
**
** Made by Chikhoune Benjamin
** Login   <B_chikho@epitech.net>
**
** Started on  Mon Oct 10 12:27:06 2016 Chikhoune Benjamin
** Last update Thu Mar 30 16:18:41 2017 Roulleau Julien
*/

#include <unistd.h>
#include "is_func.h"

int		my_strncmp(char *s1, char *s2, int n)
{
  int		i;

  i = 0;
  if (n < 0)
    return (2000000000);
  if ((s1 == NULL) || (s2 == NULL))
    return (-1);
  while (((s1[i] == s2[i]) && (s1[i] != '\0')) && i < (n - 1))
    i = i + 1;
  if ((i == (n - 1) && (is_legal_char(s2[i + 1], " \t,\0")) == 1))
    return ((s1[i] - s2[i]));
  return (-1);
}
