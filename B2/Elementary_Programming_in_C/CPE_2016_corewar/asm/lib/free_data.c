/*
** free_data.c for Benkei et Minamoto in /home/benjamin/
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Tue Mar 21 23:17:56 2017 Benjamin
** Last update Thu Mar 30 16:19:18 2017 Roulleau Julien
*/

#include <stdlib.h>
#include "struct.h"

int		tablen(char **tab)
{
  int		i;

  i = -1;
  while (tab[++i]);
  return (i);
}

void		free_list(t_list *list)
{
  t_elem	*tmp;

  while (list->head)
    {
      tmp = list->head;
      list->head = list->head->next;
      free(tmp->name);
      free(tmp);
    }
  free(list);
}

void		free_tab(char **tab, int size)
{
  int		i;

  i = 0;
  if (tab)
    {
      while (++i < size)
	free(tab[i]);
    }
}

void		free_list_and_data(t_list *list, t_data *data)
{
  if (data->tb)
    free(data->tb);
  free_list(list);
}
