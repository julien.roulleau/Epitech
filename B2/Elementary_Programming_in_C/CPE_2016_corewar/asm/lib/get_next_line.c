/*
** get_next_line.c for get_next_line.c in /home/chikho_b/rendu/
**
** Made by Chikhoune Benjamin
** Login   <chikho_b@epitech.net>
**
** Started on  Tue Jan 12 14:50:30 2016 Chikhoune Benjamin
** Last update Wed Mar 22 21:35:04 2017 Roulleau Julien
*/

#include <stdio.h>
#include "get_next_line.h"

char	*my_realloc(char *str, int oldsize, int added_size)
{
  char	*new;
  int	i;

  i = 0;
  new = malloc(sizeof(char) * (oldsize + added_size));
  if (new == NULL)
    {
      free(str);
      return (NULL);
    }
  while (i < oldsize)
    {
      new[i] = str[i];
      i = i + 1;
    }
  new[i] = '\0';
  free(str);
  return (new);
}

char	get_next_char(const int fd)
{
  static char	str[READ_SIZE + 1];
  static int	i = 0;
  static int	nread = 0;
  char	c;

  if (fd == -1)
    {
      i = 0;
      nread = 0;
      return (0);
    }
  if (i == nread)
    {
      if ((nread = read(fd, str, READ_SIZE)) <= 0)
	return (0);
      i = 0;
    }
  c = str[i];
  i = i + 1;
  return (c);
}

char	*get_next_line(const int fd)
{
  char	*str;
  int	i;
  char	c;

  i = 0;
  c = '&';
  str = malloc(sizeof(char) * READ_SIZE);
  if (str == NULL)
    return (NULL);
  while (c != '\n')
    {
      if ((c = get_next_char(fd)) == 0)
	{
	  free(str);
	  return (NULL);
	}
      str[i] = c;
      if ((i + 1) % READ_SIZE == 0)
	str = my_realloc(str, i + 1, READ_SIZE);
      if (str[i] == '\n')
	str[i] = '\0';
      i = i + 1;
    }
  return (str);
}
