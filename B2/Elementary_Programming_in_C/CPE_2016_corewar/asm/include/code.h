/*
** code.h for corewar in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Mar 22 20:57:15 2017 Roulleau Julien
** Last update Thu Mar 30 16:16:18 2017 Roulleau Julien
*/

#ifndef CORE_H
# define CORE_H

#include "struct.h"
#include "op.h"

int		init_label_and_count(int, t_list *, t_data *);
int		write_file(header_t, t_list *, char *, t_data *);
u_bytes		reverse_bytes(int);
s_bytes		reverse_two_bytes(short int);
int		replace_label(char *, t_elem *, t_data*);
int             fill_coding_byte(char *, char *, int, t_bool *);
int		increase_data_size(char, t_data *, char);

#endif /* CORE_H */
