/*
** struct.h for corewar in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Mar 22 17:32:41 2017 Roulleau Julien
** Last update Thu Mar 30 16:18:07 2017 Roulleau Julien
*/

#ifndef STRUCT_H
# define STRUCT_H

typedef union	bytes
{
  int		n;
  char		b[4];
}		u_bytes;

typedef union	t_bytes
{
  short int	n;
  char		b[2];
}		s_bytes;

typedef struct	s_write
{
  unsigned char		c;
  char			coding;
  unsigned int		n;
  unsigned short int	k;
}		t_write;

typedef struct	s_elem
{
  unsigned int	size;
  char		*name;
  struct s_elem	*next;
  struct s_elem *prev;
}		t_elem;

typedef struct	s_bool
{
  int		r;
  int		i;
  int		d;
}		t_bool;

typedef struct	s_list
{
  t_elem	*head;
  t_elem	*tail;
  int		size;
}		t_list;

typedef struct	s_data
{
  unsigned int	size;
  int		line;
  int		place;
  t_bool	*tb;
  int		new_fd;
  char		*prog_name;
  char		*prog_param;
}		t_data;

typedef struct	s_pack
{
  char		*str;
  char		c;
  t_list	*bytes;
}		t_pack;

typedef struct s_ptr
{
  char		*flag;
  int		(*ptr)(char **, t_data *);
}		t_ptr;

typedef struct s_ptrw
{
  char		*flag;
  int		(*ptr)(char **, t_list *, t_data *);
}		t_ptrw;

#endif /* STRUCT_H */
