/*
** pt_func.h for corewar in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Mar 22 17:35:33 2017 Roulleau Julien
** Last update Thu Mar 30 16:18:03 2017 Roulleau Julien
*/

#ifndef PT_FUNC_H
# define PT_FUNC_H

#include "struct.h"

int		live_function(char **, t_data *);
int		ld_function(char **, t_data *);
int		st_function(char **, t_data *);
int		add_function(char **, t_data *);
int		sub_function(char **, t_data *);
int		and_function(char **, t_data *);
int		or_function(char **, t_data *);
int		xor_function(char **, t_data *);
int		zjump_function(char **, t_data *);
int		ldi_function(char **, t_data *);
int		sti_function(char **, t_data *);
int		fork_function(char **, t_data *);
int		lld_function(char **, t_data *);
int		lldi_function(char **, t_data *);
int		lfork_function(char **, t_data *);
int		aff_function(char **, t_data *);
int		live_function_wr(char **, t_list *list, t_data *);
int		ld_function_wr(char **, t_list *list, t_data *);
int		ld_function_wr_next(char *, t_list *, t_data *, t_write *);
int		st_function_wr_next(char *, t_list *, t_data *, t_write *);
int		and_or_xor_function_wr_next(char *, t_list *list,
				  t_data *, t_write *);
int		ldi_function_wr_next(char *, t_list *list,
				  t_data *, t_write *);
int		lld_function_wr_next(char *, t_list *list,
				  t_data *, t_write *);
int		add_sub_function_wr_next(char *, t_list *list,
				  t_data *, t_write *);
int		st_function_wr(char **, t_list *list, t_data *);
int		add_function_wr(char **, t_list *list, t_data *);
int		sub_function_wr(char **, t_list *list, t_data *);
int		and_function_wr(char **, t_list *list, t_data *);
int		or_function_wr(char **, t_list *list, t_data *);
int		xor_function_wr(char **, t_list *list, t_data *);
int		zjump_function_wr(char **, t_list *list, t_data *);
int		ldi_function_wr(char **, t_list *list, t_data *);
int		sti_function_wr(char **, t_list *list, t_data *);
int		fork_function_wr(char **, t_list *list, t_data *);
int		lld_function_wr(char **, t_list *list, t_data *);
int		lldi_function_wr(char **, t_list *list, t_data *);
int		lfork_function_wr(char **, t_list *list, t_data *);
int		aff_function_wr(char **, t_list *list, t_data *);

#endif /* PT_FUNC_H */
