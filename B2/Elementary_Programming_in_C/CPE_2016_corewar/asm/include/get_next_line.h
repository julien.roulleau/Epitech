/*
** get_next_line.h for get_next_line.h in /home/chikho_b/
**
** Made by Chikhoune Benjamin
** Login   <chikho_b@epitech.net>
**
** Started on  Mon Jan 11 20:14:31 2016 Chikhoune Benjamin
** Last update Thu Mar 30 16:16:28 2017 Roulleau Julien
*/

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

# ifndef READ_SIZE
# define READ_SIZE (14)

char	*my_realloc(char *, int, int);
char	get_next_char(const int);
char	*get_next_line(const int);

# endif /* !READ_SIZE */
