/*
** is_func.h for corewar in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Mar 22 17:49:01 2017 Roulleau Julien
** Last update Thu Mar 30 16:16:34 2017 Roulleau Julien
*/

#ifndef IS_FUNC_H
# define IS_FUNC_H

#include "struct.h"

int		is_label(char *, t_data *);
int		is_legal_char(char, char *cmp);
int		which_bytes(char *, t_bool *);
unsigned int	which_value(char *, t_list *list, t_data *, t_bool *);
int             fill_coding_byte(char *, char *, int, t_bool *);
int		check_coding_byte(char, int, t_data *);
int		increase_data_size(char, t_data *, char);
u_bytes		reverse_bytes(int);
s_bytes		reverse_two_bytes(short int);
char		**init_tab(int *, t_data *, char **);
int		increase_str(char **, t_elem *, t_data*);
t_list		*init_list();
int		add_in_list(t_list *, char *, int);

#endif /* IS_FUNC_H */
