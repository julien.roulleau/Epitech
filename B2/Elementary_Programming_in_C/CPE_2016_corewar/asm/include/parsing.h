/*
** parsing.h for corewar in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Mar 22 20:44:28 2017 Roulleau Julien
** Last update Thu Mar 30 16:17:48 2017 Roulleau Julien
*/

#ifndef PARSING_H
# define PARSING_H

#include "struct.h"
#include "op.h"

int		receipt_name_and_comment(header_t *, int, t_data *);
int		my_strcmp(char *, char *);
int		my_strncmp(char *, char *, int);
char		*get_next_line(int);
char		*spec_epur_str(char *, char *);
int		pars_label(char **, int, t_list *, t_data *);
char		*give_label(char *);
int		which_bytes(char *, t_bool *);
unsigned int	which_value(char *, t_list *, t_data *, t_bool *);
int		pars_direct(char *);
int		pars_indirect(char *);
int		pars_register(char *);
int		pars_name_and_comment(int, t_data *);
int		fill_coding_byte(char *, char *, int, t_bool *);
int		check_coding_byte(char, int, t_data *);

#endif /* PARSING_H */
