/*
** define.h for corewar in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Mar 22 21:03:01 2017 Roulleau Julien
** Last update Thu Mar 30 16:16:24 2017 Roulleau Julien
*/

#ifndef DEFINE_H
# define DEFINE_H

# define CHECK_BIT(var, pos) ((var >> pos) & 1)

#endif /* DEFINE_H */
