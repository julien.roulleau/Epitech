/*
** lib.h for corewar in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Mar 22 17:44:54 2017 Roulleau Julien
** Last update Thu Mar 30 16:16:38 2017 Roulleau Julien
*/

#ifndef COREWAR_H
# define COREWAR_H

#include "struct.h"

int		my_strlen(char *);
int		tablen(char **);
int		my_suuuper_strlen(char *, char);
void		my_suuper_putstr(char *, int);
void		my_suuuper_putchar(char, int);
void		super_print_tab(char **, int);
int		my_put_nbr(int);
char		**my_str_to_wordtab(char *, char);
t_bool		*mk_bool(int, int, int);
int		my_getnbr(char *);
char		*new_name(char *);
void		my_memset(char *, int, char);
void		*my_super_memset(void *, int, char);
t_write		*init_w(int);
char		*my_strdup(char *);
char		*correct_param(char *);
void		free_list_and_data(t_list *, t_data *);
void		free_list(t_list *);
void		free_tab(char **, int);
int		print_error_int(char *, int, t_data *, int);
int		is_big_endian();

#endif /* COREWAR_H */
