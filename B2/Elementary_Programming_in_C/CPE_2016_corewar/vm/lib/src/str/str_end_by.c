/*
** str_end_by.c for corewar in /CPE_2016_corewar/vm/lib/src/str/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Thu Mar 23 16:05:45 2017 Fesquet David
** Last update Thu Mar 23 16:48:19 2017 Fesquet David
*/

int	str_end_by(char *str, char *end)
{
	int	i1;
	int	i2;

	if (!str || !end)
		return (0);
	i1 = i2 = 0;
	while (str[i2++]);
	while (end[i1++]);
	i1 -= 1;
	i2 -= 1;
	while (i1 >= 0 && i2 >= 0)
	{
		if (str[i2] != end[i1])
			return (0);
		i1--;
		i2--;
	}
	return (1);
}
