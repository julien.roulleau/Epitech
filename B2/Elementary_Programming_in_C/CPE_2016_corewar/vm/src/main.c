/*
** main.c for corewar in /CPE_2016_corewar/vm/src/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Wed Mar 22 17:27:57 2017 Fesquet David
** Last update Sun Apr  2 23:05:51 2017 Fesquet David
*/

#include "op.h"
#include "init.h"
#include "vm.h"
#include "src.h"

int	main(int ac, char **av)
{
	t_corewar_init	core_i;
	int		i;
	int		err;

	i = 0;
	if (ac < 3)
		return (84);
	core_i = get_option_corewar(ac, av);
	(void)i;
	(void)err;
	return (core_i.error);
}
