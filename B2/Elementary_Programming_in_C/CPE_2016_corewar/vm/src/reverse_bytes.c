/*
** reverse_bytes.c for California Love in /home/benjamin/chikho_b/Corewar/src
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Sun Mar 19 14:26:31 2017 Benjamin
** Last update Sat Apr  1 18:01:42 2017 Fesquet David
*/

#include "init.h"
#include "struct.h"

# define CHECK_BIT(var, pos) ((var >> pos) & 1)

int		is_big_endian()
{
	int	x;

	x = 1;
	if ((int) (((char *)&x)[0]) == 1)
		return (0);
	return (1);
}

u_bytes		reverse_bytes(int nb)
{
  char          n;
  u_bytes       bytes;

  bytes.n = nb;
  if ((is_big_endian()) == 1)
    return (bytes);
  n = bytes.b[0];
  bytes.b[0] = bytes.b[3];
  bytes.b[3] = n;
  n = bytes.b[1];
  bytes.b[1] = bytes.b[2];
  bytes.b[2] = n;
  return (bytes);
}

s_bytes		reverse_two_bytes(short int nb)
{
  char		n;
  s_bytes	bytes;

  bytes.n = nb;
  if ((is_big_endian()) == 1)
    return (bytes);
  n = bytes.b[0];
  bytes.b[0] = bytes.b[1];
  bytes.b[1] = n;
  return (bytes);
}

int		check_coding_byte(char coding, int place)
{
  if (((CHECK_BIT(coding, place)) == 1) &&
      (CHECK_BIT(coding, (place - 1))) == 0)
    return (4);
  else if (((CHECK_BIT(coding, place)) == 1) &&
	   (CHECK_BIT(coding, (place - 1))) == 1)
    return (2);
  else if (((CHECK_BIT(coding, place)) == 0) &&
	   (CHECK_BIT(coding, (place - 1))) == 1)
    return (1);
  return (0);
}
