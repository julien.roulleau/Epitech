/*
** get_opt_corewar.c for corewar in /CPE_2016_corewar/vm/src/option/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Wed Mar 22 18:27:06 2017 Fesquet David
** Last update Sat Apr  1 17:59:10 2017 Fesquet David
*/

#include <stdlib.h>
#include "init.h"

int	set_champ_number(t_corewar_init *core)
{
	int	i;
	int	i2;
	int	nb;

	i = nb = i2 = 0;
	while (i < core->nb_champ)
	{
		if (core->champ[i].nb == -1)
		{
			core->champ[i].nb = ++nb;
		}
		i++;
	}
	return (0);
}

t_corewar_init	get_option_corewar(int ac, char **av)
{
	int		i;
	int		ichamp;
	t_corewar_init	core;

	i = 1;
	ichamp = core.error = 0;
	core.nb_champ = c_champ(ac, av);
	if ((core.champ = malloc(sizeof(t_champ_init) * core.nb_champ)) == NULL
		&& (core.error = 1))
		return (core);
	i += get_dump(&core, ac, av);
	while (ichamp < core.nb_champ)
	{
		i = get_champ(i, &(core.champ[ichamp]), ac, av);
		if (i == -1 && (core.error = 2))
			return (core);
		if (core.champ[ichamp].head.magic != COREWAR_EXEC_MAGIC
			&& (core.error = 3))
			return (core);
		ichamp++;
	}
	set_champ_number(&core);
	if ((i = set_head_champ(&core)))
		core.error = i;
	return (core);
}
