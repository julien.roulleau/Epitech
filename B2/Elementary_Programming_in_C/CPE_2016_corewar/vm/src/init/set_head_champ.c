/*
** set_head_champ.c for corewar in /CPE_2016_corewar/vm/src/init/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Mon Mar 27 18:33:46 2017 Fesquet David
** Last update Thu Mar 30 19:02:50 2017 Fesquet David
*/

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

#include "init.h"
#include "op.h"
#include "struct.h"

#include <stdio.h>

static int	set_head(t_champ_init *champ, int fd)
{
	if (read(fd, &(champ->head), sizeof(header_t)) <= 0)
		return (1);
		champ->head.magic = reverse_bytes(champ->head.magic).n;
		champ->head.prog_size = reverse_bytes(champ->head.prog_size).n;
	return (0);
}

static int	set_champ(t_champ_init *champ, int fd)
{
	if (!(champ->champ = malloc(sizeof(char) * champ->head.prog_size)))
		return (1);
	read(fd, champ->champ, champ->head.prog_size);
	return (0);
}

int	set_head_champ(t_corewar_init *core)
{
	int	fd;
	int	i;

	i = 0;
	while (i < core->nb_champ)
	{
		if ((fd = open(core->champ[i].name, O_RDONLY)) < 0)
			return (3);
		if (set_head(&(core->champ[i]), fd))
			return (4);
		if (set_champ(&(core->champ[i]), fd))
			return (5);
		close(fd);
		i++;
	}
	return (0);
}
