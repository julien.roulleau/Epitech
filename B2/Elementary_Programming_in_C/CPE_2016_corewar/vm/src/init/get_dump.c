/*
** get_dump.c for corewar in /CPE_2016_corewar/vm/src/option/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Wed Mar 22 18:45:39 2017 Fesquet David
** Last update Sun Apr  2 23:08:10 2017 Fesquet David
*/

#include "src.h"
#include "init.h"

#define DUMP_OPT "-dump"

int	get_dump(t_corewar_init *core, int ac, char **av)
{
	(void)ac;
	if (my_strcmp(av[1], DUMP_OPT))
	{
		if (ac < 2)
			return (0);
		core->dump_cycle = my_getnbr(av[2]);
		return (2);
	}
	return (0);
}
