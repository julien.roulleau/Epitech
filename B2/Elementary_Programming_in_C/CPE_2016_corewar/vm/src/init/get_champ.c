/*
** get_champ.c for corewar in /CPE_2016_corewar/vm/src/option/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Wed Mar 22 18:45:22 2017 Fesquet David
** Last update Fri Mar 24 15:39:03 2017 Fesquet David
*/

#include "src.h"
#include "init.h"

#define COR ".cor"
#define A_OPT "-a"
#define N_OPT "-n"

int	get_champ(int i, t_champ_init *chmp, int ac, char **av)
{
	chmp->adr = chmp->nb = -1;
	chmp->name = NULL;
	while (av[i] && !str_end_by(av[i], COR))
	{
		if (my_strcmp(av[i], A_OPT) && (i += 2))
		{
			if (i >= ac || chmp->adr != -1 || !is_num(av[i - 1]))
				return (-1);
			chmp->adr = my_getnbr(av[i - 1]);
		}
		else if (my_strcmp(av[i], N_OPT) && (i += 2))
		{
			if (i >= ac || chmp->nb != -1 || !is_num(av[i - 1]))
				return (-1);
			chmp->nb = my_getnbr(av[i - 1]);
		}
		else
			return (-1);
	}
	if (!av[i])
		return (-1);
	chmp->name = my_strdup(av[i]);
	return (i + 1);
}
