/*
** select_champ.c for corewar in /home/na/Dropbox/Job/En_Cours/CPE_2016_corewar/vm/src/init/champ/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Mar 31 12:51:28 2017 Roulleau Julien
** Last update Sun Apr  2 00:45:59 2017 Roulleau Julien
*/

#include "init_champ.h"

static int	one_set(t_corewar *core, t_corewar_init *core_init)
{
	int	nb;

	nb = find_unset_champ(core, core_init, 0);
	set_default(core, core_init, core_init->champ[nb].adr, nb);
	return (1);
}

static int	all_set(t_corewar *core, t_corewar_init *core_init)
{
	int 	i;

	i = -1;
	while (++i < core->nb_champ)
		if (core_init->champ[i].adr != 0)
			if (!put_champ(core, core_init,
					i, core_init->champ[i].adr))
				return (0);
	return (1);
}

static int	put_big_place(t_corewar *core,
			t_corewar_init *core_init, int champ)
{
	t_pos2i	*pos;
	int	set;

	pos = find_big_place(core);
	if (pos->from < pos->to)
	{
		set = pos->from + (pos->to - pos->from) / 2 -
			(core_init->champ[champ].head.prog_size / 2);
		if (!put_champ(core, core_init, champ, set))
			return (0);
	}
	else if (pos->from > pos->to)
	{
		pos->from -= MEM_SIZE;
		set = pos->from + (pos->to - pos->from) / 2 -
			(core_init->champ[champ].head.prog_size / 2);
		if (set < 0)
			set += MEM_SIZE;
		if (!put_champ(core, core_init, champ, set))
			return (0);
	}
	return (0);
}

static int	multi_set(t_corewar *core, t_corewar_init *core_init, int nb)
{
	int	un[2];

	if (!all_set(core, core_init))
		return (0);
	if (nb - core->nb_champ == 1)
	{
		un[0] = find_unset_champ(core, core_init, 0);
		if (!put_big_place(core, core_init, un[0]))
			return (0);
	}
	else
	{
		un[0] = find_unset_champ(core, core_init, 0);
		un[1] = find_unset_champ(core, core_init, un[0]);
		if (un[0] > un[1] && !put_big_place(core, core_init, un[0]))
			return (0);
		else if (!put_big_place(core, core_init, un[1]))
			return (0);
		if (un[0] > un[1] && !put_big_place(core, core_init, un[1]))
			return (0);
		else if (!put_big_place(core, core_init, un[0]))
			return (0);
	}
	return (1);
}

int		set_select(t_corewar *core, t_corewar_init *core_init, int nb)
{
	if (nb == 1 && !one_set(core, core_init))
			return (0);
	else if (nb == core->nb_champ && !all_set(core, core_init))
			return (0);
	else if (!multi_set(core, core_init, nb))
			return (0);
	return (1);
}
