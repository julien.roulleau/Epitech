/*
** set_pos.c for corewar in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Mar 30 23:11:27 2017 Roulleau Julien
** Last update Sun Apr  2 19:52:52 2017 Roulleau Julien
*/

#include <stdlib.h>
#include "init_champ.h"

int		put_champ(t_corewar *core, t_corewar_init *core_init,
				int pos, int champ)
{
	int	i;

	i = -1;
	while (++i < core_init->champ[champ].head.prog_size)
	{
		if (core_init->champ[champ].champ[i] != 0)
			return (0);
		else
			core->vm[pos++] = core_init->champ[champ].champ[i];
		if (pos >= MEM_SIZE)
			pos -= MEM_SIZE;
	}
	return (1);
}

int		cal_dist(t_corewar *core, t_corewar_init *core_init)
{
	int	sum;
	int	i;

	sum = 0;
	i = -1;
	while (++i < core->nb_champ)
		sum += core_init->champ[i].head.prog_size;
	return ((MEM_SIZE - sum) / core->nb_champ);
}

int		set_default(t_corewar *core,
			t_corewar_init *core_init, int pos, int champ)
{
	int 	dist;
	int	i;

	i = -1;
	dist = cal_dist(core, core_init);
	while (++i < core->nb_champ)
	{
		if (!put_champ(core, core_init, pos, champ))
			return (0);
		pos += dist + core_init->champ[champ].head.prog_size;
		champ++;
		if (champ >= core->nb_champ)
			champ -= core->nb_champ;
		if (pos >= MEM_SIZE)
			pos -= MEM_SIZE;
	}
	return (1);
}

int	find_unset_champ(t_corewar *core, t_corewar_init *core_init, int i)
{
	i--;
	while (++i < core->nb_champ)
		if (core_init->champ[i].adr != -1)
			return (i);
	return (0);
}

t_pos2i		*find_big_place(t_corewar *core)
{
	t_pos2i	*pos;

	pos = malloc(sizeof(t_pos2i));
	pos->to = 0;
	pos->from = 0;
	return (pos);
}
