/*
** init_corewar.c for corewar in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Mar 30 15:27:45 2017 Roulleau Julien
** Last update Sun Apr  2 17:33:24 2017 Roulleau Julien
*/

#include <stdlib.h>
#include "init_champ.h"

static int	set_value(t_corewar *core, t_corewar_init *core_init)
{
	int	i;
	int	n;

	core->nb_champ = core_init->nb_champ;
	if (!(core->champs = malloc(sizeof(t_champ) * core->nb_champ)))
		return (0);
	core->dump_cycle = core_init->dump_cycle;
	core->lives = 0;
	core->cycle_die = CYCLE_TO_DIE;
	core->t_lives = 0;
	core->t_cycle = 0;
	i = -1;
	while (++i < core->nb_champ)
	{
		n = -1;
		core->champs[i].nb = core_init->champ[i].nb;
		core->champs[i].pc->carry = 0;
		while (++n < 16)
			core->champs[i].pc->reg[n] = 0;
		core->champs[i].pc->reg[0] = core->nb_champ;
		core->champs[i].pc->cycle = -1;
		core->champs[i].pc->f = 0;
		core->champs[i].pc->adr = core_init->champ[i].adr;
	}
	return (1);
}

static void 	init_vm(char *vm, char *vm_graph)
{
	int 	i;

	i = -1;
	while (++i < MEM_SIZE)
		vm[i] = 0;
	i = -1;
	while (++i < MEM_SIZE)
		vm[i] = -1;
}

int		init_corewar(t_corewar *core, t_corewar_init *core_init)
{
	if (!set_value(core, core_init))
		return (0);
	init_vm(core->vm, core->vm_graph);
	if (!set_pos_champ(core, core_init))
		return (0);
	return (1);
}
