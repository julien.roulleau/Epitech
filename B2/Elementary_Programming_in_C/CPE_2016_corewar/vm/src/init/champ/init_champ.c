/*
** init_pos.c for corewar in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Mar 30 22:47:16 2017 Roulleau Julien
** Last update Fri Mar 31 13:41:58 2017 Roulleau Julien
*/

#include "init_champ.h"

int 		set_pos_champ(t_corewar *core, t_corewar_init *core_init)
{
	int	i;
	int	nb;

	i = -1;
	nb = 0;
	while (++i < core->nb_champ)
		if (core_init->champ[i].adr != -1)
			nb++;
	if (nb == 0)
	{
		if (!set_default(core, core_init, 0, 0))
			return (0);
	}
	else
	{
		if (!set_select(core, core_init, nb))
			return (0);
	}
	return (1);
}
