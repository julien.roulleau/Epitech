/*
** count_champ.c for corewar in /CPE_2016_corewar/vm/src/option/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Wed Mar 22 18:29:34 2017 Fesquet David
** Last update Fri Mar 24 15:38:55 2017 Fesquet David
*/

#include "init.h"
#include "src.h"

#define COR ".cor"

int		c_champ(int ac, char **av)
{
	int	nb;
	int	i;

	nb = i = 0;
	while (i < ac)
	{
		if (str_end_by(av[i], COR))
			nb++;
		i++;
	}
	return (nb);
}
