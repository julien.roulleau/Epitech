/*
** get_param.c for corewar in /home/david/project/en_cour/CPE_2016_corewar/vm/src/game/core_fct.c/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Sat Apr  1 17:43:40 2017 Fesquet David
** Last update Sun Apr  2 16:56:48 2017 Fesquet David
*/

#include <stdlib.h>
#include "vm.h"
#include "struct.h"

int	set_for_bytes(t_corewar *core, t_pc *cur, int pos, int val)
{
	u_bytes	val2;

	val2.n = val;
	(void)cur;
	core->vm[ADR(pos)] = val2.b[0];
	core->vm[ADR(pos + 1)] = val2.b[1];
	core->vm[ADR(pos + 2)] = val2.b[2];
	core->vm[ADR(pos + 3)] = val2.b[3];
	return (0);
}

int	get_for_bytes(t_corewar *core, t_pc *cur, int pos)
{
	u_bytes	val;

	(void)cur;
	val.b[0] = core->vm[ADR(pos)];
	val.b[1] = core->vm[ADR(pos + 1)];
	val.b[2] = core->vm[ADR(pos + 2)];
	val.b[3] = core->vm[ADR(pos + 3)];
	val = reverse_bytes(val.n);
	return (val.n);
}

int	get_two_bytes(t_corewar *core, t_pc *cur, int pos)
{
	s_bytes	val;

	(void)cur;
	val.b[0] = core->vm[ADR(pos)];
	val.b[1] = core->vm[ADR(pos + 1)];
	val = reverse_two_bytes(val.n);
	return (val.n);
}

int	*get_param(t_corewar *core, int nb, t_pc *cur)
{
	int	i;
	int	place;
	int	*params;

	i = 0;
	place = 7;
	if (!(params = malloc(sizeof(int) * nb)))
		return (NULL);
	while (i < nb)
	{
		params[i] = check_coding_byte(core->vm[cur->adr + 1], place);
		i++;
		place -= 2;
	}
	return (params);
}
