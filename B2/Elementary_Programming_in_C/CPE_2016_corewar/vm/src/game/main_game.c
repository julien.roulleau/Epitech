/*
** main_game.c for corewar in /CPE_2016_corewar/vm/src/game/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Wed Mar 29 14:14:37 2017 Fesquet David
** Last update Wed Mar 29 15:37:44 2017 Fesquet David
*/

#include "vm.h"
#include "op.h"

int	game(t_corewar *core)
{
	int	champ_alive;
	int	cycle;

	champ_alive = core->nb_champ;
	while (champ_alive >= 2)
	{
		exec_pt(core);
		if (++cycle >= core->cycle_die)
		{
			cycle = 0;
			champ_alive -= kill_champ(core);
		}
		core->t_cycle++;
	}
	return (0);
}
