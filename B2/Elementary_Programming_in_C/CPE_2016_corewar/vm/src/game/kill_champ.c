/*
** kill_champ.c for corewar in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Mar 30 13:27:20 2017 Roulleau Julien
** Last update Thu Mar 30 13:36:09 2017 Roulleau Julien
*/

#include <stdlib.h>
#include "vm.h"

int		kill_champ(t_corewar *cw)
{
	int	nb_champ;
	int	i;

	nb_champ = 0;
	i = -1;
	while (++i < cw->nb_champ)
	{
		if (cw->champs[i].live > 0)
			nb_champ++;
		else if (cw->champs[i].live <= 0)
		{
			free (cw->champs[i].pc);
			cw->champs[i].pc = 0;
		}
	}
	return (nb_champ);
}
