/*
** xor.c for corewar in /CPE_2016_corewar/vm/src/game/core_fct.c/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Thu Mar 30 15:52:46 2017 Fesquet David
** Last update Thu Mar 30 15:52:59 2017 Fesquet David
*/

#include "vm.h"

int	core_xor(t_corewar *core, int ichamp, t_pc *current, int cycle)
{
	if (current->cycle == -1)
		current->cycle = cycle;
	if (current->cycle > 0)
	{
		current->cycle -= 1;
	}
	return (0);
}
