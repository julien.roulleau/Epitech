/*
** st.c for corewar in /CPE_2016_corewar/vm/src/game/core_fct.c/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Thu Mar 30 15:46:39 2017 Fesquet David
** Last update Sun Apr  2 16:57:15 2017 Fesquet David
*/

#include <stdlib.h>
#include "vm.h"
#include "struct.h"

static int	st_c(int *param, t_corewar *core, int nb, t_pc *cur)
{
	int	val;
	int	val2;

	if (param == NULL || param[0] != 1 || (param[1] != 1 || param[1] != 2))
		return (1);
	val2 = core->vm[ADR(cur->adr + 1)];
	if ((val2 < 0 || val2 > REG_NUMBER))
		return (1);
	val = core->vm[ADR(cur->adr + 1 + param[0])];
	if (param[1] == 1)
	{
		if (val < 0 || val > REG_NUMBER)
			return (1);
		cur->reg[val2] = val;
	}
	else if (param[1] == 2)
	{
		val = get_two_bytes(core, cur, cur->adr + 2);
		set_for_bytes(core, cur, cur->adr + val % IDX_MOD, val2);
	}
	return (param[0] + param[1] + 2);
}

int	core_st(t_corewar *core, int nb, t_pc *cur, int cycle)
{
	int	jump;

	if (cur->cycle == -1)
		cur->cycle = cycle;
	if (cur->cycle > 0)
	{
		cur->cycle -= 1;
	}
	else
	{
		jump = st_c(get_param(core, nb, cur), core, nb, cur);
		cur->adr = ADR(cur->adr + jump);
		cur->cycle = -1;
	}
	return (0);
}
