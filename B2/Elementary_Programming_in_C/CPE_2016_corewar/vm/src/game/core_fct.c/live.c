/*
** live.c for corewar in /CPE_2016_corewar/vm/src/game/core_fct.c/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Thu Mar 30 15:30:43 2017 Fesquet David
** Last update Sat Apr  1 21:51:05 2017 Fesquet David
*/

#include "vm.h"
#include "struct.h"
#include "src.h"

static int	is_alive(int id)
{
	my_putstr("The player ");
	my_putnbr(id);
	my_putstr("is alive.\n");
	return (0);
}

static int	live_c(t_corewar *core, t_pc *current)
{
	u_bytes	id;
	int	i;

	id.b[0] = core->vm[current->adr + 1];
	id.b[1] = core->vm[current->adr + 2];
	id.b[2] = core->vm[current->adr + 3];
	id.b[3] = core->vm[current->adr + 4];
	id = reverse_bytes(id.n);
	current->adr += 5;
	i = 0;
	while (i < core->nb_champ && core->champs[i].nb != id.n)
		i++;
	if (i == core->nb_champ)
		return (1);
	core->t_lives += 1;
	core->lives += 1;
	core->champs[i].t_live += 1;
	if (++(core->lives) == 40 && (core->lives = 0))
		core->cycle_die -= CYCLE_DELTA;
	is_alive(id.n);
	return (0);
}

int	core_live(t_corewar *core, int nb, t_pc *current, int cycle)
{
	(void)nb;
	if (current->cycle == -1)
		current->cycle = cycle;
	if (current->cycle > 0)
		current->cycle -= 1;
	else
	{
		if (live_c(core, current))
			current->adr -= 4;
		current->cycle = -1;
	}
	return (0);
}
