/*
** add.c for corewar in /CPE_2016_corewar/vm/src/game/core_fct.c/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Thu Mar 30 15:37:54 2017 Fesquet David
** Last update Sun Apr  2 16:59:13 2017 Fesquet David
*/

#include <stdlib.h>
#include "vm.h"
#include "struct.h"

static int	add_c(int *param, t_corewar *core, int nb, t_pc *cur)
{
	if (param == NULL || param[0] != 1 || param[1] != 1 || param[2] != 1)
	{
		cur->adr = ADR(cur->adr + 1);
		return (1);
	}
	return (param[0] + param[1] + param[2] + 2);
}

int	core_add(t_corewar *core, int nb, t_pc *cur, int cycle)
{
	int	jump;

	if (cur->cycle == -1)
		cur->cycle = cycle;
	if (cur->cycle > 0)
	{
		cur->cycle -= 1;
	}
	else
	{
		jump = add_c(get_param(core, nb, cur), core, nb, cur);
		cur->adr = ADR(cur->adr + jump);
		cur->cycle = -1;
	}
	return (0);
}
