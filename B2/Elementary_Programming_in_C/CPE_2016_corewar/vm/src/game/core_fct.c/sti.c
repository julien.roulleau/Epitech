/*
** sti.c for corewar in /CPE_2016_corewar/vm/src/game/core_fct.c/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Thu Mar 30 15:51:54 2017 Fesquet David
** Last update Sun Apr  2 23:00:03 2017 Fesquet David
*/

#include <stdlib.h>
#include "vm.h"
#include "struct.h"

static int	sti_c(int *param, t_corewar *core, int nb, t_pc *cur)
{
	int	val;
	int	val2;

	if (param == NULL || param[0] != 1 || (param[1]))
		return (1);
	if (param[0] == 2)
	return ();
}

int	core_sti(t_corewar *core, int nb, t_pc *cur, int cycle)
{
	int	jump;

	if (cur->cycle == -1)
		cur->cycle = cycle;
	if (cur->cycle > 0)
	{
		cur->cycle -= 1;
	}
	else
	{
		jump = sti_c(get_param(core, nb, cur), core, nb, cur);
		cur->adr = ADR(cur->adr + jump);
		cur->cycle = -1;
	}
	return (0);
}
