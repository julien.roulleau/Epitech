/*
** ld.c for corewar in /CPE_2016_corewar/vm/src/game/core_fct.c/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Thu Mar 30 15:40:19 2017 Fesquet David
** Last update Sun Apr  2 16:57:07 2017 Fesquet David
*/

#include <stdlib.h>
#include "vm.h"
#include "struct.h"

static int	ld_c(int *param, t_corewar *core, int nb, t_pc *cur)
{
	int	val;
	int	val2;

	if (param == NULL)
		return (1);
	if (param[0] == 2)
	{
		val = get_two_bytes(core, cur, cur->adr + 2);
		val = get_for_bytes(core, cur, cur->adr + val % IDX_MOD);
	}
	else if (param[0] == 4)
		val = get_for_bytes(core, cur, cur->adr + 2);
	val2 = core->vm[cur->adr + 1 + param[0]];
	if (param[1] == 1 && (val2 > 0 && val2 < REG_NUMBER))
		cur->reg[val2] = val;
	cur->adr = ADR(cur->adr + param[0] + param[1]);
	return (param[0] + param[1] + 2);
}

int	core_ld(t_corewar *core, int nb, t_pc *cur, int cycle)
{
	int	jump;

	if (cur->cycle == -1)
		cur->cycle = cycle;
	if (cur->cycle > 0)
	{
		cur->cycle -= 1;
	}
	else
	{
		jump = ld_c(get_param(core, nb, cur), core, nb, cur);
		cur->adr = ADR(cur->adr + jump);
		cur->cycle = -1;
	}
	return (0);
}
