/*
** exec_pt.c for corewar in /CPE_2016_corewar/vm/src/game/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Thu Mar 30 13:17:51 2017 Fesquet David
** Last update Sat Apr  1 18:13:27 2017 Fesquet David
*/

#include "vm.h"
#include "fct_champ.h"
#include <stdlib.h>

t_ptr   ptr[] =
{
	{1, 10, &core_live, 1},
	{2, 5, &core_ld, 2},
	{3, 5, &core_st, 2},
	{4, 10, &core_add, 3},
	{5, 10, &core_sub, 3},
	{6, 6, &core_and, 3},
	{7, 6, &core_or, 3},
	{8, 6, &core_xor, 3},
	{9, 20, &core_zjump, 1},
	{10, 25, &core_ldi, 3},
	{11, 25, &core_sti, 3},
	{12, 800, &core_fork, 1},
	{13, 10, &core_lld, 2},
	{14, 50, &core_lldi, 3},
	{15, 1000, &core_lfork, 1},
	{16, 2, &core_aff, 1},
	{0, 0, NULL}
};

int	exec_champ_pc(t_corewar *core, int ichamp)
{
	t_pc	*cur;

	cur = core->champs[ichamp].pc;
	while (cur != NULL)
	{
		if (ptr[cur->f].cycle == -1 || cur->f == 0)
		{
			cur->f = 0;
			ptr[cur->f].cycle = -1;
			while (core->vm[cur->adr] != ptr[cur->f].flag &&
				ptr[cur->f].flag != 0)
				(cur->f)++;
		}
		if (ptr[cur->f].flag != 0)
			ptr[cur->f].ptr(core, ptr[cur->f].nb,
				cur, ptr[cur->f].cycle);
		else
			cur->adr += 1;
		cur = cur->next;
	}
	return (0);
}

int	exec_pt(t_corewar *core)
{
	int	ichamp;

	ichamp = 0;
	while (ichamp < core->nb_champ)
	{
		if (core->champs[ichamp].nb_pc)
			exec_champ_pc(core, ichamp);
		ichamp++;
	}
	return (0);
}
