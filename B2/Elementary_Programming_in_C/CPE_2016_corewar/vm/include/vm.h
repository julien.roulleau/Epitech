/*
** vm.h for  in /home/david/project/en_cour/CPE_2016_corewar/vm/include/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Fri Mar 24 12:28:06 2017 Fesquet David
** Last update Sun Apr  2 23:00:59 2017 Fesquet David
*/

#ifndef VM_H_
#define VM_H_

#include "op.h"
#include "init.h"

#define ADR(n) ((n < MEM_SIZE)? n : n - MEM_SIZE)

typedef struct	 	s_pc
{
	int		adr;
	int		carry;
	int		reg[REG_NUMBER];
	int		cycle;
	int		f;
	struct s_pc	*next;
}			t_pc;

typedef struct s_champ
{
	int	nb;
	t_pc	*pc;
	int	nb_pc;
	int	live;
	int	t_live;
}		t_champ;

typedef struct s_corewar
{
	char	vm[MEM_SIZE];
	char	vm_graph[MEM_SIZE];
	int	nb_champ;
	t_champ *champs;
	int	dump_cycle;
	int	lives;
	int	cycle_die;
	int	t_lives;
	int	t_cycle;
}		t_corewar;

typedef struct s_ptr
{
	char	flag;
	int	cycle;
	int	(*ptr)(t_corewar*, int, t_pc*, int);
	int	nb;
}		t_ptr;

int	init_corewar(t_corewar *core, t_corewar_init *core_init);
int	game(t_corewar *);
int	exec_pt(t_corewar*);
int	kill_champ(t_corewar*);
int	*get_param(t_corewar *core, int nb, t_pc *cur);

#endif
