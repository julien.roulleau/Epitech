/*
** option.h for corewar in /CPE_2016_corewar/vm/include/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Wed Mar 22 17:49:24 2017 Fesquet David
** Last update Sun Apr  2 23:00:40 2017 Fesquet David
*/

#ifndef OPTION_H_
#define OPTION_H_

#include "op.h"

typedef struct	s_champ_init
{
	header_t	head;
	char		*name;
	int		adr;
	int		nb;
	char		*champ;
}			t_champ_init;

typedef struct	s_corewar_init
{
	t_champ_init	*champ;
	int		nb_champ;
	int		dump_cycle;
	int		error;
}			t_corewar_init;

int	c_champ(int ac, char **av);
int	get_dump(t_corewar_init *core, int ac, char **av);
int	get_champ(int i, t_champ_init *chmp, int ac, char **av);
int	set_head_champ(t_corewar_init *core);
t_corewar_init	get_option_corewar(int ac, char **av);

#endif
