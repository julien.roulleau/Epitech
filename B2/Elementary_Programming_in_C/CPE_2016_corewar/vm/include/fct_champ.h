/*
** fct_champ.h for corewar in /CPE_2016_corewar/vm/include/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Thu Mar 30 13:27:05 2017 Fesquet David
** Last update Thu Mar 30 15:34:35 2017 Fesquet David
*/

#ifndef FCT_CHAMP_H_
#define FCT_CHAMP_H_

#include "vm.h"

int	core_add(t_corewar *, int, t_pc*, int);
int	core_aff(t_corewar *, int, t_pc*, int);
int	core_and(t_corewar *, int, t_pc*, int);
int	core_fork(t_corewar *, int, t_pc*, int);
int	core_ld(t_corewar *, int, t_pc*, int);
int	core_ldi(t_corewar *, int, t_pc*, int);
int	core_lfork(t_corewar *, int, t_pc*, int);
int	core_live(t_corewar *, int, t_pc*, int);
int	core_lld(t_corewar *, int, t_pc*, int);
int	core_lldi(t_corewar *, int, t_pc*, int);
int	core_or(t_corewar *, int, t_pc*, int);
int	core_st(t_corewar *, int, t_pc*, int);
int	core_sti(t_corewar *, int, t_pc*, int);
int	core_sub(t_corewar *, int, t_pc*, int);
int	core_xor(t_corewar *, int, t_pc*, int);
int	core_zjump(t_corewar *, int, t_pc*, int);

#endif
