/*
** init_champ.h for corewar in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Mar 30 23:12:14 2017 Roulleau Julien
** Last update Sun Apr  2 01:06:35 2017 Roulleau Julien
*/

#ifndef INIT_CHAMP_H
# define INIT_CHAMP_H

#include "vm.h"

typedef struct		s_pos2i
{
	int		from;
	int		to;
}			t_pos2i;

int 	set_pos_champ(t_corewar *core, t_corewar_init *core_init);
int	put_champ(t_corewar *, t_corewar_init *, int pos, int champ);
int	set_select(t_corewar *core, t_corewar_init *core_init, int nb);
int	cal_dist(t_corewar *core, t_corewar_init *core_init);
int	set_default(t_corewar *,t_corewar_init *, int pos, int champ);
int	find_unset_champ(t_corewar *core, t_corewar_init *core_init, int);
t_pos2i	*find_big_place(t_corewar *core);


#endif /* INIT_CHAMP_H */
