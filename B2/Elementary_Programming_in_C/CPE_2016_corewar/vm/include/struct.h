/*
** struct.h for corewar in /CPE_2016_corewar/vm/include/
**
** Made by Fesquet David
** Login   <david.fesquet@epitech.eu>
**
** Started on  Thu Mar 30 18:55:59 2017 Fesquet David
** Last update Sun Apr  2 12:30:12 2017 Fesquet David
*/

#ifndef STRUCT_H_
#define STRUCT_H_

#include "vm.h"

typedef union	bytes
{
  int		n;
  char		b[4];
}		u_bytes;

typedef union	t_bytes
{
  short int	n;
  char		b[2];
}		s_bytes;

u_bytes		reverse_bytes(int nb);
int		check_coding_byte(char coding, int place);
s_bytes		reverse_two_bytes(short int nb);
int		get_two_bytes(t_corewar *core, t_pc *cur, int pos);
int		get_for_bytes(t_corewar *core, t_pc *cur, int pos);
int		set_for_bytes(t_corewar *core, t_pc *cur, int pos, int val);


#endif
