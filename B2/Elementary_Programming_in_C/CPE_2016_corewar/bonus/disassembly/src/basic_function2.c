/*
** basic_function.c for  in /home/benjamin/chikho_b/
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Wed Mar 22 03:09:02 2017 Benjamin
** Last update Thu Mar 30 16:23:28 2017 Roulleau Julien
*/

#include "dis.h"

t_write		*init_w(int inst)
{
  t_write	*w;

  if ((w = malloc(sizeof(t_write))) == NULL)
    return (NULL);
  w = memset(w, 0, sizeof(t_write));
  w->c = inst;
  w->coding = 0;
  return (w);
}

static void	cut_str(char **str)
{
  int		i;

  i = my_strlen(*str);
  while (--i != 0 && (*str)[i] != '/');
  if ((*str)[i] == '/')
    ++i;
  *str = *str + i;
}

char		*new_name(char *str)
{
  char		*new;
  int		i;

  cut_str(&str);
  if ((new = malloc(sizeof(char) * (my_suuuper_strlen(str, '.') + 3))) == \
      NULL)
    return (NULL);
  i = -1;
  while (str[++i] && str[i] != '.')
    new[i] = str[i];
  new[i] = '.';
  new[++i] = 's';
  new[++i] = '\0';
  return (new);
}
