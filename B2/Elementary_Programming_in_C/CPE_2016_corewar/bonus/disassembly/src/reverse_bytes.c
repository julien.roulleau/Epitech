/*
** reverse_bytes.c for California Love in /home/benjamin/
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Sun Mar 19 14:26:31 2017 Benjamin
** Last update Thu Mar 30 16:23:40 2017 Roulleau Julien
*/

#include "dis.h"

u_bytes		reverse_bytes(int nb)
{
  char          n;
  u_bytes       bytes;

  bytes.n = nb;
  n = bytes.b[0];
  bytes.b[0] = bytes.b[3];
  bytes.b[3] = n;
  n = bytes.b[1];
  bytes.b[1] = bytes.b[2];
  bytes.b[2] = n;
  return (bytes);
}

s_bytes		reverse_two_bytes(short int nb)
{
  char		n;
  s_bytes	bytes;

  bytes.n = nb;
  n = bytes.b[0];
  bytes.b[0] = bytes.b[1];
  bytes.b[1] = n;
  return (bytes);
}
