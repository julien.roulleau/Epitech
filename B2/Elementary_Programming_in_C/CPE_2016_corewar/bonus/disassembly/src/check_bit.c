/*
** check_bit.c for check_bit in /home/benjamin/chikho_b/Corewar/src
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Sat Mar 18 18:57:21 2017 Benjamin
** Last update Wed Mar 22 06:56:43 2017 Benjamin
*/

#include "dis.h"

int		check_coding_byte(char coding, int place)
{
  if (((CHECK_BIT(coding, place)) == 1) &&
      (CHECK_BIT(coding, (place - 1))) == 0)
    return (4);
  else if (((CHECK_BIT(coding, place)) == 1) &&
	   (CHECK_BIT(coding, (place - 1))) == 1)
    return (2);
  else if (((CHECK_BIT(coding, place)) == 0) &&
	   (CHECK_BIT(coding, (place - 1))) == 1)
    return (1);
  return (0);
}

static int	part_two(int nb, char *buf, t_data *data, int ble)
{
  t_write	w;

  if (nb == 1)
    {
      read(data->fd, &(w.c), sizeof(char));
      sprintf(buf, "%hhd", w.c);
      dprintf(data->nfd, "r%s", buf);
      data->pc = data->pc + 1;
    }
  else if (nb == 2 && ble == 1)
    {
      read(data->fd, &(w.k), sizeof(short int));
      sprintf(buf, "%hd", reverse_two_bytes((short int)(w.k)).n);
      dprintf(data->nfd, "%s", buf);
      data->pc = data->pc + 2;
    }
  else
    return (0);
  return (1);
}

static int	check_two_bits(char coding, int place, t_data *data, int ble)
{
  int		nb;
  t_write	w;
  char		buf[34];

  if (data->bool == 1)
    dprintf(data->nfd, ", ");
  if ((nb = check_coding_byte(coding, place)) == 0)
    return (0);
  if (ble == 1 && nb == 4)
    nb = nb - 2;
  if (nb == 4)
    {
      read(data->fd, &(w.n), sizeof(int));
      sprintf(buf, "%d", reverse_bytes(w.n).n);
      dprintf(data->nfd, "%%%s", buf);
    }
  else if (nb == 2 && ble == 0)
    {
      read(data->fd, &(w.k), sizeof(short int));
      sprintf(buf, "%hd", reverse_two_bytes((short int)(w.k)).n);
      dprintf(data->nfd, "%s", buf);
    }
  else
    return ((part_two(nb, buf, data, ble)));
  data->pc = data->pc + nb;
  return (1);
}

int		check_real_coding_byte(char coding, t_data *data, int blee)
{
  data->bool = 0;
  if ((check_two_bits(coding, 7, data, blee)) == 0)
    return (0);
  data->bool = 1;
  if ((check_two_bits(coding, 5, data, blee)) == 0)
    return (0);
  if ((check_two_bits(coding, 3, data, blee)) == 0)
    return (0);
  return (0);
}

int		write_four(t_order order, t_data *data)
{
  t_write	w;
  char		buf[32];

  if (order.ble == 2)
    {
      read(data->fd, &(w.n), sizeof(int));
      sprintf(buf, "%d", reverse_bytes(w.n).n);
      dprintf(data->nfd, "%%%s", buf);
      data->pc = data->pc + 4;
    }
  else if (order.ble == 3)
    {
      read(data->fd, &(w.k), sizeof(short int));
      sprintf(buf, "%hd", reverse_two_bytes((short int)(w.k)).n);
      dprintf(data->nfd, "%%%s", buf);
      data->pc = data->pc + 2;
    }
  return (0);
}
