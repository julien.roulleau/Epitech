/*
** convert_file.c for  in /home/benjamin/chikho_b/Corewar/disassembly/src
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Wed Mar 22 03:59:51 2017 Benjamin
** Last update Wed Mar 22 06:46:47 2017 Benjamin
*/

#include "dis.h"

t_order order[] =
  {
    {"live", 1, 1, {T_DIR}, 2},
    {"ld", 2, 2, {T_DIR | T_IND, T_REG}, 0},
    {"st", 2, 3, {T_REG, T_IND | T_REG}, 0},
    {"add", 3, 4, {T_REG, T_REG, T_REG}, 0},
    {"sub", 3, 5, {T_REG, T_REG, T_REG}, 0},
    {"and", 3, 6, {T_REG | T_DIR | T_IND, T_REG | T_IND | T_DIR, T_REG}, 0},
    {"or", 3, 7, {T_REG | T_IND | T_DIR, T_REG | T_IND | T_DIR, T_REG}, 0},
    {"xor", 3, 8, {T_REG | T_IND | T_DIR, T_REG | T_IND | T_DIR, T_REG}, 0},
    {"zjmp", 1, 9, {T_DIR}, 3},
    {"ldi", 3, 10, {T_REG | T_DIR | T_IND, T_DIR | T_REG, T_REG}, 1},
    {"sti", 3, 11, {T_REG, T_REG | T_DIR | T_IND, T_DIR | T_REG}, 1},
    {"fork", 1, 12, {T_DIR}, 3},
    {"lld", 2, 13, {T_DIR | T_IND, T_REG}, 0},
    {"lldi", 3, 14, {T_REG | T_DIR | T_IND, T_DIR | T_REG, T_REG}, 1},
    {"lfork", 1, 15, {T_DIR}, 3},
    {"aff", 1, 16, {T_REG}, 0},
    {0, 0, 0, {0}, 0}
  };

void		convert_hex_asm(int nfd, int fd, int size)
{
  char		c;
  char		tmp;
  t_data	data;

  data.pc = 0;
  data.nfd = nfd;
  data.fd = fd;
  while (data.pc < size)
    {
      read(fd, &c, 1);
      data.pc = data.pc + 1;
      dprintf(nfd, "%s,\t", order[(int)c - 1].name);
      if (order[((int)c - 1)].ble == 1 || order[((int)c - 1)].ble == 0)
	{
	  read(fd, &tmp, 1);
	  data.pc = data.pc + 1;
	  check_real_coding_byte(tmp, &data, order[((int)c - 1)].ble);
	  dprintf(data.nfd,"\n");
	}
      else if (order[((int)c - 1)].ble == 2 || order[((int)c - 1)].ble == 3)
	{
	  write_four(order[(int)c - 1], &data);
	  dprintf(data.nfd,"\n");
	}
    }
}
