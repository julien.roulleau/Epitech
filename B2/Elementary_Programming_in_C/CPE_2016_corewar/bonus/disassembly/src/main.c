/*
** main.c for main.c in /home/benjamin/chikho_b/Corewar/disassembly/src
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Wed Mar 22 02:43:18 2017 Benjamin
** Last update Wed Mar 22 06:47:45 2017 Benjamin
*/

#include "dis.h"

t_bool		*mk_bool(int r, int i, int d)
{
  t_bool	*bool;

  if ((bool = malloc(sizeof(t_bool))) == NULL)
    return (NULL);
  bool->r = r;
  bool->i = i;
  bool->d = d;
  return (bool);
}

int		write_header(header_t header, int nfd)
{
  write(nfd, ".name \"", 7);
  write(nfd, header.prog_name, my_strlen(header.prog_name));
  write(nfd, "\"\n.comment \"", 12);
  write(nfd, header.comment, my_strlen(header.comment));
  write(nfd, "\"\n\n", 3);
  return (0);
}

int		all_eyez_on_me(int fd, int nfd, header_t header)
{
  write_header(header, nfd);
  convert_hex_asm(nfd, fd, reverse_bytes(header.prog_size).n);
  return (0);
}

int		main(int argc, char **argv)
{
  int		fd;
  int		nfd;
  int		nb;
  header_t	header;

  if (argc == 2)
    {
      if ((fd = open(argv[1], O_RDONLY)) == -1)
	return (-1);
      read(fd, &header, sizeof(header_t));
      nb = reverse_bytes(header.magic).n;
      if ((nfd = open(new_name(argv[1]), O_RDWR | O_CREAT, S_IRWXU)) == -1)
	return (-1);
      if (nb == COREWAR_EXEC_MAGIC)
	all_eyez_on_me(fd, nfd, header);
      else
	printf("bad file\n");
    }
  return (0);
}
