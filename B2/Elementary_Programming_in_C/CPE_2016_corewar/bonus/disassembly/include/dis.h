/*
** dis.h for NIGGA in /home/benjamin/chikho_b/Corewar
**
** Made by Benjamin
** Login   <benjamin@epitech.net>
**
** Started on  Wed Mar 22 02:42:15 2017 Benjamin
** Last update Thu Mar 30 16:22:32 2017 Roulleau Julien
*/

# ifndef DIS_H_
# define DIS_H_

# include <stdio.h>
# include <unistd.h>
# include <stdlib.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <string.h>
# include "op.h"

# define CHECK_BIT(var, pos) ((var >> pos) & 1)

typedef union	bytes
{
  int		n;
  char		b[4];
}		u_bytes;

typedef union	t_bytes
{
  short int	n;
  char		b[2];
}		s_bytes;

typedef struct	s_write
{
  char		c;
  char		coding;
  int		n;
  short int	k;
}		t_write;

typedef struct	s_bool
{
  int		r;
  int		i;
  int		d;
}		t_bool;

typedef struct	s_order
{
  char		*name;
  int		nb_param;
  int		value;
  args_type_t	type[MAX_ARGS_NUMBER];
  int		ble;
}		t_order;

typedef struct	s_data
{
  int		pc;
  int		fd;
  int		nfd;
  char		bool;
}		t_data;

int		check_coding_byte(char coding, int place);
u_bytes		reverse_bytes(int nb);
s_bytes		reverse_two_bytes(short int nb);
t_write		*init_w(int inst);
char		*new_name(char *str);
int		my_strlen(char *str);
int		my_suuuper_strlen(char *str, char c);
void		my_suuper_putstr(char *str, int fd);
void		my_suuuper_putchar(char c, int fd);
void		super_print_tab(char **tab, int fd);
void		convert_hex_asm(int nfd, int fd, int size);
t_bool		*mk_bool(int r, int i, int d);
int		check_real_coding_byte(char coding, t_data *data, int blee);
int		write_four(t_order order, t_data *data);

# endif /* DIS_H_ */
