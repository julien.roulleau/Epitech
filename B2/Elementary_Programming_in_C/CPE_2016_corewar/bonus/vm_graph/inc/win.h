/*
** win.h for corewar in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Mar 28 15:55:21 2017 Roulleau Julien
** Last update Thu Mar 30 14:32:39 2017 Roulleau Julien
*/

#ifndef WIN_H
# define WIN_H

#include "framebuffer.h"

t_window	init_window();
void 		kill_window(t_window);
void		render(t_window, char *);

#endif /* WIN_H */
