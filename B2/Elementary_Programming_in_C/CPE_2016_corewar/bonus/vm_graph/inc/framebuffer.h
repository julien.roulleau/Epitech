/*
** framebuffer.h for corewar in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Mar 15 21:59:21 2017 Roulleau Julien
** Last update Tue Mar 28 23:19:02 2017 Roulleau Julien
*/

#ifndef FRAMEBUFFER_H
# define FRAMEBUFFER_H

# include <SFML/Graphics.h>

# define SCREEN_WIDTH 1200
# define SCREEN_HEIGHT 600
# define BITS_PER_PIXEL 32

typedef struct		s_fb
{
	sfUint8		*pixels;
	int		width;
	int		height;
}			t_fb;

typedef struct		s_window
{
	sfRenderWindow	*window;
	sfSprite	*sprite;
	sfTexture	*texture;
	t_fb		*fb;
}			t_window;

void 		my_put_pixel(t_fb *, int, int, sfColor);
sfVector2i	pos2d(int, int);
sfVector3f	pos3d(int, int, int);
sfColor		color_put(int, int, int, int);
void 		display(t_window, char *);
void 		draw_square(t_fb *, sfVector2i, sfVector2i, sfColor);

#endif /* FRAMEBUFFER_H */
