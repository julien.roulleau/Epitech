/*
** color.h for color in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Mar 29 11:01:17 2017 Roulleau Julien
** Last update Thu Mar 30 14:32:48 2017 Roulleau Julien
*/

#ifndef COLOR_H
# define COLOR_H

#define C_RED color_put(255, 0, 0, 255)
#define C_GREEN color_put(0, 255, 0, 255)
#define C_BLUE color_put(0, 0, 255, 255)
#define C_ORANGE color_put(255, 150, 0, 255)
#define C_GREY color_put(150, 150, 150, 255)
#define C_WHITE color_put(255, 255, 255, 255)

#endif /* COLOR_H */
