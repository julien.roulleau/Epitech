/*
** window.c for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Mar 15 22:03:13 2017 Roulleau Julien
** Last update Tue Mar 28 15:54:50 2017 Roulleau Julien
*/

#include <stdlib.h>
#include "framebuffer.h"

static t_fb* 	my_framebuffer_create(int width, int height)
{
	t_fb	*fb;
	int	p;

	p = -1;
	fb = malloc(sizeof(t_fb));
	fb->pixels = malloc(sizeof(char) * 4 * width * height);
	fb->width = width;
	fb->height = height;
	while (++p < width * height * 4)
			fb->pixels[p] = 0;
	return (fb);
}

t_window		init_window()
{
	t_window	window;
	sfVideoMode	mode;

	mode.width = SCREEN_WIDTH;
	mode.height = SCREEN_HEIGHT;
	mode.bitsPerPixel = BITS_PER_PIXEL;
	window.window = sfRenderWindow_create (mode, "SFMLwindow",
			sfResize | sfClose, NULL);
	window.sprite = sfSprite_create();
	window.texture = sfTexture_create(SCREEN_WIDTH, SCREEN_HEIGHT);
	sfSprite_setTexture(window.sprite, window.texture, sfTrue);
	window.fb = my_framebuffer_create(SCREEN_WIDTH, SCREEN_HEIGHT);
	sfRenderWindow_clear(window.window, sfBlack);
	return (window);
}

void 		kill_window(t_window window)
{
	sfRenderWindow_destroy(window.window);
}
