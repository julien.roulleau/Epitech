/*
** draw_square.c for corewar in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Mar 28 23:08:15 2017 Roulleau Julien
** Last update Thu Mar 30 14:33:12 2017 Roulleau Julien
*/

#include "framebuffer.h"

void 			draw_square(t_fb *fb, sfVector2i p,
				sfVector2i s, sfColor color)
{
	sfVector2i	pos;
	sfVector2i	size;

	size.x = p.x + s.x;
	size.y = p.y + s.y;
	pos.x = p.x;
	while (++pos.x < size.x && (pos.y = p.y))
		while (++pos.y < size.y)
			my_put_pixel(fb, pos.x, pos.y, color);
}
