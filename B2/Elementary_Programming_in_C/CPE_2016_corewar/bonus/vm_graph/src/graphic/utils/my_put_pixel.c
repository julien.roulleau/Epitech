/*
** pixel.c for pixel in /home/infocraft/Job/bswireframe/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Nov 18 20:08:40 2016 Roulleau Julien
** Last update Tue Mar 28 23:24:23 2017 Roulleau Julien
*/

#include "framebuffer.h"

void 		my_put_pixel(t_fb *fb, int x, int y, sfColor color)
{
	if (x < 0 || y < 0)
		return;
	if (x >= fb->width || y >= fb->height)
		return;
	fb->pixels[(fb->width * y + x) * 4] = color.r;
	fb->pixels[(fb->width * y + x) * 4 + 1] = color.g;
	fb->pixels[(fb->width * y + x) * 4 + 2] = color.b;
	fb->pixels[(fb->width * y + x) * 4 + 3] = color.a;
}
