/*
** sf.c for raycast in /home/na/Dropbox/Job/En_Cours/raytracer1/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Mar 16 20:19:43 2017 Roulleau Julien
** Last update Thu Mar 16 20:20:48 2017 Roulleau Julien
*/

# include <SFML/Graphics.h>

sfVector2i		pos2d(int x, int y)
{
	sfVector2i	vect;

	vect.x = x;
	vect.y = y;
	return (vect);
}

sfVector3f		pos3d(int x, int y, int z)
{
	sfVector3f	vect;

	vect.x = x;
	vect.y = y;
	vect.z = z;
	return (vect);
}

sfColor			color_put(int r, int g, int b, int a)
{
	sfColor 	color;

	color.r = r;
	color.g = g;
	color.b = b;
	color.a = a;
	return (color);
}
