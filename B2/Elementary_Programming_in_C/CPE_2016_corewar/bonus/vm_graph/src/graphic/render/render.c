/*
** render.c for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Mar 16 14:04:02 2017 Roulleau Julien
** Last update Tue Mar 28 23:04:44 2017 Roulleau Julien
*/

#include "framebuffer.h"

static int 	event(sfRenderWindow *window)
{
	sfEvent	event;

	while (sfRenderWindow_pollEvent(window, &event))
			if (event.type == sfEvtClosed)
				sfRenderWindow_close(window);
	if (sfKeyboard_isKeyPressed(sfKeyEscape) == sfTrue)
		sfRenderWindow_close(window);
	return (0);
}

void		render(t_window window, char *map)
{
	event(window.window);
	display(window, map);
	sfTexture_updateFromPixels(window.texture, window.fb->pixels,
			SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0);
	sfRenderWindow_drawSprite(window.window, window.sprite, NULL);
	sfRenderWindow_display(window.window);
}
