/*
** display.c for corewar in /home/na/Dropbox/Job/En_Cours/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Mar 28 23:02:53 2017 Roulleau Julien
** Last update Thu Mar 30 23:09:23 2017 Roulleau Julien
*/

#include "framebuffer.h"
#include "color.h"
#include "op.h"

#define S 10
#define DIV 55

sfColor		color_champ(char pos)
{
	if (pos == 0)
		return (C_RED);
	else if (pos == 1)
		return (C_BLUE);
	else if (pos == 2)
		return (C_GREEN);
	else if (pos == 3)
		return (C_ORANGE);
	else
		return (C_GREY);
}

static int	find_div()
{
	int	nb;

	nb = SCREEN_HEIGHT / S;
	while (MEM_SIZE % nb != 0 && (MEM_SIZE / nb) < SCREEN_WIDTH / S - 2)
		nb -= 1;
	return (nb);
}

static void 		display_board(t_window window, char *map)
{
	sfVector2i	pos;
	sfVector2i	size;
	int		nb;
	int		i;
	(void) map;
	(void) i;
	(void) nb;

	nb = find_div();
	size.x = MEM_SIZE / nb + 1;
	size.y = nb + 1;
	draw_square(window.fb, pos2d(S - 1, S - 1),
			pos2d(size.x * S - S + 2, size.y * S + 2), C_WHITE);
	pos.y = 0;
	i = -1;
	while (++pos.y < size.y && !(pos.x = 0))
		while (++pos.x < size.x)
			draw_square(window.fb, pos2d(pos.x * S, pos.y * S),
				pos2d(S, S), color_champ(map[++i]));
	pos.x = 0;
	while (++i < MEM_SIZE)
		draw_square(window.fb, pos2d(++pos.x * S, pos.y * S),
			pos2d(S, S), color_champ(map[i]));
}

void 			display(t_window window, char *map)
{
	display_board(window, map);
}
