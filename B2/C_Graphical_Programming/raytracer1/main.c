/*
** main.c for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Feb 24 10:06:43 2017 Roulleau Julien
** Last update Fri Mar 17 12:00:34 2017 Roulleau Julien
*/

#include "raytracer.h"
#include "parser.h"

int 				main(int argc, char **argv)
{
	t_window		window;
	t_obj			**obj;
	t_scene			scene;
	t_my_framebuffer	*framebuffer;

	if (argc < 2)
		return (84);
	init_windows(&window.window, &window.sprite,
			&window.texture, &framebuffer);
	sfRenderWindow_clear(window.window, sfBlack);
	parser(argv[1], &obj, &scene);
	render(framebuffer, window, obj, scene);
	sfRenderWindow_destroy(window.window);
	return (0);
}
