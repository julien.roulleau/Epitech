/*
** parser.c for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Mar 16 22:23:20 2017 Roulleau Julien
** Last update Fri Mar 17 11:31:54 2017 Roulleau Julien
*/

#include <stdlib.h>
#include <fcntl.h>
#include "struct.h"
#include "src.h"
#include "framebuffer.h"

/*
** fist line
** 	eye.x:eye.y:eye.z:light.x:light.y:light.z:nb_obj
*/
static t_scene 	init_scene(char *param)
{
	t_scene	scene;
	char	**tab;

	tab = str_to_wordtab(param, ':');
	scene.size.y = SCREEN_HEIGHT;
	scene.size.x = SCREEN_WIDTH;
	scene.eye.x = my_getnbr(tab[0]);
	scene.eye.y = my_getnbr(tab[1]);
	scene.eye.z = my_getnbr(tab[2]);
	scene.light.x = my_getnbr(tab[3]);
	scene.light.y = my_getnbr(tab[4]);
	scene.light.z = my_getnbr(tab[5]);
	scene.nb_obj = my_getnbr(tab[6]);
	return (scene);
}
#include <stdio.h>
/*
** obj line
** 	type:pos.x:pos.y:pos.z:color.r:color.g:color.b:r.x:r.y:r.z:param
*/
static t_obj	*init_obj(char *param)
{
	t_obj	*obj;
	char	**tab;

	obj = malloc(sizeof(t_obj));
	tab = str_to_wordtab(param, ':');
	obj->type = my_getnbr(tab[0]);
	obj->pos.x = my_getnbr(tab[1]);
	obj->pos.y = my_getnbr(tab[2]);
	obj->pos.z = my_getnbr(tab[3]);
	obj->color.r = my_getnbr(tab[4]);
	obj->color.g = my_getnbr(tab[5]);
	obj->color.b = my_getnbr(tab[6]);
	obj->r.x = my_getnbr(tab[7]);
	obj->r.y = my_getnbr(tab[8]);
	obj->r.z = my_getnbr(tab[9]);
	obj->param = my_getnbr(tab[10]);
	return (obj);
}

void		parser(char *file, t_obj ***obj, t_scene *scene)
{
	char	*buffer;
	int 	fd;
	int	i;

	if (!(buffer = malloc(sizeof(char) * (5))))
		exit (84);
	if ((fd = open(file, O_RDONLY)) == -1)
			exit (84);
	buffer = get_next_line(fd);
	*scene = init_scene(buffer);
	*obj = malloc(sizeof(t_obj) * ((*scene).nb_obj));
	i = -1;
	while (++i < (*scene).nb_obj)
	{
		buffer = get_next_line(fd);
		(*obj)[i] = init_obj(buffer);
	}
}
