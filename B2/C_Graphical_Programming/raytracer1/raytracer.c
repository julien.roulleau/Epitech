/*
** raytracer.c for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Mar 15 22:10:00 2017 Roulleau Julien
** Last update Fri Mar 17 11:27:43 2017 Roulleau Julien
*/
#include "trigo.h"
#include "struct.h"
#include "define.h"
#include "framebuffer.h"

static void 	put_obj(t_my_framebuffer *framebuffer,
			sfVector2i pos, sfColor color, float coef)
{
	my_put_pixel(framebuffer, pos.x, pos.y,
		color_put(color.r * coef, color.g * coef, color.b * coef, 255));
}

float		set_dist(t_scene scene, t_obj *obj)
{
	if (obj->type == SPHERE)
		return (intersect_sphere(itranslate(
			scene.eye, obj->pos), rotate_zyx(scene.vect,
			pos3d(obj->r.x, obj->r.y, obj->r.z)), obj->param));
	if (obj->type == PLANE)
		return (intersect_plane(itranslate(
			scene.eye, obj->pos), rotate_zyx(scene.vect,
			pos3d(obj->r.x, obj->r.y, obj->r.z))));
	if (obj->type == CYLINDER)
		return (intersect_cylinder(itranslate(
			scene.eye, obj->pos), rotate_zyx(scene.vect,
			pos3d(obj->r.x, obj->r.y, obj->r.z)), obj->param));
	if (obj->type == CONE)
		return (intersect_cone(itranslate(
			scene.eye, obj->pos), rotate_zyx(scene.vect,
			pos3d(obj->r.x, obj->r.y, obj->r.z)), obj->param));
	else
		return (0);
}

#include <stdio.h>
int 		select_obj(t_obj **obj, int nb_obj)
{
	float 	last;
	int	i;
	int 	select;

	i = -1;
	while (++i < nb_obj && obj[i]->dist <= 0);
	if (i == nb_obj)
		return (-1);
	select = i;
	last = obj[select]->dist;
	while (++i < nb_obj)
	{
		if (obj[i]->dist > 0 && obj[i]->dist < last)
		{
			select = i;
			last = obj[select]->dist;
		}
	}
	return (select);
}

int		raytracer(t_my_framebuffer *framebuffer,
				t_scene scene, t_obj **obj)
{
	int	select;
	int	i;

	scene.pos.y = -1;
	while (++scene.pos.y < SCREEN_HEIGHT && (scene.pos.x = -1))
		while (++scene.pos.x < SCREEN_WIDTH && (i = -1))
		{
			scene.vect = calc_dir_vector(
				DTP, scene.size, scene.pos);
			while (++i < scene.nb_obj)
				obj[i]->dist = set_dist(scene, obj[i]);
			if ((select = select_obj(obj, scene.nb_obj)) == -1)
				put_obj(framebuffer, scene.pos,
					COLOR_BACK, 1);
			else
				put_obj(framebuffer, scene.pos,
				obj[select]->color, light(scene, obj[select]));
		}
	return (0);
}
