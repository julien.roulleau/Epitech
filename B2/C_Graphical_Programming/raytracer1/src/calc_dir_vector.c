/*
** calc_dir_vector.c for raytracer in /raytracer1/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Feb 10 10:42:43 2017 Roulleau Julien
** Last update Thu Mar 16 19:48:12 2017 Roulleau Julien
*/

#include "utils.h"

sfVector3f		calc_dir_vector(float dist_to_plane,
					sfVector2i screen_size,
					sfVector2i screen_pos)
{
	sfVector3f	vect;

	vect.x = dist_to_plane;
	vect.y = ((float) screen_size.x / 2.0) - (float) screen_pos.x;
	vect.z = ((float) screen_size.y / 2.0) - (float) screen_pos.y;
	return (vect);
}
