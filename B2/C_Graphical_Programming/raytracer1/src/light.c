/*
** light.c for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Mar 14 17:11:01 2017 Roulleau Julien
** Last update Fri Mar 17 14:05:51 2017 Roulleau Julien
*/

#include "utils.h"
#include "struct.h"
#include "trigo.h"
#include "define.h"

sfVector3f	cal_light_vector(sfVector3f light,
					sfVector3f d_vect, float dist)
{
	sfVector3f vect;

	vect.x = light.x + d_vect.x * dist;
	vect.y = light.y + d_vect.y * dist;
	vect.z = light.z + d_vect.z * dist;
	return (vect);
}

sfVector3f	impact(sfVector3f eye, sfVector3f ray, float dist)
{
	sfVector3f vect;

	vect.x = eye.x + ray.x * dist;
	vect.y = eye.y + ray.y * dist;
	vect.z = eye.z + ray.z * dist;
	return (vect);
}

sfVector3f	normalise(sfVector3f vect)
{
	float norm;

	norm = sqrt(POW(vect.x) + POW(vect.y) + POW(vect.z));
	vect.x /= norm;
	vect.y /= norm;
	vect.z /= norm;
	return (vect);
}

float		get_light_coef(sfVector3f light, sfVector3f normal)
{
	float	coef;

	light = normalise(light);
	normal = normalise(normal);
	coef = light.x * normal.x + light.y * normal.y + light.z * normal.z;
	return (coef);
}

float		light(t_scene scene, t_obj *obj)
{
	if (obj->type == SPHERE)
		return (get_light_coef(cal_light_vector(
			scene.light, impact(scene.eye, scene.vect, obj->dist),
			obj->dist), get_normal_sphere(scene.vect)));
	else if (obj->type == PLANE)
		return (get_light_coef(cal_light_vector(
			scene.light, impact(scene.eye, scene.vect, obj->dist),
			obj->dist), get_normal_plane(obj->param)));
	else if (obj->type == CYLINDER)
		return (get_light_coef(cal_light_vector(
			scene.light, impact(scene.eye, scene.vect, obj->dist),
			obj->dist), get_normal_cylinder(scene.vect)));
	else if (obj->type == CONE)
		return (get_light_coef(cal_light_vector(
			scene.light, impact(scene.eye, scene.vect, obj->dist),
			obj->dist), get_normal_cone(scene.vect, obj->param)));
	else
		return (1);
}
