/*
** translate.c for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Mar 14 16:46:31 2017 Roulleau Julien
** Last update Thu Mar 16 19:01:38 2017 Roulleau Julien
*/

#include "utils.h"

sfVector3f		translate(sfVector3f to_translate,
					sfVector3f translations)
{
	to_translate.x += translations.x;
	to_translate.y += translations.y;
	to_translate.z += translations.z;
	return (to_translate);
}

sfVector3f		itranslate(sfVector3f to_translate,
					sfVector3f translations)
{
	to_translate.x -= translations.x;
	to_translate.y -= translations.y;
	to_translate.z -= translations.z;
	return (to_translate);
}
