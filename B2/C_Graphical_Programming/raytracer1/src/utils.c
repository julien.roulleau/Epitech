/*
** utils.c for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Mar 15 15:40:29 2017 Roulleau Julien
** Last update Fri Mar 17 11:57:41 2017 Roulleau Julien
*/

#include "utils.h"

float		cal_delta(sfVector3f abc)
{
	float	delta;
	float	k;
	float	k1;
	float	k2;

	delta = POW(abc.y) - (4 * abc.x * abc.z);
	k = -abc.y / (2 * abc.x);
	if (delta < 0)
		return (0);
	else if (delta == 0)
		return (k < 0 ? -1 : k);
	k1 = (-abc.y + sqrt(delta)) / (2 * abc.x);
	k2 = (-abc.y - sqrt(delta)) / (2 * abc.x);
	if (k1 > 0 && k2 < 0)
		return (k1);
	else if (k1 < 0 && k2 > 0)
		return (k2);
	else if (k1 < 0 && k2 < 0)
		return (-1);
	else
		return (k1 < k2 ? k1 : k2);
}
