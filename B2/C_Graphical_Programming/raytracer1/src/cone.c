/*
** cone.c for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Mar 14 16:57:28 2017 Roulleau Julien
** Last update Fri Mar 17 21:08:45 2017 Roulleau Julien
*/

#include "utils.h"

float			intersect_cone(sfVector3f eye_pos,
					sfVector3f dir_vect,
					float semiangle)
{
	sfVector3f	abc;

	(void) semiangle;
	abc.x = POW(dir_vect.x) +
		POW(dir_vect.y) -
		POW(dir_vect.z) * POW(TAN(semiangle));
	abc.y = 2 *
		(eye_pos.x * dir_vect.x +
		eye_pos.y * dir_vect.y +
		eye_pos.z * dir_vect.z);
	abc.z = POW(eye_pos.x) +
		POW(eye_pos.y) +
		POW(eye_pos.z) -
		POW(semiangle);
	return (cal_delta(abc));
}

sfVector3f	get_normal_cone(sfVector3f pt,
				float semiangle)
{
	float	normal;

	(void) semiangle;
	normal = sqrt(POW(pt.x) + POW(pt.y) + POW(pt.z));
	pt.x /= normal;
	pt.y /= normal;
	pt.z = 0;
	return (pt);
}
