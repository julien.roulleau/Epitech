/*
** rotate.c for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Mar 14 16:54:26 2017 Roulleau Julien
** Last update Fri Mar 17 11:45:42 2017 Roulleau Julien
*/

#include "utils.h"

sfVector3f		rotate_xyz(sfVector3f r, sfVector3f angle)
{
	sfVector3f	rn;

	rn.x = r.x;
	rn.y = COS(angle.x) * r.y + (-SIN(angle.x)) * r.z;
	rn.z = SIN(angle.x) * r.y + COS(angle.x) * r.z;
	r = rn;
	rn.x = COS(angle.y) * r.x + SIN(angle.y) * r.z;
	rn.y = r.y;
	rn.z = (-SIN(angle.y)) * r.x + COS(angle.y) * r.z;
	r = rn;
	rn.x = COS(angle.z) * r.x + (-SIN(angle.z)) * r.y;
	rn.y = SIN(angle.z) * r.x + COS(angle.z) * r.y;
	rn.z = r.z;
	r = rn;
	return (r);
}

sfVector3f		rotate_zyx(sfVector3f r, sfVector3f angle)
{
	sfVector3f	rn;

	rn.x = COS(angle.x) * r.x + (-SIN(angle.x)) * r.y;
	rn.y = SIN(angle.x) * r.x + COS(angle.x) * r.y;
	rn.z = r.z;
	r = rn;
	rn.x = COS(angle.y) * r.x + SIN(angle.y) * r.z;
	rn.y = r.y;
	rn.z = (-SIN(angle.y)) * r.x + COS(angle.y) * r.z;
	r = rn;
	rn.x = r.x;
	rn.y = COS(angle.z) * r.y + (-SIN(angle.z)) * r.z;
	rn.z = SIN(angle.z) * r.y + COS(angle.z) * r.z;
	r = rn;
	return (r);
}
