/*
** plane.c for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Mar 14 16:57:28 2017 Roulleau Julien
** Last update Fri Mar 17 10:55:33 2017 Roulleau Julien
*/

#include "utils.h"

float		intersect_plane(sfVector3f eye_pos,
					sfVector3f dir_vect)
{
	return (dir_vect.z != 0 ? -eye_pos.z / dir_vect.z : 0);
}

sfVector3f		get_normal_plane(int upward)
{
	sfVector3f	normal;

	(void) upward;
	if (upward)
		normal.x = 1;
	else
		normal.x = -1;
	normal.z = 0;
	normal.y = 0;
	return (normal);
}
