/*
** sphere.c for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Mar 14 16:57:28 2017 Roulleau Julien
** Last update Thu Mar 16 17:58:10 2017 Roulleau Julien
*/

#include "utils.h"

float			intersect_sphere(sfVector3f eye_pos,
					sfVector3f dir_vect,
					float raduis)
{
	sfVector3f	abc;

	abc.x = POW(dir_vect.x) +
		POW(dir_vect.y) +
		POW(dir_vect.z);
	abc.y = 2 *
		(eye_pos.x * dir_vect.x +
		eye_pos.y * dir_vect.y +
		eye_pos.z * dir_vect.z);
	abc.z = POW(eye_pos.x) +
		POW(eye_pos.y) +
		POW(eye_pos.z) -
		POW(raduis);
	return (cal_delta(abc));
}

sfVector3f	get_normal_sphere(sfVector3f pt)
{
	float	normal;

	normal = sqrt(POW(pt.x) + POW(pt.y) + POW(pt.z));
	pt.x /= normal;
	pt.y /= normal;
	pt.z /= normal;
	return (pt);
}
