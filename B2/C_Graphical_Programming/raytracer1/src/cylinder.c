/*
** cylinder.c for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Tue Mar 14 16:57:28 2017 Roulleau Julien
** Last update Thu Mar 16 19:17:32 2017 Roulleau Julien
*/

#include "utils.h"

float			intersect_cylinder(sfVector3f eye_pos,
					sfVector3f dir_vect,
					float radius)
{
	sfVector3f	abc;

	abc.x = POW(dir_vect.x) +
		POW(dir_vect.y);
	abc.y = 2 *
		(eye_pos.x * dir_vect.x +
		eye_pos.y * dir_vect.y);
	abc.z = POW(eye_pos.x) +
		POW(eye_pos.y) -
		POW(radius);
	return (cal_delta(abc));
}

sfVector3f	get_normal_cylinder(sfVector3f pt)
{
	float	normal;

	normal = sqrt(POW(pt.x) + POW(pt.y) + POW(pt.z));
	pt.x /= normal;
	pt.y /= normal;
	pt.z = 0;
	return (pt);
}
