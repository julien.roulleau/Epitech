/*
** window.c for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Mar 15 22:03:13 2017 Roulleau Julien
** Last update Wed Mar 15 22:06:41 2017 Roulleau Julien
*/

#include <stdlib.h>
#include "framebuffer.h"

t_my_framebuffer* 		my_framebuffer_create(int width, int height)
{
	t_my_framebuffer	*framebuffer;
	int			 p;

	p = -1;
	framebuffer = malloc(sizeof(t_my_framebuffer));
	framebuffer->pixels = malloc(sizeof(char) * 4 * width * height);
	framebuffer->width = width;
	framebuffer->height = height;
	while (++p < width * height * 4)
			framebuffer->pixels[p] = 0;
	return (framebuffer);
}

void			init_windows(sfRenderWindow **window,
					sfSprite **sprite,
					sfTexture **texture,
					t_my_framebuffer **framebuffer)
{
	sfVideoMode	mode;

	mode.width = SCREEN_WIDTH;
	mode.height = SCREEN_HEIGHT;
	mode.bitsPerPixel = BITS_PER_PIXEL;
	*window = sfRenderWindow_create (mode, "SFMLwindow",
			sfResize | sfClose, NULL);
	*sprite = sfSprite_create();
	*texture = sfTexture_create(SCREEN_WIDTH, SCREEN_HEIGHT);
	sfSprite_setTexture(*sprite, *texture, sfTrue);
	*framebuffer = my_framebuffer_create(SCREEN_WIDTH, SCREEN_HEIGHT);
}
