/*
** raytracer.h for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Mar 15 22:13:15 2017 Roulleau Julien
** Last update Thu Mar 16 22:48:59 2017 Roulleau Julien
*/

#ifndef RAYTRACER_H
# define RAYTRACER_H

# include "framebuffer.h"
# include "struct.h"

int	raytracer(t_my_framebuffer *, t_scene, t_obj **);
void	render(t_my_framebuffer *, t_window , t_obj **, t_scene);

#endif /* RAYTRACER_H */
