/*
** define.h for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Mar 16 22:32:55 2017 Roulleau Julien
** Last update Fri Mar 17 00:02:02 2017 Roulleau Julien
*/

#ifndef DEFINE_H
#define	DEFINE_H

# define SPHERE 1
# define PLANE 2
# define CYLINDER 3
# define CONE 4

# define COLOR_BACK color_put(0, 0, 0, 255)

# define DTP 500

# define SPEED 5
#endif /* DEFINE_H */
