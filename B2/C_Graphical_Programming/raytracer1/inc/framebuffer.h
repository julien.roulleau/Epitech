/*
** framebuffer.h for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Mar 15 21:59:21 2017 Roulleau Julien
** Last update Thu Mar 16 20:21:13 2017 Roulleau Julien
*/

#ifndef FRAMEBUFFER_H
# define FRAMEBUFFER_H

# include <SFML/Graphics.h>

# define SCREEN_WIDTH 1200
# define SCREEN_HEIGHT 600
# define BITS_PER_PIXEL 32

typedef struct		s_my_framebuffer
{
	sfUint8		*pixels;
	int		width;
	int		height;
}			t_my_framebuffer;

void		init_windows(sfRenderWindow **, sfSprite **,
			sfTexture **, t_my_framebuffer **);

void 		my_put_pixel(t_my_framebuffer *, int, int, sfColor);
sfVector2i	pos2d(int, int);
sfVector3f	pos3d(int, int, int);
sfColor		color_put(int, int, int, int);

#endif /* FRAMEBUFFER_H */
