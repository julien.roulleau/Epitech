/*
** utils.h for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Mar 15 21:43:21 2017 Roulleau Julien
** Last update Fri Mar 17 20:57:55 2017 Roulleau Julien
*/

#ifndef UTILS_H
# define UTILS_H

# include <SFML/Graphics.h>
# include <math.h>

# define POW(x) (x * x)
# define COS(angle) cos(angle * M_PI / 180.0)
# define SIN(angle) sin(angle * M_PI / 180.0)
# define TAN(angle) tan(angle * M_PI / 180.0)

float	cal_delta(sfVector3f);

#endif /* UTILS_H */
