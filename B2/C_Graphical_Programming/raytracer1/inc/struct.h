/*
** struct.h for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Mar 16 17:25:15 2017 Roulleau Julien
** Last update Fri Mar 17 11:27:17 2017 Roulleau Julien
*/

#ifndef STRUCT_H
# define STRUCT_H

# include <SFML/Graphics.h>

typedef struct		s_obj
{
	sfVector3f	pos;
	sfVector3f	r;
	sfColor		color;
	float		dist;
	int		type;
	int		param;
}			t_obj;

typedef struct		s_scene
{
	sfVector3f	light;
	sfVector3f	vect;
	sfVector2i	size;
	sfVector2i	pos;
	sfVector3f	eye;
	int 		nb_obj;
}			t_scene;

typedef struct		s_window
{
	sfRenderWindow	*window;
	sfSprite	*sprite;
	sfTexture	*texture;
}			t_window;
#endif /* STRUCT_H */
