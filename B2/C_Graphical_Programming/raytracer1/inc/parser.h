/*
** parser.h for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Mar 16 22:42:12 2017 Roulleau Julien
** Last update Thu Mar 16 23:35:57 2017 Roulleau Julien
*/

#ifndef PARSER_H
# define PARSER_H

# include "struct.h"

void		parser(char *, t_obj ***, t_scene *);

#endif /* PARSER_H */
