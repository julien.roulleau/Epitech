/*
** trigo.h for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Wed Mar 15 21:46:45 2017 Roulleau Julien
** Last update Thu Mar 16 22:19:22 2017 Roulleau Julien
*/

#ifndef TRIGO_H
# define TRIGO_H

# include "utils.h"
# include "struct.h"

/* Vector */
sfVector3f	calc_dir_vector(float, sfVector2i, sfVector2i);

/* Translate */
sfVector3f	translate(sfVector3f, sfVector3f);
sfVector3f	itranslate(sfVector3f, sfVector3f);

/* Rotate */
sfVector3f	rotate_xyz(sfVector3f, sfVector3f);
sfVector3f	rotate_zyx(sfVector3f, sfVector3f);

/* Plane */
float		intersect_plane(sfVector3f, sfVector3f);
sfVector3f	get_normal_plane(int);

/* Sphere */
float		intersect_sphere(sfVector3f, sfVector3f, float);
sfVector3f	get_normal_sphere(sfVector3f);

/* Cylinder */
float		intersect_cylinder(sfVector3f, sfVector3f, float);
sfVector3f	get_normal_cylinder(sfVector3f);

/* Cone */
float		intersect_cone(sfVector3f, sfVector3f, float);
sfVector3f	get_normal_cone(sfVector3f, float);

/* Light*/
float		get_light_coef(sfVector3f, sfVector3f);
float		light(t_scene, t_obj *);

#endif /* TRIGO_H */
