/*
** render.c for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Mar 16 14:04:02 2017 Roulleau Julien
** Last update Fri Mar 17 11:37:35 2017 Roulleau Julien
*/
#include "raytracer.h"
#include "struct.h"
#include "define.h"

static int 	event(sfRenderWindow *window, t_scene *scene)
{
	sfEvent	event;

	while (sfRenderWindow_pollEvent(window, &event))
			if (event.type == sfEvtClosed)
				sfRenderWindow_close(window);
	if (sfKeyboard_isKeyPressed(sfKeyEscape) == sfTrue)
		sfRenderWindow_close(window);
	if (sfKeyboard_isKeyPressed(sfKeyZ) == sfTrue)
		scene->eye.z += SPEED;
	if (sfKeyboard_isKeyPressed(sfKeyS) == sfTrue)
		scene->eye.z -= SPEED;
	if (sfKeyboard_isKeyPressed(sfKeyQ) == sfTrue)
		scene->eye.y += SPEED;
	if (sfKeyboard_isKeyPressed(sfKeyD) == sfTrue)
		scene->eye.y -= SPEED;
	if (sfKeyboard_isKeyPressed(sfKeyA) == sfTrue)
		scene->eye.x += SPEED;
	if (sfKeyboard_isKeyPressed(sfKeyE) == sfTrue)
		scene->eye.x -= SPEED;
	if (sfKeyboard_isKeyPressed(sfKeySpace) == sfTrue)
		return (1);
	return (0);
}

void		render(t_my_framebuffer *framebuffer,
			t_window window, t_obj **obj, t_scene scene)
{
	while (sfRenderWindow_isOpen(window.window))
	{
		if (event(window.window, &scene))
			raytracer(framebuffer, scene, obj);
		sfTexture_updateFromPixels(window.texture, framebuffer->pixels,
				SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0);
		sfRenderWindow_drawSprite(window.window, window.sprite, NULL);
		sfRenderWindow_display(window.window);
	}
}
