/*
** parallax.h for scroller in /home/onehandedpenguin/Dev/CGP_2016/scroller/inc/
**
** Made by Paul Laffitte
** Login   <paul.laffitte@epitech.eu>
**
** Started on  Fri Mar 31 23:23:49 2017 Paul Laffitte
** Last update Sat Apr  1 17:33:57 2017 Paul Laffitte
*/

#ifndef PARALLAX_H_
# define PARALLAX_H_

# include <SFML/Graphics.h>

typedef struct	s_parallax
{
  sfSprite	*sprite;
  float		speed;
  int		width;
}		t_parallax;

#endif /* !PARALLAX_H_ */
