/*
** obj_3d.h for scroller in /home/onehandedpenguin/Dev/CGP_2016/scroller/inc/effects/
**
** Made by Paul Laffitte
** Login   <paul.laffitte@epitech.eu>
**
** Started on  Sat Apr  1 19:58:09 2017 Paul Laffitte
** Last update Sun Apr  2 20:39:30 2017 Paul Laffitte
*/

#ifndef OBJ_3D_H_
# define OBJ_3D_H_

# include <SFML/Graphics.h>

typedef struct	s_triangle
{
  float		dist;
  sfVector3f	vertexes[3];
}		t_triangle;

typedef struct	s_obj3d
{
  sfVector3f	**vertexes;
  t_triangle	*triangles;
  sfVertexArray	*array;
  sfVector3f	position;
  sfVector3f	rotation;
  sfVector3f	rotation_offset;
  float		angle;
  float		far;
  float		near;
  int		nb_vertexes;
  int		nb_triangles;
}		t_obj3d;

sfVector3f	**set_obj(char *file);

sfColor		get_color(t_obj3d *obj, float distance);
void		set_near_far(t_obj3d *obj);

void		sort_triangles(t_obj3d *obj);
void		set_distances(t_obj3d *obj);

sfVector2f	projection(sfVector3f *pos3d, float angle);
void		rotation3d(sfVector3f *vertex, sfVector3f *rotation);

#endif /* !OBJ_3D_H_ */
