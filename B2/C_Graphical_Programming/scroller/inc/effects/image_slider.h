/*
** image_slider.h for image_slayer in /home/loxo/Bureau/EPITECH/RUSH/Scroller/scroller/inc/effects/
**
** Made by Loick Mury
** Login   <loick.mury@epitech.eu@epitech.eu>
**
** Started on  Sat Apr  1 19:53:38 2017 Loick Mury
** Last update Sat Apr  1 19:57:40 2017 Loick Mury
*/

#ifndef IMAGE_SLIDER_H_
# define IMAGE_SLIDER_H_

# include <SFML/Graphics.h>

typedef struct	s_slider
{
  int		x;
  int		y;
  int		k;
  sfImage	*image;
  sfVector2u	size_image;
}           	t_slider;

#endif /* !IMAGE_SLIDER_H_ */
