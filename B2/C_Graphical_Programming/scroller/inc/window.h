/*
** window.h for scroller in /home/onehandedpenguin/Dev/CGP_2016/scroller/inc/
**
** Made by Paul Laffitte
** Login   <paul.laffitte@epitech.eu>
**
** Started on  Sat Apr  1 12:06:02 2017 Paul Laffitte
** Last update Sun Apr  2 20:08:57 2017 Roulleau Julien
*/

#ifndef WINDOW_H_
# define WINDOW_H_

# include "framebuffer.h"

typedef struct		s_window
{
	sfRenderWindow	*window;
	sfSprite	*sprite;
	sfTexture	*texture;
	sfSprite	*s_wolf;
	sfTexture	*t_wolf;
	sfIntRect	i_wolf;
	t_fb		*fb;
}			t_window;

t_window	*init_window(t_window *window);
void		render(t_window *window);

#endif /* !WINDOW_H_ */
