/*
** effect.h for scroller in /home/na/Dropbox/Job/En_Cours/scroller/inc/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Mar 31 21:54:39 2017 Roulleau Julien
** Last update Sun Apr  2 21:13:22 2017 Roulleau Julien
*/

#ifndef SCROLLER_H
# define SCROLLER_H

#include <SFML/Audio.h>
# include "list.h"
# include "window.h"
# include "effects/parallax.h"
# include "effects/3d.h"
# include "effects/image_slider.h"

# define LOAD_TEX(filename) (sfTexture_createFromFile(filename, NULL))
# define LD_MUSIC(path) (sfMusic_createFromFile(path))

typedef struct		s_sound
{
	sfMusic		*atari;
	sfMusic		*axelf;
	sfMusic		*beep;
	sfMusic		*korg;
	sfMusic		*laser;
	sfMusic		*lol;
	sfMusic		*prophet;
	sfMusic		*sonic;
}			t_sound;

typedef struct		s_music
{
	sfMusic		*gameover;
	sfMusic		*intro;
	sfMusic		*lemmings;
	sfMusic		*sotb;
	sfMusic		*ymvst;
	sfMusic		*Zac;
}			t_music;

typedef struct		s_res
{
	sfTexture	*title;
        sfTexture	*mountains;
        sfTexture	*clouds1;
        sfTexture	*clouds2;
        sfTexture	*clouds3;
	sfTexture	*clouds4;
	sfTexture	*grass2;
	sfTexture	*wolf;
	sfMusic		*music[6];
	int		current_music;
	t_sound		sound;

}			t_res;

typedef struct		s_effect
{
	sfText		*text;
	sfImage		*image;
	t_list		*parallaxes;
	t_list		*objs3d;
}			t_effect;

/* Initialization */
int	init_effects(t_effect *effects, t_res *res);
int	add_effects(t_effect *effects, t_res *res);
int	add_parallax(t_list *parallaxes, sfTexture *texture,
		     float speed, int position);
int	add_obj3d(t_list *objs, sfVector3f position,
		  sfVector3f rotation_offset, char *filename);

/* Scrolling Text */
void	scrolling_text(sfText *, sfRenderWindow *, sfVector2f, int);
sfText	*deftext(char *sentences);

/* Image Slider */
void	sliderstruct(t_fb*, sfImage*, int, int);

/* Sprite animation */
void	sprite_animation(t_window *window,
	char *pathname, sfVector2f position);

/* Parallax */
void	parallax(t_window *window, t_parallax *parallax);

/* Parallax */
void	obj3d(t_window *window, t_obj3d *obj);

/* Tracker */
int	tracker(sfMusic *track, int set);
int	check_music(t_res *res);

void 	kill_window(t_window *window, t_res res);

#endif /* SCROLLER_H */
