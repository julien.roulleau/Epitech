/*
** timer.h for scroller in /home/onehandedpenguin/Dev/CGP_2016/scroller/inc/
**
** Made by Paul Laffitte
** Login   <paul.laffitte@epitech.eu>
**
** Started on  Sat Apr  1 16:43:02 2017 Paul Laffitte
** Last update Sat Apr  1 16:44:21 2017 Paul Laffitte
*/

#ifndef TIMER_H_
# define TIMER_H_

#include <SFML/System.h>

typedef struct	s_timer
{
  sfClock	*timer;
  sfInt64	last_frame;
  float		delta;
}		t_timer;

extern t_timer	timer;

int		init_timer();
void		update_clock();

#endif /* !TIMER_H_ */
