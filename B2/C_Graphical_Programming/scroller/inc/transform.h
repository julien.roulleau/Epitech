/*
** transform.h for raytracer1 in /home/onehandedpenguin/Dev/CGP_2016/raytracer1/include/
**
** Made by Paul Laffitte
** Login   <paul.laffitte@epitech.eu>
**
** Started on  Wed Feb  8 14:42:11 2017 Paul Laffitte
** Last update Tue Mar 14 18:26:52 2017 Paul Laffitte
*/

#ifndef transform_H_
# define transform_H_

# include "SFML/System.h"

typedef struct	s_transform
{
  float		matrix[4][4];
}		t_transform;

void	init_transform(t_transform *transform);
void	copy_transform(t_transform *src, t_transform *dest);
float	dot_product(float *vector1, float *vector2);
void	combine_transforms(t_transform *transform1, t_transform *transform2);
void	get_transform_line(t_transform *transform, int line, float *buffer);

void	get_translation(sfVector3f *translation, t_transform *buffer);
void	translate(t_transform *transform, sfVector3f *translation);

void	get_rotation(sfVector3f *rotation, t_transform *buffer);
void	rotate(t_transform *transform, sfVector3f *rotation);

void	get_homothety(sfVector3f *homothety, t_transform *buffer);
void	scale(t_transform *transform, sfVector3f *homothety);

void	apply_transform(sfVector3f *vector, t_transform *transform, int w);

void	reverse(t_transform *transform);

#endif /* !transform_H_ */
