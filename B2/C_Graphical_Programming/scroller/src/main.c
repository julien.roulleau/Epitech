/*
** main.c for scroller in /home/na/Dropbox/Job/En_Cours/scroller/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Fri Mar 31 21:02:44 2017 Roulleau Julien
** Last update Sun Apr  2 16:19:46 2017 Roulleau Julien
*/

#include "window.h"

int			main()
{
	t_window	window;

	init_window(&window);
	render(&window);
	return (0);
}
