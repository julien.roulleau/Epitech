/*
** rotation.c for raytracer1 in /home/onehandedpenguin/Dev/CGP_2016/raytracer1/src/math/
**
** Made by Paul Laffitte
** Login   <paul.laffitte@epitech.eu>
**
** Started on  Wed Feb  8 18:30:19 2017 Paul Laffitte
** Last update Sun Apr  2 16:27:12 2017 Paul Laffitte
*/

#include <math.h>
#include "transform.h"
#include "src.h"

void		get_rotation_x(float rotation, t_transform *buffer)
{
  init_transform(buffer);
  rotation = RAD(rotation);
  buffer->matrix[1][1] = cos(rotation);
  buffer->matrix[2][1] = -sin(rotation);
  buffer->matrix[1][2] = sin(rotation);
  buffer->matrix[2][2] = cos(rotation);
}

void		get_rotation_y(float rotation, t_transform *buffer)
{
  init_transform(buffer);
  rotation = RAD(rotation);
  buffer->matrix[0][0] = cos(rotation);
  buffer->matrix[2][0] = sin(rotation);
  buffer->matrix[0][2] = -sin(rotation);
  buffer->matrix[2][2] = cos(rotation);
}

void		get_rotation_z(float rotation, t_transform *buffer)
{
  init_transform(buffer);
  rotation = RAD(rotation);
  buffer->matrix[0][0] = cos(rotation);
  buffer->matrix[1][0] = -sin(rotation);
  buffer->matrix[0][1] = sin(rotation);
  buffer->matrix[1][1] = cos(rotation);
}

void		get_rotation(sfVector3f *rotation, t_transform *buffer)
{
  t_transform	tmp;

  init_transform(buffer);
  get_rotation_x(rotation->x, &tmp);
  combine_transforms(buffer, &tmp);
  get_rotation_y(rotation->y, &tmp);
  combine_transforms(buffer, &tmp);
  get_rotation_z(rotation->z, &tmp);
  combine_transforms(buffer, &tmp);
}

void		rotate(t_transform *transform, sfVector3f *rotation)
{
  t_transform	rotation_matrix;

  get_rotation(rotation, &rotation_matrix);
  combine_transforms(transform, &rotation_matrix);
}
