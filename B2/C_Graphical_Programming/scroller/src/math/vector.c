/*
** vector.c for raytracer1 in /home/onehandedpenguin/Dev/CGP_2016/raytracer1/src_/math/
**
** Made by Paul Laffitte
** Login   <paul.laffitte@epitech.eu>
**
** Started on  Mon Mar 13 13:36:59 2017 Paul Laffitte
** Last update Sun Apr  2 16:29:15 2017 Paul Laffitte
*/

#include <SFML/Graphics.h>
#include "src.h"
#include "transform.h"

void		apply_transform(sfVector3f *vector,
				t_transform *transform, int w)
{
  sfVector3f	tmp;

  tmp = *vector;
  vector->x = tmp.x * transform->matrix[0][0]
            + tmp.y * transform->matrix[0][1]
            + tmp.z * transform->matrix[0][2]
            + w * transform->matrix[0][3];
  vector->y = tmp.x * transform->matrix[1][0]
            + tmp.y * transform->matrix[1][1]
            + tmp.z * transform->matrix[1][2]
            + w * transform->matrix[1][3];
  vector->z = tmp.x * transform->matrix[2][0]
            + tmp.y * transform->matrix[2][1]
            + tmp.z * transform->matrix[2][2]
            + w * transform->matrix[2][3];
}

void		normalize(sfVector3f *vector)
{
  float		norm;

  norm = NORM(*vector);
  vector->x /= norm;
  vector->y /= norm;
  vector->z /= norm;
}

void		rebound(sfVector2f *vector, sfVector2i *max)
{
  float		ratio;

  ratio = max->x / (float)max->y;
  vector->x = vector->x * 2.0 * ratio / max->x - ratio;
  vector->y = -vector->y * 2.0 / max->y + 1;
}

sfVector3f	vector_from_2_points(sfVector3f *from, sfVector3f *to)
{
  return ((sfVector3f){to->x - from->x, to->y - from->y, to->z - from->z});
}
