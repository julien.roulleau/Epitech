/*
** reverse.c for raytracer1 in /home/onehandedpenguin/Dev/CGP_2016/raytracer1/src/math/
**
** Made by Paul Laffitte
** Login   <paul.laffitte@epitech.eu>
**
** Started on  Sat Feb 11 20:37:50 2017 Paul Laffitte
** Last update Mon Mar 13 20:00:58 2017 Paul Laffitte
*/

#include "transform.h"

static void	pivot_line(t_transform *transform, t_transform *unitary,
			   int pivot, int line)
{
  float		tmp;
  int		i;

  tmp = transform->matrix[line][pivot];
  i = 0;
  while (i != 4)
    {
      unitary->matrix[line][i] = tmp * unitary->matrix[pivot][i]
	  - unitary->matrix[pivot][pivot] * unitary->matrix[line][i];
      transform->matrix[line][i] = tmp * transform->matrix[pivot][i]
	  - transform->matrix[pivot][pivot] * transform->matrix[line][i];
      i++;
    }
}

static void	coeff(t_transform *transform, t_transform *unitary, int line)
{
  float		pivot;

  pivot = unitary->matrix[line][line];
  transform->matrix[line][0] /= pivot;
  transform->matrix[line][1] /= pivot;
  transform->matrix[line][2] /= pivot;
  transform->matrix[line][3] /= pivot;
}

void		reverse(t_transform *transform)
{
  t_transform	unitary;

  init_transform(&unitary);
  pivot_line(transform, &unitary, 0, 1);
  pivot_line(transform, &unitary, 0, 2);
  pivot_line(transform, &unitary, 0, 3);
  pivot_line(transform, &unitary, 1, 0);
  pivot_line(transform, &unitary, 1, 2);
  pivot_line(transform, &unitary, 1, 3);
  pivot_line(transform, &unitary, 2, 0);
  pivot_line(transform, &unitary, 2, 1);
  pivot_line(transform, &unitary, 2, 3);
  pivot_line(transform, &unitary, 3, 0);
  pivot_line(transform, &unitary, 3, 1);
  pivot_line(transform, &unitary, 3, 2);
  coeff(&unitary, transform, 0);
  coeff(&unitary, transform, 1);
  coeff(&unitary, transform, 2);
  coeff(&unitary, transform, 3);
  *transform = unitary;
}
