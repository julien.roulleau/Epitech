/*
** translation.c for raytracer1 in /home/onehandedpenguin/Dev/CGP_2016/raytracer1/src/math/
**
** Made by Paul Laffitte
** Login   <paul.laffitte@epitech.eu>
**
** Started on  Wed Feb  8 15:06:33 2017 Paul Laffitte
** Last update Tue Mar 14 00:43:44 2017 Paul Laffitte
*/

#include "transform.h"

void		get_translation(sfVector3f *translation, t_transform *buffer)
{
  init_transform(buffer);
  buffer->matrix[0][3] = translation->x;
  buffer->matrix[1][3] = translation->y;
  buffer->matrix[2][3] = translation->z;
}

void		translate(t_transform *transform, sfVector3f *translation)
{
  t_transform	translation_matrix;

  get_translation(translation, &translation_matrix);
  combine_transforms(transform, &translation_matrix);
}
