/*
** transform.c for raytracer1 in /home/onehandedpenguin/Dev/CGP_2016/raytracer1/src/math/
**
** Made by Paul Laffitte
** Login   <paul.laffitte@epitech.eu>
**
** Started on  Wed Feb  8 14:41:33 2017 Paul Laffitte
** Last update Tue Mar 14 00:47:12 2017 Paul Laffitte
*/

#include "transform.h"

void		init_transform(t_transform *transform)
{
  int		x;
  int		y;

  y = 0;
  while (y < 4)
    {
      x = 0;
      while (x < 4)
	{
	  transform->matrix[y][x] = (x == y);
	  x++;
	}
      y++;
    }
}

void		copy_transform(t_transform *src, t_transform *dest)
{
  int		x;
  int		y;

  y = 0;
  while (y < 4)
    {
      x = 0;
      while (x < 4)
	{
	  dest->matrix[y][x] = src->matrix[y][x];
	  x++;
        }
      y++;
    }
}

float		dot_product(float *vector1, float *vector2)
{
  float		product;
  int		i;

  product = 0;
  i = 0;
  while (i < 4)
    {
      product += vector1[i] * vector2[i];
      i++;
    }
  return (product);
}

void		combine_transforms(t_transform *t1,
				   t_transform *t2)
{
  int		x;
  int		y;
  float		vector[4];
  t_transform	tmp;

  copy_transform(t1, &tmp);
  y = 0;
  while (y < 4)
    {
      x = 0;
      while (x < 4)
	{
	  get_transform_line(&tmp, y, (float*)&vector);
	  t1->matrix[x][y] = dot_product((float*)&vector,
					 (float*)&t2->matrix[x]);
	  x++;
	}
      y++;
    }
}

void		get_transform_line(t_transform *transform,
				   int line, float *buffer)
{
  int		i;

  i = 0;
  while (i < 4)
    {
      buffer[i] = transform->matrix[i][line];
      i++;
    }
}
