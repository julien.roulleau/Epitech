/*
** homothety.c for raytracer1 in /home/onehandedpenguin/Dev/CGP_2016/raytracer1/src/math/
**
** Made by Paul Laffitte
** Login   <paul.laffitte@epitech.eu>
**
** Started on  Wed Feb  8 21:12:10 2017 Paul Laffitte
** Last update Wed Feb  8 22:14:26 2017 Paul Laffitte
*/

#include "transform.h"

void		get_homothety(sfVector3f *homothety, t_transform *buffer)
{
  init_transform(buffer);
  buffer->matrix[3][0] = homothety->x;
  buffer->matrix[3][1] = homothety->y;
  buffer->matrix[3][2] = homothety->z;
}

void		scale(t_transform *transform, sfVector3f *homothety)
{
  t_transform	homothety_matrix;

  get_homothety(homothety, &homothety_matrix);
  combine_transforms(transform, &homothety_matrix);
}
