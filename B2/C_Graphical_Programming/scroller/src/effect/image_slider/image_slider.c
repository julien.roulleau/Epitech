/*
** text.c for text in /home/loxo/Bureau/EPITECH/RUSH/Scroller/Scrolling text/
**
** Made by Loick Mury
** Login   <loick.mury@epitech.eu@epitech.eu>
**
** Started on  Sat Apr  1 07:34:58 2017 Loick Mury
** Last update Sun Apr  2 21:00:12 2017 Paul Laffitte
*/

#include <unistd.h>
#include "timer.h"
#include "framebuffer.h"

typedef struct	s_slider
{
  int		x;
  int		y;
  int		k;
  sfImage	*image;
  sfVector2u	size_image;
}		t_slider;

void		image_slider(t_fb *buffer, t_slider slider, int k)
{
  int		j;
  unsigned int	i;

  j = slider.y;
  while (j < slider.y + (slider.k - k))
    {
      i = 0;
      while (i < slider.size_image.x)
	{
	  my_put_pixel(buffer, i + slider.x, j + slider.y,
		       sfImage_getPixel(slider.image, i, (slider.k - k)));
	  i = i + 1;
	}
      j = j + 1;
    }
}

void		sliderstruct(t_fb *buffer, sfImage *image, int x, int y)
{
  static float 	tmp = 0;
  static unsigned int	k = 0;
  int		i;
  t_slider	slider;

  i = 0;
  slider.image = image;
  slider.x = x;
  slider.y = y;
  slider.size_image = sfImage_getSize(image);
  slider.k = slider.size_image.y;
  if (k == slider.size_image.y)
    i = 1;
  tmp += timer.delta;
  image_slider(buffer, slider, k);
  if (tmp > 0.05 && i == 0)
  {
    k = k + 1;
    tmp = 0;
  }
}
