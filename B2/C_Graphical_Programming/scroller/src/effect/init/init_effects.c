/*
** init_effects.c for scroller in /home/onehandedpenguin/Dev/CGP_2016/scroller/src/effect/init/
**
** Made by Paul Laffitte
** Login   <paul.laffitte@epitech.eu>
**
** Started on  Sat Apr  1 13:42:37 2017 Paul Laffitte
** Last update Sun Apr  2 21:12:49 2017 Roulleau Julien
*/

#include <SFML/Graphics.h>
#include "src.h"
#include "effect.h"

int		add_effects(t_effect *effects, t_res *res)
{
  if (add_parallax(effects->parallaxes, res->clouds4, -20, 350) == 84
      || add_parallax(effects->parallaxes, res->clouds3, -35, 310) == 84
      || add_parallax(effects->parallaxes, res->clouds2, -55, 230) == 84
      || add_parallax(effects->parallaxes, res->clouds1, -80, 70) == 84
      || add_parallax(effects->parallaxes, res->mountains, -170, 280) == 84
      || add_parallax(effects->parallaxes, res->grass2, -200, 572) == 84
      || add_obj3d(effects->objs3d, VECTOR3F(0, 100, 200),
		   VECTOR3F(73, 30, 42), "ressource/mesh/cube.b3s") == 84
      || !(effects->image = sfImage_createFromFile("ressource/png/title.png"))
      || !(effects->text = deftext("Scrolling")))
      return (84);
  return (0);
}

int		load_res(t_res *res)
{
  res->current_music = 0;
  if (!(res->title = LOAD_TEX("ressource/png/title.png"))
      || !(res->mountains = LOAD_TEX("ressource/png/montagnes.png"))
      || !(res->clouds1 = LOAD_TEX("ressource/png/nuages1.png"))
      || !(res->clouds2 = LOAD_TEX("ressource/png/nuages2.png"))
      || !(res->clouds3 = LOAD_TEX("ressource/png/nuages3.png"))
      || !(res->clouds4 = LOAD_TEX("ressource/png/nuages4.png"))
      || !(res->grass2 = LOAD_TEX("ressource/png/herbe2.png"))
      || !(res->wolf = LOAD_TEX("ressource/sprite/wolf.png"))
      || !(res->sound.atari = LD_MUSIC("ressource/song/atari.ogg"))
      || !(res->sound.axelf = LD_MUSIC("ressource/song/axelf.ogg"))
      || !(res->sound.beep = LD_MUSIC("ressource/song/beep.ogg"))
      || !(res->sound.korg = LD_MUSIC("ressource/song/korg.ogg"))
      || !(res->sound.laser = LD_MUSIC("ressource/song/laser.ogg"))
      || !(res->sound.lol = LD_MUSIC("ressource/song/lol.ogg"))
      || !(res->sound.prophet = LD_MUSIC("ressource/song/prophet.ogg"))
      || !(res->sound.sonic = LD_MUSIC("ressource/song/sonic.ogg"))
      || !(res->music[0] = LD_MUSIC("ressource/music/gameover.ogg"))
      || !(res->music[1] = LD_MUSIC("ressource/music/intro.ogg"))
      || !(res->music[2] = LD_MUSIC("ressource/music/lemmings.ogg"))
      || !(res->music[3] = LD_MUSIC("ressource/music/sotb.ogg"))
      || !(res->music[4] = LD_MUSIC("ressource/music/ymVST.ogg"))
      || !(res->music[5] = LD_MUSIC("ressource/music/zac.ogg")))
    return (84);
  return (0);
}

int		init_effects(t_effect *effects, t_res *res)
{
  if (!(effects->parallaxes = create_list(LINKED_LIST))
      || !(effects->objs3d = create_list(LINKED_LIST))
      || load_res(res) == 84
      || add_effects(effects, res) == 84)
    return (84);
  return (0);
}
