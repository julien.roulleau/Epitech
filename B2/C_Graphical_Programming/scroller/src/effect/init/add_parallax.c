/*
** add_parallax.c for scroller in /home/onehandedpenguin/Dev/CGP_2016/scroller/src/effect/init/
**
** Made by Paul Laffitte
** Login   <paul.laffitte@epitech.eu>
**
** Started on  Sat Apr  1 13:47:02 2017 Paul Laffitte
** Last update Sun Apr  2 13:39:22 2017 Paul Laffitte
*/

#include <stdlib.h>
#include <SFML/Graphics.h>
#include "effect.h"
#include "effects/parallax.h"

int		add_parallax(t_list *parallaxes, sfTexture *texture,
			     float speed, int position)
{
  t_parallax	*parallax;

  if (!(parallax = malloc(sizeof(t_parallax)))
      || !(parallax->sprite = sfSprite_create())
      || push_back(parallaxes, parallax))
    return (84);
  sfSprite_setTexture(parallax->sprite, texture, sfFalse);
  sfSprite_setPosition(parallax->sprite, (sfVector2f){0, position});
  parallax->speed = speed;
  parallax->width = sfTexture_getSize(texture).x;
  return (0);
}
