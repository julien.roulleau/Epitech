/*
** add_parallax.c for scroller in /home/onehandedpenguin/Dev/CGP_2016/scroller/src/effect/init/
**
** Made by Paul Laffitte
** Login   <paul.laffitte@epitech.eu>
**
** Started on  Sat Apr  1 13:47:02 2017 Paul Laffitte
** Last update Sun Apr  2 21:00:15 2017 Paul Laffitte
*/

#include <stdlib.h>
#include <SFML/Graphics.h>
#include "src.h"
#include "effect.h"
#include "effects/parallax.h"

int		add_obj3d(t_list *objs, sfVector3f position,
			  sfVector3f rotation_offset, char *filename)
{
  t_obj3d	*obj;

  if (!(obj = malloc(sizeof(t_obj3d)))
      || !(obj->vertexes = set_obj(filename))
      || !(obj->array = sfVertexArray_create())
      || push_back(objs, obj))
    return (84);
  obj->nb_vertexes = 0;
  while (obj->vertexes[obj->nb_vertexes])
    {
      obj->vertexes[obj->nb_vertexes]->x /= 2;
      obj->vertexes[obj->nb_vertexes]->y /= 2;
      obj->vertexes[obj->nb_vertexes]->z /= 2;
      obj->nb_vertexes++;
    }
  sfVertexArray_setPrimitiveType(obj->array, sfTriangles);
  obj->position = position;
  obj->rotation = VECTOR3F(0, 0, 0);
  obj->rotation_offset = rotation_offset;
  obj->angle = 0;
  obj->nb_triangles = obj->nb_vertexes / 3;
  if (!(obj->triangles = malloc(sizeof(t_triangle) * obj->nb_triangles)))
    return (84);
  return (0);
}
