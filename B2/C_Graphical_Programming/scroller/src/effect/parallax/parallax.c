/*
** parallax.c for scroller in /home/onehandedpenguin/Dev/CGP_2016/scroller/src/effect/parallax/
**
** Made by Paul Laffitte
** Login   <paul.laffitte@epitech.eu>
**
** Started on  Fri Mar 31 23:25:03 2017 Paul Laffitte
** Last update Sun Apr  2 21:00:24 2017 Paul Laffitte
*/

#include "effects/parallax.h"
#include "timer.h"
#include "window.h"

void		parallax(t_window *window, t_parallax *parallax)
{
  sfVector2f	position;

  position = sfSprite_getPosition(parallax->sprite);
  position.x += parallax->speed * timer.delta;
  while (position.x > 0)
    position.x -= parallax->width;
  while (position.x < SCREEN_WIDTH)
    {
      sfSprite_setPosition(parallax->sprite, position);
      sfRenderWindow_drawSprite(window->window, parallax->sprite, NULL);
      position.x += parallax->width;
    }
}
