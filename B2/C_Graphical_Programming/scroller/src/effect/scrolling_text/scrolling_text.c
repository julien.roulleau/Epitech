/*
** scrolling_text.c for scrolling in /home/na/Dropbox/Job/En_Cours
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sat Apr  1 13:43:01 2017 Roulleau Julien
** Last update Sat Apr  1 15:44:22 2017 Paul Laffitte
*/

#include "framebuffer.h"

#define FONT "ressource/font/timesnewarial.ttf"

void		scrolling_text(sfText *text, sfRenderWindow *window,
				sfVector2f offset, int deg)
{
  sfVector2f	pos;

  sfText_setRotation(text, deg);
  sfText_move(text, offset);
  pos = sfText_getPosition(text);
  if (pos.x >= SCREEN_WIDTH)
    {
      pos.x = -150 * offset.x;
      pos.y = -150 * offset.y;
      sfText_setPosition(text, pos);
    }
  else if (pos.y >= SCREEN_HEIGHT)
    {
      pos.x = -150 * offset.x;
      pos.y = -150 * offset.y;
      sfText_setPosition(text, pos);
    }
  sfText_move(text, offset);
  sfRenderWindow_drawText(window, text, NULL);
}

sfText		*deftext(char *sentences)
{
  sfFont	*font;
  sfText        *text;

  if (!(text = sfText_create())
      || !(font = sfFont_createFromFile(FONT)))
    return (NULL);
  sfText_setString(text, sentences);
  sfText_setCharacterSize(text, 100);
  sfText_setFont(text, font);
  return (text);
}
