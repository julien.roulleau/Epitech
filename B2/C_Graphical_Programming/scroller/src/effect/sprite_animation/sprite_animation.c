/*
** sprite_animation.c for sprite_animation in /home/loxo/Bureau/EPITECH/RUSH/Scroller/scroller/src/effect/sprite_animation/
**
** Made by Loick Mury
** Login   <loick.mury@epitech.eu@epitech.eu>
**
** Started on  Sat Apr  1 23:41:18 2017 Loick Mury
** Last update Sun Apr  2 21:07:50 2017 Roulleau Julien
*/

#include <SFML/Graphics.h>
#include "window.h"
#include "timer.h"

sfIntRect	imageremp(int top, int left)
{
  sfIntRect	image;

  image.top = top;
  image.left = left;
  image.width = 100;
  image.height = 100;
  return (image);
}

void	sprite_animation(t_window *window, char *pathname, sfVector2f position)
{
  static float		tmp = 0;
  static unsigned int	l = 0;
  static unsigned int	t = 0;

  window->s_wolf = sfSprite_create();
  window->i_wolf = imageremp(t, l);
  sfSprite_setPosition(window->s_wolf, position);
  if (window->i_wolf.top == 300 && window->i_wolf.left == 200)
    t = 0;
  window->t_wolf = sfTexture_createFromFile(pathname, &window->i_wolf);
  sfSprite_setTexture(window->s_wolf, window->t_wolf, sfTrue);
  sfRenderWindow_drawSprite(window->window, window->s_wolf, NULL);
  tmp += timer.delta;
  if (tmp > 0.1)
    {
      l = l + 100;
      if (l > 400 || (l == 400 && t == 200))
	{
	  l = 0;
	  t = t + 100;
	}
      tmp = 0;
    }
}
