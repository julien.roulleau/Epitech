/*
** tracker.c for scroller in /home/na/Dropbox/Job/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sat Apr  1 13:22:34 2017 Roulleau Julien
** Last update Sun Apr  2 20:58:12 2017 Roulleau Julien
*/

#include "effect.h"

#define SPEED 5

/*
**	set = 1; Pause / Play
**	set = 2; Up volume
**	set = 3; Down volume
*/
int			tracker(sfMusic *track, int set)
{
	sfSoundStatus	status;
	static float	volume = 100;

	status = sfMusic_getStatus(track);
	if (status == sfPaused && set == 1)
		sfMusic_play(track);
	else if (status == sfPlaying && set == 1)
		sfMusic_pause(track);
	else if (set == 2 && volume <= 100 - SPEED)
	{
		volume += SPEED;
		sfMusic_setVolume(track, volume);
	}
	else if (set == 3 && volume >= 0 + SPEED)
	{
		volume -= SPEED;
		sfMusic_setVolume(track, volume);
	}
	return (0);
}

int			check_music(t_res *res)
{
	sfSoundStatus	status;

	status = sfMusic_getStatus(res->music[res->current_music]);
	if (status == sfStopped)
	{
		res->current_music++;
		if (res->current_music == 6)
			res->current_music = 0;
		sfMusic_play(res->music[res->current_music]);
	}
	return (0);
}
