/*
** sort.c for scroller in /home/onehandedpenguin/Dev/CGP_2016/scroller/src/effect/obj3d/
**
** Made by Paul Laffitte
** Login   <paul.laffitte@epitech.eu>
**
** Started on  Sun Apr  2 18:35:59 2017 Paul Laffitte
** Last update Sun Apr  2 21:00:21 2017 Paul Laffitte
*/

#include "window.h"
#include "timer.h"
#include "effects/3d.h"
#include "src.h"
#include "transform.h"

void	set_distances(t_obj3d *obj)
{
  int		i;

  i = 0;
  while (i != obj->nb_triangles)
    {
      obj->triangles[i].dist = (NORM(obj->triangles[i].vertexes[0])
			      + NORM(obj->triangles[i].vertexes[1])
			      + NORM(obj->triangles[i].vertexes[2])) / 3;
      i++;
    }
}

static void	swap_triangles(t_obj3d *obj, int id1, int id2)
{
  t_triangle	tmp;

  tmp = obj->triangles[id2];
  obj->triangles[id2] = obj->triangles[id1];
  obj->triangles[id1] = tmp;
}

void		sort_triangles(t_obj3d *obj)
{
  int		i;
  int		j;

  i = obj->nb_triangles - 1;
  while (i != 0)
    {
      j = 0;
      while (j < i)
	{
	  if (obj->triangles[j + 1].dist < obj->triangles[j].dist)
	    swap_triangles(obj, j, j + 1);
	  j++;
	}
      i--;
    }
}
