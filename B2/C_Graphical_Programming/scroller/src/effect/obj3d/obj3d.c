/*
** obj3d.c for scroller in /home/onehandedpenguin/Dev/CGP_2016/scroller/src/effect/obj3d/
**
** Made by Paul Laffitte
** Login   <paul.laffitte@epitech.eu>
**
** Started on  Sat Apr  1 20:03:12 2017 Paul Laffitte
** Last update Sun Apr  2 20:39:32 2017 Paul Laffitte
*/

#include "window.h"
#include "timer.h"
#include "effects/3d.h"
#include "src.h"
#include "transform.h"

static sfVertex		get_vertex2d(t_obj3d *obj, int id_tri, int id_vert)
{
  sfVector3f		*position;
  sfVertex		vertex;

  position = &obj->triangles[id_tri].vertexes[id_vert];
  vertex.position = projection(position, obj->angle);
  vertex.color = get_color(obj, obj->triangles[id_tri].dist);
  vertex.texCoords = (sfVector2f){0, 0};
  return (vertex);
}

static sfVector3f	place_vertex(t_obj3d *obj, int id_vertex)
{
  sfVector3f		vertex;

  vertex = *(obj->vertexes[id_vertex]);
  rotation3d(&vertex, &obj->rotation);
  vertex = VECTOR_ADD(vertex, obj->position);
  return (vertex);
}

static void		set_triangles(t_obj3d *obj)
{
  int		i;

  i = 0;
  while (i != obj->nb_triangles)
    {
      obj->triangles[i].vertexes[0] = place_vertex(obj, i * 3);
      obj->triangles[i].vertexes[1] = place_vertex(obj, i * 3 + 1);
      obj->triangles[i].vertexes[2] = place_vertex(obj, i * 3 + 2);
      i++;
    }
  set_distances(obj);
}

void			obj3d(t_window *window, t_obj3d *obj)
{
  sfVertexArray		*array;
  int			i;

  (void)window;
  set_triangles(obj);
  sort_triangles(obj);
  set_near_far(obj);
  array = obj->array;
  i = 0;
  while (i < obj->nb_triangles)
    {
      sfVertexArray_append(array, get_vertex2d(obj, i, 0));
      sfVertexArray_append(array, get_vertex2d(obj, i, 1));
      sfVertexArray_append(array, get_vertex2d(obj, i, 2));
      i++;
    }
  sfRenderWindow_drawVertexArray(window->window, array, NULL);
  sfVertexArray_clear(array);
  obj->rotation.x += obj->rotation_offset.x * timer.delta;
  obj->rotation.y += obj->rotation_offset.y * timer.delta;
  obj->rotation.z += obj->rotation_offset.z * timer.delta;
}
