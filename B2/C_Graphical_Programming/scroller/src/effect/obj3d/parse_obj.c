/*
** parse_obj.c for scroller in /home/na/Dropbox/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sat Apr  1 18:07:45 2017 Roulleau Julien
** Last update Sun Apr  2 21:00:18 2017 Paul Laffitte
*/

#include <fcntl.h>
#include "framebuffer.h"
#include "src.h"

static int 	open_skip(char *file)
{
	int	fd;

	if ((fd = open(file, O_RDONLY)) == -1)
		return (84);
	get_next_line(fd);
	get_next_line(fd);
	get_next_line(fd);
	get_next_line(fd);
	return (fd);
}

static char		*get_next_value(int fd)
{
	static char 	**tab = 0;
	static int	i = -1;
	char		*buffer;
	char		*tmp;
	char		*last;

	if (tab == 0)
	{
		if (!(buffer = get_next_line(fd)))
			return (0);
		tab = str_to_wordtab(buffer, ',');
	}
	if (tab[++i] && tab[i + 1])
		return (tab[i]);
	else
	{
		last = tab[i];
		i = -1;
		tab = 0;
		if (!(tmp = get_next_value(fd)))
			return (last);
		return (tmp);
	}
}

static sfVector3f	*get_next_vect(int fd)
{
	sfVector3f	*vect;
	char		*value;

	vect = malloc(sizeof(sfVector3f));
	if (!(value = get_next_value(fd)))
		return (0);
	vect->x = my_getfnbr(value);
	if (!(value = get_next_value(fd)))
		return (0);
	vect->y = my_getfnbr(value);
	if (!(value = get_next_value(fd)))
		return (0);
	vect->z = my_getfnbr(value);
	return (vect);
}

sfVector3f		**set_obj(char *file)
{
	sfVector3f 	**tab;
	t_list 		*list;
	sfVector3f	*data;
	int		fd;

	list = create_list(LINKED_LIST);
	fd = open_skip(file);
	while ((data = get_next_vect(fd)))
		push_back(list, data);
	tab = (sfVector3f **) list_to_tab(list);
	free_list(list);
	return (tab);
}
