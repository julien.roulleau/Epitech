/*
** light.c for scroller in /home/onehandedpenguin/Dev/CGP_2016/scroller/src/effect/obj3d/
**
** Made by Paul Laffitte
** Login   <paul.laffitte@epitech.eu>
**
** Started on  Sun Apr  2 16:30:36 2017 Paul Laffitte
** Last update Sun Apr  2 20:36:13 2017 Paul Laffitte
*/

#include "effects/3d.h"
#include "src.h"

sfColor		get_color(t_obj3d *obj, float distance)
{
  float		light;
  if (distance < 0)
    return (sfBlack);
  light = (distance - obj->near) / (obj->far - obj->near) * 255;
  if (light > 255)
    light = 255;
  else if (light < 0)
    light = 0;
  return ((sfColor){light, light, light, 255});
}

void		set_near_far(t_obj3d *obj)
{
  int		i;
  float		tmp;

  obj->far = 0;
  obj->near = 1000000;
  i = 0;
  while (obj->vertexes[i])
    {
      tmp = NORM(VECTOR_ADD((*(obj->vertexes[i])), (obj->position)));
      if (tmp < obj->near)
	obj->near = tmp;
      if (tmp > obj->far)
	obj->far = tmp;
      i++;
    }
}
