/*
** transformations.c for scroller in /home/onehandedpenguin/Dev/CGP_2016/scroller/src/effect/obj3d/
**
** Made by Paul Laffitte
** Login   <paul.laffitte@epitech.eu>
**
** Started on  Sun Apr  2 20:39:14 2017 Paul Laffitte
** Last update Sun Apr  2 20:40:26 2017 Paul Laffitte
*/

#include <SFML/Graphics.h>
#include <math.h>
#include "transform.h"

sfVector2f	projection(sfVector3f *pos3d, float angle)
{
  sfVector2f	projection;

  angle = angle * M_PI / 180;
  projection.x = pos3d->x - pos3d->y * sin(angle) + 900;
  projection.y = pos3d->y * cos(angle) - pos3d->z + 400;
  return (projection);
}

void		rotation3d(sfVector3f *vertex, sfVector3f *rotation)
{
  t_transform		transform;

  (void)vertex;
  get_rotation(rotation, &transform);
  apply_transform(vertex, &transform, 0);
}
