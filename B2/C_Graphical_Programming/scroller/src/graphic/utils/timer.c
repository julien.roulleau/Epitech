/*
** timer.c for scroller in /home/onehandedpenguin/Dev/CGP_2016/scroller/src/graphic/utils/
**
** Made by Paul Laffitte
** Login   <paul.laffitte@epitech.eu>
**
** Started on  Sat Apr  1 16:28:55 2017 Paul Laffitte
** Last update Sat Apr  1 16:44:19 2017 Paul Laffitte
*/

#include "timer.h"

t_timer		timer;

int		init_timer()
{
  if (!(timer.timer = sfClock_create()))
    return (84);
  timer.delta = 0;
  timer.last_frame = 0;
  return (0);
}

void		update_clock()
{
  sfInt64	now;

  now = sfClock_getElapsedTime(timer.timer).microseconds;
  timer.delta = (now - timer.last_frame) / 1000000.0;
  timer.last_frame = now;
}
