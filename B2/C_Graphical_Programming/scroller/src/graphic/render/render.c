/*
** render.c for raytracer in /home/na/Dropbox/Job/En_Cours/raytracer1/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Thu Mar 16 14:04:02 2017 Roulleau Julien
** Last update Sun Apr  2 21:13:33 2017 Roulleau Julien
*/

#include <unistd.h>
#include "window.h"
#include "effect.h"
#include "timer.h"

static int	event_key(int key_code, t_res res)
{
	if (key_code == sfKeyP)
		tracker(res.music[res.current_music], 1);
	if (key_code == sfKeyO)
		tracker(res.music[res.current_music], 2);
	if (key_code == sfKeyI)
		tracker(res.music[res.current_music], 3);
	if (key_code == sfKeyNum1)
		sfMusic_play(res.sound.atari);
	if (key_code == sfKeyNum2)
		sfMusic_play(res.sound.axelf);
	if (key_code == sfKeyNum3)
		sfMusic_play(res.sound.beep);
	if (key_code == sfKeyNum4)
		sfMusic_play(res.sound.korg);
	if (key_code == sfKeyNum5)
		sfMusic_play(res.sound.laser);
	if (key_code == sfKeyNum6)
		sfMusic_play(res.sound.lol);
	if (key_code == sfKeyNum7)
		sfMusic_play(res.sound.prophet);
	if (key_code == sfKeyNum8)
		sfMusic_play(res.sound.sonic);
	return (0);
}

static int 	event(sfRenderWindow *window, t_res *res)
{
	sfEvent	event;

	check_music(res);
	while (sfRenderWindow_pollEvent(window, &event))
	{
		if (event.type == sfEvtClosed)
			sfRenderWindow_close(window);
		if (event.type == sfEvtKeyPressed)
			event_key(event.key.code, *res);
	}
	if (sfKeyboard_isKeyPressed(sfKeyEscape) == sfTrue)
		sfRenderWindow_close(window);
	return (0);
}

void			display_effect(t_window *window,
				       t_list *data, void *effect)
{
	t_list_elem	*iterator;

	iterator = data->front;
	while (iterator)
	{
		((void(*)(t_window*, void*))effect)(window, iterator->data);
		iterator = iterator->next;
	}
}

void 		display(t_effect *effects, t_window *window)
{
  sliderstruct(window->fb, effects->image, 100, 100);
  scrolling_text(effects->text, window->window, (sfVector2f){1, 0}, 0);
  display_effect(window, effects->parallaxes, &parallax);
  sprite_animation(window, "ressource/sprite/wolf.png",
		   (sfVector2f){100, 500});
  display_effect(window, effects->objs3d, &obj3d);
}

void			render(t_window *window)
{
	t_effect	effects;
	t_res		res;

	if (init_effects(&effects, &res) == 84
	    || init_timer() == 84)
		return;
	while (sfRenderWindow_isOpen(window->window))
	{
		update_clock();
		sfRenderWindow_clear(window->window,
			sfColor_fromRGB(20, 30, 100));
		event(window->window, &res);
		display(&effects, window);
		sfTexture_updateFromPixels(window->texture,
					   window->fb->pixels,
					   SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0);
		sfRenderWindow_drawSprite(window->window,
					  window->sprite, NULL);
		sfRenderWindow_display(window->window);
	}
	kill_window(window, res);
}
