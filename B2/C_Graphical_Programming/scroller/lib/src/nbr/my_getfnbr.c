/*
** my_getfnbr.c for libmy in /home/onehandedpenguin/Dev/libmy/src/basic
**
** Made by Paul Laffitte
** Login   <paul.laffitte@epitech.eu>
**
** Started on  Thu Jan  5 16:48:52 2017 Paul Laffitte
** Last update Sat Apr  1 12:41:29 2017 Paul Laffitte
*/

#include "src.h"

static void	add_decimal(char digit, int pos, float *number)
{
  *number += (digit - '0') / (float)my_power_rec(10, pos + 1)
      * (*number >= 0 ? 1 : -1);
}

static char	*get_after_units(char *str, int int_part)
{
  int		len;

  len = my_nbrlen(int_part);
  if (str[len] != '.')
    return (NULL);
  return (&str[len + 1]);
}

int		my_power_rec(int nb, int p)
{
  if (p == 0)
    return (1);
  if (p == 1)
    return (nb);
  if (p % 2 == 0)
    return (my_power_rec(SQUARE(nb), p / 2));
  if (p % 2 == 1)
    return (nb * my_power_rec(SQUARE(nb), (p - 1) / 2));
  return (0);
}

float		my_getfnbr(char *str)
{
  float		number;
  int		sign;
  int		i;

  number = my_getnbr(str);
  sign = (*str == '-') ? -1 : 1;
  if (!(str = get_after_units(str, number == 0 && sign == -1 ? -1 : number)))
    return (number);
  i = 0;
  while (str[i] >= '0' && str[i] <= '9')
    {
      add_decimal(str[i], i, &number);
      i++;
    }
  if (sign == -1 && number > 0)
    number *= -1;
  return (number);
}
