/*
** my_nbrlen.c for src in /home/infocraft/Job/Lib/my_printf/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun Jan  8 20:41:33 2017 Roulleau Julien
** Last update Sun Jan  8 22:43:04 2017 Roulleau Julien
*/

static int	my_nbrlen_count(int nb, int count)
{
	return (nb > 0 ? my_nbrlen_count(nb / 10, ++count) : count);
}

int		my_nbrlen(int nb)
{
	return (my_nbrlen_count(nb, 0));
}
