/*
** my_put_nbr.c for src in /home/infocraft/Job/Lib/my_printf/src/
**
** Made by Roulleau Julien
** Login   <julien.roulleau@epitech.eu>
**
** Started on  Sun Jan  8 21:06:13 2017 Roulleau Julien
** Last update Sat Apr  1 13:22:21 2017 Paul Laffitte
*/

#include <unistd.h>

int		my_strlen(char *);
void		my_putchar(char c);

void		my_putnbr_base(int nb, char *base)
{
	int	div_max;
	int	len_base;

	div_max = 1;
	len_base = my_strlen(base);
	if (nb < 0)
	{
		write(1, "-", 1);
		nb *= -1;
	}
	while (nb / div_max > len_base - 1)
		div_max *= len_base;
	while (div_max > 0)
	{
		my_putchar(base[nb / div_max % len_base]);
		div_max /= len_base;
	}
}

void			my_putnbr_base_unsigned(unsigned int nb, char *base)
{
	unsigned int	div_max;
	unsigned int	len_base;

	div_max = 1;
	len_base = my_strlen(base);
	while (nb / div_max > len_base - 1)
		div_max *= len_base;
	while (div_max > 0)
	{
		my_putchar(base[nb / div_max % len_base]);
		div_max /= len_base;
	}
}

void				my_putnbr_base_pointer(unsigned long int nb,
							char *base)
{
	unsigned long int	div_max;
	unsigned long int	len_base;

	div_max = 1;
	len_base = my_strlen(base);
	while (nb / div_max > len_base - 1)
		div_max *= len_base;
	while (div_max > 0)
	{
		my_putchar(base[nb / div_max % len_base]);
		div_max /= len_base;
	}
}

void		my_putnbr(int nbr)
{
	my_putnbr_base(nbr, "0123456789");
}
