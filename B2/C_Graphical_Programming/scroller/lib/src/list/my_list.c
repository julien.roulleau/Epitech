/*
** list.c for eval_expr in /home/onehandedpenguin/CPool_evalexpr/
**
** Made by Paul Laffitte
** Login   <paul.laffitte@epitech.eu>
**
** Started on  Tue Oct 25 17:42:53 2016 Paul Laffitte
** Last update Sat Apr  1 13:23:43 2017 Paul Laffitte
*/

#include <stdlib.h>
#include "src.h"

t_list		*create_list(t_type_list type)
{
  t_list	*list;

  if (!(list = malloc(sizeof(t_list))))
    return (NULL);
  list->front = NULL;
  list->back = NULL;
  list->size = 0;
  list->type = type;
  return (list);
}

int		exec_list(t_list *list, t_list_func func, void *data)
{
  t_list_elem	*iterator;
  int		i;

  iterator = list->front;
  i = 0;
  while (i < list->size)
    {
      if (func(list, iterator, data) == 84)
	return (84);
      iterator = iterator->next;
      i += 1;
    }
  return (0);
}

t_list		*rev_list(t_list *list)
{
  t_list_elem	*iterator;
  t_list_elem	*tmp;
  int		i;

  iterator = list->front;
  i = 0;
  while (i < list->size)
    {
      tmp = iterator->next;
      iterator->next = iterator->prev;
      iterator->prev = tmp;
      iterator = tmp;
      i += 1;
    }
  tmp = list->front;
  list->front = list->back;
  list->back = tmp;
  return (list);
}

int		print_list(t_list *list)
{
  if (exec_list(list, (t_list_func)&print_list_elem, NULL) != 84)
    {
      if (list->type == CIRCULAR_LIST)
	my_putstr(" -> CIRCULAR");
      my_putchar('\n');
    }
  else
    return (84);
  return (0);
}

int		free_list(t_list *list)
{
  while (list->size != 0)
    {
      if (pop(list, list->front) == 84)
	return (84);
    }
  free(list);
  return (0);
}
